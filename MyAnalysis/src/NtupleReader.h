#ifndef NtupleReader_h
#define NtupleReader_h

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TMatrixDfwd.h"
#include "TDecompSVD.h"

class PhysObjBase;
class myTrack;
class myJet;
class myElectron;
class myMuon;
class myZee;
class myZmumu;
class myMET;
class myTruth;

class SortMyJet{
 public:
  bool operator () (const myJet *left, const myJet *right) const{
    return (left->p4.Pt()) > (right->p4.Pt());    
  }
};

class SortMyObj{
 public:
  bool operator () (const PhysObjBase *left, const PhysObjBase *right) const{
    return (left->p4.Pt()) > (right->p4.Pt());    
  }
};

class NtupleReader{
 public:
  NtupleReader(const std::string);
  NtupleReader(TFile *, const std::string, int Option=0);
  ~NtupleReader();

  void Test(void);
  int GetEntry(int);
  int GetJetVariables(void);
  int GetDRAWFlag(void);

  int nEntry;
  //private:
  TFile *m_file;
  TTree *m_tree;
  TH1D *hnEventsProcessedBCK;
  TH1D *hnPrimaryVertex;
  TH1D *hProdType;
  TH1D *hSumOfNumber;
  TH1D *hSumOfWeights;
  TH1D *hSumOfWeightsBCK;
  TH1D *hSumOfWeightsSquaredBCK;

  TClonesArray *m_drawJets;
  TClonesArray *m_goodJets;
  TClonesArray *m_truthJets;
  TClonesArray *m_goodMuons;
  TClonesArray *m_msTracks;
  TClonesArray *m_goodElectrons;
  TClonesArray *m_egammaClusters;
  TClonesArray *m_truthParticles;
  TClonesArray *m_conventionalTracks;
  TClonesArray *m_threeLayerTracks;
  TClonesArray *m_fourLayerTracks;
  TClonesArray *m_Tracks;

  bool IsData;
  bool GRL;
  bool IsPassedDRAWFilter;
  bool IsPassedDRAWMuonVeto;
  bool IsPassedDRAWElectronVeto;
  bool IsPassedBadEventVeto;
  bool IsPassedLeptonVeto;

  bool IsPassedBadJet;
  bool IsPassedNCBVeto;
  bool IsPassedBadMuonVeto;
  bool IsPassedBadMuonMETCleaning;

  bool LU_METtrigger_SUSYTools;
  bool LU_SingleElectrontrigger;
  bool LU_SingleMuontrigger;

  double JetMetdPhiMin50;
  double JetMetdPhiMin20;

  double JetMetdPhiMin50_Electron;
  double JetMetdPhiMin20_Electron;
  double JetMetdPhiMin50_Muon;
  double JetMetdPhiMin20_Muon;
  double JetMetdPhiMin50_XeTrigger;
  double JetMetdPhiMin20_XeTrigger;
  double HT;
  double EffMass;
  double Aplanarity;

  double TrigEff_Chain0;
  double TrigEff_Chain1;
  double TrigEff_Chain2;
  double TrigEff_Chain3;
  double TrigEff_Chain4;
  double TrigEff_Chain5;
  double TrigEff_Chain6;
  float weightXsec;
  float weightMCweight;
  float weightSherpaNjets;
  float weightPileupReweighting;
  float weightElectronSF;
  float weightElectronTriggerSF;
  float weightTagElectronTriggerSF;
  float weightMuonSF;
  float weightMuonTriggerSF;
  float weightTagMuonTriggerSF;

  float actualInteractionsPerCrossing;
  float averageInteractionsPerCrossing;
  float CorrectedAverageInteractionsPerCrossing;
  int treatAsYear;
  int prodType;
  UInt_t randomRunNumber;
  UInt_t RunNumber;
  UInt_t lumiBlock;
  ULong64_t EventNumber;
  int DetectorError;
  UInt_t nPrimaryVertex;
  float nTracksFromPrimaryVertex;
  //std::vector<myMET *> *missingET;
  myMET *missingET;
  myMET *missingET_Electron;
  myMET *missingET_Muon;
  myMET *missingET_XeTrigger;
  myMET *missingET_DRAW;
  std::vector<myJet *> *drawJets;
  std::vector<myJet *> *goodJets;
  std::vector<myJet *> *truthJets;
  std::vector<myMuon *> *goodMuons;
  std::vector<myTrack *> *msTracks;
  std::vector<myElectron *> *goodElectrons;
  std::vector<myCaloCluster *> *egammaClusters;
  std::vector<myTruth *> *truthParticles;
  
  std::map<std::string, std::vector<myJet *>> *syst_goodJets;
  std::map<std::string, myMET*> syst_missingET;

  std::vector<myTrack *> *conventionalTracks;
  std::vector<myTrack *> *threeLayerTracks;
  std::vector<myTrack *> *fourLayerTracks;
  std::vector<myTrack *> *Tracks;

};
#endif
