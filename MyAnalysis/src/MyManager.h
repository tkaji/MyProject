#ifndef MyManager_h
#define MyManager_h

#include <iostream>
#include <fstream>
#include <vector>
#include "src/NtupleReader.h"
#include "src/NtupleWriter.h"
#include "src/Constant.h"

#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TFile.h"
#include "TClonesArray.h"
#include "TGraphAsymmErrors.h"

class HistManager;

class MyManager{
 public:
  MyManager(const std::vector<std::string>, const std::string);
  MyManager(const std::string, const std::string);
  ~MyManager();

  int Initialize(void);
  int Finalize(void);
  int Run(void);
  int LoadMyFile(int iOpt=0);
  int FillDisappearingTrack(myTrack *);
  int FillOld4LTracks(myTrack *);
  int FillTruthMatchedInfo(myTrack *, myTruth *);
  int FillTracks(void);
  NtupleReader *GetNtupleReader(std::string);
  int SetCutFlowLevel(void); // almost for 3L
  int SetKinematicsVal(void);// for 3L
  void SetMyOption(int hoge){myOption = hoge;}
  int ApplyTruthMatching(void);
  int DuplicateTrackRemoval(void);
  int CutFlowLevel;
  int CutFlowLevelLowMET;
  std::vector<std::string> InputFileList;
  std::string InputFileName;
  std::string OutputFileName;
  std::string DRAWEffPath="";
  int myOption=0;
  int TrackCategory;
  
  HistManager *myHist_Old4L; // main hist manager
  NtupleReader *myCommon;
  NtupleWriter *my3LTree;
  NtupleWriter *my4LTree;
  TFile *myFile;

  NtupleWriter *myTree_Old4LTracks;
  NtupleReader *myRead_Old4LTracks;

  NtupleWriter *myTree_Old4L_MinimumCutTracks;
  NtupleWriter *myTree_Old4L_Common_SR;
  NtupleWriter *myTree_Old4L_Common_hadCR;
  NtupleWriter *myTree_Old4L_Common_fakeCR;
  NtupleWriter *myTree_Old4L_Common_singleEleCR;
  NtupleWriter *myTree_Old4L_Common_singleMuCR;

  NtupleReader *myRead_Old4L_MinimumCutTracks;
  NtupleReader *myRead_Old4L_Common_SR;
  NtupleReader *myRead_Old4L_Common_hadCR;
  NtupleReader *myRead_Old4L_Common_fakeCR;
  NtupleReader *myRead_Old4L_Common_singleEleCR;
  NtupleReader *myRead_Old4L_Common_singleMuCR;

  std::vector<myTrack *> *old4LTracks;

  std::vector<myTrack *> *old4L_MinimumCutTracks;
  std::vector<myTrack *> *old4L_Common_SRTracks;
  std::vector<myTrack *> *old4L_Common_hadCRTracks;
  std::vector<myTrack *> *old4L_Common_fakeCRTracks;
  std::vector<myTrack *> *old4L_Common_singleEleCRTracks;
  std::vector<myTrack *> *old4L_Common_singleMuCRTracks;

  int    IsolatedHighestTrackID;
  double IsolatedHighestTrackPt;
  int    IsolatedHighestTrackletID;
  double IsolatedHighestTrackletPt;

  double KinematicsVal[12]; // for 3L
  std::vector<myTrack *> *myDisappearingTracks; // for 3L, 4L
  std::vector<int> vTrackLayer; // for 3L, 4L
  TFile *tfDRAWEff;
  TFile *tfMyInput;
  TH2D *hTrigEff[nKinematicsChain][nMETTrig];
  TGraphAsymmErrors *gDRAWEff;
  double SF_DRAWEff;
};

#endif
