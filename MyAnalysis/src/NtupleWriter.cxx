#include "src/PhysicsObjectProxyBase.h"
#include "src/NtupleReader.h"
#include "src/NtupleWriter.h"
#include "TROOT.h"

NtupleWriter::NtupleWriter(const std::string TreeName)
  : m_tree(NULL),IsData(false),GRL(false),IsPassedBadEventVeto(false),IsPassedLeptonVeto(false),LU_METtrigger_SUSYTools(false),LU_SingleElectrontrigger(false),LU_SingleMuontrigger(false),TrackCategory(-1),JetMetdPhiMin50(-1),JetMetdPhiMin20(-1),JetMetdPhiMin50_ForEleCR(-1),JetMetdPhiMin20_ForEleCR(-1),JetMetdPhiMin50_ForMuCR(-1),JetMetdPhiMin20_ForMuCR(-1),HT(-1),EffMass(-1),Aplanarity(-1),SF_Eff_DRAW(-1),TrigEff_Chain0(-1),TrigEff_Chain1(-1),TrigEff_Chain2(-1),TrigEff_Chain3(-1),TrigEff_Chain4(-1),TrigEff_Chain5(-1),TrigEff_Chain6(-1),weightXsec(-1),weightMCweight(-1),weightSherpaNjets(-1),weightPileupReweighting(-1),weightElectronSF(-1),weightElectronTriggerSF(-1),weightMuonSF(-1),weightMuonTriggerSF(-1),weightTagMuonTriggerSF(-1),actualInteractionsPerCrossing(-1),averageInteractionsPerCrossing(-1),CorrectedAverageInteractionsPerCrossing(-1),treatAsYear(-1),prodType(-1),randomRunNumber(0),RunNumber(0),lumiBlock(0),EventNumber(-1),DetectorError(-1),nPrimaryVertex(0),nTracksFromPrimaryVertex(-1),missingET(NULL),missingET_Electron(NULL),missingET_Muon(NULL)
{
  m_goodJets.SetClass("myJet");
  m_goodMuons.SetClass("myMuon");
  m_msTracks.SetClass("myTrack");
  m_goodElectrons.SetClass("myElectron");
  m_egammaClusters.SetClass("myCaloCluster");
  m_truthParticles.SetClass("myTruth");
  m_disappearingTracks.SetClass("myTrack");

  m_tree = new TTree(TreeName.c_str(), TreeName.c_str());

  m_tree->Branch("GRL", &GRL, "GRL/O");
  m_tree->Branch("IsData", &IsData, "IsData/O");
  m_tree->Branch("IsPassedBadEventVeto", &IsPassedBadEventVeto, "IsPassedBadEventVeto/O");
  m_tree->Branch("IsPassedLeptonVeto", &IsPassedLeptonVeto, "IsPassedLeptonVeto/O");
  m_tree->Branch("LU_METtrigger_SUSYTools", &LU_METtrigger_SUSYTools, "LU_METtrigger_SUSYTools/O");
  m_tree->Branch("LU_SingleElectrontrigger", &LU_SingleElectrontrigger, "LU_SingleElectrontrigger/O");
  m_tree->Branch("LU_SingleMuontrigger", &LU_SingleMuontrigger, "LU_SingleMuontrigger/O");

  m_tree->Branch("TrigEff_Chain0"            , &TrigEff_Chain0            , "TrigEff_Chain0/D");
  m_tree->Branch("TrigEff_Chain1"            , &TrigEff_Chain1            , "TrigEff_Chain1/D");
  m_tree->Branch("TrigEff_Chain2"            , &TrigEff_Chain2            , "TrigEff_Chain2/D");
  m_tree->Branch("TrigEff_Chain3"            , &TrigEff_Chain3            , "TrigEff_Chain3/D");
  m_tree->Branch("TrigEff_Chain4"            , &TrigEff_Chain4            , "TrigEff_Chain4/D");
  m_tree->Branch("TrigEff_Chain5"            , &TrigEff_Chain5            , "TrigEff_Chain5/D");
  m_tree->Branch("TrigEff_Chain6"            , &TrigEff_Chain6            , "TrigEff_Chain6/D");

  m_tree->Branch("weightXsec"                , &weightXsec                , "weightXsec/F");
  m_tree->Branch("weightMCweight"            , &weightMCweight            , "weightMCweight/F");
  m_tree->Branch("weightSherpaNjets"         , &weightSherpaNjets         , "weightSherpaNjets/F");
  m_tree->Branch("weightPileupReweighting"   , &weightPileupReweighting   , "weightPileupReweighting/F");
  m_tree->Branch("weightElectronSF"          , &weightElectronSF          , "weightElectronSF/F");
  m_tree->Branch("weightElectronTriggerSF"   , &weightElectronTriggerSF   , "weightElectronTriggerSF/F");
  m_tree->Branch("weightTagElectronTriggerSF", &weightTagElectronTriggerSF, "weightTagElectronTriggerSF/F");
  m_tree->Branch("weightMuonSF"              , &weightMuonSF              , "weightMuonSF/F");
  m_tree->Branch("weightMuonTriggerSF"       , &weightMuonTriggerSF       , "weightMuonTriggerSF/F");
  m_tree->Branch("weightTagMuonTriggerSF"    , &weightTagMuonTriggerSF    , "weightTagMuonTriggerSF/F");

  m_tree->Branch("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, "actualInteractionsPerCrossing/F");
  m_tree->Branch("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, "averageInteractionsPerCrossing/F");
  m_tree->Branch("CorrectedAverageInteractionsPerCrossing", &CorrectedAverageInteractionsPerCrossing, "CorrectedAverageInteractionsPerCrossing/F");

  m_tree->Branch("treatAsYear", &treatAsYear, "treatAsYear/I");
  m_tree->Branch("prodType", &prodType, "prodType/I");
  m_tree->Branch("randomRunNumber", &randomRunNumber, "randomRunNumber/i");
  m_tree->Branch("RunNumber", &RunNumber, "RunNumber/i");
  m_tree->Branch("lumiBlock", &lumiBlock, "lumiBlock/i");
  m_tree->Branch("EventNumber", &EventNumber, "EventNumber/l");
  m_tree->Branch("DetectorError", &DetectorError, "DetectorError/I");
  m_tree->Branch("nPrimaryVertex", &nPrimaryVertex, "nPrimaryVertex/i");
  m_tree->Branch("nTracksFromPrimaryVertex", &nTracksFromPrimaryVertex, "nTracksFromPrimaryVertex/F");
  m_tree->Branch("TrackCategory", &TrackCategory, "TrackCategory/I");

  m_tree->Branch("JetMetdPhiMin50", &JetMetdPhiMin50, "JetMetdPhiMin50/D");
  m_tree->Branch("JetMetdPhiMin20", &JetMetdPhiMin20, "JetMetdPhiMin20/D");
  m_tree->Branch("JetMetdPhiMin50_ForEleCR", &JetMetdPhiMin50_ForEleCR, "JetMetdPhiMin50_ForEleCR/D");
  m_tree->Branch("JetMetdPhiMin20_ForEleCR", &JetMetdPhiMin20_ForEleCR, "JetMetdPhiMin20_ForEleCR/D");
  m_tree->Branch("JetMetdPhiMin50_ForMuCR", &JetMetdPhiMin50_ForMuCR, "JetMetdPhiMin50_ForMuCR/D");
  m_tree->Branch("JetMetdPhiMin20_ForMuCR", &JetMetdPhiMin20_ForMuCR, "JetMetdPhiMin20_ForMuCR/D");
  m_tree->Branch("HT", &HT, "HT/D");
  m_tree->Branch("EffMass", &EffMass, "EffMass/D");
  m_tree->Branch("Aplanarity", &Aplanarity, "Aplanarity/D");
  m_tree->Branch("SF_Eff_DRAW", &SF_Eff_DRAW, "SF_Eff_DRAW/D");

  m_tree->Branch("MET.", "myMET", &missingET, 32000, 99);
  m_tree->Branch("MET_ForEleCR.", "myMET", &missingET_Electron, 32000, 99);
  m_tree->Branch("MET_ForMuCR.", "myMET", &missingET_Muon, 32000, 99);

  m_tree->Branch("GoodJets"          , "TClonesArray", &m_goodJets);
  m_tree->Branch("GoodMuons"         , "TClonesArray", &m_goodMuons);
  m_tree->Branch("MSTracks"          , "TClonesArray", &m_msTracks);
  m_tree->Branch("GoodElectrons"     , "TClonesArray", &m_goodElectrons);
  m_tree->Branch("EgammaClusters"    , "TClonesArray", &m_egammaClusters);
  m_tree->Branch("Truths"            , "TClonesArray", &m_truthParticles);
  m_tree->Branch("DisappearingTracks", "TClonesArray", &m_disappearingTracks);

}

NtupleWriter::~NtupleWriter(){
  delete m_tree; m_tree=NULL;
}

int NtupleWriter::FillBasicInfo(NtupleReader *myCommon){
  GRL = myCommon->GRL;
  IsData = myCommon->IsData;
  IsPassedBadEventVeto = myCommon->IsPassedBadEventVeto;
  IsPassedLeptonVeto = myCommon->IsPassedLeptonVeto;
  LU_METtrigger_SUSYTools = myCommon->LU_METtrigger_SUSYTools;
  LU_SingleElectrontrigger = myCommon->LU_SingleElectrontrigger;
  LU_SingleMuontrigger = myCommon->LU_SingleMuontrigger;

  weightXsec                 = myCommon->weightXsec;
  weightMCweight             = myCommon->weightMCweight;
  weightSherpaNjets          = myCommon->weightSherpaNjets;
  weightPileupReweighting    = myCommon->weightPileupReweighting;
  weightElectronSF           = myCommon->weightElectronSF;
  weightElectronTriggerSF    = myCommon->weightElectronTriggerSF;
  weightTagElectronTriggerSF = myCommon->weightTagElectronTriggerSF;
  weightMuonSF               = myCommon->weightMuonSF;
  weightMuonTriggerSF        = myCommon->weightMuonTriggerSF;
  weightTagMuonTriggerSF     = myCommon->weightTagMuonTriggerSF;

  actualInteractionsPerCrossing  = myCommon->actualInteractionsPerCrossing;
  averageInteractionsPerCrossing = myCommon->averageInteractionsPerCrossing;
  CorrectedAverageInteractionsPerCrossing = myCommon->CorrectedAverageInteractionsPerCrossing;
  treatAsYear = myCommon->treatAsYear;
  prodType = myCommon->prodType;
  randomRunNumber = myCommon->randomRunNumber;
  RunNumber = myCommon->RunNumber;
  lumiBlock = myCommon->lumiBlock;
  EventNumber = myCommon->EventNumber;
  DetectorError = myCommon->DetectorError;
  nPrimaryVertex = myCommon->nPrimaryVertex;
  nTracksFromPrimaryVertex = myCommon->nTracksFromPrimaryVertex;

  JetMetdPhiMin20 = myCommon->JetMetdPhiMin20;
  JetMetdPhiMin50 = myCommon->JetMetdPhiMin50;
  JetMetdPhiMin20_ForEleCR = myCommon->JetMetdPhiMin20_Electron;
  JetMetdPhiMin50_ForEleCR = myCommon->JetMetdPhiMin50_Electron;
  JetMetdPhiMin20_ForMuCR = myCommon->JetMetdPhiMin20_Muon;
  JetMetdPhiMin50_ForMuCR = myCommon->JetMetdPhiMin50_Muon;
  HT = myCommon->HT;
  EffMass = myCommon->EffMass;
  Aplanarity = myCommon->Aplanarity;
  
  missingET = myCommon->missingET;
  missingET_Electron = myCommon->missingET_Electron;
  missingET_Muon = myCommon->missingET_Muon;

  // Fill Good Jets
  m_goodJets.Clear();
  for(unsigned int i=0;i<(myCommon->goodJets->size());i++){
    myJet *myObj = (myJet *)m_goodJets.ConstructedAt(m_goodJets.GetEntries());
    *myObj = *myCommon->goodJets->at(i);
  }

  // Fill Good Muons
  m_goodMuons.Clear();
  for(unsigned int i=0;i<(myCommon->goodMuons->size());i++){
    myMuon *myObj = (myMuon *)m_goodMuons.ConstructedAt(m_goodMuons.GetEntries());
    *myObj = *myCommon->goodMuons->at(i);
  }

  // Fill MS Track
  m_msTracks.Clear();
  for(unsigned int i=0;i<(myCommon->msTracks->size());i++){
    myTrack *myObj = (myTrack *)m_msTracks.ConstructedAt(m_msTracks.GetEntries());
    *myObj = *myCommon->msTracks->at(i);
  }

  // Fill Good Electrons
  m_goodElectrons.Clear();
  for(unsigned int i=0;i<(myCommon->goodElectrons->size());i++){
    myElectron *myObj = (myElectron *)m_goodElectrons.ConstructedAt(m_goodElectrons.GetEntries());
    *myObj = *myCommon->goodElectrons->at(i);
  }

  // Fill EGamma Cluster
  m_egammaClusters.Clear();
  for(unsigned int i=0;i<(myCommon->egammaClusters->size());i++){
    myCaloCluster *myObj = (myCaloCluster *)m_egammaClusters.ConstructedAt(m_egammaClusters.GetEntries());
    *myObj = *myCommon->egammaClusters->at(i);
  }

  // Fill Truth Particles
  m_truthParticles.Clear();
  for(unsigned int i=0;i<(myCommon->truthParticles->size());i++){
    myTruth *myObj = (myTruth *)m_truthParticles.ConstructedAt(m_truthParticles.GetEntries());
    *myObj = *myCommon->truthParticles->at(i);
  }
  
  m_disappearingTracks.Clear();
  TrackCategory = 0;
  SF_Eff_DRAW = 1.0;

  return 0;
}

int NtupleWriter::FillDisappearingTrack(myTrack *track){
  myTrack *myObj = (myTrack *)m_disappearingTracks.ConstructedAt(m_disappearingTracks.GetEntries());
  *myObj = *track;

  return 0;
}

int NtupleWriter::FillTrackCategory(int Val){
  TrackCategory = Val;
  return 0;
}

int NtupleWriter::Write(void){
  m_tree->Write();
  return 0;
}

int NtupleWriter::Fill(void){
  m_tree->Fill();
  return 0;
}

int NtupleWriter::FillDRAWEff(double val){
  SF_Eff_DRAW = val;
  return 0;
}

int NtupleWriter::FillTrigEff(double *val){
  TrigEff_Chain0 = val[0];
  TrigEff_Chain1 = val[1];
  TrigEff_Chain2 = val[2];
  TrigEff_Chain3 = val[3];
  TrigEff_Chain4 = val[4];
  TrigEff_Chain5 = val[5];
  TrigEff_Chain6 = val[6];

  return 0;
}
