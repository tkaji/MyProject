#ifndef __MyAnalysis__Constant_H_
#define __MyAnalysis__Constant_H_

#include <functional>

const double Z0Max=5.0;
const double Z_mass = 91.1876e+3;
const double ElectronMass = 0.510998928;
const double MuonMass = 105.6583715;
const double TauMass = 1776.86;
//const double LowPtThreshold = 100.0; // GeV
const double LowPtThreshold = 58.48035; // GeV
//const double LowPtThreshold = 60.0; // GeV
//const double LowPtThreshold = 50.0; // GeV
const double VeryLowMET_Strong = 100.0;
const double LowMET_Strong = 150.0;
const double VeryLowMET_EWK = 100.0;
const double LowMET_EWK = 200.0;

const int nPileupCategory=7;
const int nCaloCategory=5;

const int nMETTrig = 8; // number of MET triggers in 2015-2018
const std::string METTrigName[nMETTrig] = {"HLT_xe70_mht", "HLT_xe90_mht_L1XE50", "HLT_xe100_mht_L1XE50", "HLT_xe110_mht_L1XE50", "HLT_xe110_pufit_L1XE55", "HLT_xe110_pufit_L1XE50", "HLT_xe110_pufit_xe70_L1XE50", "HLT_xe110_pufit_xe65_L1XE50"};

// binning for track pT
//const int nLogPt = 24;
//const double XBinsLogPt[nLogPt+1] = {20, 22.8706, 26.1532, 29.907, 34.1995, 39.1082, 44.7214, 51.1402, 58.4804, 66.874, 76.4724, 87.4485, 100, 130.766, 170.998, 223.607, 292.402, 382.362, 500, 747.674, 1118.03, 1671.85, 2500, 5590.17, 12500};

//const double XBinsLogPt[nLogPt+1] = {20, 26.15321, 34.19952, 44.72136, 58.48035, 76.47245, 100, 130.766, 170.9976, 223.6068, 292.4018, 382.3622, 500, 653.8302, 854.988, 1118.034, 1462.009, 1911.811, 2500, 3269.151, 4274.94, 5590.17, 7310.044, 9559.056, 12500};
const int nLogPt = 12;
const double XBinsLogPt[nLogPt+1] = {20, 26.1532, 34.1995, 44.7214, 58.4804, 76.4724, 100, 170.998, 292.402, 500, 1118.03, 2500, 12500};

//const int nLogPt = 24;
//const int nLogPt = 5;
//const int nLogPt = 21;
//const int nLogPt = 10;
//const double XBinsLogPt[nLogPt+1] = {20.0, 30.0, 40.0, 60.0, 100.0, 12500.0};
//const double XBinsLogPt[nLogPt+1] = {20.0, 30.0, 40.0, 60.0, 100.0, 200.0};
//const double XBinsLogPt[nLogPt+1] = {10.0, 12.5, 15.0, 17.5, 20.0, 30.0, 40.0, 50.0, 60.0, 100.0, 2500.0};
//const double XBinsLogPt[nLogPt+1] = {10.0, 11.220185, 12.589254, 14.125375, 15.848932, 17.782794, 19.952623, 22.387211, 25.118864, 28.183829, 31.622777, 35.481339, 39.810717, 44.668359, 50.118723, 56.234133, 63.095734, 70.794578, 79.432823, 89.125094, 100.0, 12500.0};

// binning for transfer factor 
const int nLogPtForLepton = 90;
const double XBinsLogPtForLepton[nLogPtForLepton+1] = {4, 4.37413, 4.78325, 5.23064, 5.71988, 6.25487, 6.8399, 7.47966, 8.17925, 8.94427, 9.78085, 10.6957, 11.6961, 12.79, 13.9863, 15.2945, 16.725, 18.2894, 20, 21.8706, 23.9163, 26.1532, 28.5994, 31.2744, 34.1995, 37.3983, 40.8962, 44.7214, 48.9043, 53.4784, 58.4804, 63.9502, 69.9316, 76.4724, 83.6251, 91.4468, 100, 109.353, 119.581, 130.766, 142.997, 156.372, 170.998, 186.991, 204.481, 223.607, 244.521, 267.392, 292.402, 319.751, 349.658, 382.362, 418.126, 457.234, 500, 546.766, 597.907, 653.83, 714.985, 781.859, 854.988, 934.957, 1022.41, 1118.03, 1222.61, 1336.96, 1462.01, 1598.75, 1748.29, 1911.81, 2090.63, 2286.17, 2500, 2733.83, 2989.53, 3269.15, 3574.92, 3909.29, 4274.94, 4674.79, 5112.03, 5590.17, 6113.03, 6684.8, 7310.04, 7993.77, 8741.45, 9559.06, 10453.1, 11430.8, 12500};

const double XBinsLogPt13ForLepton[13+1] = {4, 5.23064, 6.8399, 8.94427, 11.6961, 15.2945, 20, 26.1532, 34.1995, 44.7214, 58.4804, 76.4724, 100, 12500};

const double XBinsLogPt18ForLepton[18+1] = {4, 4.78325, 5.71988, 6.8399, 8.17925, 9.78085, 11.6961, 13.9863, 16.725, 20, 23.9163, 28.5994, 34.1995, 40.8962, 48.9043, 58.4804, 69.9316, 83.6251, 12500};
const double XBinsLogPt10ForLepton[10+1] = {4, 4.78325, 5.71988, 6.8399, 8.17925, 9.78085, 11.6961, 13.9863, 16.725, 20, 12500};

// number of kinematics cut in strong channel : MET, 1st-3rd jet pT, dPhi
const int nOldKinematicsCut_Strong = 5; // 0x1F
const std::string TitleOldKinematicsCut_Strong[nOldKinematicsCut_Strong] = { /*  0 */ "MET [GEV]",
									     /*  1 */ "1st jet p_{T} [GeV]",
									     /*  2 */ "2nd jet p_{T} [GeV]",
									     /*  3 */ "3rd jet p_{T} [GeV]",
									     /*  4 */ "#Delta#phi (MET, jets p_{T>50GeV})"};
const double MinOldKinematicsCut_Strong[nOldKinematicsCut_Strong] = { /*  0 */ 0.0,
								      /*  1 */ 0.0,
								      /*  2 */ 0.0,
								      /*  3 */ 0.0,
								      /*  4 */ 0.0 };
const double MaxOldKinematicsCut_Strong[nOldKinematicsCut_Strong] = { /*  0 */ 1000.0,
								      /*  1 */ 1000.0,
								      /*  2 */ 1000.0,
								      /*  3 */ 1000.0,
								      /*  4 */ TMath::Pi()};
const int BinOldKinematicsCut_Strong[nOldKinematicsCut_Strong] = { /*  0 */ 1000,
								   /*  1 */ 1000,
								   /*  2 */ 1000,
								   /*  3 */ 1000,
								   /*  4 */ 100 };

// number of kinematics cut in strong channel : MET, 1st-3rd jet pT, dPhi
const int nOldKinematicsCut_EWK = 3; // 0x7
const std::string TitleOldKinematicsCut_EWK[nOldKinematicsCut_EWK] = { /*  0 */ "MET [GEV]",
								       /*  1 */ "1st jet p_{T} [GeV]",
								       /*  2 */ "#Delta#phi (MET, jets p_{T>50GeV})"};
const double MinOldKinematicsCut_EWK[nOldKinematicsCut_EWK] = { /*  0 */ 0.0,
								/*  1 */ 0.0,
								/*  2 */ 0.0 };
const double MaxOldKinematicsCut_EWK[nOldKinematicsCut_EWK] = { /*  0 */ 1000.0,
								/*  1 */ 1000.0,
								/*  2 */ TMath::Pi()};
const int BinOldKinematicsCut_EWK[nOldKinematicsCut_EWK] = { /*  0 */ 1000,
							     /*  1 */ 1000,
							     /*  2 */ 100 };

// number of track selections including calo veto
const int nOld4LTracksCut = 19;  // 0x7FFFF
const std::string TitleOld4LTracksCut[nOld4LTracksCut] = { /*  0 */"Track q/p_{T} [GeV^{-1}]",
							   /*  1 */"number of SCT hits",
							   /*  2 */"number of contrib pixel layer",
							   /*  3 */"number of inner-most pixel layer hits",
							           
							   /*  4 */"number of ganged flagged fakes",
							   /*  5 */"number of silicon holes",
							   /*  6 */"number of pixel spoilt hits",
							   /*  7 */"number of pixel outliers",
							           
							   /*  8 */"d_{0} significance",
							   /*  9 */"z_{0} sin#theta [mm]",
							   /* 10 */"ptcone40/p_{T}",
							   /* 11 */"is passed leading track",
							           
							   /* 12 */"#DeltaR_{jets}",
							   /* 13 */"#DeltaR_{e}",
							   /* 14 */"#DeltaR_{#mu}",
							   /* 15 */"#DeltaR_{MS}",
							           
							   /* 16 */"track quality",
							   /* 17 */"track #eta",
							   /* 18 */"etclus20_topo [GeV]"};                  

const std::string LabelOld4LTracksCut[nOld4LTracksCut] = { /*  0 */ "20 GeV < Track pT < 100 GeV",
							   /*  1 */ "nSCT Hits == 0",
							   /*  2 */ "nContribPixelLayer >= 4",
							   /*  3 */ "innermost pixel layer", 
							  
							   /*  4 */ "nGangedFlaggedFake==0",
							   /*  5 */ "nSiHoles==0",
							   /*  6 */ "PixelSpoiltHits==0",
							   /*  7 */ "nPixelOutliers==0",

							   /*  8 */ "|d0 significance| < 1.5",
							   /*  9 */ "|z0 sintheta| < 0.5",
							   /* 10 */ "ptcone40/pT < 0.04",
							   /* 11 */ "IsIsolatedLeading",

							   /* 12 */ "dR(Jet50) > 0.4",
							   /* 13 */ "dR(Electron) > 0.4",
							   /* 14 */ "dR(Muon) > 0.4",
							   /* 15 */ "dR(MS Track) > 0.4",

							   /* 16 */ "Quality > 0.1",
							   /* 17 */ "0.1 < |eta| < 1.9",
							   /* 18 */ "etclus20_topo < 5 GeV"};

const int BinOld4LTracksCut[nOld4LTracksCut] = { /*  0 */ 200, // "Track q/p_{T} [GeV^{-1}]",
						 /*  1 */ 20, // "number of SCT hits",
						 /*  2 */ 8, // "number of contrib pixel layer",
						 /*  3 */ 5, // "number of inner-most pixel layer hits",
						 
						 /*  4 */ 5, // "number of ganged flagged fakes",
						 /*  5 */ 5, // "number of silicon holes",
						 /*  6 */ 5, // "number of pixel spoilt hits",
						 /*  7 */ 5, // "number of pixel outliers",
			  			 
						 /*  8 */ 50, // "d_{0} significance",
						 /*  9 */ 50, //  "|z_{0} sin#theta|",
						 /* 10 */ 250, // "ptcone40/p_{T}",
						 /* 11 */ 2, //  "is passed leading track",
			  			         
						 /* 12 */ 50, // "#DeltaR_{jets}",
						 /* 13 */ 50, //  "#DeltaR_{e}",
						 /* 14 */ 50, //  "#DeltaR_{#mu}",
						 /* 15 */ 50, //  "#DeltaR_{MS}",
			 			         
						 /* 16 */ 21, //  "track quality",
						 /* 17 */ 50, // "track #eta"
						 /* 18 */ 50}; // etclus20

const double MinOld4LTracksCut[nOld4LTracksCut] = { /*  0 */ -0.05, // "Track q/p_{T} [GeV^{-1}]",
						    /*  1 */ 0, // "number of SCT hits",
						    /*  2 */ 0, // "number of contrib pixel layer",
						    /*  3 */ 0, // "number of inner-most pixel layer hits",
						            
						    /*  4 */ 0, // "number of ganged flagged fakes",
						    /*  5 */ 0, // "number of silicon holes",
						    /*  6 */ 0, // "number of pixel spoilt hits",
						    /*  7 */ 0, // "number of pixel outliers",
						            
						    /*  8 */ -3.0, // "d_{0} significance",
						    /*  9 */ -2.0, //  "z_{0} sin#theta",
						    /* 10 */ 0.0, // "ptcone40/p_{T}",
						    /* 11 */ 0, //  "is passed leading track",
						            
						    /* 12 */ 0.0, // "#DeltaR_{jets}",
						    /* 13 */ 0.0, //  "#DeltaR_{e}",
						    /* 14 */ 0.0, //  "#DeltaR_{#mu}",
						    /* 15 */ 0.0, //  "#DeltaR_{MS}",
						            
						    /* 16 */ 0.0, //  "track quality",
						    /* 17 */ -2.5, // "track #eta"};
						    /* 18 */ 0.0}; // etclus20

const double MaxOld4LTracksCut[nOld4LTracksCut] = { /*  0 */ 0.05, // "Track q/p_{T} [GeV^{-1}]",
						    /*  1 */ 20, // "number of SCT hits",
						    /*  2 */ 8, // "number of contrib pixel layer",
						    /*  3 */ 5, // "number of inner-most pixel layer hits",
						            
						    /*  4 */ 5, // "number of ganged flagged fakes",
						    /*  5 */ 5, // "number of silicon holes",
						    /*  6 */ 5, // "number of pixel spoilt hits",
						    /*  7 */ 5, // "number of pixel outliers",
						            
						    /*  8 */ 3.0, // "d_{0} significance",
						    /*  9 */ 2.0, //  "z_{0} sin#theta",
						    /* 10 */ 1.0, // "ptcone40/p_{T}",
						    /* 11 */ 2, //  "is passed leading track",
						            
						    /* 12 */ 1.0, // "#DeltaR_{jets}",
						    /* 13 */ 1.0, //  "#DeltaR_{e}",
						    /* 14 */ 1.0, //  "#DeltaR_{#mu}",
						    /* 15 */ 1.0, //  "#DeltaR_{MS}",
						            
						    /* 16 */ 1.05, //  "track quality",
						    /* 17 */ 2.5, // "track #eta"};
						    /* 18 */ 20.0}; // etclus20

const int nLabelKinematicsCutFlow_Strong = 9;
const std::string LabelKinematicsCutFlow_Strong[nLabelKinematicsCutFlow_Strong] = { /*  0 */ "All Events",
										    /*  1 */ "GRL and Event Cleaning",
										    /*  2 */ "MET Trigger", 
										    /*  3 */ "Lepton VETO", 
										    /*  4 */ "MET > 150 GeV",
										    /*  5 */ "1st jet p_{T} > 100 GeV",
										    /*  6 */ "2nd jet p_{T} >  50 GeV",
										    /*  7 */ "3rd jet p_{T} >  50 GeV",
										    /*  8 */ "#Delta#phi (MET, jets p_{T} > 50GeV) > 0.4"};

const int nLabelKinematicsCutFlow_EWK = 7;
const std::string LabelKinematicsCutFlow_EWK[nLabelKinematicsCutFlow_EWK] = { /*  0 */ "All Events",
									      /*  1 */ "GRL and Event Cleaning",
									      /*  2 */ "MET Trigger", 
									      /*  3 */ "Lepton VETO", 
									      /*  4 */ "MET > 200 GeV",
									      /*  5 */ "1st jet p_{T} > 150 GeV",
									      /*  6 */ "#Delta#phi (MET, jets p_{T} > 50GeV) > 1.0"};


const int nLabelCommon = 4;
const std::string LabelCommon[nLabelCommon] = { /*  0 */ "All Events",
						/*  1 */ "GRL and Event Cleaning",
						/*  2 */ "MET Trigger", 
						/*  3 */ "Lepton VETO", };
const int nKinematicsCut = 12; // 0xFFF
//const int nKinematicsChain = 21;
const int nKinematicsChain = 7;
//const int nKinematicsChain = 11;
/*
const double KinematicsChain[nKinematicsChain][nKinematicsCut] = {
  //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  200.0,  150.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 0
  {  200.0,  140.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 1
  {  200.0,  130.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 2
  {  200.0,  120.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 3
  {  200.0,  110.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 4
  {  200.0,  100.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 5
  {  200.0,   90.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 6
  {  200.0,   80.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 7
  {  200.0,   70.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 8
  {  200.0,   60.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 9
  {  200.0,   50.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 10
}; // KinematicsChain
*/

const double KinematicsChain[nKinematicsChain][nKinematicsCut] = {
  //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  200.0,  150.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 0 
  {  200.0,  100.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 1 EWK
  {  200.0,  150.0,  100.0,   50.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 2 
  {  250.0,  100.0,   20.0,   20.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 3 Strong
  {  200.0,  100.0,   20.0,   20.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 4 
  {  140.0,  140.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 5
    {  150.0,  100.0,   50.0,   50.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 6

  //  {  200.0,  100.0,   50.0,   20.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 5
  //  {  200.0,  100.0,   50.0,   50.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 6
}; // KinematicsChain
/*
const double KinematicsChain[nKinematicsChain][nKinematicsCut] = {
  //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  200.0,  150.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 0
  {  200.0,  400.0,   20.0, -999.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 1
  {  200.0,  300.0,   50.0, -999.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 2
  {  200.0,  200.0,  150.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 3
    //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  200.0,  200.0,  100.0, -999.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 4
  {  200.0,  200.0,   50.0,   50.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 5
  {  200.0,  150.0,  100.0,   50.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 6
  {  200.0,  100.0,  100.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 7
  //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  200.0,  100.0,  100.0, -999.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 8
  {  200.0,  100.0,   50.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 9
  {  250.0,  100.0,  100.0,   50.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 10
  {  250.0,  100.0,   50.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 11
  //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  250.0,  100.0,   20.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 12
  {  250.0,  100.0,   20.0,   20.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 13
  {  300.0,  100.0,   50.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 14
  {  300.0,  100.0,   20.0,   20.0,   20.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 15
  //   MET,   Jet1,   Jet2,   Jet3,   Jet4, dPhi50, dPhi20,     HT,   Meff, Aplanarity,   MET/sqrt{HT}, MET/Meff
  {  300.0,  100.0,   20.0,   20.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 16
  {  200.0,  100.0,   20.0,   20.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 17
  {  150.0,  100.0,   50.0,   50.0, -999.0,    0.4, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 18
  {  250.0,  150.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 19

  {  200.0,  100.0, -999.0, -999.0, -999.0,    1.0, -999.0, -999.0, -999.0,     -999.0,         -999.0,   -999.0}, // 20
}; // KinematicsChain
*/
const std::string KinematicsCutLabel[nKinematicsCut] = { /*  0 */ "E_{T}^{miss} [GEV]",
							 /*  1 */ "1st jet p_{T} [GeV]",
							 /*  2 */ "2nd jet p_{T} [GeV]",
							 /*  3 */ "3rd jet p_{T} [GeV]",
							 /*  4 */ "4th jet p_{T} [GeV]",
							 /*  5 */ "#Delta#phi (MET, jets p_{T>50GeV})",
							 /*  6 */ "#Delta#phi (MET, jets p_{T>20GeV})",
							 /*  7 */ "H_{T} [GeV]",
							 /*  8 */ "m_{eff} [GeV]",
							 /*  9 */ "Aplanarity",
							 /* 10 */ "MET/#sqrt{H_{T}} [GeV^{1/2}]",
							 /* 11 */ "MET/m_{eff}"};
const double KinematicsCutMin[nKinematicsCut] = { /*  0 */ 0.0,
						  /*  1 */ 0.0,
						  /*  2 */ 0.0,
						  /*  3 */ 0.0,
						  /*  4 */ 0.0,
						  /*  5 */ 0.0,
						  /*  6 */ 0.0,
						  /*  7 */ 0.0,
						  /*  8 */ 0.0,
						  /*  9 */ 0.0,
						  /* 10 */ 0.0,
						  /* 11 */ 0.0};

const double KinematicsCutMax[nKinematicsCut] = { /*  0 */ 1000.0,
						  /*  1 */ 1000.0,
						  /*  2 */ 1000.0,
						  /*  3 */ 1000.0,
						  /*  4 */ 1000.0,
						  /*  5 */ TMath::Pi(),
						  /*  6 */ TMath::Pi(),
						  /*  7 */ 2500.0,
						  /*  8 */ 2500.0,
						  /*  9 */ 1.0,
						  /* 10 */ 50.0,
						  /* 11 */ 1.0};

const int KinematicsCutBin[nKinematicsCut] = { /*  0 */ 200,
					       /*  1 */ 200,
					       /*  2 */ 200,
					       /*  3 */ 200,
					       /*  4 */ 200,
					       /*  5 */ 200,
					       /*  6 */ 200,
					       /*  7 */ 200,
					       /*  8 */ 200,
					       /*  9 */ 200,
					       /* 10 */ 200,
					       /* 11 */ 200};


/* **********  For 3L Track (no maintenance)  ********** */
const int nTrackQualityFor3L=13;
const std::string BasicTrackValLabel[nTrackQualityFor3L] = { /*  0 */ "d0 significance",
							     /*  1 */ "z0 sin#theta",
							     /*  2 */ "KVU #eta",
							     /*  3 */ "KVU Quality",
							     /*  4 */ "KVU Pt [GeV]",
							     /*  5 */ "number of spoilt hits",
							     /*  6 */ "number of ganged flagged fakes",
							     /*  7 */ "#DeltaR(jets)",
							     /*  8 */ "#DeltaR(e)",
							     /*  9 */ "#DeltaR(#mu)",
							     /* 10 */ "#DeltaR(MS track)",
							     /* 11 */ "ptcone40/p_{T}",
							     /* 12 */ "etclus20_topo [GeV]"};
//							/* 13 */ "number of TRT hits + outliers"};

const double BasicTrackValMin[nTrackQualityFor3L] = { /*  0 */ -20.0,
						      /*  1 */ -5.0,
						      /*  2 */ -2.5,
						      /*  3 */ 0.0,
						      /*  4 */ 0.0,
						      /*  5 */ 0.0,
						      /*  6 */ 0.0,
						      /*  7 */ 0.0,
						      /*  8 */ 0.0,
						      /*  9 */ 0.0,
						      /* 10 */ 0.0,
						      /* 11 */ 0.0,
						      /* 12 */ 0.0};
//						 /* 13 */ 0.0};

const double BasicTrackValMax[nTrackQualityFor3L] = { /*  0 */ 20.0,
						      /*  1 */ 5.0,
						      /*  2 */ 2.5,
						      /*  3 */ 1.0,
						      /*  4 */ 1000.0,
						      /*  5 */ 5.0,
						      /*  6 */ 5.0,
						      /*  7 */ 3.0,
						      /*  8 */ 3.0,
						      /*  9 */ 3.0,
						      /* 10 */ 3.0,
						      /* 11 */ 1.0,
						      /* 12 */ 50.0};
//						 /* 13 */ 60.0};

const int BasicTrackValBin[nTrackQualityFor3L] = { /*  0 */ 160,
						   /*  1 */ 100,
						   /*  2 */ 50,
						   /*  3 */ 50,
						   /*  4 */ 50,
						   /*  5 */ 5,
						   /*  6 */ 5,
						   /*  7 */ 60,
						   /*  8 */ 60,
						   /*  9 */ 60,
						   /* 10 */ 60,
						   /* 11 */ 50,
						   /* 12 */ 50};
//					      /* 13 */ 60};

const int nCutFlowFor3L = 10;
const std::string CutFlowLabelFor3L[nCutFlowFor3L] = { /*  0 */ "All Events",
						  /*  1 */ "GRL and Event Cleaning",
						  /*  2 */ "xe triggers", 
						  /*  3 */ "Lepton VETO", 
						  /*  4 */ "MET > 250 GeV",
						  /*  5 */ "1st jet p_{T} > 80 GeV",
						  /*  6 */ "2nd jet p_{T} > 80 GeV",
						  /*  7 */ "3rd jet p_{T} > 40 GeV",
						  /*  8 */ "4th jet p_{T} > 20 GeV",
						  /*  9 */ "#Delta#phi (MET, jets p_{T} > 50GeV) > 0.4"};


#endif
