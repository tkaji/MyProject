#ifndef MyNtupleWriter_h
#define MyNtupleWriter_h

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "TTree.h"
#include "TFile.h"
#include "src/NtupleReader.h"

class PhysObjBase;
class myTrack;
class myJet;
class myElectron;
class myMuon;
class myZee;
class myZmumu;
class myMET;
class myTruth;

class NtupleWriter{
 public:
  //NtupleWriter(const std::string);
  NtupleWriter(const std::string);
  ~NtupleWriter();
  int FillBasicInfo(NtupleReader *);
  int FillDisappearingTrack(myTrack *);
  int FillTrackCategory(int);
  int FillDRAWEff(double);
  int FillTrigEff(double *);
  int Write(void);
  int Fill(void);

  TTree *m_tree;
  bool IsData;
  bool GRL;
  bool IsPassedBadEventVeto;
  bool IsPassedLeptonVeto;
  bool LU_METtrigger_SUSYTools;
  bool LU_SingleElectrontrigger;
  bool LU_SingleMuontrigger;

  int TrackCategory;
  double JetMetdPhiMin50;
  double JetMetdPhiMin20;
  double JetMetdPhiMin50_ForEleCR;
  double JetMetdPhiMin20_ForEleCR;
  double JetMetdPhiMin50_ForMuCR;
  double JetMetdPhiMin20_ForMuCR;
  double HT;
  double EffMass;
  double Aplanarity;
  double SF_Eff_DRAW;
  double TrigEff_Chain0;
  double TrigEff_Chain1;
  double TrigEff_Chain2;
  double TrigEff_Chain3;
  double TrigEff_Chain4;
  double TrigEff_Chain5;
  double TrigEff_Chain6;

  float weightXsec;
  float weightMCweight;
  float weightSherpaNjets;
  float weightPileupReweighting;
  float weightElectronSF;
  float weightElectronTriggerSF;
  float weightTagElectronTriggerSF;
  float weightMuonSF;
  float weightMuonTriggerSF;
  float weightTagMuonTriggerSF;

  float actualInteractionsPerCrossing;
  float averageInteractionsPerCrossing;
  float CorrectedAverageInteractionsPerCrossing;
  int treatAsYear;
  int prodType;
  UInt_t randomRunNumber;
  UInt_t RunNumber;
  UInt_t lumiBlock;
  ULong64_t EventNumber;
  int DetectorError;
  UInt_t nPrimaryVertex;
  float nTracksFromPrimaryVertex;

  TClonesArray m_goodJets;
  TClonesArray m_goodMuons;
  TClonesArray m_msTracks;
  TClonesArray m_goodElectrons;
  TClonesArray m_egammaClusters;
  TClonesArray m_truthParticles;
  TClonesArray m_disappearingTracks;

  myMET *missingET;
  myMET *missingET_Electron;
  myMET *missingET_Muon;
  /*
  std::vector<myJet *> *goodJets;
  std::vector<myMuon *> *goodMuons;
  std::vector<myTrack *> *msTracks;
  std::vector<myElectron *> *goodElectrons;
  std::vector<myCaloCluster *> *egammaClusters;
  std::vector<myTruth *> *truthParticles;
  std::vector<myTrack *> *disappearingTracks;
  */
};


#endif

