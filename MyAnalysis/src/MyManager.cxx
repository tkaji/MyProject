#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/Utility.h"
#include "src/HistManager.h"
#include "TROOT.h"
#include "TBasket.h"

MyManager::MyManager(const std::vector<std::string> inFileList, const std::string outFileName)
{  
  InputFileList = inFileList;
  OutputFileName = outFileName;
}

MyManager::MyManager(const std::string inFileName, const std::string outFileName)
{  
  InputFileList.push_back(inFileName);
  OutputFileName = outFileName;
}

MyManager::~MyManager()
{  
  delete myRead_Old4LTracks;          myRead_Old4LTracks=NULL;

  //delete myRead_Old4L_MinimumCutTracks;   myRead_Old4L_MinimumCutTracks=NULL;
  delete myRead_Old4L_Common_SR;          myRead_Old4L_Common_SR=NULL;
  delete myRead_Old4L_Common_hadCR;       myRead_Old4L_Common_hadCR=NULL;
  delete myRead_Old4L_Common_fakeCR;      myRead_Old4L_Common_fakeCR=NULL;
  delete myRead_Old4L_Common_singleEleCR; myRead_Old4L_Common_singleEleCR=NULL;
  delete myRead_Old4L_Common_singleMuCR;  myRead_Old4L_Common_singleMuCR=NULL;

  delete myHist_Old4L; myHist_Old4L=NULL;

  delete myFile; myFile=NULL;
}


int MyManager::Initialize(void){
  myFile = new TFile(OutputFileName.c_str(), "RECREATE");
  gROOT->cd();
  myHist_Old4L = new HistManager("Old4L");
  myHist_Old4L->Init();

  myDisappearingTracks = new std::vector<myTrack *>;

  old4LTracks = new std::vector<myTrack *>;

  //old4L_MinimumCutTracks = new std::vector<myTrack *>;
  old4L_Common_SRTracks = new std::vector<myTrack *>;
  old4L_Common_hadCRTracks = new std::vector<myTrack *>;
  old4L_Common_fakeCRTracks = new std::vector<myTrack *>;
  old4L_Common_singleEleCRTracks = new std::vector<myTrack *>;
  old4L_Common_singleMuCRTracks = new std::vector<myTrack *>;

  my3LTree = new NtupleWriter("my3LTree");
  my4LTree = new NtupleWriter("my4LTree");

  myTree_Old4LTracks = new NtupleWriter("myTree_Old4LTracks");

  //myTree_Old4L_MinimumCutTracks = new NtupleWriter("myTree_Old4L_MinimumCutTracks");
  myTree_Old4L_Common_SR = new NtupleWriter("myTree_Old4L_Common_SR");
  myTree_Old4L_Common_hadCR       = new NtupleWriter("myTree_Old4L_Common_hadCR");
  myTree_Old4L_Common_fakeCR      = new NtupleWriter("myTree_Old4L_Common_fakeCR");
  myTree_Old4L_Common_singleEleCR = new NtupleWriter("myTree_Old4L_Common_singleEleCR");
  myTree_Old4L_Common_singleMuCR  = new NtupleWriter("myTree_Old4L_Common_singleMuCR");

  if(DRAWEffPath!=""){
    tfDRAWEff = new TFile(DRAWEffPath.c_str(), "READ");
    gROOT->cd();
    gDRAWEff = (TGraphAsymmErrors *)tfDRAWEff->Get("gEff_DRAW");
  }
  if(myOption==3){
    tfMyInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/Input.root", "READ");
    gROOT->cd();
    for(int iChain=0;iChain<nKinematicsChain;iChain++){
      for(int iTrig=0;iTrig<nMETTrig;iTrig++){
	hTrigEff[iChain][iTrig] = (TH2D *)tfMyInput->Get(Form("METTrigEff/hTrigMETJetEff_Chain%d_%d", iChain, iTrig));
      }// for iTrig
    }// for iChain
  }

  return 0;
}

int MyManager::Finalize(void){
  myFile->cd();
  my3LTree->Write();
  my4LTree->Write();
  //myTree->Write();
  //myTree_Old4LTracks->Write();

  //myTree_Old4L_MinimumCutTracks->Write();
  myTree_Old4L_Common_SR->Write();
  myTree_Old4L_Common_hadCR->Write();
  myTree_Old4L_Common_fakeCR->Write();
  myTree_Old4L_Common_singleEleCR->Write();
  myTree_Old4L_Common_singleMuCR->Write();

  myHist_Old4L->Write(myFile);
  myFile->Close();
  std::cout << "MyManager :: jobs succeeded to run" << std::endl;

  return 0;
}

int MyManager::Run(void){  
  Initialize();
  
  int nFile = InputFileList.size();
  for(int iFile=0;iFile<nFile;iFile++){
    InputFileName = InputFileList.at(iFile);
    myCommon = new NtupleReader(InputFileName);
    myHist_Old4L->FillFileInfo(myCommon);
    
    for(int iEntry=0;iEntry<(myCommon->nEntry);iEntry++){
      myCommon->GetEntry(iEntry);
      /*
      if((myCommon->RunNumber==280673) && (myCommon->lumiBlock==132) && (myCommon->EventNumber==7336544))
	continue;
      if((myCommon->RunNumber==332303) && (myCommon->lumiBlock==157) && (myCommon->EventNumber==496094779))
	continue;
      if((myCommon->RunNumber==333380) && (myCommon->lumiBlock==80 ) && (myCommon->EventNumber==274926714))
	continue;
      if((myCommon->RunNumber==280673) && (myCommon->lumiBlock==132) && (myCommon->EventNumber==7285417))
	continue;
      */
      if(myOption==1){
	myCommon->weightXsec = 1.0;
	myCommon->weightMCweight = 1.0;
	myCommon->weightPileupReweighting = 1.0;	
      }else if(myOption==2){
	myCommon->weightXsec = 1.0;
      }

      SetCutFlowLevel();
      SetKinematicsVal();
      myHist_Old4L->FillBeforeAnyCut(myCommon);
      if(!MyUtil::IsPassed(CutFlowLevel, 1))
	continue; // only data quality cut

      /*     Duplicate Track Removal     */
      DuplicateTrackRemoval();

      /*     Truth Matching   */
      ApplyTruthMatching();

      /*     Fill Disappearing Track Candidates and Calculate Track Category   */
      FillTracks();

      /*    Fill Hist   */
      myHist_Old4L->FillHist(myCommon);
    }// for iEntry
    
    delete myCommon;
    myCommon = NULL;    
    //}// while Input File List
  }
  Finalize();
    
  return 0;
}

int MyManager::FillTracks(void){
  myDisappearingTracks->clear();
  vTrackLayer.clear();
  //old4LTracks->clear();

  //old4L_MinimumCutTracks->clear();
  old4L_Common_SRTracks->clear();
  old4L_Common_hadCRTracks->clear();
  old4L_Common_fakeCRTracks->clear();
  old4L_Common_singleEleCRTracks->clear();
  old4L_Common_singleMuCRTracks->clear();
  
  IsolatedHighestTrackID = -1.0;
  IsolatedHighestTrackPt = 0.0;

  IsolatedHighestTrackletID = -1.0;
  IsolatedHighestTrackletPt = 0.0;

  /*   Calculate Isolated Highest Track/Tracklet Pt   */
  for(unsigned int iTrack=0;iTrack<(myCommon->conventionalTracks->size());iTrack++){
    if(myCommon->conventionalTracks->at(iTrack)->PixelTracklet){
      if( ((myCommon->conventionalTracks->at(iTrack)->ptcone40overPt_1gev) < 0.04 ) &&
	  (IsolatedHighestTrackletPt < (myCommon->conventionalTracks->at(iTrack)->p4.Pt())) ){
	IsolatedHighestTrackletPt = myCommon->conventionalTracks->at(iTrack)->p4.Pt();
	IsolatedHighestTrackletID = iTrack;
      }
    }else{
      if( ((myCommon->conventionalTracks->at(iTrack)->ptcone40overPt_1gev) < 0.04 ) &&
	  (IsolatedHighestTrackPt < (myCommon->conventionalTracks->at(iTrack)->p4.Pt())) ){
	IsolatedHighestTrackPt = myCommon->conventionalTracks->at(iTrack)->p4.Pt();
	IsolatedHighestTrackID = iTrack;
      }
    }// standard track
  }// for iTrack

  /*     Standard Tracks and Pixel-tracklet    */
  for(unsigned int iTrack=0;iTrack<(myCommon->conventionalTracks->size());iTrack++){
    if(myCommon->conventionalTracks->at(iTrack)->PixelTracklet){
      if((int)iTrack == IsolatedHighestTrackletID)
	myCommon->conventionalTracks->at(iTrack)->IsPassedIsolatedLeading = true;
    }else{
      if((int)iTrack == IsolatedHighestTrackID   )
	myCommon->conventionalTracks->at(iTrack)->IsPassedIsolatedLeading = true;
    }
    FillOld4LTracks(myCommon->conventionalTracks->at(iTrack));
  }// for iTrack
  
  /*     4L Tracks     */
  for(unsigned int iTrack=0;iTrack<(myCommon->fourLayerTracks->size());iTrack++){
    if(TMath::Abs(myCommon->fourLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
      continue;
    
    int nTrackLayer  = MyUtil::CalcNumberOfTrackLayer(myCommon->fourLayerTracks->at(iTrack));
    int TrackQuality = MyUtil::CalcTrackQuality(myCommon->fourLayerTracks->at(iTrack), nTrackLayer);    
    if(myCommon->fourLayerTracks->at(iTrack)->KVUPt > IsolatedHighestTrackPt)
      myCommon->fourLayerTracks->at(iTrack)->IsPassedIsolatedLeading = true;
    if(TrackQuality==((1<<nTrackQualityFor3L)-1)){
      myDisappearingTracks->push_back(myCommon->fourLayerTracks->at(iTrack));
      vTrackLayer.push_back(nTrackLayer);
    }
  }// for iTrack
  
  /*     3L Tracks     */
  for(unsigned int iTrack=0;iTrack<(myCommon->threeLayerTracks->size());iTrack++){
    if(TMath::Abs(myCommon->threeLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
      continue;
    
    int nTrackLayer  = MyUtil::CalcNumberOfTrackLayer(myCommon->threeLayerTracks->at(iTrack));
    int TrackQuality = MyUtil::CalcTrackQuality(myCommon->threeLayerTracks->at(iTrack), nTrackLayer);    
    if(myCommon->threeLayerTracks->at(iTrack)->KVUPt > IsolatedHighestTrackPt)
      myCommon->threeLayerTracks->at(iTrack)->IsPassedIsolatedLeading = true;
    if(TrackQuality==((1<<nTrackQualityFor3L)-1)){
      myDisappearingTracks->push_back(myCommon->threeLayerTracks->at(iTrack));
      vTrackLayer.push_back(nTrackLayer);
    }
  }// for iTrack
  
  /*     Single Electron CR  no MET   */
  if( (myCommon->goodElectrons->size()==1) && (myCommon->goodMuons->size()==0) && (myCommon->goodElectrons->at(0)->IsSignal==1) && (myCommon->goodElectrons->at(0)->index_IDTrack.size()==1) ){
    myTrack *Track = myCommon->conventionalTracks->at(myCommon->goodElectrons->at(0)->index_IDTrack.at(0));
    int TrackQuality = MyUtil::CalcOld4LTrackQuality(Track);
    //int KinematicsQuality_Strong = MyUtil::CalcOldKinematicsQuality_Strong(myCommon);
    //int KinematicsQuality_EWK    = MyUtil::CalcOldKinematicsQuality_EWK(myCommon);
    
    // 011 1101 0111 1111 1100
    if((myCommon->LU_METtrigger_SUSYTools==true) && MyUtil::IsPassed(TrackQuality, 0x3DFFC) && (myCommon->goodElectrons->at(0)->p4.Pt()/1000.0 > 10.0)){
      // Common
      old4L_Common_singleEleCRTracks->push_back(Track);
      myTree_Old4L_Common_singleEleCR->FillBasicInfo(myCommon);
      myTree_Old4L_Common_singleEleCR->FillDisappearingTrack(Track);
      myTree_Old4L_Common_singleEleCR->Fill();
    }
  }// single electron CR
  
  /*     Single Muon CR no MET    */
  if( (myCommon->goodMuons->size()==1) && (myCommon->goodElectrons->size()==0) && (myCommon->goodMuons->at(0)->IsSignal==1) && (myCommon->goodMuons->at(0)->index_IDTrack.size()==1) ){
    myTrack *Track = myCommon->conventionalTracks->at(myCommon->goodMuons->at(0)->index_IDTrack.at(0));
    int TrackQuality = MyUtil::CalcOld4LTrackQuality(Track);
    //int KinematicsQuality_Strong = MyUtil::CalcOldKinematicsQuality_Strong(myCommon);
    //int KinematicsQuality_EWK    = MyUtil::CalcOldKinematicsQuality_EWK(myCommon);

    // 011 0011 0111 1111 1100
    if((myCommon->LU_METtrigger_SUSYTools==true) && MyUtil::IsPassed(TrackQuality, 0x33FFC) && (myCommon->goodMuons->at(0)->p4.Pt()/1000.0 > 10.0) ){
      // Common
      old4L_Common_singleMuCRTracks->push_back(Track);
      myTree_Old4L_Common_singleMuCR->FillBasicInfo(myCommon);
      myTree_Old4L_Common_singleMuCR->FillDisappearingTrack(Track);
      myTree_Old4L_Common_singleMuCR->Fill();
    }
  } // single muon CR

  /* -----   Calc Track Category   -----  */
  TrackCategory=0;
  for(unsigned int iTrack=0;iTrack<(myDisappearingTracks->size());iTrack++){
    TrackCategory += vTrackLayer.at(iTrack)*TMath::Power(10, (int)iTrack);
  }// for iTrack
  
  if(TrackCategory==3){
    my3LTree->FillBasicInfo(myCommon);
    if(DRAWEffPath!="")
      my3LTree->FillDRAWEff(SF_DRAWEff);
    my3LTree->FillDisappearingTrack(myDisappearingTracks->at(0));
    my3LTree->FillTrackCategory(TrackCategory);
    my3LTree->Fill();
    
  }else if(TrackCategory==4){
    my4LTree->FillBasicInfo(myCommon);
    if(DRAWEffPath!="")
      my4LTree->FillDRAWEff(SF_DRAWEff);
    my4LTree->FillDisappearingTrack(myDisappearingTracks->at(0));
    my4LTree->FillTrackCategory(TrackCategory);
    my4LTree->Fill();
  }// 3-layer and 4-layer disappearing track
    
  return 0;
}

int MyManager::FillOld4LTracks(myTrack *Track){
  //int KinematicsQuality_Strong = MyUtil::CalcOldKinematicsQuality_Strong(myCommon);
  //int KinematicsQuality_EWK    = MyUtil::CalcOldKinematicsQuality_EWK(myCommon);
  int TrackQuality = MyUtil::CalcOld4LTrackQuality(Track);
  //int nTrackLayer  = MyUtil::CalcNumberOfTrackLayer(Track);
  int nTrackLayer  = MyUtil::CalcNumberOfBarrelOnlyLayer(Track);
  myHist_Old4L->FillOld4LTracks(myCommon, Track);
  
  /*   old4LTracks   */
  /*
  if((nTrackLayer==4) && MyUtil::IsPassed(TrackQuality, 0x3FCBF)){ // 011 1111 1100 1011 1111
      old4LTracks->push_back(Track);
      myTree_Old4LTracks->FillBasicInfo(myCommon);
      myTree_Old4LTracks->FillDisappearingTrack(Track);
      myTree_Old4LTracks->Fill();
  }// old4LTracks
  */

  /*   MinimumCutTracks   no kinematics cut   */
  /*
  if((nTrackLayer==4) && MyUtil::IsPassed(TrackQuality, 0x1FE01)){ // 001 1111 1110 0000 0001
    if(TMath::Abs(Track->d0/Track->d0err) < 5.0 && Track->etclus20_topo/1000.0 < 30.0){
      // Common
      old4L_MinimumCutTracks->push_back(Track);
      myTree_Old4L_MinimumCutTracks->FillBasicInfo(myCommon);
      myTree_Old4L_MinimumCutTracks->FillDisappearingTrack(Track);
      myTree_Old4L_MinimumCutTracks->Fill();
    }
  }// old4LTracks
  */
  /*   old4L_SRTracks no MET cut  */
  if((nTrackLayer==4) && (myCommon->LU_METtrigger_SUSYTools==true || myOption==3) && (myCommon->IsPassedLeptonVeto==true) && MyUtil::IsPassed(TrackQuality, 0x3FFFF)){
    // Common
    old4L_Common_SRTracks->push_back(Track);
    myTree_Old4L_Common_SR->FillBasicInfo(myCommon);
    myTree_Old4L_Common_SR->FillDisappearingTrack(Track);
    if(myOption==3){
      // save
      int iTrig = MyUtil::GetUnprescaledMETTrigger(myCommon->randomRunNumber);
      double Eff[nKinematicsChain]={0.0};
      for(int iChain=0;iChain<nKinematicsChain;iChain++){
	double MET   = MyUtil::GetKinematicsVal(myCommon, 0);
	double JetPt = MyUtil::GetKinematicsVal(myCommon, 1);
	int iX = hTrigEff[iChain][iTrig]->GetXaxis()->FindBin(MET);
	int iY = hTrigEff[iChain][iTrig]->GetYaxis()->FindBin(JetPt);
	if(iX==0) iX=1;
	if(iY==0) iY=1;
	if(iX==(hTrigEff[iChain][iTrig]->GetNbinsX()+1)) iX=hTrigEff[iChain][iTrig]->GetNbinsX();
	if(iY==(hTrigEff[iChain][iTrig]->GetNbinsY()+1)) iY=hTrigEff[iChain][iTrig]->GetNbinsY();
	Eff[iChain] = hTrigEff[iChain][iTrig]->GetBinContent(iX, iY);
      }// for iChain
      myTree_Old4L_Common_SR->FillTrigEff(Eff);
    }
    myTree_Old4L_Common_SR->Fill();
  }// old4L_SRTracks
  
  /*   old4L_hadCRTracks no MET cut  */
  if((myCommon->LU_METtrigger_SUSYTools==true) && (myCommon->IsPassedLeptonVeto==true) && MyUtil::IsPassed(TrackQuality, 0x3FFFC) 
     && (Track->p4.Pt()/1000.0 > 10.0) && (Track->nTRTHits >= 15) && (Track->nSCTHits>=6) 
     && (Track->etcone20_topo/1000.0 > 3.0) && (Track->etclus40overPt_topo > 0.5)){

    // Common
    old4L_Common_hadCRTracks->push_back(Track);
    myTree_Old4L_Common_hadCR->FillBasicInfo(myCommon);
    myTree_Old4L_Common_hadCR->FillDisappearingTrack(Track);
    myTree_Old4L_Common_hadCR->Fill();
  }// old4L_hadCRTracks
  
  /*   old4L_fakeCRTracks no MET, d0 sig > 2.0   */
  if((nTrackLayer==4) && (myCommon->LU_METtrigger_SUSYTools==true) && (myCommon->IsPassedLeptonVeto==true) && MyUtil::IsPassed(TrackQuality, 0x3FEFF)
     && (TMath::Abs(Track->d0/Track->d0err) > 2.0)){

    // Common
    old4L_Common_fakeCRTracks->push_back(Track);
    myTree_Old4L_Common_fakeCR->FillBasicInfo(myCommon);
    myTree_Old4L_Common_fakeCR->FillDisappearingTrack(Track);
    myTree_Old4L_Common_fakeCR->Fill();
  }// old4L_fakeCRTracks
  
  return 0;
}

NtupleReader *MyManager::GetNtupleReader(std::string TreeName){
  if(TreeName=="Old4LTracks"){
    return myRead_Old4LTracks;
  }else if(TreeName=="Old4L_MinimumCutTracks"){
    return myRead_Old4L_MinimumCutTracks;
  }else if(TreeName=="Old4L_Common_SR"){
    return myRead_Old4L_Common_SR;
  }else if(TreeName=="Old4L_Common_hadCR"){
    return myRead_Old4L_Common_hadCR;
  }else if(TreeName=="Old4L_Common_fakeCR"){
    return myRead_Old4L_Common_fakeCR;
  }else if(TreeName=="Old4L_Common_singleEleCR"){
    return myRead_Old4L_Common_singleEleCR;
  }else if(TreeName=="Old4L_Common_singleMuCR"){
    return myRead_Old4L_Common_singleMuCR;
  }

  return NULL;
}

int MyManager::LoadMyFile(int iOpt){
  myFile = new TFile(InputFileList.at(0).c_str(), "READ");
  gROOT->cd();

  myHist_Old4L = new HistManager("Old4L");
  myHist_Old4L->Get(myFile, iOpt);
  
  // myRead_Old4LTracks          = new NtupleReader(myFile, "myTree_Old4LTracks");

  //myRead_Old4L_MinimumCutTracks   = new NtupleReader(myFile, "myTree_Old4L_MinimumCutTracks", iOpt);
  myRead_Old4L_Common_SR          = new NtupleReader(myFile, "myTree_Old4L_Common_SR", iOpt);
  myRead_Old4L_Common_hadCR       = new NtupleReader(myFile, "myTree_Old4L_Common_hadCR", iOpt);
  myRead_Old4L_Common_fakeCR      = new NtupleReader(myFile, "myTree_Old4L_Common_fakeCR", iOpt);
  myRead_Old4L_Common_singleEleCR = new NtupleReader(myFile, "myTree_Old4L_Common_singleEleCR", iOpt);
  myRead_Old4L_Common_singleMuCR  = new NtupleReader(myFile, "myTree_Old4L_Common_singleMuCR", iOpt);

  return 0;
}

int MyManager::FillTruthMatchedInfo(myTrack *Track, myTruth *Truth){
  Track->DecayRadius = Truth->DecayRadius;
  Track->DecayProperTime = Truth->ProperTime;

  Track->deltaPt = Truth->p4.Pt() - Track->p4.Pt();
  Track->deltaQoverP = Truth->qoverp - Track->qoverp;
  Track->deltaQoverPt = Truth->qoverpt - Track->qoverpt;
  Track->deltaEta = Truth->p4.Eta() - Track->p4.Eta();
  Track->deltaTheta = Truth->p4.Theta() - Track->p4.Theta();
  Track->deltaPhi = Truth->p4.Phi() - Track->p4.Phi();
  Track->deltaR = Truth->p4.DeltaR(Track->p4);

  Track->TruthPt = Truth->p4.Pt();
  Track->TruthEta = Truth->p4.Eta();
  Track->TruthPhi = Truth->p4.Phi();
  Track->TruthCharge = Truth->Charge;
  Track->TruthMass = Truth->Mass;

  return 0;
}

int MyManager::ApplyTruthMatching(void){
  for(unsigned int iTruth=0;iTruth<(myCommon->truthParticles->size());iTruth++){
    if(TMath::Abs(myCommon->truthParticles->at(iTruth)->PdgId)!=1000024)
      continue;

    double dRMin = 99.0;
    /* **********   Calculate minimum delta R   ********** */
    for(unsigned int iTrack=0;iTrack<(myCommon->conventionalTracks->size());iTrack++){
      double dR = myCommon->truthParticles->at(iTruth)->p4.DeltaR(myCommon->conventionalTracks->at(iTrack)->p4);
      if(dR < dRMin)	dRMin = dR;
    }// for iTrack 
    for(unsigned int iTrack=0;iTrack<(myCommon->threeLayerTracks->size());iTrack++){
      if(TMath::Abs(myCommon->threeLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
	continue;
      double dR = myCommon->truthParticles->at(iTruth)->p4.DeltaR(myCommon->threeLayerTracks->at(iTrack)->p4);
      if(dR < dRMin)	dRMin = dR;
    }// for iTrack
    for(unsigned int iTrack=0;iTrack<(myCommon->fourLayerTracks->size());iTrack++){
      if(TMath::Abs(myCommon->fourLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
	continue;
      double dR = myCommon->truthParticles->at(iTruth)->p4.DeltaR(myCommon->fourLayerTracks->at(iTrack)->p4);
      if(dR < dRMin)	dRMin = dR;
    }// for iTrack

    /* **********   Find Truth Matched Track   ********** */
    /*     Standard Tracks     */
    for(unsigned int iTrack=0;iTrack<(myCommon->conventionalTracks->size());iTrack++){
      double dR = myCommon->truthParticles->at(iTruth)->p4.DeltaR(myCommon->conventionalTracks->at(iTrack)->p4);
      if(dR < 0.05 && (dR-dRMin)<0.001)
	FillTruthMatchedInfo(myCommon->conventionalTracks->at(iTrack), myCommon->truthParticles->at(iTruth));
    }// for iTrack

    /*     3L Tracks     */
    for(unsigned int iTrack=0;iTrack<(myCommon->threeLayerTracks->size());iTrack++){
      if(TMath::Abs(myCommon->threeLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
	continue;
      double dR = myCommon->truthParticles->at(iTruth)->p4.DeltaR(myCommon->threeLayerTracks->at(iTrack)->p4);
      if(dR < 0.05 && (dR-dRMin)<0.001)
	FillTruthMatchedInfo(myCommon->threeLayerTracks->at(iTrack), myCommon->truthParticles->at(iTruth));
    }// for iTrack

    /*     4L Tracks     */
    for(unsigned int iTrack=0;iTrack<(myCommon->fourLayerTracks->size());iTrack++){
      if(TMath::Abs(myCommon->fourLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
	continue;
      double dR = myCommon->truthParticles->at(iTruth)->p4.DeltaR(myCommon->fourLayerTracks->at(iTrack)->p4);
      if(dR < 0.05 && (dR-dRMin)<0.001)
	FillTruthMatchedInfo(myCommon->fourLayerTracks->at(iTrack), myCommon->truthParticles->at(iTruth));
    }// for iTrack
  }// for iTruth

  return 0;
}

int MyManager::DuplicateTrackRemoval(void){
  for(unsigned int iTrack=0;iTrack<(myCommon->threeLayerTracks->size());iTrack++){
    if(TMath::Abs(myCommon->threeLayerTracks->at(iTrack)->dRTrack_5gev) < 0.05)
      continue;

    // duplicate track removal from 4-layer track
    for(unsigned int jTrack=0;jTrack<(myCommon->fourLayerTracks->size());jTrack++){
      if(TMath::Abs(myCommon->fourLayerTracks->at(jTrack)->dRTrack_5gev) < 0.05)
	continue;
      double Pt = myCommon->fourLayerTracks->at(jTrack)->KVUPt/1000.0;
      double dR = myCommon->threeLayerTracks->at(iTrack)->p4.DeltaR(myCommon->fourLayerTracks->at(jTrack)->p4);

      myCommon->threeLayerTracks->at(iTrack)->dRTrack_5gev = dR;
      if(myCommon->threeLayerTracks->at(iTrack)->dRTrack_10gev > dR && Pt>10.0)
	myCommon->threeLayerTracks->at(iTrack)->dRTrack_10gev = dR;
    }// for jTrack for 4-layer
    
    // duplicate track removal from 3-layer track
    if(iTrack==(myCommon->threeLayerTracks->size()-1))
      break;
    for(unsigned int jTrack=iTrack+1;jTrack<(myCommon->threeLayerTracks->size());jTrack++){
      if(TMath::Abs(myCommon->threeLayerTracks->at(jTrack)->dRTrack_5gev) < 0.05)
	continue;
      double dR = myCommon->threeLayerTracks->at(iTrack)->p4.DeltaR(myCommon->threeLayerTracks->at(jTrack)->p4);
      if(dR < 0.05){
	if((myCommon->threeLayerTracks->at(iTrack)->KVUQuality) > (myCommon->threeLayerTracks->at(jTrack)->KVUQuality)){
	  myCommon->threeLayerTracks->erase(myCommon->threeLayerTracks->begin() + jTrack);
	  jTrack--;
	}else{
	  myCommon->threeLayerTracks->erase(myCommon->threeLayerTracks->begin() + iTrack);
	  iTrack--;
	  break;
	}
      }      
    }// for jTrack for 3-layer
  }// for iTrack
  
  return 0;
}

int MyManager::SetKinematicsVal(void){
  KinematicsVal[0]  = myCommon->missingET->p4.Pt()/1000.0; // MET [GeV]
  KinematicsVal[1]  = (myCommon->goodJets->size()>0) ? myCommon->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
  KinematicsVal[2]  = (myCommon->goodJets->size()>1) ? myCommon->goodJets->at(1)->p4.Pt()/1000.0 : 0.0;
  KinematicsVal[3]  = (myCommon->goodJets->size()>2) ? myCommon->goodJets->at(2)->p4.Pt()/1000.0 : 0.0;
  KinematicsVal[4]  = (myCommon->goodJets->size()>3) ? myCommon->goodJets->at(3)->p4.Pt()/1000.0 : 0.0;
  KinematicsVal[5]  = myCommon->JetMetdPhiMin50;
  KinematicsVal[6]  = myCommon->JetMetdPhiMin20;
  KinematicsVal[7]  = myCommon->HT;
  KinematicsVal[8]  = myCommon->EffMass;
  KinematicsVal[9]  = myCommon->Aplanarity;
  KinematicsVal[10] = (myCommon->missingET->p4.Pt()/1000.0)/TMath::Sqrt(myCommon->HT);
  KinematicsVal[11] = (myCommon->missingET->p4.Pt()/1000.0)/myCommon->EffMass;
  
  SF_DRAWEff = ((DRAWEffPath!="") && (myCommon->IsData==false)) ? gDRAWEff->Eval(myCommon->missingET->p4.Pt()/1000.0) : 1.0;

  if((DRAWEffPath!="") && (myCommon->IsData==false)){
    if(KinematicsVal[0] > 1000.0){
      SF_DRAWEff = gDRAWEff->Eval(1000);
    }else{
      SF_DRAWEff = gDRAWEff->Eval(KinematicsVal[0]);
    }
  }else{
    SF_DRAWEff = 1.0;
  }

  return 0;
}

int MyManager::SetCutFlowLevel(void){
  // All Events
  CutFlowLevel=0;
  CutFlowLevelLowMET=0;

  // GRL and Event Cleaning
  if(((myCommon->GRL==true && myCommon->DetectorError==0) || (myCommon->IsData==false)) && myCommon->IsPassedBadEventVeto==true){
    CutFlowLevel |= (1<<0);
    CutFlowLevelLowMET |= (1<<0);
  }  

  // xe triggers
  if(myCommon->LU_METtrigger_SUSYTools==true){
    CutFlowLevel |= (1<<1);
    CutFlowLevelLowMET |= (1<<1);
  }

  // Lepton VETO
  if(myCommon->IsPassedLeptonVeto==true){
    CutFlowLevel |= (1<<2);
    CutFlowLevelLowMET |= (1<<2);
  }

  // MET > 250 GeV
  if(myCommon->missingET->p4.Pt()/1000.0 > 250.0){
    CutFlowLevel |= (1<<3);
  }else if(myCommon->missingET->p4.Pt()/1000.0 > 100.0 && myCommon->missingET->p4.Pt()/1000.0 < 200.0){
    CutFlowLevelLowMET |= (1<<3);
  }

  // 1st jet pT > 80 GeV
  if(myCommon->goodJets->size()>0 && myCommon->goodJets->at(0)->p4.Pt()/1000.0 > 80.0){
    CutFlowLevel |= (1<<4);
    CutFlowLevelLowMET |= (1<<4);
  }

  // 2nd jet pT > 80 GeV
  if(myCommon->goodJets->size()>1 && myCommon->goodJets->at(1)->p4.Pt()/1000.0 > 80.0){
    CutFlowLevel |= (1<<5);
    CutFlowLevelLowMET |= (1<<5);
  }


  // 3rd jet pT > 40 GeV
  if(myCommon->goodJets->size()>2 && myCommon->goodJets->at(2)->p4.Pt()/1000.0 > 40.0){
    CutFlowLevel |= (1<<6);
    CutFlowLevelLowMET |= (1<<6);
  }

  // 4th jet pT > 20 GeV
  if(myCommon->goodJets->size()>3 && myCommon->goodJets->at(3)->p4.Pt()/1000.0 > 20.0){
    CutFlowLevel |= (1<<7);
    CutFlowLevelLowMET |= (1<<7);
  }

  // dPhi (MET, jets) > 0.4
  if(myCommon->JetMetdPhiMin50 > 0.4){
    CutFlowLevel |= (1<<8);
    CutFlowLevelLowMET |= (1<<8);
  }
  
  return CutFlowLevel;
}
