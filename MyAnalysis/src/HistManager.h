#ifndef HistManager_h
#define HistManager_h

#include <iostream>
#include <fstream>
#include <vector>
#include "src/NtupleReader.h"
#include "src/NtupleWriter.h"
#include "src/Constant.h"

#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TFile.h"
#include "TClonesArray.h"
#include "TGraphAsymmErrors.h"

class HistManager{
 public:
  HistManager(const std::string myTag);
  ~HistManager();

  int Init(void);
  int Finalize(void);
  int Write(TFile *myFile);
  int Get(TFile *myFile, int iOpt=0);
  TH1D *GetHist1D(std::string);
  TH1D *GetHist1D(std::string, int);
  TH2D *GetHist2D(std::string);
  TH2D *GetHist2D(std::string, int);
  TH2D *GetHist2D(std::string, int, int);
  TH1D *GetHistChain1D(std::string, int, int);
  TH2D *GetHistChain2D(std::string, int, int);
  int FillFileInfo(NtupleReader *myReader);
  int FillBeforeAnyCut(NtupleReader *myReader);
  int FillHist(NtupleReader *myReader);
  int FillOld4LTracks(NtupleReader *myReader,myTrack *Track);
  std::string HistTag;

  TH1D *hTracks_withoutMET[nKinematicsChain][nOld4LTracksCut];
  TH1D *hTracks_N1_withoutMET[nKinematicsChain][nOld4LTracksCut];
  TH1D *hOld4LTracks_MinimumTrackCut[nOld4LTracksCut];
  TH1D *hOld4LTracks_MinimumTrackCut_Wino[nOld4LTracksCut];

  TH1D *hKinematics[nKinematicsChain][nKinematicsCut];
  TH1D *hKinematics_N1[nKinematicsChain][nKinematicsCut];

  TH1D *hnEvents;

  TH1D *hnEventsProcessedBCK;
  TH1D *hnPrimaryVertex;
  TH1D *hProdType;
  TH1D *hSumOfNumber;
  TH1D *hSumOfWeights;
  TH1D *hSumOfWeightsBCK;
  TH1D *hSumOfWeightsSquaredBCK;

  TH1D *hNvtx_Chain1;
  TH1D *hNvtx_Chain3;
  TH1D *hNvtxReweighted_Chain1;
  TH1D *hNvtxReweighted_Chain3;

  TH1D *hActualIPC_Chain1;
  TH1D *hActualIPC_Chain3;
  TH1D *hCorrectedAIPC_Chain1;
  TH1D *hCorrectedAIPC_Chain3;

  TH1D *hActualIPC_ForMuCR_Chain1;
  TH1D *hActualIPC_ForMuCR_Chain3;
  TH1D *hCorrectedAIPC_ForMuCR_Chain1;
  TH1D *hCorrectedAIPC_ForMuCR_Chain3;

  TH1D *hActualIPC_ForEleCR_Chain1;
  TH1D *hActualIPC_ForEleCR_Chain3;
  TH1D *hCorrectedAIPC_ForEleCR_Chain1;
  TH1D *hCorrectedAIPC_ForEleCR_Chain3;

  TH1D *hAIPC;
  TH1D *hCorrectedAIPC;
  TH1D *hAIPC_Old4LTracks;
  TH1D *hCorrectedAIPC_Old4LTracks;

  TH1D *hKinematics_CutFlow[nKinematicsChain];
  TH1D *hTracks_CutFlow[nKinematicsChain];
  bool fTracks_CutFlow[nKinematicsChain][nOld4LTracksCut];

  TH1D *hOld4LTracks_Ele_DeltaR;
  TH1D *hOld4LTracks_Mu_DeltaR;

  TH2D *hOld4LTracks_EleBG_BasicOS_TruthCharge;
  TH2D *hOld4LTracks_EleBG_BasicSS_TruthCharge;
  TH2D *hOld4LTracks_EleBG_DisapOS_TruthCharge;
  TH2D *hOld4LTracks_EleBG_DisapSS_TruthCharge;

  TH2D *hOld4LTracks_MuBG_BasicOS_TruthCharge;
  TH2D *hOld4LTracks_MuBG_BasicSS_TruthCharge;
  TH2D *hOld4LTracks_MuBG_DisapOS_TruthCharge;
  TH2D *hOld4LTracks_MuBG_DisapSS_TruthCharge;

  TH1D *hOld4LTracks_EleBG_Basic_ZMass;
  TH2D *hOld4LTracks_EleBG_Basic_PtEta;
  TH2D *hOld4LTracks_EleBG_Basic_PhiEta;
  TH1D *hOld4LTracks_EleBG_Disap_ZMass;
  TH2D *hOld4LTracks_EleBG_Disap_PtEta;
  TH2D *hOld4LTracks_EleBG_Disap_PhiEta;
  TH1D *hOld4LTracks_EleBG_BasicOS_ZMass;
  TH1D *hOld4LTracks_EleBG_BasicSS_ZMass;
  TH2D *hOld4LTracks_EleBG_BasicOS_PtEta;
  TH2D *hOld4LTracks_EleBG_BasicSS_PtEta;
  TH2D *hOld4LTracks_EleBG_BasicOS_InvPtEta;
  TH2D *hOld4LTracks_EleBG_BasicSS_InvPtEta;
  TH1D *hOld4LTracks_EleBG_DisapOS_ZMass;
  TH1D *hOld4LTracks_EleBG_DisapSS_ZMass;
  TH2D *hOld4LTracks_EleBG_DisapOS_PtEta;
  TH2D *hOld4LTracks_EleBG_DisapSS_PtEta;
  TH2D *hOld4LTracks_EleBG_DisapOS_InvPtEta;
  TH2D *hOld4LTracks_EleBG_DisapSS_InvPtEta;

  TH1D *hOld4LTracks_TFCalo_EleBG_ZMass_432[5*25];
  TH1D *hOld4LTracks_TFCalo_EleBG_ZMass_1[5*25];
  TH1D *hOld4LTracks_TFCalo_EleBG_ZMass_0[5*25];

  TH1D *hOld4LTracks_CaloIso_EleBG_ZMass[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_PhiEta[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_EleBG_OS_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_EleBG_SS_ZMass[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_OS_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_SS_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_OS_PhiEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_EleBG_SS_PhiEta[nCaloCategory];

  TH1D *hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[nCaloCategory][nPileupCategory];
  TH1D *hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[nCaloCategory][nPileupCategory];

  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_mT[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[nCaloCategory];

  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[nCaloCategory];

  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_mT[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[nCaloCategory];

  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[nCaloCategory];
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[nCaloCategory];
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[nCaloCategory];

  TH1D *hOld4LTracks_CaloIso_EleBG_Basic_ZMass;
  TH2D *hOld4LTracks_CaloIso_EleBG_Basic_PtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_Basic_PhiEta;
  TH1D *hOld4LTracks_CaloIso_EleBG_Disap_ZMass;
  TH2D *hOld4LTracks_CaloIso_EleBG_Disap_PtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_Disap_PhiEta;

  TH1D *hOld4LTracks_CaloIso_EleBG_BasicOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_EleBG_BasicSS_ZMass;
  TH2D *hOld4LTracks_CaloIso_EleBG_BasicOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_BasicSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_BasicOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_BasicSS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_BasicOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_BasicSS_PhiEta;
  TH1D *hOld4LTracks_CaloIso_EleBG_DisapOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_EleBG_DisapSS_ZMass;
  TH2D *hOld4LTracks_CaloIso_EleBG_DisapOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_DisapSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_DisapOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_DisapSS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_DisapOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_EleBG_DisapSS_PhiEta;

  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_Basic_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_Basic_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_Basic_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_Basic_PhiEta;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_Disap_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_Disap_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_Disap_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_Disap_PhiEta;

  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_mT;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_PhiEta;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_mT;
  TH1D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_PhiEta;

  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_Basic_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_Basic_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_Basic_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_Basic_PhiEta;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_Disap_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_Disap_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_Disap_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_Disap_PhiEta;

  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_mT;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_PhiEta;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_ZMass;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_mT;
  TH1D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_mT;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_PhiEta;

  TH1D *hOld4LTracks_EleBG_DisapOS_DeltaQoverPt;
  TH1D *hOld4LTracks_EleBG_DisapSS_DeltaQoverPt;

  TH1D *hOld4LTracks_MuBG_Basic_ZMass;
  TH2D *hOld4LTracks_MuBG_Basic_PtEta;
  TH2D *hOld4LTracks_MuBG_Basic_PhiEta;
  TH1D *hOld4LTracks_MuBG_Disap_ZMass;
  TH2D *hOld4LTracks_MuBG_Disap_PtEta;
  TH2D *hOld4LTracks_MuBG_Disap_PhiEta;

  TH1D *hOld4LTracks_MuBG_BasicOS_ZMass;
  TH1D *hOld4LTracks_MuBG_BasicSS_ZMass;
  TH2D *hOld4LTracks_MuBG_BasicOS_PtEta;
  TH2D *hOld4LTracks_MuBG_BasicSS_PtEta;
  TH2D *hOld4LTracks_MuBG_BasicOS_InvPtEta;
  TH2D *hOld4LTracks_MuBG_BasicSS_InvPtEta;
  TH1D *hOld4LTracks_MuBG_DisapOS_ZMass;
  TH1D *hOld4LTracks_MuBG_DisapSS_ZMass;
  TH2D *hOld4LTracks_MuBG_DisapOS_PtEta;
  TH2D *hOld4LTracks_MuBG_DisapSS_PtEta;
  TH2D *hOld4LTracks_MuBG_DisapOS_InvPtEta;
  TH2D *hOld4LTracks_MuBG_DisapSS_InvPtEta;

  TH1D *hOld4LTracks_CaloIso_MuBG_Basic_ZMass;
  TH2D *hOld4LTracks_CaloIso_MuBG_Basic_PtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_Basic_PhiEta;
  TH1D *hOld4LTracks_CaloIso_MuBG_Disap_ZMass;
  TH2D *hOld4LTracks_CaloIso_MuBG_Disap_PtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_Disap_PhiEta;

  TH1D *hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass;
  TH2D *hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta;
  TH1D *hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass;
  TH2D *hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta;
  TH2D *hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta;

  TH1D *hOld4LTracks_MuBG_DisapOS_DeltaQoverPt;
  TH1D *hOld4LTracks_MuBG_DisapSS_DeltaQoverPt;

  TH1D *hOld4LTracks_MSBG_Basic_ZMass;
  TH2D *hOld4LTracks_MSBG_Basic_PhiEta;
  TH1D *hOld4LTracks_MSBG_Disap_ZMass;
  TH2D *hOld4LTracks_MSBG_Disap_PhiEta;

  TH1D *hOld4LTracks_MSBG_BasicOS_ZMass;
  TH1D *hOld4LTracks_MSBG_BasicSS_ZMass;
  TH2D *hOld4LTracks_MSBG_BasicOS_PhiEta;
  TH2D *hOld4LTracks_MSBG_BasicSS_PhiEta;
  TH1D *hOld4LTracks_MSBG_DisapOS_ZMass;
  TH1D *hOld4LTracks_MSBG_DisapSS_ZMass;
  TH2D *hOld4LTracks_MSBG_DisapOS_PhiEta;
  TH2D *hOld4LTracks_MSBG_DisapSS_PhiEta;

  TH1D *hOld4LTracks_CaloIso_MSBG_Basic_ZMass;
  TH2D *hOld4LTracks_CaloIso_MSBG_Basic_PhiEta;
  TH1D *hOld4LTracks_CaloIso_MSBG_Disap_ZMass;
  TH2D *hOld4LTracks_CaloIso_MSBG_Disap_PhiEta;

  TH1D *hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass;
  TH2D *hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta;
  TH1D *hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass;
  TH1D *hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass;
  TH2D *hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta;
  TH2D *hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta;
  TH2D *hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta;

  TH2D *hTrigMETEff_1stJetMET[nKinematicsChain][nMETTrig];
  TH2D *hTrigMETEff_1stJetMET_Passed[nKinematicsChain][nMETTrig];
  TH1D *hTrigMETEff_mT[nKinematicsChain][nMETTrig];

  TH1D *hHoge;

  const double Z_Window=10.0;

};

#endif
