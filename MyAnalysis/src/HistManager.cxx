#include "src/PhysicsObjectProxyBase.h"
#include "src/HistManager.h"
//#include "src/Utility.h"
#include "TROOT.h"
#include "TBasket.h"

namespace MyUtil{
  bool IsPassed(int, int);
  int GetPileupCategory(NtupleReader *myReader);
  int GetCaloCategory(myTrack *Track);
  double GetKinematicsVal(NtupleReader *, int );
  int CalcKinematicsQuality(NtupleReader*, int []);
  int CalcOldKinematicsQuality_Strong(NtupleReader *);
  double GetOldKinematicsVal_Strong(NtupleReader *myReader, int iVal);
  int CalcOldKinematicsQuality_EWK(NtupleReader *);
  double GetOldKinematicsVal_EWK(NtupleReader *myReader, int iVal);
  int CalcOld4LTrackQuality(myTrack *);
  double GetOld4LTrackVal(myTrack *Track, int iVal);
  std::vector<std::string> GetUnprescaledSingleElectronTrigger(unsigned int runNumber);
  std::vector<std::string> GetUnprescaledSingleMuonTrigger    (unsigned int runNumber);
  int GetUnprescaledMETTrigger    (unsigned int runNumber);
  int CalcNumberOfBarrelOnlyLayer(myTrack *);

  int CalcNumberOfTrackLayer(myTrack *);
  double GetBasicTrackVal(myTrack *, int);
  int CalcTrackQuality(myTrack *, int);
}

HistManager::HistManager(const std::string myTag)
{
  HistTag = myTag;
}

HistManager::~HistManager()
{
  delete hnEvents; hnEvents=NULL;
  delete hnEventsProcessedBCK; hnEventsProcessedBCK=NULL;
  delete hnPrimaryVertex; hnPrimaryVertex=NULL;
  delete hProdType; hProdType=NULL;
  delete hSumOfNumber; hSumOfNumber=NULL;
  delete hSumOfWeights; hSumOfWeights=NULL;
  delete hSumOfWeightsBCK; hSumOfWeightsBCK=NULL;
  delete hSumOfWeightsSquaredBCK; hSumOfWeightsSquaredBCK=NULL;
  
  delete hNvtx_Chain1; hNvtx_Chain1=NULL;
  delete hNvtx_Chain3; hNvtx_Chain3=NULL;
  delete hNvtxReweighted_Chain1; hNvtxReweighted_Chain1=NULL;
  delete hNvtxReweighted_Chain3; hNvtxReweighted_Chain3=NULL;

  delete hActualIPC_Chain1; hActualIPC_Chain1=NULL;
  delete hActualIPC_Chain3; hActualIPC_Chain3=NULL;
  delete hCorrectedAIPC_Chain1; hCorrectedAIPC_Chain1=NULL;
  delete hCorrectedAIPC_Chain3; hCorrectedAIPC_Chain3=NULL;

  delete hActualIPC_ForMuCR_Chain1; hActualIPC_ForMuCR_Chain1=NULL;
  delete hActualIPC_ForMuCR_Chain3; hActualIPC_ForMuCR_Chain3=NULL;
  delete hCorrectedAIPC_ForMuCR_Chain1; hCorrectedAIPC_ForMuCR_Chain1=NULL;
  delete hCorrectedAIPC_ForMuCR_Chain3; hCorrectedAIPC_ForMuCR_Chain3=NULL;

  delete hActualIPC_ForEleCR_Chain1; hActualIPC_ForEleCR_Chain1=NULL;
  delete hActualIPC_ForEleCR_Chain3; hActualIPC_ForEleCR_Chain3=NULL;
  delete hCorrectedAIPC_ForEleCR_Chain1; hCorrectedAIPC_ForEleCR_Chain1=NULL;
  delete hCorrectedAIPC_ForEleCR_Chain3; hCorrectedAIPC_ForEleCR_Chain3=NULL;

  delete hAIPC; hAIPC=NULL;
  delete hCorrectedAIPC; hCorrectedAIPC=NULL;
  delete hAIPC_Old4LTracks; hAIPC_Old4LTracks=NULL;
  delete hCorrectedAIPC_Old4LTracks; hCorrectedAIPC_Old4LTracks=NULL;

  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    delete hKinematics_CutFlow[iChain]; hKinematics_CutFlow[iChain]=NULL;
    delete hTracks_CutFlow[iChain]; hTracks_CutFlow[iChain]=NULL;

    for(int iCut=0;iCut<nKinematicsCut;iCut++){
      delete hKinematics[iChain][iCut];    hKinematics[iChain][iCut]=NULL;
      delete hKinematics_N1[iChain][iCut]; hKinematics_N1[iChain][iCut]=NULL;
    }// for iCut
    for(int iCut=0;iCut<nOld4LTracksCut;iCut++){
      delete hTracks_withoutMET[iChain][iCut];    hTracks_withoutMET[iChain][iCut]=NULL;
      delete hTracks_N1_withoutMET[iChain][iCut]; hTracks_N1_withoutMET[iChain][iCut]=NULL;
    }// for iCut
  }// i

  delete hOld4LTracks_EleBG_BasicOS_TruthCharge; hOld4LTracks_EleBG_BasicOS_TruthCharge=NULL;
  delete hOld4LTracks_EleBG_BasicSS_TruthCharge; hOld4LTracks_EleBG_BasicSS_TruthCharge=NULL;
  delete hOld4LTracks_EleBG_DisapOS_TruthCharge; hOld4LTracks_EleBG_DisapOS_TruthCharge=NULL;
  delete hOld4LTracks_EleBG_DisapSS_TruthCharge; hOld4LTracks_EleBG_DisapSS_TruthCharge=NULL;
  
  delete hOld4LTracks_MuBG_BasicOS_TruthCharge; hOld4LTracks_MuBG_BasicOS_TruthCharge=NULL;
  delete hOld4LTracks_MuBG_BasicSS_TruthCharge; hOld4LTracks_MuBG_BasicSS_TruthCharge=NULL;
  delete hOld4LTracks_MuBG_DisapOS_TruthCharge; hOld4LTracks_MuBG_DisapOS_TruthCharge=NULL;
  delete hOld4LTracks_MuBG_DisapSS_TruthCharge; hOld4LTracks_MuBG_DisapSS_TruthCharge=NULL;
  
  delete hOld4LTracks_EleBG_Basic_ZMass; hOld4LTracks_EleBG_Basic_ZMass=NULL;
  delete hOld4LTracks_EleBG_Disap_ZMass; hOld4LTracks_EleBG_Disap_ZMass=NULL;
  delete hOld4LTracks_EleBG_Basic_PtEta; hOld4LTracks_EleBG_Basic_PtEta=NULL;
  delete hOld4LTracks_EleBG_Disap_PtEta; hOld4LTracks_EleBG_Disap_PtEta=NULL;
  delete hOld4LTracks_EleBG_Basic_PhiEta; hOld4LTracks_EleBG_Basic_PhiEta=NULL;
  delete hOld4LTracks_EleBG_Disap_PhiEta; hOld4LTracks_EleBG_Disap_PhiEta=NULL;

  delete hOld4LTracks_EleBG_BasicOS_ZMass; hOld4LTracks_EleBG_BasicOS_ZMass=NULL;
  delete hOld4LTracks_EleBG_BasicSS_ZMass; hOld4LTracks_EleBG_BasicSS_ZMass=NULL;
  delete hOld4LTracks_EleBG_DisapOS_ZMass; hOld4LTracks_EleBG_DisapOS_ZMass=NULL;
  delete hOld4LTracks_EleBG_DisapSS_ZMass; hOld4LTracks_EleBG_DisapSS_ZMass=NULL;
  
  delete hOld4LTracks_EleBG_BasicOS_PtEta; hOld4LTracks_EleBG_BasicOS_PtEta=NULL;
  delete hOld4LTracks_EleBG_BasicSS_PtEta; hOld4LTracks_EleBG_BasicSS_PtEta=NULL;
  delete hOld4LTracks_EleBG_DisapOS_PtEta; hOld4LTracks_EleBG_DisapOS_PtEta=NULL;
  delete hOld4LTracks_EleBG_DisapSS_PtEta; hOld4LTracks_EleBG_DisapSS_PtEta=NULL;

  delete hOld4LTracks_EleBG_BasicOS_InvPtEta; hOld4LTracks_EleBG_BasicOS_InvPtEta=NULL;
  delete hOld4LTracks_EleBG_BasicSS_InvPtEta; hOld4LTracks_EleBG_BasicSS_InvPtEta=NULL;
  delete hOld4LTracks_EleBG_DisapOS_InvPtEta; hOld4LTracks_EleBG_DisapOS_InvPtEta=NULL;
  delete hOld4LTracks_EleBG_DisapSS_InvPtEta; hOld4LTracks_EleBG_DisapSS_InvPtEta=NULL;
  
  for(int i=0;i<5*25;i++){
    delete hOld4LTracks_TFCalo_EleBG_ZMass_432[i];
    delete hOld4LTracks_TFCalo_EleBG_ZMass_1[i];
    delete hOld4LTracks_TFCalo_EleBG_ZMass_0[i];
    hOld4LTracks_TFCalo_EleBG_ZMass_432[i]=NULL;
    hOld4LTracks_TFCalo_EleBG_ZMass_1[i]=NULL;
    hOld4LTracks_TFCalo_EleBG_ZMass_0[i]=NULL;
  }

  for(int i=0;i<nCaloCategory;i++){
    delete hOld4LTracks_CaloIso_EleBG_ZMass[i];  hOld4LTracks_CaloIso_EleBG_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_PtEta[i];  hOld4LTracks_CaloIso_EleBG_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_PhiEta[i];  hOld4LTracks_CaloIso_EleBG_PhiEta[i]=NULL;

    delete hOld4LTracks_CaloIso_EleBG_OS_ZMass[i];  hOld4LTracks_CaloIso_EleBG_OS_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_SS_ZMass[i];  hOld4LTracks_CaloIso_EleBG_SS_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_OS_PtEta[i];  hOld4LTracks_CaloIso_EleBG_OS_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_SS_PtEta[i];  hOld4LTracks_CaloIso_EleBG_SS_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[i];  hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[i];  hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_OS_PhiEta[i]; hOld4LTracks_CaloIso_EleBG_OS_PhiEta[i]=NULL;
    delete hOld4LTracks_CaloIso_EleBG_SS_PhiEta[i]; hOld4LTracks_CaloIso_EleBG_SS_PhiEta[i]=NULL;

    for(int j=0;j<nPileupCategory;j++){
      delete hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[i][j]; hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[i][j]=NULL;
      delete hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[i][j]; hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[i][j]=NULL;
    }

    delete hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[i];   hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_mT[i];      hOld4LTracks_CaloIso_TauBG_EleTag_mT[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[i];   hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[i];  hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[i]=NULL;

    delete hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[i];  hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[i];  hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[i];     hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[i];     hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[i];  hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[i];  hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[i];  hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[i];  hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[i]; hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[i]; hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[i]=NULL;

    delete hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[i];   hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_mT[i];      hOld4LTracks_CaloIso_TauBG_MuTag_mT[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[i];   hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[i];  hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[i]=NULL;

    delete hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[i];  hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[i];  hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[i];     hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[i];     hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[i];  hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[i];  hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[i];  hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[i];  hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[i]; hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[i]=NULL;
    delete hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[i]; hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[i]=NULL;
  }// for iCaloCategory
  
  delete hOld4LTracks_EleBG_DisapSS_DeltaQoverPt;
  delete hOld4LTracks_EleBG_DisapOS_DeltaQoverPt;

  delete hOld4LTracks_MuBG_Basic_ZMass;
  delete hOld4LTracks_MuBG_Basic_PtEta;
  delete hOld4LTracks_MuBG_Basic_PhiEta;
  delete hOld4LTracks_MuBG_Disap_ZMass;
  delete hOld4LTracks_MuBG_Disap_PtEta;
  delete hOld4LTracks_MuBG_Disap_PhiEta;

  delete hOld4LTracks_MuBG_BasicOS_ZMass;
  delete hOld4LTracks_MuBG_BasicSS_ZMass;
  delete hOld4LTracks_MuBG_DisapOS_ZMass;
  delete hOld4LTracks_MuBG_DisapSS_ZMass;
  
  delete hOld4LTracks_MuBG_BasicOS_PtEta;
  delete hOld4LTracks_MuBG_BasicSS_PtEta;
  delete hOld4LTracks_MuBG_DisapOS_PtEta;
  delete hOld4LTracks_MuBG_DisapSS_PtEta;

  delete hOld4LTracks_MuBG_BasicOS_InvPtEta;
  delete hOld4LTracks_MuBG_BasicSS_InvPtEta;
  delete hOld4LTracks_MuBG_DisapOS_InvPtEta;
  delete hOld4LTracks_MuBG_DisapSS_InvPtEta;
  
  delete hOld4LTracks_CaloIso_MuBG_Basic_ZMass;
  delete hOld4LTracks_CaloIso_MuBG_Basic_PtEta;
  delete hOld4LTracks_CaloIso_MuBG_Basic_PhiEta;
  delete hOld4LTracks_CaloIso_MuBG_Disap_ZMass;
  delete hOld4LTracks_CaloIso_MuBG_Disap_PtEta;
  delete hOld4LTracks_CaloIso_MuBG_Disap_PhiEta;

  delete hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass;
  delete hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass;
  delete hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass;
  delete hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass;

  delete hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta;
  delete hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta;
  delete hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta;
  delete hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta;

  delete hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta;
  delete hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta;
  delete hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta;
  delete hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta;
  
  delete hOld4LTracks_MuBG_DisapSS_DeltaQoverPt;
  delete hOld4LTracks_MuBG_DisapOS_DeltaQoverPt;

  delete hOld4LTracks_MSBG_Basic_ZMass;
  delete hOld4LTracks_MSBG_Disap_ZMass;
  delete hOld4LTracks_MSBG_Basic_PhiEta;
  delete hOld4LTracks_MSBG_Disap_PhiEta;

  delete hOld4LTracks_MSBG_BasicOS_ZMass;
  delete hOld4LTracks_MSBG_BasicSS_ZMass;
  delete hOld4LTracks_MSBG_DisapOS_ZMass;
  delete hOld4LTracks_MSBG_DisapSS_ZMass;
  
  delete hOld4LTracks_MSBG_BasicOS_PhiEta;
  delete hOld4LTracks_MSBG_BasicSS_PhiEta;
  delete hOld4LTracks_MSBG_DisapOS_PhiEta;
  delete hOld4LTracks_MSBG_DisapSS_PhiEta;
  
  delete hOld4LTracks_CaloIso_MSBG_Basic_ZMass;
  delete hOld4LTracks_CaloIso_MSBG_Disap_ZMass;
  delete hOld4LTracks_CaloIso_MSBG_Basic_PhiEta;
  delete hOld4LTracks_CaloIso_MSBG_Disap_PhiEta;

  delete hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass;
  delete hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass;
  delete hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass;
  delete hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass;

  delete hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta;
  delete hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta;
  delete hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta;
  delete hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta;

  delete hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta;
  delete hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta;
  delete hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta;
  delete hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta;

  // mask
  for(int i=0;i<nMETTrig;i++){
    for(int iChain=0;iChain<nKinematicsChain;iChain++){
      delete hTrigMETEff_1stJetMET[iChain][i];
      delete hTrigMETEff_1stJetMET_Passed[iChain][i];
      delete hTrigMETEff_mT[iChain][i];
    }
  }

  for(int i=0;i<nOld4LTracksCut;i++)
    delete hOld4LTracks_MinimumTrackCut[i];
  for(int i=0;i<nOld4LTracksCut;i++)
    delete hOld4LTracks_MinimumTrackCut_Wino[i];
  // mask
}

int HistManager::Init(void){  
  hnEvents    = new TH1D("hnEvents"   , "",   1,    0,   2);
  hnEventsProcessedBCK    = new TH1D("hnEventsProcessedBCK"   , "",   1,    0,   2);
  hnPrimaryVertex         = new TH1D("hnPrimaryVertex"        , "", 100,    0, 100);
  hProdType               = new TH1D("hProdType"              , "",   8, -2.5, 5.5);
  hSumOfNumber            = new TH1D("hSumOfNumber"           , "",   1,    0,   2);
  hSumOfWeights           = new TH1D("hSumOfWeights"          , "",   1,    0,   2);
  hSumOfWeightsBCK        = new TH1D("hSumOfWeightsBCK"       , "",   1,    0,   2);
  hSumOfWeightsSquaredBCK = new TH1D("hSumOfWeightsSquaredBCK", "",   1,    0,   2);

  hNvtx_Chain1 = new TH1D("hNvtx_Chain1", "", 100, 0, 100);
  hNvtx_Chain3 = new TH1D("hNvtx_Chain3", "", 100, 0, 100);
  hNvtxReweighted_Chain1 = new TH1D("hNvtxReweighted_Chain1", "", 100, 0, 100);
  hNvtxReweighted_Chain3 = new TH1D("hNvtxReweighted_Chain3", "", 100, 0, 100);

  hActualIPC_Chain1 = new TH1D("hActualIPC_Chain1", "", 100, 0, 100);
  hActualIPC_Chain3 = new TH1D("hActualIPC_Chain3", "", 100, 0, 100);
  hCorrectedAIPC_Chain1 = new TH1D("hCorrectedAIPC_Chain1", "", 100, 0, 100);
  hCorrectedAIPC_Chain3 = new TH1D("hCorrectedAIPC_Chain3", "", 100, 0, 100);

  hActualIPC_ForMuCR_Chain1 = new TH1D("hActualIPC_ForMuCR_Chain1", "", 100, 0, 100);
  hActualIPC_ForMuCR_Chain3 = new TH1D("hActualIPC_ForMuCR_Chain3", "", 100, 0, 100);
  hCorrectedAIPC_ForMuCR_Chain1 = new TH1D("hCorrectedAIPC_ForMuCR_Chain1", "", 100, 0, 100);
  hCorrectedAIPC_ForMuCR_Chain3 = new TH1D("hCorrectedAIPC_ForMuCR_Chain3", "", 100, 0, 100);

  hActualIPC_ForEleCR_Chain1 = new TH1D("hActualIPC_ForEleCR_Chain1", "", 100, 0, 100);
  hActualIPC_ForEleCR_Chain3 = new TH1D("hActualIPC_ForEleCR_Chain3", "", 100, 0, 100);
  hCorrectedAIPC_ForEleCR_Chain1 = new TH1D("hCorrectedAIPC_ForEleCR_Chain1", "", 100, 0, 100);
  hCorrectedAIPC_ForEleCR_Chain3 = new TH1D("hCorrectedAIPC_ForEleCR_Chain3", "", 100, 0, 100);

  hAIPC = new TH1D("hAIPC", "", 100, 0, 100);
  hCorrectedAIPC = new TH1D("hCorrectedAIPC", "", 100, 0, 100);
  hAIPC_Old4LTracks = new TH1D("hAIPC_Old4LTracks", "", 100, 0, 100);
  hCorrectedAIPC_Old4LTracks = new TH1D("hCorrectedAIPC_Old4LTracks", "", 100, 0, 100);
  
  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    hKinematics_CutFlow[iChain] = new TH1D(Form("hKinematics_CutFlow_Chain%d", iChain), ";;Events", nKinematicsCut+nLabelCommon, 0, nKinematicsCut+nLabelCommon);
    hTracks_CutFlow[iChain]     = new TH1D(Form("hTracks_CutFlow_Chain%d"    , iChain), ";;Events",  nOld4LTracksCut, 0,  nOld4LTracksCut);
    
    for(int iCut=0;iCut<nKinematicsCut;iCut++){
      hKinematics[iChain][iCut]    = new TH1D(Form("hKinematics_Chain%d_%d", iChain, iCut), Form(";%s;Events", KinematicsCutLabel[iCut].c_str()),
					      KinematicsCutBin[iCut], KinematicsCutMin[iCut], KinematicsCutMax[iCut]);
      hKinematics_N1[iChain][iCut] = new TH1D(Form("hKinematics_N1_Chain%d_%d", iChain, iCut), Form(";%s;Events", KinematicsCutLabel[iCut].c_str()),
					      KinematicsCutBin[iCut], KinematicsCutMin[iCut], KinematicsCutMax[iCut]);
    }// for iCut
    for(int iCut=0;iCut<nOld4LTracksCut;iCut++){
      hTracks_withoutMET[iChain][iCut]     = new TH1D(Form("hTracks_withoutMET_Chain%d_%d", iChain,  iCut), 
						      Form(";%s;Tracks", TitleOld4LTracksCut[iCut].c_str()),
						      BinOld4LTracksCut[iCut], MinOld4LTracksCut[iCut], MaxOld4LTracksCut[iCut]);
      hTracks_N1_withoutMET[iChain][iCut]  = new TH1D(Form("hOld4LTracks_N1_withoutMET_Chain%d_%d", iChain, iCut), 
						      Form(";%s;Tracks", TitleOld4LTracksCut[iCut].c_str()),
						      BinOld4LTracksCut[iCut], MinOld4LTracksCut[iCut], MaxOld4LTracksCut[iCut]);
    }// for iCut
  }// iChain
  

  for(int i=0;i<nOld4LTracksCut;i++){
    hOld4LTracks_MinimumTrackCut[i] = new TH1D(Form("hOld4LTracks_MinimumTrackCut_%d", i), 
					       Form(";%s;Tracks", TitleOld4LTracksCut[i].c_str()),
					       BinOld4LTracksCut[i], MinOld4LTracksCut[i], MaxOld4LTracksCut[i]);
    hOld4LTracks_MinimumTrackCut_Wino[i] = new TH1D(Form("hOld4LTracks_MinimumTrackCut_Wino_%d", i), 
						    Form(";%s;Tracks", TitleOld4LTracksCut[i].c_str()),
						    BinOld4LTracksCut[i], MinOld4LTracksCut[i], MaxOld4LTracksCut[i]);
  }// for i

  hOld4LTracks_EleBG_BasicOS_TruthCharge = new TH2D("hOld4LTracks_EleBG_BasicOS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);
  hOld4LTracks_EleBG_BasicSS_TruthCharge = new TH2D("hOld4LTracks_EleBG_BasicSS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);
  hOld4LTracks_EleBG_DisapOS_TruthCharge = new TH2D("hOld4LTracks_EleBG_DisapOS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);
  hOld4LTracks_EleBG_DisapSS_TruthCharge = new TH2D("hOld4LTracks_EleBG_DisapSS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);

  hOld4LTracks_MuBG_BasicOS_TruthCharge = new TH2D("hOld4LTracks_MuBG_BasicOS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);
  hOld4LTracks_MuBG_BasicSS_TruthCharge = new TH2D("hOld4LTracks_MuBG_BasicSS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);
  hOld4LTracks_MuBG_DisapOS_TruthCharge = new TH2D("hOld4LTracks_MuBG_DisapOS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);
  hOld4LTracks_MuBG_DisapSS_TruthCharge = new TH2D("hOld4LTracks_MuBG_DisapSS_TruthCharge", ";Tag;Probe;", 3, -1.5, 1.5, 3, -1.5, 1.5);

  hOld4LTracks_EleBG_Basic_ZMass = new TH1D("hOld4LTracks_EleBG_Basic_ZMass", ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_EleBG_Disap_ZMass = new TH1D("hOld4LTracks_EleBG_Disap_ZMass", ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);

  hOld4LTracks_EleBG_BasicOS_ZMass = new TH1D("hOld4LTracks_EleBG_BasicOS_ZMass", ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_EleBG_BasicSS_ZMass = new TH1D("hOld4LTracks_EleBG_BasicSS_ZMass", ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_EleBG_DisapOS_ZMass = new TH1D("hOld4LTracks_EleBG_DisapOS_ZMass", ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_EleBG_DisapSS_ZMass = new TH1D("hOld4LTracks_EleBG_DisapSS_ZMass", ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);

  hOld4LTracks_EleBG_Basic_PtEta = new TH2D("hOld4LTracks_EleBG_Basic_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_Disap_PtEta = new TH2D("hOld4LTracks_EleBG_Disap_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_Basic_PhiEta = new TH2D("hOld4LTracks_EleBG_Basic_PhiEta", ";#phi;#eta", 
					     50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_EleBG_Disap_PhiEta = new TH2D("hOld4LTracks_EleBG_Disap_PhiEta", ";#phi;#eta", 
					     50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_EleBG_BasicOS_PtEta = new TH2D("hOld4LTracks_EleBG_BasicOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_BasicSS_PtEta = new TH2D("hOld4LTracks_EleBG_BasicSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_DisapOS_PtEta = new TH2D("hOld4LTracks_EleBG_DisapOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_DisapSS_PtEta = new TH2D("hOld4LTracks_EleBG_DisapSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  hOld4LTracks_EleBG_BasicOS_InvPtEta = new TH2D("hOld4LTracks_EleBG_BasicOS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						 50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_BasicSS_InvPtEta = new TH2D("hOld4LTracks_EleBG_BasicSS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						 50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_DisapOS_InvPtEta = new TH2D("hOld4LTracks_EleBG_DisapOS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						 50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_EleBG_DisapSS_InvPtEta = new TH2D("hOld4LTracks_EleBG_DisapSS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						 50, 0.0, 0.1, 25, -2.5, 2.5);

  for(int i=0;i<5*25;i++){
    hOld4LTracks_TFCalo_EleBG_ZMass_432[i] = new TH1D(Form("hOld4LTracks_TFCalo_EleBG_ZMass_432_%d", i), ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
    hOld4LTracks_TFCalo_EleBG_ZMass_1[i]   = new TH1D(Form("hOld4LTracks_TFCalo_EleBG_ZMass_1_%d", i), ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
    hOld4LTracks_TFCalo_EleBG_ZMass_0[i]   = new TH1D(Form("hOld4LTracks_TFCalo_EleBG_ZMass_0_%d", i), ";M_{ee} [GeV];Events;", 150, 50.0, 200.0);
  }// for i

  for(int i=0;i<nCaloCategory;i++){
    hOld4LTracks_CaloIso_EleBG_ZMass[i] = new TH1D(Form("hOld4LTracks_CaloIso_EleBG_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
						   150, 50.0, 200.0);
    hOld4LTracks_CaloIso_EleBG_PtEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_PtEta_%d", i), ";p_{T} [GeV];#eta", 
						   nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_EleBG_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_PhiEta_%d", i), ";#phi;#eta", 
						    50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

    hOld4LTracks_CaloIso_EleBG_OS_ZMass[i] = new TH1D(Form("hOld4LTracks_CaloIso_EleBG_OS_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
						      150, 50.0, 200.0);
    hOld4LTracks_CaloIso_EleBG_SS_ZMass[i] = new TH1D(Form("hOld4LTracks_CaloIso_EleBG_SS_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
						      150, 50.0, 200.0);
    hOld4LTracks_CaloIso_EleBG_OS_PtEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_OS_PtEta_%d", i), ";p_{T} [GeV];#eta", 
						      nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_EleBG_SS_PtEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_SS_PtEta_%d", i), ";p_{T} [GeV];#eta", 
						      nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_OS_InvPtEta_%d", i), ";1/p_{T} [GeV^{-1}];#eta", 
							 50, 0.0, 0.1, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_SS_InvPtEta_%d", i), ";1/p_{T} [GeV^{-1}];#eta", 
							 50, 0.0, 0.1, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_EleBG_OS_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_OS_PhiEta_%d", i), ";#phi;#eta", 
						       50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_EleBG_SS_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_EleBG_SS_PhiEta_%d", i), ";#phi;#eta", 
						       50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
    for(int j=0;j<nPileupCategory;j++){
      hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[i][j] = new TH1D(Form("hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt_%d_%d", i, j), ";p_{T} [GeV];", 
							       nLogPtForLepton, XBinsLogPtForLepton);
      hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[i][j] = new TH1D(Form("hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt_%d_%d", i, j), ";p_{T} [GeV];", 
							       nLogPtForLepton, XBinsLogPtForLepton);
    }

    hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[i]  = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
							   200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_EleTag_mT[i]     = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_mT_%d", i), ";M_{ee} [GeV];Events;", 
							   200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_PtEta_%d", i), ";p_{T} [GeV];#eta", 
							   nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta_%d", i), ";#phi;#eta", 
							   50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[i]  = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
							   200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_MuTag_mT[i]     = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_mT_%d", i), ";M_{ee} [GeV];Events;", 
							   200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_PtEta_%d", i), ";p_{T} [GeV];#eta", 
							   nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta_%d", i), ";#phi;#eta", 
							   50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
    
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[i]  = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
							     200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[i]  = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
							      200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[i]     = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT_%d", i), ";M_{ee} [GeV];Events;", 
							      200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[i]     = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT_%d", i), ";M_{ee} [GeV];Events;", 
							      200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta_%d", i), ";p_{T} [GeV];#eta", 
							      nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta_%d", i), ";p_{T} [GeV];#eta", 
							      nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta_%d", i), ";1/p_{T} [GeV^{-1}];#eta", 
								 50, 0.0, 0.1, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta_%d", i), ";1/p_{T} [GeV^{-1}];#eta", 
								 50, 0.0, 0.1, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta_%d", i), ";#phi;#eta", 
							      50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta_%d", i), ";#phi;#eta", 
							      50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

    hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[i]  = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
							     200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[i]  = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass_%d", i), ";M_{ee} [GeV];Events;", 
							      200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[i]     = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT_%d", i), ";M_{ee} [GeV];Events;", 
							      200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[i]     = new TH1D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT_%d", i), ";M_{ee} [GeV];Events;", 
							      200, 0.0, 200.0);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta_%d", i), ";p_{T} [GeV];#eta", 
							      nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta_%d", i), ";p_{T} [GeV];#eta", 
							      nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta_%d", i), ";1/p_{T} [GeV^{-1}];#eta", 
								50, 0.0, 0.1, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[i]  = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta_%d", i), ";1/p_{T} [GeV^{-1}];#eta", 
								50, 0.0, 0.1, 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta_%d", i), ";#phi;#eta", 
							      50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[i] = new TH2D(Form("hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta_%d", i), ";#phi;#eta", 
							      50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  }// for iCaloCategory

  hOld4LTracks_EleBG_DisapOS_DeltaQoverPt = new TH1D("hOld4LTracks_EleBG_DisapOS_DeltaQoverPt", ";#Deltaq/p_{T} [TeV^{-1}];Tracks", 200, -200.0, 200.0);
  hOld4LTracks_EleBG_DisapSS_DeltaQoverPt = new TH1D("hOld4LTracks_EleBG_DisapSS_DeltaQoverPt", ";#Deltaq/p_{T} [TeV^{-1}];Tracks", 200, -200.0, 200.0);

  hOld4LTracks_MuBG_Basic_ZMass = new TH1D("hOld4LTracks_MuBG_Basic_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MuBG_Disap_ZMass = new TH1D("hOld4LTracks_MuBG_Disap_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MuBG_Basic_PtEta = new TH2D("hOld4LTracks_MuBG_Basic_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_Disap_PtEta = new TH2D("hOld4LTracks_MuBG_Disap_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_Basic_PhiEta = new TH2D("hOld4LTracks_MuBG_Basic_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_MuBG_Disap_PhiEta = new TH2D("hOld4LTracks_MuBG_Disap_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_MuBG_BasicOS_ZMass = new TH1D("hOld4LTracks_MuBG_BasicOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MuBG_BasicSS_ZMass = new TH1D("hOld4LTracks_MuBG_BasicSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MuBG_DisapOS_ZMass = new TH1D("hOld4LTracks_MuBG_DisapOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MuBG_DisapSS_ZMass = new TH1D("hOld4LTracks_MuBG_DisapSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);

  hOld4LTracks_MuBG_BasicOS_PtEta = new TH2D("hOld4LTracks_MuBG_BasicOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_BasicSS_PtEta = new TH2D("hOld4LTracks_MuBG_BasicSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_DisapOS_PtEta = new TH2D("hOld4LTracks_MuBG_DisapOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_DisapSS_PtEta = new TH2D("hOld4LTracks_MuBG_DisapSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  hOld4LTracks_MuBG_BasicOS_InvPtEta = new TH2D("hOld4LTracks_MuBG_BasicOS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_BasicSS_InvPtEta = new TH2D("hOld4LTracks_MuBG_BasicSS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_DisapOS_InvPtEta = new TH2D("hOld4LTracks_MuBG_DisapOS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_MuBG_DisapSS_InvPtEta = new TH2D("hOld4LTracks_MuBG_DisapSS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);

  hOld4LTracks_CaloIso_MuBG_Basic_ZMass = new TH1D("hOld4LTracks_CaloIso_MuBG_Basic_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MuBG_Disap_ZMass = new TH1D("hOld4LTracks_CaloIso_MuBG_Disap_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MuBG_Basic_PtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_Basic_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_Disap_PtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_Disap_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_Basic_PhiEta = new TH2D("hOld4LTracks_CaloIso_MuBG_Basic_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_Disap_PhiEta = new TH2D("hOld4LTracks_CaloIso_MuBG_Disap_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass = new TH1D("hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass = new TH1D("hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass = new TH1D("hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass = new TH1D("hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);

  hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta = new TH2D("hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 
						50, 0.0, 0.1, 25, -2.5, 2.5);

  hOld4LTracks_MuBG_DisapOS_DeltaQoverPt = new TH1D("hOld4LTracks_MuBG_DisapOS_DeltaQoverPt", ";#Deltaq/p_{T} [TeV^{-1}];Tracks", 200, -200.0, 200.0);
  hOld4LTracks_MuBG_DisapSS_DeltaQoverPt = new TH1D("hOld4LTracks_MuBG_DisapSS_DeltaQoverPt", ";#Deltaq/p_{T} [TeV^{-1}];Tracks", 200, -200.0, 200.0);

  hOld4LTracks_MSBG_Basic_ZMass = new TH1D("hOld4LTracks_MSBG_Basic_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MSBG_Disap_ZMass = new TH1D("hOld4LTracks_MSBG_Disap_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MSBG_Basic_PhiEta = new TH2D("hOld4LTracks_MSBG_Basic_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_MSBG_Disap_PhiEta = new TH2D("hOld4LTracks_MSBG_Disap_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_MSBG_BasicOS_ZMass = new TH1D("hOld4LTracks_MSBG_BasicOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MSBG_BasicSS_ZMass = new TH1D("hOld4LTracks_MSBG_BasicSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MSBG_DisapOS_ZMass = new TH1D("hOld4LTracks_MSBG_DisapOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_MSBG_DisapSS_ZMass = new TH1D("hOld4LTracks_MSBG_DisapSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);

  hOld4LTracks_MSBG_BasicOS_PhiEta = new TH2D("hOld4LTracks_MSBG_BasicOS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_MSBG_BasicSS_PhiEta = new TH2D("hOld4LTracks_MSBG_BasicSS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_MSBG_DisapOS_PhiEta = new TH2D("hOld4LTracks_MSBG_DisapOS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_MSBG_DisapSS_PhiEta = new TH2D("hOld4LTracks_MSBG_DisapSS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_CaloIso_MSBG_Basic_ZMass = new TH1D("hOld4LTracks_CaloIso_MSBG_Basic_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MSBG_Disap_ZMass = new TH1D("hOld4LTracks_CaloIso_MSBG_Disap_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MSBG_Basic_PhiEta = new TH2D("hOld4LTracks_CaloIso_MSBG_Basic_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_Disap_PhiEta = new TH2D("hOld4LTracks_CaloIso_MSBG_Disap_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass = new TH1D("hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass = new TH1D("hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass = new TH1D("hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);
  hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass = new TH1D("hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass", ";M_{#mu#mu} [GeV];Events;", 150, 50.0, 200.0);

  hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta = new TH2D("hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta = new TH2D("hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta = new TH2D("hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta = new TH2D("hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta = new TH2D("hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta = new TH2D("hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta = new TH2D("hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta = new TH2D("hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  for(int i=0;i<nMETTrig;i++){
    for(int iChain=0;iChain<nKinematicsChain;iChain++){
      hTrigMETEff_1stJetMET[iChain][i] = new TH2D(Form("hTrigMETEff_1stJetMET_Chain%d_%d", iChain, i), ";MET [GeV];1st jet p_{T} [GeV]", 100, 0.0, 1000.0, 100, 0.0, 1000.0);
      hTrigMETEff_1stJetMET_Passed[iChain][i] = new TH2D(Form("hTrigMETEff_1stJetMET_Passed_Chain%d_%d", iChain, i), ";MET [GeV];1st jet p_{T} [GeV]", 100, 0.0, 1000.0, 100, 0.0, 1000.0);
      hTrigMETEff_mT[iChain][i] = new TH1D(Form("hTrigMETEff_mT_Chain%d_%d", iChain, i), ";m_{T}(#mu, MET) [GeV];Events", 100, 0.0, 250.0);    
    }
  }// for iMETTrig

  hHoge = new TH1D("hHoge", "", 100, 0.0, 100.0);

  hOld4LTracks_Ele_DeltaR = new TH1D("hOld4LTracks_Ele_DeltaR", ";ele #DeltaR", 100, 0.0, 3.0);
  hOld4LTracks_Mu_DeltaR = new TH1D("hOld4LTracks_Mu_DeltaR", ";mu #DeltaR", 100, 0.0, 3.0);

  return 0;
}

int HistManager::Finalize(void){

  return 0;
}

int HistManager::Write(TFile *myFile){
  hnEvents->Write(""    , TObject::kOverwrite);
  hnEventsProcessedBCK->Write(""    , TObject::kOverwrite);
  hnPrimaryVertex->Write(""    , TObject::kOverwrite);
  hProdType->Write(""    , TObject::kOverwrite);
  hSumOfNumber->Write(""    , TObject::kOverwrite);
  hSumOfWeights->Write(""    , TObject::kOverwrite);
  hSumOfWeightsBCK->Write(""    , TObject::kOverwrite);
  hSumOfWeightsSquaredBCK->Write(""    , TObject::kOverwrite);


  hNvtx_Chain1->Write(""    , TObject::kOverwrite);
  hNvtx_Chain3->Write(""    , TObject::kOverwrite);
  hNvtxReweighted_Chain1->Write(""    , TObject::kOverwrite);
  hNvtxReweighted_Chain3->Write(""    , TObject::kOverwrite);

  hActualIPC_Chain1->Write(""    , TObject::kOverwrite);
  hActualIPC_Chain3->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_Chain1->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_Chain3->Write(""    , TObject::kOverwrite);

  hActualIPC_ForMuCR_Chain1->Write(""    , TObject::kOverwrite);
  hActualIPC_ForMuCR_Chain3->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_ForMuCR_Chain1->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_ForMuCR_Chain3->Write(""    , TObject::kOverwrite);

  hActualIPC_ForEleCR_Chain1->Write(""    , TObject::kOverwrite);
  hActualIPC_ForEleCR_Chain3->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_ForEleCR_Chain1->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_ForEleCR_Chain3->Write(""    , TObject::kOverwrite);

  hAIPC->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC->Write(""    , TObject::kOverwrite);
  hAIPC_Old4LTracks->Write(""    , TObject::kOverwrite);
  hCorrectedAIPC_Old4LTracks->Write(""    , TObject::kOverwrite);

  hOld4LTracks_Ele_DeltaR->Write(""    , TObject::kOverwrite);
  hOld4LTracks_Mu_DeltaR->Write(""    , TObject::kOverwrite);

  myFile->mkdir("Common");
  myFile->cd("Common");
  hOld4LTracks_EleBG_BasicOS_TruthCharge->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_BasicSS_TruthCharge->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapOS_TruthCharge->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapSS_TruthCharge->Write("", TObject::kOverwrite);

  hOld4LTracks_MuBG_BasicOS_TruthCharge->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_BasicSS_TruthCharge->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapOS_TruthCharge->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapSS_TruthCharge->Write("", TObject::kOverwrite);

  hOld4LTracks_EleBG_Basic_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_Disap_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_Basic_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_Disap_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_Basic_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_Disap_PhiEta->Write("", TObject::kOverwrite);

  hOld4LTracks_EleBG_BasicOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_BasicSS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapSS_ZMass->Write("", TObject::kOverwrite);

  hOld4LTracks_EleBG_BasicOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_BasicSS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapSS_PtEta->Write("", TObject::kOverwrite);

  hOld4LTracks_EleBG_BasicOS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_BasicSS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapOS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapSS_InvPtEta->Write("", TObject::kOverwrite);

  for(int i=0;i<5*25;i++){
    hOld4LTracks_TFCalo_EleBG_ZMass_432[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_TFCalo_EleBG_ZMass_1[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_TFCalo_EleBG_ZMass_0[i]->Write("", TObject::kOverwrite);
  }// for i

  for(int i=0;i<nCaloCategory;i++){
    hOld4LTracks_CaloIso_EleBG_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_PhiEta[i]->Write("", TObject::kOverwrite);

    hOld4LTracks_CaloIso_EleBG_OS_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_SS_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_OS_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_SS_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_OS_PhiEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_EleBG_SS_PhiEta[i]->Write("", TObject::kOverwrite);
    for(int j=0;j<nPileupCategory;j++){
      hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[i][j]->Write("", TObject::kOverwrite);
      hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[i][j]->Write("", TObject::kOverwrite);
    }

    hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_mT[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[i]->Write("", TObject::kOverwrite);

    hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[i]->Write("", TObject::kOverwrite);

    hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_mT[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[i]->Write("", TObject::kOverwrite);

    hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[i]->Write("", TObject::kOverwrite);
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[i]->Write("", TObject::kOverwrite);
  }

  hOld4LTracks_EleBG_DisapSS_DeltaQoverPt->Write("", TObject::kOverwrite);
  hOld4LTracks_EleBG_DisapOS_DeltaQoverPt->Write("", TObject::kOverwrite);

  hOld4LTracks_MuBG_Basic_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_Disap_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_Basic_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_Disap_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_Basic_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_Disap_PhiEta->Write("", TObject::kOverwrite);

  hOld4LTracks_CaloIso_MuBG_Basic_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_Disap_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_Basic_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_Disap_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_Basic_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_Disap_PhiEta->Write("", TObject::kOverwrite);

  hOld4LTracks_MuBG_BasicOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_BasicSS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapSS_ZMass->Write("", TObject::kOverwrite);

  hOld4LTracks_MuBG_BasicOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_BasicSS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapSS_PtEta->Write("", TObject::kOverwrite);
  
  hOld4LTracks_MuBG_BasicOS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_BasicSS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapOS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapSS_InvPtEta->Write("", TObject::kOverwrite);
  
  hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass->Write("", TObject::kOverwrite);

  hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta->Write("", TObject::kOverwrite);

  hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta->Write("", TObject::kOverwrite);

  hOld4LTracks_MuBG_DisapSS_DeltaQoverPt->Write("", TObject::kOverwrite);
  hOld4LTracks_MuBG_DisapOS_DeltaQoverPt->Write("", TObject::kOverwrite);

  hOld4LTracks_MSBG_Basic_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_Basic_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_Disap_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_Disap_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_Basic_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_Basic_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_Disap_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_Disap_PhiEta->Write("", TObject::kOverwrite);

  hOld4LTracks_MSBG_BasicOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_BasicSS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_DisapOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_DisapSS_ZMass->Write("", TObject::kOverwrite);

  hOld4LTracks_MSBG_BasicOS_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_BasicSS_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_DisapOS_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_MSBG_DisapSS_PhiEta->Write("", TObject::kOverwrite);

  hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass->Write("", TObject::kOverwrite);

  hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta->Write("", TObject::kOverwrite);

  hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta->Write("", TObject::kOverwrite);
  hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta->Write("", TObject::kOverwrite);
  
  for(int i=0;i<nOld4LTracksCut;i++)
    hOld4LTracks_MinimumTrackCut[i]->Write(""    , TObject::kOverwrite);
  for(int i=0;i<nOld4LTracksCut;i++)
    hOld4LTracks_MinimumTrackCut_Wino[i]->Write(""    , TObject::kOverwrite);

  myFile->cd("../");

  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    myFile->mkdir(Form("Chain%d", iChain));
    myFile->cd(Form("Chain%d", iChain));

    hKinematics_CutFlow[iChain]->Write("", TObject::kOverwrite);
    hTracks_CutFlow[iChain]->Write("", TObject::kOverwrite);

    for(int iCut=0;iCut<nKinematicsCut;iCut++){
      hKinematics[iChain][iCut]->Write("", TObject::kOverwrite);
      hKinematics_N1[iChain][iCut]->Write("", TObject::kOverwrite);
    }// for iCut
    
    for(int iCut=0;iCut<nOld4LTracksCut;iCut++){
      hTracks_withoutMET[iChain][iCut]->Write(""    , TObject::kOverwrite);
      hTracks_N1_withoutMET[iChain][iCut]->Write("" , TObject::kOverwrite);
    }// for iCut
    
    for(int iTrig=0;iTrig<nMETTrig;iTrig++){
      hTrigMETEff_1stJetMET[iChain][iTrig]->Write("", TObject::kOverwrite);
      hTrigMETEff_1stJetMET_Passed[iChain][iTrig]->Write("", TObject::kOverwrite);
      hTrigMETEff_mT[iChain][iTrig]->Write("", TObject::kOverwrite);
    }// for iTrig

    myFile->cd("../");
  }// for iChain

  hHoge->Write();
  return 0;
}

int HistManager::FillFileInfo(NtupleReader *myReader){
  hnEvents->Fill(1, myReader->nEntry);
  hnEventsProcessedBCK->Add(myReader->hnEventsProcessedBCK, 1);
  hnPrimaryVertex->Add(myReader->hnPrimaryVertex, 1);
  hProdType->Add(myReader->hProdType, 1);
  hSumOfNumber->Add(myReader->hSumOfNumber, 1);
  hSumOfWeights->Add(myReader->hSumOfWeights, 1);
  hSumOfWeightsBCK->Add(myReader->hSumOfWeightsBCK, 1);
  hSumOfWeightsSquaredBCK->Add(myReader->hSumOfWeightsSquaredBCK, 1);
  
  return 0;
}

int HistManager::FillBeforeAnyCut(NtupleReader *myReader){
  double weight = (myReader->IsData) ? 1.0 : ((myReader->weightXsec) * (myReader->weightMCweight) * (myReader->weightPileupReweighting));
  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    hKinematics_CutFlow[iChain]->Fill(0.0, weight);
  }
  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    for(int i=0;i<nOld4LTracksCut;i++){
      fTracks_CutFlow[iChain][i] = false;
    }// for iChain
  }
  
  return 0;
}

int HistManager::FillHist(NtupleReader *myReader){
  int KinematicsQuality_Strong = MyUtil::CalcOldKinematicsQuality_Strong(myReader);
  int KinematicsQuality_EWK    = MyUtil::CalcOldKinematicsQuality_EWK(myReader);
  double weight = (myReader->IsData) ? 1.0 : ((myReader->weightXsec) * (myReader->weightMCweight) * (myReader->weightPileupReweighting));
  double weightwoPRW = (myReader->IsData) ? 1.0 : ((myReader->weightXsec) * (myReader->weightMCweight));

  int KinematicsQuality[nKinematicsChain];
  MyUtil::CalcKinematicsQuality(myReader, KinematicsQuality);

  /*   Fill Cut Flow   */
  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    hKinematics_CutFlow[iChain]->Fill(1, weight);
    if(myReader->LU_METtrigger_SUSYTools==false)
      continue;
    hKinematics_CutFlow[iChain]->Fill(2, weight);

    if(myReader->IsPassedLeptonVeto==false)
      continue;
    hKinematics_CutFlow[iChain]->Fill(3, weight);
      
    for(int iCut=0;iCut<nKinematicsCut;iCut++){
      if(MyUtil::IsPassed(KinematicsQuality[iChain], (1<<(iCut+1))-1)){
	hKinematics_CutFlow[iChain]->Fill(nLabelCommon+iCut, weight);
      }
      
      // 1110 1111
      if(MyUtil::IsPassed(KinematicsQuality[iChain], ((1<<nKinematicsCut)-1) & ~(1<<iCut)) ){
	hKinematics_N1[iChain][iCut]->Fill(MyUtil::GetKinematicsVal(myReader, iCut), weight);
      }
      
      // 0000 1111
      if(MyUtil::IsPassed(KinematicsQuality[iChain], (1<<iCut) - 1) ){
	hKinematics[iChain][iCut]->Fill(MyUtil::GetKinematicsVal(myReader, iCut), weight);
      }
    }// for iCut

    for(int iCut=0;iCut<nOld4LTracksCut;iCut++){
      if(fTracks_CutFlow[iChain][iCut]){
	hTracks_CutFlow[iChain]->Fill(iCut, weight);
      }
    }// for iCut
  }// for iChain
  
  if(MyUtil::IsPassed(KinematicsQuality[1], 0xFFE) && (myReader->missingET->p4.Pt()/1000.0 > 100.0)){
    hNvtx_Chain1->Fill(myReader->nPrimaryVertex, weightwoPRW);
    hNvtxReweighted_Chain1->Fill(myReader->nPrimaryVertex, weight);
    hActualIPC_Chain1->Fill(myReader->actualInteractionsPerCrossing, weight);
    hCorrectedAIPC_Chain1->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);
  }
  if(MyUtil::IsPassed(KinematicsQuality[1], 0xFDE) && (myReader->missingET_Electron->p4.Pt()/1000.0 > 100.0) && (myReader->JetMetdPhiMin50_Electron > KinematicsChain[1][5])){
    hActualIPC_ForEleCR_Chain1->Fill(myReader->actualInteractionsPerCrossing, weight);
    hCorrectedAIPC_ForEleCR_Chain1->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);
  }
  if(MyUtil::IsPassed(KinematicsQuality[1], 0xFDE) && (myReader->missingET_Muon->p4.Pt()/1000.0 > 100.0) && (myReader->JetMetdPhiMin50_Muon > KinematicsChain[1][5])){
    hActualIPC_ForMuCR_Chain1->Fill(myReader->actualInteractionsPerCrossing, weight);
    hCorrectedAIPC_ForMuCR_Chain1->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);
  }

  if(MyUtil::IsPassed(KinematicsQuality[3], 0xFFE) && (myReader->missingET->p4.Pt()/1000.0 > 100.0)){
    hNvtx_Chain3->Fill(myReader->nPrimaryVertex, weightwoPRW);
    hNvtxReweighted_Chain3->Fill(myReader->nPrimaryVertex, weight);
    hActualIPC_Chain3->Fill(myReader->actualInteractionsPerCrossing, weight);
    hCorrectedAIPC_Chain3->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);
  }
  if(MyUtil::IsPassed(KinematicsQuality[3], 0xFDE) && (myReader->missingET_Electron->p4.Pt()/1000.0 > 100.0) && (myReader->JetMetdPhiMin50_Electron > KinematicsChain[3][5])){
    hActualIPC_ForEleCR_Chain3->Fill(myReader->actualInteractionsPerCrossing, weight);
    hCorrectedAIPC_ForEleCR_Chain3->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);
  }
  if(MyUtil::IsPassed(KinematicsQuality[3], 0xFDE) && (myReader->missingET_Muon->p4.Pt()/1000.0 > 100.0) && (myReader->JetMetdPhiMin50_Muon > KinematicsChain[3][5])){
    hActualIPC_ForMuCR_Chain3->Fill(myReader->actualInteractionsPerCrossing, weight);
    hCorrectedAIPC_ForMuCR_Chain3->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);
  }

  hAIPC->Fill(myReader->averageInteractionsPerCrossing, weight);
  hCorrectedAIPC->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);

  /*   MET Trigger Efficiency (W->munu)   */
  int iMET = MyUtil::GetUnprescaledMETTrigger(((myReader->IsData) ? myReader->RunNumber : myReader->randomRunNumber));
  if((iMET!=-1) &&
     (myReader->LU_SingleMuontrigger) && 
     (myReader->goodElectrons->size()==0) &&
     (myReader->goodMuons->size()==1) &&
     (myReader->goodMuons->at(0)->IsSignal==1) &&
     (myReader->goodMuons->at(0)->p4.Pt()/1000.0 > 27.0)){
    // basic + muon trigger + signal muon
    double MuWeight = (myReader->IsData) ? 1.0 : (weight * (myReader->weightMuonTriggerSF) * (myReader->weightMuonSF));
    double mT = (TMath::Sqrt( 2.0 * (myReader->goodMuons->at(0)->p4.Pt()) * (myReader->missingET->p4.Pt()) * 
			      (1.0 - TMath::Cos(myReader->goodMuons->at(0)->p4.DeltaPhi(myReader->missingET->p4)) ) ))/1000.0;

    for(int iChain=0;iChain<nKinematicsChain;iChain++){
      // 1111 1101 1110
      if(MyUtil::IsPassed(KinematicsQuality[iChain], 0xFDE) && (myReader->JetMetdPhiMin50_XeTrigger > KinematicsChain[iChain][5])){
	hTrigMETEff_mT[iChain][iMET]->Fill(mT, MuWeight);
	if( (mT > 30.0) && (mT < 100.0) ){
	  hTrigMETEff_1stJetMET[iChain][iMET]->Fill(myReader->missingET_XeTrigger->p4.Pt()/1000.0, myReader->goodJets->at(0)->p4.Pt()/1000.0, MuWeight);
	  if(myReader->LU_METtrigger_SUSYTools){
	    hTrigMETEff_1stJetMET_Passed[iChain][iMET]->Fill(myReader->missingET_XeTrigger->p4.Pt()/1000.0, myReader->goodJets->at(0)->p4.Pt()/1000.0, MuWeight);
	  }// Passed MET Trig
	}// mT (mu, MET)
      }
    }// iChain
  }// iMET 

  for(unsigned int i=0;i<(myReader->goodElectrons->size());i++){
    for(unsigned int j=0;j<(myReader->goodElectrons->at(i)->index_IDTrack.size());j++){
      int trackID = myReader->goodElectrons->at(i)->index_IDTrack.at(j);
      double dR = myReader->goodElectrons->at(i)->p4.DeltaR(myReader->conventionalTracks->at(trackID)->p4);
      hOld4LTracks_Ele_DeltaR->Fill(dR);
    }
  }

  for(unsigned int i=0;i<(myReader->goodMuons->size());i++){
    for(unsigned int j=0;j<(myReader->goodMuons->at(i)->index_IDTrack.size());j++){
      int trackID = myReader->goodMuons->at(i)->index_IDTrack.at(j);
      double dR = myReader->goodMuons->at(i)->p4.DeltaR(myReader->conventionalTracks->at(trackID)->p4);
      hOld4LTracks_Mu_DeltaR->Fill(dR);
    }
  }

  return 0;
}

int HistManager::FillOld4LTracks(NtupleReader *myReader, myTrack *Track){
  int KinematicsQuality_Strong = MyUtil::CalcOldKinematicsQuality_Strong(myReader);
  int KinematicsQuality_EWK    = MyUtil::CalcOldKinematicsQuality_EWK(myReader);
  int TrackQuality = MyUtil::CalcOld4LTrackQuality(Track);
  int nTrackLayer  = MyUtil::CalcNumberOfBarrelOnlyLayer(Track);
  double weight = (myReader->IsData) ? 1.0 : ((myReader->weightXsec) * (myReader->weightMCweight) * (myReader->weightPileupReweighting));
  int KinematicsQuality[nKinematicsChain];
  MyUtil::CalcKinematicsQuality(myReader, KinematicsQuality);
  int iPileupCategory = MyUtil::GetPileupCategory(myReader);

  // old4LTracks 
  if((nTrackLayer==4) && MyUtil::IsPassed(TrackQuality, 0x1FE01)){ // 001 1111 1110 0000 0001
    if(TMath::Abs(Track->d0/Track->d0err) < 5.0 && Track->etclus20_topo/1000.0 < 30.0){
      hAIPC_Old4LTracks->Fill(myReader->averageInteractionsPerCrossing, weight);
      hCorrectedAIPC_Old4LTracks->Fill(myReader->CorrectedAverageInteractionsPerCrossing, weight);

      for(int i=0;i<nOld4LTracksCut;i++){
	hOld4LTracks_MinimumTrackCut[i]->Fill(MyUtil::GetOld4LTrackVal(Track, i), weight);	
	if(Track->TruthType=="Wino")
	  hOld4LTracks_MinimumTrackCut_Wino[i]->Fill(MyUtil::GetOld4LTrackVal(Track, i), weight);	
      }// for 
    }
  }// old4LTracks

  // Pass MET trigger 
  if((myReader->LU_METtrigger_SUSYTools==true) && (myReader->IsPassedLeptonVeto==true)){
    bool fLowPt = (myReader->IsData ? Track->p4.Pt()/1000.0 < LowPtThreshold : true);

    for(int iChain=0;iChain<nKinematicsChain;iChain++){
      for(int iCut=0;iCut<nOld4LTracksCut;iCut++){
	// if iCut == 4
	// 0001 1111  Pass Kinematics
	if(fLowPt && (nTrackLayer==4) && MyUtil::IsPassed(KinematicsQuality[iChain], 0xFFF) && MyUtil::IsPassed(TrackQuality, (1<<(iCut+1))-1)){
	  fTracks_CutFlow[iChain][iCut] = true;	  
	}// 

	// 1110 1111   Pass Kinematics without MET
	if((nTrackLayer==4) && MyUtil::IsPassed(KinematicsQuality[iChain], 0xFFE) && MyUtil::IsPassed(TrackQuality, ((1<<nOld4LTracksCut) -1) & ~(1<<iCut)) ){
	  hTracks_N1_withoutMET[iChain][iCut]->Fill(MyUtil::GetOld4LTrackVal(Track, iCut), weight);
	}
	// 0000 1111
	if((nTrackLayer==4) && MyUtil::IsPassed(KinematicsQuality[iChain], 0xFFE) && MyUtil::IsPassed(TrackQuality, (1<<iCut) - 1) ){
	  hTracks_withoutMET[iChain][iCut]->Fill(MyUtil::GetOld4LTrackVal(Track, iCut), weight);
	}
      }// iCut
    }// iChain
  }//

  // Electron trigger
  if( (myReader->LU_SingleElectrontrigger==true) && (myReader->goodMuons->size()==0) && (myReader->goodElectrons->size()>0) ){
    for(unsigned int iEle=0;iEle<(myReader->goodElectrons->size());iEle++){
      if( (myReader->goodElectrons->at(iEle)->IsSignal==1) && (myReader->goodElectrons->at(iEle)->p4.Pt()/1000.0 > 30.0) ){
	double EleWeight = (myReader->IsData) ? 1.0 : (weight * (myReader->weightElectronTriggerSF) * (myReader->weightElectronSF));
	bool IsTrigMatched=false;
	std::vector<std::string> triggerList = MyUtil::GetUnprescaledSingleElectronTrigger(((myReader->IsData) ? myReader->RunNumber : myReader->randomRunNumber));
	for(unsigned int iTrig=0;iTrig<(triggerList.size());iTrig++){
	  IsTrigMatched |= myReader->goodElectrons->at(iEle)->triggerMatching[triggerList.at(iTrig)];
	}
	if(IsTrigMatched==false)
	  continue;

	/*   Calculate TF disap   */
	for(unsigned int iClus=0;iClus<(myReader->egammaClusters->size());iClus++){
	  // 11 1101 0111 1111 1100
	  if( (MyUtil::IsPassed(TrackQuality, 0x3D7FC)) && (Track->p4.Pt()/1000.0 > 10.0) && (Track->p4.DeltaR(myReader->egammaClusters->at(iClus)->p4) < 0.2) &&
	      (myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0 > 10.0) && (TMath::Abs(myReader->egammaClusters->at(iClus)->p4.Eta()) < 1.9) && (TMath::Abs(myReader->egammaClusters->at(iClus)->p4.Eta()) > 0.1)){
	  
	    bool IsSameElectron = false;
	    bool IsElectron = false;
	    for(unsigned int jEle=0;jEle<(myReader->goodElectrons->size());jEle++){
	      for(unsigned int iTrack=0;iTrack<(myReader->goodElectrons->at(jEle)->index_IDTrack.size());iTrack++){
		int jID = myReader->goodElectrons->at(jEle)->index_IDTrack.at(iTrack);
		double dR = Track->p4.DeltaR(myReader->conventionalTracks->at(jID)->p4);
		if(dR < 0.001){
		  IsElectron = true;
		  if(iEle==jEle)
		    IsSameElectron = true;
		}
	      }
	    }// for jEle is electron to check if this track is electron or not
	    if(IsSameElectron)
	      continue;
	    TLorentzVector prob;
	    prob.SetPtEtaPhiM(myReader->egammaClusters->at(iClus)->p4.Pt(), myReader->egammaClusters->at(iClus)->p4.Eta(), myReader->egammaClusters->at(iClus)->p4.Phi(), ElectronMass);
	    double RecoMass = (myReader->goodElectrons->at(iEle)->p4 + prob).M();
	    int ChargePair = (myReader->goodElectrons->at(iEle)->Charge)*(Track->charge);

	    // Truth Matching
	    int TagTruthCharge = 0;
	    int ProbeTruthCharge = 0;
	    for(unsigned int iTruth=0;iTruth<(myReader->truthParticles->size());iTruth++){
	      if(TMath::Abs(myReader->truthParticles->at(iTruth)->PdgId)!=11)
		continue;
	      double dR = myReader->truthParticles->at(iTruth)->p4.DeltaR(myReader->goodElectrons->at(iEle)->p4);
	      if(dR < 0.05){
		TagTruthCharge = (myReader->truthParticles->at(iTruth)->Charge)*(myReader->goodElectrons->at(iEle)->Charge);
	      }

	      dR = myReader->truthParticles->at(iTruth)->p4.DeltaR(Track->p4);
	      if(dR < 0.05){
		ProbeTruthCharge = (myReader->truthParticles->at(iTruth)->Charge)*(Track->charge);
	      }
	    }// truth matching
	    
	    if(IsElectron){
	      // probe track is basic electron
	      hOld4LTracks_EleBG_Basic_ZMass->Fill(RecoMass/1000.0, EleWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_EleBG_Basic_PtEta->Fill(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0, myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		hOld4LTracks_EleBG_Basic_PhiEta->Fill(myReader->egammaClusters->at(iClus)->p4.Phi(), myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
	      }
	      if(ChargePair==1){
		hOld4LTracks_EleBG_BasicSS_ZMass->Fill(RecoMass/1000.0, EleWeight);
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_EleBG_BasicSS_PtEta->Fill(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0, myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_BasicSS_InvPtEta->Fill(1.0/(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0), myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_BasicSS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }else if(ChargePair==-1){
		hOld4LTracks_EleBG_BasicOS_ZMass->Fill(RecoMass/1000.0, EleWeight);
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_EleBG_BasicOS_PtEta->Fill(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0, myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_BasicOS_InvPtEta->Fill(1.0/(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0), myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_BasicOS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }
	    }else if(Track->nSCTHits==0){
	      // probe track is disappearing track
	      hOld4LTracks_EleBG_Disap_ZMass->Fill(RecoMass/1000.0, EleWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_EleBG_Disap_PtEta->Fill(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0, myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		hOld4LTracks_EleBG_Disap_PhiEta->Fill(myReader->egammaClusters->at(iClus)->p4.Phi(), myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
	      }
	      if(ChargePair==1){
		hOld4LTracks_EleBG_DisapSS_ZMass->Fill(RecoMass/1000.0, EleWeight);
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_EleBG_DisapSS_PtEta->Fill(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0, myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_DisapSS_InvPtEta->Fill(1.0/(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0), myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_DisapSS_DeltaQoverPt->Fill(1e6*(Track->qoverpt - (myReader->goodElectrons->at(iEle)->Charge/myReader->egammaClusters->at(iClus)->p4.Pt())), EleWeight);
		  hOld4LTracks_EleBG_DisapSS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }else if(ChargePair==-1){
		hOld4LTracks_EleBG_DisapOS_ZMass->Fill(RecoMass/1000.0, EleWeight);
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_EleBG_DisapOS_PtEta->Fill(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0, myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_DisapOS_InvPtEta->Fill(1.0/(myReader->egammaClusters->at(iClus)->p4.Pt()/1000.0), myReader->egammaClusters->at(iClus)->p4.Eta(), EleWeight);
		  hOld4LTracks_EleBG_DisapOS_DeltaQoverPt->Fill(1e6*(Track->qoverpt - (-1.0*myReader->goodElectrons->at(iEle)->Charge/myReader->egammaClusters->at(iClus)->p4.Pt())), EleWeight);
		  hOld4LTracks_EleBG_DisapOS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }	      
	    }
	  }// associated track was found
	}// for iClus to find probe cluster

	/*   Calculate TF Calo cluster   */
	// 11 1101 0111 1111 1100
	if( (MyUtil::IsPassed(TrackQuality, 0x3D7FC)) && (Track->p4.Pt()/1000.0 > 10.0) && (Track->nSCTHits>=6) ){ // for EleBG
	  bool IsSameEle = false;
	  for(unsigned int iTrack=0;iTrack<(myReader->goodElectrons->at(iEle)->index_IDTrack.size());iTrack++){
	    int iID = myReader->goodElectrons->at(iEle)->index_IDTrack.at(iTrack);
	    double dR = Track->p4.DeltaR(myReader->conventionalTracks->at(iID)->p4);
	    if(dR < 0.001){
	      IsSameEle = true;
	    }
	  }// for iTrack
	  if(IsSameEle)
	    continue;

	  TLorentzVector prob;
	  prob.SetPtEtaPhiM(Track->p4.Pt(), Track->p4.Eta(), Track->p4.Phi(), ElectronMass);
	  double RecoMass = (myReader->goodElectrons->at(iEle)->p4 + prob).M();
	  int ChargePair = (myReader->goodElectrons->at(iEle)->Charge)*(Track->charge);

	  int CaloCategory = MyUtil::GetCaloCategory(Track);
	  hOld4LTracks_CaloIso_EleBG_ZMass[CaloCategory]->Fill(RecoMass/1000.0, EleWeight);
	  if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
	    hOld4LTracks_CaloIso_EleBG_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), EleWeight);
	    hOld4LTracks_CaloIso_EleBG_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), EleWeight);
	  }
	  if(ChargePair==1){
	    hOld4LTracks_CaloIso_EleBG_SS_ZMass[CaloCategory]->Fill(RecoMass/1000.0, EleWeight);
	    if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
	      hOld4LTracks_CaloIso_EleBG_SS_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_EleBG_SS_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[CaloCategory]->Fill(1.0/(Track->p4.Pt()/1000.0), Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[CaloCategory][iPileupCategory]->Fill(Track->p4.Pt()/1000.0);
	    }
	  }else if(ChargePair==-1){
	    hOld4LTracks_CaloIso_EleBG_OS_ZMass[CaloCategory]->Fill(RecoMass/1000.0, EleWeight);

	    double vEta = Track->p4.Eta();
	    double vPt  = Track->p4.Pt()/1000.0;

	    for(int iEta=0;iEta<25;iEta++){
	      if(vEta > (iEta*0.2 - 2.5) && vEta < (iEta*0.2 - 2.3)){
		for(int iPt=0;iPt<5;iPt++){
		  if(vPt > XBinsLogPt10ForLepton[iPt+5] && vPt < XBinsLogPt10ForLepton[iPt+6]){
		    if(CaloCategory==0){
		      hOld4LTracks_TFCalo_EleBG_ZMass_0[iEta*5+iPt]->Fill(RecoMass/1000.0, EleWeight);
		    }else if(CaloCategory==1){
		      hOld4LTracks_TFCalo_EleBG_ZMass_1[iEta*5+iPt]->Fill(RecoMass/1000.0, EleWeight);
		    }else{
		      hOld4LTracks_TFCalo_EleBG_ZMass_432[iEta*5+iPt]->Fill(RecoMass/1000.0, EleWeight);
		    }
		  }
		}
	      }// for iPt
	    }// for iEta

	    if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
	      hOld4LTracks_CaloIso_EleBG_OS_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_EleBG_OS_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[CaloCategory]->Fill(1.0/(Track->p4.Pt()/1000.0), Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[CaloCategory][iPileupCategory]->Fill(Track->p4.Pt()/1000.0);
	    }
	  }
	}// find probe

	// 11 1111 0111 1111 1100
	if( (MyUtil::IsPassed(TrackQuality, 0x3F7FC)) && (Track->p4.Pt()/1000.0 > 10.0) && (Track->nSCTHits>=6) ){// for TauBG
	  TLorentzVector prob;
	  prob.SetPtEtaPhiM(Track->p4.Pt(), Track->p4.Eta(), Track->p4.Phi(), TauMass);
	  double RecoMass = (myReader->goodElectrons->at(iEle)->p4 + prob).M()/1000.0;
	  int ChargePair = (myReader->goodElectrons->at(iEle)->Charge)*(Track->charge);
	  double mT = (TMath::Sqrt( 2.0 * (myReader->goodElectrons->at(iEle)->p4.Pt()) * (myReader->missingET->p4.Pt()) * 
				    (1.0 - TMath::Cos(myReader->goodElectrons->at(iEle)->p4.DeltaPhi(myReader->missingET->p4))) ) )/1000.0;
	  bool fPassmT = (mT < 40.0);
	  bool fPassZMass = ((Z_mass/1000.0 - RecoMass) > 15.0) && ((Z_mass/1000.0 - RecoMass) < 50.0);

	  int CaloCategory = MyUtil::GetCaloCategory(Track);
	  if(fPassmT) hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[CaloCategory]->Fill(RecoMass, EleWeight);
	  if(fPassZMass) hOld4LTracks_CaloIso_TauBG_EleTag_mT[CaloCategory]->Fill(mT,       EleWeight);
	  if(fPassmT && fPassZMass){
	    hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), EleWeight);
	    hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), EleWeight);
	  }
	  if(ChargePair==1){
	    if(fPassmT) hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[CaloCategory]->Fill(RecoMass, EleWeight);
	    if(fPassZMass) hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[CaloCategory]->Fill(mT,       EleWeight);
	    if(fPassmT && fPassZMass){
	      hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[CaloCategory]->Fill(1.0/(Track->p4.Pt()/1000.0), Track->p4.Eta(), EleWeight);
	    }
	  }else if(ChargePair==-1){
	    if(fPassmT) hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[CaloCategory]->Fill(RecoMass, EleWeight);
	    if(fPassZMass) hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[CaloCategory]->Fill(mT,       EleWeight);
	    if(fPassmT && fPassZMass){
	      hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), EleWeight);
	      hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[CaloCategory]->Fill(1.0/(Track->p4.Pt()/1000.0), Track->p4.Eta(), EleWeight);
	    }
	  }
	}// for tauBG
      }// find tag electron
    }// for iEle
  }// electron trigger

  // Muon trigger
  if( (myReader->LU_SingleMuontrigger==true) && (myReader->goodElectrons->size()==0) && (myReader->goodMuons->size()>0) ){
    for(unsigned int iMu=0;iMu<(myReader->goodMuons->size());iMu++){
      if( (myReader->goodMuons->at(iMu)->IsSignal==1) && (myReader->goodMuons->at(iMu)->p4.Pt()/1000.0 > 30.0) ){
	double MuWeight = (myReader->IsData) ? 1.0 : (weight * (myReader->weightMuonTriggerSF) * (myReader->weightMuonSF));
	bool IsTrigMatched=false;
	std::vector<std::string> triggerList = MyUtil::GetUnprescaledSingleMuonTrigger(((myReader->IsData) ? myReader->RunNumber : myReader->randomRunNumber));
	for(unsigned int iTrig=0;iTrig<(triggerList.size());iTrig++){
	  IsTrigMatched |= myReader->goodMuons->at(iMu)->triggerMatching[triggerList.at(iTrig)];
	}
	if(IsTrigMatched==false)
	  continue;
	// tag muon was found

	/*   Calculate TF disap   */
	for(unsigned int iMS=0;iMS<(myReader->msTracks->size());iMS++){
	  // 11 0011 0111 1111 1100
	  if( (MyUtil::IsPassed(TrackQuality, 0x337FC)) && (Track->p4.Pt()/1000.0 > 10.0) && (Track->p4.DeltaR(myReader->msTracks->at(iMS)->p4) < 0.2) &&
	      (myReader->msTracks->at(iMS)->p4.Pt()/1000.0 > 10.0) && (TMath::Abs(myReader->msTracks->at(iMS)->p4.Eta()) < 1.9) && (TMath::Abs(myReader->msTracks->at(iMS)->p4.Eta()) > 0.1)){
	    bool IsSameMuon = false;
	    bool IsMuon = false;
	    for(unsigned int jMu=0;jMu<(myReader->goodMuons->size());jMu++){
	      for(unsigned int iTrack=0;iTrack<(myReader->goodMuons->at(jMu)->index_IDTrack.size());iTrack++){
		int jID = myReader->goodMuons->at(jMu)->index_IDTrack.at(iTrack);
		double dR = Track->p4.DeltaR(myReader->conventionalTracks->at(jID)->p4);
		if(dR < 0.001){
		  IsMuon = true;
		  if(iMu==jMu)
		    IsSameMuon = true;
		}
	      }
	    }// for jMu is muon to check if this track is muon or not
	    if(IsSameMuon)
	      continue;
	    TLorentzVector prob;
	    prob.SetPtEtaPhiM(myReader->msTracks->at(iMS)->p4.Pt(), myReader->msTracks->at(iMS)->p4.Eta(), myReader->msTracks->at(iMS)->p4.Phi(), MuonMass);
	    double RecoMass = (myReader->goodMuons->at(iMu)->p4 + prob).M();
	    int ChargePair = (myReader->goodMuons->at(iMu)->Charge)*(Track->charge);

	    // Truth Matching
	    int TagTruthCharge = 0;
	    int ProbeTruthCharge = 0;
	    for(unsigned int iTruth=0;iTruth<(myReader->truthParticles->size());iTruth++){
	      if(TMath::Abs(myReader->truthParticles->at(iTruth)->PdgId)!=13)
		continue;
	      double dR = myReader->truthParticles->at(iTruth)->p4.DeltaR(myReader->goodMuons->at(iMu)->p4);
	      if(dR < 0.05){
		TagTruthCharge = (myReader->truthParticles->at(iTruth)->Charge)*(myReader->goodMuons->at(iMu)->Charge);
	      }

	      dR = myReader->truthParticles->at(iTruth)->p4.DeltaR(Track->p4);
	      if(dR < 0.05){
		ProbeTruthCharge = (myReader->truthParticles->at(iTruth)->Charge)*(Track->charge);
	      }
	    }// truth matching

	    if(IsMuon){
	      // probe track is basic electron
	      hOld4LTracks_MuBG_Basic_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MuBG_Basic_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_MuBG_Basic_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		hOld4LTracks_MuBG_Basic_PhiEta->Fill(myReader->msTracks->at(iMS)->p4.Phi(), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		  hOld4LTracks_CaloIso_MuBG_Basic_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  hOld4LTracks_CaloIso_MuBG_Basic_PhiEta->Fill(myReader->msTracks->at(iMS)->p4.Phi(), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		}
	      }

	      if(ChargePair==1){
		hOld4LTracks_MuBG_BasicSS_ZMass->Fill(RecoMass/1000.0, MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		  hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass->Fill(RecoMass/1000.0, MuWeight);		
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_MuBG_BasicSS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  hOld4LTracks_MuBG_BasicSS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		    hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		    hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  }
		  hOld4LTracks_MuBG_BasicSS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }else if(ChargePair==-1){
		hOld4LTracks_MuBG_BasicOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		  hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_MuBG_BasicOS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  hOld4LTracks_MuBG_BasicOS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		    hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		    hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  }
		  hOld4LTracks_MuBG_BasicOS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }
	    }else if(Track->nSCTHits==0){
	      // probe track is disappearing track
	      hOld4LTracks_MuBG_Disap_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MuBG_Disap_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_MuBG_Disap_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		hOld4LTracks_MuBG_Disap_PhiEta->Fill(myReader->msTracks->at(iMS)->p4.Phi(), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		  hOld4LTracks_CaloIso_MuBG_Disap_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  hOld4LTracks_CaloIso_MuBG_Disap_PhiEta->Fill(myReader->msTracks->at(iMS)->p4.Phi(), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		}
	      }
	      if(ChargePair==1){
		hOld4LTracks_MuBG_DisapSS_ZMass->Fill(RecoMass/1000.0, MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		  hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass->Fill(RecoMass/1000.0, MuWeight);

		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_MuBG_DisapSS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  hOld4LTracks_MuBG_DisapSS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		    hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		    hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  }
		  hOld4LTracks_MuBG_DisapSS_DeltaQoverPt->Fill(1e6*(Track->qoverpt - myReader->msTracks->at(iMS)->qoverpt), MuWeight);
		  hOld4LTracks_MuBG_DisapSS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }else if(ChargePair==-1){
		hOld4LTracks_MuBG_DisapOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		  hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
		if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		  hOld4LTracks_MuBG_DisapOS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  hOld4LTracks_MuBG_DisapOS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		    hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta->Fill(myReader->msTracks->at(iMS)->p4.Pt()/1000.0, myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		    hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta->Fill(1.0/(myReader->msTracks->at(iMS)->p4.Pt()/1000.0), myReader->msTracks->at(iMS)->p4.Eta(), MuWeight);
		  }
		  hOld4LTracks_MuBG_DisapOS_DeltaQoverPt->Fill(1e6*(Track->qoverpt - myReader->msTracks->at(iMS)->qoverpt), MuWeight);
		  hOld4LTracks_MuBG_DisapOS_TruthCharge->Fill(TagTruthCharge, ProbeTruthCharge);
		}
	      }	      
	    }
	  }// associated track was found
	}// for iMS to find probe MS track

	/*   Calculate TF MS track   */
	// 11 0011 0111 1111 1100
	if( (MyUtil::IsPassed(TrackQuality, 0x337FC)) && (Track->p4.Pt()/1000.0 > 10.0) && (Track->nSCTHits>=6) ){
	  bool IsSameMuon = false;
	  for(unsigned int iTrack=0;iTrack<(myReader->goodMuons->at(iMu)->index_IDTrack.size());iTrack++){
	    int iID = myReader->goodMuons->at(iMu)->index_IDTrack.at(iTrack);
	    double dR = Track->p4.DeltaR(myReader->conventionalTracks->at(iID)->p4);
	    if(dR < 0.001){
	      IsSameMuon = true;
	    }
	  }// for iTrack
	  if(IsSameMuon)
	    continue;

	  TLorentzVector prob;
	  prob.SetPtEtaPhiM(Track->p4.Pt(), Track->p4.Eta(), Track->p4.Phi(), MuonMass);
	  double RecoMass = (myReader->goodMuons->at(iMu)->p4 + prob).M();
	  int ChargePair = (myReader->goodMuons->at(iMu)->Charge)*(Track->charge);
	  if(Track->dRMSTrack < 0.4){
	    // probe track has associated MS track
	    hOld4LTracks_MSBG_Basic_ZMass->Fill(RecoMass/1000.0, MuWeight);
	    if(MyUtil::IsPassed(TrackQuality, 0x737FC))
	      hOld4LTracks_CaloIso_MSBG_Basic_ZMass->Fill(RecoMass/1000.0, MuWeight);
	    if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
	      hOld4LTracks_MSBG_Basic_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MSBG_Basic_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	    }

	    if(ChargePair==1){
	      hOld4LTracks_MSBG_BasicSS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_MSBG_BasicSS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		  hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		  hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
		}
	      }
	    }else if(ChargePair==-1){
	      hOld4LTracks_MSBG_BasicOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_MSBG_BasicOS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		  hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		  hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
		}
	      }
	    }
	  }else{
	    // probe track does not have associated MS track
	    hOld4LTracks_MSBG_Disap_ZMass->Fill(RecoMass/1000.0, MuWeight);
	    if(MyUtil::IsPassed(TrackQuality, 0x737FC))
	      hOld4LTracks_CaloIso_MSBG_Disap_ZMass->Fill(RecoMass/1000.0, MuWeight);
	    if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
	      hOld4LTracks_MSBG_Disap_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MSBG_Disap_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	    }

	    if(ChargePair==1){
	      hOld4LTracks_MSBG_DisapSS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_MSBG_DisapSS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		  hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		  hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
		}
	      }
	    }else if(ChargePair==-1){
	      hOld4LTracks_MSBG_DisapOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(MyUtil::IsPassed(TrackQuality, 0x737FC))
		hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass->Fill(RecoMass/1000.0, MuWeight);
	      if(TMath::Abs(RecoMass - Z_mass)/1000.0 < Z_Window){
		hOld4LTracks_MSBG_DisapOS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		if(MyUtil::IsPassed(TrackQuality, 0x737FC)){
		  hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
		  hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
		}
	      }
	    }
	  }
	}// find probe track

	// 11 1111 0111 1111 1100
	if( (MyUtil::IsPassed(TrackQuality, 0x3F7FC)) && (Track->p4.Pt()/1000.0 > 10.0) && (Track->nSCTHits>=6) ){ // for TauBG
	  TLorentzVector prob;
	  prob.SetPtEtaPhiM(Track->p4.Pt(), Track->p4.Eta(), Track->p4.Phi(), TauMass);
	  double RecoMass = (myReader->goodMuons->at(iMu)->p4 + prob).M()/1000.0;
	  int ChargePair = (myReader->goodMuons->at(iMu)->Charge)*(Track->charge);
	  double mT = (TMath::Sqrt( 2.0 * (myReader->goodMuons->at(iMu)->p4.Pt()) * (myReader->missingET->p4.Pt()) * 
				    (1.0 - TMath::Cos(myReader->goodMuons->at(iMu)->p4.DeltaPhi(myReader->missingET->p4))) ) )/1000.0;
	  bool fPassmT = (mT < 40.0);
	  bool fPassZMass = ((Z_mass/1000.0 - RecoMass) > 15.0) && ((Z_mass/1000.0 - RecoMass) < 50.0);

	  int CaloCategory = MyUtil::GetCaloCategory(Track);
	  if(fPassmT) hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[CaloCategory]->Fill(RecoMass, MuWeight);
	  if(fPassZMass) hOld4LTracks_CaloIso_TauBG_MuTag_mT[CaloCategory]->Fill(mT,       MuWeight);
	  if(fPassmT && fPassZMass){
	    hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	    hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
	  }
	  if(ChargePair==1){
	    if(fPassmT) hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[CaloCategory]->Fill(RecoMass, MuWeight);
	    if(fPassZMass) hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[CaloCategory]->Fill(mT,       MuWeight);
	    if(fPassmT && fPassZMass){
	      hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	      hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
	      hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[CaloCategory]->Fill(1.0/(Track->p4.Pt()/1000.0), Track->p4.Eta(), MuWeight);
	    }
	  }else if(ChargePair==-1){
	    if(fPassmT) hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[CaloCategory]->Fill(RecoMass, MuWeight);
	    if(fPassZMass) hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[CaloCategory]->Fill(mT,       MuWeight);
	    if(fPassmT && fPassZMass){
	      hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[CaloCategory]->Fill(Track->p4.Phi(), Track->p4.Eta(), MuWeight);
	      hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[CaloCategory]->Fill(Track->p4.Pt()/1000.0, Track->p4.Eta(), MuWeight);
	      hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[CaloCategory]->Fill(1.0/(Track->p4.Pt()/1000.0), Track->p4.Eta(), MuWeight);
	    }
	  }
	} // for tauBG
      }// find tag muon
    }// for iMu
  }// muon trigger

  return 0;
}
 
int HistManager::Get(TFile *myFile, int iOpt){
  hnEvents  = (TH1D *)myFile->Get("hnEvents");
  hnEventsProcessedBCK  = (TH1D *)myFile->Get("hnEventsProcessedBCK");
  hnPrimaryVertex       = (TH1D *)myFile->Get("hnPrimaryVertex");
  hProdType             = (TH1D *)myFile->Get("hProdType");
  hSumOfNumber            = (TH1D *)myFile->Get("hSumOfNumber");
  hSumOfWeights           = (TH1D *)myFile->Get("hSumOfWeights");
  hSumOfWeightsBCK        = (TH1D *)myFile->Get("hSumOfWeightsBCK");
  hSumOfWeightsSquaredBCK = (TH1D *)myFile->Get("hSumOfWeightsSquaredBCK");

  hNvtx_Chain1 = (TH1D *)myFile->Get("hNvtx_Chain1");
  hNvtx_Chain3 = (TH1D *)myFile->Get("hNvtx_Chain3");
  hNvtxReweighted_Chain1 = (TH1D *)myFile->Get("hNvtxReweighted_Chain1");
  hNvtxReweighted_Chain3 = (TH1D *)myFile->Get("hNvtxReweighted_Chain3");

  hActualIPC_Chain1 = (TH1D *)myFile->Get("hActualIPC_Chain1");
  hActualIPC_Chain3 = (TH1D *)myFile->Get("hActualIPC_Chain3");
  hCorrectedAIPC_Chain1 = (TH1D *)myFile->Get("hCorrectedAIPC_Chain1");
  hCorrectedAIPC_Chain3 = (TH1D *)myFile->Get("hCorrectedAIPC_Chain3");

  hActualIPC_ForMuCR_Chain1 = (TH1D *)myFile->Get("hActualIPC_ForMuCR_Chain1");
  hActualIPC_ForMuCR_Chain3 = (TH1D *)myFile->Get("hActualIPC_ForMuCR_Chain3");
  hCorrectedAIPC_ForMuCR_Chain1 = (TH1D *)myFile->Get("hCorrectedAIPC_ForMuCR_Chain1");
  hCorrectedAIPC_ForMuCR_Chain3 = (TH1D *)myFile->Get("hCorrectedAIPC_ForMuCR_Chain3");

  hActualIPC_ForEleCR_Chain1 = (TH1D *)myFile->Get("hActualIPC_ForEleCR_Chain1");
  hActualIPC_ForEleCR_Chain3 = (TH1D *)myFile->Get("hActualIPC_ForEleCR_Chain3");
  hCorrectedAIPC_ForEleCR_Chain1 = (TH1D *)myFile->Get("hCorrectedAIPC_ForEleCR_Chain1");
  hCorrectedAIPC_ForEleCR_Chain3 = (TH1D *)myFile->Get("hCorrectedAIPC_ForEleCR_Chain3");

  hAIPC = (TH1D *)myFile->Get("hAIPC");
  hCorrectedAIPC = (TH1D *)myFile->Get("hCorrectedAIPC");
  hAIPC_Old4LTracks = (TH1D *)myFile->Get("hAIPC_Old4LTracks");
  hCorrectedAIPC_Old4LTracks = (TH1D *)myFile->Get("hCorrectedAIPC_Old4LTracks");

  if(iOpt==0){
    hOld4LTracks_EleBG_BasicOS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicOS_TruthCharge");
    hOld4LTracks_EleBG_BasicSS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicSS_TruthCharge");
    hOld4LTracks_EleBG_DisapOS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapOS_TruthCharge");
    hOld4LTracks_EleBG_DisapSS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapSS_TruthCharge");
    
    hOld4LTracks_MuBG_BasicOS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicOS_TruthCharge");
    hOld4LTracks_MuBG_BasicSS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicSS_TruthCharge");
    hOld4LTracks_MuBG_DisapOS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapOS_TruthCharge");
    hOld4LTracks_MuBG_DisapSS_TruthCharge = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapSS_TruthCharge");
  }

  hOld4LTracks_EleBG_Basic_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_Basic_ZMass");
  hOld4LTracks_EleBG_Disap_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_Disap_ZMass");

  hOld4LTracks_EleBG_BasicOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicOS_ZMass");
  hOld4LTracks_EleBG_BasicSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicSS_ZMass");
  hOld4LTracks_EleBG_DisapOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapOS_ZMass");
  hOld4LTracks_EleBG_DisapSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapSS_ZMass");

  if(iOpt==0){
    hOld4LTracks_EleBG_Basic_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_Basic_PtEta");
    hOld4LTracks_EleBG_Disap_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_Disap_PtEta");
    hOld4LTracks_EleBG_Basic_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_Basic_PhiEta");
    hOld4LTracks_EleBG_Disap_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_Disap_PhiEta");
    
    hOld4LTracks_EleBG_BasicOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicOS_PtEta");
    hOld4LTracks_EleBG_BasicSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicSS_PtEta");
    hOld4LTracks_EleBG_DisapOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapOS_PtEta");
    hOld4LTracks_EleBG_DisapSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapSS_PtEta");
    
    hOld4LTracks_EleBG_BasicOS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicOS_InvPtEta");
    hOld4LTracks_EleBG_BasicSS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_BasicSS_InvPtEta");
    hOld4LTracks_EleBG_DisapOS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapOS_InvPtEta");
    hOld4LTracks_EleBG_DisapSS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapSS_InvPtEta");
  }
  for(int i=0;i<5*25;i++){
    hOld4LTracks_TFCalo_EleBG_ZMass_0[i] = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_TFCalo_EleBG_ZMass_0_%d", i));
    hOld4LTracks_TFCalo_EleBG_ZMass_1[i] = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_TFCalo_EleBG_ZMass_1_%d", i));
    hOld4LTracks_TFCalo_EleBG_ZMass_432[i] = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_TFCalo_EleBG_ZMass_432_%d", i));    
  }// for i

  for(int i=0;i<nCaloCategory;i++){
    hOld4LTracks_CaloIso_EleBG_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_ZMass_%d", i));
    if(iOpt==0){
      hOld4LTracks_CaloIso_EleBG_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_PtEta_%d", i));
      hOld4LTracks_CaloIso_EleBG_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_PhiEta_%d", i));
    }

    hOld4LTracks_CaloIso_EleBG_OS_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_OS_ZMass_%d", i));
    hOld4LTracks_CaloIso_EleBG_SS_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_SS_ZMass_%d", i));
    if(iOpt==0){
      hOld4LTracks_CaloIso_EleBG_OS_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_OS_PtEta_%d", i));
      hOld4LTracks_CaloIso_EleBG_SS_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_SS_PtEta_%d", i));
      hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_OS_InvPtEta_%d", i));
      hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_SS_InvPtEta_%d", i));
      hOld4LTracks_CaloIso_EleBG_OS_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_OS_PhiEta_%d", i));
      hOld4LTracks_CaloIso_EleBG_SS_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_SS_PhiEta_%d", i));
      for(int j=0;j<nPileupCategory;j++){
	hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[i][j]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt_%d_%d", i, j));
	hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[i][j]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt_%d_%d", i, j));
      }
    }

    hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_ZMass_%d", i));
    hOld4LTracks_CaloIso_TauBG_EleTag_mT[i]     = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_mT_%d", i));
    if(iOpt==0){
      hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_PtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta_%d", i));
    }

    hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass_%d", i));
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass_%d", i));
    hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[i]     = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT_%d", i));
    hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[i]     = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT_%d", i));
    if(iOpt==0){
      hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta_%d", i));
    }

    hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_ZMass_%d", i));
    hOld4LTracks_CaloIso_TauBG_MuTag_mT[i]     = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_mT_%d", i));
    if(iOpt==0){
      hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_PtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta_%d", i));
    }
    
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass_%d", i));
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass_%d", i));
    hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[i]     = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT_%d", i));
    hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[i]     = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT_%d", i));
    if(iOpt==0){
      hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[i]  = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta_%d", i));
      hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[i] = (TH2D *)myFile->Get(Form("Common/hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta_%d", i));
    }
  }

  hOld4LTracks_EleBG_DisapSS_DeltaQoverPt = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapSS_DeltaQoverPt");
  hOld4LTracks_EleBG_DisapOS_DeltaQoverPt = (TH1D *)myFile->Get("Common/hOld4LTracks_EleBG_DisapOS_DeltaQoverPt");

  hOld4LTracks_MuBG_Basic_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_Basic_ZMass");
  hOld4LTracks_MuBG_Disap_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_Disap_ZMass");

  if(iOpt==0){
    hOld4LTracks_MuBG_Basic_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_Basic_PtEta");
    hOld4LTracks_MuBG_Disap_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_Disap_PtEta");
    hOld4LTracks_MuBG_Basic_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_Basic_PhiEta");
    hOld4LTracks_MuBG_Disap_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_Disap_PhiEta");
  }

  hOld4LTracks_MuBG_BasicOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicOS_ZMass");
  hOld4LTracks_MuBG_BasicSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicSS_ZMass");
  hOld4LTracks_MuBG_DisapOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapOS_ZMass");
  hOld4LTracks_MuBG_DisapSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapSS_ZMass");

  if(iOpt==0){
    hOld4LTracks_MuBG_BasicOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicOS_PtEta");
    hOld4LTracks_MuBG_BasicSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicSS_PtEta");
    hOld4LTracks_MuBG_DisapOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapOS_PtEta");
    hOld4LTracks_MuBG_DisapSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapSS_PtEta");
    
    hOld4LTracks_MuBG_BasicOS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicOS_InvPtEta");
    hOld4LTracks_MuBG_BasicSS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_BasicSS_InvPtEta");
    hOld4LTracks_MuBG_DisapOS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapOS_InvPtEta");
    hOld4LTracks_MuBG_DisapSS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapSS_InvPtEta");
  }

  hOld4LTracks_CaloIso_MuBG_Basic_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_Basic_ZMass");
  hOld4LTracks_CaloIso_MuBG_Disap_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_Disap_ZMass");
  if(iOpt==0){
    hOld4LTracks_CaloIso_MuBG_Basic_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_Basic_PtEta");
    hOld4LTracks_CaloIso_MuBG_Disap_PtEta  = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_Disap_PtEta");
    hOld4LTracks_CaloIso_MuBG_Basic_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_Basic_PhiEta");
    hOld4LTracks_CaloIso_MuBG_Disap_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_Disap_PhiEta");
  }

  hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass");
  hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass");
  hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass");
  hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass");

  if(iOpt==0){
    hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta");
    hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta");
    hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta");
    hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta");
    
    hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta");
    hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta");
    hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta");
    hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta");
  }

  hOld4LTracks_MuBG_DisapSS_DeltaQoverPt = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapSS_DeltaQoverPt");
  hOld4LTracks_MuBG_DisapOS_DeltaQoverPt = (TH1D *)myFile->Get("Common/hOld4LTracks_MuBG_DisapOS_DeltaQoverPt");

  hOld4LTracks_MSBG_Basic_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MSBG_Basic_ZMass");
  hOld4LTracks_MSBG_Disap_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MSBG_Disap_ZMass");
  if(iOpt==0){
    hOld4LTracks_MSBG_Basic_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MSBG_Basic_PhiEta");
    hOld4LTracks_MSBG_Disap_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MSBG_Disap_PhiEta");
  }

  hOld4LTracks_MSBG_BasicOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MSBG_BasicOS_ZMass");
  hOld4LTracks_MSBG_BasicSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MSBG_BasicSS_ZMass");
  hOld4LTracks_MSBG_DisapOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MSBG_DisapOS_ZMass");
  hOld4LTracks_MSBG_DisapSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_MSBG_DisapSS_ZMass");

  if(iOpt==0){
    hOld4LTracks_MSBG_BasicOS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MSBG_BasicOS_PhiEta");
    hOld4LTracks_MSBG_BasicSS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MSBG_BasicSS_PhiEta");
    hOld4LTracks_MSBG_DisapOS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MSBG_DisapOS_PhiEta");
    hOld4LTracks_MSBG_DisapSS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_MSBG_DisapSS_PhiEta");
  }

  hOld4LTracks_CaloIso_MSBG_Basic_ZMass  = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_Basic_ZMass");
  hOld4LTracks_CaloIso_MSBG_Disap_ZMass  = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_Disap_ZMass");
  if(iOpt==0){
    hOld4LTracks_CaloIso_MSBG_Basic_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_Basic_PhiEta");
    hOld4LTracks_CaloIso_MSBG_Disap_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_Disap_PhiEta");
  }

  hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass");
  hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass");
  hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass");
  hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass = (TH1D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass");
  if(iOpt==0){
    hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta");
    hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta");
    hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta");
    hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta");

    hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta");
    hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta");
    hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta");
    hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta = (TH2D *)myFile->Get("Common/hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta");
  }

  for(int i=0;i<nOld4LTracksCut;i++)
    hOld4LTracks_MinimumTrackCut[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_MinimumTrackCut_%d", i));
  for(int i=0;i<nOld4LTracksCut;i++)
    hOld4LTracks_MinimumTrackCut_Wino[i]  = (TH1D *)myFile->Get(Form("Common/hOld4LTracks_MinimumTrackCut_Wino_%d", i));

  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    
    hKinematics_CutFlow[iChain] = (TH1D *)myFile->Get(Form("Chain%d/hKinematics_CutFlow_Chain%d", iChain, iChain));
    hTracks_CutFlow[iChain]     = (TH1D *)myFile->Get(Form("Chain%d/hTracks_CutFlow_Chain%d", iChain, iChain));

    for(int iCut=0;iCut<nKinematicsCut;iCut++){
      hKinematics[iChain][iCut]    = (TH1D *)myFile->Get(Form("Chain%d/hKinematics_Chain%d_%d", iChain, iChain, iCut));
      hKinematics_N1[iChain][iCut] = (TH1D *)myFile->Get(Form("Chain%d/hKinematics_N1_Chain%d_%d", iChain, iChain, iCut));
    }// for iCut

    for(int iCut=0;iCut<nOld4LTracksCut;iCut++){
      hTracks_withoutMET[iChain][iCut]    = (TH1D *)myFile->Get(Form("Chain%d/hTracks_withoutMET_Chain%d_%d", iChain, iChain, iCut));
      hTracks_N1_withoutMET[iChain][iCut] = (TH1D *)myFile->Get(Form("Chain%d/hOld4LTracks_N1_withoutMET_Chain%d_%d", iChain, iChain, iCut));      
    }// for iCut

    for(int i=0;i<nMETTrig;i++){
      if(iOpt==0){
	hTrigMETEff_1stJetMET[iChain][i]        = (TH2D *)myFile->Get(Form("Chain%d/hTrigMETEff_1stJetMET_Chain%d_%d", iChain, iChain, i));
	hTrigMETEff_1stJetMET_Passed[iChain][i] = (TH2D *)myFile->Get(Form("Chain%d/hTrigMETEff_1stJetMET_Passed_Chain%d_%d", iChain, iChain, i));
      }
      hTrigMETEff_mT[iChain][i]               = (TH1D *)myFile->Get(Form("Chain%d/hTrigMETEff_mT_Chain%d_%d", iChain, iChain, i));
    }// for iTrig

  }// for iChain

  return 0;
}

TH1D *HistManager::GetHistChain1D(std::string HistName, int iChain, int iHist){
  if(HistName==""){
    return NULL;
  }else if(HistName=="hKinematics_CutFlow"){
    return hKinematics_CutFlow[iChain];
  }else if(HistName=="hTracks_CutFlow"){
    return hTracks_CutFlow[iChain];
  }else if(HistName=="hKinematics"){
    return hKinematics[iChain][iHist];
  }else if(HistName=="hKinematics_N1"){
    return hKinematics_N1[iChain][iHist];
  }else if(HistName=="hTracks_withoutMET"){
    return hTracks_withoutMET[iChain][iHist];
  }else if(HistName=="hTracks_N1_withoutMET"){
    return hTracks_N1_withoutMET[iChain][iHist];
  }else if(HistName=="hTrigMETEff_mT"){
    return hTrigMETEff_mT[iChain][iHist];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt"){
    return hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt[iChain][iHist];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt"){
    return hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt[iChain][iHist];
  }else{
    return NULL;
  }
}// GetHistChain1D

TH2D *HistManager::GetHistChain2D(std::string HistName, int iChain, int iHist){
  if(HistName==""){
    return NULL;
  }else if(HistName=="hTrigMETEff_1stJetMET"){
    return hTrigMETEff_1stJetMET[iChain][iHist];
  }else if(HistName=="hTrigMETEff_1stJetMET_Passed"){
    return hTrigMETEff_1stJetMET_Passed[iChain][iHist];
  }else{
    return NULL;
  }
}// GetHistChain2D

TH1D *HistManager::GetHist1D(std::string HistName, int iOld4LTracks){
  if(iOld4LTracks==-1){
    return GetHist1D(HistName);
  }else if(HistName=="hOld4LTracks_MinimumTrackCut"){
    return hOld4LTracks_MinimumTrackCut[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_MinimumTrackCut_Wino"){
    return hOld4LTracks_MinimumTrackCut_Wino[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_TFCalo_EleBG_ZMass_0"){
    return hOld4LTracks_TFCalo_EleBG_ZMass_0[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_TFCalo_EleBG_ZMass_1"){
    return hOld4LTracks_TFCalo_EleBG_ZMass_1[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_TFCalo_EleBG_ZMass_432"){
    return hOld4LTracks_TFCalo_EleBG_ZMass_432[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_OS_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_OS_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_SS_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_SS_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_OS_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_SS_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_mT[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_OS_mT[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_SS_mT[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_mT[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT[iOld4LTracks];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT[iOld4LTracks];
  }
  
  return NULL;
} 

TH1D *HistManager::GetHist1D(std::string HistName){
  if(HistName=="hnEvents"){
    return hnEvents;
  }else if(HistName=="hnEventsProcessedBCK"){
    return hnEventsProcessedBCK;
  }else if(HistName=="hnPrimaryVertex"){
    return hnPrimaryVertex;
  }else if(HistName=="hProdType"){
    return hProdType;
  }else if(HistName=="hSumOfNumber"){
    return hSumOfNumber;
  }else if(HistName=="hSumOfWeights"){
    return hSumOfWeights;
  }else if(HistName=="hSumOfWeightsBCK"){
    return hSumOfWeightsBCK;
  }else if(HistName=="hSumOfWeightsSquaredBCK"){
    return hSumOfWeightsSquaredBCK;

  }else if(HistName=="hNvtx_Chain1"){
    return hNvtx_Chain1;
  }else if(HistName=="hNvtx_Chain3"){
    return hNvtx_Chain3;
  }else if(HistName=="hNvtxReweighted_Chain1"){
    return hNvtxReweighted_Chain1;
  }else if(HistName=="hNvtxReweighted_Chain3"){
    return hNvtxReweighted_Chain3;

  }else if(HistName=="hActualIPC_Chain1"){
    return hActualIPC_Chain1;
  }else if(HistName=="hActualIPC_Chain3"){
    return hActualIPC_Chain3;
  }else if(HistName=="hCorrectedAIPC_Chain1"){
    return hCorrectedAIPC_Chain1;
  }else if(HistName=="hCorrectedAIPC_Chain3"){
    return hCorrectedAIPC_Chain3;

  }else if(HistName=="hActualIPC_ForMuCR_Chain1"){
    return hActualIPC_ForMuCR_Chain1;
  }else if(HistName=="hActualIPC_ForMuCR_Chain3"){
    return hActualIPC_ForMuCR_Chain3;
  }else if(HistName=="hCorrectedAIPC_ForMuCR_Chain1"){
    return hCorrectedAIPC_ForMuCR_Chain1;
  }else if(HistName=="hCorrectedAIPC_ForMuCR_Chain3"){
    return hCorrectedAIPC_ForMuCR_Chain3;

  }else if(HistName=="hActualIPC_ForEleCR_Chain1"){
    return hActualIPC_ForEleCR_Chain1;
  }else if(HistName=="hActualIPC_ForEleCR_Chain3"){
    return hActualIPC_ForEleCR_Chain3;
  }else if(HistName=="hCorrectedAIPC_ForEleCR_Chain1"){
    return hCorrectedAIPC_ForEleCR_Chain1;
  }else if(HistName=="hCorrectedAIPC_ForEleCR_Chain3"){
    return hCorrectedAIPC_ForEleCR_Chain3;

  }else if(HistName=="hAIPC"){
    return hAIPC;
  }else if(HistName=="hCorrectedAIPC"){
    return hCorrectedAIPC;
  }else if(HistName=="hAIPC_Old4LTracks"){
    return hAIPC_Old4LTracks;
  }else if(HistName=="hCorrectedAIPC_Old4LTracks"){
    return hCorrectedAIPC_Old4LTracks;
  }else if(HistName=="hOld4LTracks_EleBG_Basic_ZMass"){
    return hOld4LTracks_EleBG_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_EleBG_BasicOS_ZMass"){
    return hOld4LTracks_EleBG_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_EleBG_BasicSS_ZMass"){
    return hOld4LTracks_EleBG_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_EleBG_Disap_ZMass"){
    return hOld4LTracks_EleBG_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_EleBG_DisapOS_ZMass"){
    return hOld4LTracks_EleBG_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_EleBG_DisapSS_ZMass"){
    return hOld4LTracks_EleBG_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_Basic_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_BasicOS_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_BasicSS_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_Disap_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_DisapOS_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_DisapSS_ZMass"){
    return hOld4LTracks_CaloIso_EleBG_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Basic_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Disap_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Basic_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Basic_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Disap_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Disap_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_mT"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Basic_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Disap_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_ZMass"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Basic_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Basic_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Disap_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Disap_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_mT;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_mT"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_mT;
  }else if(HistName=="hOld4LTracks_EleBG_DisapOS_DeltaQoverPt"){
    return hOld4LTracks_EleBG_DisapOS_DeltaQoverPt;
  }else if(HistName=="hOld4LTracks_EleBG_DisapSS_DeltaQoverPt"){
    return hOld4LTracks_EleBG_DisapSS_DeltaQoverPt;
  }else if(HistName=="hOld4LTracks_MuBG_Basic_ZMass"){
    return hOld4LTracks_MuBG_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_MuBG_BasicOS_ZMass"){
    return hOld4LTracks_MuBG_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_MuBG_BasicSS_ZMass"){
    return hOld4LTracks_MuBG_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_MuBG_Disap_ZMass"){
    return hOld4LTracks_MuBG_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_MuBG_DisapOS_ZMass"){
    return hOld4LTracks_MuBG_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_MuBG_DisapSS_ZMass"){
    return hOld4LTracks_MuBG_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_Basic_ZMass"){
    return hOld4LTracks_CaloIso_MuBG_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass"){
    return hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass"){
    return hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_Disap_ZMass"){
    return hOld4LTracks_CaloIso_MuBG_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass"){
    return hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass"){
    return hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_MuBG_DisapOS_DeltaQoverPt"){
    return hOld4LTracks_MuBG_DisapOS_DeltaQoverPt;
  }else if(HistName=="hOld4LTracks_MuBG_DisapSS_DeltaQoverPt"){
    return hOld4LTracks_MuBG_DisapSS_DeltaQoverPt;
  }else if(HistName=="hOld4LTracks_MSBG_Basic_ZMass"){
    return hOld4LTracks_MSBG_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_MSBG_BasicOS_ZMass"){
    return hOld4LTracks_MSBG_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_MSBG_BasicSS_ZMass"){
    return hOld4LTracks_MSBG_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_MSBG_Disap_ZMass"){
    return hOld4LTracks_MSBG_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_MSBG_DisapOS_ZMass"){
    return hOld4LTracks_MSBG_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_MSBG_DisapSS_ZMass"){
    return hOld4LTracks_MSBG_DisapSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_Basic_ZMass"){
    return hOld4LTracks_CaloIso_MSBG_Basic_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass"){
    return hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass"){
    return hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_Disap_ZMass"){
    return hOld4LTracks_CaloIso_MSBG_Disap_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass"){
    return hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass"){
    return hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass;
  }

  return NULL;
}

TH2D *HistManager::GetHist2D(std::string HistName, int i, int j){
  if(j==-1){
    return GetHist2D(HistName, i);
  }

  return NULL;
}

TH2D *HistManager::GetHist2D(std::string HistName, int i){
  if(i==-1){
    return GetHist2D(HistName);
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_OS_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_OS_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_SS_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_SS_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_OS_InvPtEta"){
    return hOld4LTracks_CaloIso_EleBG_OS_InvPtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_SS_InvPtEta"){
    return hOld4LTracks_CaloIso_EleBG_SS_InvPtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_OS_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_OS_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_SS_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_SS_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_OS_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_SS_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_OS_InvPtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_SS_InvPtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_OS_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_SS_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_OS_PhiEta[i];
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_SS_PhiEta[i];
  }

  return NULL;
}

TH2D *HistManager::GetHist2D(std::string HistName){
  if(HistName=="hOld4LTracks_EleBG_Basic_PtEta"){
    return hOld4LTracks_EleBG_Basic_PtEta;
  }else if(HistName=="hOld4LTracks_EleBG_Basic_PhiEta"){
    return hOld4LTracks_EleBG_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_EleBG_BasicOS_PtEta"){
    return hOld4LTracks_EleBG_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_EleBG_BasicSS_PtEta"){
    return hOld4LTracks_EleBG_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_EleBG_Disap_PtEta"){
    return hOld4LTracks_EleBG_Disap_PtEta;
  }else if(HistName=="hOld4LTracks_EleBG_Disap_PhiEta"){
    return hOld4LTracks_EleBG_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_EleBG_DisapOS_PtEta"){
    return hOld4LTracks_EleBG_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_EleBG_DisapSS_PtEta"){
    return hOld4LTracks_EleBG_DisapSS_PtEta;
  }else if(HistName=="hOld4LTracks_EleBG_BasicOS_InvPtEta"){
    return hOld4LTracks_EleBG_BasicOS_InvPtEta;
  }else if(HistName=="hOld4LTracks_EleBG_BasicSS_InvPtEta"){
    return hOld4LTracks_EleBG_BasicSS_InvPtEta;
  }else if(HistName=="hOld4LTracks_EleBG_DisapOS_InvPtEta"){
    return hOld4LTracks_EleBG_DisapOS_InvPtEta;
  }else if(HistName=="hOld4LTracks_EleBG_DisapSS_InvPtEta"){
    return hOld4LTracks_EleBG_DisapSS_InvPtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_Basic_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_Basic_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_BasicOS_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_BasicSS_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_Disap_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_Disap_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_DisapOS_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_DisapSS_PtEta"){
    return hOld4LTracks_CaloIso_EleBG_DisapSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_Basic_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_BasicOS_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_BasicOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_BasicSS_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_BasicSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_Disap_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_DisapOS_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_DisapOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_EleBG_DisapSS_PhiEta"){
    return hOld4LTracks_CaloIso_EleBG_DisapSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Basic_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Basic_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Disap_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Disap_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Basic_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_BasicSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_Disap_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_EleTag_DisapSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Basic_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Basic_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Disap_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Disap_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_PtEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Basic_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_BasicSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_Disap_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_PhiEta"){
    return hOld4LTracks_CaloIso_TauBG_MuTag_DisapSS_PhiEta;
  }else if(HistName=="hOld4LTracks_MuBG_Basic_PtEta"){
    return hOld4LTracks_MuBG_Basic_PtEta;
  }else if(HistName=="hOld4LTracks_MuBG_Basic_PhiEta"){
    return hOld4LTracks_MuBG_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_MuBG_BasicOS_PtEta"){
    return hOld4LTracks_MuBG_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_MuBG_BasicSS_PtEta"){
    return hOld4LTracks_MuBG_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_MuBG_Disap_PtEta"){
    return hOld4LTracks_MuBG_Disap_PtEta;
  }else if(HistName=="hOld4LTracks_MuBG_Disap_PhiEta"){
    return hOld4LTracks_MuBG_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_MuBG_DisapOS_PtEta"){
    return hOld4LTracks_MuBG_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_MuBG_DisapSS_PtEta"){
    return hOld4LTracks_MuBG_DisapSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_Basic_PhiEta"){
    return hOld4LTracks_CaloIso_MuBG_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta"){
    return hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta"){
    return hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_Disap_PtEta"){
    return hOld4LTracks_CaloIso_MuBG_Disap_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_Disap_PhiEta"){
    return hOld4LTracks_CaloIso_MuBG_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta"){
    return hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta"){
    return hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta;
  }else if(HistName=="hOld4LTracks_MuBG_BasicOS_InvPtEta"){
    return hOld4LTracks_MuBG_BasicOS_InvPtEta;
  }else if(HistName=="hOld4LTracks_MuBG_BasicSS_InvPtEta"){
    return hOld4LTracks_MuBG_BasicSS_InvPtEta;
  }else if(HistName=="hOld4LTracks_MuBG_DisapOS_InvPtEta"){
    return hOld4LTracks_MuBG_DisapOS_InvPtEta;
  }else if(HistName=="hOld4LTracks_MuBG_DisapSS_InvPtEta"){
    return hOld4LTracks_MuBG_DisapSS_InvPtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta"){
    return hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta"){
    return hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta"){
    return hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta"){
    return hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta;
  }else if(HistName=="hOld4LTracks_MSBG_Basic_PhiEta"){
    return hOld4LTracks_MSBG_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_MSBG_BasicOS_PhiEta"){
    return hOld4LTracks_MSBG_BasicOS_PhiEta;
  }else if(HistName=="hOld4LTracks_MSBG_BasicSS_PhiEta"){
    return hOld4LTracks_MSBG_BasicSS_PhiEta;
  }else if(HistName=="hOld4LTracks_MSBG_Disap_PhiEta"){
    return hOld4LTracks_MSBG_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_MSBG_DisapOS_PhiEta"){
    return hOld4LTracks_MSBG_DisapOS_PhiEta;
  }else if(HistName=="hOld4LTracks_MSBG_DisapSS_PhiEta"){
    return hOld4LTracks_MSBG_DisapSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_Basic_PhiEta"){
    return hOld4LTracks_CaloIso_MSBG_Basic_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta"){
    return hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta"){
    return hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_Disap_PhiEta"){
    return hOld4LTracks_CaloIso_MSBG_Disap_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta"){
    return hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta"){
    return hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta;


  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta;
  }else if(HistName=="hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta"){
    return hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta;

  }else if(HistName=="hOld4LTracks_EleBG_BasicOS_TruthCharge"){
    return hOld4LTracks_EleBG_BasicOS_TruthCharge;
  }else if(HistName=="hOld4LTracks_EleBG_BasicSS_TruthCharge"){
    return hOld4LTracks_EleBG_BasicSS_TruthCharge;
  }else if(HistName=="hOld4LTracks_EleBG_DisapOS_TruthCharge"){
    return hOld4LTracks_EleBG_DisapOS_TruthCharge;
  }else if(HistName=="hOld4LTracks_EleBG_DisapSS_TruthCharge"){
    return hOld4LTracks_EleBG_DisapSS_TruthCharge;
  }else if(HistName=="hOld4LTracks_MuBG_BasicOS_TruthCharge"){
    return hOld4LTracks_MuBG_BasicOS_TruthCharge;
  }else if(HistName=="hOld4LTracks_MuBG_BasicSS_TruthCharge"){
    return hOld4LTracks_MuBG_BasicSS_TruthCharge;
  }else if(HistName=="hOld4LTracks_MuBG_DisapOS_TruthCharge"){
    return hOld4LTracks_MuBG_DisapOS_TruthCharge;
  }else if(HistName=="hOld4LTracks_MuBG_DisapSS_TruthCharge"){
    return hOld4LTracks_MuBG_DisapSS_TruthCharge;
  }

  return NULL;
}
