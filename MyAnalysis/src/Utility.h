#ifndef __MyAnalysis__Utility_H_
#define __MyAnalysis__Utility_H_

#include <functional>
#include "src/PhysicsObjectProxyBase.h"
#include "src/Constant.h"
#include "src/NtupleReader.h"

namespace MyUtil{
  bool IsPassed(int Val, int Mask){
    return (Val&Mask)==Mask;    
  }

  int GetPileupCategory(NtupleReader *myReader){
    double MuVal = myReader->CorrectedAverageInteractionsPerCrossing;
    if(MuVal < 10.0){
      return 0;
    }else if(MuVal < 20.0){
      return 1;
    }else if(MuVal < 30.0){
      return 2;
    }else if(MuVal < 40.0){
      return 3;
    }else if(MuVal < 50.0){
      return 4;
    }else if(MuVal < 60.0){
      return 5;
    }else{
      return 6;
    }
  }

  int GetCaloCategory(myTrack *Track){
    double CaloEnergy = Track->etclus20_topo/1000.0;
    if(CaloEnergy < 5.0){
      return 0;
    }else if(CaloEnergy < 10.0){
      return 1;
    }else if(CaloEnergy < 15.0){
      return 2;
    }else if(CaloEnergy < 20.0){
      return 3;
    }else{
      return 4;
    }
  }// GetCaloCategory
  
  double GetKinematicsVal(NtupleReader *myReader, int iVal){
    if(iVal < 0 || iVal>=nKinematicsCut)
      return -1;
    double Val[nKinematicsCut] = { myReader->missingET->p4.Pt()/1000.0,
				   ((myReader->goodJets->size()>0) ? myReader->goodJets->at(0)->p4.Pt()/1000.0 : 0.0),
				   ((myReader->goodJets->size()>1) ? myReader->goodJets->at(1)->p4.Pt()/1000.0 : 0.0),
				   ((myReader->goodJets->size()>2) ? myReader->goodJets->at(2)->p4.Pt()/1000.0 : 0.0),
				   
				   ((myReader->goodJets->size()>3) ? myReader->goodJets->at(3)->p4.Pt()/1000.0 : 0.0),
				   myReader->JetMetdPhiMin50,
				   myReader->JetMetdPhiMin20,
				   myReader->HT,

				   myReader->EffMass,
				   myReader->Aplanarity,
				   ((myReader->HT!=0) ? (myReader->missingET->p4.Pt()/1000.0)/TMath::Sqrt(myReader->HT) : 0.0),
				   ((myReader->EffMass!=0) ? (myReader->missingET->p4.Pt()/1000.0)/(myReader->EffMass) : 0.0),
    };
    return Val[iVal];
  }// GetKinematicsVal

  int CalcKinematicsQuality(NtupleReader *myReader, int ret[nKinematicsChain]){
    for(int iChain=0;iChain<nKinematicsChain;iChain++){
      ret[iChain] = 0;
      
      for(int iCut=0;iCut<nKinematicsCut;iCut++){
	ret[iChain] |= ((GetKinematicsVal(myReader, iCut) > KinematicsChain[iChain][iCut]) << iCut);
      }// for iCut
    }// for iChain
    return 0;
  }// CalcKinematicsQuality

  int CalcOldKinematicsQuality_Strong(NtupleReader *myReader){
    // 0x1F
    bool fOldKinematics[nOldKinematicsCut_Strong]={ myReader->missingET->p4.Pt()/1000.0 > 150.0,
						    myReader->goodJets->size()>0 && myReader->goodJets->at(0)->p4.Pt()/1000.0 > 100.0,
						    myReader->goodJets->size()>1 && myReader->goodJets->at(1)->p4.Pt()/1000.0 >  50.0,
						    myReader->goodJets->size()>2 && myReader->goodJets->at(2)->p4.Pt()/1000.0 >  50.0,
						    
						    myReader->JetMetdPhiMin50 > 0.4};
    int ret=0;
    for(int i=0;i<nOldKinematicsCut_Strong;i++){
      ret |= (fOldKinematics[i] << i);
    }  
    
    return ret;
  }

  double GetOldKinematicsVal_Strong(NtupleReader *myReader, int iVal){
    // 0x1F
    double OldKinematicsVal[nOldKinematicsCut_Strong]= { myReader->missingET->p4.Pt()/1000.0,
							 ((myReader->goodJets->size()>0) ? myReader->goodJets->at(0)->p4.Pt()/1000.0 : 0.0),
							 ((myReader->goodJets->size()>1) ? myReader->goodJets->at(1)->p4.Pt()/1000.0 : 0.0),
							 ((myReader->goodJets->size()>2) ? myReader->goodJets->at(2)->p4.Pt()/1000.0 : 0.0),
							 myReader->JetMetdPhiMin50};    
    return OldKinematicsVal[iVal];
  }// GetOldKinematicsVal_Strong

  int CalcOldKinematicsQuality_EWK(NtupleReader *myReader){
    // 0x7
    bool fOldKinematics[nOldKinematicsCut_EWK]={ myReader->missingET->p4.Pt()/1000.0 > 200.0,
						 myReader->goodJets->size()>0 && myReader->goodJets->at(0)->p4.Pt()/1000.0 > 150.0,
						 myReader->JetMetdPhiMin50 > 1.0};
    int ret=0;
    for(int i=0;i<nOldKinematicsCut_EWK;i++){
      ret |= (fOldKinematics[i] << i);
    }  
    
    return ret;
  }

  double GetOldKinematicsVal_EWK(NtupleReader *myReader, int iVal){
    // 0x7
    double OldKinematicsVal[nOldKinematicsCut_EWK]= { myReader->missingET->p4.Pt()/1000.0,
						      ((myReader->goodJets->size()>0) ? myReader->goodJets->at(0)->p4.Pt()/1000.0 : 0.0),
						      myReader->JetMetdPhiMin50};    
    return OldKinematicsVal[iVal];
  }// GetOldKinematicsVal_EWK

  int CalcOld4LTrackQuality(myTrack *Track){
    // 0x3FFFF
    // 0x7FFFF
    bool fOld4LBasic[nOld4LTracksCut]= { Track->p4.Pt()/1000.0 > 20.0,
					 Track->nSCTHits==0,
					 Track->numberOfContribPixelLayers >= 4,		   
					 Track->expectInnermostPixelLayerHit==0 || Track->numberOfInnermostPixelLayerHits>=1,
				      
					 Track->numberOfGangedFlaggedFakes==0,
					 Track->nSiHoles==0,
					 Track->numberOfPixelSpoiltHits==0,
					 Track->nPixelOutliers==0,
				     
					 TMath::Abs(Track->d0sigTool) < 1.5,
					 //					 TMath::Abs(Track->d0/Track->d0err) < 1.5,
					 TMath::Abs(Track->z0sinthetawrtPV) < 0.5,
					 Track->ptcone40overPt_1gev < 0.04,
					 Track->IsPassedIsolatedLeading,
				      
					 Track->dRJet50 > 0.4,
					 Track->dRElectron > 0.4,
					 Track->dRMuon > 0.4,
					 Track->dRMSTrack > 0.4,
				      
					 Track->Quality > 0.1,
					 TMath::Abs(Track->p4.Eta()) > 0.1 && TMath::Abs(Track->p4.Eta()) < 1.9,
					 Track->etclus20_topo/1000.0 < 5.0};
    int ret=0;
    for(int i=0;i<nOld4LTracksCut;i++){
      ret |= (fOld4LBasic[i]<<i);
    }
    return ret;
  }// CalcOld4LTrackQuality

  double GetOld4LTrackVal(myTrack *Track, int iVal){
    // 0x3FFFF
    // 0x7FFFF
    double Old4LTrackVal[nOld4LTracksCut]= { (double)1000.0*Track->qoverpt,
					     (double)Track->nSCTHits,
					     (double)Track->numberOfContribPixelLayers,		   
					     (double)Track->numberOfInnermostPixelLayerHits,
					  
					     (double)Track->numberOfGangedFlaggedFakes,
					     (double)Track->nSiHoles,
					     (double)Track->numberOfPixelSpoiltHits,
					     (double)Track->nPixelOutliers,
					  
					     (double)Track->d0sigTool,
					     (double)Track->z0sinthetawrtPV,
					     (double)Track->ptcone40overPt_1gev,
					     (double)Track->IsPassedIsolatedLeading,
					  
					     (double)Track->dRJet50,
					     (double)Track->dRElectron,
					     (double)Track->dRMuon,
					     (double)Track->dRMSTrack,
					  
					     (double)Track->Quality,
					     (double)Track->p4.Eta(),
					     (double)Track->etclus20_topo/1000.0};
    
    return Old4LTrackVal[iVal];
  }// GetOld4LTrackVal

  std::vector<std::string> GetUnprescaledSingleElectronTrigger(unsigned int runNumber){
    if(runNumber == 0                                  ) return std::vector<std::string>{""};
    else if( 0       < runNumber && runNumber <=284484 ) return std::vector<std::string>{"HLT_e24_lhmedium_L1EM20VH"};//2015
    else if( runNumber == 298687                       ) return std::vector<std::string>{"HLT_e24_lhtight_nod0_ivarloose","HLT_e24_lhmedium_nod0_L1EM20VH"};//2016, periodA
    else if( 296939 <= runNumber && runNumber < 300345 ) return std::vector<std::string>{"HLT_e24_lhtight_nod0_ivarloose"};//2016, periodA
    else if( 300345 <= runNumber && runNumber < 302919 ) return std::vector<std::string>{"HLT_e24_lhtight_nod0_ivarloose"};//2016, periodB-D3
    else if( 302919 <= runNumber                       ) return std::vector<std::string>{"HLT_e26_lhtight_nod0_ivarloose"};//2016, periodD4-
    else{
      std::cout<<"GetUnprescaledSingleElectronTrigger : Invalid runNumber (" << runNumber << ") " <<std::endl;
      return std::vector<std::string>{""};
    }
  }
  std::vector<std::string> GetUnprescaledSingleMuonTrigger(unsigned int runNumber){
    if(runNumber == 0                                  ) return std::vector<std::string>{""};
    else if( 0       < runNumber && runNumber <=284484 ) return std::vector<std::string>{"HLT_mu20_iloose_L1MU15"};//2015
    else if( 296939 <= runNumber && runNumber < 300345 ) return std::vector<std::string>{"HLT_mu24_ivarloose","HLT_mu24_ivarloose_L1MU15","HLT_mu24_iloose","HLT_mu24_iloose_L1MU15"};//2016, periodA
    else if( 300345 <= runNumber && runNumber < 302919 ) return std::vector<std::string>{"HLT_mu24_ivarmedium","HLT_mu24_imedium"};//2016, periodB-D3
    else if( 302919 <= runNumber && runNumber < 303943 ) return std::vector<std::string>{"HLT_mu26_ivarmedium","HLT_mu26_imedium"};//2016, periodD4-E
    else if( 303943 <= runNumber && runNumber < 305380 ) return std::vector<std::string>{"HLT_mu26_ivarmedium","HLT_mu26_imedium"};//2016, periodF-G2
    else if( 305380 <= runNumber && runNumber < 307619 ) return std::vector<std::string>{"HLT_mu26_ivarmedium"};//2016, periodG3-I3
    else if( 307619 <= runNumber                       ) return std::vector<std::string>{"HLT_mu26_ivarmedium"};//2016, periodI4-
    else{
      std::cout<<"GetUnprescaledSingleMuonTrigger : Invalid runNumber (" << runNumber << ") " <<std::endl;
      return std::vector<std::string>{""};
    }
  }

  int GetUnprescaledMETTrigger(unsigned int runNumber){
    if     ( 0 < runNumber && runNumber <=284484 )       return 0;    // 2015   
    else if(runNumber >= 296939 && runNumber <= 302872 ) return 1;   // 2016 A-D3   
    else if(runNumber >= 302919 && runNumber <= 303892 ) return 2;   // 2016 D4-F1   
    else if(runNumber >= 303943 && runNumber <= 311481 ) return 3;   // 2016 F2-(open)   
    else if(runNumber >= 325713 && runNumber <= 331975 ) return 4;  // 2017 B1-D5   
    else if(runNumber >= 332303 && runNumber <= 340453 ) return 5;  // 2017 D6-(open)   
    else if(runNumber >= 348885 && runNumber <= 350013 ) return 6; // 2018 B-C5   
    else if(runNumber >= 350067 && runNumber <= 364292 ) return 7; // 2018 C5-(open)   return false; } 
    else{
      std::cout<<"GetUnprescaledMETTrigger : Invalid runNumber (" << runNumber << ") " <<std::endl;
      return -1;
    }
  }

  int CalcNumberOfBarrelOnlyLayer(myTrack *track){
    int nLayerBit=0;
    
    // pixel barrel
    if(track->pixelBarrel0==1)
      nLayerBit |= (1<<0);  
    if(track->pixelBarrel1==1)
      nLayerBit |= (1<<1);  
    if(track->pixelBarrel2==1)
      nLayerBit |= (1<<2);  
    if(track->pixelBarrel3==1)
      nLayerBit |= (1<<3);  
    
    // pixel endcap
    if(track->pixelEndCap0==1)
      nLayerBit |= (1<<4);  
    if(track->pixelEndCap1==1)
      nLayerBit |= (1<<5);  
    if(track->pixelEndCap2==1)
      nLayerBit |= (1<<6);  

    // sct barrel
    if(track->sctBarrel0==1)
      nLayerBit |= (1<<7);  
    if(track->sctBarrel1==1)
      nLayerBit |= (1<<8);  
    if(track->sctBarrel2==1)
      nLayerBit |= (1<<9);  
    if(track->sctBarrel3==1)
      nLayerBit |= (1<<10);  

    // sct endcap
    if(track->sctEndCap0==1)
      nLayerBit |= (1<<11);  
    if(track->sctEndCap1==1)
      nLayerBit |= (1<<12);  
    if(track->sctEndCap2==1)
      nLayerBit |= (1<<13);  
    if(track->sctEndCap3==1)
      nLayerBit |= (1<<14);  
    if(track->sctEndCap4==1)
      nLayerBit |= (1<<15);  
    if(track->sctEndCap5==1)
      nLayerBit |= (1<<16);  
    if(track->sctEndCap6==1)
      nLayerBit |= (1<<17);  
    if(track->sctEndCap7==1)
      nLayerBit |= (1<<18);  
    if(track->sctEndCap8==1)
      nLayerBit |= (1<<19);  

    if(nLayerBit==0xF){
      return 4;
    }else if(nLayerBit==0x7){
      return 3;
    }else{
      return -1;
    }
  }
  
  double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau){
    if(TMath::Abs(PropTime) > 1e10)
      return 1.0;
    else
      return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
  }  
  
  /* **********  For 3L Track (no maintenance)  ********** */
  int CalcNumberOfTrackLayer(myTrack *track){
    int nLayerBit=0;

    if(track->pixelBarrel0==1)
      nLayerBit |= (1<<0);  

    if(track->pixelBarrel1==1)
      nLayerBit |= (1<<1);
    
    if(track->pixelBarrel2==1 || track->pixelEndCap2==1)
      nLayerBit |= (1<<2);
    
    if(track->pixelBarrel3==1 || track->pixelEndCap0==1 || track->pixelEndCap1==1)
      nLayerBit |= (1<<3);
    
    if(track->sctBarrel0==1 || track->sctEndCap3==1 || track->sctEndCap7==1)
      nLayerBit |= (1<<4);

    if(track->sctBarrel1==1 || track->sctEndCap2==1 || track->sctEndCap6==1)
      nLayerBit |= (1<<5);
    
    if(track->sctBarrel2==1 || track->sctEndCap1==1 || track->sctEndCap5==1)
      nLayerBit |= (1<<6);
    
    if(track->sctBarrel3==1 || track->sctEndCap0==1 || track->sctEndCap4==1 || track->sctEndCap8==1)
      nLayerBit |= (1<<7);
    
    int nTrackLayer=0;
    int nRefBit=0;  
    for(int i=0;i<8;i++){
      nRefBit |= (1<<i);
      if(nLayerBit==nRefBit)
	nTrackLayer = i+1;
    }
  
    return nTrackLayer;
  } // CalcNumberOfTrackLayer

  double GetBasicTrackVal(myTrack *Track, int iTrackQuality){
    if(iTrackQuality==0){
      return Track->d0sigTool;
      //return Track->d0/Track->d0err;
    }else if(iTrackQuality==1){
      return Track->z0sinthetawrtPV;
    }else if(iTrackQuality==2){
      return Track->KVUEta;
    }else if(iTrackQuality==3){
      return Track->KVUQuality;
    }else if(iTrackQuality==4){
      return Track->KVUPt/1000.0;
    }else if(iTrackQuality==5){
      return Track->numberOfPixelSpoiltHits;
    }else if(iTrackQuality==6){
      return Track->numberOfGangedFlaggedFakes;
    }else if(iTrackQuality==7){
      return Track->dRJet50;
    }else if(iTrackQuality==8){
      return Track->dRElectron;
    }else if(iTrackQuality==9){
      return Track->dRMuon;
    }else if(iTrackQuality==10){
      return Track->dRMSTrack;
    }else if(iTrackQuality==11){
      return Track->ptcone40_1gev/Track->KVUPt;    
    }else if(iTrackQuality==12){
      return Track->etclus20_topo/1000.0;
    }else if(iTrackQuality==13){
      return Track->nTRTHitsPlusOutlier;
    }else{
      return -999.0;
    }
  }// GetBasicTrackVal

  int CalcTrackQuality(myTrack *Track, int nTrackLayer){
    double d0sig = TMath::Abs(Track->d0sigTool);
    //double d0sig = TMath::Abs(Track->d0/Track->d0err);
    double z0sin = TMath::Abs(Track->z0sinthetawrtPV);
    double eta = Track->KVUEta;
    int ret=0;

    if(d0sig<1.5)
      ret |= (1<<0);
    if(z0sin<0.5)
      ret |= (1<<1);
    if((TMath::Abs(eta)<1.9) && (TMath::Abs(eta)>0.1))
      ret |= (1<<2);
    if((nTrackLayer==3 && Track->KVUQuality>0.3) || (nTrackLayer==4 && Track->KVUQuality>0.1))
      ret |= (1<<3);
    if(Track->KVUPt/1000.0>20.0)
      ret |= (1<<4);
    if(Track->numberOfPixelSpoiltHits==0)
      ret |= (1<<5);
    if(Track->numberOfGangedFlaggedFakes==0)
      ret |= (1<<6);
    if(Track->dRJet50>0.4)
      ret |= (1<<7);
    if(Track->dRElectron>0.4)
      ret |= (1<<8);
    if(Track->dRMuon>0.4)
      ret |= (1<<9);
    if(Track->dRMSTrack>0.4)
      ret |= (1<<10);
    if((Track->ptcone40_1gev/Track->KVUPt) < 0.04)
      ret |= (1<<11);
    if(Track->etclus20_topo/1000.0 < 5.0)
      ret |= (1<<12);
    //if(Track->nTRTHitsPlusOutlier<5)
    //ret |= (1<<13);
    
    return ret;
  } // CalcTrackQuality
    
}

#endif
