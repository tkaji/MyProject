
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../MyAnalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( MyAnalysisAlg )

DECLARE_FACTORY_ENTRIES( MyAnalysis ) 
{
  DECLARE_ALGORITHM( MyAnalysisAlg );
}
