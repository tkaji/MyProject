#ifndef MyAnalysis_LinkDef_h
#define MyAnalysis_LinkDef_h

//MYCODE
//#include <MyAnalysis/WeightMap.h>
#include <src/PhysicsObjectProxyBase.h>

//ROOT
#include <TLorentzVector.h>
#include <TVector3.h>

//SYSTEM
#include <vector>
#include <utility>
#include <stdint.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class std::vector<TLorentzVector>+;

	//#pragma link C++ class WeightMap+;

#pragma link C++ class PhysObjBase+;
#pragma link C++ class myJet+;
#pragma link C++ class myTau+;
#pragma link C++ class myElectron+;
#pragma link C++ class myMuon+;
#pragma link C++ class myTrack+;
#pragma link C++ class myTruth+;
#pragma link C++ class myCaloCluster+;
#pragma link C++ class myZee+;
#pragma link C++ class myZmumu+;
#pragma link C++ class myMET+;
#pragma link C++ class std::vector<PhysObjBase*>+;
#pragma link C++ class std::vector<myJet*>+;
#pragma link C++ class std::vector<myTau*>+;
#pragma link C++ class std::vector<myElectron*>+;
#pragma link C++ class std::vector<myMuon*>+;
#pragma link C++ class std::vector<myTrack*>+;
#pragma link C++ class std::vector<myTruth*>+;
#pragma link C++ class std::vector<myZee*>+;
#pragma link C++ class std::vector<myZmumu*>+;
//#pragma link C++ class std::map< std::string, std::vector<PhysObjBase*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myJet*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myElectron*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myMuon*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myTrack*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myTruth*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myZee*>>+;
//#pragma link C++ class std::map< std::string, std::vector<myZmumu*>>+;
//#pragma link C++ class std::map< std::string, myMET*>+;
#endif

#endif /*MyAnalysis_LinkDef_h*/
