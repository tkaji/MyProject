#include "src/PhysicsObjectProxyBase.h"
#include "src/NtupleReader.h"
#include "TROOT.h"

NtupleReader::NtupleReader(const std::string FileName)
  : nEntry(-1),m_file(NULL),m_tree(NULL),hnEventsProcessedBCK(NULL),hnPrimaryVertex(NULL),hProdType(NULL),hSumOfNumber(NULL),hSumOfWeights(NULL),hSumOfWeightsBCK(NULL),hSumOfWeightsSquaredBCK(NULL),m_drawJets(NULL),m_goodJets(NULL),m_truthJets(NULL),m_goodMuons(NULL),m_msTracks(NULL),m_goodElectrons(NULL),m_egammaClusters(NULL),m_truthParticles(NULL),m_conventionalTracks(NULL),m_threeLayerTracks(NULL),m_fourLayerTracks(NULL),m_Tracks(NULL),GRL(false),IsPassedDRAWFilter(false),IsPassedDRAWMuonVeto(false),IsPassedDRAWElectronVeto(false),IsPassedBadEventVeto(false),IsPassedLeptonVeto(false),LU_METtrigger_SUSYTools(false),LU_SingleElectrontrigger(false),LU_SingleMuontrigger(false),weightXsec(0),weightMCweight(0),weightSherpaNjets(0),weightPileupReweighting(0),weightElectronSF(0),weightElectronTriggerSF(0),weightTagElectronTriggerSF(0),weightMuonSF(0),weightMuonTriggerSF(0),weightTagMuonTriggerSF(0),actualInteractionsPerCrossing(-1),averageInteractionsPerCrossing(-1),CorrectedAverageInteractionsPerCrossing(-1),treatAsYear(-1),prodType(-1),randomRunNumber(0),RunNumber(0),lumiBlock(0),EventNumber(-1),DetectorError(-1),nPrimaryVertex(0),nTracksFromPrimaryVertex(-1),missingET(NULL),missingET_Electron(NULL), missingET_Muon(NULL),missingET_XeTrigger(NULL),missingET_DRAW(NULL),drawJets(NULL),goodJets(NULL),truthJets(NULL),goodMuons(NULL),msTracks(NULL),goodElectrons(NULL),egammaClusters(NULL),truthParticles(NULL),conventionalTracks(NULL),threeLayerTracks(NULL),fourLayerTracks(NULL),Tracks(NULL)
{  
  drawJets           = new std::vector<myJet *>;
  goodJets           = new std::vector<myJet *>;
  truthJets          = new std::vector<myJet *>;
  goodMuons          = new std::vector<myMuon *>;
  msTracks           = new std::vector<myTrack *>;
  goodElectrons      = new std::vector<myElectron *>;
  egammaClusters     = new std::vector<myCaloCluster *>;
  truthParticles     = new std::vector<myTruth *>;
  Tracks             = new std::vector<myTrack *>;
  conventionalTracks = new std::vector<myTrack *>;
  threeLayerTracks   = new std::vector<myTrack *>;
  fourLayerTracks    = new std::vector<myTrack *>;

  // open input file
  m_file = TFile::Open(FileName.c_str(), "READ");
  gROOT->cd();
  hnEventsProcessedBCK    = (TH1D *)m_file->Get("nEventsProcessedBCK");
  hnPrimaryVertex         = (TH1D *)m_file->Get("nPrimaryVertex");
  hProdType               = (TH1D *)m_file->Get("prodType");
  hSumOfNumber            = (TH1D *)m_file->Get("sumOfNumber");
  hSumOfWeights           = (TH1D *)m_file->Get("sumOfWeights");
  hSumOfWeightsBCK        = (TH1D *)m_file->Get("sumOfWeightsBCK");
  hSumOfWeightsSquaredBCK = (TH1D *)m_file->Get("sumOfWeightsSquaredBCK");

  m_tree = (TTree *)m_file->Get("tree");
  m_tree->SetBranchAddress("MET.", &missingET);
  m_tree->SetBranchAddress("MET_ForEleCR.", &missingET_Electron);
  m_tree->SetBranchAddress("MET_ForMuCR.", &missingET_Muon);
  m_tree->SetBranchAddress("MET_ForXeTrigger.", &missingET_XeTrigger);
  //m_tree->SetBranchAddress("MET_ForDRAW.", &missingET_DRAW);
  //m_tree->SetBranchAddress("DRAWJets", &m_drawJets);
  m_tree->SetBranchAddress("GoodJets", &m_goodJets);
  m_tree->SetBranchAddress("TruthJets", &m_truthJets);
  m_tree->SetBranchAddress("GoodMuons", &m_goodMuons);
  m_tree->SetBranchAddress("MSTracks", &m_msTracks);
  m_tree->SetBranchAddress("GoodElectrons", &m_goodElectrons);
  m_tree->SetBranchAddress("EgammaClusters", &m_egammaClusters);
  m_tree->SetBranchAddress("Truths", &m_truthParticles);
  m_tree->SetBranchAddress("Tracks", &m_conventionalTracks);
  m_tree->SetBranchAddress("ThreeLayerTracks", &m_threeLayerTracks);
  m_tree->SetBranchAddress("FourLayerTracks", &m_fourLayerTracks);

  m_tree->SetBranchAddress("GRL", &GRL);
  //m_tree->SetBranchAddress("IsPassedDRAWMuonVeto", &IsPassedDRAWMuonVeto);
  //m_tree->SetBranchAddress("IsPassedDRAWElectronVeto", &IsPassedDRAWElectronVeto);
  m_tree->SetBranchAddress("IsPassedBadJet", &IsPassedBadJet);
  m_tree->SetBranchAddress("IsPassedNCBVeto", &IsPassedNCBVeto);
  m_tree->SetBranchAddress("IsPassedBadMuonVeto", &IsPassedBadMuonVeto);
  m_tree->SetBranchAddress("IsPassedBadMuonMETCleaning", &IsPassedBadMuonMETCleaning);


  m_tree->SetBranchAddress("IsPassedBadEventVeto", &IsPassedBadEventVeto);
  m_tree->SetBranchAddress("IsPassedLeptonVeto", &IsPassedLeptonVeto);
  m_tree->SetBranchAddress("LU_METtrigger_SUSYTools", &LU_METtrigger_SUSYTools);
  m_tree->SetBranchAddress("LU_SingleElectrontrigger", &LU_SingleElectrontrigger);
  m_tree->SetBranchAddress("LU_SingleMuontrigger", &LU_SingleMuontrigger);

  m_tree->SetBranchAddress("weightXsec"                , &weightXsec);
  m_tree->SetBranchAddress("weightMCweight"            , &weightMCweight);
  m_tree->SetBranchAddress("weightSherpaNjets"         , &weightSherpaNjets);
  m_tree->SetBranchAddress("weightPileupReweighting"   , &weightPileupReweighting);
  m_tree->SetBranchAddress("weightElectronSF"          , &weightElectronSF);
  m_tree->SetBranchAddress("weightElectronTriggerSF"   , &weightElectronTriggerSF);
  m_tree->SetBranchAddress("weightTagElectronTriggerSF", &weightTagElectronTriggerSF);
  m_tree->SetBranchAddress("weightMuonSF"              , &weightMuonSF);
  m_tree->SetBranchAddress("weightMuonTriggerSF"       , &weightMuonTriggerSF);
  m_tree->SetBranchAddress("weightTagMuonTriggerSF"    , &weightTagMuonTriggerSF);

  m_tree->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing);
  m_tree->SetBranchAddress("CorrectedAverageInteractionsPerCrossing", &CorrectedAverageInteractionsPerCrossing);
  m_tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  m_tree->SetBranchAddress("treatAsYear", &treatAsYear);
  m_tree->SetBranchAddress("prodType", &prodType);
  m_tree->SetBranchAddress("randomRunNumber", &randomRunNumber);
  m_tree->SetBranchAddress("RunNumber", &RunNumber);
  m_tree->SetBranchAddress("lumiBlock", &lumiBlock);
  m_tree->SetBranchAddress("EventNumber", &EventNumber);
  m_tree->SetBranchAddress("DetectorError", &DetectorError);
  m_tree->SetBranchAddress("nPrimaryVertex", &nPrimaryVertex);
  m_tree->SetBranchAddress("nTracksFromPrimaryVertex", &nTracksFromPrimaryVertex);

  nEntry = m_tree->GetEntries();
  IsData = (m_tree->GetEntries("weightMCweight>0") > 0) ? false : true;

  std::cout << "NtupleReader :: Input File : " << FileName << std::endl;
  std::cout << "NtupleReader :: Entries    : " << nEntry << std::endl;
}

NtupleReader::NtupleReader(TFile *myFile, const std::string TreeName, int Option)
  : nEntry(-1),m_file(NULL),m_tree(NULL),hnEventsProcessedBCK(NULL),hnPrimaryVertex(NULL),hProdType(NULL),hSumOfNumber(NULL),hSumOfWeights(NULL),hSumOfWeightsBCK(NULL),hSumOfWeightsSquaredBCK(NULL),m_drawJets(NULL),m_goodJets(NULL),m_truthJets(NULL),m_goodMuons(NULL),m_msTracks(NULL),m_goodElectrons(NULL),m_egammaClusters(NULL),m_truthParticles(NULL),m_conventionalTracks(NULL),m_threeLayerTracks(NULL),m_fourLayerTracks(NULL),m_Tracks(NULL),GRL(false),IsPassedDRAWFilter(false),IsPassedDRAWMuonVeto(false),IsPassedDRAWElectronVeto(false),IsPassedBadEventVeto(false),IsPassedLeptonVeto(false),LU_METtrigger_SUSYTools(false),LU_SingleElectrontrigger(false),LU_SingleMuontrigger(false),weightXsec(0),weightMCweight(0),weightSherpaNjets(0),weightPileupReweighting(0),weightElectronSF(0),weightElectronTriggerSF(0),weightTagElectronTriggerSF(0),weightMuonSF(0),weightMuonTriggerSF(0),weightTagMuonTriggerSF(0),actualInteractionsPerCrossing(-1),averageInteractionsPerCrossing(-1),CorrectedAverageInteractionsPerCrossing(-1),treatAsYear(-1),prodType(-1),randomRunNumber(0),RunNumber(0),lumiBlock(0),EventNumber(-1),DetectorError(-1),nPrimaryVertex(0),nTracksFromPrimaryVertex(-1),missingET(NULL),missingET_Electron(NULL), missingET_Muon(NULL),missingET_XeTrigger(NULL),missingET_DRAW(NULL),drawJets(NULL),goodJets(NULL),truthJets(NULL),goodMuons(NULL),msTracks(NULL),goodElectrons(NULL),egammaClusters(NULL),truthParticles(NULL),conventionalTracks(NULL),threeLayerTracks(NULL),fourLayerTracks(NULL),Tracks(NULL)
{  
  drawJets           = new std::vector<myJet *>;
  goodJets           = new std::vector<myJet *>;
  truthJets          = new std::vector<myJet *>;
  goodMuons          = new std::vector<myMuon *>;
  msTracks           = new std::vector<myTrack *>;
  goodElectrons      = new std::vector<myElectron *>;
  egammaClusters     = new std::vector<myCaloCluster *>;
  truthParticles     = new std::vector<myTruth *>;
  Tracks             = new std::vector<myTrack *>;
  conventionalTracks = new std::vector<myTrack *>;
  threeLayerTracks   = new std::vector<myTrack *>;
  fourLayerTracks    = new std::vector<myTrack *>;
  
  if(Option==5){

  }

  hnEventsProcessedBCK    = (TH1D *)myFile->Get("nEventsProcessedBCK");
  hnPrimaryVertex         = (TH1D *)myFile->Get("nPrimaryVertex");
  hProdType               = (TH1D *)myFile->Get("prodType");
  hSumOfNumber            = (TH1D *)myFile->Get("sumOfNumber");
  hSumOfWeights           = (TH1D *)myFile->Get("sumOfWeights");
  hSumOfWeightsBCK        = (TH1D *)myFile->Get("sumOfWeightsBCK");
  hSumOfWeightsSquaredBCK = (TH1D *)myFile->Get("sumOfWeightsSquaredBCK");

  m_tree = (TTree *)myFile->Get(TreeName.c_str());
  m_tree->SetBranchAddress("MET.", &missingET);
  m_tree->SetBranchAddress("MET_ForEleCR.", &missingET_Electron);
  m_tree->SetBranchAddress("MET_ForMuCR.", &missingET_Muon);
  m_tree->SetBranchAddress("GoodJets", &m_goodJets);
  m_tree->SetBranchAddress("GoodMuons", &m_goodMuons);
  m_tree->SetBranchAddress("MSTracks", &m_msTracks);
  m_tree->SetBranchAddress("GoodElectrons", &m_goodElectrons);
  m_tree->SetBranchAddress("EgammaClusters", &m_egammaClusters);
  m_tree->SetBranchAddress("Truths", &m_truthParticles);
  m_tree->SetBranchAddress("DisappearingTracks", &m_Tracks);

  m_tree->SetBranchAddress("GRL", &GRL);
  m_tree->SetBranchAddress("IsPassedBadEventVeto", &IsPassedBadEventVeto);
  m_tree->SetBranchAddress("IsPassedLeptonVeto", &IsPassedLeptonVeto);
  m_tree->SetBranchAddress("LU_METtrigger_SUSYTools", &LU_METtrigger_SUSYTools);
  m_tree->SetBranchAddress("LU_SingleElectrontrigger", &LU_SingleElectrontrigger);
  m_tree->SetBranchAddress("LU_SingleMuontrigger", &LU_SingleMuontrigger);

  m_tree->SetBranchAddress("JetMetdPhiMin50", &JetMetdPhiMin50);
  m_tree->SetBranchAddress("JetMetdPhiMin20", &JetMetdPhiMin20);
  m_tree->SetBranchAddress("JetMetdPhiMin50_ForEleCR", &JetMetdPhiMin50_Electron);
  m_tree->SetBranchAddress("JetMetdPhiMin20_ForEleCR", &JetMetdPhiMin20_Electron);
  m_tree->SetBranchAddress("JetMetdPhiMin50_ForMuCR" , &JetMetdPhiMin50_Muon);
  m_tree->SetBranchAddress("JetMetdPhiMin20_ForMuCR" , &JetMetdPhiMin20_Muon);

  m_tree->SetBranchAddress("HT", &HT);
  m_tree->SetBranchAddress("EffMass", &EffMass);
  m_tree->SetBranchAddress("Aplanarity", &Aplanarity);
  if(Option==3){
    m_tree->SetBranchAddress("TrigEff_Chain0"         , &TrigEff_Chain0);
    m_tree->SetBranchAddress("TrigEff_Chain1"         , &TrigEff_Chain1);
    m_tree->SetBranchAddress("TrigEff_Chain2"         , &TrigEff_Chain2);
    m_tree->SetBranchAddress("TrigEff_Chain3"         , &TrigEff_Chain3);
    m_tree->SetBranchAddress("TrigEff_Chain4"         , &TrigEff_Chain4);
    m_tree->SetBranchAddress("TrigEff_Chain5"         , &TrigEff_Chain5);
    m_tree->SetBranchAddress("TrigEff_Chain6"         , &TrigEff_Chain6);
  }

  m_tree->SetBranchAddress("weightXsec"                , &weightXsec);
  m_tree->SetBranchAddress("weightMCweight"            , &weightMCweight);
  m_tree->SetBranchAddress("weightSherpaNjets"         , &weightSherpaNjets);
  m_tree->SetBranchAddress("weightPileupReweighting"   , &weightPileupReweighting);
  m_tree->SetBranchAddress("weightElectronSF"          , &weightElectronSF);
  m_tree->SetBranchAddress("weightElectronTriggerSF"   , &weightElectronTriggerSF);
  m_tree->SetBranchAddress("weightTagElectronTriggerSF", &weightTagElectronTriggerSF);
  m_tree->SetBranchAddress("weightMuonSF"              , &weightMuonSF);
  m_tree->SetBranchAddress("weightMuonTriggerSF"       , &weightMuonTriggerSF);
  m_tree->SetBranchAddress("weightTagMuonTriggerSF"    , &weightTagMuonTriggerSF);

  m_tree->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing);
  m_tree->SetBranchAddress("CorrectedAverageInteractionsPerCrossing", &CorrectedAverageInteractionsPerCrossing);
  m_tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  m_tree->SetBranchAddress("prodType", &prodType);
  m_tree->SetBranchAddress("DetectorError", &DetectorError);
  m_tree->SetBranchAddress("randomRunNumber", &randomRunNumber);
  m_tree->SetBranchAddress("RunNumber", &RunNumber);
  m_tree->SetBranchAddress("lumiBlock", &lumiBlock);
  m_tree->SetBranchAddress("EventNumber", &EventNumber);
  nEntry = m_tree->GetEntries();
  IsData = (m_tree->GetEntries("weightMCweight>0") > 0) ? false : true;
}

NtupleReader::~NtupleReader(){
  delete hnEventsProcessedBCK; hnEventsProcessedBCK=NULL;
  delete hnPrimaryVertex;      hnPrimaryVertex=NULL;
  delete hProdType;            hProdType=NULL;
  delete hSumOfNumber;     hSumOfNumber=NULL;
  delete hSumOfWeights;    hSumOfWeights=NULL;
  delete hSumOfWeightsBCK; hSumOfWeightsBCK=NULL;
  delete hSumOfWeightsSquaredBCK; hSumOfWeightsSquaredBCK=NULL;

  delete m_tree;             m_tree=NULL;
  //m_file->Close();
  //delete m_file;             m_file=NULL;
  delete drawJets;           drawJets=NULL;
  delete goodJets;           goodJets=NULL;
  delete truthJets;          truthJets=NULL;
  delete goodMuons;          goodMuons=NULL;
  delete msTracks;           msTracks=NULL;
  delete goodElectrons;      goodElectrons=NULL;
  delete egammaClusters;     egammaClusters=NULL;
  delete truthParticles;     truthParticles=NULL;
  delete Tracks;             Tracks=NULL;
  delete conventionalTracks; conventionalTracks=NULL;
  delete threeLayerTracks;   threeLayerTracks=NULL;
  delete fourLayerTracks;    fourLayerTracks=NULL;
}

int NtupleReader::GetEntry(int iEntry){
  m_tree->GetEntry(iEntry);

  drawJets->clear();
  goodJets->clear();
  truthJets->clear();
  goodMuons->clear();
  msTracks->clear();
  goodElectrons->clear();
  egammaClusters->clear();
  truthParticles->clear();
  Tracks->clear();  
  conventionalTracks->clear();  
  threeLayerTracks->clear();  
  fourLayerTracks->clear();  

  
  // DRAW Jets
  //for(int i=0;i<(m_drawJets->GetEntries());i++){
  //    drawJets->push_back((myJet *)m_drawJets->ConstructedAt(i));
  //}// for good jets
  //std::sort(drawJets->begin(), drawJets->end(), SortMyJet());
  //  GetDRAWFlag();

  // Good Jets
  for(int i=0;i<(m_goodJets->GetEntries());i++){
    goodJets->push_back((myJet *)m_goodJets->ConstructedAt(i));
  }// for good jets
  std::sort(goodJets->begin(), goodJets->end(), SortMyJet());
  GetJetVariables();
  
  /*
  // Truth Jets
  for(int i=0;i<(m_truthJets->GetEntries());i++){
    truthJets->push_back((myJet *)m_truthJets->ConstructedAt(i));
  }// for truth jets
  std::sort(truthJets->begin(), truthJets->end(), SortMyJet());
  */

  // Good Muons
  for(int i=0;i<(m_goodMuons->GetEntries());i++){
    goodMuons->push_back((myMuon *)m_goodMuons->ConstructedAt(i));
  }// for good muons
  std::sort(goodMuons->begin(), goodMuons->end(), SortMyObj());

  // MS Tracks
  for(int i=0;i<(m_msTracks->GetEntries());i++){
    msTracks->push_back((myTrack *)m_msTracks->ConstructedAt(i));
  }// for good muons

  // Good Electrons
  for(int i=0;i<(m_goodElectrons->GetEntries());i++){
    goodElectrons->push_back((myElectron *)m_goodElectrons->ConstructedAt(i));
  }// for good muons

  // Egamma Clusters
  for(int i=0;i<(m_egammaClusters->GetEntries());i++){
    egammaClusters->push_back((myCaloCluster *)m_egammaClusters->ConstructedAt(i));
  }// for good muons
  std::sort(egammaClusters->begin(), egammaClusters->end(), SortMyObj());

  // Truth Particles
  for(int i=0;i<(m_truthParticles->GetEntries());i++){
    truthParticles->push_back((myTruth *)m_truthParticles->ConstructedAt(i));
  }// for truth particles
  
  // Conventional Tracks (standard track + pixel-tracklet)
  if(m_conventionalTracks!=NULL)
    for(int i=0;i<(m_conventionalTracks->GetEntries());i++){
      conventionalTracks->push_back((myTrack *)m_conventionalTracks->ConstructedAt(i));    
    }// for conventional tracks

  // Three Layer Tracks
  if(m_threeLayerTracks!=NULL)
    for(int i=0;i<(m_threeLayerTracks->GetEntries());i++){
      threeLayerTracks->push_back((myTrack *)m_threeLayerTracks->ConstructedAt(i));    
    }// for three layer tracks
  
  // Four Layer Tracks
  if(m_fourLayerTracks!=NULL)
    for(int i=0;i<(m_fourLayerTracks->GetEntries());i++){
      fourLayerTracks->push_back((myTrack *)m_fourLayerTracks->ConstructedAt(i));    
    }// for four layer tracks  

  // Disappearing Tracks
  if(m_Tracks!=NULL){
    for(int i=0;i<(m_Tracks->GetEntries());i++){
      Tracks->push_back((myTrack *)m_Tracks->ConstructedAt(i));    
    }// for disappearing tracks  
  }

  return 0;
}

int NtupleReader::GetDRAWFlag(void){
  IsPassedDRAWFilter = true;
  
  // Is passed Xe trigger ?
  if(LU_METtrigger_SUSYTools==false){
    IsPassedDRAWFilter = false;
    return -1;
  }
  
  // Is passed lepton veto ?
  if(IsPassedDRAWMuonVeto==false || IsPassedDRAWElectronVeto==false){
    IsPassedDRAWFilter = false;
    return -1;
  }
  
  // Is passed MET > 170 GeV ?
  if(missingET_DRAW->p4.Pt()/1000.0 < 170.0){
    IsPassedDRAWFilter = false;
    return -1;
  }

  int nJets=0;
  double dPhiMin=99.0;
  if(drawJets->size()>=2){
    for(unsigned int iJet=0;iJet<(drawJets->size());iJet++){
      if(nJets>=2)
	break;
      if(drawJets->at(iJet)->p4.Pt()/1000.0 < 40.0 || TMath::Abs(drawJets->at(iJet)->p4.Eta()) > 3.2)
	continue;
      double dPhi = TMath::Abs(drawJets->at(iJet)->p4.DeltaPhi(missingET_DRAW->p4));
      if(dPhi < dPhiMin)
	dPhiMin = dPhi;
      nJets++;
    }// for iJet
  }else{
    IsPassedDRAWFilter = false;
    return -1;
  }
  
  if(dPhiMin < 0.2 || (drawJets->at(1)->p4.Pt()/1000.0 < 60.0))
    IsPassedDRAWFilter = false;

  return 0;
}

int NtupleReader::GetJetVariables(void){
  JetMetdPhiMin20 = -1.0;
  JetMetdPhiMin50 = -1.0;
  JetMetdPhiMin20_Electron = -1.0;
  JetMetdPhiMin50_Electron = -1.0;
  JetMetdPhiMin20_Muon = -1.0;
  JetMetdPhiMin50_Muon = -1.0;
  JetMetdPhiMin20_XeTrigger = -1.0;
  JetMetdPhiMin50_XeTrigger = -1.0;
  HT = 0.0;
  EffMass = 0.0;
  Aplanarity = -1.0;

  TMatrixD MomentumTensor(3,3);
  double P2Sum = 0.0;
  if(goodJets->size()>0){
    for(unsigned int iJet=0;iJet<(goodJets->size());iJet++){
      if(iJet==4)
	break;
      
      double tmpdPhi           = TMath::Abs(missingET->p4.DeltaPhi(goodJets->at(iJet)->p4));
      double tmpdPhi_Electron  = TMath::Abs(missingET_Electron->p4.DeltaPhi(goodJets->at(iJet)->p4));
      double tmpdPhi_Muon      = TMath::Abs(missingET_Muon->p4.DeltaPhi(goodJets->at(iJet)->p4));
      double tmpdPhi_XeTrigger = (missingET_XeTrigger!=NULL) ? TMath::Abs(missingET_XeTrigger->p4.DeltaPhi(goodJets->at(iJet)->p4)) : 0.0;
      double JetPt = goodJets->at(iJet)->p4.Pt()/1000.0;
      double JetPx = goodJets->at(iJet)->p4.Px()/1000.0;
      double JetPy = goodJets->at(iJet)->p4.Py()/1000.0;
      double JetPz = goodJets->at(iJet)->p4.Pz()/1000.0;

      if(JetPt > 50.0){
	if(JetMetdPhiMin50<0 || JetMetdPhiMin50>tmpdPhi)
	  JetMetdPhiMin50 = tmpdPhi;
	if(JetMetdPhiMin50_Electron<0 || JetMetdPhiMin50_Electron>tmpdPhi_Electron)
	  JetMetdPhiMin50_Electron = tmpdPhi_Electron;
	if(JetMetdPhiMin50_Muon<0 || JetMetdPhiMin50_Muon>tmpdPhi_Muon)
	  JetMetdPhiMin50_Muon = tmpdPhi_Muon;
	if(JetMetdPhiMin50_XeTrigger<0 || JetMetdPhiMin50_XeTrigger>tmpdPhi_XeTrigger)
	  JetMetdPhiMin50_XeTrigger = tmpdPhi_XeTrigger;
      }

      if(JetPt > 20.0){
	if(JetMetdPhiMin20<0 || JetMetdPhiMin20>tmpdPhi)
	  JetMetdPhiMin20 = tmpdPhi;
	if(JetMetdPhiMin20_Electron<0 || JetMetdPhiMin20_Electron>tmpdPhi_Electron)
	  JetMetdPhiMin20_Electron = tmpdPhi_Electron;
	if(JetMetdPhiMin20_Muon<0 || JetMetdPhiMin20_Muon>tmpdPhi_Muon)
	  JetMetdPhiMin20_Muon = tmpdPhi_Muon;
	if(JetMetdPhiMin20_XeTrigger<0 || JetMetdPhiMin20_XeTrigger>tmpdPhi_XeTrigger)
	  JetMetdPhiMin20_XeTrigger = tmpdPhi_XeTrigger;

	HT += JetPt;
      
	MomentumTensor(0, 0) += JetPx*JetPx;
	MomentumTensor(0, 1) += JetPx*JetPy;
	MomentumTensor(0, 2) += JetPx*JetPz;
	MomentumTensor(1, 0) += JetPy*JetPx;
	MomentumTensor(1, 1) += JetPy*JetPy;
	MomentumTensor(1, 2) += JetPy*JetPz;
	MomentumTensor(2, 0) += JetPz*JetPx;
	MomentumTensor(2, 1) += JetPz*JetPy;
	MomentumTensor(2, 2) += JetPz*JetPz;
	P2Sum += JetPx*JetPx + JetPy*JetPy + JetPz*JetPz;
      } // Jet Pt > 20 GeV    
    }// for iJet

    if(P2Sum>0.0){
      for(int i=0;i<3;i++){
	for(int j=0;j<3;j++){
	  MomentumTensor(i, j) /= P2Sum;
	}
      }
    }

    TDecompSVD *aSVD = new TDecompSVD(MomentumTensor);
    TVectorD Lambda = aSVD->GetSig();
    Aplanarity = 1.5*Lambda[2];
    delete aSVD; aSVD=NULL;
  }
  EffMass = HT + (missingET->p4.Pt()/1000.0);

  return 0;
}

void NtupleReader::Test(void){
  for(int iEntry=0;iEntry<5;iEntry++){
    this->GetEntry(iEntry);
    std::cout << "iEntry = " << iEntry << std::endl;

    std::cout << "MET  : " << (missingET->p4).Pt()/1000.0 << " GeV" << std::endl;
    std::cout << "nJet : " << goodJets->size() << std::endl;
    std::cout << "nMuon: " << goodMuons->size() << std::endl;
    std::cout << "nMSTracks: " << msTracks->size() << std::endl;
    std::cout << "nElectron: " << goodElectrons->size() << std::endl;
    std::cout << "nEgammaClusters: " << egammaClusters->size() << std::endl;
    std::cout << "nTruth : " << truthParticles->size() << std::endl;

    std::cout << "nTracks : " << conventionalTracks->size() << std::endl;
    std::cout << "n3LTracks : " << threeLayerTracks->size() << std::endl;
    std::cout << "n4LTracks : " << fourLayerTracks->size() << std::endl;
  }
}
