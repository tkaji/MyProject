#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TRandom3.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TRandom3 *rn;
MyManager *myData;
HistManager *myHistData;
NtupleWriter *myDummy;
TEventList *eventList;
const int nFile=100;

const int nCat0=4;
const int nCat2=2;
double nFakeSR[nCat0+nCat2]     = {   0, 1000,  5.29, 5.29*2      , 15.88,   5.29              };
double nHadronSR[nCat0+nCat2]   = {5000,    0,  6.73, 6.73-5.29   ,150.75, 150.75+15.88-5.29   };
double nElectronSR[nCat0+nCat2] = {0.55, 0.55,  0.55, 0.55        ,114.12, 114.12              };
double nMuonSR[nCat0+nCat2]     = {0.31, 0.31,  0.31, 0.31        ,  0.35,   0.35              };
double nFakeVR[nCat0+nCat2]     = {   0, 7448, 39.37, 39.37*2     ,114.10,  39.37              };
double nHadronVR[nCat0+nCat2]   = {5000,    0, 52.28, 52.28-39.37 ,329.41, 329.41+114.10-39.37 };
double nElectronVR[nCat0+nCat2] = {2.63, 2.63,  2.63, 2.63        ,446.75, 446.75              };
double nMuonVR[nCat0+nCat2]     = {0.80, 0.80,  0.80, 0.80        ,  0.87,   0.87              };

double nSignalSR[nCat0+nCat2]   = {5.4829, 5.4829, 5.4829, 5.4829, 5.4829, 5.4829};
double nSignalVR[nCat0+nCat2]   = {0.730308, 0.730308, 0.730308, 0.730308, 0.730308, 0.730308};

TH1D *hFake;
TH1D *hHadronSR;
TH1D *hHadronVR;
TH1D *hElectronSR;
TH1D *hElectronVR;
TH1D *hMuonSR;
TH1D *hMuonVR;
TH1D *hSignalSR;
TH1D *hSignalVR;

TFile *tfInput;
TFile *tfOutput;

TCut GetKinematicsCut(int iMETRegion, int iMETType);
TCut CaloVETO  = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
int iChain;
int CaloReg;
double MuVal;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("CaloReg", po::value<int>()->default_value(1), "0 or 2")
    ("MuVal", po::value<double>()->default_value(1.0), "Signal Mu")
    ("output-file,o", po::value<std::string>()->default_value("myDummy"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  CaloReg = vm["CaloReg"].as<int>();
  MuVal = vm["MuVal"].as<double>();

  myData = new MyManager("/data3/tkaji/myAna/Data/data15-18_13TeV.root", "Data 2015-2018");
  myData->LoadMyFile();
  myHistData = myData->myHist_Old4L;
  eventList = new TEventList("eventList", "eventList");

  rn = new TRandom3();

  tfInput = new TFile(Form("/afs/cern.ch/user/t/tkaji/ROOT/chain1_CaloReg%d_SR.root", CaloReg), "READ");
  gROOT->cd();
  hFake = (TH1D *)tfInput->Get("hFakeSR");
  hHadronSR = (TH1D *)tfInput->Get("hHadronSR");
  hHadronVR = (TH1D *)tfInput->Get("hHadronVR");
  hElectronSR = (TH1D *)tfInput->Get("hElectronSR");
  hElectronVR = (TH1D *)tfInput->Get("hElectronVR");
  hMuonSR = (TH1D *)tfInput->Get("hMuonSR");
  hMuonVR = (TH1D *)tfInput->Get("hMuonVR");
  hSignalSR = (TH1D *)tfInput->Get("hSignalSR");
  hSignalVR = (TH1D *)tfInput->Get("hSignalVR");

  // Get Event
  eventList->Reset();
  TCut KinematicsCut= "(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0) &&";
  myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(3, 0) && CaloVETO);
  myData->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(0));

  int nCat = (CaloReg==0 ? nCat0 : nCat2);
  for(int iCat=0;iCat<nCat;iCat++){
    if(iCat!=2)
      continue;
    nSignalVR[iCat+CaloReg*2] = MuVal*nSignalVR[iCat+CaloReg*2];
    nSignalSR[iCat+CaloReg*2] = MuVal*nSignalSR[iCat+CaloReg*2];

    for(int iFile=0;iFile<nFile;iFile++){
      myDummy = new NtupleWriter("myTree_Old4L_Common_SR");
      int nEvent;

      // FakeVR    
      nEvent = rn->Poisson(nFakeVR[iCat+CaloReg*2]);      
      std::cout << Form("Cat%d_File%d_FakeVR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 110.0;
	double PtVal  = hFake->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // HadronVR
      nEvent = rn->Poisson(nHadronVR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_HadronVR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 120.0;
	double PtVal  = hHadronVR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // ElectronVR
      nEvent = rn->Poisson(nElectronVR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_ElectronVR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 130.0;
	double PtVal  = hElectronVR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->missingET_Electron->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // MuonVR
      nEvent = rn->Poisson(nMuonVR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_MuonVR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 140.0;
	double PtVal  = hMuonVR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->missingET_Muon->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // SignalVR
      nEvent = rn->Poisson(nSignalVR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_SignalVR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 145.0;
	double PtVal  = hSignalVR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent


      // FakeSR
      nEvent = rn->Poisson(nFakeSR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_FakeSR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 260.0;
	double PtVal  = hFake->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // HadronSR
      nEvent = rn->Poisson(nHadronSR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_HadronSR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 270.0;
	double PtVal  = hHadronSR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // ElectronSR
      nEvent = rn->Poisson(nElectronSR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_ElectronSR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 280.0;
	double PtVal  = hElectronSR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->missingET_Electron->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // MuonSR
      nEvent = rn->Poisson(nMuonSR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_MuonSR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 290.0;
	double PtVal  = hMuonSR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->missingET_Muon->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent

      // SignalSR
      nEvent = rn->Poisson(nSignalSR[iCat+CaloReg*2]);
      std::cout << Form("Cat%d_File%d_SignalSR: %d", iCat+CaloReg*2, iFile, nEvent) << std::endl;
      for(int iEvent=0;iEvent<nEvent;iEvent++){
	double METVal = 295.0;
	double PtVal  = hSignalSR->GetRandom();
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent
      
      //tfOutput = new TFile(Form("myDummy_Cat%d_File%d.root", iCat+CaloReg*2, iFile), "RECREATE");
      tfOutput = new TFile(Form("myDummy_Cat%d_MuVal%lf_File%d.root", iCat+CaloReg*2, MuVal, iFile), "RECREATE");
      myDummy->Write();
      tfOutput->Close();
      delete tfOutput; tfOutput=NULL;    
      delete myDummy; myDummy=NULL;
    }// for iFake
  }// iFile

  return 0;
}


TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==2){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 20.0)";
  return ret;
}
