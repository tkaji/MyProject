#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TCanvas *c0;
std::string PDFName;

TFile *tfMyInput;
TFile *tfOutput;

NtupleReader *myData;

const int nCut=14;
TH1D *hCutflow_Cleaning;

TH1D *hCutflow_EWK;
TH1D *hCutflow_Strong;

TH1D *hInvCutflow_EWK;
TH1D *hInvCutflow_Strong;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("output-file,o", po::value<std::string>()->default_value("MakeCutflowData"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");

  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  std::vector<std::string> ReadableInputFileList;
  ReadableInputFileList = InputFileList;

  std::string OutputFileName = vm["output-file"].as<std::string>();
  hCutflow_Cleaning = new TH1D("hCutflow_Cleaning", "", 8, 0, 8);

  unsigned int nFile = ReadableInputFileList.size();
  std::cout << "nFile : " << nFile << std::endl;

  for(unsigned int iFile=0;iFile<nFile;iFile++){
    myData = new NtupleReader(ReadableInputFileList.at(iFile));
    
    for(int iEntry=0;iEntry<(myData->nEntry);iEntry++){
      myData->GetEntry(iEntry);

      // 0 : All event
      hCutflow_Cleaning->Fill(0);
      
      if(myData->GRL==true){
	hCutflow_Cleaning->Fill(1);
      }else{
	continue;
      }
      if(myData->DetectorError==0){
	hCutflow_Cleaning->Fill(2);
      }else{
	continue;
      }
	
      if(myData->IsPassedBadJet){
	hCutflow_Cleaning->Fill(3);
      }else{
	continue;
      }

      if(myData->IsPassedNCBVeto){
	hCutflow_Cleaning->Fill(4);
      }else{
	continue;
      }

      if(myData->IsPassedBadMuonVeto){
	hCutflow_Cleaning->Fill(5);
      }else{
	continue;
      }

      if(myData->IsPassedBadMuonMETCleaning){
	hCutflow_Cleaning->Fill(6);
      }else{
	continue;
      }

      if(myData->LU_METtrigger_SUSYTools){
	hCutflow_Cleaning->Fill(7);
      }else{
	continue;
      }

    }// for iEntry    
    delete myData; myData=NULL;
  }// for iFile

  tfOutput = new TFile(OutputFileName.c_str(), "RECREATE");
  hCutflow_Cleaning->Write("", TObject::kOverwrite);

  return 0;
}
