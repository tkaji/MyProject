#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TCut.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TFile *tf;
TEventList *eventList=NULL;

TCanvas *c0;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH2D *hFrame;
std::string PDFName;
const int nMC16 = 3;
const int nRunMC16 = 3;
const std::string DirSignal = "/data3/tkaji/SignalProduction/Common_v2.3.x/local_v2.3.4/Output/";
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];

std::vector<int> vDSID_EWK_noMET = {448392, 448393};
std::vector<NtupleReader *> *myReader_noMET[nMC16];
std::vector<double> vSFSig_EWK_noMET[nMC16];
TH1D *hSignal_EWK_noMET;
TH1D *hSignal_EWK_noMET_WithJet;

std::vector<int> vDSID_EWK_Filter = {448302, 448303};
std::vector<NtupleReader *> *myReader_Filter[nMC16];
std::vector<double> vSFSig_EWK_Filter[nMC16];
TH1D *hSignal_EWK_Filter;
TH1D *hSignal_EWK_Filter_WithJet;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("output-file,o", po::value<std::string>()->default_value("plot_METFilter"), "output file");
  
  po::positional_options_description p;
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  c0 = new TCanvas("c0", "c0", 800, 600);
  hTMP = new TH1D("hTMP", "", 50, 0, 500);

  std::cout << "Start Loading Signal MC" << std::endl;
  /* **********   Start Loading Signal MC   ********** */
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    std::cout << MCName[iMC16] << " started" << std::endl;

    /* **********   Load Signal MC for Electroweak no MET filter  ********** */
    std::cout << "EWK noMET" << std::endl;
    myReader_noMET[iMC16] = new std::vector<NtupleReader *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_EWK_noMET.size());iDSID++){
      NtupleReader *myReader = new NtupleReader(Form("%s/out.%d_%s.PassThrough._000001.common_ntuple.root", DirSignal.c_str(), vDSID_EWK_noMET.at(iDSID), MCName[iMC16].c_str()));
      myReader_noMET[iMC16]->push_back(myReader);
      vSFSig_EWK_noMET[iMC16].push_back(IntLumi[iMC16]/(myReader_noMET[iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
    }// for iDSID
    if(iMC16==0){
      hSignal_EWK_noMET = new TH1D("hSignal_EWK_noMET", "", 50, 0, 500);
      hSignal_EWK_noMET_WithJet = new TH1D("hSignal_EWK_noMET_WithJet", "", 50, 0, 500);
    }

    /* **********   Load Signal MC for Electroweak Filtered Sample  ********** */
    std::cout << "EWK Filter" << std::endl;
    myReader_Filter[iMC16] = new std::vector<NtupleReader *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_EWK_Filter.size());iDSID++){
      NtupleReader *myReader = new NtupleReader(Form("%s/out.%d_%s.PassThrough._000001.common_ntuple.root", DirSignal.c_str(), vDSID_EWK_Filter.at(iDSID), MCName[iMC16].c_str()));
      myReader_Filter[iMC16]->push_back(myReader);
      vSFSig_EWK_Filter[iMC16].push_back(IntLumi[iMC16]/(myReader_Filter[iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
    }// for iDSID
    if(iMC16==0){
      hSignal_EWK_Filter = new TH1D("hSignal_EWK_Filter", "", 50, 0, 500);
      hSignal_EWK_Filter_WithJet = new TH1D("hSignal_EWK_Filter_WithJet", "", 50, 0, 500);
    }
  }// iMC16

  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    for(int iDSID=0;iDSID<2;iDSID++){

      int nEntry = myReader_noMET[iMC16]->at(iDSID)->nEntry;
      for(int iEntry=0;iEntry<nEntry;iEntry++){
	myReader_noMET[iMC16]->at(iDSID)->GetEntry(iEntry);

	double MET = myReader_noMET[iMC16]->at(iDSID)->missingET->p4.Pt()/1000.0;
	double Jet = (myReader_noMET[iMC16]->at(iDSID)->goodJets->size() > 0) ? myReader_noMET[iMC16]->at(iDSID)->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
	double dPhi = myReader_noMET[iMC16]->at(iDSID)->JetMetdPhiMin50;
	double weight = (myReader_noMET[iMC16]->at(iDSID)->weightXsec)*(myReader_noMET[iMC16]->at(iDSID)->weightMCweight)*(myReader_noMET[iMC16]->at(iDSID)->weightPileupReweighting);
	hSignal_EWK_noMET->Fill(MET, weight*vSFSig_EWK_noMET[iMC16].at(iDSID));
	if(Jet > 100.0 && dPhi > 1.0)
	  hSignal_EWK_noMET_WithJet->Fill(MET, weight*vSFSig_EWK_noMET[iMC16].at(iDSID));
      }// for iEntry

      nEntry = myReader_Filter[iMC16]->at(iDSID)->nEntry;
      for(int iEntry=0;iEntry<nEntry;iEntry++){
	myReader_Filter[iMC16]->at(iDSID)->GetEntry(iEntry);

	double MET = myReader_Filter[iMC16]->at(iDSID)->missingET->p4.Pt()/1000.0;
	double Jet = (myReader_Filter[iMC16]->at(iDSID)->goodJets->size() > 0) ? myReader_Filter[iMC16]->at(iDSID)->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
	double dPhi = myReader_Filter[iMC16]->at(iDSID)->JetMetdPhiMin50;
	double weight = (myReader_Filter[iMC16]->at(iDSID)->weightXsec)*(myReader_Filter[iMC16]->at(iDSID)->weightMCweight)*(myReader_Filter[iMC16]->at(iDSID)->weightPileupReweighting);
	hSignal_EWK_Filter->Fill(MET, weight*vSFSig_EWK_Filter[iMC16].at(iDSID));
	if(Jet > 100.0 && dPhi > 1.0)
	  hSignal_EWK_Filter_WithJet->Fill(MET, weight*vSFSig_EWK_Filter[iMC16].at(iDSID));
      }// for iEntry
    }// for iDSID
  }// for iMC16

  hFrame = new TH2D("hFrame", ";E_{T}^{miss} [GeV];Number of events", 50, 0, 500, 100, 2e-1, 2e4);
  hFrame->Draw();
  hSignal_EWK_noMET->SetLineColor(kRed);
  hSignal_EWK_noMET_WithJet->SetLineColor(kRed);
  hSignal_EWK_noMET_WithJet->SetLineStyle(2);
  hSignal_EWK_Filter->SetLineColor(kBlue);
  hSignal_EWK_Filter_WithJet->SetLineColor(kBlue);
  hSignal_EWK_Filter_WithJet->SetLineStyle(2);
  
  hSignal_EWK_noMET->Draw("samehist");
  hSignal_EWK_noMET_WithJet->Draw("samehist");
  hSignal_EWK_Filter->Draw("samehist");
  hSignal_EWK_Filter_WithJet->Draw("samehist");

  ATLASLabel(0.5, 0.88, "Internal", kBlack);
  myText(0.5, 0.88-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  TLegend *tl = new TLegend(0.5, 0.6, 0.9, 0.8);
  tl->AddEntry(hSignal_EWK_noMET, "w/o MET filter", "l");
  tl->AddEntry(hSignal_EWK_Filter  , "w/  MET filter", "l");
  tl->AddEntry(hSignal_EWK_noMET_WithJet, "w/o MET filter, 1st jet p_{T}>100GeV, #Delta#phi>1.0", "l");
  tl->AddEntry(hSignal_EWK_Filter_WithJet  , "w/  MET filter, 1st jet p_{T}>100GeV, #Delta#phi>1.0", "l");
  tl->Draw("same");
  c0->SetLogy(true);

  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  tf = new TFile("plot_METFilter.root", "RECREATE");
  hSignal_EWK_Filter->Write();
  hSignal_EWK_Filter_WithJet->Write();
  hSignal_EWK_noMET->Write();
  hSignal_EWK_noMET_WithJet->Write();
  tf->Close();

  return 0;
}

