#include "mySignal.h"
namespace po = boost::program_options;

MySignal *MyEWK[nTau_EWK];
MySignal *MyHiggsino[nTau_Higgsino];
MySignal *MyStrong[nTau_Strong];
MyManager *myData;
HistManager *myHistData;
TFile *tfOut;
TCanvas *c0;
TCanvas *c1;
double GetTF(double pT, double Eta, double Phi, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1);
double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau);
TCut GetKinematicsCut(int iMETRegion, int iMETType);
double crystallBallIntegral(double* x, double *par);
double GetTrigEff(NtupleReader *myReader);
void SmearPt(TH1D *hHoge, double pt, double weight, bool);
int FillDataYield(void);
double CalcFracError(double vNume, double vDenom, double eNume, double eDenom);
int MakeGRatio(TGraphAsymmErrors *gRatio, TH1F *hPass, TH1F *hAll);

std::ofstream ofs;
std::ofstream ofsDetail;
std::string PDFName;
int iChain;
int EWKOff;
bool ReadData=true;

TFile *tfOldLimit;
TGraph *gRun2_ATLAS_EWK;
TGraph *gRun2_ATLAS_Higgsino;
TGraph *gRun2_ATLAS_Strong0p2;
TGraph *gRun2_ATLAS_Strong1p0;

TH1D *hTrackPt;
TH1D *hData_SR[nMETRegion];
TH1D *hData_HadCR;
TH1D *hData_FakeCR;
TH1D *hData_MuCR_EtClus20;
TH1D *hData_FakeCR_EtClus20;
TH1D *hData_EleCR_EtClus20;
TH1D *hData_HadCR_EtClus20;
double nData_SR_LowPt[nMETRegion];
double nData_SR_HighPt[nMETRegion];
double nData_SR_CaloSideBand[nMETRegion];
double nData_HadCR[nMETRegion];
double nData_FakeCR[nMETRegion];
double nData_SingleEleCR[nMETRegion];
double nData_SingleMuCR[nMETRegion];

TH1D *hData_CaloClus20_HadCR;
TH2D *hEWK_Contami_Frame;
TH2D *hEWK_Contami_SR_LowPt[nMETRegion];
TH2D *hEWK_Contami_SR_HighPt[nMETRegion];
TH2D *hEWK_Contami_SR_CaloSideBand[nMETRegion];
TH2D *hEWK_Contami_HadCR[nMETRegion];
TH2D *hEWK_Contami_FakeCR[nMETRegion];
TH2D *hEWK_Contami_SingleEleCR[nMETRegion];
TH2D *hEWK_Contami_SingleMuCR[nMETRegion];

TH2D *hHiggsino_Contami_SR_LowPt[nMETRegion];
TH2D *hHiggsino_Contami_SR_HighPt[nMETRegion];
TH2D *hHiggsino_Contami_SR_CaloSideBand[nMETRegion];
TH2D *hHiggsino_Contami_HadCR[nMETRegion];
TH2D *hHiggsino_Contami_FakeCR[nMETRegion];
TH2D *hHiggsino_Contami_SingleEleCR[nMETRegion];
TH2D *hHiggsino_Contami_SingleMuCR[nMETRegion];

TH2D *hStrong_Contami_SR_LowPt[nMETRegion];
TH2D *hStrong_Contami_SR_HighPt[nMETRegion];
TH2D *hStrong_Contami_SR_CaloSideBand[nMETRegion];
TH2D *hStrong_Contami_HadCR[nMETRegion];
TH2D *hStrong_Contami_FakeCR[nMETRegion];
TH2D *hStrong_Contami_SingleEleCR[nMETRegion];
TH2D *hStrong_Contami_SingleMuCR[nMETRegion];

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("iChain", po::value<int>()->default_value(0), "i Chain (0 ~ 19)")
    ("EWKOff", po::value<int>()->default_value(0), "0: ON, 1: OFF")
    ("SmearTruthPt", po::value<bool>()->default_value(true), "0: ON, 1: OFF")
    ("UseTriggerBit", po::value<bool>()->default_value(false), "0: ON, 1: OFF")
    ("DeadModule", po::value<bool>()->default_value(true), "0: ON, 1: OFF")
    ("output-file,o", po::value<std::string>()->default_value("mySignal"), "output file");
  
  po::positional_options_description p;

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  iChain = vm["iChain"].as<int>();
  EWKOff = vm["EWKOff"].as<int>();
  fSmearTruthPt  = vm["SmearTruthPt"].as<bool>();
  fUseTriggerBit = vm["UseTriggerBit"].as<bool>();
  fDeadModule    = vm["DeadModule"].as<bool>();
  std::cout << "**********   Settings   **********" << std::endl;
  std::cout << " * iChain = "<< iChain << std::endl;
  std::cout << " * EWKOff  = " << EWKOff << std::endl;
  std::cout << " * SmearTruthPt  = " << fSmearTruthPt << std::endl;
  std::cout << " * UseTriggerBit = " << fUseTriggerBit << std::endl;
  std::cout << " * DeadModule    = " << fDeadModule << std::endl;

  c1 = new TCanvas("c1", "c1", 1200, 400);
  c1->SetRightMargin(0.05);
  c1->SetLeftMargin(0.10);
  c1->SetTopMargin(0.05);
  c1->SetBottomMargin(0.15);

  c0 = new TCanvas("c0", "c0", 800, 600);
  c0->SetRightMargin(0.15);
  c0->SetLeftMargin(0.12);
  c0->SetTopMargin(0.05);
  c0->SetBottomMargin(0.15);

  hTrackPt = new TH1D("hTrackPt", ";track p_{T} [GeV]", nBinsMySignal, XBinsMySignal);
  hData_HadCR = new TH1D("hData_HadCR", ";track p_{T} [GeV]", nBinsMySignal, XBinsMySignal);
  hData_FakeCR = new TH1D("hData_FakeCR", ";track p_{T} [GeV]", nBinsMySignal, XBinsMySignal);
  hData_FakeCR_EtClus20 = new TH1D("hData_FakeCR_EtClus20", ";E_{T}^{topoclus20} [GeV]", 20, 0.0, 20.0);
  hData_MuCR_EtClus20  = new TH1D("hData_MuCR_EtClus20", ";E_{T}^{topoclus20} [GeV]",20, 0.0, 20.0);
  hData_HadCR_EtClus20 = new TH1D("hData_HadCR_EtClus20", ";E_{T}^{topoclus20} [GeV]", 50, 0.0, 50.0);
  hData_EleCR_EtClus20 = new TH1D("hData_EleCR_EtClus20", ";E_{T}^{topoclus20} [GeV]", 50, 0.0, 50.0);
  for(int iMET=0;iMET<nMETRegion;iMET++){
    hData_SR[iMET] = new TH1D(Form("hData_SR_%d", iMET), ";track p_{T} [GeV]", nBinsMySignal, XBinsMySignal);
    hData_SR[iMET]->SetBinErrorOption(TH1::kPoisson);
  }// for iMET

  tfOldLimit = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/OldLimit.root", "READ");
  gROOT->cd();
  gRun2_ATLAS_EWK = (TGraph *)tfOldLimit->Get("EWK_36_obs");
  gRun2_ATLAS_EWK->SetLineStyle(5);
  gRun2_ATLAS_EWK->SetLineWidth(3);
  gRun2_ATLAS_EWK->SetLineColor(EColor::kViolet);
  gRun2_ATLAS_Higgsino = (TGraph *)tfOldLimit->Get("Higgsino_36_obs");
  gRun2_ATLAS_Higgsino->SetLineStyle(5);
  gRun2_ATLAS_Higgsino->SetLineWidth(3);
  gRun2_ATLAS_Higgsino->SetLineColor(EColor::kViolet);
  gRun2_ATLAS_Strong1p0 = (TGraph *)tfOldLimit->Get("Strong1p0_36_obs");
  gRun2_ATLAS_Strong1p0->SetLineStyle(5);
  gRun2_ATLAS_Strong1p0->SetLineWidth(3);
  gRun2_ATLAS_Strong1p0->SetLineColor(EColor::kViolet);

  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/tmp/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  hLumiHistData15 = (TH1F *)tfInput->Get("lumi_histo_data15");
  hLumiHistData16 = (TH1F *)tfInput->Get("lumi_histo_data16");
  hLumiHistData17 = (TH1F *)tfInput->Get("lumi_histo_data17");
  hLumiHistData18 = (TH1F *)tfInput->Get("lumi_histo_data18");
  hEleBG_TF_PtEta    = (TH2D *)tfInput->Get("hEleBG_TF_PtEta");
  hEleBG_TF_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_InvPtEta");
  hEleBG_TF_CaloIso_4to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_PtEta");
  hEleBG_TF_CaloIso_4to123_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_PtEta");
  hEleBG_TF_CaloIso_432to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_PtEta");
  hEleBG_TF_CaloIso_432to1_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_PtEta");
  hEleBG_TF_CaloIso_432to01_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_PtEta");
  hEleBG_TF_CaloIso_4to0_InvPtEta   = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_InvPtEta");
  hEleBG_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_InvPtEta");
  hEleBG_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_InvPtEta");
  hEleBG_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_InvPtEta");
  hEleBG_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_InvPtEta");
  hMuBG_TF_1Bin     = (TH1D *)tfInput->Get("hMuBG_TF_1Bin");
  hMuBG_TF_PtEta    = (TH2D *)tfInput->Get("hMuBG_TF_PtEta");
  hMuBG_TF_InvPtEta = (TH2D *)tfInput->Get("hMuBG_TF_InvPtEta");
  hMSBG_TF_PhiEta   = (TH2D *)tfInput->Get("hMSBG_TF_PhiEta");
  P_caloveto05_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto05_hadron_StdTrk");
  P_caloveto10_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto10_hadron_StdTrk");
  P_caloveto05_10_hadron_StdTrk   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_StdTrk");  
  P_caloveto05_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto05_hadron_Trklet");     
  P_caloveto10_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto10_hadron_Trklet");     
  P_caloveto05_10_hadron_Trklet   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_Trklet");
  P_caloveto05_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto05_electron_StdTrk");   
  P_caloveto10_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto10_electron_StdTrk");   
  P_caloveto05_10_electron_StdTrk = (TH1D *)tfInput->Get("P_caloveto05_10_electron_StdTrk");
  P_caloveto05_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto05_electron_Trklet");   
  P_caloveto10_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto10_electron_Trklet");   
  P_caloveto05_10_electron_Trklet = (TH1D *)tfInput->Get("P_caloveto05_10_electron_Trklet");

  for(int iMC16=0;iMC16<nMC16;iMC16++){
    gSF_DeadModule[iMC16] = (TGraphAsymmErrors *)tfInput->Get(Form("SF_DeadModule_%s", MCName[iMC16].c_str()));
  }
  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");
  
  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");

  hAxisData15 = (TH1F *)hLumiHistData15->Clone("hAxisData15"); hAxisData15->Reset();
  hAxisData16 = (TH1F *)hLumiHistData16->Clone("hAxisData16"); hAxisData16->Reset();
  hAxisData17 = (TH1F *)hLumiHistData17->Clone("hAxisData17"); hAxisData17->Reset();
  hAxisData18 = (TH1F *)hLumiHistData18->Clone("hAxisData18"); hAxisData18->Reset();

  hAxisData15->SetYTitle("Number of events [1/pb^{-1}]");  hAxisData15->GetYaxis()->SetTitleOffset(0.5);
  hAxisData16->SetYTitle("Number of events [1/pb^{-1}]");  hAxisData16->GetYaxis()->SetTitleOffset(0.5);
  hAxisData17->SetYTitle("Number of events [1/pb^{-1}]");  hAxisData17->GetYaxis()->SetTitleOffset(0.5); hAxisData17->GetXaxis()->SetLabelSize(0.04);
  hAxisData18->SetYTitle("Number of events [1/pb^{-1}]");  hAxisData18->GetYaxis()->SetTitleOffset(0.5); hAxisData18->GetXaxis()->SetLabelSize(0.04);

  for(int iMET=0;iMET<nMETRegion;iMET++){
    hEWK_Contami_SR_LowPt[iMET] = new TH2D(Form("hEWK_Contami_SR_LowPt_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);
    hEWK_Contami_SR_HighPt[iMET] = new TH2D(Form("hEWK_Contami_SR_HighPt_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);
    hEWK_Contami_SR_CaloSideBand[iMET] = new TH2D(Form("hEWK_Contami_SR_CaloSideBand_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);
    hEWK_Contami_HadCR[iMET] = new TH2D(Form("hEWK_Contami_HadCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);
    hEWK_Contami_FakeCR[iMET] = new TH2D(Form("hEWK_Contami_FakeCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);
    hEWK_Contami_SingleEleCR[iMET] = new TH2D(Form("hEWK_Contami_SingleEleCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);
    hEWK_Contami_SingleMuCR[iMET] = new TH2D(Form("hEWK_Contami_SingleMuCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 33, TauBin);

    hHiggsino_Contami_SR_LowPt[iMET] = new TH2D(Form("hHiggsino_Contami_SR_LowPt_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);
    hHiggsino_Contami_SR_HighPt[iMET] = new TH2D(Form("hHiggsino_Contami_SR_HighPt_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);
    hHiggsino_Contami_SR_CaloSideBand[iMET] = new TH2D(Form("hHiggsino_Contami_SR_CaloSideBand_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);
    hHiggsino_Contami_HadCR[iMET] = new TH2D(Form("hHiggsino_Contami_HadCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);
    hHiggsino_Contami_FakeCR[iMET] = new TH2D(Form("hHiggsino_Contami_FakeCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);
    hHiggsino_Contami_SingleEleCR[iMET] = new TH2D(Form("hHiggsino_Contami_SingleEleCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);
    hHiggsino_Contami_SingleMuCR[iMET] = new TH2D(Form("hHiggsino_Contami_SingleMuCR_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 110, 310, 33, TauBin);

    hStrong_Contami_SR_LowPt[iMET] = new TH2D(Form("hStrong_Contami_SR_LowPt_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
    hStrong_Contami_SR_HighPt[iMET] = new TH2D(Form("hStrong_Contami_SR_HighPt_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
    hStrong_Contami_SR_CaloSideBand[iMET] = new TH2D(Form("hStrong_Contami_SR_CaloSideBand_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
    hStrong_Contami_HadCR[iMET] = new TH2D(Form("hStrong_Contami_HadCR_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
    hStrong_Contami_FakeCR[iMET] = new TH2D(Form("hStrong_Contami_FakeCR_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
    hStrong_Contami_SingleEleCR[iMET] = new TH2D(Form("hStrong_Contami_SingleEleCR_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
    hStrong_Contami_SingleMuCR[iMET] = new TH2D(Form("hStrong_Contami_SingleMuCR_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 20, 450, 2450, 24, 50, 2450);
  }// for iMET

  c0->Print(Form("out_mySignal_Chain%d.pdf[", iChain), "pdf");

  if(ReadData){
    //myData = new MyManager("/data3/tkaji/dev_MyProject/files/data15-18_13TeV.root", "Data 2015-2018");
    myData = new MyManager("/data3/tkaji/myAna/Data/data15-18_13TeV.root", "Data 2015-2018");
    myData->LoadMyFile();
    myHistData = myData->myHist_Old4L;
    FillDataYield();
  }
  std::cout << "EWK" << std::endl;
  for(int iTau=0;iTau<nTau_EWK;iTau++){
    MyEWK[iTau] = new MySignal("EWK", Tau_EWK[iTau]);
    MyEWK[iTau]->FillPtHist();
    MyEWK[iTau]->FillYield();
  }// for iTau

  std::cout << "Higgsino" << std::endl;
  for(int iTau=0;iTau<nTau_Higgsino;iTau++){
    MyHiggsino[iTau] = new MySignal("Higgsino", Tau_Higgsino[iTau]);
    MyHiggsino[iTau]->FillPtHist();
    MyHiggsino[iTau]->FillYield();
  }// for iTau

  std::cout << "Strong" << std::endl;
  for(int iTau=0;iTau<nTau_Strong;iTau++){
    MyStrong[iTau] = new MySignal("Strong", Tau_Strong[iTau]);
    MyStrong[iTau]->FillPtHist();
    MyStrong[iTau]->FillYield();
  }// for iTau

  // EWK
  ofs.open(Form("List_EWK_Chain%d.txt", iChain));
  for(int iTau=0;iTau<nTau_EWK;iTau++){
    double Uncert = 0.16;
    for(int iSignal=0;iSignal<(MyEWK[iTau]->nSignal);iSignal++){
      double Val0 = MyEWK[iTau]->hPt[0].at(iSignal)->Integral(iPtBin+1, nBinsMySignal+1);
      ofs << std::setw(10) << Form("%.1lf", MyEWK[iTau]->CMass.at(iSignal)) << std::setw(10) << Form("%.3lf", MyEWK[iTau]->TauVal)
	  << std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;

      for(int iFrom=0;iFrom<(MyEWK[iTau]->nReweight);iFrom++){
	double Val1 = MyEWK[iTau]->hPt_Reweighted[0].at(iSignal).at(iFrom)->Integral(iPtBin+1, nBinsMySignal+1);
	ofs << std::setw(10) << Form("%.1lf", MyEWK[iTau]->CMass.at(iSignal)) << std::setw(10) << Form("%.3lf", MyEWK[iTau]->TauReweight.at(iFrom))
	    << std::setw(20) << Form("%.4lf", Val1) << std::setw(20) << Form("%.4lf", Val1*Uncert) << std::endl;
      }// for iFrom
    }// for iSignal
  }//for iTau
  ofs.close();

  ofs.open(Form("PtScan_EWK_Chain%d.txt", iChain));
  for(int iSignal=0;iSignal<(MyEWK[0]->nSignal);iSignal++){
    for(int iPt=0;iPt<nPtScan;iPt++){
      double Uncert = 0.16;
      ofs << std::setw(10) << Form("%.1lf", MyEWK[0]->CMass.at(iSignal)) << std::setw(10) << iPt
	  << std::setw(20) << Form("%.4lf", MyEWK[0]->nSR_PtScan[iPt][0].at(iSignal)) << std::setw(20) << Form("%.4lf", Uncert*MyEWK[0]->nSR_PtScan[iPt][0].at(iSignal)) << std::endl;
    }// for iPt
  }// for iSignal
  ofs.close();

  // Higgsino
  ofs.open(Form("List_Higgsino_Chain%d.txt", iChain));
  for(int iTau=0;iTau<nTau_Higgsino;iTau++){
    double Uncert = 0.16;
    for(int iSignal=0;iSignal<(MyHiggsino[iTau]->nSignal);iSignal++){
      double Val0 = MyHiggsino[iTau]->hPt[0].at(iSignal)->Integral(iPtBin+1, nBinsMySignal+1);
      ofs << std::setw(10) << Form("%.1lf", MyHiggsino[iTau]->CMass.at(iSignal)) << std::setw(10) << Form("%.3lf", MyHiggsino[iTau]->TauVal)
	  << std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;

      for(int iFrom=0;iFrom<(MyHiggsino[iTau]->nReweight);iFrom++){
	double Val1 = MyHiggsino[iTau]->hPt_Reweighted[0].at(iSignal).at(iFrom)->Integral(iPtBin+1, nBinsMySignal+1);
	ofs << std::setw(10) << Form("%.1lf", MyHiggsino[iTau]->CMass.at(iSignal)) << std::setw(10) << Form("%.3lf", MyHiggsino[iTau]->TauReweight.at(iFrom))
	    << std::setw(20) << Form("%.4lf", Val1) << std::setw(20) << Form("%.4lf", Val1*Uncert) << std::endl;
      }// for iFrom
    }// for iSignal
  }//for iTau
  ofs.close();

  ofs.open(Form("PtScan_Higgsino_Chain%d.txt", iChain));
  for(int iSignal=0;iSignal<(MyHiggsino[0]->nSignal);iSignal++){
    for(int iPt=0;iPt<nPtScan;iPt++){
      double Uncert = 0.16;
      ofs << std::setw(10) << Form("%.1lf", MyHiggsino[0]->CMass.at(iSignal)) << std::setw(10) << iPt
	  << std::setw(20) << Form("%.4lf", MyHiggsino[0]->nSR_PtScan[iPt][0].at(iSignal)) << std::setw(20) << Form("%.4lf", Uncert*MyHiggsino[0]->nSR_PtScan[iPt][0].at(iSignal)) << std::endl;
    }// for iPt
  }// for iSignal
  ofs.close();

  // Strong
  ofs.open(Form("List_Strong1p0_Chain%d.txt", iChain));
  for(int iSignal=0;iSignal<(MyStrong[1]->nSignal);iSignal++){
    double Uncert = 0.14;
    double Val0 = MyStrong[1]->hPt[0].at(iSignal)->Integral(iPtBin+1, nBinsMySignal+1);
    ofs << std::setw(10) << Form("%.1lf", MyStrong[1]->GMass.at(iSignal)) << std::setw(10) << Form("%.1lf", MyStrong[1]->CMass.at(iSignal))
	<< std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;
  }//for iSignal
  ofs.close();
  ofs.open(Form("List_Strong0p2_Chain%d.txt", iChain));
  for(int iSignal=0;iSignal<(MyStrong[1]->nSignal);iSignal++){
    double Uncert = 0.14;
    double Val0 = MyStrong[1]->hPt_Reweighted[0].at(iSignal).at(0)->Integral(iPtBin+1, nBinsMySignal+1);
    ofs << std::setw(10) << Form("%.1lf", MyStrong[1]->GMass.at(iSignal)) << std::setw(10) << Form("%.1lf", MyStrong[1]->CMass.at(iSignal))
	<< std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;
  }//for iSignal
  ofs.close();

  // EWK Contamination
  for(int iMET=0;iMET<nMETRegion;iMET++){
    for(int iTau=0;iTau<nTau_EWK;iTau++){
      for(int iSignal = 0;iSignal<(MyEWK[iTau]->nSignal);iSignal++){
	hEWK_Contami_SR_LowPt[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nSR_LowPt[iMET].at(iSignal))/nData_SR_LowPt[iMET]);
	if(iMET==0){
	  hEWK_Contami_SR_HighPt[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nSR_HighPt[iMET].at(iSignal))/3.26971);
	}else{
	  hEWK_Contami_SR_HighPt[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nSR_HighPt[iMET].at(iSignal))/nData_SR_HighPt[iMET]);
	}
	hEWK_Contami_SR_CaloSideBand[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nSR_CaloSideBand[iMET].at(iSignal))/nData_SR_CaloSideBand[iMET]);

	hEWK_Contami_HadCR[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nHadCR[iMET].at(iSignal))/nData_HadCR[iMET]);
	hEWK_Contami_FakeCR[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nFakeCR[iMET].at(iSignal))/nData_FakeCR[iMET]);
	hEWK_Contami_SingleEleCR[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nSingleEleCR[iMET].at(iSignal))/nData_SingleEleCR[iMET]);
	hEWK_Contami_SingleMuCR[iMET]->Fill(MyEWK[iTau]->CMass.at(iSignal), MyEWK[iTau]->TauVal, (MyEWK[iTau]->nSingleMuCR[iMET].at(iSignal))/nData_SingleMuCR[iMET]);
      }// for iSignal
    }// for iTau
  }// for iMET

  c0->SetLogy(true);
  c0->SetLogz(true);
  for(int iMET=0;iMET<nMETRegion;iMET++){
    double XPos = 0.2;
    double YPos = 0.2;
    hEWK_Contami_SR_LowPt[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_SR_LowPt[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_SR_LowPt[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination low-pT, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_SR_LowPt_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hEWK_Contami_SR_HighPt[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_SR_HighPt[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_SR_HighPt[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination high-pT, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_SR_HighPt_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hEWK_Contami_SR_CaloSideBand[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_SR_CaloSideBand[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_SR_CaloSideBand[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination CaloSideBand, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_VR_CaloSideBand_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hEWK_Contami_HadCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_HadCR[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_HadCR[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination HadCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_HadCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hEWK_Contami_FakeCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_FakeCR[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_FakeCR[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination FakeCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_FakeCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");

    hEWK_Contami_SingleEleCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_SingleEleCR[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_SingleEleCR[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination SingleEleCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_EleCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");

    hEWK_Contami_SingleMuCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hEWK_Contami_SingleMuCR[iMET]->GetYaxis()->SetTitleOffset(1.0);
    hEWK_Contami_SingleMuCR[iMET]->Draw("colz0");
    gRun2_ATLAS_EWK->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination SingleMuCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/EWK_Contami_MuCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");    
  }

  // Strong Contamination
  for(int iMET=0;iMET<nMETRegion;iMET++){
    for(int iSignal=0;iSignal<(MyStrong[1]->nSignal);iSignal++){
	hStrong_Contami_SR_LowPt[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nSR_LowPt[iMET].at(iSignal))/nData_SR_LowPt[iMET]);
	if(iMET==0){
	  hStrong_Contami_SR_HighPt[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nSR_HighPt[iMET].at(iSignal))/0.660929);
	}else{
	  hStrong_Contami_SR_HighPt[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nSR_HighPt[iMET].at(iSignal))/nData_SR_HighPt[iMET]);
	}
	hStrong_Contami_SR_CaloSideBand[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nSR_CaloSideBand[iMET].at(iSignal))/nData_SR_CaloSideBand[iMET]);

	hStrong_Contami_HadCR[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nHadCR[iMET].at(iSignal))/nData_HadCR[iMET]);
	hStrong_Contami_FakeCR[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nFakeCR[iMET].at(iSignal))/nData_FakeCR[iMET]);
	hStrong_Contami_SingleEleCR[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nSingleEleCR[iMET].at(iSignal))/nData_SingleEleCR[iMET]);
	hStrong_Contami_SingleMuCR[iMET]->Fill(MyStrong[1]->GMass.at(iSignal), MyStrong[1]->CMass.at(iSignal), (MyStrong[1]->nSingleMuCR[iMET].at(iSignal))/nData_SingleMuCR[iMET]);      
    }// for iSignal
  }// for iMET

  c0->SetLogy(false);
  c0->SetLogz(true);
  for(int iMET=0;iMET<nMETRegion;iMET++){
    double XPos = 0.2;
    double YPos = 0.9;
    hStrong_Contami_SR_LowPt[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_SR_LowPt[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_SR_LowPt[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination low-pT, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_SR_LowPt_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hStrong_Contami_SR_HighPt[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_SR_HighPt[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_SR_HighPt[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination high-pT, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_SR_HighPt_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hStrong_Contami_SR_CaloSideBand[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_SR_CaloSideBand[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_SR_CaloSideBand[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination CaloSideBand, %s", RegName[iMET].c_str()));
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_VR_CaloSideBand_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");

    hStrong_Contami_HadCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_HadCR[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_HadCR[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination HadCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_HadCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");

    hStrong_Contami_FakeCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_FakeCR[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_FakeCR[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination FakeCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_FakeCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");

    hStrong_Contami_SingleEleCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_SingleEleCR[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_SingleEleCR[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination SingleEleCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_EleCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");

    hStrong_Contami_SingleMuCR[iMET]->GetZaxis()->SetRangeUser(1e-3, 1.0);
    hStrong_Contami_SingleMuCR[iMET]->GetYaxis()->SetTitleOffset(1.2);
    hStrong_Contami_SingleMuCR[iMET]->Draw("colz0");
    gRun2_ATLAS_Strong1p0->Draw("sameC");
    myText(XPos, YPos, kBlack, Form("signal contamination SingleMuCR, %s", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Strong_Contami_MuCR_Chain%d_METReg%d.pdf", iChain, iMET), "pdf");
    c0->Print(Form("out_mySignal_Chain%d.pdf", iChain), "pdf");    
  }
  c0->Print(Form("out_mySignal_Chain%d.pdf]", iChain), "pdf");

  tfOut = new TFile(Form("out_mySignal_Chain%d.root", iChain), "RECREATE");
  for(int iMET=0;iMET<nMETRegion;iMET++){
    hData_SR[iMET]->Write();
  }//

  hData_HadCR->Write();
  hData_FakeCR->Write();
  hData_HadCR_EtClus20->Write();
  hData_EleCR_EtClus20->Write();
  hData_MuCR_EtClus20->Write();
  hData_FakeCR_EtClus20->Write();

  tfOut->mkdir("Period");
  tfOut->cd("Period");
  std::cout << " Write SR" << std::endl;
  c1->cd();
  c1->Print(Form("out_mySignal_Period_Chain%d.pdf[", iChain), "pdf");
  for(int iMET=0;iMET<nMETRegion;iMET++){
    c1->Clear();
    hAxisData15->GetYaxis()->SetRangeUser(0.0, gData15_Period_SR[iMET]->GetYaxis()->GetXmax());
    hAxisData15->Draw();
    gData15_Period_SR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("SR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_Period15.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData16->GetYaxis()->SetRangeUser(0.0, gData16_Period_SR[iMET]->GetYaxis()->GetXmax());
    hAxisData16->Draw();
    gData16_Period_SR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("SR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_Period16.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData17->GetYaxis()->SetRangeUser(0.0, gData17_Period_SR[iMET]->GetYaxis()->GetXmax());
    hAxisData17->Draw();
    gData17_Period_SR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("SR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_Period17.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData18->GetYaxis()->SetRangeUser(0.0, gData18_Period_SR[iMET]->GetYaxis()->GetXmax());
    hAxisData18->Draw();
    gData18_Period_SR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("SR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_Period18.pdf", iChain, iMET), "pdf");

    gData15_Period_SR[iMET]->Write();
    gData16_Period_SR[iMET]->Write();
    gData17_Period_SR[iMET]->Write();
    gData18_Period_SR[iMET]->Write();
  }

  std::cout << " Write VR" << std::endl;
  for(int iMET=0;iMET<nMETRegion;iMET++){
    c1->Clear();
    hAxisData15->GetYaxis()->SetRangeUser(0.0, gData15_Period_VR[iMET]->GetYaxis()->GetXmax());
    hAxisData15->Draw();
    gData15_Period_VR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("VR_{calo}, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_Period15.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData16->GetYaxis()->SetRangeUser(0.0, gData16_Period_VR[iMET]->GetYaxis()->GetXmax());
    hAxisData16->Draw();
    gData16_Period_VR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("VR_{calo}, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_Period16.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData17->GetYaxis()->SetRangeUser(0.0, gData17_Period_VR[iMET]->GetYaxis()->GetXmax());
    hAxisData17->SetTitle(Form("VR, %s", RegName[iMET].c_str()));
    hAxisData17->Draw();
    gData17_Period_VR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("VR_{calo}, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_Period17.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData18->GetYaxis()->SetRangeUser(0.0, gData18_Period_VR[iMET]->GetYaxis()->GetXmax());
    hAxisData18->SetTitle(Form("VR, %s", RegName[iMET].c_str()));
    hAxisData18->Draw();
    gData18_Period_VR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("VR_{calo}, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_Period18.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

   gData15_Period_VR[iMET]->Write();
   gData16_Period_VR[iMET]->Write();
   gData17_Period_VR[iMET]->Write();
   gData18_Period_VR[iMET]->Write();
  }

  std::cout << " Write Fake CR" << std::endl;
  c1->Clear();
  hAxisData15->GetYaxis()->SetRangeUser(0.0, gData15_Period_FakeCR->GetYaxis()->GetXmax());
  hAxisData15->Draw();
  gData15_Period_FakeCR->Draw("samePL");
  myText(0.2, 0.9, kBlack, Form("FakeCR"));
  c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_Period15.pdf", iChain), "pdf");
  c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

  c1->Clear();
  hAxisData16->GetYaxis()->SetRangeUser(0.0, gData16_Period_FakeCR->GetYaxis()->GetXmax());
  hAxisData16->Draw();
  gData16_Period_FakeCR->Draw("samePL");
  myText(0.2, 0.9, kBlack, Form("FakeCR"));
  c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_Period16.pdf", iChain), "pdf");
  c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

  c1->Clear();
  hAxisData17->GetYaxis()->SetRangeUser(0.0, gData17_Period_FakeCR->GetYaxis()->GetXmax());
  hAxisData17->Draw();
  gData17_Period_FakeCR->Draw("samePL");
  myText(0.2, 0.9, kBlack, Form("FakeCR"));
  c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_Period17.pdf", iChain), "pdf");
  c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

  c1->Clear();
  hAxisData18->GetYaxis()->SetRangeUser(0.0, gData18_Period_FakeCR->GetYaxis()->GetXmax());
  hAxisData18->Draw();
  gData18_Period_FakeCR->Draw("samePL");
  myText(0.2, 0.9, kBlack, Form("FakeCR"));
  c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_Period18.pdf", iChain), "pdf");
  c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

  gData15_Period_FakeCR->Write();
  gData16_Period_FakeCR->Write();
  gData17_Period_FakeCR->Write();
  gData18_Period_FakeCR->Write();
    
  for(int iMET=0;iMET<nMETRegion;iMET++){
    c1->Clear();
    hAxisData15->GetYaxis()->SetRangeUser(0.0, gData15_Period_HadCR[iMET]->GetYaxis()->GetXmax());
    hAxisData15->Draw();
    gData15_Period_HadCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("HadCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_Period15.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData16->GetYaxis()->SetRangeUser(0.0, gData16_Period_HadCR[iMET]->GetYaxis()->GetXmax());
    hAxisData16->Draw();
    gData16_Period_HadCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("HadCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_Period16.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData17->GetYaxis()->SetRangeUser(0.0, gData17_Period_HadCR[iMET]->GetYaxis()->GetXmax());
    hAxisData17->Draw();
    gData17_Period_HadCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("HadCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_Period17.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData18->GetYaxis()->SetRangeUser(0.0, gData18_Period_HadCR[iMET]->GetYaxis()->GetXmax());
    hAxisData18->Draw();
    gData18_Period_HadCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("HadCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_Period18.pdf", iChain, iMET), "pdf");

   gData15_Period_HadCR[iMET]->Write();
   gData16_Period_HadCR[iMET]->Write();
   gData17_Period_HadCR[iMET]->Write();
   gData18_Period_HadCR[iMET]->Write();
  }

  for(int iMET=0;iMET<nMETRegion;iMET++){
    c1->Clear();
    hAxisData15->GetYaxis()->SetRangeUser(0.0, gData15_Period_EleCR[iMET]->GetYaxis()->GetXmax());
    hAxisData15->Draw();
    gData15_Period_EleCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("EleCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_Period15.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData16->GetYaxis()->SetRangeUser(0.0, gData16_Period_EleCR[iMET]->GetYaxis()->GetXmax());
    hAxisData16->Draw();
    gData16_Period_EleCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("EleCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_Period16.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData17->GetYaxis()->SetRangeUser(0.0, gData17_Period_EleCR[iMET]->GetYaxis()->GetXmax());
    hAxisData17->Draw();
    gData17_Period_EleCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("EleCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_Period17.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData18->GetYaxis()->SetRangeUser(0.0, gData18_Period_EleCR[iMET]->GetYaxis()->GetXmax());
    hAxisData18->Draw();
    gData18_Period_EleCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("EleCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_Period18.pdf", iChain, iMET), "pdf");

   gData15_Period_EleCR[iMET]->Write();
   gData16_Period_EleCR[iMET]->Write();
   gData17_Period_EleCR[iMET]->Write();
   gData18_Period_EleCR[iMET]->Write();
  }

  for(int iMET=0;iMET<nMETRegion;iMET++){
    c1->Clear();
    hAxisData15->GetYaxis()->SetRangeUser(0.0, gData15_Period_MuCR[iMET]->GetYaxis()->GetXmax());
    hAxisData15->Draw();
    gData15_Period_MuCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("MuCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_Period15.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData16->GetYaxis()->SetRangeUser(0.0, gData16_Period_MuCR[iMET]->GetYaxis()->GetXmax());
    hAxisData16->Draw();
    gData16_Period_MuCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("MuCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_Period16.pdf", iChain, iMET), "pdf");
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");

    c1->Clear();
    hAxisData17->GetYaxis()->SetRangeUser(0.0, gData17_Period_MuCR[iMET]->GetYaxis()->GetXmax());
    hAxisData17->Draw();
    gData17_Period_MuCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("MuCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_Period17.pdf", iChain, iMET), "pdf");

    c1->Clear();
    hAxisData18->GetYaxis()->SetRangeUser(0.0, gData18_Period_MuCR[iMET]->GetYaxis()->GetXmax());
    hAxisData18->Draw();
    gData18_Period_MuCR[iMET]->Draw("samePL");
    myText(0.2, 0.9, kBlack, Form("MuCR, %s", RegName[iMET].c_str()));
    c1->Print(Form("out_mySignal_Period_Chain%d.pdf", iChain), "pdf");
    c1->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_Period18.pdf", iChain, iMET), "pdf");

    gData15_Period_MuCR[iMET]->Write();
    gData16_Period_MuCR[iMET]->Write();
    gData17_Period_MuCR[iMET]->Write();
    gData18_Period_MuCR[iMET]->Write();
  }
  c1->Print(Form("out_mySignal_Period_Chain%d.pdf]", iChain), "pdf");
  tfOut->cd("../");

  tfOut->mkdir("EWK");
  tfOut->cd("EWK");
  for(int iTau=0;iTau<nTau_EWK;iTau++){
    for(int iSignal = 0;iSignal<(MyEWK[iTau]->nSignal);iSignal++){
      for(int iMET=0;iMET<nMETRegion;iMET++){    
	MyEWK[iTau]->hPt[iMET].at(iSignal)->Write();
      }
    }// Signal
  }// Tau
  tfOut->cd("../");

  tfOut->mkdir("Higgsino");
  tfOut->cd("Higgsino");
  for(int iTau=0;iTau<nTau_Higgsino;iTau++){
    for(int iSignal = 0;iSignal<(MyHiggsino[iTau]->nSignal);iSignal++){
      for(int iMET=0;iMET<nMETRegion;iMET++){    
	MyHiggsino[iTau]->hPt[iMET].at(iSignal)->Write();
      }
    }// Signal
  }// Tau
  tfOut->cd("../");

  tfOut->mkdir("Strong");
  tfOut->cd("Strong");
  for(int iTau=0;iTau<nTau_Strong;iTau++){
    for(int iSignal = 0;iSignal<(MyStrong[iTau]->nSignal);iSignal++){
      for(int iMET=0;iMET<nMETRegion;iMET++){    
	MyStrong[iTau]->hPt[iMET].at(iSignal)->Write();
	for(int iFrom=0;iFrom<MyStrong[iTau]->nReweight;iFrom++){
	  MyStrong[iTau]->hPt_Reweighted[iMET].at(iSignal).at(iFrom)->Write();
	}
      }
    }// Signal
  }// Tau
  tfOut->cd("../");

  for(int iMET=0;iMET<nMETRegion;iMET++){    
    hEWK_Contami_SR_LowPt[iMET]->Write();
    hEWK_Contami_SR_HighPt[iMET]->Write();
    hEWK_Contami_SR_CaloSideBand[iMET]->Write();
    hEWK_Contami_HadCR[iMET]->Write();
    hEWK_Contami_FakeCR[iMET]->Write();
    hEWK_Contami_SingleEleCR[iMET]->Write();
    hEWK_Contami_SingleMuCR[iMET]->Write();
  }
  tfOut->Close();

  std::cout << "mySignal : succeeded to run " << std::endl;

  return 0;
}

int MakeGRatio(TGraphAsymmErrors *gRatio, TH1F *hPass, TH1F *hAll){
  int nBinX = hAll->GetNbinsX();
  /*
  double *xVal = new double[nBinX];
  double *yVal = new double[nBinX];
  double *xErrL = new double[nBinX];
  double *xErrH = new double[nBinX];
  double *yErrL = new double[nBinX];
  double *yErrH = new double[nBinX];
  */


  double xVal ;
  double yVal ;
  double xErrL;
  double xErrH;
  double yErrL;
  double yErrH;

  //gRatio = new TGraphAsymmErrors();
  //gRatio->SetName("gRatio");

  for(int iBin=0;iBin<nBinX;iBin++){
    double vNume  = hPass->GetBinContent(iBin+1);
    double eNumeL = hPass->GetBinErrorLow(iBin+1);
    double eNumeH = hPass->GetBinErrorUp(iBin+1);

    double vDenom  = hAll->GetBinContent(iBin+1);
    double eDenomL = hAll->GetBinErrorLow(iBin+1);
    double eDenomH = hAll->GetBinErrorUp(iBin+1);

    xVal = iBin + 0.5;
    if(vDenom!=0)
      yVal = vNume/vDenom;
    else
      yVal = 0.0;
    
    xErrL = 0.5;
    xErrH = 0.5;

    yErrL = CalcFracError(vNume, vDenom, eNumeL, eDenomL);
    yErrH = CalcFracError(vNume, vDenom, eNumeH, eDenomH);    
    gRatio->SetPoint(iBin, xVal, yVal);
    gRatio->SetPointError(iBin, xErrL, xErrH, yErrL, yErrH);
    /*
    xVal[iBin] = iBin + 0.5;
    if(vDenom!=0)
      yVal[iBin] = vNume/vDenom;
    else
      yVal[iBin] = 0.0;
    
    xErrL[iBin] = 0.5;
    xErrH[iBin] = 0.5;

    yErrL[iBin] = CalcFracError(vNume, vDenom, eNumeL, eDenomL);
    yErrH[iBin] = CalcFracError(vNume, vDenom, eNumeH, eDenomH);    
    gRatio->SetPoint(iBin, xVal[iBin], yVal[iBin]);
    gRatio->SetPointError(iBin, xErrL[iBin], xErrH[iBin], yErrL[iBin], yErrH[iBin]);
    */
  }// for iBin

  //  gRatio = new TGraphAsymmErrors(nBinX, xVal, yVal, xErrL, xErrH, yErrL, yErrH);

  return 0;
}

int MySignal::FillYield(void){
  std::cout << "MySignal::FillYield" << std::endl;
  TH1D *hCount = new TH1D("hCount", "", 1, 0, 10);
  for(int iSignal=0;iSignal<nSignal;iSignal++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      double iSR_CaloSideBand = 0.000001;
      double iHadCR = 0.000001;
      double iFakeCR = 0.000001;
      double iSingleEleCR = 0.000001;
      double iSingleMuCR = 0.000001;

      nSR_LowPt[iMET].push_back(hPt[iMET].at(iSignal)->Integral(1, iPtBin));
      nSR_HighPt[iMET].push_back(hPt[iMET].at(iSignal)->Integral(iPtBin+1, nBinsMySignal+1));
      for(int iPt=0;iPt<nPtScan;iPt++){
	nSR_PtScan[iPt][iMET].push_back(hPt[iMET].at(iSignal)->Integral(1+iPt, nBinsMySignal+1));
      }

      for(unsigned int iDSID=0;iDSID<vDSID.at(iSignal).size();iDSID++){
	for(int iMC16=0;iMC16<nRunMC16;iMC16++){	  
	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 0) && CaloVETO_1)*MCWeight);
	  iSR_CaloSideBand += hCount->Integral()*SF[iMC16].at(iSignal).at(iDSID);

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_hadCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 0))*MCWeight);
	  iHadCR += hCount->Integral()*SF[iMC16].at(iSignal).at(iDSID);

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_fakeCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 0) && FakeD0_CR && CaloVETO_0)*MCWeight);
	  iFakeCR += hCount->Integral()*SF[iMC16].at(iSignal).at(iDSID);

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_singleEleCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 1))*MCWeight);
	  iSingleEleCR += hCount->Integral()*SF[iMC16].at(iSignal).at(iDSID);

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_singleMuCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 2) && CaloVETO_0)*MCWeight);
	  iSingleEleCR += hCount->Integral()*SF[iMC16].at(iSignal).at(iDSID);
	}// for iMC16
      }// for iDSID

      nSR_CaloSideBand[iMET].push_back(iSR_CaloSideBand);
      nHadCR[iMET].push_back(iHadCR);
      nFakeCR[iMET].push_back(iFakeCR);
      nSingleEleCR[iMET].push_back(iSingleEleCR);
      nSingleMuCR[iMET].push_back(iSingleMuCR);      
    }// for iMET
  }// for iSignal
  delete hCount; hCount=NULL;
  
  return 0;
}

int MySignal::FillPtHist(void){
  std::cout << "MySignal::FillPtHist" << std::endl;
  if(EWKOff==1)
    return 0;
  for(int iSignal=0;iSignal<nSignal;iSignal++){
    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      for(int iMET=0;iMET<nMETRegion;iMET++){
	for(unsigned int iDSID=0;iDSID<vDSID.at(iSignal).size();iDSID++){
	  eventList->Reset();
	  if(Type=="Higgsino" && (Tau=="0p2" || Tau=="1p0" || Tau=="10p0") && (iSignal==2 || iSignal==4) && iDSID==0){
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0 && "prodType==0");
	  }else if(Type=="Higgsino" && (Tau=="0p2" || Tau=="1p0" || Tau=="10p0") && (iSignal==2 || iSignal==4) && iDSID==1){
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0 && "prodType==1");
	  }else{
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0);
	  }

	  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	    if(fUseTriggerBit && GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->LU_METtrigger_SUSYTools==false)
	      continue;
	  
	    double weightcommon = 1.0;
	    std::vector<double> weight;
	    weight.resize(nReweight);
	    for(int iFrom=0;iFrom<nReweight;iFrom++){
	      weight.at(iFrom) = 1.0;
	    }
	    for(unsigned int iTruth=0;iTruth<(GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->truthParticles->size());iTruth++){
	      int PDGID = (int)GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	      if(TMath::Abs(PDGID)!=1000024)
		continue;
	      float ProperTime = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	      for(int iFrom=0;iFrom<nReweight;iFrom++){
		weight.at(iFrom) *= GetWeightLifetime(ProperTime, TauVal, TauReweight.at(iFrom));
	      }
	    }// for iTruth
	    
	    if(fUseTriggerBit==false)
	      weightcommon *= GetTrigEff(GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR"));
	    if(fDeadModule==true){
	      double trackEta = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->Tracks->at(0)->p4.Eta();
	      weightcommon *= gSF_DeadModule[iMC16]->Eval(trackEta);
	    }
	    weightcommon *= GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->weightXsec;
	    weightcommon *= GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->weightMCweight;
	    weightcommon *= GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->weightPileupReweighting;
	    if(fSmearTruthPt){
	      double truthPt = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->Tracks->at(0)->TruthPt/1000.0;
	      hTrackPt->Reset();
	      if(truthPt > 0.0)
		SmearPt(hTrackPt, truthPt, weightcommon*SF[iMC16].at(iSignal).at(iDSID), false);
	      else
		hTrackPt->Fill(-1, weightcommon*SF[iMC16].at(iSignal).at(iDSID));
	      hPt[iMET].at(iSignal)->Add(hTrackPt, 1.0);
	      for(int iFrom=0;iFrom<nReweight;iFrom++){
		hPt_Reweighted[iMET].at(iSignal).at(iFrom)->Add(hTrackPt, weight.at(iFrom));
	      }
	    }else{
	      double trackPt = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	      hPt[iMET].at(iSignal)->Fill(trackPt, weightcommon*SF[iMC16].at(iSignal).at(iDSID));
	      for(int iFrom=0;iFrom<nReweight;iFrom++){
		hPt_Reweighted[iMET].at(iSignal).at(iFrom)->Fill(trackPt, weightcommon*weight.at(iFrom)*SF[iMC16].at(iSignal).at(iDSID));	    
	      }
	    }
	  }// for iEntry
	}
      }// for iDSID
    }// for iSignal
  }// for iMC16

  return 0;
}

double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau){
  if(TMath::Abs(PropTime) > 1e10)
    return 1.0;
  else
    return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
}

TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf) && (TMath::Abs(DisappearingTracks.d0sigTool) < 1.5)", METType.c_str(), KinematicsChain[iChain][0]);
      //myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0) && (TMath::Abs(DisappearingTracks.d0sigTool) < 1.5)", METType.c_str(), METType.c_str());
      //myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==2){
      //myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0)", METType.c_str(), METType.c_str());
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0) && (TMath::Abs(DisappearingTracks.d0sigTool) < 1.5)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 20.0)";
  return ret;
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

double GetTrigEff(NtupleReader *myReader){
  if(iChain==0) return myReader->TrigEff_Chain0;
  else if(iChain==1) return myReader->TrigEff_Chain1;
  else if(iChain==2) return myReader->TrigEff_Chain2;
  else if(iChain==3) return myReader->TrigEff_Chain3;
  else if(iChain==4) return myReader->TrigEff_Chain4;
  else if(iChain==5) return myReader->TrigEff_Chain5;
  else if(iChain==6) return myReader->TrigEff_Chain6;
  else return 0.0;
}


void SmearPt(TH1D *hHoge, double pt, double weight, bool fMuonFlag){
 for(int i=0;i<nBinsMySignal;i++){
    double ptLow = XBinsMySignal[i];
    double ptUp  = XBinsMySignal[i+1];
    if(i==(nBinsMySignal-1)){
      ptUp = 12500.0;
    }

    int iFunc=-1;
    if(pt < 15.0){
      iFunc = 0;
    }else if(pt < 20.0){
      iFunc = 1;
    }else if(pt < 25.0){
      iFunc = 2;
    }else if(pt < 35.0){
      iFunc = 3;
    }else if(pt < 45.0){
      iFunc = 4;
    }else if(pt < 60.0){
      iFunc = 5;
    }else if(pt < 100.0){
      iFunc = 6;
    }else{
      iFunc = 7;
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(fMuonFlag){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(fMuonFlag){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 

int FillDataYield(void){
  std::cout << "FillDataYield" << std::endl;
  TCut LowPtCut  = "(DisappearingTracks.p4.Pt()/1000.0 > 20.0 && DisappearingTracks.p4.Pt()/1000.0 < 58.48035)";
  TCut HighPtCut = "(DisappearingTracks.p4.Pt()/1000.0 > 58.48035)";
  for(int iMET=0;iMET<nMETRegion;iMET++){
    nData_SR_LowPt[iMET]  = myData->GetNtupleReader("Old4L_Common_SR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && CaloVETO_0 && LowPtCut);
    if(iMET==0)
      nData_SR_HighPt[iMET] = -1.0;
    else
      nData_SR_HighPt[iMET] = myData->GetNtupleReader("Old4L_Common_SR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && CaloVETO_0 && HighPtCut);

    if(iMET==0){
    //if(false){
      myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(Form("DisappearingTracks.p4.Pt()/1000.0 >> hData_SR_%d", iMET), GetKinematicsCut(iMET, 0) && CaloVETO_0 && LowPtCut);
    }else{
      myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(Form("DisappearingTracks.p4.Pt()/1000.0 >> hData_SR_%d", iMET), GetKinematicsCut(iMET, 0) && CaloVETO_0);
    }

    nData_SR_CaloSideBand[iMET] = myData->GetNtupleReader("Old4L_Common_SR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && CaloVETO_1);
    nData_HadCR[iMET]           = myData->GetNtupleReader("Old4L_Common_hadCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0));
    nData_FakeCR[iMET]          = myData->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && FakeD0_CR  && CaloVETO_0);
    nData_SingleEleCR[iMET]     = myData->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 1));
    nData_SingleMuCR[iMET]      = myData->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 2) && CaloVETO_0);

    std::cout << iMET << "   hadCR       : " << nData_HadCR[iMET] << std::endl;
    std::cout << iMET << "   fakeCR      : " << nData_FakeCR[iMET] << std::endl;
    std::cout << iMET << "   singleEleCR : " << nData_SingleEleCR[iMET] << std::endl;
    std::cout << iMET << "   singleMuCR  : " << nData_SingleMuCR[iMET]  << std::endl;
  }// for iMET

  eventList->Reset();
  myData->GetNtupleReader("Old4L_Common_hadCR")->m_tree->Draw(">>eventList", GetKinematicsCut(0, 0));
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_hadCR")->GetEntry(eventList->GetEntry(iEntry));
    double pT  = myData->GetNtupleReader("Old4L_Common_hadCR")->Tracks->at(0)->p4.Pt()/1000.0;
    double Eta = myData->GetNtupleReader("Old4L_Common_hadCR")->Tracks->at(0)->p4.Eta();
    double Phi = myData->GetNtupleReader("Old4L_Common_hadCR")->Tracks->at(0)->p4.Phi();
    double TF  = GetTF(pT, Eta, Phi, -1, 0, 0);
    SmearPt(hData_HadCR, pT, TF, true);
  }// for iEntry
  
  myData->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->Draw("DisappearingTracks.p4.Pt()/1000.0 >> hData_FakeCR", GetKinematicsCut(3, 0) && FakeD0_CR  && CaloVETO_0);

  myData->GetNtupleReader("Old4L_Common_hadCR")->m_tree->Draw("DisappearingTracks.etclus20_topo/1000.0 >> hData_HadCR_EtClus20", GetKinematicsCut(3, 0));
  myData->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->Draw("DisappearingTracks.etclus20_topo/1000.0 >> hData_EleCR_EtClus20", GetKinematicsCut(3, 1));
  myData->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->Draw("DisappearingTracks.etclus20_topo/1000.0 >> hData_MuCR_EtClus20", GetKinematicsCut(3, 2));
  myData->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->Draw("DisappearingTracks.etclus20_topo/1000.0 >> hData_FakeCR_EtClus20", GetKinematicsCut(3, 0) && FakeD0_CR  && CaloVETO_0);

  // SR
  for(int iMET=0;iMET<nMETRegion;iMET++){
    hData15_Period_SR[iMET] = (TH1F *)hLumiHistData15->Clone(Form("hData15_Period_SR_%d", iMET)); hData15_Period_SR[iMET]->Reset();
    hData16_Period_SR[iMET] = (TH1F *)hLumiHistData16->Clone(Form("hData16_Period_SR_%d", iMET)); hData16_Period_SR[iMET]->Reset();
    hData17_Period_SR[iMET] = (TH1F *)hLumiHistData17->Clone(Form("hData17_Period_SR_%d", iMET)); hData17_Period_SR[iMET]->Reset();
    hData18_Period_SR[iMET] = (TH1F *)hLumiHistData18->Clone(Form("hData18_Period_SR_%d", iMET)); hData18_Period_SR[iMET]->Reset();

    eventList->Reset();
    if(iMET==0){
      myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0 && LowPtCut);
    }else{
      myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0);
    }

    ofs.open(Form("eventList_Chain%d_SR_MET%d.txt", iChain, iMET));
    ofsDetail.open(Form("eventList_Chain%d_SR_MET%d.csv", iChain, iMET));
    for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
      myData->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
      UInt_t RunNumber      = myData->GetNtupleReader("Old4L_Common_SR")->RunNumber;
      UInt_t lumiBlock      = myData->GetNtupleReader("Old4L_Common_SR")->lumiBlock;
      ULong64_t EventNumber = myData->GetNtupleReader("Old4L_Common_SR")->EventNumber;
      int nJet = myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->size();
      
      ofs << std::setw(10) << RunNumber << std::setw(10) << lumiBlock << std::setw(20) << EventNumber << std::endl;
      ofsDetail << myData->GetNtupleReader("Old4L_Common_SR")-> RunNumber   << ", " 
		<< myData->GetNtupleReader("Old4L_Common_SR")-> EventNumber << ", " 
		<< myData->GetNtupleReader("Old4L_Common_SR")-> lumiBlock   << ", " 
		<< myData->GetNtupleReader("Old4L_Common_SR")-> CorrectedAverageInteractionsPerCrossing << ", " 

		<< myData->GetNtupleReader("Old4L_Common_SR")-> missingET->p4.Pt()/1000.0   << ", " 
		<< myData->GetNtupleReader("Old4L_Common_SR")-> missingET->p4.Phi()         << ", " 

		<< (nJet > 0 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(0)->p4.Pt()/1000.0 : 0.0) << ", " 
		<< (nJet > 0 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(0)->p4.Eta()       : 0.0) << ", " 
		<< (nJet > 0 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(0)->p4.Phi()       : 0.0) << ", " 

		<< (nJet > 1 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(1)->p4.Pt()/1000.0 : 0.0) << ", " 
		<< (nJet > 1 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(1)->p4.Eta()       : 0.0) << ", " 
		<< (nJet > 1 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(1)->p4.Phi()       : 0.0) << ", " 

		<< (nJet > 2 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(2)->p4.Pt()/1000.0 : 0.0) << ", " 
		<< (nJet > 2 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(2)->p4.Eta()       : 0.0) << ", " 
		<< (nJet > 2 ? myData->GetNtupleReader("Old4L_Common_SR")-> goodJets->at(2)->p4.Phi()       : 0.0) << ", " 

		<< myData->GetNtupleReader("Old4L_Common_SR")-> JetMetdPhiMin50 << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> HT << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> EffMass << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Aplanarity << ", "
	
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->p4.Pt()/1000.0  << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->p4.Eta()        << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->p4.Phi()        << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->d0sigTool       << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->z0sinthetawrtPV << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->ptcone40overPt_1gev << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->Quality << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->etclus20_topo/1000.0 << ", "
		<< myData->GetNtupleReader("Old4L_Common_SR")-> Tracks->at(0)->pixeldEdx << std::endl;
      

      if(RunNumber >= 276262 && RunNumber <= 284484){
	hData15_Period_SR[iMET]->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 297730 && RunNumber <= 311481){
	hData16_Period_SR[iMET]->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 325713 && RunNumber <= 340453){
	hData17_Period_SR[iMET]->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 348885 && RunNumber <= 364292){
	hData18_Period_SR[iMET]->Fill(Form("%d", RunNumber), 1.0);
      }      
    }// for iEntry
    ofs.close();
    ofsDetail.close();

    gData15_Period_SR[iMET] = new TGraphAsymmErrors();
    gData15_Period_SR[iMET]->SetName(Form("gData15_Period_SR_%d", iMET));
    MakeGRatio(gData15_Period_SR[iMET], hData15_Period_SR[iMET], hLumiHistData15);
    
    gData16_Period_SR[iMET] = new TGraphAsymmErrors();
    gData16_Period_SR[iMET]->SetName(Form("gData16_Period_SR_%d", iMET));
    MakeGRatio(gData16_Period_SR[iMET], hData16_Period_SR[iMET], hLumiHistData16);
    
    gData17_Period_SR[iMET] = new TGraphAsymmErrors();
    gData17_Period_SR[iMET]->SetName(Form("gData17_Period_SR_%d", iMET));
    MakeGRatio(gData17_Period_SR[iMET], hData17_Period_SR[iMET], hLumiHistData17);

    gData18_Period_SR[iMET] = new TGraphAsymmErrors();
    gData18_Period_SR[iMET]->SetName(Form("gData18_Period_SR_%d", iMET));
    MakeGRatio(gData18_Period_SR[iMET], hData18_Period_SR[iMET], hLumiHistData18);
  }// for iMET

  // VR
  for(int iMET=0;iMET<nMETRegion;iMET++){
    TH1F *hData15_Period = (TH1F *)hLumiHistData15->Clone("hData15_Period"); hData15_Period->Reset();
    TH1F *hData16_Period = (TH1F *)hLumiHistData16->Clone("hData16_Period"); hData16_Period->Reset();
    TH1F *hData17_Period = (TH1F *)hLumiHistData17->Clone("hData17_Period"); hData17_Period->Reset();
    TH1F *hData18_Period = (TH1F *)hLumiHistData18->Clone("hData18_Period"); hData18_Period->Reset();

    eventList->Reset();
    myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_1);

    ofs.open(Form("eventList_Chain%d_VR_MET%d.txt", iChain, iMET));
    for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
      myData->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
      UInt_t RunNumber      = myData->GetNtupleReader("Old4L_Common_SR")->RunNumber;
      UInt_t lumiBlock      = myData->GetNtupleReader("Old4L_Common_SR")->lumiBlock;
      ULong64_t EventNumber = myData->GetNtupleReader("Old4L_Common_SR")->EventNumber;
      
      ofs << std::setw(10) << RunNumber << std::setw(10) << lumiBlock << std::setw(20) << EventNumber << std::endl;
      if(RunNumber >= 276262 && RunNumber <= 284484){
	hData15_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 297730 && RunNumber <= 311481){
	hData16_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 325713 && RunNumber <= 340453){
	hData17_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 348885 && RunNumber <= 364292){
	hData18_Period->Fill(Form("%d", RunNumber), 1.0);
      }      
    }// for iEntry
    ofs.close();
    
    gData15_Period_VR[iMET] = new TGraphAsymmErrors();
    gData15_Period_VR[iMET]->SetName(Form("gData15_Period_VR_%d", iMET));
    MakeGRatio(gData15_Period_VR[iMET], hData15_Period, hLumiHistData15);

    gData16_Period_VR[iMET] = new TGraphAsymmErrors();
    gData16_Period_VR[iMET]->SetName(Form("gData16_Period_VR_%d", iMET));
    MakeGRatio(gData16_Period_VR[iMET], hData16_Period, hLumiHistData16);

    gData17_Period_VR[iMET] = new TGraphAsymmErrors();
    gData17_Period_VR[iMET]->SetName(Form("gData17_Period_VR_%d", iMET));
    MakeGRatio(gData17_Period_VR[iMET], hData17_Period, hLumiHistData17);

    gData18_Period_VR[iMET] = new TGraphAsymmErrors();
    gData18_Period_VR[iMET]->SetName(Form("gData18_Period_VR_%d", iMET));
    MakeGRatio(gData18_Period_VR[iMET], hData18_Period, hLumiHistData18);
  }// for iMET

  // FakeCR
  {
    TH1F *hData15_Period = (TH1F *)hLumiHistData15->Clone("hData15_Period"); hData15_Period->Reset();
    TH1F *hData16_Period = (TH1F *)hLumiHistData16->Clone("hData16_Period"); hData16_Period->Reset();
    TH1F *hData17_Period = (TH1F *)hLumiHistData17->Clone("hData17_Period"); hData17_Period->Reset();
    TH1F *hData18_Period = (TH1F *)hLumiHistData18->Clone("hData18_Period"); hData18_Period->Reset();

    eventList->Reset();
    myData->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->Draw(">>eventList", GetKinematicsCut(-1, 0) && FakeD0_CR && CaloVETO_0);

    ofs.open(Form("eventList_Chain%d_FakeCR.txt", iChain));
    for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
      myData->GetNtupleReader("Old4L_Common_fakeCR")->GetEntry(eventList->GetEntry(iEntry));
      UInt_t RunNumber      = myData->GetNtupleReader("Old4L_Common_fakeCR")->RunNumber;
      UInt_t lumiBlock      = myData->GetNtupleReader("Old4L_Common_fakeCR")->lumiBlock;
      ULong64_t EventNumber = myData->GetNtupleReader("Old4L_Common_fakeCR")->EventNumber;
      
      ofs << std::setw(10) << RunNumber << std::setw(10) << lumiBlock << std::setw(20) << EventNumber << std::endl;
      if(RunNumber >= 276262 && RunNumber <= 284484){
	hData15_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 297730 && RunNumber <= 311481){
	hData16_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 325713 && RunNumber <= 340453){
	hData17_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 348885 && RunNumber <= 364292){
	hData18_Period->Fill(Form("%d", RunNumber), 1.0);
      }      
    }// for iEntry
    ofs.close();
    
    gData15_Period_FakeCR = new TGraphAsymmErrors();
    gData15_Period_FakeCR->SetName(Form("gData15_Period_FakeCR"));
    MakeGRatio(gData15_Period_FakeCR, hData15_Period, hLumiHistData15);

    gData16_Period_FakeCR = new TGraphAsymmErrors();
    gData16_Period_FakeCR->SetName(Form("gData16_Period_FakeCR"));
    MakeGRatio(gData16_Period_FakeCR, hData16_Period, hLumiHistData16);

    gData17_Period_FakeCR = new TGraphAsymmErrors();
    gData17_Period_FakeCR->SetName(Form("gData17_Period_FakeCR"));
    MakeGRatio(gData17_Period_FakeCR, hData17_Period, hLumiHistData17);

    gData18_Period_FakeCR = new TGraphAsymmErrors();
    gData18_Period_FakeCR->SetName(Form("gData18_Period_FakeCR"));
    MakeGRatio(gData18_Period_FakeCR, hData18_Period, hLumiHistData18);
  }// for iMET

  // HadCR
  for(int iMET=0;iMET<nMETRegion;iMET++){
    TH1F *hData15_Period = (TH1F *)hLumiHistData15->Clone("hData15_Period"); hData15_Period->Reset();
    TH1F *hData16_Period = (TH1F *)hLumiHistData16->Clone("hData16_Period"); hData16_Period->Reset();
    TH1F *hData17_Period = (TH1F *)hLumiHistData17->Clone("hData17_Period"); hData17_Period->Reset();
    TH1F *hData18_Period = (TH1F *)hLumiHistData18->Clone("hData18_Period"); hData18_Period->Reset();

    eventList->Reset();
    myData->GetNtupleReader("Old4L_Common_hadCR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0));

    ofs.open(Form("eventList_Chain%d_HadCR_MET%d.txt", iChain, iMET));
    for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
      myData->GetNtupleReader("Old4L_Common_hadCR")->GetEntry(eventList->GetEntry(iEntry));
      UInt_t RunNumber      = myData->GetNtupleReader("Old4L_Common_hadCR")->RunNumber;
      UInt_t lumiBlock      = myData->GetNtupleReader("Old4L_Common_hadCR")->lumiBlock;
      ULong64_t EventNumber = myData->GetNtupleReader("Old4L_Common_hadCR")->EventNumber;
      
      ofs << std::setw(10) << RunNumber << std::setw(10) << lumiBlock << std::setw(20) << EventNumber << std::endl;
      if(RunNumber >= 276262 && RunNumber <= 284484){
	hData15_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 297730 && RunNumber <= 311481){
	hData16_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 325713 && RunNumber <= 340453){
	hData17_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 348885 && RunNumber <= 364292){
	hData18_Period->Fill(Form("%d", RunNumber), 1.0);
      }      
    }// for iEntry
    ofs.close();
    
    gData15_Period_HadCR[iMET] = new TGraphAsymmErrors();
    gData15_Period_HadCR[iMET]->SetName(Form("gData15_Period_HadCR_%d", iMET));
    MakeGRatio(gData15_Period_HadCR[iMET], hData15_Period, hLumiHistData15);

    gData16_Period_HadCR[iMET] = new TGraphAsymmErrors();
    gData16_Period_HadCR[iMET]->SetName(Form("gData16_Period_HadCR_%d", iMET));
    MakeGRatio(gData16_Period_HadCR[iMET], hData16_Period, hLumiHistData16);

    gData17_Period_HadCR[iMET] = new TGraphAsymmErrors();
    gData17_Period_HadCR[iMET]->SetName(Form("gData17_Period_HadCR_%d", iMET));
    MakeGRatio(gData17_Period_HadCR[iMET], hData17_Period, hLumiHistData17);

    gData18_Period_HadCR[iMET] = new TGraphAsymmErrors();
    gData18_Period_HadCR[iMET]->SetName(Form("gData18_Period_HadCR_%d", iMET));
    MakeGRatio(gData18_Period_HadCR[iMET], hData18_Period, hLumiHistData18);
  }// for iMET

  // EleCR
  for(int iMET=0;iMET<nMETRegion;iMET++){
    TH1F *hData15_Period = (TH1F *)hLumiHistData15->Clone("hData15_Period"); hData15_Period->Reset();
    TH1F *hData16_Period = (TH1F *)hLumiHistData16->Clone("hData16_Period"); hData16_Period->Reset();
    TH1F *hData17_Period = (TH1F *)hLumiHistData17->Clone("hData17_Period"); hData17_Period->Reset();
    TH1F *hData18_Period = (TH1F *)hLumiHistData18->Clone("hData18_Period"); hData18_Period->Reset();

    eventList->Reset();
    myData->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 1));

    ofs.open(Form("eventList_Chain%d_EleCR_MET%d.txt", iChain, iMET));
    for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
      myData->GetNtupleReader("Old4L_Common_singleEleCR")->GetEntry(eventList->GetEntry(iEntry));
      UInt_t RunNumber      = myData->GetNtupleReader("Old4L_Common_singleEleCR")->RunNumber;
      UInt_t lumiBlock      = myData->GetNtupleReader("Old4L_Common_singleEleCR")->lumiBlock;
      ULong64_t EventNumber = myData->GetNtupleReader("Old4L_Common_singleEleCR")->EventNumber;
      
      ofs << std::setw(10) << RunNumber << std::setw(10) << lumiBlock << std::setw(20) << EventNumber << std::endl;
      if(RunNumber >= 276262 && RunNumber <= 284484){
	hData15_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 297730 && RunNumber <= 311481){
	hData16_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 325713 && RunNumber <= 340453){
	hData17_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 348885 && RunNumber <= 364292){
	hData18_Period->Fill(Form("%d", RunNumber), 1.0);
      }      
    }// for iEntry
    ofs.close();
    
    gData15_Period_EleCR[iMET] = new TGraphAsymmErrors();
    gData15_Period_EleCR[iMET]->SetName(Form("gData15_Period_EleCR_%d", iMET));
    MakeGRatio(gData15_Period_EleCR[iMET], hData15_Period, hLumiHistData15);

    gData16_Period_EleCR[iMET] = new TGraphAsymmErrors();
    gData16_Period_EleCR[iMET]->SetName(Form("gData16_Period_EleCR_%d", iMET));
    MakeGRatio(gData16_Period_EleCR[iMET], hData16_Period, hLumiHistData16);

    gData17_Period_EleCR[iMET] = new TGraphAsymmErrors();
    gData17_Period_EleCR[iMET]->SetName(Form("gData17_Period_EleCR_%d", iMET));
    MakeGRatio(gData17_Period_EleCR[iMET], hData17_Period, hLumiHistData17);

    gData18_Period_EleCR[iMET] = new TGraphAsymmErrors();
    gData18_Period_EleCR[iMET]->SetName(Form("gData18_Period_EleCR_%d", iMET));
    MakeGRatio(gData18_Period_EleCR[iMET], hData18_Period, hLumiHistData18);
  }// for iMET

  // MuCR
  for(int iMET=0;iMET<nMETRegion;iMET++){
    TH1F *hData15_Period = (TH1F *)hLumiHistData15->Clone("hData15_Period"); hData15_Period->Reset();
    TH1F *hData16_Period = (TH1F *)hLumiHistData16->Clone("hData16_Period"); hData16_Period->Reset();
    TH1F *hData17_Period = (TH1F *)hLumiHistData17->Clone("hData17_Period"); hData17_Period->Reset();
    TH1F *hData18_Period = (TH1F *)hLumiHistData18->Clone("hData18_Period"); hData18_Period->Reset();

    eventList->Reset();
    myData->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 2) && CaloVETO_0);

    ofs.open(Form("eventList_Chain%d_MuCR_MET%d.txt", iChain, iMET));
    for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
      myData->GetNtupleReader("Old4L_Common_singleMuCR")->GetEntry(eventList->GetEntry(iEntry));
      UInt_t RunNumber      = myData->GetNtupleReader("Old4L_Common_singleMuCR")->RunNumber;
      UInt_t lumiBlock      = myData->GetNtupleReader("Old4L_Common_singleMuCR")->lumiBlock;
      ULong64_t EventNumber = myData->GetNtupleReader("Old4L_Common_singleMuCR")->EventNumber;
      
      ofs << std::setw(10) << RunNumber << std::setw(10) << lumiBlock << std::setw(20) << EventNumber << std::endl;
      if(RunNumber >= 276262 && RunNumber <= 284484){
	hData15_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 297730 && RunNumber <= 311481){
	hData16_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 325713 && RunNumber <= 340453){
	hData17_Period->Fill(Form("%d", RunNumber), 1.0);
      }else if(RunNumber >= 348885 && RunNumber <= 364292){
	hData18_Period->Fill(Form("%d", RunNumber), 1.0);
      }      
    }// for iEntry
    ofs.close();
    
    gData15_Period_MuCR[iMET] = new TGraphAsymmErrors();
    gData15_Period_MuCR[iMET]->SetName(Form("gData15_Period_MuCR_%d", iMET));
    MakeGRatio(gData15_Period_MuCR[iMET], hData15_Period, hLumiHistData15);

    gData16_Period_MuCR[iMET] = new TGraphAsymmErrors();
    gData16_Period_MuCR[iMET]->SetName(Form("gData16_Period_MuCR_%d", iMET));
    MakeGRatio(gData16_Period_MuCR[iMET], hData16_Period, hLumiHistData16);

    gData17_Period_MuCR[iMET] = new TGraphAsymmErrors();
    gData17_Period_MuCR[iMET]->SetName(Form("gData17_Period_MuCR_%d", iMET));
    MakeGRatio(gData17_Period_MuCR[iMET], hData17_Period, hLumiHistData17);

    gData18_Period_MuCR[iMET] = new TGraphAsymmErrors();
    gData18_Period_MuCR[iMET]->SetName(Form("gData18_Period_MuCR_%d", iMET));
    MakeGRatio(gData18_Period_MuCR[iMET], hData18_Period, hLumiHistData18);
  }// for iMET

  return 0;
}

double GetTF(double pT, double Eta, double Phi, int iTFDisap, int iTFMSCalo, int iCorrect){
  double ret=1.0;
  /*
  int iBinInvPt = (int)(50.0/(0.1*pT)) + 1;
  int iBinPt  = (int)(18.0*(TMath::Log(pT/4.0)/TMath::Log(5.0)) + 1);
  int iBinEta = (int)(Eta/0.2) + 13;
  int iBinPhi = (int)(50.0*(Phi+TMath::Pi())/(2.0*TMath::Pi())) + 1;
  */
  if(iTFDisap==0){
    int iBinPt  = hEleBG_TF_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFDisap==1){
    int iBinInvPt  = hEleBG_TF_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFDisap==2){
    ret *= hMuBG_TF_1Bin->GetBinContent(1);
  }

  if(iTFMSCalo==3){
    int iBinPt  = hEleBG_TF_CaloIso_4to0_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_4to0_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to0_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==4){
    int iBinPt  = hEleBG_TF_CaloIso_4to123_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_4to123_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to123_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==0){ // CR
    int iBinPt  = hEleBG_TF_CaloIso_432to0_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to0_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to0_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==1){ // VR
    int iBinPt  = hEleBG_TF_CaloIso_432to1_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to1_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to1_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==2){ // Old
    int iBinPt  = hEleBG_TF_CaloIso_432to01_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to01_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to01_PtEta->GetBinContent(iBinPt, iBinEta);

  }else if(iTFMSCalo==8){
    int iBinInvPt  = hEleBG_TF_CaloIso_4to0_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_4to0_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to0_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==9){
    int iBinInvPt  = hEleBG_TF_CaloIso_4to123_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_4to123_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to123_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==5){ // CR
    int iBinInvPt  = hEleBG_TF_CaloIso_432to0_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_432to0_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to0_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==6){ // VR
    int iBinInvPt  = hEleBG_TF_CaloIso_432to1_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_432to1_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to1_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==7){ // Old
    int iBinInvPt  = hEleBG_TF_CaloIso_432to01_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_432to01_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to01_InvPtEta->GetBinContent(iBinInvPt, iBinEta);

  }else if(iTFMSCalo==10){
    int iBinPhi = hMSBG_TF_PhiEta->GetXaxis()->FindBin(Phi);
    int iBinEta = hMSBG_TF_PhiEta->GetYaxis()->FindBin(Eta);
    ret *= hMSBG_TF_PhiEta->GetBinContent(iBinPhi, iBinEta);
  }

  //int iBin =  (int)(15.0*(TMath::Log(pT/5.0)/TMath::Log(20.0)) + 1);
  if(iCorrect==0){ // CR
    int iBin = P_caloveto05_hadron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_hadron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_hadron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==1){ // VR
    int iBin = P_caloveto05_10_hadron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_10_hadron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_10_hadron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==2){ // Old
    int iBin = P_caloveto10_hadron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto10_hadron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto10_hadron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;

  }else if(iCorrect==10){ // CR
    int iBin = P_caloveto05_electron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_electron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_electron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==11){ // VR
    int iBin = P_caloveto05_10_electron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_10_electron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_10_electron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==12){ // Old
    int iBin = P_caloveto10_electron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto10_electron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto10_electron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }

  return ret;
}

double CalcFracError(double vNume, double vDenom, double eNume, double eDenom){
  return TMath::Sqrt((eNume/vDenom)*(eNume/vDenom) + (vNume*eDenom/(vDenom*vDenom))*(vNume*eDenom/(vDenom*vDenom)));
}
