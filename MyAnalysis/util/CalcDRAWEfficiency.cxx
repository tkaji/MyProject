#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/NtupleReader.h"
//#include "src/Constant.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TPad.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

const int nSignal=2;
const int GMass[nSignal]={1400, 1800};
const int CMass[nSignal]={1200,  800};
EColor SigColor[nSignal]={EColor::kMagenta, EColor::kCyan};

const int nData=1;
const std::string DataFile[nData]={"/data3/tkaji/MyAnalysis/files/data.root"};
const std::string SignalFile[nSignal]={"/data3/tkaji/MyAnalysis/files/noPileUp.20180706_2/Work_1400_1200/out.myAna.noPileUp.20180706_2.1400_1200.root",
                                  "/data3/tkaji/MyAnalysis/files/noPileUp.20180706_2/Work_1800_800/out.myAna.noPileUp.20180706_2.1800_800.root"};

const std::string DataTag[nData]={"data_2015-2016"};
// 2015 : 3.21956
// 2016 : 32.9954
// 2017 : 0.733809

const int nRun2015=66;
const int nRun2016=151;
const int nRun2017=185;
const int nPeriod2015=6;
const int nPeriod2016=10;
const int nPeriod2017=1;
const std::string Period2015[nPeriod2015]={"D", "E", "F", "G", "H", "J"};
const std::string Period2016[nPeriod2016]={"A", "B", "C", "D", "E", "F", "G", "I", "K", "L"};
const double IntLumi=3.21956 + 32.9954; // fb-1
const double SignalXS[nSignal]={1e3 * 0.0253,
                                1e3 * 0.00276}; // fb = 1e3*pb
const double SignalProdEff[nSignal]={1.0, 1.0};
double SFSig[nSignal];
MyManager *mySignal[nSignal];
MyManager *myData[nSignal];

const std::string StdDir ="/data3/tkaji/MyAnalysis/files/StandardAOD";
const std::string DRAWDir="/data3/tkaji/MyAnalysis/files/RPVLLStream";

TFile *tfPeriod2015_Std[nPeriod2015];
TFile *tfPeriod2016_Std[nPeriod2016];
TFile *tfPeriod2017_Std[nPeriod2017];
TFile *tfPeriod2015_DRAW[nPeriod2015];
TFile *tfPeriod2016_DRAW[nPeriod2016];
TFile *tfPeriod2017_DRAW[nPeriod2017];

TH1D *hPeriod2015_Std[nPeriod2015];
TH1D *hPeriod2016_Std[nPeriod2016];
TH1D *hPeriod2017_Std[nPeriod2017];
TH1D *hPeriod2015_DRAW[nPeriod2015];
TH1D *hPeriod2016_DRAW[nPeriod2016];
TH1D *hPeriod2017_DRAW[nPeriod2017];

TGraphAsymmErrors *gPeriod2015[nPeriod2015];
TGraphAsymmErrors *gPeriod2016[nPeriod2016];
TH1D *hPeriod2015[nPeriod2015];
TH1D *hPeriod2016[nPeriod2016];
TH1D *hPeriod2017[nPeriod2017];
std::string outName;

TCanvas *c0;
TPad *pad0;
TPad *pad1[2];

TH1D *hPass;
TH1D *hAll;
TLegend *tl2015;
TLegend *tl2016;
TLegend *tl2017;

TH2D *hFrame;
std::string PDFName;
int DrawTH1RatioPlot(TH1D *hKin, TH1D *hTrack, EColor hCol);
int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("test-val", po::value<int>(), "test value")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("DRAWEffPath", po::value<std::string>(), "path of DRAW filter efficiency for MC")
    ("output-file,o", po::value<std::string>()->default_value("gDRAWEff"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  outName = vm["output-file"].as<std::string>();

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  c0 = new TCanvas("c0", "c0", 900, 600);

  hFrame = new TH2D("hFrame", ";MET [GeV];DRAW Efficiency", 50, 0.0, 1000.0, 150, 0.0, 1.5);
  c0->Print(Form("%s.pdf[", outName.c_str()), "pdf");

  tl2015 = new TLegend(0.1, 0.1, 0.3, 0.3);
  tl2016 = new TLegend(0.1, 0.1, 0.3, 0.3);

  hFrame->Draw();
  // 2015 Period
  for(int i=0;i<nPeriod2015;i++){
    tfPeriod2015_Std[i] = new TFile(Form("%s/data15_13TeV.period%s.root", StdDir.c_str(), Period2015[i].c_str()), "READ");
    gROOT->cd();
    hPeriod2015_Std[i]  = (TH1D *)tfPeriod2015_Std[i]->Get("hKinematics_0");
    hPeriod2015_Std[i]->SetName(Form("hPeriod2015_Std_%d", i));
    hPeriod2015_Std[i]->Sumw2();

    tfPeriod2015_DRAW[i] = new TFile(Form("%s/data15_13TeV.period%s.root", DRAWDir.c_str(), Period2015[i].c_str()), "READ");
    gROOT->cd();
    hPeriod2015_DRAW[i]  = (TH1D *)tfPeriod2015_DRAW[i]->Get("hKinematics_0");
    hPeriod2015_DRAW[i]->SetName(Form("hPeriod2015_DRAW_%d", i));
    hPeriod2015_DRAW[i]->Sumw2();

    /*
    gPeriod2015[i] = new TGraphAsymmErrors(hPeriod2015_DRAW[i], hPeriod2015_Std[i]);
    gPeriod2015[i]->SetMarkerColor(45+i*5);
    gPeriod2015[i]->SetLineColor(45+i*5);
    gPeriod2015[i]->Draw("sameEL");    
    */

    hPeriod2015[i] = (TH1D *)hPeriod2015_DRAW[i]->Clone(Form("hPeriod2015_%d", i));
    hPeriod2015[i]->Divide(hPeriod2015_Std[i]);
    hPeriod2015[i]->SetMarkerColor(45+i*5);
    hPeriod2015[i]->SetLineColor(45+i*5);
    hPeriod2015[i]->Draw("sameEL");
    tl2015->AddEntry(hPeriod2015[i], Form("data15 Period %s", Period2015[i].c_str()), "el");
  }// for i
  tl2015->Draw("same");
  c0->Print(Form("%s.pdf", outName.c_str()), "pdf");

  hFrame->Draw();
  // 2016 Period
  for(int i=0;i<nPeriod2016;i++){
    tfPeriod2016_Std[i] = new TFile(Form("%s/data16_13TeV.period%s.root", StdDir.c_str(), Period2016[i].c_str()), "READ");
    gROOT->cd();
    hPeriod2016_Std[i]  = (TH1D *)tfPeriod2016_Std[i]->Get("hKinematics_0");
    hPeriod2016_Std[i]->SetName(Form("hPeriod2016_Std_%d", i));
    hPeriod2016_Std[i]->Sumw2();

    tfPeriod2016_DRAW[i] = new TFile(Form("%s/data16_13TeV.period%s.root", DRAWDir.c_str(), Period2016[i].c_str()), "READ");
    gROOT->cd();
    hPeriod2016_DRAW[i]  = (TH1D *)tfPeriod2016_DRAW[i]->Get("hKinematics_0");
    hPeriod2016_DRAW[i]->SetName(Form("hPeriod2016_DRAW_%d", i));
    hPeriod2016_DRAW[i]->Sumw2();
    /*
    gPeriod2016[i] = new TGraphAsymmErrors(hPeriod2016_DRAW[i], hPeriod2016_Std[i]);
    gPeriod2016[i]->SetMarkerColor(45+i*5);
    gPeriod2016[i]->SetLineColor(45+i*5);
    gPeriod2016[i]->Draw("sameEL");    
    */
    hPeriod2016[i] = (TH1D *)hPeriod2016_DRAW[i]->Clone(Form("hPeriod2016_%d", i));
    hPeriod2016[i]->Divide(hPeriod2016_Std[i]);
    hPeriod2016[i]->SetMarkerColor(45+i*5);
    hPeriod2016[i]->SetLineColor(45+i*5);
    hPeriod2016[i]->Draw("sameEL");
    tl2016->AddEntry(hPeriod2016[i], Form("data16 Period %s", Period2016[i].c_str()), "el");
  }// for i
  tl2016->Draw("same");
  c0->Print(Form("%s.pdf", outName.c_str()), "pdf");

  hFrame->Draw();
  // 2016 Period
  for(int i=0;i<nPeriod2017;i++){
    //tfPeriod2017_Std[i] = new TFile(Form("%s/data17_13TeV.period%s.root", StdDir.c_str(), Period2017[i].c_str()), "READ");
    tfPeriod2017_Std[i] = new TFile(Form("%s/out.myAna.data17_13TeV.Standard.00331085.root", StdDir.c_str()), "READ");
    gROOT->cd();
    hPeriod2017_Std[i]  = (TH1D *)tfPeriod2017_Std[i]->Get("hKinematics_0");
    hPeriod2017_Std[i]->SetName(Form("hPeriod2017_Std_%d", i));
    hPeriod2017_Std[i]->Sumw2();

    //tfPeriod2017_DRAW[i] = new TFile(Form("%s/data17_13TeV.period%s.root", DRAWDir.c_str(), Period2017[i].c_str()), "READ");
    tfPeriod2017_DRAW[i] = new TFile(Form("%s/out.myAna.data17_13TeV.20180812_1.00331085.root", DRAWDir.c_str()), "READ");
    gROOT->cd();
    hPeriod2017_DRAW[i]  = (TH1D *)tfPeriod2017_DRAW[i]->Get("hKinematics_0");
    hPeriod2017_DRAW[i]->SetName(Form("hPeriod2017_DRAW_%d", i));
    hPeriod2017_DRAW[i]->Sumw2();
    /*
    gPeriod2017[i] = new TGraphAsymmErrors(hPeriod2017_DRAW[i], hPeriod2017_Std[i]);
    gPeriod2017[i]->SetMarkerColor(45+i*5);
    gPeriod2017[i]->SetLineColor(45+i*5);
    gPeriod2017[i]->Draw("sameEL");    
    */
    hPeriod2017[i] = (TH1D *)hPeriod2017_DRAW[i]->Clone(Form("hPeriod2017_%d", i));
    hPeriod2017[i]->Divide(hPeriod2017_Std[i]);
    hPeriod2017[i]->SetMarkerColor(45+i*5);
    hPeriod2017[i]->SetLineColor(45+i*5);
    hPeriod2017[i]->Draw("sameEL");
    //tl2017->AddEntry(hPeriod2017[i], Form("data16 Period %s", Period2017[i].c_str()), "el");
  }// for i
  //tl2017->Draw("same");
  c0->Print(Form("%s.pdf", outName.c_str()), "pdf");

  c0->Print(Form("%s.pdf]", outName.c_str()), "pdf");

  return 0;
}
