#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyAnalysisAlg.h"
#include "src/NtupleReader.h"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

myTrack *myT;
NtupleReader *myCommon;

int main(int argc, char **argv){
  std::cout << "this is a test script" << std::endl;
  std::cout << "argc = " << argc << std::endl;
  for(int i=0;i<argc;i++){
    std::cout << "argv[" << i << "] = " << argv[i] << std::endl;
  }

  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("output-file,o", po::value<std::string>(), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  //("input-files,i", po::value<std::stringstream>(), "input Files");
  //    ("input-files,i", po::value<std::string>(), "input Files");
  po::positional_options_description p;
  p.add("input-files", -1);
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::cout << vm["output-file"].as<std::string>() << std::endl;

  for(unsigned int i=0;i<vm["input-files"].as<std::vector<std::string> >().size();i++){
    std::cout << vm["input-files"].as<std::vector<std::string> >().at(i)<< std::endl;
  }
  //std::cout << vm["input-files"].as<std::>() << std::endl;

  return 0;
}
