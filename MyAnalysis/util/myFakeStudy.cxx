#include "myFakeStudy.h"
namespace po = boost::program_options;
int LoadInput(void);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("doSmear", po::value<bool>()->default_value(true), "doSmearing")
    ("SimpleSmear", po::value<bool>()->default_value(false), "SimpleSmearing")
    ("output-file,o", po::value<std::string>()->default_value("myFakeStudy"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  fDoSmearing = vm["doSmear"].as<bool>();
  fSimpleSmear = vm["SimpleSmear"].as<bool>();

  LoadInput();

  myData = new MyManager(DataFile, "Data");
  myData->LoadMyFile();
  //myHistData = myData->myHist_Old4L;
  std::string TreeName = "Old4L_Common_fakeCR";
  
  eventList->Reset();
  myData->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", GetKinematicsCut(-1, 0) && "(DisappearingTracks.p4.Pt()/1000.0 > 20.0)");

  std::cout << " N = " << eventList->GetN() << std::endl;
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    if((iEntry+1)%10000==0)
      std::cout << " - " << iEntry+1 << std::endl;

    myData->GetNtupleReader(TreeName)->GetEntry(eventList->GetEntry(iEntry));
    b_pT  = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Pt()/1000.0;
    b_Eta = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Eta();
    b_Phi = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Phi();
    b_D0  = TMath::Abs(myData->GetNtupleReader(TreeName)->Tracks->at(0)->d0sigTool);
    b_Calo= myData->GetNtupleReader(TreeName)->Tracks->at(0)->etclus20_topo/1000.0;
    b_MET = myData->GetNtupleReader(TreeName)->missingET->p4.Pt()/1000.0;

    hD0Calo->Fill(b_D0, b_Calo);
    hD0MET->Fill(b_D0, b_MET);
    hCaloMET->Fill(b_Calo, b_MET);

    hPtCalo->Fill(b_pT, b_Calo);

    if(b_Calo < 5.0){
    //if(true){
      if(b_D0 > 10.0){
	if(b_MET < 50.0){
	  hFakeMET[0]->Fill(b_pT);
	}else if(b_MET > 50.0 && b_MET < 100.0){
	  hFakeMET[1]->Fill(b_pT);
	}else if(b_MET > 100.0){
	  hFakeMET[2]->Fill(b_pT);
	}
      }
      if(b_D0 > 10.0){
	if(b_MET < 100.0){
	  hFakeMET2[0]->Fill(b_pT);
	}else if(b_MET > 100.0 && b_MET < 150.0){
	  hFakeMET2[1]->Fill(b_pT);
	}else if(b_MET > 150.0 && b_MET < 200.0){
	  hFakeMET2[2]->Fill(b_pT);
	}else if(b_MET > 200.0){
	  hFakeMET2[3]->Fill(b_pT);
	}
      }
      /*
      if(b_D0 < 5.0){
	hFakeD0[0]->Fill(b_pT);
      }else if(b_D0 > 3.0 && b_D0 < 10.0){
	hFakeD0[1]->Fill(b_pT);
	//}else if(b_D0 > 10.0 && b_D0 < 50.0){
      }else if(b_D0 > 10.0){
	hFakeD0[2]->Fill(b_pT);
      }else if(b_D0 > 50.0){
	hFakeD0[3]->Fill(b_pT);
      }
      */
      if(b_D0 > 3.0 && b_D0 < 10.0){
	hFakeD0[0]->Fill(b_pT);
	//}else if(b_D0 > 10.0 && b_D0 < 50.0){
      }else if(b_D0 > 10.0 && b_D0 < 50.0){
	hFakeD0[1]->Fill(b_pT);
      }else if(b_D0 > 50.0){
	hFakeD0[2]->Fill(b_pT);
      }
    }
  }// for iEntry

  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");

  hD0Calo->Draw("colz");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hD0MET->Draw("colz");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hCaloMET->Draw("colz");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  c0->SetLogx();
  hPtCalo->Draw("colz");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  c0->SetLogx();
  c0->SetLogy();

  double NormMET[3] = {53.1296, 105.96, 51.7358};
  for(int i=0;i<3;i++){
    for(int iBin=0;iBin<nLogPt;iBin++){
      double BinVal = hFakeMET[i]->GetBinContent(iBin+1);
      double BinErr = hFakeMET[i]->GetBinError(iBin+1);

      hFakeMET[i]->SetBinContent(iBin+1, BinVal/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin]));
      hFakeMET[i]->SetBinError(  iBin+1, BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin]));

      grFakeMET[i]->SetPoint(iBin, XbinCenterVal[iBin], BinVal/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin])/NormMET[i]);
      grFakeMET[i]->SetPointEXlow(iBin , XbinCenterVal[iBin] - XBinsLogPt[iBin]);
      grFakeMET[i]->SetPointEXhigh(iBin, XBinsLogPt[iBin+1] - XbinCenterVal[iBin]);
      grFakeMET[i]->SetPointEYlow(iBin , BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin])/NormMET[i]);
      grFakeMET[i]->SetPointEYhigh(iBin, BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin])/NormMET[i]);
    }// for iBin

    hFrame->Draw();
    //hFakeMET[i]->Draw("same");
    grFakeMET[i]->Draw("sameEP");
    fFakeFuncMET[i]->SetParameter(0, 1.22);
    std::cout << "hFakeMET " << i << "  = " << hFakeMET[i]->Integral()<< std::endl;
    hFakeMET[i]->Scale(1.0/hFakeMET[i]->Integral());
    //hFakeMET[i]->Fit(fFakeFuncMET[i]);
    //hFakeMET[i]->Fit(fFakeFuncMET[i]);
    //hFakeMET[i]->Fit(fFakeFuncMET[i]);
    grFakeMET[i]->Fit(fFakeFuncMET[i]);
    grFakeMET[i]->Fit(fFakeFuncMET[i]);
    grFakeMET[i]->Fit(fFakeFuncMET[i]);
    //double p0 = hFakeMET[i]->GetFunction(Form("fFakeFuncMET_%d", i))->GetParameter(0);
    //double p1 = hFakeMET[i]->GetFunction(Form("fFakeFuncMET_%d", i))->GetParameter(1);
    double p0 = grFakeMET[i]->GetFunction(Form("fFakeFuncMET_%d", i))->GetParameter(0);
    double p1 = grFakeMET[i]->GetFunction(Form("fFakeFuncMET_%d", i))->GetParameter(1);
    std::cout << "FakeMET " << i << " : (p0, p1) = (" << p0 << ", " << p1 << ") " << std::endl;
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }// for i


  double NormD0[3] = {119.347, 83.6798, 127.146};
  for(int i=0;i<3;i++){
    for(int iBin=0;iBin<nLogPt;iBin++){
      double BinVal = hFakeD0[i]->GetBinContent(iBin+1);
      double BinErr = hFakeD0[i]->GetBinError(iBin+1);

      hFakeD0[i]->SetBinContent(iBin+1, BinVal/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin]));
      hFakeD0[i]->SetBinError(  iBin+1, BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin]));

      grFakeD0[i]->SetPoint(iBin, XbinCenterVal[iBin], BinVal/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin])/NormD0[i]);
      grFakeD0[i]->SetPointEXlow(iBin , XbinCenterVal[iBin] - XBinsLogPt[iBin]);
      grFakeD0[i]->SetPointEXhigh(iBin, XBinsLogPt[iBin+1] - XbinCenterVal[iBin]);
      grFakeD0[i]->SetPointEYlow(iBin , BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin])/NormD0[i]);
      grFakeD0[i]->SetPointEYhigh(iBin, BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin])/NormD0[i]);
    }// for iBin

    hFrame->Draw();
    //hFakeD0[i]->Draw("same");
    grFakeD0[i]->Draw("sameEP");
    fFakeFuncD0[i]->SetParameter(0, 1.22);
    std::cout << "hFakeD0 " << i << "  = " << hFakeD0[i]->Integral()<< std::endl;
    hFakeD0[i]->Scale(1.0/hFakeD0[i]->Integral());
    //hFakeD0[i]->Fit(fFakeFuncD0[i]);
    //hFakeD0[i]->Fit(fFakeFuncD0[i]);
    //hFakeD0[i]->Fit(fFakeFuncD0[i]);
    grFakeD0[i]->Fit(fFakeFuncD0[i]);
    grFakeD0[i]->Fit(fFakeFuncD0[i]);
    grFakeD0[i]->Fit(fFakeFuncD0[i]);
    double p0 = grFakeD0[i]->GetFunction(Form("fFakeFuncD0_%d", i))->GetParameter(0);
    double p1 = grFakeD0[i]->GetFunction(Form("fFakeFuncD0_%d", i))->GetParameter(1);
    std::cout << "FakeD0 " << i << " : (p0, p1) = (" << p0 << ", " << p1 << ") " << std::endl;
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }// for i

  hFrame->Draw();
  tl = new TLegend(0.20, 0.20, 0.65, 0.4);
  for(int i=0;i<3;i++){
    grFakeMET[i]->Draw("sameEP");
  }
  tl->AddEntry(grFakeMET[0], "MET < 50 GeV", "EPL");
  tl->AddEntry(grFakeMET[1], "50 GeV < MET < 100 GeV", "EPL");
  tl->AddEntry(grFakeMET[2], "100 GeV < MET", "EPL");
  tl->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  
  delete tl; tl=NULL;

  hFrame->Draw();
  for(int i=0;i<3;i++){
    grFakeD0[i]->Draw("sameEP");
  }
  tl = new TLegend(0.20, 0.20, 0.65, 0.4);
  tl->AddEntry(grFakeD0[0], "3.0 < d0sig < 10.0", "EPL");
  tl->AddEntry(grFakeD0[1], "10.0 < d0sig < 50.0", "EPL");
  tl->AddEntry(grFakeD0[2], "50.0 < d0sig", "EPL");
  tl->Draw("same");

  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  tfOutput = new TFile(Form("%s.root", PDFName.c_str()), "RECREATE");
  hD0Calo->Write();
  hD0MET->Write();
  hCaloMET->Write();
  hPtCalo->Write();

  for(int i=0;i<3;i++){
    hFakeMET[i]->Write();
  }
  for(int i=0;i<4;i++){
    hFakeD0[i]->Write();
  }
  tfOutput->Close();

  for(int i=0;i<4;i++){
    std::cout << "Hist " << i << " = " << hFakeMET2[i]->Integral() << std::endl;
    for(int iBin=0;iBin<12;iBin++){
      std::cout << hFakeMET2[i]->GetBinContent(iBin+1) << ", ";
    }
    std::cout << std::endl;
  }

  return 0;  
}

int LoadInput(void){
  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/tmp/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  hLumiHistData15 = (TH1F *)tfInput->Get("lumi_histo_data15");
  hLumiHistData16 = (TH1F *)tfInput->Get("lumi_histo_data16");
  hLumiHistData17 = (TH1F *)tfInput->Get("lumi_histo_data17");
  hLumiHistData18 = (TH1F *)tfInput->Get("lumi_histo_data18");
  hEleBG_TF_PtEta    = (TH2D *)tfInput->Get("hEleBG_TF_PtEta");
  hEleBG_TF_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_InvPtEta");
  hEleBG_TF_CaloIso_4to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_PtEta");
  hEleBG_TF_CaloIso_4to123_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_PtEta");
  hEleBG_TF_CaloIso_432to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_PtEta");
  hEleBG_TF_CaloIso_432to1_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_PtEta");
  hEleBG_TF_CaloIso_432to01_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_PtEta");
  hEleBG_TF_CaloIso_4to0_InvPtEta   = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_InvPtEta");
  hEleBG_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_InvPtEta");
  hEleBG_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_InvPtEta");
  hEleBG_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_InvPtEta");
  hEleBG_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_InvPtEta");
  hMuBG_TF_1Bin     = (TH1D *)tfInput->Get("hMuBG_TF_1Bin");
  hMuBG_TF_13Bin    = (TH1D *)tfInput->Get("hMuBG_TF_13Bin");
  hMuBG_TF_PtEta    = (TH2D *)tfInput->Get("hMuBG_TF_PtEta");
  hMuBG_TF_InvPtEta = (TH2D *)tfInput->Get("hMuBG_TF_InvPtEta");
  hMSBG_TF_PhiEta   = (TH2D *)tfInput->Get("hMSBG_TF_PhiEta");
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta    = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta");

  P_caloveto05_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto05_hadron_StdTrk");
  P_caloveto10_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto10_hadron_StdTrk");
  P_caloveto05_10_hadron_StdTrk   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_StdTrk");  
  P_caloveto05_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto05_hadron_Trklet");     
  P_caloveto10_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto10_hadron_Trklet");     
  P_caloveto05_10_hadron_Trklet   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_Trklet");  
							                                 
  P_caloveto05_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto05_electron_StdTrk");   
  P_caloveto10_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto10_electron_StdTrk");   
  P_caloveto05_10_electron_StdTrk = (TH1D *)tfInput->Get("P_caloveto05_10_electron_StdTrk");
  P_caloveto05_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto05_electron_Trklet");   
  P_caloveto10_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto10_electron_Trklet");   
  P_caloveto05_10_electron_Trklet = (TH1D *)tfInput->Get("P_caloveto05_10_electron_Trklet");

  hCorrHad = (TH1D *)tfInput->Get("hCorrHad_caloveto05");
  hCorrHad_Side = (TH1D *)tfInput->Get("hCorrHad_caloveto05_10");
  hCorrEle = (TH1D *)tfInput->Get("hCorrEle_caloveto05");
  hCorrEle_Side = (TH1D *)tfInput->Get("hCorrEle_caloveto05_10");

  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");
  gErrorIgnoreLevel = 1001;

  fSmearInt = new TF1("fSmearInt", [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
  fSmearInt->SetParameter(0, g_par_Mean);
  fSmearInt->SetParameter(1, g_par_Sigma);
  fSmearInt->SetParameter(2, g_par_Alpha);

  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i
  c0 = new TCanvas("c0", "", 800, 600);
  //c0->SetLogx();
  //c0->SetLogy();

  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nLogPt, XBinsLogPt);
  hData1D = new TH1D("hData1D", "hoge", 10, 0, 10);
  double xBins[961];
  xBins[0]=5.0;
  for(int i=1;i<=960;i++){
    xBins[i] = xBins[i-1]*(pow(12500.0/5.0, 1.0/960.0));
  }
  hSimpleSmear = new TH1D("hSimpleSmear", "", 960, xBins);

  hD0Calo = new TH2D("hD0Calo", ";d0 significance;calo clus [GeV]", 50, 0.0, 50.0, 54, -4.0, 50.0);
  hD0MET  = new TH2D("hD0MET", ";d0 significance;MET [GeV]", 50, 0.0, 50.0, 50, 0.0, 500.0);
  hCaloMET  = new TH2D("hCaloMET", ";calo clus [GeV];MET [GeV]", 54, -4.0, 50.0, 50, 0.0, 500.0);

  hPtCalo = new TH2D("hPtCalo", ";p_{T} [GeV];calo clus [GeV];", nLogPt, XBinsLogPt, 54, -4.0, 50.0);

  fFakeFunc = new TF1("fFakeFunc", "[1]*TMath::Exp(-[0]*log(x) - (0.183553 - 0.107612*[0])*log(x)*log(x))", 20, 12500);
  fFakeFunc->SetParameter(0, 1.22);

  for(int i=0;i<3;i++){
    hFakeMET[i] = (TH1D *)hTemplate_TrackPt->Clone(Form("hFakeMET_%d", i));
    hFakeMET[i]->SetLineColor(i+1);
    hFakeMET[i]->SetMarkerColor(i+1);
    fFakeFuncMET[i] = new TF1(Form("fFakeFuncMET_%d", i), "[1]*TMath::Exp(-[0]*log(x) - (0.183553 - 0.107612*[0])*log(x)*log(x))", 20, 12500);
    fFakeFuncMET[i]->SetParameter(0, 1.22);
    fFakeFuncMET[i]->SetLineColor(i+1);

    grFakeMET[i] = new TGraphAsymmErrors();
    grFakeMET[i]->SetLineColor(i+1);
    grFakeMET[i]->SetMarkerColor(i+1);
  }

  for(int i=0;i<4;i++){
    hFakeMET2[i] = (TH1D *)hTemplate_TrackPt->Clone(Form("hFakeMET2_%d", i));
    hFakeD0[i] = (TH1D *)hTemplate_TrackPt->Clone(Form("hFakeD0_%d", i));
    hFakeD0[i]->SetLineColor(i+1);
    hFakeD0[i]->SetMarkerColor(i+1);
    fFakeFuncD0[i] = new TF1(Form("fFakeFuncD0_%d", i), "[1]*TMath::Exp(-[0]*log(x) - (0.183553 - 0.107612*[0])*log(x)*log(x))", 20, 12500);
    fFakeFuncD0[i]->SetParameter(0, 1.22);
    fFakeFuncD0[i]->SetLineColor(i+1);

    grFakeD0[i] = new TGraphAsymmErrors();
    grFakeD0[i]->SetLineColor(i+1);
    grFakeD0[i]->SetMarkerColor(i+1);

  }

  hFrame = new TH2D("hFrame", ";tracklet p_{T} [GeV];a.u.", 50, 20, 12500, 50, 5e-7, 5e-1);

  return 0;
}
