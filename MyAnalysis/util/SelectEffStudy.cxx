#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TCanvas *c0;
const int nMC16 = 3;

const std::string PlotStatus="Internal";
//const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
const double IntLumi[nMC16]={2798.72 + 31260.7, 44083.8, 58125.9}; // special GRL pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];
const double dRThres=0.05;
std::string PDFName;

TFile *tfMyInput;
TFile *tfOutput;

const std::string dataName="/gpfs/fs6001/toshiaki/MakeCommon/RetrackedSample/out.RetrackedData.root";
const std::string MCName[nMC16]={"/gpfs/fs6001/toshiaki/MakeCommon/RetrackedSample/out.RetrackedZmumu_mc16a._000001.common_ntuple.root", "/gpfs/fs6001/toshiaki/MakeCommon/RetrackedSample/out.RetrackedZmumu_mc16d._000001.common_ntuple.root", "/gpfs/fs6001/toshiaki/MakeCommon/RetrackedSample/out.RetrackedZmumu_mc16e._000001.common_ntuple.root"};

TH1D *hData;
TH1D *hData_Ineff;
TH1D *hDataDirect_Ineff;
TH1D *hMC;
TH1D *hMC_Ineff;
TH1D *hMCDirect_Ineff;
TH1D *hMCDirect_woDeadMap_Ineff;
TH1D *hMC_tmp;
TH1D *hMC_Ineff_tmp;
TH1D *hMCDirect_Ineff_tmp;
TH1D *hMCDirect_woDeadMap_Ineff_tmp;
NtupleReader *myData;
NtupleReader *myMC[nMC16];
TGraphAsymmErrors *gDeadModule[nMC16];

bool fMC=false;
bool fMC16d=false;
std::string inData;
int CalcData(void);
int CalcMC(void);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("fMC", po::value<bool>()->default_value(false), "fMC")
    ("output-file,o", po::value<std::string>()->default_value("SelectEffStudy"), "output file")
    ("input-file,i", po::value<std::string>()->default_value(dataName), "input Files");
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  inData  = vm["input-file"].as<std::string>();
  fMC = vm["fMC"].as<bool>();

  hData = new TH1D("hData", "", 8, 0, 8);
  hData_Ineff = new TH1D("hData_Ineff", "", 19, 0, 19);
  hDataDirect_Ineff = new TH1D("hDataDirect_Ineff", "", 19, 0, 19);
  hMC = new TH1D("hMC", "", 8, 0, 8);
  hMC_Ineff = new TH1D("hMC_Ineff", "", 19, 0, 19);
  hMCDirect_Ineff = new TH1D("hMCDirect_Ineff", "", 19, 0, 19);
  hMCDirect_woDeadMap_Ineff = new TH1D("hMCDirect_woDeadMap_Ineff", "", 19, 0, 19);

  hMC_tmp = new TH1D("hMC_tmp", "", 8, 0, 8);
  hMC_Ineff_tmp = new TH1D("hMC_Ineff_tmp", "", 19, 0, 19);
  hMCDirect_Ineff_tmp = new TH1D("hMCDirect_Ineff_tmp", "", 19, 0, 19);
  hMCDirect_woDeadMap_Ineff_tmp = new TH1D("hMCDirect_woDeadMap_Ineff_tmp", "", 19, 0, 19);

  tfMyInput = new TFile("/gpfs/fs6001/toshiaki/Fitting/disappearingtracksearch_shapefitter/data/Input.root", "READ");
  gROOT->cd();
  gDeadModule[0] = (TGraphAsymmErrors *)tfMyInput->Get("SF_DeadModule_mc16a");
  gDeadModule[1] = (TGraphAsymmErrors *)tfMyInput->Get("SF_DeadModule_mc16d");
  gDeadModule[2] = (TGraphAsymmErrors *)tfMyInput->Get("SF_DeadModule_mc16e");

  if(fMC){
    CalcMC();
  }else{
    CalcData();
  }
  
  return 0;
}

int CalcData(void){
  /*  Data   */
  //myData = new NtupleReader(dataName);
  myData = new NtupleReader(inData);

  std::cout << "Data - nEntry : " << myData->nEntry << std::endl; 
  for(int iEntry=0;iEntry<(myData->nEntry);iEntry++){
    if((iEntry+1)%100000==0)
      std::cout << " data iEntry = " << iEntry+1 << std::endl;

    myData->GetEntry(iEntry);
    if(myData->GRL!=true || myData->DetectorError!=0 || myData->IsPassedBadEventVeto!=true)
      continue;

    if(myData->LU_SingleMuontrigger!=true)
      continue;

    if(myData->goodElectrons->size()!=0)
      continue;

    if(myData->goodMuons->size()<2)
      continue;

    if(myData->goodMuons->at(0)->p4.Pt()/1000.0 < 25.0)
      continue;
    if(myData->goodMuons->at(1)->p4.Pt()/1000.0 < 20.0)
      continue;

    hData->Fill(0.5); // 0
	  
    double RecoMass = (myData->goodMuons->at(0)->p4 + myData->goodMuons->at(1)->p4).M();
    int ChargePair = (myData->goodMuons->at(0)->Charge)*(myData->goodMuons->at(1)->Charge);
    
    if(TMath::Abs(RecoMass - Z_mass)/1000.0 > 10.0 || ChargePair==1)
	continue;

    hData->Fill(1.5); // 1

    for(unsigned int iTrack=0;iTrack<(myData->conventionalTracks->size());iTrack++){
      if(myData->conventionalTracks->at(iTrack)->PixelTracklet)
	continue;
      
      for(int iMuon=0;iMuon<2;iMuon++){	  
	double dR = myData->goodMuons->at(iMuon)->p4.DeltaR(myData->conventionalTracks->at(iTrack)->p4);
	if(dR > dRThres)
	  continue;

	hData->Fill(2.5); // 2
    
	int TrackQuality = MyUtil::CalcOld4LTrackQuality(myData->conventionalTracks->at(iTrack));
	if(MyUtil::IsPassed(TrackQuality, 0x737FC)==false)
	  continue;

	hData->Fill(3.5); // 3
	  
	for(unsigned int jTrack=0;jTrack<(myData->conventionalTracks->size());jTrack++){
	  if(myData->conventionalTracks->at(jTrack)->PixelTracklet==false)
	    continue;
	    
	  double dR_tracklet = myData->conventionalTracks->at(iTrack)->p4.DeltaR(myData->conventionalTracks->at(jTrack)->p4);
	  if(dR_tracklet > dRThres)
	    continue;

	  hData->Fill(4.5); // 4 
	    
	  int TrackletQuality = MyUtil::CalcOld4LTrackQuality(myData->conventionalTracks->at(jTrack));
	  for(int iBin=0;iBin<19;iBin++){
	    if(MyUtil::IsPassed(TrackletQuality, 1<<iBin))
	      hData_Ineff->Fill(iBin);
	  }
	  if(MyUtil::IsPassed(TrackletQuality, 0x733FC)==false)
	    continue;


	  hData->Fill(5.5); // 5
	}// for jTrack     	
      }// for iMuon
    }// for iTrack

    for(unsigned int iTrack=0;iTrack<(myData->conventionalTracks->size());iTrack++){
      if(myData->conventionalTracks->at(iTrack)->PixelTracklet==false)
	continue;
      
      for(int iMuon=0;iMuon<2;iMuon++){	  
	double dR = myData->goodMuons->at(iMuon)->p4.DeltaR(myData->conventionalTracks->at(iTrack)->p4);
	if(dR > dRThres)
	  continue;

	hData->Fill(6); // 6

	int TrackletQuality = MyUtil::CalcOld4LTrackQuality(myData->conventionalTracks->at(iTrack));
	  for(int iBin=0;iBin<19;iBin++){
	    if(MyUtil::IsPassed(TrackletQuality, 1<<iBin))
	      hDataDirect_Ineff->Fill(iBin);
	  }
	if(MyUtil::IsPassed(TrackletQuality, 0x733FC)==false)
	  continue;

	hData->Fill(7); // 7
	
      }// for iMuon
    }// for iTrack

  }// for iEntry

  for(int i=0;i<8;i++){
    std::cout << "i = " << i << " : " << hData->GetBinContent(i+1) << std::endl;
  }

  std::cout << " Eff " << std::endl;
  for(int i=0;i<19;i++){
    std::cout << "i = " << i << " : " << hData_Ineff->GetBinContent(i+1) << std::endl;

  }

  std::cout << " Direct Eff " << std::endl;
  for(int i=0;i<19;i++){
    std::cout << "i = " << i << " : " << hDataDirect_Ineff->GetBinContent(i+1) << std::endl;

  }

  tfOutput = new TFile(Form("%s.root", PDFName.c_str()), "RECREATE");
  hData->Write();
  hData_Ineff->Write();
  hDataDirect_Ineff->Write();
  
  return 0;
}

int CalcMC(void){
  /*  MC   */
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    if(iMC16==1)
      continue;
    myMC[iMC16] = new NtupleReader(MCName[iMC16]);
    
    double SF = IntLumi[iMC16]/(myMC[iMC16]->hSumOfWeightsBCK->GetBinContent(1));
    std::cout << "iMC16 = " << iMC16 << ", sumofweight = " << myMC[iMC16]->hSumOfWeightsBCK->GetBinContent(1) << std::endl;
    hMC_tmp->Reset();
    hMC_Ineff_tmp->Reset();
    hMCDirect_Ineff_tmp->Reset();
    hMCDirect_woDeadMap_Ineff_tmp->Reset();

    for(int iEntry=0;iEntry<(myMC[iMC16]->nEntry);iEntry++){
      if((iEntry+1)%100000==0){
	std::cout << " iMC" << iMC16 << " : " << iEntry+1 << std::endl;
      }
      myMC[iMC16]->GetEntry(iEntry);
      double weight = myMC[iMC16]->weightXsec * 
	myMC[iMC16]->weightMCweight * 
	myMC[iMC16]->weightPileupReweighting * 
	myMC[iMC16]->weightMuonSF * 
	myMC[iMC16]->weightMuonTriggerSF;

      if(myMC[iMC16]->IsPassedBadEventVeto!=true)
	continue;

      if(myMC[iMC16]->LU_SingleMuontrigger!=true)
	continue;

      if(myMC[iMC16]->goodElectrons->size()!=0)
	continue;

      if(myMC[iMC16]->goodMuons->size()<2)
	continue;

      if(myMC[iMC16]->goodMuons->at(0)->p4.Pt()/1000.0 < 25.0)
	continue;
      if(myMC[iMC16]->goodMuons->at(1)->p4.Pt()/1000.0 < 20.0)
	continue;

      hMC_tmp->Fill(0.5, weight); // 0
	  
      double RecoMass = (myMC[iMC16]->goodMuons->at(0)->p4 + myMC[iMC16]->goodMuons->at(1)->p4).M();
      int ChargePair = (myMC[iMC16]->goodMuons->at(0)->Charge)*(myMC[iMC16]->goodMuons->at(1)->Charge);
    
      if(TMath::Abs(RecoMass - Z_mass)/1000.0 > 10.0 || ChargePair==1)
	continue;

      hMC_tmp->Fill(1.5, weight); // 1

      for(unsigned int iTrack=0;iTrack<(myMC[iMC16]->conventionalTracks->size());iTrack++){
	if(myMC[iMC16]->conventionalTracks->at(iTrack)->PixelTracklet)
	  continue;
      
	for(int iMuon=0;iMuon<2;iMuon++){	  
	  double dR = myMC[iMC16]->goodMuons->at(iMuon)->p4.DeltaR(myMC[iMC16]->conventionalTracks->at(iTrack)->p4);
	  if(dR > dRThres)
	    continue;

	  hMC_tmp->Fill(2.5, weight); // 2
    
	  int TrackQuality = MyUtil::CalcOld4LTrackQuality(myMC[iMC16]->conventionalTracks->at(iTrack));
	  if(MyUtil::IsPassed(TrackQuality, 0x737FC)==false)
	    continue;

	  double Eta = myMC[iMC16]->conventionalTracks->at(iTrack)->p4.Eta();
	  double DeadWeight = gDeadModule[iMC16]->Eval(Eta);

	  hMC_tmp->Fill(3.5, DeadWeight*weight); // 3
	  
	  for(unsigned int jTrack=0;jTrack<(myMC[iMC16]->conventionalTracks->size());jTrack++){
	    if(myMC[iMC16]->conventionalTracks->at(jTrack)->PixelTracklet==false)
	      continue;
	    
	    double dR_tracklet = myMC[iMC16]->conventionalTracks->at(iTrack)->p4.DeltaR(myMC[iMC16]->conventionalTracks->at(jTrack)->p4);
	    if(dR_tracklet > dRThres)
	      continue;

	    hMC_tmp->Fill(4.5, DeadWeight*weight); // 4 
	    
	    int TrackletQuality = MyUtil::CalcOld4LTrackQuality(myMC[iMC16]->conventionalTracks->at(jTrack));
	    for(int iBin=0;iBin<19;iBin++){
	      if(MyUtil::IsPassed(TrackletQuality, 1<<iBin))
		hMC_Ineff_tmp->Fill(iBin, DeadWeight*weight);
	    }
	    if(MyUtil::IsPassed(TrackletQuality, 0x733FC)==false)
	      continue;

	    hMC_tmp->Fill(5.5, DeadWeight*weight); // 5
	  }// for jTrack     	
	}// for iMuon
      }// for iTrack


      for(unsigned int iTrack=0;iTrack<(myMC[iMC16]->conventionalTracks->size());iTrack++){
	if(myMC[iMC16]->conventionalTracks->at(iTrack)->PixelTracklet==false)
	  continue;
      
	for(int iMuon=0;iMuon<2;iMuon++){	  
	  double dR = myMC[iMC16]->goodMuons->at(iMuon)->p4.DeltaR(myMC[iMC16]->conventionalTracks->at(iTrack)->p4);
	  if(dR > dRThres)
	    continue;

	  hMC_tmp->Fill(6.5, weight); // 6

	  double Eta = myMC[iMC16]->conventionalTracks->at(iTrack)->p4.Eta();
	  double DeadWeight = gDeadModule[iMC16]->Eval(Eta);

	  int TrackletQuality = MyUtil::CalcOld4LTrackQuality(myMC[iMC16]->conventionalTracks->at(iTrack));
	  for(int iBin=0;iBin<19;iBin++){
	    if(MyUtil::IsPassed(TrackletQuality, 1<<iBin)){
	      hMCDirect_Ineff_tmp->Fill(iBin, DeadWeight*weight);
	      hMCDirect_woDeadMap_Ineff_tmp->Fill(iBin, weight);
	    }
	  }
	  if(MyUtil::IsPassed(TrackletQuality, 0x733FC)==false)
	    continue;
	  
	  hMC_tmp->Fill(7.5, DeadWeight*weight); // 7
	  //hMC_tmp->Fill(7.5, weight); // 7
	  
	}// for iMuon
      }// for iTrack
                        
    }// for iEntry
    hMC->Add(hMC_tmp, SF);
    hMC_Ineff->Add(hMC_Ineff_tmp, SF);
    hMCDirect_Ineff->Add(hMCDirect_Ineff_tmp, SF);
    hMCDirect_woDeadMap_Ineff->Add(hMCDirect_woDeadMap_Ineff_tmp, SF);
  }// for iMC16

  for(int i=0;i<8;i++){
    std::cout << "i = " << i << " : " << hMC->GetBinContent(i+1) << std::endl;
  }

  std::cout << " Eff " << std::endl;
  for(int i=0;i<19;i++){
    std::cout << "i = " << i << " : " << hMC_Ineff->GetBinContent(i+1) << std::endl;

  }

  std::cout << " Eff " << std::endl;
  for(int i=0;i<19;i++){
    std::cout << "i = " << i << " : " << hMC_Ineff->GetBinContent(i+1) << std::endl;

  }

  //tfOutput = new TFile(Form("%s.root", PDFName.c_str()), "RECREATE");
  tfOutput = new TFile("tfMC.root", "RECREATE");
  hMC->Write();
  hMC_Ineff->Write();
  hMCDirect_Ineff->Write();
  hMCDirect_woDeadMap_Ineff->Write();
  
  return 0;
}
