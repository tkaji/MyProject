#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TCanvas *c0;
std::string PDFName;

TFile *tfMyInput;
TFile *tfOutput;

NtupleReader *myData;

const int nCut=14;
TH1D *hCutflow_EWK;
TH1D *hCutflow_Strong;

TH1D *hInvCutflow_EWK;
TH1D *hInvCutflow_Strong;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("output-file,o", po::value<std::string>()->default_value("MakeCutflowData"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");

  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  std::vector<std::string> ReadableInputFileList;
  ReadableInputFileList = InputFileList;

  std::string OutputFileName = vm["output-file"].as<std::string>();
  hCutflow_EWK    = new TH1D("hCutflow_EWK"   , "", nCut, 0, nCut);
  hCutflow_Strong = new TH1D("hCutflow_Strong", "", nCut, 0, nCut);

  //hInvCutflow_EWK    = new TH1D("hInvCutflow_EWK"   , "", nCut, 0, nCut);
  //hInvCutflow_Strong = new TH1D("hInvCutflow_Strong", "", nCut, 0, nCut);

  unsigned int nFile = ReadableInputFileList.size();
  std::cout << "nFile : " << nFile << std::endl;

  for(unsigned int iFile=0;iFile<nFile;iFile++){
    myData = new NtupleReader(ReadableInputFileList.at(iFile));
    
    for(int iEntry=0;iEntry<(myData->nEntry);iEntry++){
      myData->GetEntry(iEntry);

      // 0 : All event
      hCutflow_EWK->Fill(0);      hCutflow_Strong->Fill(0);
      //hInvCutflow_EWK->Fill(0);      hInvCutflow_Strong->Fill(0);

      if(((myData->GRL==true) && (myData->DetectorError==0) && (myData->IsPassedBadEventVeto==true) && (myData->LU_METtrigger_SUSYTools==true))==false)
	continue;
      // 1 : Trigger
      hCutflow_EWK->Fill(1);      hCutflow_Strong->Fill(1);
      //hInvCutflow_EWK->Fill(1);      hInvCutflow_Strong->Fill(1);
      
      if(myData->IsPassedLeptonVeto==false)
	continue;
      // 2 : Lepton VETO
      hCutflow_EWK->Fill(2);      hCutflow_Strong->Fill(2);
      //hInvCutflow_EWK->Fill(2);      hInvCutflow_Strong->Fill(2);

      bool fEWK=false;
      bool fStrong=false;

      double MET    = myData->missingET->p4.Pt()/1000.0;
      double JetPt0 = (myData->goodJets->size()>0) ? myData->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
      double JetPt1 = (myData->goodJets->size()>1) ? myData->goodJets->at(1)->p4.Pt()/1000.0 : 0.0;
      double JetPt2 = (myData->goodJets->size()>2) ? myData->goodJets->at(2)->p4.Pt()/1000.0 : 0.0;
      double dPhi   = myData->JetMetdPhiMin50;
      if(MET > 200.0){
	hCutflow_EWK->Fill(3);
	if(JetPt0 > 100.0){
	  hCutflow_EWK->Fill(4);
	  hCutflow_EWK->Fill(5);
	  hCutflow_EWK->Fill(6);
	  if(dPhi > 1.0){
	    hCutflow_EWK->Fill(7);
	    fEWK=true;
	  }// dPhi
	}// Jet
      }// MET

      if(MET > 250.0){
	hCutflow_Strong->Fill(3);
	if(JetPt0 > 100.0){
	  hCutflow_Strong->Fill(4);
	  if(JetPt1 > 20.0){
	    hCutflow_Strong->Fill(5);
	    if(JetPt2 > 20.0){
	      hCutflow_Strong->Fill(6);
	      if(dPhi > 0.4){
		hCutflow_Strong->Fill(7);
		fStrong=true;
	      }// dPhi	      
	    }// 3rd jet
	  }// 2nd jet
	}// 1st jet
      }// MET
      
      
      if(fEWK==false && fStrong==false)
	continue;
      
      bool fTracklet  = false; // 8
      bool fQuality   = false; // 9
      bool fIsolation = false; // 10
      bool fGeometry  = false; // 11
      bool fCaloveto  = false; // 12
      bool fPt        = false; // 13
      
      int IsoTrackID = -1.0;
      int IsoTrackPt =  0.0;
      int IsoTrackletID = -1.0;
      int IsoTrackletPt =  0.0;

      /*   Calculate Isolated Highest Track/Tracklet Pt   */
      for(unsigned int iTrack=0;iTrack<(myData->conventionalTracks->size());iTrack++){
	if(myData->conventionalTracks->at(iTrack)->PixelTracklet){
	  if( ((myData->conventionalTracks->at(iTrack)->ptcone40overPt_1gev) < 0.04 ) &&
	      (IsoTrackletPt < (myData->conventionalTracks->at(iTrack)->p4.Pt())) ){
	    IsoTrackletPt = myData->conventionalTracks->at(iTrack)->p4.Pt();
	    IsoTrackletID = iTrack;
	  }
	}else{
	  if( ((myData->conventionalTracks->at(iTrack)->ptcone40overPt_1gev) < 0.04 ) &&
	      (IsoTrackPt < (myData->conventionalTracks->at(iTrack)->p4.Pt())) ){
	    IsoTrackPt = myData->conventionalTracks->at(iTrack)->p4.Pt();
	    IsoTrackID = iTrack;
	  }
	}// standard track
      }// for iTrack
      
      /*     Leading    */
      for(unsigned int iTrack=0;iTrack<(myData->conventionalTracks->size());iTrack++){
	if(myData->conventionalTracks->at(iTrack)->PixelTracklet){
	  if((int)iTrack == IsoTrackletID)
	    myData->conventionalTracks->at(iTrack)->IsPassedIsolatedLeading = true;
	}else{
	  if((int)iTrack == IsoTrackID   )
	    myData->conventionalTracks->at(iTrack)->IsPassedIsolatedLeading = true;
	}

	int nTrackLayer  = MyUtil::CalcNumberOfBarrelOnlyLayer(myData->conventionalTracks->at(iTrack));
	if((nTrackLayer==4) && (myData->conventionalTracks->at(iTrack)->p4.Pt()/1000.0 > 20.0)
	   && (myData->conventionalTracks->at(iTrack)->nSCTHits==0)
	   && (myData->conventionalTracks->at(iTrack)->numberOfContribPixelLayers >= 4)
	   && (myData->conventionalTracks->at(iTrack)->numberOfGangedFlaggedFakes==0)
	   && (myData->conventionalTracks->at(iTrack)->nSiHoles==0)
	   && (myData->conventionalTracks->at(iTrack)->numberOfPixelSpoiltHits==0)
	   && (myData->conventionalTracks->at(iTrack)->nPixelOutliers==0)){
	  fTracklet=true;

	  if((TMath::Abs(myData->conventionalTracks->at(iTrack)->d0sigTool)       < 1.5) &&
	     (TMath::Abs(myData->conventionalTracks->at(iTrack)->z0sinthetawrtPV) < 0.5) &&
	     (myData->conventionalTracks->at(iTrack)->Quality > 0.1)){
	    fQuality=true;
	    
	    if((myData->conventionalTracks->at(iTrack)->ptcone40overPt_1gev < 0.04) &&
	       (myData->conventionalTracks->at(iTrack)->IsPassedIsolatedLeading==true) &&
	       (myData->conventionalTracks->at(iTrack)->dRJet50 > 0.4) &&
	       (myData->conventionalTracks->at(iTrack)->dRElectron > 0.4) &&
	       (myData->conventionalTracks->at(iTrack)->dRMuon > 0.4) &&
	       (myData->conventionalTracks->at(iTrack)->dRMSTrack > 0.4)){
	      fIsolation=true;
	      
	      if((TMath::Abs(myData->conventionalTracks->at(iTrack)->p4.Eta()) > 0.1) &&
		 (TMath::Abs(myData->conventionalTracks->at(iTrack)->p4.Eta()) < 1.9)){
		fGeometry=true;
		
		if(myData->conventionalTracks->at(iTrack)->etclus20_topo/1000.0 < 5.0){
		  fCaloveto=true;
		  
		  if(myData->conventionalTracks->at(iTrack)->p4.Pt()/1000.0 > 60.0){
		    fPt=true;
		  }// Ptcut
		}// Caloveto
	      }// Geometry
	    }// Isolation
	  }// Quality	  
	}// fTracklet	
      }// for iTrack
      
      if(fEWK){
	if(fTracklet)  hCutflow_EWK ->Fill( 8);
	if(fQuality)   hCutflow_EWK ->Fill( 9);
	if(fIsolation) hCutflow_EWK ->Fill(10);
	if(fGeometry)  hCutflow_EWK ->Fill(11);
	if(fCaloveto)  hCutflow_EWK ->Fill(12);
	if(fPt)        hCutflow_EWK ->Fill(13);
      }

      if(fStrong){
	if(fTracklet)  hCutflow_Strong ->Fill( 8);
	if(fQuality)   hCutflow_Strong ->Fill( 9);
	if(fIsolation) hCutflow_Strong ->Fill(10);
	if(fGeometry)  hCutflow_Strong ->Fill(11);
	if(fCaloveto)  hCutflow_Strong ->Fill(12);
	if(fPt)        hCutflow_Strong ->Fill(13);
      }

    }// for iEntry    
    delete myData; myData=NULL;
  }// for iFile

  tfOutput = new TFile(OutputFileName.c_str(), "RECREATE");
  hCutflow_EWK->Write("", TObject::kOverwrite);
  hCutflow_Strong->Write("", TObject::kOverwrite);

  return 0;
}
