#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include "METTrigStudy.h"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TCanvas *c0;
std::string PDFName;
TH1D *hTrackPt;

const int nBinsMySignal = 12;
const double XBinsMySignal[nBinsMySignal+1] = {20, 26.1532, 34.1995, 44.7214, 60.0, 76.4724, 100, 170.998, 292.402, 500, 1118.03, 2500, 12500};
const std::string DirName="/gpfs/fs6001/toshiaki/Common_v2_3_4_tmp";
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};

TFile *tfTrig;
TFile *tfInput;
TFile *tfOutput;
const int nMC16=3;
const double IntLumi[nMC16]={2798.72 + 31260.7, 44083.8, 58125.9}; // special GRL pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];

TH2D *hFrame;
TH2D *hTrigMETJetEff[nMETTrig];
TGraphAsymmErrors *gSF_DeadModule[nMC16];

double crystallBallIntegral(double* x, double *par);
double GetTrigEffVal(UInt_t v_runNum, double v_METPt, double v_JetPt);
double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau);

NtupleReader *mySignal;

const int nCut=14;
const std::string CutName[nCut] = {"All events", "Trigger", "Lepton VETO", "MET", "1st jet", "2nd jet", "3rd jet", "dPhi", 
				   "Pixel Tracklet", "Quality cut", "Isolation cut", "Geometrical cut", "Calo-veto", "pT cut"};
TH1D *hCutflow_Cleaning;
TH1D *hCutflow;
TH1D *hCutflow_EWK;
TH1D *hCutflow_Strong;
bool fReweight=false;
double TargetLifetime;
int iChain;
std::vector<int>vDSID;

//std::vector<int> vDSID_Signal = {448304, 448305}; // 0p2
std::vector<int> vDSID_Signal = {448496, 448497};   // 1p0
//std::vector<int> vDSID_Wmunu  = {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169};

TH1D *hSignal_All;
TH1D *hSignal_Pass;
TH1D *hWmunu_All;
TH1D *hWmunu_Pass;
TGraphAsymmErrors *gSignal;
TGraphAsymmErrors *gWmunu;

const int nTrig=8;
TH1D *hSignal_All_iTrig[nTrig];
TH1D *hSignal_Pass_iTrig[nTrig];
TH1D *hWmunu_All_iTrig[nTrig];
TH1D *hWmunu_Pass_iTrig[nTrig];
TGraphAsymmErrors *gSignal_iTrig[nTrig];
TGraphAsymmErrors *gWmunu_iTrig[nTrig];

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("iChain", po::value<int>()->default_value(1), "1: EWK, 3: Strong")
    ("output-file,o", po::value<std::string>()->default_value("METTrigStudy"), "output file");
  
  po::positional_options_description p;
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  iChain         = vm["iChain"].as<int>();

  std::string OutputFileName = vm["output-file"].as<std::string>();

  hSignal_All  = new TH1D("hSignal_All" , "", 500, 50, 500);
  hSignal_Pass = new TH1D("hSignal_Pass", "", 500, 50, 500);
  hWmunu_All  = new TH1D("hWmunu_All" , "", 500, 50, 500);
  hWmunu_Pass = new TH1D("hWmunu_Pass", "", 500, 50, 500);
  
  for(int iTrig=0;iTrig<nTrig;iTrig++){
    hSignal_All_iTrig[iTrig]  = new TH1D(Form("hSignal_All_iTrig_%d" , iTrig), "", 500, 50, 500);
    hSignal_Pass_iTrig[iTrig] = new TH1D(Form("hSignal_Pass_iTrig_%d", iTrig), "", 500, 50, 500);
    hWmunu_All_iTrig[iTrig]   = new TH1D(Form("hWmunu_All_iTrig_%d"  , iTrig), "", 500, 50, 500);
    hWmunu_Pass_iTrig[iTrig]  = new TH1D(Form("hWmunu_Pass_iTrig_%d" , iTrig), "", 500, 50, 500);
  }// for iTrig

  std::vector<int> vDSID = vDSID_Signal;

  for(int iMC16=0;iMC16<nMC16;iMC16++){
    for(unsigned int iDSID=0;iDSID<vDSID.size();iDSID++){
      mySignal = new NtupleReader(Form("%s/out.%d_%s._000001.common_ntuple.root", DirName.c_str(), vDSID.at(iDSID), MCName[iMC16].c_str()));
      double vSF = IntLumi[iMC16]/mySignal->hSumOfWeightsBCK->GetBinContent(1);

      for(int iEntry=0;iEntry<(mySignal->nEntry);iEntry++){
	mySignal->GetEntry(iEntry);
	double weight = vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting);

	bool fKinematics=false;
	double MET    = mySignal->missingET->p4.Pt()/1000.0;
	double JetPt0 = (mySignal->goodJets->size()>0) ? mySignal->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
	double JetEta0 = (mySignal->goodJets->size()>0) ? mySignal->goodJets->at(0)->p4.Eta() : 999;
	double JetPt1 = (mySignal->goodJets->size()>1) ? mySignal->goodJets->at(1)->p4.Pt()/1000.0 : 0.0;
	double JetPt2 = (mySignal->goodJets->size()>2) ? mySignal->goodJets->at(2)->p4.Pt()/1000.0 : 0.0;
	double dPhi   = mySignal->JetMetdPhiMin50;
	//double TrigVal = GetTrigEffVal(mySignal->randomRunNumber, MET, JetPt0);

	//weight *= TrigVal;
	if(mySignal->IsPassedBadEventVeto==false)
	  continue;

	if(MET > KinematicsChain[iChain][0]){
	  if(JetPt0 > KinematicsChain[iChain][1]){
	    if(JetPt1 > KinematicsChain[iChain][2]){
	      if(JetPt2 > KinematicsChain[iChain][3]){
		if(dPhi > KinematicsChain[iChain][5]){
		  fKinematics=true;
		}// dPhi
	      }// 3rd Jet
	    }// 2nd Jet
	  }// 1st Jet
	}// MET
	if(fKinematics==false)
	  continue;

	hSignal_All->Fill(MET, weight);
	if(mySignal->LU_METtrigger_SUSYTools==true){
	  hSignal_Pass->Fill(MET, weight);
	}
      }// for iEntry

      delete mySignal; mySignal=NULL;
    }// for iDSID
  }//for iMC16

  //vDSID = vDSID_Wmunu;
  
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    //for(unsigned int iDSID=0;iDSID<vDSID.size();iDSID++){
    for(unsigned int iDSID=0;iDSID<vWmunu.at(iMC16).size();iDSID++){

      //mySignal = new NtupleReader(Form("%s/SMBG/out.%d_%s._000001.common_ntuple.root", DirName.c_str(), vDSID.at(iDSID), MCName[iMC16].c_str()));
      mySignal = new NtupleReader(Form("root://eosatlas//eos/atlas/unpledged/group-tokyo/datafiles/SUSY/tkaji/mc16_13TeV/%s", vWmunu.at(iMC16).at(iDSID).c_str()));
      double vSF = IntLumi[iMC16]/mySignal->hSumOfWeightsBCK->GetBinContent(1);

      for(int iEntry=0;iEntry<(mySignal->nEntry);iEntry++){
	mySignal->GetEntry(iEntry);
	double weight = vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting);

	bool fKinematics=false;
	double MET    = mySignal->missingET_XeTrigger->p4.Pt()/1000.0;
	double JetPt0 = (mySignal->goodJets->size()>0) ? mySignal->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
	double JetEta0 = (mySignal->goodJets->size()>0) ? mySignal->goodJets->at(0)->p4.Eta() : 999;
	double JetPt1 = (mySignal->goodJets->size()>1) ? mySignal->goodJets->at(1)->p4.Pt()/1000.0 : 0.0;
	double JetPt2 = (mySignal->goodJets->size()>2) ? mySignal->goodJets->at(2)->p4.Pt()/1000.0 : 0.0;
	double dPhi   = mySignal->JetMetdPhiMin50_XeTrigger;
	//double TrigVal = GetTrigEffVal(mySignal->randomRunNumber, MET, JetPt0);

	//weight *= TrigVal;
	if(mySignal->IsPassedBadEventVeto==false)
	  continue;

	if(MET > KinematicsChain[iChain][0]){
	  if(JetPt0 > KinematicsChain[iChain][1]){
	    if(JetPt1 > KinematicsChain[iChain][2]){
	      if(JetPt2 > KinematicsChain[iChain][3]){
		if(dPhi > KinematicsChain[iChain][5]){
		  fKinematics=true;
		}// dPhi
	      }// 3rd Jet
	    }// 2nd Jet
	  }// 1st Jet
	}// MET
	if(fKinematics==false)
	  continue;

	hWmunu_All->Fill(MET, weight);
	if(mySignal->LU_METtrigger_SUSYTools==true){
	  hWmunu_Pass->Fill(MET, weight);
	}
      }// for iEntry

      delete mySignal; mySignal=NULL;
    }// for iDSID
  }//for iMC16

  gSignal = new TGraphAsymmErrors(hSignal_Pass, hSignal_All);
  gSignal->SetName("gSignal");
  gSignal->SetLineWidth(2);
  gSignal->SetLineColor(kRed);
  gSignal->SetMarkerStyle(8);
  gSignal->SetMarkerSize(1.0);
  gSignal->SetMarkerColor(kRed);
  gWmunu = new TGraphAsymmErrors(hWmunu_Pass, hWmunu_All);
  gWmunu->SetName("gWmunu");
  gWmunu->SetLineWidth(2);
  gWmunu->SetLineColor(kBlue);
  gWmunu->SetMarkerStyle(8);
  gWmunu->SetMarkerSize(1.0);
  gWmunu->SetMarkerColor(kBlue);

  c0 = new TCanvas("c0", "c0", 800, 600);
  hFrame = new TH2D("hFrame", ";E^{miss}_{T};Efficiency", 50, 0, 500, 55, 0.0, 1.1);
  hFrame->Draw();
  gSignal->Draw("samePE");
  gWmunu->Draw("samePE");
  c0->SaveAs(Form("%s.pdf", OutputFileName.c_str()), "pdf");

  tfOutput = new TFile(Form("%s.root", OutputFileName.c_str()), "RECREATE");
  hSignal_All->Write();
  hSignal_Pass->Write();
  gSignal->Write();
  hWmunu_All->Write();
  hWmunu_Pass->Write();
  gWmunu->Write();

  return 0;
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

double GetTrigEffVal(UInt_t v_runNum, double v_METPt, double v_JetPt){
  int iTrig = MyUtil::GetUnprescaledMETTrigger(v_runNum);
  int iX = hTrigMETJetEff[iTrig]->GetXaxis()->FindBin(v_METPt);
  int iY = hTrigMETJetEff[iTrig]->GetYaxis()->FindBin(v_JetPt);
  if(iX==0) iX=1;
  if(iY==0) iY=1;
  if(iX==(hTrigMETJetEff[iTrig]->GetNbinsX()+1)) iX=hTrigMETJetEff[iTrig]->GetNbinsX();
  if(iY==(hTrigMETJetEff[iTrig]->GetNbinsY()+1)) iY=hTrigMETJetEff[iTrig]->GetNbinsY();
  
  double ret = hTrigMETJetEff[iTrig]->GetBinContent(iX, iY);
  if(isnan(ret) || ret < 0.0){
    return 0.0;
  }else{
    return hTrigMETJetEff[iTrig]->GetBinContent(iX, iY);
  }
}



double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau){
  if(TMath::Abs(PropTime) > 1e10)
    return 1.0;
  else
    return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
}
