#ifndef SelectEffStudy_h
#define SelectEffStudy_h

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"

const std::string UpDown[2] = {"__1up", "__1down"};
const int nSyst_JES = 7;
const int nSyst_JER = 8;
const int nSyst_JVT = 2;
const int nSyst_PRW = 1;
const int nSyst_TST = 4;
const std::string SystName_JES[nSyst_JES] = {"JET_EtaIntercalibration_NonClosure_highE",
					     "JET_EtaIntercalibration_NonClosure_negEta",
					     "JET_EtaIntercalibration_NonClosure_posEta",
					     "JET_Flavor_Response",
					     "JET_GroupedNP_1",
					     "JET_GroupedNP_2",
					     "JET_GroupedNP_3"};

const std::string SystName_JER[nSyst_JER] = {"JET_JER_DataVsMC_MC16",
					     "JET_JER_EffectiveNP_1",
					     "JET_JER_EffectiveNP_2",
					     "JET_JER_EffectiveNP_3",
					     "JET_JER_EffectiveNP_4",
					     "JET_JER_EffectiveNP_5",
					     "JET_JER_EffectiveNP_6",
					     "JET_JER_EffectiveNP_7restTerm"};
const std::string SystName_JVT[nSyst_JVT] = {"JET_JvtEfficiency",
					     "JET_fJvtEfficiency"};
const std::string SystName_PRW[nSyst_PRW] = {"PRW_DATASF"};
const std::string SystName_TST[nSyst_TST] = {"MET_SoftTrk_ResoPara",
					     "MET_SoftTrk_ResoPerp",
					     "MET_SoftTrk_ScaleDown",
					     "MET_SoftTrk_ScaleUp"};


class mySignalSyst{
 public:
  mySignalSyst(const std::string);
  ~mySignalSyst();

  int nEntry;
  //private:
  TFile *m_file;
  TTree *m_tree;
  TH1D *hnEventsProcessedBCK;
  TH1D *hnPrimaryVertex;
  TH1D *hProdType;
  TH1D *hSumOfNumber;
  TH1D *hSumOfWeights;
  TH1D *hSumOfWeightsBCK;
  TH1D *hSumOfWeightsSquaredBCK;

  float weightXsec;
  float weightMCweight;
  float weightPileupReweighting;

  float actualInteractionsPerCrossing;
  float averageInteractionsPerCrossing;
  float CorrectedAverageInteractionsPerCrossing;
  int treatAsYear;
  int prodType;
  UInt_t randomRunNumber;
  UInt_t RunNumber;
  UInt_t lumiBlock;
  ULong64_t EventNumber;
  int DetectorError;
  UInt_t nPrimaryVertex;
  float nTracksFromPrimaryVertex;

  float METPt;
  float JetPt;
  bool  IsPassedKinematics_EWK;
  bool  IsPassedKinematics_Strong;

  float METPt_JES[2][nSyst_JES];
  float JetPt_JES[2][nSyst_JES];
  float weightPileupReweighting_JES[2][nSyst_JES];
  bool  IsPassedKinematics_EWK_JES[2][nSyst_JES];
  bool  IsPassedKinematics_Strong_JES[2][nSyst_JES];

  float METPt_JER[2][nSyst_JER];
  float JetPt_JER[2][nSyst_JER];
  float weightPileupReweighting_JER[2][nSyst_JER];
  bool  IsPassedKinematics_EWK_JER[2][nSyst_JER];
  bool  IsPassedKinematics_Strong_JER[2][nSyst_JER];
  
  float METPt_JVT[2][nSyst_JVT];
  float JetPt_JVT[2][nSyst_JVT];
  float weightPileupReweighting_JVT[2][nSyst_JVT];
  bool  IsPassedKinematics_EWK_JVT[2][nSyst_JVT];
  bool  IsPassedKinematics_Strong_JVT[2][nSyst_JVT];
  
  float METPt_PRW[2][nSyst_PRW];
  float JetPt_PRW[2][nSyst_PRW];
  float weightPileupReweighting_PRW[2][nSyst_PRW];
  bool  IsPassedKinematics_EWK_PRW[2][nSyst_PRW];
  bool  IsPassedKinematics_Strong_PRW[2][nSyst_PRW];
  
  float METPt_TST[nSyst_TST];
  float JetPt_TST[nSyst_TST];
  float weightPileupReweighting_TST[nSyst_TST];
  bool  IsPassedKinematics_EWK_TST[nSyst_TST];
  bool  IsPassedKinematics_Strong_TST[nSyst_TST];
};

mySignalSyst::mySignalSyst(const std::string FileName)
: nEntry(-1),m_file(NULL),m_tree(NULL),hnEventsProcessedBCK(NULL),hnPrimaryVertex(NULL),hProdType(NULL),hSumOfNumber(NULL),hSumOfWeights(NULL),hSumOfWeightsBCK(NULL),hSumOfWeightsSquaredBCK(NULL),weightXsec(0),weightMCweight(0),weightPileupReweighting(0),actualInteractionsPerCrossing(-1),averageInteractionsPerCrossing(-1),CorrectedAverageInteractionsPerCrossing(-1),treatAsYear(-1),prodType(-1),randomRunNumber(0),RunNumber(0),lumiBlock(0),EventNumber(-1),DetectorError(-1),nPrimaryVertex(0),nTracksFromPrimaryVertex(-1)
{
  // open input file
  m_file = TFile::Open(FileName.c_str(), "READ");
  gROOT->cd();

  hnEventsProcessedBCK    = (TH1D *)m_file->Get("nEventsProcessedBCK");
  hnPrimaryVertex         = (TH1D *)m_file->Get("nPrimaryVertex");
  hProdType               = (TH1D *)m_file->Get("prodType");
  hSumOfNumber            = (TH1D *)m_file->Get("sumOfNumber");
  hSumOfWeights           = (TH1D *)m_file->Get("sumOfWeights");
  hSumOfWeightsBCK        = (TH1D *)m_file->Get("sumOfWeightsBCK");
  hSumOfWeightsSquaredBCK = (TH1D *)m_file->Get("sumOfWeightsSquaredBCK");

  m_tree = (TTree *)m_file->Get("tree");

  m_tree->SetBranchAddress("weightXsec"                , &weightXsec);
  m_tree->SetBranchAddress("weightMCweight"            , &weightMCweight);
  m_tree->SetBranchAddress("weightPileupReweighting"   , &weightPileupReweighting);

  m_tree->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing);
  m_tree->SetBranchAddress("CorrectedAverageInteractionsPerCrossing", &CorrectedAverageInteractionsPerCrossing);
  m_tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  m_tree->SetBranchAddress("treatAsYear", &treatAsYear);
  m_tree->SetBranchAddress("prodType", &prodType);
  m_tree->SetBranchAddress("randomRunNumber", &randomRunNumber);
  m_tree->SetBranchAddress("RunNumber", &RunNumber);
  m_tree->SetBranchAddress("lumiBlock", &lumiBlock);
  m_tree->SetBranchAddress("EventNumber", &EventNumber);
  m_tree->SetBranchAddress("DetectorError", &DetectorError);
  m_tree->SetBranchAddress("nPrimaryVertex", &nPrimaryVertex);
  m_tree->SetBranchAddress("nTracksFromPrimaryVertex", &nTracksFromPrimaryVertex);

  m_tree->SetBranchAddress("METPt", &METPt);
  m_tree->SetBranchAddress("JetPt", &JetPt);
  m_tree->SetBranchAddress("IsPassedKinematics_EWK", &IsPassedKinematics_EWK);
  m_tree->SetBranchAddress("IsPassedKinematics_Strong", &IsPassedKinematics_Strong);

  for(int iSys=0;iSys<nSyst_JES;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &METPt_JES[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_JES[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_JES[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_JES[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_JES[iUp][iSys] );
    }
  }// for iSyst_JES

  for(int iSys=0;iSys<nSyst_JER;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &METPt_JER[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_JER[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_JER[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_JER[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_JER[iUp][iSys] );
    }
  }// for iSyst_JER

  for(int iSys=0;iSys<nSyst_JVT;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &METPt_JVT[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_JVT[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_JVT[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_JVT[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_JVT[iUp][iSys] );
    }
  }// for iSyst_JVT

  for(int iSys=0;iSys<nSyst_PRW;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &METPt_PRW[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_PRW[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_PRW[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_PRW[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_PRW[iUp][iSys] );
    }
  }// for iSyst_PRW

  for(int iSys=0;iSys<nSyst_TST;iSys++){
    m_tree->SetBranchAddress(Form("METPt%s"                    , SystName_TST[iSys].c_str()), &METPt_TST[iSys]                     );
    m_tree->SetBranchAddress(Form("JetPt%s"                    , SystName_TST[iSys].c_str()), &JetPt_TST[iSys]                     );
    m_tree->SetBranchAddress(Form("weightPileupReweighting%s"  , SystName_TST[iSys].c_str()), &weightPileupReweighting_TST[iSys]   );
    m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s"   , SystName_TST[iSys].c_str()), &IsPassedKinematics_EWK_TST[iSys]    );
    m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s", SystName_TST[iSys].c_str()), &IsPassedKinematics_Strong_TST[iSys] );
  }// for iSyst_TST
  
  nEntry = m_tree->GetEntries();
  
  std::cout << "SignalSyst :: Input File : " << FileName << std::endl;
  std::cout << "SignalSyst :: Entries    : " << nEntry << std::endl;
}


mySignalSyst::~mySignalSyst(){
  delete hnEventsProcessedBCK; hnEventsProcessedBCK=NULL;
  delete hnPrimaryVertex;      hnPrimaryVertex=NULL;
  delete hProdType;            hProdType=NULL;
  delete hSumOfNumber;     hSumOfNumber=NULL;
  delete hSumOfWeights;    hSumOfWeights=NULL;
  delete hSumOfWeightsBCK; hSumOfWeightsBCK=NULL;
  delete hSumOfWeightsSquaredBCK; hSumOfWeightsSquaredBCK=NULL;
  
  delete m_tree;             m_tree=NULL;
  delete m_file; m_file=NULL;
}


class mySystVal{
 public:
  mySystVal(){};
  ~mySystVal(){};
  int CalcFinalVal();

  double Val_Nominal=0.0;
  double Val_Syst_JES[2][nSyst_JES]={{0},{0}};
  double Val_Syst_JER[2][nSyst_JER]={{0},{0}};
  double Val_Syst_JVT[2][nSyst_JVT]={{0},{0}};
  double Val_Syst_PRW[2][nSyst_PRW]={{0},{0}};
  double Val_Syst_TST[nSyst_TST]={0};

  double FinalVal_Syst_JES[2]={0};
  double FinalVal_Syst_JER[2]={0};
  double FinalVal_Syst_JVT[2]={0};
  double FinalVal_Syst_PRW[2]={0};
  double FinalVal_Syst_TST[2]={0};
};

int mySystVal::CalcFinalVal(void){
  double vUp=0.0;
  double vDown=0.0;
  
  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_JES[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for JES
  FinalVal_Syst_JES[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_JES[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_JER[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for JER
  FinalVal_Syst_JER[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_JER[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_JVT[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for JVT
  FinalVal_Syst_JVT[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_JVT[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_PRW[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for PRW
  FinalVal_Syst_PRW[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_PRW[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
    double Val = 100.0*(Val_Syst_TST[iSyst]/Val_Nominal - 1.0);
    if(Val>0){
      vUp += Val*Val;
    }else{
      vDown += Val*Val;
    }
  }// for TST
  FinalVal_Syst_TST[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_TST[1] = -TMath::Sqrt(vDown);

  return 0;
}

#endif

