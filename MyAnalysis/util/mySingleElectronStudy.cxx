#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TFile *tf;
NtupleReader *myReader=NULL;
TEventList *eventList=NULL;

TCanvas *c0;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH2D *hFrame;
std::string PDFName;

const int nCategory = 2;
TH2D *h2D;
const int nRegion=7;
double Region[nRegion+1]={10.0, 15.0, 20.0, 25.0, 35.0, 45.0, 60.0, 100.0};
TH1D *h1D[nRegion];
TF1 *fFit[nRegion];

double crystallBall(double* x, double *par);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("mySingleElectronStudy"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  std::vector<std::string> ReadableInputFileList;
  if(vm["grid"].as<int>()==0){
    ReadableInputFileList = InputFileList;
  }else{
    std::string CommaList = InputFileList.at(0);
    while(true){
      std::string::size_type pos = CommaList.find(',');
      if(pos != std::string::npos){
	ReadableInputFileList.push_back(CommaList.substr(0, pos));
	CommaList.erase(0, pos+1);
      }else{
	ReadableInputFileList.push_back(CommaList);
	break;
      }
    }
  }
  PDFName = vm["output-file"].as<std::string>();
  std::string OutputFileName = vm["output-file"].as<std::string>();

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");

  h2D = new TH2D("h2D", ";p_{T} [GeV];#Delta q/p_{T} [TeV^{-1}]", 100, 0, 100, 200, -200, 200);

  for(int i=0;i<nRegion;i++){
    h1D[i] = new TH1D(Form("h1D_%d", i), ";#Delta q/p_{T} [TeV^{-1};", 200, -200, 200);
    fFit[i] = new TF1(Form("fFit_%d", i), [&](double *x, double *p){return crystallBall(x, p);}, -200, 200, 4);
    fFit[i]->SetParameters(1000.0, 0.0, 15.0, 1.6);
  }
  
  int nFile = ReadableInputFileList.size();
  std::cout << "nFile = " << nFile << std::endl;
  for(int iFile=0;iFile<nFile;iFile++){
    std::cout << " iFile = " << iFile << std::endl;
    myReader = new NtupleReader(ReadableInputFileList.at(iFile));

    int nEntry = myReader->nEntry;
    std::cout << " nEntry = " << nEntry << std::endl;
    for(int iEntry=0;iEntry<nEntry;iEntry++){
      if((iEntry+1)%10000==0 ){
	std::cout << "  iEntry = " << (iEntry+1) << std::endl;
      }
      myReader->GetEntry(iEntry);
      
      int StdTrackID = -1;
      int TrackletID = -1;

      // loop over standard track
      for(unsigned int iTrack=0;iTrack<(myReader->conventionalTracks->size());iTrack++){
	if(myReader->conventionalTracks->at(iTrack)->PixelTracklet==1)
	  continue;
	int TrackQuality = MyUtil::CalcOld4LTrackQuality(myReader->conventionalTracks->at(iTrack)); // 011 0000 0100 1111 1100
	double QoverPt  = 1e6*myReader->conventionalTracks->at(iTrack)->qoverpt;
	double TrackPt  = myReader->conventionalTracks->at(iTrack)->p4.Pt()/1000.0;
	double TrackEta = myReader->conventionalTracks->at(iTrack)->p4.Eta();
	double calo20 = myReader->conventionalTracks->at(iTrack)->etclus20_topo/1000.0;
	double calo40 = myReader->conventionalTracks->at(iTrack)->etclus40_topo/1000.0;

	if(MyUtil::IsPassed(TrackQuality, 0x304FC) && (myReader->conventionalTracks->at(iTrack)->nSCTHits>=7) && (myReader->conventionalTracks->at(iTrack)->etclus20overPt_topo>0.5)){
	  StdTrackID = iTrack;
	}
      }// for iTrack

      // find tracklet
      for(unsigned int jTrack=0;jTrack<(myReader->conventionalTracks->size());jTrack++){
	if(myReader->conventionalTracks->at(jTrack)->PixelTracklet==0)
	  continue;
	int TrackQuality = MyUtil::CalcOld4LTrackQuality(myReader->conventionalTracks->at(jTrack)); // 011 0000 0100 1111 1110
	double QoverPt  = 1e6*myReader->conventionalTracks->at(jTrack)->qoverpt;
	if(MyUtil::IsPassed(TrackQuality, 0x304FE)){
	  TrackletID = jTrack;
	}
      }// for jTrack
      
      if(StdTrackID==-1 || TrackletID==-1)
	continue;
      if(myReader->conventionalTracks->at(StdTrackID)->p4.DeltaR(myReader->conventionalTracks->at(TrackletID)->p4) > 0.2)
	continue;

      double StdTrackPt = myReader->conventionalTracks->at(StdTrackID)->p4.Pt()/1000.0;
      double DeltaQoverPt = 1e6*(myReader->conventionalTracks->at(StdTrackID)->qoverpt - myReader->conventionalTracks->at(TrackletID)->qoverpt);
      h2D->Fill(StdTrackPt, DeltaQoverPt);
      
      for(int i=0;i<nRegion;i++){
	if((StdTrackPt > Region[i]) && (StdTrackPt < Region[i+1])){
	  h1D[i]->Fill(DeltaQoverPt);
	}
      }

    }// for iEntry
    delete myReader; myReader=NULL;
  }// for iFile
    
  tf = new TFile(Form("%s.root", OutputFileName.c_str()), "RECREATE");
  h2D->Write();
  for(int i=0;i<nRegion;i++){
    h1D[i]->Write();
  }
  tf->Close();

  return 0;
}

double crystallBall(double* x, double *par){
  double constant = par[0];
  double mean = par[1];
  double sigma = par[2];
  double alpha = par[3];

  if (sigma < 0.)     return 0.;
  if (alpha < 0.)     return 0.;
  double z = (x[0] - mean)/sigma;
  alpha = std::abs(alpha);
  double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
  double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
  double norm3 = norm2;
  constant /= (norm1 + norm2 + norm3);
  if (z  < - alpha){
    return constant * std::exp( alpha * (z + 0.5 * alpha));
  }else if (z  > + alpha){
    return constant * std::exp(-alpha * (z - 0.5 * alpha));
  }else{
    return constant * std::exp(- 0.5 * z * z);
  }
}
