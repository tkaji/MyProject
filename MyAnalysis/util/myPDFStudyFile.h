#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TEventList *eventList=NULL;
const int nMETRegion=3;
const std::string RegName[nMETRegion] = {"High"  , "Middle"  , "Low"};
const int nCat = 3; // hadron, electron, muon
const std::string CatName[nCat] = {"had", "singleEle", "singleMu"};

bool fDoSmearing=true;
bool fSimpleSmear=false;

//const std::string CatName[nCat] = {"HadronHigh", "HadronLow", "ElectronHigh", "ElectronLow", "MuonHigh", "MuonLow"};

//const std::string CatName[nCat] = {"HadronHigh", "HadronLow", "ElectronHigh", "ElectronLow", "MuonHigh", "MuonLow"};
TFile *tfOut;
TTree *treeCat[nCat*nMETRegion];
double TFVal[2];
double b_Calo;
double b_pT;
double b_Eta;
double b_Phi;
double b_MET;
double b_vTF;
double b_eTF;
double b_Bin0;
double b_Bin1;
double b_Bin2;
double b_Bin3;
double b_Bin4;
double b_Bin5;
double b_Bin6;
double b_Bin7;
double b_Bin8;
double b_Bin9;
double b_Bin10;
double b_Bin11;

int iChain=3;
TFile *tfOutput;
const std::string DataFile="/data3/tkaji/myAna/Data/data15-18_13TeV.root";
const std::string PlotStatus="Internal";
const double SumLumi = 2798.72 + 31260.7 + 44083.8 + 58125.9;

//=== Smearing function parameters ===//
const double g_par_Mean  = -4.50114e-01;
const double g_par_Sigma =  1.35062e+01;
const double g_par_Alpha =  1.68961e+00;
const int nSmearRegion = 8;
const double g_par_Mean_ele[nSmearRegion]  = {0, 0, 0, 0, 0, 0, 0, 0};
const double g_par_Sigma_ele[nSmearRegion] = {20.944675, 19.536572, 18.330746, 17.005663, 15.424842, 14.490877, 13.900709, 14.030027};
const double g_par_Alpha_ele[nSmearRegion] = { 1.858616,  1.858616,  1.858616,  1.858616,  1.822388,  1.655727,  1.544172,  1.637405};

const double g_par_Mean_mu[nSmearRegion]  = {0, 0, 0, 0, 0, 0, 0, 0};
const double g_par_Sigma_mu[nSmearRegion] = {16.957926, 15.542328, 14.912544, 14.836049, 14.212644, 13.638716, 13.440928, 13.208069};
const double g_par_Alpha_mu[nSmearRegion] = { 1.716832,  1.716832,  1.716832,  1.716832,  1.663774,  1.618689,  1.682396,  1.644168};

TF1 *fSmearInt;
TF1 *fSmearInt_ele[nSmearRegion];
TF1 *fSmearInt_mu[nSmearRegion];
bool fSmearMuon=false;

// Load Common Input File
TFile *tfInput=NULL;
TH1F *hLumiHistData15;
TH1F *hLumiHistData16;
TH1F *hLumiHistData17;
TH1F *hLumiHistData18;
TH2D *hTrigMETJetEff[nMETTrig];
TH2D *hEleBG_TF_PtEta=NULL;
TH2D *hEleBG_TF_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_InvPtEta=NULL;
TH1D *hMuBG_TF_1Bin=NULL;
TH1D *hMuBG_TF_13Bin=NULL;
TH2D *hMuBG_TF_PtEta=NULL;
TH2D *hMuBG_TF_InvPtEta=NULL;
TH2D *hMSBG_TF_PhiEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta=NULL;

TH1D *P_caloveto05_hadron_StdTrk=NULL;
TH1D *P_caloveto10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_hadron_Trklet=NULL;
TH1D *P_caloveto10_hadron_Trklet=NULL;
TH1D *P_caloveto05_10_hadron_Trklet=NULL;

TH1D *P_caloveto05_electron_StdTrk=NULL;
TH1D *P_caloveto10_electron_StdTrk=NULL;
TH1D *P_caloveto05_10_electron_StdTrk=NULL;
TH1D *P_caloveto05_electron_Trklet=NULL;
TH1D *P_caloveto10_electron_Trklet=NULL;
TH1D *P_caloveto05_10_electron_Trklet=NULL;

TH1D *hCorrHad=NULL;
TH1D *hCorrHad_Side=NULL;
TH1D *hCorrEle=NULL;
TH1D *hCorrEle_Side=NULL;

TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];

MyManager *myData;
HistManager *myHistData;

TCanvas *c0;
TH1D *h1000;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTemplate_Phi=NULL;
TH2D *hTemplate_PhiEta=NULL;
TH1D *hTemplate_MET=NULL;
TH1D *hTemplate_Calo=NULL;
TH1D *hTemplate_AIPC=NULL;
TH1D *hData1D=NULL;
TH2D *hData2D=NULL;
TH1D *hSimpleSmear=NULL;

TH2D *hFrame;
TH2D *hRatioFrame;
std::string PDFName;
double CalcFracError(double vNume, double vDenom, double eNume, double eDenom);
double CalcABCDError(double CRL, double CRH, double VRL, double VRH);
int GetTF(double *ret, double pT, double Eta, double Phi, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1);
int DrawTrackPt(std::string TreeName, TCut myCut, int iOpt=0, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1, bool fSmear=false);
int DrawText(double xPos, double yPos, double iLumi, std::string myText1="", std::string myText2="");
double crystallBallIntegral(double* x, double *par);
void SmearPt(TH1D *hHoge, double pt, double weight=1.0, bool f2Flag=false);
int FillOverflowBin(TH1D *h1);
int FillOverflowBinForData(TH1D *h1);
TCut GetKinematicsCut(int iMETRegion=0,  // 0:SR,  1:VR(150 < MET < 200),  2:CR(100 < MET < 150)
		      int iMETType=0);   // 0:MET, 1:MET_ForEleCR, 2:MET_ForMuCR

TCut D0Cut        = "(TMath::Abs(DisappearingTracks.d0sigTool) < 1.5)";
TCut CaloVETO_0   = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
TCut CaloVETO_1   = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 10.0)";
TCut CaloVETO_123 = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 20.0)";

TCut CaloVETO_432 = "(DisappearingTracks.etclus20_topo/1000.0 > 10.0)";
TCut CaloVETO_4   = "(DisappearingTracks.etclus20_topo/1000.0 > 20.0)";
TCut FakeD0_CR    = "(TMath::Abs(DisappearingTracks.d0sigTool)>10.0)";
TCut FakeD0_VR    = "(TMath::Abs(DisappearingTracks.d0sigTool)>3.0 && TMath::Abs(DisappearingTracks.d0sigTool)<10.0)";
TCut FakeD0_VR1   = "(TMath::Abs(DisappearingTracks.d0sigTool)>5.0 && TMath::Abs(DisappearingTracks.d0sigTool)<10.0)";
TCut FakeD0_VR2   = "(TMath::Abs(DisappearingTracks.d0sigTool)>2.0 && TMath::Abs(DisappearingTracks.d0sigTool)<5.0)";

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

void SmearPt(TH1D *hHoge, double pt, double weight, bool f2Flag){
  double Hoge=0.0;

  int iFunc=-1;
  if(f2Flag){
    if(pt < 15.0){
      iFunc = 0;
    }else if(pt < 20.0){
      iFunc = 1;
    }else if(pt < 25.0){
      iFunc = 2;
    }else if(pt < 35.0){
      iFunc = 3;
    }else if(pt < 45.0){
      iFunc = 4;
    }else if(pt < 60.0){
      iFunc = 5;
    }else if(pt < 100.0){
      iFunc = 6;
    }else{
      iFunc = 7;
    }
  }

  for(int i=0;i<nLogPt;i++){
    double ptLow = XBinsLogPt[i];
    double ptUp  = XBinsLogPt[i+1];
    if(i==(nLogPt-1)){
      ptUp = 12500.0;
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(f2Flag==false){
      w1Low = fSmearInt->Eval(qoverptLow);
      w1Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(f2Flag==false){
      w2Low = fSmearInt->Eval(qoverptLow);
      w2Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
    Hoge+= Y;
  }
  //std::cout << "SmearPt: Result: " << Hoge << std::endl;
  
}// 


int FillOverflowBin(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  double LowVal      = h1->GetBinContent(0);
  double LowError    = h1->GetBinError(0);
  double LowOrgVal   = h1->GetBinContent(1);
  double LowOrgError = h1->GetBinError(1);
  double HighVal      = h1->GetBinContent(nBinsX+1);
  double HighError    = h1->GetBinError(nBinsX+1);
  double HighOrgVal   = h1->GetBinContent(nBinsX);
  double HighOrgError = h1->GetBinError(nBinsX);

  h1->SetBinError(1,   TMath::Sqrt(LowError*LowError + LowOrgError*LowOrgError));
  h1->SetBinContent(1, LowVal + LowOrgVal);

  h1->SetBinError(nBinsX, TMath::Sqrt(HighError*HighError + HighOrgError*HighOrgError));
  h1->SetBinContent(nBinsX, HighVal + HighOrgVal);

  return 0;
}

int FillOverflowBinForData(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  h1->AddBinContent(1, h1->GetBinContent(0));
  h1->SetBinContent(0, 0.0);
  h1->AddBinContent(nBinsX  , h1->GetBinContent(nBinsX+1));
  h1->SetBinContent(nBinsX+1, 0.0);

  return 0;
}

int DrawText(double xPos, double yPos, double iLumi, std::string myText1, std::string myText2){
  ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
  myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", iLumi));
  myText(xPos, yPos-0.12, kBlack, Form("%s", myText1.c_str()));
  myText(xPos, yPos-0.18, kBlack, Form("%s", myText2.c_str()));

  return 0;
}

TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==2){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 15.0)";
  ret = ret && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", 10.0);

  return ret;
}

int GetTF(double *ret, double pT, double Eta, double Phi, int iTFDisap, int iTFMSCalo, int iCorrect){
  ret[0] = 1.0; // val
  ret[1] = 1.0; // error
  if(iTFDisap==0){
    int iBinPt  = hEleBG_TF_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_PtEta->GetYaxis()->FindBin(Eta);
    if(iBinPt ==0) iBinPt =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPt ==(hEleBG_TF_PtEta->GetNbinsX()+1)) iBinPt =hEleBG_TF_PtEta->GetNbinsX();
    if(iBinEta==(hEleBG_TF_PtEta->GetNbinsY()+1)) iBinEta =hEleBG_TF_PtEta->GetNbinsY();

    ret[0] *= hEleBG_TF_PtEta->GetBinContent(iBinPt, iBinEta);
    ret[1] += pow(hEleBG_TF_PtEta->GetBinError(iBinPt, iBinEta)/hEleBG_TF_PtEta->GetBinContent(iBinPt, iBinEta), 2);
  }else if(iTFDisap==2){
    int iBinPt = hMuBG_TF_1Bin->FindBin(pT);
    if(iBinPt==0) iBinPt = 1;
    if(iBinPt ==(hMuBG_TF_1Bin->GetNbinsX()+1)) iBinPt =hMuBG_TF_1Bin->GetNbinsX();

    ret[0] *= hMuBG_TF_1Bin->GetBinContent(iBinPt);
    ret[1] += pow(hMuBG_TF_1Bin->GetBinError(iBinPt)/hMuBG_TF_1Bin->GetBinContent(iBinPt), 2);
  }

  if(iTFMSCalo==3){
    int iBinPt  = hEleBG_TF_CaloIso_4to0_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_4to0_PtEta->GetYaxis()->FindBin(Eta);
    if(iBinPt ==0) iBinPt =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPt ==(hEleBG_TF_CaloIso_4to0_PtEta->GetNbinsX()+1)) iBinPt =hEleBG_TF_CaloIso_4to0_PtEta->GetNbinsX();
    if(iBinEta==(hEleBG_TF_CaloIso_4to0_PtEta->GetNbinsY()+1)) iBinEta =hEleBG_TF_CaloIso_4to0_PtEta->GetNbinsY();

    ret[0] *= hEleBG_TF_CaloIso_4to0_PtEta->GetBinContent(iBinPt, iBinEta);
    ret[1] += pow(hEleBG_TF_CaloIso_4to0_PtEta->GetBinError(iBinPt, iBinEta)/hEleBG_TF_CaloIso_4to0_PtEta->GetBinContent(iBinPt, iBinEta), 2);
  }else if(iTFMSCalo==4){
    int iBinPt  = hEleBG_TF_CaloIso_4to123_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_4to123_PtEta->GetYaxis()->FindBin(Eta);
    if(iBinPt ==0) iBinPt =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPt ==(hEleBG_TF_CaloIso_4to123_PtEta->GetNbinsX()+1)) iBinPt =hEleBG_TF_CaloIso_4to123_PtEta->GetNbinsX();
    if(iBinEta==(hEleBG_TF_CaloIso_4to123_PtEta->GetNbinsY()+1)) iBinEta =hEleBG_TF_CaloIso_4to123_PtEta->GetNbinsY();

    ret[0] *= hEleBG_TF_CaloIso_4to123_PtEta->GetBinContent(iBinPt, iBinEta);
    ret[1] += pow(hEleBG_TF_CaloIso_4to123_PtEta->GetBinError(iBinPt, iBinEta)/hEleBG_TF_CaloIso_4to123_PtEta->GetBinContent(iBinPt, iBinEta), 2);
  }else if(iTFMSCalo==0){ // CR
    int iBinPt  = hEleBG_TF_CaloIso_432to0_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to0_PtEta->GetYaxis()->FindBin(Eta);
    if(iBinPt ==0) iBinPt =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPt ==(hEleBG_TF_CaloIso_432to0_PtEta->GetNbinsX()+1)) iBinPt =hEleBG_TF_CaloIso_432to0_PtEta->GetNbinsX();
    if(iBinEta==(hEleBG_TF_CaloIso_432to0_PtEta->GetNbinsY()+1)) iBinEta =hEleBG_TF_CaloIso_432to0_PtEta->GetNbinsY();

    ret[0] *= hEleBG_TF_CaloIso_432to0_PtEta->GetBinContent(iBinPt, iBinEta);
    ret[1] += pow(hEleBG_TF_CaloIso_432to0_PtEta->GetBinError(iBinPt, iBinEta)/hEleBG_TF_CaloIso_432to0_PtEta->GetBinContent(iBinPt, iBinEta), 2);
  }else if(iTFMSCalo==1){ // VR
    int iBinPt  = hEleBG_TF_CaloIso_432to1_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to1_PtEta->GetYaxis()->FindBin(Eta);
    if(iBinPt ==0) iBinPt =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPt ==(hEleBG_TF_CaloIso_432to1_PtEta->GetNbinsX()+1)) iBinPt =hEleBG_TF_CaloIso_432to1_PtEta->GetNbinsX();
    if(iBinEta==(hEleBG_TF_CaloIso_432to1_PtEta->GetNbinsY()+1)) iBinEta =hEleBG_TF_CaloIso_432to1_PtEta->GetNbinsY();

    ret[0] *= hEleBG_TF_CaloIso_432to1_PtEta->GetBinContent(iBinPt, iBinEta);
    ret[1] += pow(hEleBG_TF_CaloIso_432to1_PtEta->GetBinError(iBinPt, iBinEta)/hEleBG_TF_CaloIso_432to1_PtEta->GetBinContent(iBinPt, iBinEta), 2);
  }else if(iTFMSCalo==2){ // Old
    int iBinPt  = hEleBG_TF_CaloIso_432to01_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to01_PtEta->GetYaxis()->FindBin(Eta);
    if(iBinPt ==0) iBinPt =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPt ==(hEleBG_TF_CaloIso_432to01_PtEta->GetNbinsX()+1)) iBinPt =hEleBG_TF_CaloIso_432to01_PtEta->GetNbinsX();
    if(iBinEta==(hEleBG_TF_CaloIso_432to01_PtEta->GetNbinsY()+1)) iBinEta =hEleBG_TF_CaloIso_432to01_PtEta->GetNbinsY();

    ret[0] *= hEleBG_TF_CaloIso_432to01_PtEta->GetBinContent(iBinPt, iBinEta);
    ret[1] += pow(hEleBG_TF_CaloIso_432to01_PtEta->GetBinError(iBinPt, iBinEta)/hEleBG_TF_CaloIso_432to01_PtEta->GetBinContent(iBinPt, iBinEta), 2);
  }else if(iTFMSCalo==10){
    int iBinPhi = hMSBG_TF_PhiEta->GetXaxis()->FindBin(Phi);
    int iBinEta = hMSBG_TF_PhiEta->GetYaxis()->FindBin(Eta);
    if(iBinPhi ==0) iBinPhi =1;
    if(iBinEta==0) iBinEta=1;
    if(iBinPhi ==(hMSBG_TF_PhiEta->GetNbinsX()+1)) iBinPhi =hMSBG_TF_PhiEta->GetNbinsX();
    if(iBinEta==(hMSBG_TF_PhiEta->GetNbinsY()+1))  iBinEta =hMSBG_TF_PhiEta->GetNbinsY();

    ret[0] *= hMSBG_TF_PhiEta->GetBinContent(iBinPhi, iBinEta);
    ret[1] += pow(hMSBG_TF_PhiEta->GetBinError(iBinPhi, iBinEta)/hMSBG_TF_PhiEta->GetBinContent(iBinPhi, iBinEta), 2);
  }

  //int iBin =  (int)(15.0*(TMath::Log(pT/5.0)/TMath::Log(20.0)) + 1);
  if(iCorrect==0){ // CR
    int iBin = hCorrHad->FindBin(pT);
    ret[0] *= hCorrHad->GetBinContent(iBin);
  }else if(iCorrect==1){ // VR
    int iBin = hCorrHad_Side->FindBin(pT);
    ret[0] *= hCorrHad_Side->GetBinContent(iBin);
  }else if(iCorrect==10){ // CR
    int iBin = hCorrEle->FindBin(pT);
    ret[0] *= hCorrEle->GetBinContent(iBin);
  }else if(iCorrect==11){ // VR
    int iBin = hCorrEle_Side->FindBin(pT);
    ret[0] *= hCorrEle_Side->GetBinContent(iBin);
  }

  ret[1] = TMath::Sqrt(ret[1]);
  return 0;
}

double CalcFracError(double vNume, double vDenom, double eNume, double eDenom){
  return TMath::Sqrt((eNume/vDenom)*(eNume/vDenom) + (vNume*eDenom/(vDenom*vDenom))*(vNume*eDenom/(vDenom*vDenom)));
}

double CalcABCDError(double CRL, double CRH, double VRL, double VRH){
  double eCRL = TMath::Sqrt(CRL);
  double eCRH = TMath::Sqrt(CRH);
  double eVRL = TMath::Sqrt(VRL);
  double eVRH = TMath::Sqrt(VRH);

  double eNume  = TMath::Sqrt((eVRH*CRL)*(eVRH*CRL) + (VRH*eCRL)*(VRH*eCRL));
  double eDenom = TMath::Sqrt((eVRL*CRH)*(eVRL*CRH) + (VRL*eCRH)*(VRL*eCRH));

  return CalcFracError(VRH*CRL, VRL*CRH, eNume, eDenom);
}

int DrawTrackPt(std::string TreeName, TCut myCut, int iOpt, int iTFDisap, int iTFMSCalo, int iCorrect, bool fSmear){
  delete hData1D;
  hData1D = (TH1D *)hTemplate_TrackPt->Clone("hData1D");
  hData1D->Reset();
  hSimpleSmear->Reset();

  eventList->Reset();
  myData->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", myCut);
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader(TreeName)->GetEntry(eventList->GetEntry(iEntry));

    double pT  = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Pt()/1000.0;
    double Eta = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Eta();
    double Phi = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Phi();
    //double TF  = GetTF(pT, Eta, Phi, iTFDisap, iTFMSCalo, iCorrect);
    double TF  = 0;
    if(fSmear==false){
      hData1D->Fill(pT, TF);
    }else if(fSmear==true && fSimpleSmear==false){
      SmearPt(hData1D, pT, TF, true);
    }else if(fSmear==true && fSimpleSmear==true){
      hSimpleSmear->Fill(pT, TF);
    }
 }// for iEntry
  if(fSmear==true && fSimpleSmear==true){
    for(int iBin=0;iBin<(hSimpleSmear->GetNbinsX());iBin++){
      SmearPt(hData1D, hSimpleSmear->GetBinCenter(iBin+1), hSimpleSmear->GetBinContent(iBin+1), true);
    }// for iBin
  }

  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  //hData1D->SetBinErrorOption(TH1::kPoisson);
  //FillOverflowBinForData(hData1D);
  hData1D->Draw("PE");

  return 0;
}

