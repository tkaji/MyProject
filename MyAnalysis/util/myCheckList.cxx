#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

int SignalMode=0;
TEventList *eventList=NULL;
const int nMETRegion=3;
const std::string RegName[nMETRegion] = {"high-MET", "middle-MET", "low-MET"};
int iChain;
bool fUpdateNote=true;
bool fMaskPt=true;
double MaskPt=20.0;
double BoundaryOfMiddleMET;
double PtThresholdForCR;
TFile *tfOutput;
bool fSMBG=true;
TH2D *hHogeHoge;
//=== Smearing function parameters ===//
const double g_par_Mean  = -4.50114e-01;
const double g_par_Sigma =  1.35062e+01;
const double g_par_Alpha =  1.68961e+00;
const int nSmearRegion = 8;
const double g_par_Mean_ele[nSmearRegion]  = {-0.203310, -0.203310, -0.203310, -0.203310, -0.146075, -0.126751, -0.205570, -0.213854};
const double g_par_Sigma_ele[nSmearRegion] = {20.832385, 19.416139, 18.202337, 16.724907, 15.383425, 14.355560, 13.900709, 14.030027};
const double g_par_Alpha_ele[nSmearRegion] = { 1.858616,  1.858616,  1.858616,  1.858616,  1.822388,  1.655727,  1.544172,  1.637405};
const double g_par_Mean_mu[nSmearRegion]  = {-0.248458, -0.248458, -0.248458, -0.248458, -0.188993, -0.133858, -0.280374, 0.001933};
const double g_par_Sigma_mu[nSmearRegion] = {14.836049, 14.836049, 14.836049, 14.836049, 14.212644, 13.638716, 13.440928, 13.208069};
const double g_par_Alpha_mu[nSmearRegion] = { 1.716832,  1.716832,  1.716832,  1.716832,  1.663774,  1.618689,  1.682396,  1.644168};
TF1 *fSmearInt_ele[nSmearRegion];
TF1 *fSmearInt_mu[nSmearRegion];
bool fSmearMuon=false;

// Load Common Input File
TFile *tfInput=NULL;
TH1F *hLumiHistData15;
TH1F *hLumiHistData16;
TH1F *hLumiHistData17;
TH1F *hLumiHistData18;
TH2D *hTrigMETJetEff[nMETTrig];
TH2D *hEleBG_TF_PtEta=NULL;
TH2D *hEleBG_TF_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_InvPtEta=NULL;
TH1D *hMuBG_TF_1Bin=NULL;
TH1D *hMuBG_TF_13Bin=NULL;
TH2D *hMuBG_TF_PtEta=NULL;
TH2D *hMuBG_TF_InvPtEta=NULL;
TH2D *hMSBG_TF_PhiEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta=NULL;

TH1D *P_caloveto05_hadron_StdTrk=NULL;
TH1D *P_caloveto10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_hadron_Trklet=NULL;
TH1D *P_caloveto10_hadron_Trklet=NULL;
TH1D *P_caloveto05_10_hadron_Trklet=NULL;

TH1D *P_caloveto05_electron_StdTrk=NULL;
TH1D *P_caloveto10_electron_StdTrk=NULL;
TH1D *P_caloveto05_10_electron_StdTrk=NULL;
TH1D *P_caloveto05_electron_Trklet=NULL;
TH1D *P_caloveto10_electron_Trklet=NULL;
TH1D *P_caloveto05_10_electron_Trklet=NULL;

// Load XS File
TFile *tfXS=NULL;
TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];
const std::string DirSignal = "/data3/tkaji/myAna/Signal/";

const int nSignal_EWK=1;
std::vector<int> vDSID_EWK[nSignal_EWK]  = { {448304, 448305}};
//std::vector<int> vDSID_EWK[nSignal_EWK]  = { {448496, 448497}};
const int CMass_EWK[nSignal_EWK]={600};
const std::vector<double> vSignalXS_EWK[nSignal_EWK]={{0.0193666, 0.00911829}}; // pb
const std::vector<double> vFilterEff_EWK[nSignal_EWK]={{0.406994, 0.40207}};

const int nSignal_Strong=2;
const int GMass_Strong[nSignal_Strong]={1400, 1800};
const int CMass_Strong[nSignal_Strong]={1100,  900};
//const int GMass_Strong[nSignal_Strong]={1400, 2200};
//const int CMass_Strong[nSignal_Strong]={1100,  700};
const int DSID_Strong[nSignal_Strong]={448380, 448381};
//const int SignalDSID[nSignal_Strong]={448360, 448567};
EColor SigColor[nSignal_Strong]={EColor::kMagenta, EColor::kCyan};
const std::string PlotStatus="Internal";
const int nData=4;
const std::string DataFile[4]={"/data3/tkaji/myAna/Data/data15-18_13TeV.root", "/data3/tkaji/myAna/Data/data15-16_13TeV.root", 
			       "/data3/tkaji/myAna/Data/data17_13TeV.root"   , "/data3/tkaji/myAna/Data/data18_13TeV.root"};
const std::string AllDataTag = "Data 2015-2018";
const std::string DataTag[4]={"Data 2015-2018", "Data 2015-2016", "Data 2017", "Data 2018"};
const int ColData[4] = {EColor::kBlack, EColor::kRed, EColor::kGreen+1, EColor::kBlue};
// 2015 : 0
// 2016 : 1141.73
// 2017 : 3030.55
// 2018 :  881.025

const int nSM = 10;
const int ColZnunu     = EColor::kYellow-7;
const int ColWmunu     = EColor::kBlue;
const int ColWenu      = EColor::kRed-4;
const int ColWtaunu    = EColor::kGreen+1 ;
const int ColZee       = EColor::kRed-10;
const int ColZmumu     = EColor::kAzure+6 ;
const int ColDiBoson   = EColor::kViolet;
const int ColZtautau   = EColor::kSpring-4 ;
//const int ColTop       = EColor::kRed+2;
const int ColTop       = EColor::kOrange+2;
const int Colmultijets = EColor::kOrange;
const int ColSM[nSM]   = {ColZnunu, ColWmunu, ColWenu, ColWtaunu, ColZee, ColZmumu, ColZtautau, ColDiBoson, ColTop, Colmultijets};
//const std::string SMName[nSM] = {"Znunu",  "Wmunu",  "Wenu",  "Wtaunu",  "Zee",  "Zmumu", "Ztautau",  "DiBoson",  "Top",  "multijets"};
const std::string SMName[nSM] = {"Z#nu#nu",  "W#mu#nu",  "We#nu",  "W#tau#nu",  "Zee",  "Z#mu#mu", "Z#tau#tau",  "VV",  "t,t#bar{t}",  "dijet"};

const int nMC16 = 3;
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
std::vector<int> vDSID_multijets[nMC16]  = { {364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712},
					     {364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712},
					     {364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712}};
std::vector<int> vDSID_Zee[nMC16]        = { {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127},
					     {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127},
					     {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127}};
std::vector<int> vDSID_Zmumu[nMC16]      = { {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113},
					     {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113},
					     {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113}};
std::vector<int> vDSID_Ztautau[nMC16]    = { {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141},
					     {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141},
					     {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141}};
std::vector<int> vDSID_Znunu[nMC16]      = { {364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155},
					     {364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155},
					     {364222, 364223, 366010, 366011, 366012, 366013, 366014, 366015, 366016, 366017, 366019, 366020, 366021, 366022, 366023, 366024, 366025, 366026, 366028, 366029, 366030, 366031, 366032, 366033, 366034, 366035}};
double CorrectFactor[26] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
/*
double CorrectFactor[26] = {1.0, 1.0, 20.609148/232.03, 9.7086809/85.26, 0.55247617/4.5536, 0.13345916/1.2026, 5.1418320/42.624, 0.55962648/4.4689, 0.16006654/1.3752, 0.47099694/3.5485,
			    55.787010/232.03, 23.524331/85.26, 1.2138293/4.5536, 0.32278895/1.2026, 11.788607/42.624, 1.2117525/4.4689, 0.37889255/1.3752, 1.0558753/3.5485, 189.56639/232.03,
			    73.381419/85.26, 2.6724912/4.5536, 0.71441256/1.2026, 33.482713/42.624, 2.5776784/4.4689, 0.80224970/1.3752, 2.6069673/3.5485};
*/
//366024, 366025, 366026, 366028, 366029, 366030, 366031, 366032, 366033, 366034, 366035

std::vector<int> vDSID_Wmunu[nMC16]      = { {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169},
					     {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169},
					     {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169}};
std::vector<int> vDSID_Wenu[nMC16]       = { {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183},
					     {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183},
					     {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183}};
std::vector<int> vDSID_Wtaunu[nMC16]     = { {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197},
					     {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197},
					     {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197}};
std::vector<int> vDSID_Top[nMC16]        = { {410470, 410471, 410644, 410645, 410646, 410647},
					     {410470, 410471, 410644, 410645, 410646, 410647},
					     {410470, 410471, 410644, 410645, 410646, 410647}};
std::vector<int> vDSID_DiBoson[nMC16]    = { {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305},
					     {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305},
					     {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305}};

const std::string MCDir[nMC16]={"/data3/tkaji/myAna/SMBG/mc16a_link", "/data3/tkaji/myAna/SMBG/mc16d_link", "/data3/tkaji/myAna/SMBG/mc16e_link"};
const double IntLumi[nMC16]={2798.72 + 31260.7, 44083.8, 58125.9}; // special GRL pb-1
//const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];
const double SignalXS_Strong[nSignal_Strong]={0.0284,
				       0.00293}; // fb = 1e3*pb
const double SignalProdEff[nSignal_Strong]={1.0, 1.0};
double SFSig_Strong[nMC16][nSignal_Strong];
std::vector<double> vSFSig_EWK[nMC16][nSignal_EWK];

//MyManager *mySignal[nSignal];
//HistManager *mySignalHist[nSignal];
MyManager *myData[nData];
HistManager *myHistData[nData];
MyManager *myTest[50][nData];
HistManager *myHistTest[50][nData];

MyManager *mySignal_Strong[nMC16][nSignal_Strong];
HistManager *myHistSignal_Strong[nMC16][nSignal_Strong];

std::vector<MyManager *> *mySignal_EWK[nMC16][nSignal_EWK];
std::vector<HistManager *> *myHistSignal_EWK[nMC16][nSignal_EWK];

std::vector<MyManager *> *myZnunu[nMC16];
std::vector<MyManager *> *myWmunu[nMC16];
std::vector<MyManager *> *myWenu[nMC16];
std::vector<MyManager *> *myWtaunu[nMC16];
std::vector<MyManager *> *myZee[nMC16];
std::vector<MyManager *> *myZmumu[nMC16];
std::vector<MyManager *> *myZtautau[nMC16];
std::vector<MyManager *> *myDiBoson[nMC16];
std::vector<MyManager *> *myTop[nMC16];
std::vector<MyManager *> *mymultijets[nMC16];
std::vector<MyManager *> *mySM[nSM][nMC16];

std::vector<HistManager *> *myHistZnunu[nMC16];
std::vector<HistManager *> *myHistWmunu[nMC16];
std::vector<HistManager *> *myHistWenu[nMC16];
std::vector<HistManager *> *myHistWtaunu[nMC16];
std::vector<HistManager *> *myHistZee[nMC16];
std::vector<HistManager *> *myHistZmumu[nMC16];
std::vector<HistManager *> *myHistZtautau[nMC16];
std::vector<HistManager *> *myHistDiBoson[nMC16];
std::vector<HistManager *> *myHistTop[nMC16];
std::vector<HistManager *> *myHistmultijets[nMC16];
std::vector<HistManager *> *myHistSM[nSM][nMC16];

bool fDoSmearing=true;
bool fSimpleSmear=true;
int RebinVal=1;
TCanvas *c0;
TPad *pad0;
TPad *pad1[2];
TF1 *fSmearInt;
//TH1D *hData=NULL;
TLegend *tl;
TLegend *tlSM;
THStack *hStack_SM=NULL;
TH1D *h1000;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTemplate_Phi=NULL;
TH2D *hTemplate_PhiEta=NULL;
TH1D *hTemplate_MET=NULL;
TH1D *hTemplate_Calo=NULL;
TH1D *hTemplate_AIPC=NULL;
TH1D *hTemplate_AIPC100=NULL;
TH1D *hData1D=NULL;
TH2D *hData2D=NULL;
TH1D *hSignal1D_Strong[nSignal_Strong];
TH2D *hSignal2D_Strong[nSignal_Strong];
TH1D *hSignal1D_EWK[nSignal_EWK];
TH2D *hSignal2D_EWK[nSignal_EWK];

TH1D *hCalo_Signal_Strong[nSignal_Strong];
TH1D *hCalo_Muon_Strong;
TH1D *hCalo_Fake_Strong;
TH1D *hCalo_LowMETHighPt_Strong;

TH2D *hFrame;
TH2D *hRatioFrame;
std::string PDFName;
int SetGraphPoint(double vNume, double vDenom, TGraphAsymmErrors *gGraph, int iPoint, bool fZeroError=false);
double CalcFracError(double vNume, double vDenom, double eNume, double eDenom);
double CalcABCDError(double CRL, double CRH, double VRL, double VRH);
double GetTF(double pT, double Eta, double Phi, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1);
int DrawTrackPt(std::string TreeName, TCut myCut, int iOpt=0, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1, bool fSmear=false);
int DrawRatioPlot(void);
int DrawText(double xPos, double yPos, double iLumi, std::string myText1="", std::string myText2="");
int DrawTH1RatioPlot(std::string TreeName, std::string ValName, TH1D *hHist, double Scale, TCut myCut, int iOpt);
int DrawTH1RatioPlot(std::string HistName, int iHist, int nRebin=1, int iOpt=0, int SameOpt=0);
double crystallBallIntegral(double* x, double *par);
void SmearPt(TH1D *hHoge, double pt, double weight=1.0, bool f2Flag=false);
THStack *GetHStack(std::string HistName, int iHist, int nRegin=1, int iOpt=0);
THStack *GetHStack(std::string TreeName, std::string ValName, double Scale=1.0, TCut myCut="1", int iOpt=0);
int FillOverflowBin(TH1D *h1);
int FillOverflowBinForData(TH1D *h1);
int SetupCanvas(int iOption, std::string xTitle, double MinX, double MaxX, double MinY, double MaxY);
TCut GetKinematicsCut(int iMETRegion=0,  // 0:SR,  1:VR(150 < MET < 200),  2:CR(100 < MET < 150)
		      int iMETType=0);   // 0:MET, 1:MET_ForEleCR, 2:MET_ForMuCR

TCut CaloVETO_0   = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
TCut CaloVETO_1   = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 10.0)";
TCut CaloVETO_123 = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 20.0)";

TCut CaloVETO_432 = "(DisappearingTracks.etclus20_topo/1000.0 > 10.0)";
TCut CaloVETO_4   = "(DisappearingTracks.etclus20_topo/1000.0 > 20.0)";
TCut FakeD0_CR    = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>10.0)";
TCut FakeD0_VR    = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>3.0 && TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)<10.0)";
TCut FakeD0_VR1   = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>5.0 && TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)<10.0)";
TCut FakeD0_VR2   = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>2.0 && TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)<5.0)";

//int CompareData(std::string HistName, int iHist, int nRebin=1);
//int CompareData(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist=NULL, double Scale=1.0);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("SignalMode", po::value<int>()->default_value(0), "0:no signal, 1:EWK, 2:Strong")
    ("iChain", po::value<int>()->default_value(0), "i Chain (0 ~ 19)")
    ("PtLT", po::value<double>()->default_value(10.0), "Lower pT threshold for CR")
    ("MiddleMET", po::value<double>()->default_value(150.0), "Boundary of Middle-MET")
    ("output-file,o", po::value<std::string>()->default_value("myCheckList"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  SignalMode = vm["SignalMode"].as<int>();
  iChain = vm["iChain"].as<int>();
  BoundaryOfMiddleMET = vm["MiddleMET"].as<double>();
  PtThresholdForCR = vm["PtLT"].as<double>();

  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/tmp/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  hLumiHistData15 = (TH1F *)tfInput->Get("lumi_histo_data15");
  hLumiHistData16 = (TH1F *)tfInput->Get("lumi_histo_data16");
  hLumiHistData17 = (TH1F *)tfInput->Get("lumi_histo_data17");
  hLumiHistData18 = (TH1F *)tfInput->Get("lumi_histo_data18");
  hEleBG_TF_PtEta    = (TH2D *)tfInput->Get("hEleBG_TF_PtEta");
  hEleBG_TF_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_InvPtEta");
  hEleBG_TF_CaloIso_4to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_PtEta");
  hEleBG_TF_CaloIso_4to123_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_PtEta");
  hEleBG_TF_CaloIso_432to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_PtEta");
  hEleBG_TF_CaloIso_432to1_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_PtEta");
  hEleBG_TF_CaloIso_432to01_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_PtEta");
  hEleBG_TF_CaloIso_4to0_InvPtEta   = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_InvPtEta");
  hEleBG_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_InvPtEta");
  hEleBG_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_InvPtEta");
  hEleBG_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_InvPtEta");
  hEleBG_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_InvPtEta");
  hMuBG_TF_1Bin     = (TH1D *)tfInput->Get("hMuBG_TF_1Bin");
  hMuBG_TF_13Bin    = (TH1D *)tfInput->Get("hMuBG_TF_13Bin");
  hMuBG_TF_PtEta    = (TH2D *)tfInput->Get("hMuBG_TF_PtEta");
  hMuBG_TF_InvPtEta = (TH2D *)tfInput->Get("hMuBG_TF_InvPtEta");
  hMSBG_TF_PhiEta   = (TH2D *)tfInput->Get("hMSBG_TF_PhiEta");
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta    = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta");

  P_caloveto05_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto05_hadron_StdTrk");
  P_caloveto10_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto10_hadron_StdTrk");
  P_caloveto05_10_hadron_StdTrk   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_StdTrk");  
  P_caloveto05_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto05_hadron_Trklet");     
  P_caloveto10_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto10_hadron_Trklet");     
  P_caloveto05_10_hadron_Trklet   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_Trklet");  
							                                 
  P_caloveto05_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto05_electron_StdTrk");   
  P_caloveto10_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto10_electron_StdTrk");   
  P_caloveto05_10_electron_StdTrk = (TH1D *)tfInput->Get("P_caloveto05_10_electron_StdTrk");
  P_caloveto05_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto05_electron_Trklet");   
  P_caloveto10_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto10_electron_Trklet");   
  P_caloveto05_10_electron_Trklet = (TH1D *)tfInput->Get("P_caloveto05_10_electron_Trklet");

  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");
  gErrorIgnoreLevel = 1001;
  fSmearInt = new TF1("fSmearInt", [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
  fSmearInt->SetParameter(0, g_par_Mean);
  fSmearInt->SetParameter(1, g_par_Sigma);
  fSmearInt->SetParameter(2, g_par_Alpha);

  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i

  for(int iData=0;iData<4;iData++){
    myData[iData] = new MyManager(DataFile[iData], DataTag[iData]);
    myData[iData]->LoadMyFile();
    myHistData[iData] = myData[iData]->myHist_Old4L;
  }
  
  std::cout << "Start Loading Signal MC" << std::endl;
  /* **********   Start Loading Signal MC   ********** */
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    /* **********   Load Signal MC for Strong  ********** */
    if(SignalMode==2){
      for(int iSignal=0;iSignal<nSignal_Strong;iSignal++){
	mySignal_Strong[iMC16][iSignal] = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), DSID_Strong[iSignal], MCName[iMC16].c_str()), Form("Signal (%d, %d)", GMass_Strong[iSignal], CMass_Strong[iSignal]));
	mySignal_Strong[iMC16][iSignal]->LoadMyFile();
	myHistSignal_Strong[iMC16][iSignal] = mySignal_Strong[iMC16][iSignal]->myHist_Old4L;
	SFSig_Strong[iMC16][iSignal] = IntLumi[iMC16]/(myHistSignal_Strong[iMC16][iSignal]->hSumOfWeightsBCK->GetBinContent(1));
      }
    }// Strong

    /* **********   Load Signal MC for Electroweak  ********** */
    if(SignalMode==1){
      for(int iSignal=0;iSignal<nSignal_EWK;iSignal++){
	mySignal_EWK[iMC16][iSignal] = new std::vector<MyManager *>;
	myHistSignal_EWK[iMC16][iSignal] = new std::vector<HistManager *>;
	for(unsigned int iDSID=0;iDSID<(vDSID_EWK[iSignal].size());iDSID++){
	  MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_EWK[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_EWK[iSignal]));
	  myDS->LoadMyFile();
	  mySignal_EWK[iMC16][iSignal]->push_back(myDS);
	  myHistSignal_EWK[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	  vSFSig_EWK[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_EWK[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	}// for iDSID
      }// for iSignal
    }// EWK
  }// Load Signal MC

  std::cout << "Start Loading BG MC" << std::endl;
  /* **********   Load SM MC   ********** */
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    if(fSMBG==false)
      continue;
    
    // Znunu
    myZnunu[iMC16] = new std::vector<MyManager *>;
    myHistZnunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Znunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Znunu[iMC16].at(iDSID)), "Znunu");
      myDS->LoadMyFile(1);
      myZnunu[iMC16]->push_back(myDS);
      myHistZnunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Wmunu
    myWmunu[iMC16] = new std::vector<MyManager *>;
    myHistWmunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Wmunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Wmunu[iMC16].at(iDSID)), "Wmunu");
      myDS->LoadMyFile(1);
      myWmunu[iMC16]->push_back(myDS);
      myHistWmunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Wenu
    myWenu[iMC16] = new std::vector<MyManager *>;
    myHistWenu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Wenu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Wenu[iMC16].at(iDSID)), "Wenu");
      myDS->LoadMyFile(1);
      myWenu[iMC16]->push_back(myDS);
      myHistWenu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Wtaunu
    myWtaunu[iMC16] = new std::vector<MyManager *>;
    myHistWtaunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Wtaunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Wtaunu[iMC16].at(iDSID)), "Wtaunu");
      myDS->LoadMyFile(1);
      myWtaunu[iMC16]->push_back(myDS);
      myHistWtaunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Zee
    myZee[iMC16] = new std::vector<MyManager *>;
    myHistZee[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Zee[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Zee[iMC16].at(iDSID)), "Zee");
      myDS->LoadMyFile(1);
      myZee[iMC16]->push_back(myDS);
      myHistZee[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Zmumu
    myZmumu[iMC16] = new std::vector<MyManager *>;
    myHistZmumu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Zmumu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Zmumu[iMC16].at(iDSID)), "Zmumu");
      myDS->LoadMyFile(1);
      myZmumu[iMC16]->push_back(myDS);
      myHistZmumu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Ztautau
    myZtautau[iMC16] = new std::vector<MyManager *>;
    myHistZtautau[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Ztautau[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Ztautau[iMC16].at(iDSID)), "Ztautau");
      myDS->LoadMyFile(1);
      myZtautau[iMC16]->push_back(myDS);
      myHistZtautau[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // DiBoson
    myDiBoson[iMC16] = new std::vector<MyManager *>;
    myHistDiBoson[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_DiBoson[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_DiBoson[iMC16].at(iDSID)), "DiBoson");
      myDS->LoadMyFile(1);
      myDiBoson[iMC16]->push_back(myDS);
      myHistDiBoson[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Top
    myTop[iMC16] = new std::vector<MyManager *>;
    myHistTop[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Top[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Top[iMC16].at(iDSID)), "Top");
      myDS->LoadMyFile(1);
      myTop[iMC16]->push_back(myDS);
      myHistTop[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // multijets
    mymultijets[iMC16] = new std::vector<MyManager *>;
    myHistmultijets[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_multijets[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_multijets[iMC16].at(iDSID)), "multijets");
      myDS->LoadMyFile(1);
      mymultijets[iMC16]->push_back(myDS);
      myHistmultijets[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    mySM[0][iMC16] = myZnunu[iMC16];
    mySM[1][iMC16] = myWmunu[iMC16];
    mySM[2][iMC16] = myWenu[iMC16];
    mySM[3][iMC16] = myWtaunu[iMC16];
    mySM[4][iMC16] = myZee[iMC16];
    mySM[5][iMC16] = myZmumu[iMC16];
    mySM[6][iMC16] = myZtautau[iMC16];
    mySM[7][iMC16] = myDiBoson[iMC16];
    mySM[8][iMC16] = myTop[iMC16];
    mySM[9][iMC16] = mymultijets[iMC16];

    myHistSM[0][iMC16] = myHistZnunu[iMC16];
    myHistSM[1][iMC16] = myHistWmunu[iMC16];
    myHistSM[2][iMC16] = myHistWenu[iMC16];
    myHistSM[3][iMC16] = myHistWtaunu[iMC16];
    myHistSM[4][iMC16] = myHistZee[iMC16];
    myHistSM[5][iMC16] = myHistZmumu[iMC16];
    myHistSM[6][iMC16] = myHistZtautau[iMC16];
    myHistSM[7][iMC16] = myHistDiBoson[iMC16];
    myHistSM[8][iMC16] = myHistTop[iMC16];
    myHistSM[9][iMC16] = myHistmultijets[iMC16];
  }// for iMC16

  tl = new TLegend(0.5, 0.4, 0.8, 0.7);
  tlSM = new TLegend(0.5, 0.4, 0.8, 0.7);
  c0 = new TCanvas("c0", "c0", 800, 600);

  hFrame = new TH2D("hFrame", "", 10, 0, 10, 10, 0, 10);
  hStack_SM = new THStack("hStack_SM", "");
  h1000 = new TH1D("h1000", "", 1000, 0, 1000);
  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nLogPt, XBinsLogPt);
  hTemplate_MET = new TH1D("hTemplate_MET", ";MET [GeV];Tracks", 1000, 0, 1000);
  hTemplate_Phi = new TH1D("hTemplate_Phi", ";MET #phi;events", 50, -TMath::Pi(), TMath::Pi());
  hTemplate_PhiEta = new TH2D("hTemplate_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
  hTemplate_Calo = new TH1D("hTemplate_Calo", ";etclus20_topo [GeV];Tracks", 50, 0, 50);
  hTemplate_AIPC = new TH1D("hTemplate_AIPC", ";Average Interactions Per Crossing;", 50, 10, 60);
  hTemplate_AIPC100 = new TH1D("hTemplate_AIPC100", ";Average Interactions Per Crossing;", 100, 0, 100);
  hData1D = new TH1D("hData1D", "hoge", 10, 0, 10);
  hData2D = new TH2D("hData2D", "hoge", 10, 0, 10, 10, 0, 10);
  for(int i=0;i<nSignal_Strong;i++){
    hSignal1D_Strong[i] = new TH1D(Form("hSignal1D_Strong_%d", i), "", 10, 0, 10);
    hSignal2D_Strong[i] = new TH2D(Form("hSignal2D_Strong_%d", i), "", 10, 0, 10, 10, 0, 10);
  }
  for(int i=0;i<nSignal_EWK;i++){
    hSignal1D_EWK[i] = new TH1D(Form("hSignal1D_EWK_%d", i), "", 10, 0, 10);
    hSignal2D_EWK[i] = new TH2D(Form("hSignal2D_EWK_%d", i), "", 10, 0, 10, 10, 0, 10);
  }
  c0->Print(Form("%s_Chain%d.pdf[", PDFName.c_str(), iChain), "pdf");


  // plot CheckList : SR Selection Efficiency (CaloRegion 0)
  RebinVal=5;
  TH1D *hSR_AIPC_CaloVETO0[nMETRegion];
  TGraphAsymmErrors *gSR_AIPC_CaloVETO0[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : AIPC for SR (%s), CaloRegion 0", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = (iMET==0);  MaskPt = LowPtThreshold;
    DrawTH1RatioPlot("Old4L_Common_SR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC100, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("SR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hSR_AIPC_CaloVETO0[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hSR_AIPC_CaloVETO0[iMET] = (TH1D *)hData1D->Clone(Form("hSR_AIPC_CaloVETO0_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

    TH1D *hOrgHist = (TH1D *)myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain))->Clone("hOrgHist");
    hOrgHist->Rebin(RebinVal);

    //gSR_AIPC_CaloVETO0[iMET] = new TGraphAsymmErrors(hSR_AIPC_CaloVETO0[iMET], myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain)));
    gSR_AIPC_CaloVETO0[iMET] = new TGraphAsymmErrors(hSR_AIPC_CaloVETO0[iMET], hOrgHist); 
    c0->Clear();
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(true);
    pad1[0]->Draw();
    pad1[0]->cd();
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Selection Efficiency", 50, 10.0, 60.0, 50, 1e-9, 1e-4);
    hFrame->Draw();
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("SR (%s)", RegName[iMET].c_str()));
    gSR_AIPC_CaloVETO0[iMET]->Draw("samePL");    
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_Eff.pdf", iChain, iMET), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete hOrgHist; hOrgHist=NULL;
  }// for iMET

  // plot CheckList : SR Selection Efficiency (CaloRegion 0)
  RebinVal=5;
  TH1D *hSR_ActIPC_CaloVETO0[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : ActIPC for SR (%s), CaloRegion 0", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "Actual Interactions Per Crossing", 10.0, 60.0, 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = (iMET==0);  MaskPt = LowPtThreshold;
    DrawTH1RatioPlot("Old4L_Common_SR", "actualInteractionsPerCrossing", hTemplate_AIPC100, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("SR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hSR_ActIPC_CaloVETO0[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hSR_ActIPC_CaloVETO0[iMET] = (TH1D *)hData1D->Clone(Form("hSR_ActIPC_CaloVETO0_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET
  
  // plot CheckList : SR MET phi (CaloRegion 0)
  RebinVal=1;
  TH1D *hSR_METPhi_CaloVETO0[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : MET phi for SR (%s), CaloRegion 0", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "MET #phi", -TMath::Pi(), TMath::Pi(), 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = (iMET==0);  MaskPt = LowPtThreshold;
    DrawTH1RatioPlot("Old4L_Common_SR", "MET.PhysObjBase.p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("SR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_METPhi.pdf", iChain, iMET), "pdf");
    hSR_METPhi_CaloVETO0[iMET] = (TH1D *)hData1D->Clone(Form("hSR_METPhi_CaloVETO0_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : SR leading jet phi (CaloRegion 0)
  TH1D *hSR_JetPhi_CaloVETO0[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet phi for SR (%s), CaloRegion 0", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "leading jet #phi", -TMath::Pi(), TMath::Pi(), 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = (iMET==0);  MaskPt = LowPtThreshold;
    DrawTH1RatioPlot("Old4L_Common_SR", "GoodJets[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("SR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_JetPhi.pdf", iChain, iMET), "pdf");
    hSR_JetPhi_CaloVETO0[iMET] = (TH1D *)hData1D->Clone(Form("hSR_JetPhi_CaloVETO0_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : SR leading jet eta vs phi 2D (CaloRegion 0)
  TH2D *hSR_JetPhiEta_CaloVETO0[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet PhiEta for SR (%s), CaloRegion 0", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading jet #phi;leading jet #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = (iMET==0);  MaskPt = LowPtThreshold;
    if(fMaskPt){
      TCut PtCut=Form("(DisappearingTracks.p4.Pt()/1000.0 < %lf)", MaskPt);
      myData[0]->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 0) && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]) && PtCut, "colz"); // for blind
    }else{
      myData[0]->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 0) && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), "colz"); 
    }
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("SR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_SR_METReg%d_JetPhiEta.pdf", iChain, iMET), "pdf");
    hSR_JetPhiEta_CaloVETO0[iMET] = (TH2D *)hFrame->Clone(Form("hSR_JetPhieta_CaloVETO0_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET


  // plot CheckList : SR Selection Efficiency (CaloRegion 1)
  RebinVal=5;
  TH1D *hSR_AIPC_CaloVETO1[nMETRegion];
  TGraphAsymmErrors *gSR_AIPC_CaloVETO1[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : AIPC for VR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_SR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC100, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_1 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("VR_{calo} (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hSR_AIPC_CaloVETO1[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hSR_AIPC_CaloVETO1[iMET] = (TH1D *)hData1D->Clone(Form("hSR_AIPC_CaloVETO1_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

    TH1D *hOrgHist = (TH1D *)myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain))->Clone("hOrgHist");
    hOrgHist->Rebin(RebinVal);

    //gSR_AIPC_CaloVETO1[iMET] = new TGraphAsymmErrors(hSR_AIPC_CaloVETO1[iMET], myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain)));
    gSR_AIPC_CaloVETO1[iMET] = new TGraphAsymmErrors(hSR_AIPC_CaloVETO1[iMET], hOrgHist);
    c0->Clear();
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(true);
    pad1[0]->Draw();
    pad1[0]->cd();
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Selection Efficiency", 50, 10.0, 60.0, 50, 1e-9, 1e-4);
    hFrame->Draw();
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("CR_{calo} (%s)", RegName[iMET].c_str()));
    gSR_AIPC_CaloVETO1[iMET]->Draw("samePL");    
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_Eff.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete hOrgHist; hOrgHist=NULL;
  }// for iMET

  // plot CheckList : SR Selection Efficiency (CaloRegion 1)
  RebinVal=5;
  TH1D *hSR_ActIPC_CaloVETO1[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : ActIPC for VR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "Actual Interactions Per Crossing", 10.0, 60.0, 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_SR", "actualInteractionsPerCrossing", hTemplate_AIPC100, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_1 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("VR_{calo} (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hSR_ActIPC_CaloVETO1[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hSR_ActIPC_CaloVETO1[iMET] = (TH1D *)hData1D->Clone(Form("hSR_ActIPC_CaloVETO1_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : SR MET phi (CaloRegion 1)
  RebinVal=1;
  TH1D *hSR_METPhi_CaloVETO1[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : MET phi for VR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "MET #phi", -TMath::Pi(), TMath::Pi(), 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_SR", "MET.PhysObjBase.p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_1 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("VR_{calo} (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_METPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hSR_METPhi_CaloVETO1[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hSR_METPhi_CaloVETO1[iMET] = (TH1D *)hData1D->Clone(Form("hSR_METPhi_CaloVETO1_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : SR leading jet phi (CaloRegion 1)
  TH1D *hSR_JetPhi_CaloVETO1[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet phi for VR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    SetupCanvas(1, "leading jet #phi", -TMath::Pi(), TMath::Pi(), 0.0, 15.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_SR", "GoodJets[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 0) && CaloVETO_1 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("VR_{calo} (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_JetPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hSR_JetPhi_CaloVETO1[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hSR_JetPhi_CaloVETO1[iMET] = (TH1D *)hData1D->Clone(Form("hSR_JetPhi_CaloVETO1_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : SR leading jet eta vs phi 2D (CaloRegion 1)
  TH2D *hSR_JetPhiEta_CaloVETO1[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet PhiEta for VR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading jet #phi;leading jet #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = false;
    myData[0]->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 0) && CaloVETO_1 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), "colz");
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("VR_{calo} (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_VR_METReg%d_JetPhiEta.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hSR_JetPhiEta_CaloVETO1[iMET] = (TH2D *)hFrame->Clone(Form("hSR_JetPhieta_CaloVETO1_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : Fake100 CR Selection Efficiency
  RebinVal=2;
  TH1D *hFake100CR_AIPC;
  TGraphAsymmErrors *gFake100CR_AIPC;
  std::cout << Form("plot : AIPC for Fake100 CR") << std::endl;
  SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, 50.0);
  pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
  fMaskPt = false;
  DrawTH1RatioPlot("Old4L_Common_fakeCR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC100, 1.0, GetKinematicsCut(3, 0) && FakeD0_CR && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
  DrawText(0.20, 0.90, SumLumi/1000.0, Form("Fake CR"));
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  std::cout << Form("hFake100CR_AIPC : ") << hData1D->GetMaximum() << std::endl;
  hFake100CR_AIPC = (TH1D *)hData1D->Clone(Form("hFake100CR_AIPC"));
  delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

  TH1D *hOrgHist = (TH1D *)myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain))->Clone("hOrgHist");
  hOrgHist->Rebin(RebinVal);

  //gFake100CR_AIPC = new TGraphAsymmErrors(hFake100CR_AIPC, myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain)));
  gFake100CR_AIPC = new TGraphAsymmErrors(hFake100CR_AIPC, hOrgHist);
  c0->Clear();
  c0->cd();
  pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
  pad1[0]->SetLogy(true);
  pad1[0]->Draw();
  pad1[0]->cd();
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Selection Efficiency", 50, 10.0, 60.0, 50, 1e-6, 1e-3);
  hFrame->Draw();
  {
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Fake CR"));
  }
  gFake100CR_AIPC->Draw("samePL");    
  c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_Eff.pdf", iChain), "pdf");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete hOrgHist; hOrgHist=NULL;

  // plot CheckList : Fake100 CR Selection Efficiency
  RebinVal=1;
  TH1D *hFake100CR_ActIPC;
  std::cout << Form("plot : ActIPC for Fake100 CR") << std::endl;
  SetupCanvas(1, "Actual Interactions Per Crossing", 10.0, 60.0, 0.0, 50.0);
  pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
  fMaskPt = false;
  DrawTH1RatioPlot("Old4L_Common_fakeCR", "actualInteractionsPerCrossing", hTemplate_AIPC100, 1.0, GetKinematicsCut(3, 0) && FakeD0_CR && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
  DrawText(0.20, 0.90, SumLumi/1000.0, Form("Fake CR"));
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  std::cout << Form("hFake100CR_ActIPC : ") << hData1D->GetMaximum() << std::endl;
  hFake100CR_ActIPC = (TH1D *)hData1D->Clone(Form("hFake100CR_ActIPC"));
  delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

  // plot CheckList : Fake100 CR MET phi
  TH1D *hFake100CR_METPhi;
  std::cout << Form("plot : MET phi for Fake100 CR") << std::endl;
  SetupCanvas(1, "MET #phi", -TMath::Pi(), TMath::Pi(), 0.0, 50.0);
  pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
  fMaskPt = false;
  DrawTH1RatioPlot("Old4L_Common_fakeCR", "MET.PhysObjBase.p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(3, 0) && FakeD0_CR && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
  DrawText(0.20, 0.90, SumLumi/1000.0, Form("Fake CR"));
  c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_METPhi.pdf", iChain), "pdf");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  std::cout << Form("hFake100CR_METPhi : ") << hData1D->GetMaximum() << std::endl;
  hFake100CR_METPhi = (TH1D *)hData1D->Clone(Form("hFake100CR_METPhi"));
  delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

  // plot CheckList : Fake100 CR leading jet phi
  TH1D *hFake100CR_JetPhi;
  std::cout << Form("plot : Jet phi for Fake100 CR") << std::endl;
  SetupCanvas(1, "leading jet #phi", -TMath::Pi(), TMath::Pi(), 0.0, 50.0);
  pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
  fMaskPt = false;
  DrawTH1RatioPlot("Old4L_Common_fakeCR", "GoodJets[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(3, 0) && FakeD0_CR && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), 0);
  DrawText(0.20, 0.90, SumLumi/1000.0, Form("Fake CR"));
  c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_JetPhi.pdf", iChain), "pdf");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  std::cout << Form("hFake100CR_JetPhi : ") << hData1D->GetMaximum() << std::endl;
  hFake100CR_JetPhi = (TH1D *)hData1D->Clone(Form("hFake100CR_JetPhi"));
  delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

  // plot CheckList : Fake100 CR leading jet eta vs phi 2D
  TH2D *hFake100CR_JetPhiEta;
  std::cout << Form("plot : Jet PhiEta for Fake100 CR") << std::endl;
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";leading jet #phi;leading jet #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
  c0->cd();
  pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
  pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
  pad1[0]->SetLeftMargin(0.12);
  pad1[0]->SetRightMargin(0.15);
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  fMaskPt = false;
  myData[0]->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(3, 0) && FakeD0_CR && CaloVETO_0 && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", XBinsLogPt[0]), "colz");
  {
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Fake CR"));
  }
  c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_FakeCR_JetPhiEta.pdf", iChain), "pdf");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  hFake100CR_JetPhiEta = (TH2D *)hFrame->Clone(Form("hFake100CR_JetPhieta"));
  delete pad1[0]; pad1[0]=NULL;
  /*
  // plot CheckList : HadCR Selection Efficiency
  TH1D *hHadCR_AIPC[nMETRegion];
  TGraphAsymmErrors *gHadCR_AIPC[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : AIPC for HadCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {200.0, 400.0, 800.0};
    SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_hadCR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 0), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Hadron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hHadCR_AIPC[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hHadCR_AIPC[iMET] = (TH1D *)hData1D->Clone(Form("hHadCR_AIPC_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

    gHadCR_AIPC[iMET] = new TGraphAsymmErrors(hHadCR_AIPC[iMET], myHistData[0]->GetHist1D(Form("hCorrectedAIPC_Chain%d", iChain)));
    c0->Clear();
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(true);
    pad1[0]->Draw();
    pad1[0]->cd();
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Selection Efficiency", 50, 10.0, 60.0, 50, 1e-6, 1e-2);
    hFrame->Draw();
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Hadron CR (%s)", RegName[iMET].c_str()));
    gHadCR_AIPC[iMET]->Draw("samePL");    
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_Eff.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : HadCR Selection Efficiency
  TH1D *hHadCR_ActIPC[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : ActIPC for HadCR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {200.0, 400.0, 800.0};
    SetupCanvas(1, "Actual Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_hadCR", "actualInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 0), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Hadron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hHadCR_ActIPC[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hHadCR_ActIPC[iMET] = (TH1D *)hData1D->Clone(Form("hHadCR_ActIPC_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Had CR MET phi
  TH1D *hHadCR_METPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : MET phi for HadCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {120.0, 200.0, 400.0};
    SetupCanvas(1, "MET #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_hadCR", "MET.PhysObjBase.p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 0), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Hadron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_METPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hHadCR_METPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hHadCR_METPhi[iMET] = (TH1D *)hData1D->Clone(Form("hHadCR_METPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Had CR leading jet phi
  TH1D *hHadCR_JetPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet phi for HadCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {120.0, 200.0, 400.0};
    SetupCanvas(1, "leading jet #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_hadCR", "GoodJets[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 0), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Hadron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_JetPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hHadCR_JetPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hHadCR_JetPhi[iMET] = (TH1D *)hData1D->Clone(Form("hHadCR_JetPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Had CR leading jet eta vs phi 2D
  TH2D *hHadCR_JetPhiEta[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet PhiEta for HadCR (%s)", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading jet #phi;leading jet #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = false;
    myData[0]->GetNtupleReader("Old4L_Common_hadCR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 0), "colz");
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Hadron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_HadCR_METReg%d_JetPhiEta.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hHadCR_JetPhiEta[iMET] = (TH2D *)hFrame->Clone(Form("hHadCR_JetPhieta_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : EleCR Selection Efficiency
  TH1D *hEleCR_AIPC[nMETRegion];
  TGraphAsymmErrors *gEleCR_AIPC[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : AIPC for EleCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {4500.0, 8000.0, 12000.0};
    SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleEleCR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 1), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hEleCR_AIPC[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hEleCR_AIPC[iMET] = (TH1D *)hData1D->Clone(Form("hEleCR_AIPC_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

    gEleCR_AIPC[iMET] = new TGraphAsymmErrors(hEleCR_AIPC[iMET], myHistData[0]->GetHist1D(Form("hCorrectedAIPC_ForEleCR_Chain%d", iChain)));
    c0->Clear();
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(true);
    pad1[0]->Draw();
    pad1[0]->cd();
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Selection Efficiency", 50, 10.0, 60.0, 50, 1e-5, 1e-1);
    hFrame->Draw();
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Electron CR (%s)", RegName[iMET].c_str()));
    gEleCR_AIPC[iMET]->Draw("samePL");    
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_Eff.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : EleCR Selection Efficiency
  TH1D *hEleCR_ActIPC[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : ActIPC for EleCR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {4500.0, 8000.0, 12000.0};
    SetupCanvas(1, "Actual Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleEleCR", "actualInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 1), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hEleCR_ActIPC[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hEleCR_ActIPC[iMET] = (TH1D *)hData1D->Clone(Form("hEleCR_ActIPC_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Ele CR MET phi
  TH1D *hEleCR_METPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : MET phi for EleCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {3000.0, 5000.0, 10000.0};
    SetupCanvas(1, "MET #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleEleCR", "MET.PhysObjBase.p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 1), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_METPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hEleCR_METPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hEleCR_METPhi[iMET] = (TH1D *)hData1D->Clone(Form("hEleCR_METPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Ele CR leading jet phi
  TH1D *hEleCR_JetPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet phi for EleCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {3000.0, 5000.0, 10000.0};
    SetupCanvas(1, "leading jet #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleEleCR", "GoodJets[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 1), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_JetPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hEleCR_JetPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hEleCR_JetPhi[iMET] = (TH1D *)hData1D->Clone(Form("hEleCR_JetPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Ele CR leading jet eta vs phi 2D
  TH2D *hEleCR_JetPhiEta[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet PhiEta for EleCR (%s)", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading jet #phi;leading jet #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = false;
    myData[0]->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 1), "colz");
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_JetPhiEta.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hEleCR_JetPhiEta[iMET] = (TH2D *)hFrame->Clone(Form("hEleCR_JetPhieta_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : Ele CR leading lepton phi
  TH1D *hEleCR_LeptonPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Lepton phi for EleCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {3000.0, 5000.0, 10000.0};
    SetupCanvas(1, "leading electron #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleEleCR", "GoodElectrons[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 1), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_LeptonPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hEleCR_LeptonPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hEleCR_LeptonPhi[iMET] = (TH1D *)hData1D->Clone(Form("hEleCR_LeptonPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Ele CR leading lepton eta vs phi 2D
  TH2D *hEleCR_LeptonPhiEta[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Lepton PhiEta for EleCR (%s)", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading electron #phi;leading electron #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = false;
    myData[0]->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->Draw("GoodElectrons[0].p4.Eta():GoodElectrons[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 1), "colz");
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Electron CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_EleCR_METReg%d_LeptonPhiEta.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hEleCR_LeptonPhiEta[iMET] = (TH2D *)hFrame->Clone(Form("hEleCR_LeptonPhieta_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET
  */
  // plot CheckList : MuCR Selection Efficiency

  TH1D *hMuCR_AIPC[nMETRegion];
  TH1D *hMuCR_AIPC_SM[nMETRegion];
  TGraphAsymmErrors *gMuCR_AIPC[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : AIPC for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {40000.0, 60000.0, 72000.0};
    SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleMuCR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 2) && CaloVETO_0, 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hMuCR_AIPC[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hMuCR_AIPC[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_AIPC_%d", iMET));
    hMuCR_AIPC_SM[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_AIPC_SM_%d", iMET));
    hMuCR_AIPC_SM[iMET]->Reset();
    for(int i=0;i<hData1D->GetNbinsX();i++){
      double BinVal   = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinContent(i+1);
      double BinError = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinError(i+1);
      hMuCR_AIPC_SM[iMET]->SetBinContent(i+1, BinVal);
      hMuCR_AIPC_SM[iMET]->SetBinError(i+1, BinError);
    }

    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

    gMuCR_AIPC[iMET] = new TGraphAsymmErrors(hMuCR_AIPC[iMET], myHistData[0]->GetHist1D(Form("hCorrectedAIPC_ForMuCR_Chain%d", iChain)));
    c0->Clear();
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(true);
    pad1[0]->Draw();
    pad1[0]->cd();
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Selection Efficiency", 50, 10.0, 60.0, 50, 1e-3, 1e0);
    hFrame->Draw();
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Muon CR (%s)", RegName[iMET].c_str()));
    gMuCR_AIPC[iMET]->Draw("samePL");    
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_Eff.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : MuCR calo-veto efficiency
  TH1D *hMuCR_AIPC_woCaloVETO[nMETRegion];
  TH1D *hMuCR_AIPC_woCaloVETO_SM[nMETRegion];
  TGraphAsymmErrors *gMuCR_CaloVetoEfficiency_AIPC[nMETRegion];
  TGraphAsymmErrors *gMuCR_CaloVetoEfficiency_AIPC_SM[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : AIPC woCaloVeto for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {40000.0, 60000.0, 72000.0};
    SetupCanvas(1, "Average Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleMuCR", "CorrectedAverageInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 2), 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Muon CR woCaloVeto (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hMuCR_AIPC_woCaloVETO[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;

    hMuCR_AIPC_woCaloVETO[iMET]    = (TH1D *)hData1D->Clone(Form("hMuCR_AIPC_woCaloVETO_%d", iMET));
    hMuCR_AIPC_woCaloVETO_SM[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_AIPC_woCaloVETO_SM_%d", iMET));
    hMuCR_AIPC_woCaloVETO_SM[iMET]->Reset();
    for(int i=0;i<hData1D->GetNbinsX();i++){
      double BinVal   = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinContent(i+1);
      double BinError = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinError(i+1);
      hMuCR_AIPC_woCaloVETO_SM[iMET]->SetBinContent(i+1, BinVal);
      hMuCR_AIPC_woCaloVETO_SM[iMET]->SetBinError(i+1, BinError);

      std::cout << iMET << " : i+1 = " << i + 1 << ", " << hMuCR_AIPC_woCaloVETO_SM[iMET]->GetBinContent(i+1) << " / " << hMuCR_AIPC_SM[iMET]->GetBinContent(i+1) << std::endl;
    }
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;

    gMuCR_CaloVetoEfficiency_AIPC[iMET] = new TGraphAsymmErrors(hMuCR_AIPC[iMET], hMuCR_AIPC_woCaloVETO[iMET]);
    gMuCR_CaloVetoEfficiency_AIPC_SM[iMET] = new TGraphAsymmErrors(hMuCR_AIPC_SM[iMET], hMuCR_AIPC_woCaloVETO_SM[iMET]);
    gMuCR_CaloVetoEfficiency_AIPC_SM[iMET]->SetMarkerColor(kRed);
    gMuCR_CaloVetoEfficiency_AIPC_SM[iMET]->SetLineColor(kRed);
    c0->Clear();
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.35, 1.0, 1.0);
    pad1[1] = new TPad("pad1_1", "pad1_1", 0.0, 0.0 , 1.0, 0.35);
    c0->cd();
    pad1[0]->SetLogy(false);
    pad1[0]->Draw();
    pad1[0]->cd();
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Calorimeter VETO Efficiency", 50, 10.0, 60.0, 50, 0.0, 1.0);
    hFrame->Draw();
    double xPos=0.20;
    double yPos=0.40;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Muon CR (%s)", RegName[iMET].c_str()));
    gMuCR_CaloVetoEfficiency_AIPC[iMET]->Draw("samePL");    
    gMuCR_CaloVetoEfficiency_AIPC_SM[iMET]->Draw("samePL");    

    c0->cd();
    pad1[1]->SetLogy(false);
    pad1[1]->Draw();
    pad1[1]->cd();
    delete hRatioFrame; hRatioFrame=NULL;
    hRatioFrame = new TH2D("hRatioFrame", Form(";%s;data/MC ", "Average Interactions Per Crossing"), 50, 10.0, 60.0, 100, 0.2, 1.8);
    hRatioFrame->GetXaxis()->SetLabelSize(0.15);
    hRatioFrame->GetXaxis()->SetTitleSize(0.15);
    hRatioFrame->GetXaxis()->SetTitleOffset(1.0);
    hRatioFrame->GetYaxis()->SetLabelSize(0.13);
    hRatioFrame->GetYaxis()->SetTitleSize(0.13);
    hRatioFrame->GetYaxis()->SetTitleOffset(0.3);
    hRatioFrame->GetYaxis()->SetNdivisions(505);
    hRatioFrame->GetXaxis()->SetNoExponent(true);
    hRatioFrame->Draw();
    TGraphAsymmErrors *gRatioToSM = new TGraphAsymmErrors();
    int nBinsX = hData1D->GetNbinsX();
    for(int i=0;i<nBinsX;i++){
      double BinVal = hData1D->GetBinCenter(i+1);
      double vData = gMuCR_CaloVetoEfficiency_AIPC[iMET]->Eval(BinVal);
      double vMC   = gMuCR_CaloVetoEfficiency_AIPC_SM[iMET]->Eval(BinVal);
      gRatioToSM->SetPoint(i, BinVal, vData/vMC);
    }// for i
    gRatioToSM->Draw("sameEP");
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_CaloVETOEff.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : MuCR Selection Efficiency
  TH1D *hMuCR_ActIPC[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : ActIPC for MuCR (%s), CaloRegion 1", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {35000.0, 50000.0, 60000.0};
    SetupCanvas(1, "Actual Interactions Per Crossing", 10.0, 60.0, 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleMuCR", "actualInteractionsPerCrossing", hTemplate_AIPC, 1.0, GetKinematicsCut(iMET, 2) && CaloVETO_0, 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hMuCR_ActIPC[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hMuCR_ActIPC[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_ActIPC_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Mu CR MET phi
  TH1D *hMuCR_METPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : MET phi for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {24000.0, 35000.0, 45000.0};
    SetupCanvas(1, "MET #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleMuCR", "MET.PhysObjBase.p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 2) && CaloVETO_0, 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_METPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hMuCR_METPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hMuCR_METPhi[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_METPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Mu CR leading jet phi
  TH1D *hMuCR_JetPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet phi for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {24000.0, 35000.0, 45000.0};
    SetupCanvas(1, "leading jet #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleMuCR", "GoodJets[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 2) && CaloVETO_0, 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_JetPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hMuCR_JetPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hMuCR_JetPhi[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_JetPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Mu CR leading jet eta vs phi 2D
  TH2D *hMuCR_JetPhiEta[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Jet PhiEta for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading jet #phi;leading jet #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = false;
    myData[0]->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->Draw("GoodJets[0].p4.Eta():GoodJets[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 2) && CaloVETO_0, "colz");
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_JetPhiEta.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hMuCR_JetPhiEta[iMET] = (TH2D *)hFrame->Clone(Form("hMuCR_JetPhieta_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  // plot CheckList : Mu CR leading lepton phi
  TH1D *hMuCR_LeptonPhi[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Lepton phi for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    double YMax[3] = {24000.0, 35000.0, 45000.0};
    SetupCanvas(1, "leading muon #phi", -TMath::Pi(), TMath::Pi(), 0.0, YMax[iMET]);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);  pad1[1]->SetLogx(false);
    fMaskPt = false;
    DrawTH1RatioPlot("Old4L_Common_singleMuCR", "GoodMuons[0].p4.Phi()", hTemplate_Phi, 1.0, GetKinematicsCut(iMET, 2) && CaloVETO_0, 0);
    DrawText(0.20, 0.90, SumLumi/1000.0, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_LeptonPhi.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    std::cout << Form("hMuCR_LeptonPhi[%d] : ", iMET) << hData1D->GetMaximum() << std::endl;
    hMuCR_LeptonPhi[iMET] = (TH1D *)hData1D->Clone(Form("hMuCR_LeptonPhi_%d", iMET));
    delete pad1[0]; pad1[0]=NULL; delete pad1[1]; pad1[1]=NULL;
  }// for iMET

  // plot CheckList : Mu CR leading lepton eta vs phi 2D
  TH2D *hMuCR_LeptonPhiEta[nMETRegion];
  for(int iMET=0;iMET<nMETRegion;iMET++){
    std::cout << Form("plot : Lepton PhiEta for MuCR (%s)", RegName[iMET].c_str()) << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";leading muon #phi;leading muon #eta", 50, -TMath::Pi(), TMath::Pi(), 30, -3.0, 3.0);
    c0->cd();
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
    pad1[0]->SetLogy(false);  pad1[0]->SetLogx(false);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.15);
    c0->Clear();
    c0->cd();
    pad1[0]->Draw();
    pad1[0]->cd();
    fMaskPt = false;
    myData[0]->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->Draw("GoodMuons[0].p4.Eta():GoodMuons[0].p4.Phi()>>hFrame", GetKinematicsCut(iMET, 2) && CaloVETO_0, "colz");
    double xPos=0.20;
    double yPos=0.90;
    ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
    myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(xPos, yPos-0.12, kBlack, Form("Muon CR (%s)", RegName[iMET].c_str()));
    c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/App/Diagnostic_Chain%d_MuCR_METReg%d_LeptonPhiEta.pdf", iChain, iMET), "pdf");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hMuCR_LeptonPhiEta[iMET] = (TH2D *)hFrame->Clone(Form("hMuCR_LeptonPhieta_%d", iMET));
    delete pad1[0]; pad1[0]=NULL;
  }// for iMET

  c0->Print(Form("%s_Chain%d.pdf]", PDFName.c_str(), iChain), "pdf");
  /*
  tfOutput = new TFile(Form("%s_Chain%d.root", PDFName.c_str(), iChain), "RECREATE");
  tfOutput->mkdir(Form("Chain%d", iChain));
  tfOutput->cd(Form("Chain%d", iChain));
  tfOutput->Close();
  */
  std::cout << "myCheckList : succeeded to run" << std::endl;

  abort();

  return 0;  
}

THStack *GetHStack(std::string HistName, int iHist, int nRebin, int iOpt){
  THStack *ret = new THStack(Form("%s_SM", HistName.c_str()), "");
  if(fSMBG==false)
    return ret;

  TH1D *hSM[nSM];
  int OrderSM[nSM];
  double IntegralSM[nSM];

  for(int iSM=0;iSM<nSM;iSM++){
    hSM[iSM] = (TH1D *)myHistData[0]->GetHistChain1D(HistName, iChain, iHist)->Clone(Form("%s_%s", HistName.c_str(), SMName[iSM].c_str()));
    hSM[iSM]->Reset();
    if(iOpt==0){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(mySM[iSM][iMC16]->size());iDSID++){
	  //double SF = IntLumi[iMC16]/((iSM==(nSM-1) ? myHistSM[iSM][iMC16]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	  double SF = IntLumi[iMC16]/(myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  if(iSM==0 && iMC16==2)
	    SF *= CorrectFactor[iDSID];
	  hSM[iSM]->Add(myHistSM[iSM][iMC16]->at(iDSID)->GetHistChain1D(HistName, iChain, iHist), SF);
	}// for iDSID
      }// for iMC16
    }else{
      for(unsigned int iDSID=0;iDSID<(mySM[iSM][iOpt-1]->size());iDSID++){
	//double SF = IntLumi[iOpt-1]/((iSM==(nSM-1) ? myHistSM[iSM][iOpt-1]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	double SF = IntLumi[iOpt-1]/(myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	if(iSM==0 && iOpt==3)
	  SF *= CorrectFactor[iDSID];
	hSM[iSM]->Add(myHistSM[iSM][iOpt-1]->at(iDSID)->GetHistChain1D(HistName, iChain, iHist), SF);
      }// for iDSID
    }
    hSM[iSM]->SetFillColor(ColSM[iSM]);
    hSM[iSM]->SetLineWidth(1);
    hSM[iSM]->Rebin(nRebin);
    FillOverflowBin(hSM[iSM]);

    OrderSM[iSM] = iSM;
    IntegralSM[iSM] = hSM[iSM]->Integral();
    //    ret->Add(hSM[iSM]);

    //if(iOpt==0 && HistName=="hOld4LKinematics_CutFlow"){
    //std::cout << "iSM = " << iSM << std::setw(15) << SMName[iSM] << ", ";
    //std::cout << std::setw(15) << hSM[iSM]->Integral(9, 9);
    //std::cout << " +- " << std::setw(15) << hSM[iSM]->Integral(9,9) << std::endl;
    //      std::cout << std::setw(15) << hSM[iSM]->GetBinContent(9);
    //      std::cout << " +- " << std::setw(15) << hSM[iSM]->GetBinError(9) << std::endl;
    //}// for iOpt
  }// for iSM

  for(int i=0;i<nSM-1;i++){
    for(int j=0;j<nSM-1-i;j++){
      if(IntegralSM[j] > IntegralSM[j+1]){
	double tmp = IntegralSM[j];
	IntegralSM[j]   = IntegralSM[j+1];
	IntegralSM[j+1] = tmp;

	int tmpInt = OrderSM[j];
	OrderSM[j]   = OrderSM[j+1];
	OrderSM[j+1] = tmpInt;
      }
    }// for j
  }// for i
  for(int iSM=0;iSM<nSM;iSM++){
    tlSM->AddEntry(hSM[OrderSM[iSM]], SMName[OrderSM[iSM]].c_str(), "f");
    ret->Add(hSM[OrderSM[iSM]]);
  }// for iSM
  
  return ret;
}

THStack *GetHStack(std::string TreeName, std::string ValName, double Scale, TCut myCut, int iOpt){
  THStack *ret = new THStack(Form("%s_SM", TreeName.c_str()), "");
  if(fSMBG==false)
    return ret;

  TH1D *hTMPSM = (TH1D *)hData1D->Clone("hTMPSM");
  TH1D *hSM[nSM];
  int OrderSM[nSM];
  double IntegralSM[nSM];
  TCut CutAndWeight;
  if(TreeName=="Old4L_Common_singleEleCR"){
    CutAndWeight = "(weightXsec*weightMCweight*weightPileupReweighting*weightElectronSF)"*myCut;
  }else if(TreeName=="Old4L_Common_singleMuCR"){
    CutAndWeight = "(weightXsec*weightMCweight*weightPileupReweighting*weightMuonSF)"*myCut;
  }else{
    CutAndWeight = "(weightXsec*weightMCweight*weightPileupReweighting)"*myCut;
  }

  for(int iSM=0;iSM<nSM;iSM++){
    hSM[iSM] = (TH1D *)hData1D->Clone(Form("hSM_%s", SMName[iSM].c_str()));
    hSM[iSM]->Reset();
    if(iOpt==0){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(mySM[iSM][iMC16]->size());iDSID++){
	  hTMPSM->Reset();
	  //double SF = IntLumi[iMC16]/((iSM==(nSM-1) ? myHistSM[iSM][iMC16]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	  double SF = IntLumi[iMC16]/(myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  if(iSM==0 && iMC16==2)
	    SF *= CorrectFactor[iDSID];
	  mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s*%lf>>hTMPSM", ValName.c_str(), Scale), CutAndWeight);
	  hSM[iSM]->Add(hTMPSM, SF);
	}// for iDSID
      }// for iMC16
    }else{
      for(unsigned int iDSID=0;iDSID<(mySM[iSM][iOpt-1]->size());iDSID++){
	hTMPSM->Reset();
	//double SF = IntLumi[iOpt-1]/((iSM==(nSM-1) ? myHistSM[iSM][iOpt-1]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	double SF = IntLumi[iOpt-1]/(myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	if(iSM==0 && iOpt==3)
	  SF *= CorrectFactor[iDSID];
	mySM[iSM][iOpt-1]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s*%lf>>hTMPSM", ValName.c_str(), Scale), CutAndWeight);
	hSM[iSM]->Add(hTMPSM, SF);
      }// for iDSID
    }

    hSM[iSM]->SetFillColor(ColSM[iSM]);
    hSM[iSM]->SetLineWidth(1);
    FillOverflowBin(hSM[iSM]);
    OrderSM[iSM] = iSM;
    IntegralSM[iSM] = hSM[iSM]->Integral();
    //ret->Add(hSM[iSM]);
  }// for iSM

  for(int i=0;i<nSM-1;i++){
    for(int j=0;j<nSM-1-i;j++){
      if(IntegralSM[j] > IntegralSM[j+1]){
	double tmp = IntegralSM[j];
	IntegralSM[j]   = IntegralSM[j+1];
	IntegralSM[j+1] = tmp;

	int tmpInt = OrderSM[j];
	OrderSM[j]   = OrderSM[j+1];
	OrderSM[j+1] = tmpInt;
      }
    }// for j
  }// for i
  for(int iSM=0;iSM<nSM;iSM++){
    tlSM->AddEntry(hSM[OrderSM[iSM]], SMName[OrderSM[iSM]].c_str(), "f");
    ret->Add(hSM[OrderSM[iSM]]);
  }// for iSM

  delete hTMPSM; hTMPSM=NULL;

  return ret;
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

void SmearPt(TH1D *hHoge, double pt, double weight, bool f2Flag){
 for(int i=0;i<nLogPt;i++){
    double ptLow = XBinsLogPt[i];
    double ptUp  = XBinsLogPt[i+1];
    if(i==(nLogPt-1)){
      ptUp = 12500.0;
    }

    int iFunc=-1;
    if(f2Flag){
      if(pt < 15.0){
	iFunc = 0;
      }else if(pt < 20.0){
	iFunc = 1;
      }else if(pt < 25.0){
	iFunc = 2;
      }else if(pt < 35.0){
	iFunc = 3;
      }else if(pt < 45.0){
	iFunc = 4;
      }else if(pt < 60.0){
	iFunc = 5;
      }else if(pt < 100.0){
	iFunc = 6;
      }else{
	iFunc = 7;
      }
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(f2Flag==false){
      w1Low = fSmearInt->Eval(qoverptLow);
      w1Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(f2Flag){
      w2Low = fSmearInt->Eval(qoverptLow);
      w2Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 

int DrawTH1RatioPlot(std::string TreeName, std::string ValName, TH1D *hHist, double Scale, TCut myCut, int iOpt){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  
  delete hData1D;
  hData1D = (TH1D *)hHist->Clone("hData1D");
  hData1D->Reset();

  if((TreeName=="Old4L_Common_SR") && fMaskPt){
    TCut PtCut=Form("(DisappearingTracks.p4.Pt()/1000.0 < %lf)", MaskPt);
    myData[iOpt]->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s*%lf>>hData1D", ValName.c_str(), Scale), myCut*PtCut); // for blind
  }else{
    myData[iOpt]->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s*%lf>>hData1D", ValName.c_str(), Scale), myCut);  
  }
  tl->AddEntry(hData1D, DataTag[iOpt].c_str(), "PE");
  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  hData1D->SetBinErrorOption(TH1::kPoisson);
  hData1D->Rebin(RebinVal);
  FillOverflowBinForData(hData1D);

  delete hStack_SM;
  hStack_SM = GetHStack(TreeName, ValName, Scale, myCut, iOpt);

  hFrame->Draw();
  hStack_SM->Draw("samehist");
  hData1D->Draw("sameEP");

  if(fSMBG)
    DrawRatioPlot();
  
  return 0;
}

int DrawTH1RatioPlot(std::string HistName, int iHist, int nRebin, int iOpt, int SameOpt){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  
  hFrame->Draw();
  delete hData1D;
  hData1D = (TH1D *)myHistData[0]->GetHistChain1D(HistName, iChain, iHist)->Clone("hData1D");
  hData1D->Reset();

  if(SignalMode!=0 && SameOpt==1){
    if(SignalMode==1){
      for(int i=0;i<nSignal_EWK;i++){
	delete hSignal1D_EWK[i]; hSignal1D_EWK[i]=NULL;
	hSignal1D_EWK[i] = (TH1D *)myHistData[0]->GetHistChain1D(HistName, iChain, iHist)->Clone(Form("hSignal1D_EWK_%d", i));
	hSignal1D_EWK[i]->Reset();
      }// for i
    }else if(SignalMode==2){
      for(int i=0;i<nSignal_Strong;i++){
	delete hSignal1D_Strong[i]; hSignal1D_Strong[i]=NULL;
	hSignal1D_Strong[i] = (TH1D *)myHistData[0]->GetHistChain1D(HistName, iChain, iHist)->Clone(Form("hSignal1D_Strong_%d", i));
	hSignal1D_Strong[i]->Reset();
      }// for i
    }
  }
  hData1D->Add(myHistData[iOpt]->GetHistChain1D(HistName, iChain, iHist), 1.0);
  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  hData1D->SetBinErrorOption(TH1::kPoisson);
  tl->AddEntry(hData1D, DataTag[iOpt].c_str(), "PE");
  
  delete hStack_SM;
  hStack_SM = GetHStack(HistName, iHist, nRebin, iOpt);
  hStack_SM->Draw("samehist");

  hData1D->Rebin(nRebin);
  FillOverflowBinForData(hData1D);
  hData1D->Draw("sameEP");

  if(SignalMode!=0 && SameOpt==1){
    if(SignalMode==1){
      if(iOpt==0){
	for(int iMC16=0;iMC16<nMC16;iMC16++){
	  for(int iSignal=0;iSignal<nSignal_EWK;iSignal++){	  
	    for(unsigned int iDSID=0;iDSID<(vDSID_EWK[iSignal].size());iDSID++){
	      hSignal1D_EWK[iSignal]->Add(myHistSignal_EWK[iMC16][iSignal]->at(iDSID)->GetHistChain1D(HistName, iChain, iHist), vSFSig_EWK[iMC16][iSignal].at(iDSID));
	    }// for iDSID
	  }// for iSignal
	}// for iMC16
      }else{
	for(int iSignal=0;iSignal<nSignal_EWK;iSignal++){
	  for(unsigned int iDSID=0;iDSID<(vDSID_EWK[iSignal].size());iDSID++){
	    hSignal1D_EWK[iSignal]->Add(myHistSignal_EWK[iOpt-1][iSignal]->at(iDSID)->GetHistChain1D(HistName, iChain, iHist), vSFSig_EWK[iOpt-1][iSignal].at(iDSID));
	  }
	}// for iSignal
      }
      for(int iSignal=0;iSignal<nSignal_EWK;iSignal++){
	hSignal1D_EWK[iSignal]->SetMarkerStyle(1);
	hSignal1D_EWK[iSignal]->SetLineColor(SigColor[iSignal]);
	hSignal1D_EWK[iSignal]->SetLineWidth(3);
	hSignal1D_EWK[iSignal]->Rebin(nRebin);
	FillOverflowBin(hSignal1D_EWK[iSignal]);
	hSignal1D_EWK[iSignal]->Draw("sameEhist");
	tl->AddEntry(hSignal1D_EWK[iSignal], Form("Signal (%d GeV)", CMass_EWK[iSignal]), "El");
      }// for iSignal
    }else if(SignalMode==2){
      if(iOpt==0){
	for(int iMC16=0;iMC16<nMC16;iMC16++){
	  for(int iSignal=0;iSignal<nSignal_Strong;iSignal++){
	    hSignal1D_Strong[iSignal]->Add(myHistSignal_Strong[iMC16][iSignal]->GetHistChain1D(HistName, iChain, iHist), SFSig_Strong[iMC16][iSignal]);
	  }// for iSignal
	}// for iMC16
      }else{
	for(int iSignal=0;iSignal<nSignal_Strong;iSignal++){
	  hSignal1D_Strong[iSignal]->Add(myHistSignal_Strong[iOpt-1][iSignal]->GetHistChain1D(HistName, iChain, iHist), SFSig_Strong[iOpt-1][iSignal]);
	}// for iSignal
      }
      for(int iSignal=0;iSignal<nSignal_Strong;iSignal++){
	hSignal1D_Strong[iSignal]->SetMarkerStyle(1);
	hSignal1D_Strong[iSignal]->SetLineColor(SigColor[iSignal]);
	hSignal1D_Strong[iSignal]->SetLineWidth(3);
	hSignal1D_Strong[iSignal]->Rebin(nRebin);
	FillOverflowBin(hSignal1D_Strong[iSignal]);
	hSignal1D_Strong[iSignal]->Draw("sameEhist");
	tl->AddEntry(hSignal1D_Strong[iSignal], Form("Signal (%d, %d)", GMass_Strong[iSignal], CMass_Strong[iSignal]), "El");
      }// for iSignal
    }// strong
  }
  /*
  c0->cd();
  pad1[1]->SetGridy();
  pad1[1]->Draw();
  pad1[1]->cd();
  hRatioFrame->Draw();
  TH1D *hRatioToSM  = (TH1D *)hData1D->Clone(Form("%s_RatioToSM", HistName.c_str()));
  //  hRatioToSM->Reset();
  TH1D *hMCStat     = (TH1D *)hData1D->Clone(Form("%s_MCStat", HistName.c_str()));
  hMCStat->Reset();
  int nNewBinsX = hRatioToSM->GetNbinsX();
  for(int i=0;i<(nNewBinsX+2);i++){
    double BinVal   = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinContent(i);
    double BinError = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinError(i);
    if(BinVal==0.0){
      BinVal=1e-10;
    }
    hRatioToSM->SetBinError(i,   hRatioToSM->GetBinError(i)/BinVal);
    hRatioToSM->SetBinContent(i, hRatioToSM->GetBinContent(i)/BinVal);
    
    hMCStat->SetBinError(i, BinError/BinVal);
    hMCStat->SetBinContent(i, 1);
  }// for ibin
  hRatioToSM->SetMarkerSize(1.0);
  hRatioToSM->SetMarkerStyle(8);
  hRatioToSM->SetMarkerColor(kBlack);
  hRatioToSM->SetLineColor(kBlack);
  hRatioToSM->Draw("sameEP");
  hMCStat->SetFillColor(kRed);
  hMCStat->SetFillStyle(3002);
  hMCStat->SetMarkerSize(0);
  hMCStat->Draw("sameE2");

  c0->cd();
  pad1[0]->RedrawAxis();
  c0->cd();
  */
  if(fSMBG)
    DrawRatioPlot();
  return 0;
}

int FillOverflowBin(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  double LowVal      = h1->GetBinContent(0);
  double LowError    = h1->GetBinError(0);
  double LowOrgVal   = h1->GetBinContent(1);
  double LowOrgError = h1->GetBinError(1);
  double HighVal      = h1->GetBinContent(nBinsX+1);
  double HighError    = h1->GetBinError(nBinsX+1);
  double HighOrgVal   = h1->GetBinContent(nBinsX);
  double HighOrgError = h1->GetBinError(nBinsX);

  h1->SetBinError(1,   TMath::Sqrt(LowError*LowError + LowOrgError*LowOrgError));
  h1->SetBinContent(1, LowVal + LowOrgVal);

  h1->SetBinError(nBinsX, TMath::Sqrt(HighError*HighError + HighOrgError*HighOrgError));
  h1->SetBinContent(nBinsX, HighVal + HighOrgVal);

  return 0;
}

int FillOverflowBinForData(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  h1->AddBinContent(1, h1->GetBinContent(0));
  h1->SetBinContent(0, 0.0);
  h1->AddBinContent(nBinsX  , h1->GetBinContent(nBinsX+1));
  h1->SetBinContent(nBinsX+1, 0.0);

  return 0;
}

int SetupCanvas(int iOption, std::string xTitle, double MinX, double MaxX, double MinY, double MaxY){
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", Form(";%s;Tracks", xTitle.c_str()), 100, MinX, MaxX, 100, MinY, MaxY);
  delete hRatioFrame; hRatioFrame=NULL;
  hRatioFrame = new TH2D("hRatioFrame", Form(";%s;data/MC ", xTitle.c_str()), 100, MinX, MaxX, 100, 0.2, 1.8);

  c0->cd();
  if(fSMBG)
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.35, 1.0, 1.0);
  else
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
  pad1[1] = new TPad("pad1_1", "pad1_1", 0.0, 0.0, 1.0, 0.35);

  hFrame->GetXaxis()->SetNoExponent(true);
  
  pad1[0]->SetLogy(false);
  pad1[0]->SetLogx(false);
  if(fSMBG){
    hFrame->GetYaxis()->SetTitleOffset(0.6);
    hFrame->GetYaxis()->SetTitleSize(0.07);
    hFrame->GetYaxis()->SetLabelSize(0.07);
    pad1[0]->SetBottomMargin(0);
    pad1[0]->SetLeftMargin(0.12);
    pad1[1]->SetTopMargin(0);
    pad1[1]->SetLeftMargin(0.12);
    pad1[1]->SetBottomMargin(0.35);
  }else{
    hFrame->GetYaxis()->SetTitleOffset(0.8);
    hFrame->GetYaxis()->SetTitleSize(0.06);
    hFrame->GetYaxis()->SetLabelSize(0.06);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.05);
  }
  hRatioFrame->GetXaxis()->SetLabelSize(0.15);
  hRatioFrame->GetXaxis()->SetTitleSize(0.15);
  hRatioFrame->GetXaxis()->SetTitleOffset(1.0);
  hRatioFrame->GetYaxis()->SetLabelSize(0.13);
  hRatioFrame->GetYaxis()->SetTitleSize(0.13);
  hRatioFrame->GetYaxis()->SetTitleOffset(0.3);
  hRatioFrame->GetYaxis()->SetNdivisions(505);
  hRatioFrame->GetXaxis()->SetNoExponent(true);

  if(iOption==0){
    // right top : data+signal+bg
    delete tl;
    tl = new TLegend(0.20, 0.93-0.18, 0.82, 0.93-0.10);
    //    tl = new TLegend(0.30, 0.93-0.18, 0.92, 0.93-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1, 0.9, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==1){
    // right top : signal+bg or data+bg
    delete tl;
    tl = new TLegend(0.60, 0.94-0.18, 0.92, 0.94-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1, 0.9, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==2){
    // left top : data+bg
    delete tl;
    tl = new TLegend(0.50-0.35, 0.94-0.18, 0.92-0.35, 0.94-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50-0.35, 0.94-0.1, 0.9-0.35, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==3){
    // right bottom : data+bg
    delete tl;
    tl = new TLegend(0.50, 0.94-0.18-0.12, 0.92, 0.94-0.10-0.12);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1-0.12, 0.90, 0.94-0.12);
    tlSM->SetNColumns(5);
  }

  return 0;
}

int DrawRatioPlot(void){
  c0->cd();
  pad1[1]->SetGridy();
  pad1[1]->Draw();
  pad1[1]->cd();
  hRatioFrame->Draw();
  TGraphAsymmErrors *gRatioToSM = new TGraphAsymmErrors();
  //TH1D *hRatioToSM  = (TH1D *)hData1D->Clone("hRa
  TH1D *hMCStat     = (TH1D *)hData1D->Clone("hMCStat");
  //TH1D *hRatioToSM  = (TH1D *)hData1D->Clone(Form("%s_%s_RatioToSM", TreeName.c_str(), BranchName.c_str()));
  //TH1D *hMCStat     = (TH1D *)hData1D->Clone(Form("%s_%s_MCStat", TreeName.c_str(), BranchName.c_str()));
  hMCStat->Reset();
  int nBinsX = hData1D->GetNbinsX();
  for(int i=0;i<nBinsX;i++){
    double BinVal   = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinContent(i+1);
    double BinError = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinError(i+1);
    if(BinVal==0)
      continue;
    gRatioToSM->SetPoint(i, hData1D->GetBinCenter(i+1), hData1D->GetBinContent(i+1)/BinVal);
    gRatioToSM->SetPointEYlow(i, hData1D->GetBinErrorLow(i+1)/BinVal);
    gRatioToSM->SetPointEYhigh(i, hData1D->GetBinErrorUp(i+1)/BinVal);
    gRatioToSM->SetPointEXlow(i, hData1D->GetBinCenter(i+1) - hData1D->GetBinLowEdge(i+1));
    gRatioToSM->SetPointEXhigh(i, hData1D->GetBinLowEdge(i+2) - hData1D->GetBinCenter(i+1));
    
    hMCStat->SetBinError(i+1, BinError/BinVal);
    hMCStat->SetBinContent(i+1, 1);
  }// for ibin
  gRatioToSM->SetMarkerSize(1.0);
  gRatioToSM->SetMarkerStyle(8);
  gRatioToSM->SetMarkerColor(kBlack);
  gRatioToSM->Draw("sameEP");
  hMCStat->SetFillColor(kRed);
  hMCStat->SetFillStyle(3002);
  hMCStat->SetMarkerSize(0);
  hMCStat->Draw("sameE2");
  
  c0->cd();
  pad1[0]->RedrawAxis();
  c0->cd();

  return 0;
}

int DrawText(double xPos, double yPos, double iLumi, std::string myText1, std::string myText2){
  ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
  myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", iLumi));
  myText(xPos, yPos-0.12, kBlack, Form("%s", myText1.c_str()));
  myText(xPos, yPos-0.18, kBlack, Form("%s", myText2.c_str()));
  tl->Draw("same");
  tlSM->Draw("same");

  return 0;
}

TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), BoundaryOfMiddleMET, METType.c_str());
    }else if(iMETRegion==2){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < %lf)", METType.c_str(), METType.c_str(), BoundaryOfMiddleMET);
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 15.0)";
  ret = ret && Form("(DisappearingTracks.p4.Pt()/1000.0 > %lf)", PtThresholdForCR);

  return ret;
}

int DrawTrackPt(std::string TreeName, TCut myCut, int iOpt, int iTFDisap, int iTFMSCalo, int iCorrect, bool fSmear){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();

  delete hData1D;
  hData1D = (TH1D *)hTemplate_TrackPt->Clone("hData1D");
  hData1D->Reset();

  eventList->Reset();
  myData[iOpt]->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", myCut);
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData[iOpt]->GetNtupleReader(TreeName)->GetEntry(eventList->GetEntry(iEntry));

    double pT  = myData[iOpt]->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Pt()/1000.0;
    double Eta = myData[iOpt]->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Eta();
    double Phi = myData[iOpt]->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Phi();
    double TF  = GetTF(pT, Eta, Phi, iTFDisap, iTFMSCalo, iCorrect);
    if(fSmear){
      SmearPt(hData1D, pT, TF, true);
    }else{
      hData1D->Fill(pT, TF);
    }
  }// for iEntry
  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  //hData1D->SetBinErrorOption(TH1::kPoisson);
  FillOverflowBinForData(hData1D);
  tl->AddEntry(hData1D, DataTag[0].c_str(), "PE");

  delete hStack_SM;
  hStack_SM = new THStack("hStack_SM", "");
  int OrderSM[nSM];
  double IntegralSM[nSM];
  TH1D *hSM[nSM];
  TH1D *hTMPSM = (TH1D *)hData1D->Clone("hTMPSM");
  if(fSMBG){
    for(int iSM=0;iSM<nSM;iSM++){
      hSM[iSM] = (TH1D *)hData1D->Clone(Form("hSM_%s", SMName[iSM].c_str()));
      hSM[iSM]->Reset();
      h1000->Reset();
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(mySM[iSM][iMC16]->size());iDSID++){
	  hTMPSM->Reset();
	  //double SF = IntLumi[iMC16]/((iSM==(nSM-1) ? myHistSM[iSM][iMC16]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	  double SF = IntLumi[iMC16]/(myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  if(iSM==0 && iMC16==2)
	    SF *= CorrectFactor[iDSID];
	  eventList->Reset();
	  mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", myCut);

	  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	    mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->GetEntry(eventList->GetEntry(iEntry));

	    double weight = (mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->weightXsec)*
	      (mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->weightMCweight)*
	      (mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->weightPileupReweighting);

	    double pT  = mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Pt()/1000.0;
	    double Eta = mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Eta();
	    double Phi = mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Phi();
	    double TF  = GetTF(pT, Eta, Phi, iTFDisap, iTFMSCalo, iCorrect);
	    if(fSmear){
	      SmearPt(hTMPSM, pT, weight*TF, true);
	    }else{
	      hTMPSM->Fill(pT, weight*TF);
	    }
	  }// for iEntry
	  hSM[iSM]->Add(hTMPSM, SF);
	}// for iDSID
      }// for iMC16
      hSM[iSM]->SetFillColor(ColSM[iSM]);
      hSM[iSM]->SetLineWidth(1);
      FillOverflowBin(hSM[iSM]);
      OrderSM[iSM] = iSM;
      IntegralSM[iSM] = hSM[iSM]->Integral();
    }// for iSM

    for(int i=0;i<nSM-1;i++){
      for(int j=0;j<nSM-1-i;j++){
	if(IntegralSM[j] > IntegralSM[j+1]){
	  double tmp = IntegralSM[j];
	  IntegralSM[j]   = IntegralSM[j+1];
	  IntegralSM[j+1] = tmp;

	  int tmpInt = OrderSM[j];
	  OrderSM[j]   = OrderSM[j+1];
	  OrderSM[j+1] = tmpInt;
	}
      }// for j
    }// for i
    for(int iSM=0;iSM<nSM;iSM++){
      tlSM->AddEntry(hSM[OrderSM[iSM]], SMName[OrderSM[iSM]].c_str(), "f");
      hStack_SM->Add(hSM[OrderSM[iSM]]);
    }// for iSM
  }// fSMBG
  delete hTMPSM; hTMPSM=NULL;
  hFrame->Draw();
  hStack_SM->Draw("samehist");
  hData1D->Draw("sameEP");
  if(fSMBG)
    DrawRatioPlot();

  return 0;
}

double GetTF(double pT, double Eta, double Phi, int iTFDisap, int iTFMSCalo, int iCorrect){
  double ret=1.0;
  /*
  int iBinInvPt = (int)(50.0/(0.1*pT)) + 1;
  int iBinPt  = (int)(18.0*(TMath::Log(pT/4.0)/TMath::Log(5.0)) + 1);
  int iBinEta = (int)(Eta/0.2) + 13;
  int iBinPhi = (int)(50.0*(Phi+TMath::Pi())/(2.0*TMath::Pi())) + 1;
  */
  if(iTFDisap==0){
    int iBinPt  = hEleBG_TF_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFDisap==1){
    int iBinInvPt  = hEleBG_TF_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFDisap==2){
    ret *= hMuBG_TF_1Bin->GetBinContent(1);
  }

  if(iTFMSCalo==3){
    int iBinPt  = hEleBG_TF_CaloIso_4to0_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_4to0_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to0_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==4){
    int iBinPt  = hEleBG_TF_CaloIso_4to123_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_4to123_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to123_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==0){ // CR
    int iBinPt  = hEleBG_TF_CaloIso_432to0_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to0_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to0_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==1){ // VR
    int iBinPt  = hEleBG_TF_CaloIso_432to1_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to1_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to1_PtEta->GetBinContent(iBinPt, iBinEta);
  }else if(iTFMSCalo==2){ // Old
    int iBinPt  = hEleBG_TF_CaloIso_432to01_PtEta->GetXaxis()->FindBin(pT);
    int iBinEta = hEleBG_TF_CaloIso_432to01_PtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to01_PtEta->GetBinContent(iBinPt, iBinEta);

  }else if(iTFMSCalo==8){
    int iBinInvPt  = hEleBG_TF_CaloIso_4to0_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_4to0_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to0_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==9){
    int iBinInvPt  = hEleBG_TF_CaloIso_4to123_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_4to123_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_4to123_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==5){ // CR
    int iBinInvPt  = hEleBG_TF_CaloIso_432to0_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_432to0_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to0_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==6){ // VR
    int iBinInvPt  = hEleBG_TF_CaloIso_432to1_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_432to1_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to1_InvPtEta->GetBinContent(iBinInvPt, iBinEta);
  }else if(iTFMSCalo==7){ // Old
    int iBinInvPt  = hEleBG_TF_CaloIso_432to01_InvPtEta->GetXaxis()->FindBin(1.0/pT);
    int iBinEta    = hEleBG_TF_CaloIso_432to01_InvPtEta->GetYaxis()->FindBin(Eta);
    ret *= hEleBG_TF_CaloIso_432to01_InvPtEta->GetBinContent(iBinInvPt, iBinEta);

  }else if(iTFMSCalo==10){
    int iBinPhi = hMSBG_TF_PhiEta->GetXaxis()->FindBin(Phi);
    int iBinEta = hMSBG_TF_PhiEta->GetYaxis()->FindBin(Eta);
    ret *= hMSBG_TF_PhiEta->GetBinContent(iBinPhi, iBinEta);
  }

  //int iBin =  (int)(15.0*(TMath::Log(pT/5.0)/TMath::Log(20.0)) + 1);
  if(iCorrect==0){ // CR
    int iBin = P_caloveto05_hadron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_hadron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_hadron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==1){ // VR
    int iBin = P_caloveto05_10_hadron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_10_hadron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_10_hadron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==2){ // Old
    int iBin = P_caloveto10_hadron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto10_hadron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto10_hadron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;

  }else if(iCorrect==10){ // CR
    int iBin = P_caloveto05_electron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_electron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_electron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==11){ // VR
    int iBin = P_caloveto05_10_electron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto05_10_electron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto05_10_electron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }else if(iCorrect==12){ // Old
    int iBin = P_caloveto10_electron_StdTrk->FindBin(pT);
    double vStdTrk = P_caloveto10_electron_StdTrk->GetBinContent(iBin);
    double vTrklet = P_caloveto10_electron_Trklet->GetBinContent(iBin);
    if(vStdTrk>0 && vTrklet>0)
      ret *= vTrklet/vStdTrk;
  }

  return ret;
}

double CalcFracError(double vNume, double vDenom, double eNume, double eDenom){
  return TMath::Sqrt((eNume/vDenom)*(eNume/vDenom) + (vNume*eDenom/(vDenom*vDenom))*(vNume*eDenom/(vDenom*vDenom)));
}

double CalcABCDError(double CRL, double CRH, double VRL, double VRH){
  double eCRL = TMath::Sqrt(CRL);
  double eCRH = TMath::Sqrt(CRH);
  double eVRL = TMath::Sqrt(VRL);
  double eVRH = TMath::Sqrt(VRH);

  double eNume  = TMath::Sqrt((eVRH*CRL)*(eVRH*CRL) + (VRH*eCRL)*(VRH*eCRL));
  double eDenom = TMath::Sqrt((eVRL*CRH)*(eVRL*CRH) + (VRL*eCRH)*(VRL*eCRH));

  return CalcFracError(VRH*CRL, VRL*CRH, eNume, eDenom);
}

int SetGraphPoint(double vNume, double vDenom, TGraphAsymmErrors *gGraph, int iPoint, bool fZeroError){
  double eNume  = TMath::Sqrt(vNume);
  double eDenom = TMath::Sqrt(vDenom);
  gGraph->SetPoint(iPoint, iPoint+0.5, vNume/vDenom);
  if(fZeroError){
    gGraph->SetPointEYhigh(iPoint, 0);
    gGraph->SetPointEYlow(iPoint, 0);
  }else{
    gGraph->SetPointEYhigh(iPoint, TMath::Sqrt((eNume/vDenom)*(eNume/vDenom) + (vNume*eDenom/(vDenom*vDenom))*(vNume*eDenom/(vDenom*vDenom))));
    gGraph->SetPointEYlow(iPoint, TMath::Sqrt((eNume/vDenom)*(eNume/vDenom) + (vNume*eDenom/(vDenom*vDenom))*(vNume*eDenom/(vDenom*vDenom))));
  }

  return 0;
}
