#include "myPDFStudyFile.h"
namespace po = boost::program_options;
int LoadInput(void);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("doSmear", po::value<bool>()->default_value(true), "doSmearing")
    ("SimpleSmear", po::value<bool>()->default_value(false), "SimpleSmearing")
    ("output-file,o", po::value<std::string>()->default_value("myPDFStudyFile"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  fDoSmearing = vm["doSmear"].as<bool>();
  fSimpleSmear = vm["SimpleSmear"].as<bool>();

  LoadInput();

  myData = new MyManager(DataFile, "Data");
  myData->LoadMyFile();
  //myHistData = myData->myHist_Old4L;
  std::string TreeName;

  for(int iCat=0;iCat<nCat;iCat++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      std::cout << CatName[iCat] << RegName[iMET] << std::endl;
      TreeName=Form("Old4L_Common_%sCR", CatName[iCat].c_str());
      eventList->Reset();

      std::cout << TreeName << std::endl;
      if(iCat==0){
	myData->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && D0Cut);
      }else if(iCat==1){
	myData->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 1) && D0Cut);
      }else if(iCat==2){
	myData->GetNtupleReader(TreeName)->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 2) && D0Cut);
      }

      std::cout << " N = " << eventList->GetN() << std::endl;
      for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	if((iEntry+1)%10000==0)
	  std::cout << " - " << iEntry+1 << std::endl;

	myData->GetNtupleReader(TreeName)->GetEntry(eventList->GetEntry(iEntry));
	b_pT  = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Pt()/1000.0;
	b_Eta = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Eta();
	b_Phi = myData->GetNtupleReader(TreeName)->Tracks->at(0)->p4.Phi();
	b_Calo= myData->GetNtupleReader(TreeName)->Tracks->at(0)->etclus20_topo/1000.0;
	treeCat[iCat*3 + iMET]->Fill();
      }// for iEntry
    }// for iMET
  }// for iCat

  tfOutput = new TFile(Form("%s.root", PDFName.c_str()), "RECREATE");
  for(int i=0;i<(nCat*nMETRegion);i++){
    treeCat[i]->Write();
  }
  tfOutput->Close();

  return 0;  
}

int LoadInput(void){
  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/tmp/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  hLumiHistData15 = (TH1F *)tfInput->Get("lumi_histo_data15");
  hLumiHistData16 = (TH1F *)tfInput->Get("lumi_histo_data16");
  hLumiHistData17 = (TH1F *)tfInput->Get("lumi_histo_data17");
  hLumiHistData18 = (TH1F *)tfInput->Get("lumi_histo_data18");
  hEleBG_TF_PtEta    = (TH2D *)tfInput->Get("hEleBG_TF_PtEta");
  hEleBG_TF_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_InvPtEta");
  hEleBG_TF_CaloIso_4to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_PtEta");
  hEleBG_TF_CaloIso_4to123_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_PtEta");
  hEleBG_TF_CaloIso_432to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_PtEta");
  hEleBG_TF_CaloIso_432to1_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_PtEta");
  hEleBG_TF_CaloIso_432to01_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_PtEta");
  hEleBG_TF_CaloIso_4to0_InvPtEta   = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_InvPtEta");
  hEleBG_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_InvPtEta");
  hEleBG_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_InvPtEta");
  hEleBG_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_InvPtEta");
  hEleBG_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_InvPtEta");
  hMuBG_TF_1Bin     = (TH1D *)tfInput->Get("hMuBG_TF_1Bin");
  hMuBG_TF_13Bin    = (TH1D *)tfInput->Get("hMuBG_TF_13Bin");
  hMuBG_TF_PtEta    = (TH2D *)tfInput->Get("hMuBG_TF_PtEta");
  hMuBG_TF_InvPtEta = (TH2D *)tfInput->Get("hMuBG_TF_InvPtEta");
  hMSBG_TF_PhiEta   = (TH2D *)tfInput->Get("hMSBG_TF_PhiEta");
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta    = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta");

  P_caloveto05_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto05_hadron_StdTrk");
  P_caloveto10_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto10_hadron_StdTrk");
  P_caloveto05_10_hadron_StdTrk   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_StdTrk");  
  P_caloveto05_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto05_hadron_Trklet");     
  P_caloveto10_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto10_hadron_Trklet");     
  P_caloveto05_10_hadron_Trklet   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_Trklet");  
							                                 
  P_caloveto05_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto05_electron_StdTrk");   
  P_caloveto10_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto10_electron_StdTrk");   
  P_caloveto05_10_electron_StdTrk = (TH1D *)tfInput->Get("P_caloveto05_10_electron_StdTrk");
  P_caloveto05_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto05_electron_Trklet");   
  P_caloveto10_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto10_electron_Trklet");   
  P_caloveto05_10_electron_Trklet = (TH1D *)tfInput->Get("P_caloveto05_10_electron_Trklet");

  hCorrHad = (TH1D *)tfInput->Get("hCorrHad_caloveto05");
  hCorrHad_Side = (TH1D *)tfInput->Get("hCorrHad_caloveto05_10");
  hCorrEle = (TH1D *)tfInput->Get("hCorrEle_caloveto05");
  hCorrEle_Side = (TH1D *)tfInput->Get("hCorrEle_caloveto05_10");

  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");
  gErrorIgnoreLevel = 1001;

  fSmearInt = new TF1("fSmearInt", [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
  fSmearInt->SetParameter(0, g_par_Mean);
  fSmearInt->SetParameter(1, g_par_Sigma);
  fSmearInt->SetParameter(2, g_par_Alpha);

  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i
  c0 = new TCanvas("c0", "", 800, 600);
  c0->SetLogx();
  c0->SetLogy();

  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nLogPt, XBinsLogPt);
  hData1D = new TH1D("hData1D", "hoge", 10, 0, 10);
  double xBins[961];
  xBins[0]=5.0;
  for(int i=1;i<=960;i++){
    xBins[i] = xBins[i-1]*(pow(12500.0/5.0, 1.0/960.0));
  }
  hSimpleSmear = new TH1D("hSimpleSmear", "", 960, xBins);

  for(int iCat=0;iCat<nCat;iCat++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      int i = iCat*3 + iMET;
      //treeCat[i] = new TTree(Form("tree%s", CatName[i].c_str()), Form("tree%s", CatName[i].c_str()));
      treeCat[i] = new TTree(Form("tree%s%s", CatName[iCat].c_str(), RegName[iMET].c_str()), Form("tree%s%s", CatName[iCat].c_str(), RegName[iMET].c_str()));
      treeCat[i]->Branch("pT" , &b_pT , "pT/D");
      treeCat[i]->Branch("Eta", &b_Eta, "Eta/D");
      treeCat[i]->Branch("Phi", &b_Phi, "Phi/D");
      //treeCat[i]->Branch("MET", &b_MET, "MET/D");
      treeCat[i]->Branch("Calo" , &b_Calo , "Calo/D");

      //treeCat[i]->Branch("vTF", &b_vTF, "vTF/D");
      //treeCat[i]->Branch("eTF", &b_eTF, "eTF/D");
      /*
	treeCat[i]->Branch("Bin0" , &b_Bin0 , "Bin0/D");
	treeCat[i]->Branch("Bin1" , &b_Bin1 , "Bin1/D");
	treeCat[i]->Branch("Bin2" , &b_Bin2 , "Bin2/D");
	treeCat[i]->Branch("Bin3" , &b_Bin3 , "Bin3/D");
	treeCat[i]->Branch("Bin4" , &b_Bin4 , "Bin4/D");
	treeCat[i]->Branch("Bin5" , &b_Bin5 , "Bin5/D");
	treeCat[i]->Branch("Bin6" , &b_Bin6 , "Bin6/D");
	treeCat[i]->Branch("Bin7" , &b_Bin7 , "Bin7/D");
	treeCat[i]->Branch("Bin8" , &b_Bin8 , "Bin8/D");
	treeCat[i]->Branch("Bin9" , &b_Bin9 , "Bin9/D");
	treeCat[i]->Branch("Bin10", &b_Bin10, "Bin10/D");
	treeCat[i]->Branch("Bin11", &b_Bin11, "Bin11/D");
      */
    }// for iMET
  }// for iCat

  return 0;
}
