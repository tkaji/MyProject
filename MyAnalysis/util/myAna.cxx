#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/NtupleReader.h"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("myOption", po::value<int>(), "0: weightXsec,MCweight,PileupReweighting=1, 1:weightXsec=1, 2:fill trigger efficiency")
    ("fKinematicsOnly", po::value<bool>()->default_value(false), "run kinematics only for draw study")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("DRAWEffPath", po::value<std::string>(), "path of DRAW filter efficiency for MC")
    ("output-file,o", po::value<std::string>()->default_value("out.root"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  std::vector<std::string> ReadableInputFileList;
  if(vm["grid"].as<int>()==0){
    ReadableInputFileList = InputFileList;
  }else{
    std::string CommaList = InputFileList.at(0);
    while(true){
      std::string::size_type pos = CommaList.find(',');
      if(pos != std::string::npos){
	ReadableInputFileList.push_back(CommaList.substr(0, pos));
	CommaList.erase(0, pos+1);
      }else{
	ReadableInputFileList.push_back(CommaList);
	break;
      }
    }
  }
  std::cout << "Input : " << ReadableInputFileList.at(0) << std::endl;
  std::string OutputFileName = vm["output-file"].as<std::string>();
  //std::string DRAWEffPath = vm["DRAWEffPath"].as<std::string>();

  //MyManager *myMana = new MyManager(InputFileList, OutputFileName);
  MyManager *myMana = new MyManager(ReadableInputFileList, OutputFileName);
  if(vm.count("myOption"))
  myMana->SetMyOption(vm["myOption"].as<int>());
  //if(vm.count("fKinematicsOnly"))
  //myMana->SetKinematicsOnly(vm["fKinematicsOnly"].as<bool>());
  //if(vm.count("DRAWEffPath"))
  //myMana->SetDRAWEffPath(vm["DRAWEffPath"].as<std::string>());

  myMana->Run();

  return 0;
}
