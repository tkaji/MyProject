#ifndef SignalSyst_h
#define SignalSyst_h

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"

class PhysObjBase;
class myTrack;

void SmearPt(TH1D *hHoge, double pt, double weight, bool fMuonFlag);
const int nBinsMySignal = 12;
const double XBinsMySignal[nBinsMySignal+1] = {20, 26.1532, 34.1995, 44.7214, 60.0, 76.4724, 100, 170.998, 292.402, 500, 1118.03, 2500, 12500};
TH1D *hTrackPt;
TFile *tfInput;
const int nMC16 = 3;
const int nSmearRegion = 8;
TGraphAsymmErrors *gSF_DeadModule[nMC16];
TH2D *hTrigMETJetEff[nMETTrig];
TF1 *fSmearInt_ele[nSmearRegion];
TF1 *fSmearInt_mu[nSmearRegion];
const double g_par_Mean_ele[nSmearRegion]  = {-0.203310, -0.203310, -0.203310, -0.203310, -0.146075, -0.126751, -0.205570, -0.213854};
const double g_par_Sigma_ele[nSmearRegion] = {20.944675, 19.536572, 18.330746, 17.005663, 15.424842, 14.490877, 13.900709, 14.030027};
const double g_par_Alpha_ele[nSmearRegion] = { 1.858616,  1.858616,  1.858616,  1.858616,  1.822388,  1.655727,  1.544172,  1.637405};
const double g_par_Mean_mu[nSmearRegion]  = {-0.248458, -0.248458, -0.248458, -0.248458, -0.188993, -0.133858, -0.280374, 0.001933};
const double g_par_Sigma_mu[nSmearRegion] = {16.957926, 15.542328, 14.912544, 14.836049, 14.212644, 13.638716, 13.440928, 13.208069};
const double g_par_Alpha_mu[nSmearRegion] = { 1.716832,  1.716832,  1.716832,  1.716832,  1.663774,  1.618689,  1.682396,  1.644168};

const std::string UpDown[2] = {"__1up", "__1down"};
const int nSyst_JES = 7;
const int nSyst_JER = 8;
const int nSyst_JVT = 2;
const int nSyst_PRW = 1;
const int nSyst_TST = 4;
const std::string SystName_JES[nSyst_JES] = {"JET_EtaIntercalibration_NonClosure_highE",
					     "JET_EtaIntercalibration_NonClosure_negEta",
					     "JET_EtaIntercalibration_NonClosure_posEta",
					     "JET_Flavor_Response",
					     "JET_GroupedNP_1",
					     "JET_GroupedNP_2",
					     "JET_GroupedNP_3"};

const std::string SystName_JER[nSyst_JER] = {"JET_JER_DataVsMC_MC16",
					     "JET_JER_EffectiveNP_1",
					     "JET_JER_EffectiveNP_2",
					     "JET_JER_EffectiveNP_3",
					     "JET_JER_EffectiveNP_4",
					     "JET_JER_EffectiveNP_5",
					     "JET_JER_EffectiveNP_6",
					     "JET_JER_EffectiveNP_7restTerm"};
const std::string SystName_JVT[nSyst_JVT] = {"JET_JvtEfficiency",
					     "JET_fJvtEfficiency"};
const std::string SystName_PRW[nSyst_PRW] = {"PRW_DATASF"};
const std::string SystName_TST[nSyst_TST] = {"MET_SoftTrk_ResoPara",
					     "MET_SoftTrk_ResoPerp",
					     "MET_SoftTrk_ScaleDown",
					     "MET_SoftTrk_ScaleUp"};


class mySignalSyst{
 public:
  mySignalSyst(const std::string);
  ~mySignalSyst();
  double GetTrackWeight(int);

  int nEntry;
  //private:
  TFile *m_file;
  TTree *m_tree;
  TH1D *hnEventsProcessedBCK;
  TH1D *hnPrimaryVertex;
  TH1D *hProdType;
  TH1D *hSumOfNumber;
  TH1D *hSumOfWeights;
  TH1D *hSumOfWeightsBCK;
  TH1D *hSumOfWeightsSquaredBCK;

  float weightXsec;
  float weightMCweight;
  float weightPileupReweighting;

  float actualInteractionsPerCrossing;
  float averageInteractionsPerCrossing;
  float CorrectedAverageInteractionsPerCrossing;
  int treatAsYear;
  int prodType;
  UInt_t randomRunNumber;
  UInt_t RunNumber;
  UInt_t lumiBlock;
  ULong64_t EventNumber;
  int DetectorError;
  UInt_t nPrimaryVertex;
  float nTracksFromPrimaryVertex;

  TClonesArray *m_conventionalTracks;
  std::vector<myTrack *> *conventionalTracks;

  float METPt;
  float JetPt;
  bool  IsPassedKinematics_EWK;
  bool  IsPassedKinematics_Strong;

  float METPt_JES[2][nSyst_JES];
  float JetPt_JES[2][nSyst_JES];
  float weightPileupReweighting_JES[2][nSyst_JES];
  bool  IsPassedKinematics_EWK_JES[2][nSyst_JES];
  bool  IsPassedKinematics_Strong_JES[2][nSyst_JES];

  float METPt_JER[2][nSyst_JER];
  float JetPt_JER[2][nSyst_JER];
  float weightPileupReweighting_JER[2][nSyst_JER];
  bool  IsPassedKinematics_EWK_JER[2][nSyst_JER];
  bool  IsPassedKinematics_Strong_JER[2][nSyst_JER];
  
  float METPt_JVT[2][nSyst_JVT];
  float JetPt_JVT[2][nSyst_JVT];
  float weightPileupReweighting_JVT[2][nSyst_JVT];
  bool  IsPassedKinematics_EWK_JVT[2][nSyst_JVT];
  bool  IsPassedKinematics_Strong_JVT[2][nSyst_JVT];
  
  float METPt_PRW[2][nSyst_PRW];
  float JetPt_PRW[2][nSyst_PRW];
  float weightPileupReweighting_PRW[2][nSyst_PRW];
  bool  IsPassedKinematics_EWK_PRW[2][nSyst_PRW];
  bool  IsPassedKinematics_Strong_PRW[2][nSyst_PRW];
  
  float METPt_TST[nSyst_TST];
  float JetPt_TST[nSyst_TST];
  float weightPileupReweighting_TST[nSyst_TST];
  bool  IsPassedKinematics_EWK_TST[nSyst_TST];
  bool  IsPassedKinematics_Strong_TST[nSyst_TST];
};

mySignalSyst::mySignalSyst(const std::string FileName)
: nEntry(-1),m_file(NULL),m_tree(NULL),hnEventsProcessedBCK(NULL),hnPrimaryVertex(NULL),hProdType(NULL),hSumOfNumber(NULL),hSumOfWeights(NULL),hSumOfWeightsBCK(NULL),hSumOfWeightsSquaredBCK(NULL),weightXsec(0),weightMCweight(0),weightPileupReweighting(0),actualInteractionsPerCrossing(-1),averageInteractionsPerCrossing(-1),CorrectedAverageInteractionsPerCrossing(-1),treatAsYear(-1),prodType(-1),randomRunNumber(0),RunNumber(0),lumiBlock(0),EventNumber(-1),DetectorError(-1),nPrimaryVertex(0),nTracksFromPrimaryVertex(-1)
{

  conventionalTracks = new std::vector<myTrack *>;
  // open input file
  m_file = TFile::Open(FileName.c_str(), "READ");
  gROOT->cd();

  hnEventsProcessedBCK    = (TH1D *)m_file->Get("nEventsProcessedBCK");
  hnPrimaryVertex         = (TH1D *)m_file->Get("nPrimaryVertex");
  hProdType               = (TH1D *)m_file->Get("prodType");
  hSumOfNumber            = (TH1D *)m_file->Get("sumOfNumber");
  hSumOfWeights           = (TH1D *)m_file->Get("sumOfWeights");
  hSumOfWeightsBCK        = (TH1D *)m_file->Get("sumOfWeightsBCK");
  hSumOfWeightsSquaredBCK = (TH1D *)m_file->Get("sumOfWeightsSquaredBCK");

  m_tree = (TTree *)m_file->Get("tree");

  m_tree->SetBranchAddress("Tracks", &m_conventionalTracks);
  m_tree->SetBranchAddress("weightXsec"                , &weightXsec);
  m_tree->SetBranchAddress("weightMCweight"            , &weightMCweight);
  m_tree->SetBranchAddress("weightPileupReweighting"   , &weightPileupReweighting);

  m_tree->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing);
  m_tree->SetBranchAddress("CorrectedAverageInteractionsPerCrossing", &CorrectedAverageInteractionsPerCrossing);
  m_tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  m_tree->SetBranchAddress("treatAsYear", &treatAsYear);
  m_tree->SetBranchAddress("prodType", &prodType);
  m_tree->SetBranchAddress("randomRunNumber", &randomRunNumber);
  m_tree->SetBranchAddress("RunNumber", &RunNumber);
  m_tree->SetBranchAddress("lumiBlock", &lumiBlock);
  m_tree->SetBranchAddress("EventNumber", &EventNumber);
  m_tree->SetBranchAddress("DetectorError", &DetectorError);
  m_tree->SetBranchAddress("nPrimaryVertex", &nPrimaryVertex);
  m_tree->SetBranchAddress("nTracksFromPrimaryVertex", &nTracksFromPrimaryVertex);

  m_tree->SetBranchAddress("METPt", &METPt);
  m_tree->SetBranchAddress("JetPt", &JetPt);
  m_tree->SetBranchAddress("IsPassedKinematics_EWK", &IsPassedKinematics_EWK);
  m_tree->SetBranchAddress("IsPassedKinematics_Strong", &IsPassedKinematics_Strong);

  for(int iSys=0;iSys<nSyst_JES;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &METPt_JES[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_JES[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_JES[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_JES[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_JES[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_JES[iUp][iSys] );
    }
  }// for iSyst_JES

  for(int iSys=0;iSys<nSyst_JER;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &METPt_JER[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_JER[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_JER[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_JER[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_JER[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_JER[iUp][iSys] );
    }
  }// for iSyst_JER

  for(int iSys=0;iSys<nSyst_JVT;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &METPt_JVT[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_JVT[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_JVT[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_JVT[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_JVT[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_JVT[iUp][iSys] );
    }
  }// for iSyst_JVT

  for(int iSys=0;iSys<nSyst_PRW;iSys++){
    for(int iUp=0;iUp<2;iUp++){
      m_tree->SetBranchAddress(Form("METPt%s%s"                    , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &METPt_PRW[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("JetPt%s%s"                    , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &JetPt_PRW[iUp][iSys]                     );
      m_tree->SetBranchAddress(Form("weightPileupReweighting%s%s"  , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &weightPileupReweighting_PRW[iUp][iSys]   );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s%s"   , SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_EWK_PRW[iUp][iSys]    );
      m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s%s", SystName_PRW[iSys].c_str(), UpDown[iUp].c_str()), &IsPassedKinematics_Strong_PRW[iUp][iSys] );
    }
  }// for iSyst_PRW

  for(int iSys=0;iSys<nSyst_TST;iSys++){
    m_tree->SetBranchAddress(Form("METPt%s"                    , SystName_TST[iSys].c_str()), &METPt_TST[iSys]                     );
    m_tree->SetBranchAddress(Form("JetPt%s"                    , SystName_TST[iSys].c_str()), &JetPt_TST[iSys]                     );
    m_tree->SetBranchAddress(Form("weightPileupReweighting%s"  , SystName_TST[iSys].c_str()), &weightPileupReweighting_TST[iSys]   );
    m_tree->SetBranchAddress(Form("IsPassedKinematics_EWK%s"   , SystName_TST[iSys].c_str()), &IsPassedKinematics_EWK_TST[iSys]    );
    m_tree->SetBranchAddress(Form("IsPassedKinematics_Strong%s", SystName_TST[iSys].c_str()), &IsPassedKinematics_Strong_TST[iSys] );
  }// for iSyst_TST
  
  nEntry = m_tree->GetEntries();
  
  std::cout << "SignalSyst :: Input File : " << FileName << std::endl;
  std::cout << "SignalSyst :: Entries    : " << nEntry << std::endl;
}


mySignalSyst::~mySignalSyst(){
  delete hnEventsProcessedBCK; hnEventsProcessedBCK=NULL;
  delete hnPrimaryVertex;      hnPrimaryVertex=NULL;
  delete hProdType;            hProdType=NULL;
  delete hSumOfNumber;     hSumOfNumber=NULL;
  delete hSumOfWeights;    hSumOfWeights=NULL;
  delete hSumOfWeightsBCK; hSumOfWeightsBCK=NULL;
  delete hSumOfWeightsSquaredBCK; hSumOfWeightsSquaredBCK=NULL;
  
  delete m_tree;             m_tree=NULL;
  delete m_file; m_file=NULL;
}

double mySignalSyst::GetTrackWeight(int iMC16){
  double ret = 0.0;

  conventionalTracks->clear();  
  // Conventional Tracks (standard track + pixel-tracklet)
  if(m_conventionalTracks!=NULL)
    for(int i=0;i<(m_conventionalTracks->GetEntries());i++){
      conventionalTracks->push_back((myTrack *)m_conventionalTracks->ConstructedAt(i));    
    }// for conventional tracks
  
  int IsoTrackID = -1.0;
  int IsoTrackPt =  0.0;
  int IsoTrackletID = -1.0;
  int IsoTrackletPt =  0.0;

  /*   Calculate Isolated Highest Track/Tracklet Pt   */
  for(unsigned int iTrack=0;iTrack<(conventionalTracks->size());iTrack++){
    if(conventionalTracks->at(iTrack)->PixelTracklet){
      if( ((conventionalTracks->at(iTrack)->ptcone40overPt_1gev) < 0.04 ) &&
	  (IsoTrackletPt < (conventionalTracks->at(iTrack)->p4.Pt())) ){
	IsoTrackletPt = conventionalTracks->at(iTrack)->p4.Pt();
	IsoTrackletID = iTrack;
      }
    }else{
      if( ((conventionalTracks->at(iTrack)->ptcone40overPt_1gev) < 0.04 ) &&
	  (IsoTrackPt < (conventionalTracks->at(iTrack)->p4.Pt())) ){
	IsoTrackPt = conventionalTracks->at(iTrack)->p4.Pt();
	IsoTrackID = iTrack;
      }
    }// standard track
  }// for iTrack
      
  /*     Leading    */
  for(unsigned int iTrack=0;iTrack<(conventionalTracks->size());iTrack++){
    if(conventionalTracks->at(iTrack)->PixelTracklet){
      if((int)iTrack == IsoTrackletID)
	conventionalTracks->at(iTrack)->IsPassedIsolatedLeading = true;
    }else{
      if((int)iTrack == IsoTrackID   )
	conventionalTracks->at(iTrack)->IsPassedIsolatedLeading = true;
    }

    int nTrackLayer  = MyUtil::CalcNumberOfBarrelOnlyLayer(conventionalTracks->at(iTrack));
    if((nTrackLayer==4)
       && (conventionalTracks->at(iTrack)->nSCTHits==0)
       && (conventionalTracks->at(iTrack)->numberOfContribPixelLayers >= 4)
       && (conventionalTracks->at(iTrack)->numberOfGangedFlaggedFakes==0)
       && (conventionalTracks->at(iTrack)->nSiHoles==0)
       && (conventionalTracks->at(iTrack)->numberOfPixelSpoiltHits==0)
       && (conventionalTracks->at(iTrack)->nPixelOutliers==0)){

	    
      double trackEta = conventionalTracks->at(iTrack)->p4.Eta();
      double DeadWeight = gSF_DeadModule[iMC16]->Eval(trackEta);
      double truthPt = conventionalTracks->at(iTrack)->TruthPt/1000.0;
      //if(truthPt>0.0)
      //ret += DeadWeight;
      //continue;

      double weightPt20=0.0;
      double weightPt60=0.0;
      hTrackPt->Reset();
      if(truthPt > 0.0){
	SmearPt(hTrackPt, truthPt, 1.0, false);
	weightPt20 = hTrackPt->Integral(1, 12);
	weightPt60 = hTrackPt->Integral(5, 12);
      }else{
	weightPt20 = 0.0;
	weightPt60 = 0.0;
      }

      if((TMath::Abs(conventionalTracks->at(iTrack)->d0sigTool)       < 1.5) &&
	 (TMath::Abs(conventionalTracks->at(iTrack)->z0sinthetawrtPV) < 0.5) &&
	 (conventionalTracks->at(iTrack)->Quality > 0.1)){
	    
	if((conventionalTracks->at(iTrack)->ptcone40overPt_1gev < 0.04) &&
	   (conventionalTracks->at(iTrack)->IsPassedIsolatedLeading==true) &&
	   (conventionalTracks->at(iTrack)->dRJet50 > 0.4) &&
	   (conventionalTracks->at(iTrack)->dRElectron > 0.4) &&
	   (conventionalTracks->at(iTrack)->dRMuon > 0.4) &&
	   (conventionalTracks->at(iTrack)->dRMSTrack > 0.4)){
	      
	  if((TMath::Abs(conventionalTracks->at(iTrack)->p4.Eta()) > 0.1) &&
	     (TMath::Abs(conventionalTracks->at(iTrack)->p4.Eta()) < 1.9)){
		
	    if(conventionalTracks->at(iTrack)->etclus20_topo/1000.0 < 5.0){

	      //ret += DeadWeight*weightPt60;
	      ret += DeadWeight*weightPt20;
	    }// Caloveto
	  }// Geometry
	}// Isolation
      }// Quality	  
    }// fTracklet	
  }// for iTrack

  return ret;
}

class mySystVal{
 public:
  mySystVal(){};
  ~mySystVal(){};
  int CalcFinalVal();

  double Val_Nominal=0.0;
  double Val_Syst_JES[2][nSyst_JES]={{0},{0}};
  double Val_Syst_JER[2][nSyst_JER]={{0},{0}};
  double Val_Syst_JVT[2][nSyst_JVT]={{0},{0}};
  double Val_Syst_PRW[2][nSyst_PRW]={{0},{0}};
  double Val_Syst_TST[nSyst_TST]={0};

  double FinalVal_Syst_JES[2]={0};
  double FinalVal_Syst_JER[2]={0};
  double FinalVal_Syst_JVT[2]={0};
  double FinalVal_Syst_PRW[2]={0};
  double FinalVal_Syst_TST[2]={0};
};

int mySystVal::CalcFinalVal(void){
  double vUp=0.0;
  double vDown=0.0;
  
  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_JES[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for JES
  FinalVal_Syst_JES[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_JES[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_JER[iUp][iSyst]/Val_Nominal - 1.0);
      
      //if(Val>0){
	vUp += Val*Val;
	//}else{
	//vDown += Val*Val;
	//}
    }// for iUp
  }// for JER
  FinalVal_Syst_JER[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_JER[1] = -TMath::Sqrt(vUp);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_JVT[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for JVT
  FinalVal_Syst_JVT[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_JVT[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
    for(int iUp=0;iUp<2;iUp++){
      double Val = 100.0*(Val_Syst_PRW[iUp][iSyst]/Val_Nominal - 1.0);
      if(Val>0){
	vUp += Val*Val;
      }else{
	vDown += Val*Val;
      }
    }// for iUp
  }// for PRW
  FinalVal_Syst_PRW[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_PRW[1] = -TMath::Sqrt(vDown);

  vUp=0.0; vDown=0.0;
  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
    double Val = 100.0*(Val_Syst_TST[iSyst]/Val_Nominal - 1.0);
    if(Val>0){
      vUp += Val*Val;
    }else{
      vDown += Val*Val;
    }
  }// for TST
  FinalVal_Syst_TST[0] = +TMath::Sqrt(vUp);
  FinalVal_Syst_TST[1] = -TMath::Sqrt(vDown);

  return 0;
}

void SmearPt(TH1D *hHoge, double pt, double weight, bool fMuonFlag){
 for(int i=0;i<nBinsMySignal;i++){
    double ptLow = XBinsMySignal[i];
    double ptUp  = XBinsMySignal[i+1];
    if(i==(nBinsMySignal-1)){
      ptUp = 12500.0;
    }

    int iFunc=-1;
    if(pt < 15.0){
      iFunc = 0;
    }else if(pt < 20.0){
      iFunc = 1;
    }else if(pt < 25.0){
      iFunc = 2;
    }else if(pt < 35.0){
      iFunc = 3;
    }else if(pt < 45.0){
      iFunc = 4;
    }else if(pt < 60.0){
      iFunc = 5;
    }else if(pt < 100.0){
      iFunc = 6;
    }else{
      iFunc = 7;
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(fMuonFlag){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(fMuonFlag){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 

#endif

