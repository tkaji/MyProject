#include "mySignal.h"
namespace po = boost::program_options;

MySignal<3, 4> *MyEWK[nTau_EWK];
MySignal<3, 4> *MyHiggsino[nTau_Higgsino];
MySignal<3, 4> *MyStrong[nTau_Strong];
MyManager *myData;
HistManager *myHistData;

double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau);
TCut GetKinematicsCut(int iMETRegion, int iMETType);
double crystallBallIntegral(double* x, double *par);
double GetTrigEff(NtupleReader *myReader);
void SmearPt(TH1D *hHoge, double pt, double weight, bool f2Flag);
int FillDataYield(void);

std::ofstream ofs;
std::string PDFName;
int iChain;
int EWKOff;
bool ReadData=false;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("iChain", po::value<int>()->default_value(0), "i Chain (0 ~ 19)")
    ("EWKOff", po::value<int>()->default_value(0), "0: ON, 1: OFF")
    ("SmearTruthPt", po::value<bool>()->default_value(0), "0: ON, 1: OFF")
    ("output-file,o", po::value<std::string>()->default_value("mySignal"), "output file");
  
  po::positional_options_description p;

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  iChain = vm["iChain"].as<int>();
  EWKOff = vm["EWKOff"].as<int>();

  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    gSF_DeadModule[iMC16] = (TGraphAsymmErrors *)tfInput->Get(Form("SF_DeadModule_%s", MCName[iMC16].c_str()));
  }
  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");
  
  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");

  if(ReadData){
    myData = new MyManager("/data3/tkaji/dev_MyProject/files/NewSignal/data15-18_13TeV.root", "Data 2015-2018");
    myData->LoadMyFile();
    myHistData = myData->myHist_Old4L;
    FillDataYield();
  }

  for(int iTau=0;iTau<nTau_EWK;iTau++){
    MyEWK[iTau] = new MySignal<nSignal_EWK[iTau], nReweight_EWK[iTau]>("EWK", Tau_EWK[iTau]);
    MyEWK[iTau]->FillPtHist();
    MyEWK[iTau]->FillYield();
  }// for iTau

  for(int iTau=0;iTau<nTau_Higgsino;iTau++){
    MyHiggsino[iTau] = new MySignal<nSignal_Higgsino[iTau], nReweight_Higgsino[iTau]>("Higgsino", Tau_Higgsino[iTau]);
    MyHiggsino[iTau]->FillPtHist();
    MyHiggsino[iTau]->FillYield();
  }// for iTau

  for(int iTau=0;iTau<nTau_Strong;iTau++){
    MyStrong[iTau] = new MySignal<nSignal_Strong[iTau], nReweight_Strong[iTau]>("Strong", Tau_Strong[iTau]);
    MyStrong[iTau]->FillPtHist();
    MyStrong[iTau]->FillYield();
  }// for iTau

  // EWK
  ofs.open(Form("List_EWK_Chain%d.txt", iChain));
  for(int iTau=0;iTau<nTau_EWK;iTau++){
    double Uncert = 0.14;
    for(int iSignal=0;iSignal<(MyEWK[iTau]->nSignal);iSignal++){
      double Val0 = MyEWK[iTau]->hPt[iSignal][0]->Integral(iPtBin+1, nBinsMySignal+1);
      ofs << std::setw(10) << Form("%.1lf", MyEWK[iTau]->CMass.at(iSignal).at(0)) << std::setw(10) << Form("%.1lf", MyEWK[iTau]->TauVal)
	  << std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;

      for(int iFrom=0;iFrom<(MyEWK[iTau]->nReweight);iFrom++){
	double Val1 = MyEWK[iTau]->hPt_Reweighted[iSignal][0]->at(iFrom)->Integral(iPtBin+1, nBinsMySignal+1);
	ofs << std::setw(10) << Form("%.1lf", MyEWK[iTau]->CMass.at(iSignal).at(0)) << std::setw(10) << Form("%.1lf", MyEWK[iTau]->TauReweight[iFrom])
	    << std::setw(20) << Form("%.4lf", Val1) << std::setw(20) << Form("%.4lf", Val1*Uncert) << std::endl;
      }// for iFrom
    }// for iSignal
  }//for iTau
  ofs.close();

  // Higgsino
  ofs.open(Form("List_Higgsino_Chain%d.txt", iChain));
  for(int iTau=0;iTau<nTau_Higgsino;iTau++){
    double Uncert = 0.14;
    for(int iSignal=0;iSignal<(MyHiggsino[iTau]->nSignal);iSignal++){
      double Val0 = MyHiggsino[iTau]->hPt[iSignal][0]->Integral(iPtBin+1, nBinsMySignal+1);
      ofs << std::setw(10) << Form("%.1lf", MyHiggsino[iTau]->CMass.at(iSignal).at(0)) << std::setw(10) << Form("%.1lf", MyHiggsino[iTau]->TauVal)
	  << std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;

      for(int iFrom=0;iFrom<(MyHiggsino[iTau]->nReweight);iFrom++){
	double Val1 = MyHiggsino[iTau]->hPt_Reweighted[iSignal][0]->at(iFrom)->Integral(iPtBin+1, nBinsMySignal+1);
	ofs << std::setw(10) << Form("%.1lf", MyHiggsino[iTau]->CMass.at(iSignal).at(0)) << std::setw(10) << Form("%.1lf", MyHiggsino[iTau]->TauReweight[iFrom])
	    << std::setw(20) << Form("%.4lf", Val1) << std::setw(20) << Form("%.4lf", Val1*Uncert) << std::endl;
      }// for iFrom
    }// for iSignal
  }//for iTau
  ofs.close();

  // Strong
  ofs.open(From("List_Strong1p0_Chain%d.txt", iChain));
  for(int iSignal=0;iSignal<(MyStrong[1]->nSignal);iSignal++){
    double Uncert = 0.16;
    double Val0 = MyStrong[1]->hPt[iSignal][0]->Integral(iPtBin+1, nBinsMySignal+1);
    ofs << std::setw(10) << Form("%.1lf", MyStrong[1]->GMass.at(iSignal).at(0)) << std::setw(10) << Form("%.1lf", MyStrong[1]->CMass.at(iSignal).at(0))
	<< std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;
  }//for iSignal
  ofs.close();
  ofs.open(From("List_Strong0p2_Chain%d.txt", iChain));
  for(int iSignal=0;iSignal<(MyStrong[1]->nSignal);iSignal++){
    double Uncert = 0.16;
    double Val0 = MyStrong[1]->hPt_Reweighted[iSignal][0]->at(0)->Integral(iPtBin+1, nBinsMySignal+1);
    ofs << std::setw(10) << Form("%.1lf", MyStrong[1]->GMass.at(iSignal).at(0)) << std::setw(10) << Form("%.1lf", MyStrong[1]->CMass.at(iSignal).at(0))
	<< std::setw(20) << Form("%.4lf", Val0) << std::setw(20) << Form("%.4lf", Val0*Uncert) << std::endl;
  }//for iSignal
  ofs.close();

  std::cout << "mySignal : succeeded to run " << std::endl;

  return 0;
}

template<int Val3, int Val4> int MySignal<Val3, Val4>::FillYield(void){
  TH1D *hCount = new TH1D("hCount", "", 1, 0, 10);
  for(int iSignal=0;iSignal<nSignal;iSignal++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      nSR_CaloSideBand[iSignal][iMET] = 0.0;
      nHadCR[iSignal][iMET]       = 0.0;
      nFakeCR[iSignal][iMET]      = 0.0;
      nSingleEleCR[iSignal][iMET] = 0.0;
      nSingleMuCR[iSignal][iMET]  = 0.0;

      nSR_LowPt[iSignal][iMET]  = hPt[iSignal][iMET]->Integral(1, iPtBin);
      nSR_HighPt[iSignal][iMET] = hPt[iSignal][iMET]->Integral(iPtBin+1, nBinsMySignal+1);
      for(int iPt=0;iPt<nPtScan;iPt++){
	nSR_PtScan[iPt][iSignal][iMET] = hPt[iSignal][iMET]->Integral(iPtBin+1+iPt, nBinsMySignal+1);
      }

      for(unsigned int iDSID=0;iDSID<vDSID.at(iSignal).size();iDSID++){
	for(int iMC16=0;iMC16<nRunMC16;iMC16++){	  
	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 0) && CaloVETO_1)*MCWeight);
	  nSR_CaloSideBand[iSignal][iMET] += hCount->Integral();

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_hadCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 0))*MCWeight);
	  nHadCR[iSignal][iMET] += hCount->Integral();

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_fakeCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 0) && FakeD0_CR && CaloVETO_0)*MCWeight);
	  nFakeCR[iSignal][iMET] += hCount->Integral();

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_singleEleCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 1))*MCWeight);
	  nSingleEleCR[iSignal][iMET] += hCount->Integral();

	  hCount->Reset();
	  GetReader(iMC16, iSignal, iDSID, "Old4L_Common_singleMuCR")->m_tree->Draw("1>>hCount", (GetKinematicsCut(iMET, 2) && CaloVETO_0)*MCWeight);
	  nSingleEleCR[iSignal][iMET] += hCount->Integral();
	}// for iMC16
      }// for iDSID
    }// for iMET
  }// for iSignal

  return 0;
}

template<int Val3, int Val4> int MySignal<Val3, Val4>::FillPtHist(void){
  if(EWKOff==1)
    return 0;
  for(int iSignal=0;iSignal<nSignal;iSignal++){
    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      for(int iMET=0;iMET<nMETRegion;iMET++){
	for(unsigned int iDSID=0;iDSID<vDSID.at(iSignal).size();iDSID++){
	  eventList->Reset();
	  if(Type=="Higgsino" && (Tau=="0p2" || Tau=="1p0" || Tau=="10p0") && (iSignal==2 || iSignal==4) && iDSID==0){
	    
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0 && "prodType==0");
	  }else if(Type=="Higgsino" && (Tau=="0p2" || Tau=="1p0" || Tau=="10p0") && (iSignal==2 || iSignal==4) && iDSID==1){
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0 && "prodType==1");
	  }else{
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(iMET, 0) && CaloVETO_0);
	  }

	  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	    GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	    if(fUseTriggerBit && GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->LU_METtrigger_SUSYTools==false)
	      continue;
	  
	    double weightcommon = 1.0;
	    double weight[nReweight];
	    for(int iFrom=0;iFrom<nReweight;iFrom++){
	      weight[iFrom] = 1.0;
	    }
	    for(unsigned int iTruth=0;iTruth<(GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->truthParticles->size());iTruth++){
	      int PDGID = (int)GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	      if(TMath::Abs(PDGID)!=1000024)
		continue;
	      float ProperTime = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	      for(int iFrom=0;iFrom<nReweight;iFrom++){
		weight[iFrom] *= GetWeightLifetime(ProperTime, TauVal, TauReweight[iFrom]);
	      }
	    }// for iTruth
	    
	    if(fUseTriggerBit==false)
	      weightcommon *= GetTrigEff(GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR"));
	    if(fDeadModule==true){
	      double trackEta = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->Tracks->at(0)->p4.Eta();
	      weightcommon *= gSF_DeadModule[iMC16]->Eval(trackEta);
	    }
	    weightcommon *= GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->weightXsec;
	    weightcommon *= GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->weightMCweight;
	    weightcommon *= GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->weightPileupReweighting;
	    if(fSmearTruthPt){
	      double truthPt = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->Tracks->at(0)->TruthPt/1000.0;
	      hTMP->Reset();
	      if(truthPt > 0.0)
		SmearPt(hTMP, truthPt, weightcommon*SF[iMC16][iSignal].at(iDSID), true);
	      else
		hTMP->Fill(-1, weightcommon*SF[iMC16][iSignal].at(iDSID));
	      hPt[iSignal][iMET]->Add(hTMP, 1.0);
	      for(int iFrom=0;iFrom<nReweight;iFrom++){
		hPt_Reweighted[iSignal][iMET]->at(iFrom)->Add(hTMP, weight[iFrom]);
	      }
	    }else{
	      double trackPt = GetReader(iMC16, iSignal, iDSID, "Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	      hPt[iSignal][iMET]->Fill(trackPt, weightcommon*SF[iMC16][iSignal].at(iDSID));
	      for(int iFrom=0;iFrom<nReweight;iFrom++){
		hPt_Reweighted[iSignal][iMET]->at(iFrom)->Fill(trackPt, weightcommon*weight[iFrom]*SF[iMC16][iSignal].at(iDSID));	    
	      }
	    }
	  }// for iEntry
	}
      }// for iDSID
    }// for iSignal
  }// for iMC16

  return 0;
}

double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau){
  if(TMath::Abs(PropTime) > 1e10)
    return 1.0;
  else
    return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
}

TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==2){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 20.0)";
  return ret;
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

double GetTrigEff(NtupleReader *myReader){
  if(iChain==0) return myReader->TrigEff_Chain0;
  else if(iChain==1) return myReader->TrigEff_Chain1;
  else if(iChain==2) return myReader->TrigEff_Chain2;
  else if(iChain==3) return myReader->TrigEff_Chain3;
  else if(iChain==4) return myReader->TrigEff_Chain4;
  else if(iChain==5) return myReader->TrigEff_Chain5;
  else if(iChain==6) return myReader->TrigEff_Chain6;
  else return 0.0;
}


void SmearPt(TH1D *hHoge, double pt, double weight, bool f2Flag){
 for(int i=0;i<nBinsMySignal;i++){
    double ptLow = XBinsMySignal[i];
    double ptUp  = XBinsMySignal[i+1];
    if(i==(nBinsMySignal-1)){
      ptUp = 12500.0;
    }

    int iFunc=-1;
    if(f2Flag){
      if(pt < 15.0){
	iFunc = 0;
      }else if(pt < 20.0){
	iFunc = 1;
      }else if(pt < 25.0){
	iFunc = 2;
      }else if(pt < 35.0){
	iFunc = 3;
      }else if(pt < 45.0){
	iFunc = 4;
      }else if(pt < 60.0){
	iFunc = 5;
      }else if(pt < 100.0){
	iFunc = 6;
      }else{
	iFunc = 7;
      }
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(f2Flag==false){
      w1Low = fSmearInt->Eval(qoverptLow);
      w1Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(f2Flag){
      w2Low = fSmearInt->Eval(qoverptLow);
      w2Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 

int FillDataYield(void){
  TCut LowPtCut  = "(DisappearingTracks.p4.Pt()/1000.0 > 20.0 && DisappearingTracks.p4.Pt()/1000.0 < 60.0)";
  TCut HighPtCut = "(DisappearingTracks.p4.Pt()/1000.0 > 60.0)";
  for(int iMET=0;iMET<nMETRegion;iMET++){
    nData_SR_LowPt[iMET]  = myData->GetNtupleReader("Old4L_Common_SR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && CaloVETO_0 && LowPtCut);
    if(iMET==0)
      nData_SR_HighPt[iMET] = -1.0;
    else
      nData_SR_HighPt[iMET] = myData->GetNtupleReader("Old4L_Common_SR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && CaloVETO_0 && HighPtCut);

    nData_SR_CaloSideBand[iMET] = myData->GetNtupleReader("Old4L_Common_SR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && CaloVETO_1);
    nData_HadCR[iMET]           = myData->GetNtupleReader("Old4L_Common_hadCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0));
    nData_FakeCR[iMET]          = myData->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 0) && FakeD0_CR && CaloVETO_0);
    nData_SingleEleCR[iMET]     = myData->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 1));
    nData_SingleMuCR[iMET]      = myData->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->GetEntries(GetKinematicsCut(iMET, 2) && CaloVETO_0);
  }// for iMET
  return 0;
}
