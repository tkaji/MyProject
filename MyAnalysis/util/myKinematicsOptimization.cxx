#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

const int nTotalCut = 3360;
const int nMETCut = 4;
const int nJet1Cut = 5;
const int nJet2Cut = 7;
const int nJet3Cut = 6;
const int nJet4Cut = 4;

const double METCut[nMETCut]   = {150.0, 200.0, 250.0, 300.0};
const double Jet1Cut[nJet1Cut] = {100.0, 150.0, 200.0, 300.0, 400.0};
const double Jet2Cut[nJet2Cut] = { -1.0,  20.0,  50.0, 100.0, 150.0, 200.0, 300.0};
const double Jet3Cut[nJet3Cut] = { -1.0,  20.0,  50.0, 100.0, 150.0, 200.0};
const double Jet4Cut[nJet4Cut] = { -1.0,  20.0,  50.0, 100.0};

int Plot0(std::vector<std::string> InFileList, std::string OutName, int fSignal);
int Plot1(std::string OutName);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("signal", po::value<int>()->default_value(0), "signal")
    ("plot", po::value<int>()->default_value(0), "plot")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("myKinematicsOptimization"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  std::vector<std::string> ReadableInputFileList;
  if(vm["grid"].as<int>()==0){
    ReadableInputFileList = InputFileList;
  }else{
    std::string CommaList = InputFileList.at(0);
    while(true){
      std::string::size_type pos = CommaList.find(',');
      if(pos != std::string::npos){
	ReadableInputFileList.push_back(CommaList.substr(0, pos));
	CommaList.erase(0, pos+1);
      }else{
	ReadableInputFileList.push_back(CommaList);
	break;
      }
    }
  }
  std::cout << "Input : " << ReadableInputFileList.at(0) << std::endl;

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");

  int fPlot = vm["plot"].as<int>();
  
  if(fPlot==0){
    Plot0(ReadableInputFileList, vm["output-file"].as<std::string>(), vm["signal"].as<int>());
    std::cout << "myKinematicsOptimization succeeded to run (Plot0)" << std::endl;
  }else if(fPlot==1){
    Plot1(vm["output-file"].as<std::string>());
    std::cout << "myKinematicsOptimization succeeded to run (Plot1)" << std::endl;
  }

  return 0;
}

int Plot0(std::vector<std::string> InFileList, std::string OutName, int fSignal){
  NtupleReader *myReader=NULL;

  TH1D *hnEventsProcessedBCK;
  TH1D *hSumOfNumber;
  TH1D *hSumOfWeights;
  TH1D *hSumOfWeightsBCK;
  TH1D *hSumOfWeightsSquaredBCK;

  TH1D *hMET;
  TH1D *hJet1;
  TH1D *hJet2;
  TH1D *hJet3;
  TH1D *hJet4;
  TH1D *hdPhiMin;
  TH1D *hSummary;

  hnEventsProcessedBCK    = new TH1D("hnEventsProcessedBCK"   , "", 1, 0, 2);
  hSumOfNumber            = new TH1D("hSumOfNumber"           , "", 1, 0, 2);
  hSumOfWeights           = new TH1D("hSumOfWeights"          , "", 1, 0, 2);
  hSumOfWeightsBCK        = new TH1D("hSumOfWeightsBCK"       , "", 1, 0, 2);
  hSumOfWeightsSquaredBCK = new TH1D("hSumOfWeightsSquaredBCK", "", 1, 0, 2);

  hMET     = new TH1D("hMET"    , ";MET [GeV]", 100, 0, 1000);
  hJet1    = new TH1D("hJet1"   , ";1st jet p_{T} [GeV]", 100, 0, 1000);
  hJet2    = new TH1D("hJet2"   , ";2nd jet p_{T} [GeV]", 100, 0, 1000);
  hJet3    = new TH1D("hJet3"   , ";3rd jet p_{T} [GeV]", 100, 0, 1000);
  hJet4    = new TH1D("hJet4"   , ";4th jet p_{T} [GeV]", 100, 0, 1000);
  hdPhiMin = new TH1D("hdPhiMin", ";#Delta#phi_{min}", 20, 0, TMath::Pi());

  hSummary = new TH1D("hSummary", ";Category", nTotalCut, 0, nTotalCut);

  int nFile = InFileList.size();
  for(int iFile=0;iFile<nFile;iFile++){
    myReader = new NtupleReader(InFileList.at(iFile));
    hnEventsProcessedBCK->Add(myReader->hnEventsProcessedBCK, 1);
    hSumOfNumber->Add(myReader->hSumOfNumber, 1);
    hSumOfWeights->Add(myReader->hSumOfWeights, 1);
    hSumOfWeightsBCK->Add(myReader->hSumOfWeightsBCK, 1);
    hSumOfWeightsSquaredBCK->Add(myReader->hSumOfWeightsSquaredBCK, 1);
    
    for(int iEntry=0;iEntry<(myReader->nEntry);iEntry++){
      myReader->GetEntry(iEntry);
      if(fSignal==1)
	myReader->weightXsec=1.0;
      if(myReader->IsData==true && (myReader->GRL==false || myReader->DetectorError!=0))
	continue;
      if(myReader->IsPassedBadEventVeto==false || myReader->LU_METtrigger_SUSYTools==false || myReader->IsPassedLeptonVeto==false)
	continue;
      double MET    = myReader->missingET->p4.Pt()/1000.0;
      double Jet1 = (myReader->goodJets->size()>0 ? myReader->goodJets->at(0)->p4.Pt()/1000.0 : 0.0);
      double Jet2 = (myReader->goodJets->size()>1 ? myReader->goodJets->at(1)->p4.Pt()/1000.0 : 0.0);
      double Jet3 = (myReader->goodJets->size()>2 ? myReader->goodJets->at(2)->p4.Pt()/1000.0 : 0.0);
      double Jet4 = (myReader->goodJets->size()>3 ? myReader->goodJets->at(3)->p4.Pt()/1000.0 : 0.0);
      double dPhiMin = myReader->JetMetdPhiMin50;
      double weight = (myReader->IsData) ? 1.0 : ((myReader->weightXsec) * (myReader->weightMCweight) * (myReader->weightPileupReweighting));

      if(MET > METCut[0] && Jet1 > Jet1Cut[0]){
	hdPhiMin->Fill(dPhiMin, weight);
      }
      if(dPhiMin > 0.4 && Jet1 > Jet1Cut[0]){
	hMET->Fill(MET, weight);
      }
      if(dPhiMin > 0.4 && MET > METCut[0]){
	hJet1->Fill(Jet1, weight);
      }
      if(MET > METCut[0] && Jet1 > Jet1Cut[0] && dPhiMin > 0.4){
	hJet2->Fill(Jet2, weight);
	hJet3->Fill(Jet3, weight);
	hJet4->Fill(Jet4, weight);
      }
      if(MET < METCut[0] || Jet1 < Jet1Cut[0] || dPhiMin < 0.4)
	continue;

      for(int iMET=0;iMET<nMETCut;iMET++){
	for(int iJet1=0;iJet1<nJet1Cut;iJet1++){
	  for(int iJet2=0;iJet2<nJet2Cut;iJet2++){
	    for(int iJet3=0;iJet3<nJet3Cut;iJet3++){
	      for(int iJet4=0;iJet4<nJet4Cut;iJet4++){
		if( (MET  > METCut[iMET]  ) && (dPhiMin > 0.4           ) && 
		    (Jet1 > Jet1Cut[iJet1]) && (Jet2    > Jet2Cut[iJet2]) && 
		    (Jet3 > Jet3Cut[iJet3]) && (Jet4    > Jet4Cut[iJet4]) ){
		  hSummary->Fill(iJet4 + iJet3*nJet4Cut + iJet2*nJet3Cut*nJet4Cut + 
				 iJet1*nJet2Cut*nJet3Cut*nJet4Cut + iMET*nJet1Cut*nJet2Cut*nJet3Cut*nJet4Cut, weight);
		}
	      }// for iJet4
	    }// for iJet3
	  }// for iJet2
	}// for iJet1
      }// for iMET
    }// for iEntry

    delete myReader; myReader=NULL;
  }// for iFile

  TFile *tf = new TFile(OutName.c_str(), "RECREATE");
  hnEventsProcessedBCK->Write();
  hSumOfNumber->Write();
  hSumOfWeights->Write();
  hSumOfWeightsBCK->Write();
  hSumOfWeightsSquaredBCK->Write();

  hMET->Write();
  hJet1->Write();
  hJet2->Write();
  hJet3->Write();
  hJet4->Write();
  hdPhiMin->Write();

  hSummary->Write();

  tf->Close();

  return 0;
}

int Plot1(std::string OutName){
  const int nMC16 = 3;
  const int nRunMC16 = 3;
  const int nBenchmark = 10;
  const int GMass[nBenchmark] = {  1800,   1800,   1800,   1800,   1800,    1800,   1800,   1800,   1800,   1800};
  const int CMass[nBenchmark] = {   100,    300,    500,    700,    900,    1100,   1300,   1500,   1700,   1750};
  const int DSID[nBenchmark]  = {448370, 448565, 448566, 448371, 448567,  448568, 448569, 448570, 448571, 448372};
  const double nExpSignal[nBenchmark] = { 16.2404, 21.0938, 15.1953, 9.98002, 6.76211, 4.36753, 2.63993, 1.87273, 0.415152, 0.356926};
  const double nExpData = 3.5718961;
  const double nNominalSignal[nBenchmark] = { 253.2, 268.5, 273.2, 276.4, 279.4, 273.6, 263.5, 222.4, 60.0, 29.5};
  const double nNominalData = 2.01856e+06;
  const double nCommonExpSignal = 5.41;

  TCanvas *c0;
  TH1D *hSummary_Data;
  TH1D *hSummary_Signal[nBenchmark];
  TH1D *hSummary_Signal_Sig[nBenchmark];
  TH1D *hSummary_Signal_SigCommon[nBenchmark];
  
  TH1D *hMET_Data;
  TH1D *hJet1_Data;
  TH1D *hJet2_Data;
  TH1D *hJet3_Data;
  TH1D *hJet4_Data;
  TH1D *hdPhiMin_Data;
  TH1D *hMET_Signal[nBenchmark];
  TH1D *hJet1_Signal[nBenchmark];
  TH1D *hJet2_Signal[nBenchmark];
  TH1D *hJet3_Signal[nBenchmark];
  TH1D *hJet4_Signal[nBenchmark];
  TH1D *hdPhiMin_Signal[nBenchmark];

  TGraph *gSigCommon[nBenchmark];

  const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
  const double SumLumi = 3219.56 + 32995.4 + 43587.3 + 58450.1 - 1141.73 - 3030.55 - 881.025; // pb-1
  const double IntLumi[3]={3219.56 + 32995.4 - 1141.73,
			     43587.3 - 3030.55,
			     58450.1 - 881.025}; // pb-1

  TFile *tfData = new TFile("/data3/tkaji/dev_MyProject/runKinematicsOptimization/out.KO.data.root", "READ");
  gROOT->cd();
  hSummary_Data = (TH1D *)tfData->Get("hSummary");
  hSummary_Data->SetName("hSummary_Data");
  hMET_Data = (TH1D *)tfData->Get("hMET");    hMET_Data->SetName("hMET_Data");
  hJet1_Data = (TH1D *)tfData->Get("hJet1");  hJet1_Data->SetName("hJet1_Data");
  hJet2_Data = (TH1D *)tfData->Get("hJet2");  hJet2_Data->SetName("hJet2_Data");
  hJet3_Data = (TH1D *)tfData->Get("hJet3");  hJet3_Data->SetName("hJet3_Data");
  hJet4_Data = (TH1D *)tfData->Get("hJet4");  hJet4_Data->SetName("hJet4_Data");
  hdPhiMin_Data = (TH1D *)tfData->Get("hdPhiMin");  hdPhiMin_Data->SetName("hdPhiMin_Data");

  hMET_Data->AddBinContent(1, hMET_Data->GetBinContent(0));          hMET_Data->AddBinContent(100, hMET_Data->GetBinContent(101));
  hJet1_Data->AddBinContent(1, hJet1_Data->GetBinContent(0));        hJet1_Data->AddBinContent(100, hJet1_Data->GetBinContent(101));
  hJet2_Data->AddBinContent(1, hJet2_Data->GetBinContent(0));        hJet2_Data->AddBinContent(100, hJet2_Data->GetBinContent(101));
  hJet3_Data->AddBinContent(1, hJet3_Data->GetBinContent(0));        hJet3_Data->AddBinContent(100, hJet3_Data->GetBinContent(101));
  hJet4_Data->AddBinContent(1, hJet4_Data->GetBinContent(0));        hJet4_Data->AddBinContent(100, hJet4_Data->GetBinContent(101));
  hdPhiMin_Data->AddBinContent(1, hdPhiMin_Data->GetBinContent(0));  hdPhiMin_Data->AddBinContent(100, hdPhiMin_Data->GetBinContent(101));

  TFile *tfXS = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/gXS.root", "READ");
  gROOT->cd();
  TGraphErrors *gXS_Strong   = (TGraphErrors *)tfXS->Get("gXS_Strong");

  for(int i=0;i<nBenchmark;i++){
    gSigCommon[i] = new TGraph();
    hSummary_Signal[i] = new TH1D(Form("hSummary_Signal_%d", i), ";Category", nTotalCut, 0, nTotalCut);
    hSummary_Signal_Sig[i] = new TH1D(Form("hSummary_Signal_Sig_%d", i), ";Category", nTotalCut, 0, nTotalCut);
    hSummary_Signal_SigCommon[i] = new TH1D(Form("hSummary_Signal_SigCommon_%d", i), ";Category", nTotalCut, 0, nTotalCut);

    hMET_Signal[i]  = (TH1D *)hMET_Data->Clone(Form("hMET_Signal_%d", i));   hMET_Signal[i]->Reset();
    hJet1_Signal[i] = (TH1D *)hJet1_Data->Clone(Form("hJet1_Signal_%d", i)); hJet1_Signal[i]->Reset();
    hJet2_Signal[i] = (TH1D *)hJet2_Data->Clone(Form("hJet2_Signal_%d", i)); hJet2_Signal[i]->Reset();
    hJet3_Signal[i] = (TH1D *)hJet3_Data->Clone(Form("hJet3_Signal_%d", i)); hJet3_Signal[i]->Reset();
    hJet4_Signal[i] = (TH1D *)hJet4_Data->Clone(Form("hJet4_Signal_%d", i)); hJet4_Signal[i]->Reset();
    hdPhiMin_Signal[i] = (TH1D *)hdPhiMin_Data->Clone(Form("hdPhiMin_Signal_%d", i)); hdPhiMin_Signal[i]->Reset();

    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      TFile *tfSignal = new TFile(Form("/data3/tkaji/dev_MyProject/runKinematicsOptimization/out.KO.%d_%s.root", DSID[i], MCName[iMC16].c_str()), "READ");
      gROOT->cd();
      TH1D *hSumOfWeightsBCK = (TH1D *)tfSignal->Get("hSumOfWeightsBCK");
      TH1D *hSummary = (TH1D*)tfSignal->Get("hSummary");
      double SF = IntLumi[iMC16]*(gXS_Strong->Eval(GMass[i]))/hSumOfWeightsBCK->GetBinContent(1);
      
      hSummary_Signal[i]->Add(hSummary, SF);
      hSummary_Signal_Sig[i]->Add(hSummary, nExpSignal[i]*SF/nNominalSignal[i]);
      hSummary_Signal_SigCommon[i]->Add(hSummary, SF/nNominalSignal[i]);

      hMET_Signal[i]->Add(((TH1D*)tfSignal->Get("hMET")), SF);
      hJet1_Signal[i]->Add(((TH1D*)tfSignal->Get("hJet1")), SF);
      hJet2_Signal[i]->Add(((TH1D*)tfSignal->Get("hJet2")), SF);
      hJet3_Signal[i]->Add(((TH1D*)tfSignal->Get("hJet3")), SF);
      hJet4_Signal[i]->Add(((TH1D*)tfSignal->Get("hJet4")), SF);
      hdPhiMin_Signal[i]->Add(((TH1D*)tfSignal->Get("hdPhiMin")), SF);

      tfSignal->Close();
      delete tfSignal; tfSignal=NULL;
    }// for iMC16

    hMET_Signal[i]->AddBinContent(1, hMET_Signal[i]->GetBinContent(0));          hMET_Signal[i]->AddBinContent(100, hMET_Signal[i]->GetBinContent(101));
    hJet1_Signal[i]->AddBinContent(1, hJet1_Signal[i]->GetBinContent(0));        hJet1_Signal[i]->AddBinContent(100, hJet1_Signal[i]->GetBinContent(101));
    hJet2_Signal[i]->AddBinContent(1, hJet2_Signal[i]->GetBinContent(0));        hJet2_Signal[i]->AddBinContent(100, hJet2_Signal[i]->GetBinContent(101));
    hJet3_Signal[i]->AddBinContent(1, hJet3_Signal[i]->GetBinContent(0));        hJet3_Signal[i]->AddBinContent(100, hJet3_Signal[i]->GetBinContent(101));
    hJet4_Signal[i]->AddBinContent(1, hJet4_Signal[i]->GetBinContent(0));        hJet4_Signal[i]->AddBinContent(100, hJet4_Signal[i]->GetBinContent(101));
    hdPhiMin_Signal[i]->AddBinContent(1, hdPhiMin_Signal[i]->GetBinContent(0));  hdPhiMin_Signal[i]->AddBinContent(100, hdPhiMin_Signal[i]->GetBinContent(101));
  }// for i

  for(int i=0;i<nBenchmark;i++){
    for(int iCut=0;iCut<nTotalCut;iCut++){
      if((hSummary_Data->GetBinContent(iCut+1)/nNominalData < 0.1) || (iCut<840)){
	hSummary_Signal_Sig[i]->SetBinContent(iCut+1, 0);
	hSummary_Signal_SigCommon[i]->SetBinContent(iCut+1, 0);
	continue;
      }
      double Val = hSummary_Signal_Sig[i]->GetBinContent(iCut+1);
      hSummary_Signal_Sig[i]->SetBinContent(iCut+1, (Val/TMath::Sqrt(nExpData*hSummary_Data->GetBinContent(iCut+1)/nNominalData))/(nExpSignal[i]/TMath::Sqrt(nExpData)));
      double ValCommon = hSummary_Signal_SigCommon[i]->GetBinContent(iCut+1);
      hSummary_Signal_SigCommon[i]->SetBinContent(iCut+1, (ValCommon/TMath::Sqrt(hSummary_Data->GetBinContent(iCut+1)/nNominalData)));

      gSigCommon[i]->SetPoint(iCut, (ValCommon/TMath::Sqrt(hSummary_Data->GetBinContent(iCut+1)/nNominalData)), 
			      hSummary_Signal[i]->GetBinContent(iCut+1)/nNominalSignal[i]);
    }// for iCut
    int iBin = hSummary_Signal_SigCommon[i]->GetMaximumBin() - 1;
    double BinVal = hSummary_Signal_SigCommon[i]->GetBinContent(iBin+1);
    int iMET  = iBin/(nJet1Cut*nJet2Cut*nJet3Cut*nJet4Cut);
    int iJet1 = (iBin%(nJet1Cut*nJet2Cut*nJet3Cut*nJet4Cut))/(nJet2Cut*nJet3Cut*nJet4Cut);
    int iJet2 = (iBin%(nJet2Cut*nJet3Cut*nJet4Cut))/(nJet3Cut*nJet4Cut);
    int iJet3 = (iBin%(nJet3Cut*nJet4Cut))/(nJet4Cut);
    int iJet4 = (iBin%nJet4Cut);
    //std::cout << " i = " << i << ", iBin = " << iBin << ", iMET = " << iMET << ", iJet1 = " << iJet1 << ", iJet2 = " << iJet2 << ", iJet3 = " << iJet3 << ", iJet4 = " << iJet4 << std::endl;
    std::cout << " i = " << i << ", BinVal = " << BinVal << ", SigEff. = " << hSummary_Signal[i]->GetBinContent(iBin+1)/nNominalSignal[i] 
	      << ", BGEff. = " << hSummary_Data->GetBinContent(iBin+1)/nNominalData
	      << ", MET = " << METCut[iMET] << ", Jet1 = " << Jet1Cut[iJet1] << ", Jet2 = " << Jet2Cut[iJet2] << ", Jet3 = " << Jet3Cut[iJet3] << ", Jet4 = " << Jet4Cut[iJet4] << std::endl;
  }// for i benchmark

  c0 = new TCanvas("c0", "c0", 800, 600);
  c0->Print(Form("%s.pdf[", OutName.c_str()), "pdf");
  c0->SetLogy(true);

  hMET_Data->GetYaxis()->SetRangeUser(0.01, 1e9);
  hMET_Data->Draw();
  for(int i=0;i<5;i++){
    hMET_Signal[i]->SetLineColor(i+2);
    hMET_Signal[i]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  c0->Print(Form("myKinematicsOptimization/%s.png", OutName.c_str()), "pdf");
  hMET_Data->Draw();
  for(int i=0;i<5;i++){
    hMET_Signal[i+5]->SetLineColor(i+2);
    hMET_Signal[i+5]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");

  hJet1_Data->GetYaxis()->SetRangeUser(0.01, 1e9);
  hJet1_Data->Draw();
  for(int i=0;i<5;i++){
    hJet1_Signal[i]->SetLineColor(i+2);
    hJet1_Signal[i]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  hJet1_Data->Draw();
  for(int i=0;i<5;i++){
    hJet1_Signal[i+5]->SetLineColor(i+2);
    hJet1_Signal[i+5]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");

  hJet2_Data->GetYaxis()->SetRangeUser(0.01, 1e9);
  hJet2_Data->Draw();
  for(int i=0;i<5;i++){
    hJet2_Signal[i]->SetLineColor(i+2);
    hJet2_Signal[i]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  hJet2_Data->Draw();
  for(int i=0;i<5;i++){
    hJet2_Signal[i+5]->SetLineColor(i+2);
    hJet2_Signal[i+5]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");

  hJet3_Data->GetYaxis()->SetRangeUser(0.01, 1e9);
  hJet3_Data->Draw();
  for(int i=0;i<5;i++){
    hJet3_Signal[i]->SetLineColor(i+2);
    hJet3_Signal[i]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  hJet3_Data->Draw();
  for(int i=0;i<5;i++){
    hJet3_Signal[i+5]->SetLineColor(i+2);
    hJet3_Signal[i+5]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");

  hJet4_Data->GetYaxis()->SetRangeUser(0.01, 1e9);
  hJet4_Data->Draw();
  for(int i=0;i<5;i++){
    hJet4_Signal[i]->SetLineColor(i+2);
    hJet4_Signal[i]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  hJet4_Data->Draw();
  for(int i=0;i<5;i++){
    hJet4_Signal[i+5]->SetLineColor(i+2);
    hJet4_Signal[i+5]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");

  hdPhiMin_Data->GetYaxis()->SetRangeUser(0.01, 1e9);
  hdPhiMin_Data->Draw();
  for(int i=0;i<5;i++){
    hdPhiMin_Signal[i]->SetLineColor(i+2);
    hdPhiMin_Signal[i]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  hdPhiMin_Data->Draw();
  for(int i=0;i<5;i++){
    hdPhiMin_Signal[i+5]->SetLineColor(i+2);
    hdPhiMin_Signal[i+5]->Draw("sameHIST");
  }
  c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");

  c0->SetLogy(false);
  for(int i=0;i<nBenchmark;i++){
    gSigCommon[i]->GetXaxis()->SetTitle("S/#sqrt{B} improvement");
    gSigCommon[i]->GetYaxis()->SetTitle("Signal efficiency w.r.t. current threshold");
    gSigCommon[i]->Draw("AP");
    c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  }

  for(int i=0;i<nBenchmark;i++){
    hSummary_Signal[i]->Draw();
    c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  }
  for(int i=0;i<nBenchmark;i++){
    hSummary_Signal_Sig[i]->SetYTitle("Sig");
    hSummary_Signal_Sig[i]->Draw();
    c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  }
  for(int i=0;i<nBenchmark;i++){
    hSummary_Signal_SigCommon[i]->SetYTitle("S/#sqrt{B} improvement");
    hSummary_Signal_SigCommon[i]->Draw();
    c0->Print(Form("%s.pdf", OutName.c_str()), "pdf");
  }

  c0->Print(Form("%s.pdf]", OutName.c_str()), "pdf");

  TFile *tf = new TFile(Form("%s.root", OutName.c_str()), "RECREATE");
  hSummary_Data->Write();
  hMET_Data->Write();
  hJet1_Data->Write();
  hJet2_Data->Write();
  hJet3_Data->Write();
  hJet4_Data->Write();
  hdPhiMin_Data->Write();

  for(int i=0;i<nBenchmark;i++){
    hSummary_Signal[i]->Write();
    hSummary_Signal_Sig[i]->Write();
    hSummary_Signal_SigCommon[i]->Write();
  }

  for(int i=0;i<nBenchmark;i++){
    hMET_Signal[i]->Write();
    hJet1_Signal[i]->Write();
    hJet2_Signal[i]->Write();
    hJet3_Signal[i]->Write();
    hJet4_Signal[i]->Write();
    hdPhiMin_Signal[i]->Write();
  }

  tf->Close();
  
  return 0;
}
