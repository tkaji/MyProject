#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

const int nMC16 = 3;
const int nRunMC16 = 1;
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];
//TCut MCWeight = "(weightXsec*weightMCweight*weightPileupReweighting)";

TH1D *hCutFlow;
TH1D *hCutFlowWithWeight;

//std::vector<std::vector<int>> vDSID_EWK_1p0 = {{448496, 448497}};
std::vector<std::vector<int>> vDSID_EWK_1p0 = {{448305}};
std::vector<double> CMass_EWK_1p0  = {599.9};
std::vector<NtupleReader *> *mySignal_EWK_1p0[nMC16][1];
std::vector<double> vSFSig_EWK_1p0[nMC16][1];
TFile *tf;
NtupleReader *myReader=NULL;
TEventList *eventList=NULL;
TFile *tfXS=NULL;
TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];
TCanvas *c0;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH2D *hFrame;
std::string PDFName;
TLegend *tl;
TH1D *hDecayRadius;
TH1D *hAvePU;
TH1D *hDecayRadius_Pass;
TH1D *hAvePU_Pass;
TGraphAsymmErrors *gDecayRadius;
TGraphAsymmErrors *gAvePU;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("myCharginoStudy"), "output file");
  
  po::positional_options_description p;
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  PDFName = vm["output-file"].as<std::string>();
  std::string OutputFileName = vm["output-file"].as<std::string>();

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  c0 = new TCanvas("c0", "c0", 800, 600);

  hDecayRadius = new TH1D("hDecayRadius", ";Decay Radius [mm]", 50, 0.0, 500.0);
  hDecayRadius_Pass = new TH1D("hDecayRadius_Pass", ";Decay Radius [mm]", 50, 0.0, 500.0);

  hAvePU = new TH1D("hAvePU", ";Average Interactions Per Crossing", 50, 10, 60);
  hAvePU_Pass = new TH1D("hAvePU_Pass", ";Average Interactions Per Crossing", 50, 10, 60);

  hCutFlow = new TH1D("hCutFlow", "", 10, 0, 10);
  hCutFlowWithWeight = new TH1D("hCutFlowWithWeight", "", 10, 0, 10);
  
  std::cout << "Start Loading Signal MC" << std::endl;
  /* **********   Start Loading Signal MC   ********** */
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    std::cout << MCName[iMC16] << " started" << std::endl;

    /* **********   Load Signal MC for Higgsino 0p2  ********** */
    for(int iSignal=0;iSignal<1;iSignal++){
      mySignal_EWK_1p0[iMC16][iSignal] = new std::vector<NtupleReader *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_1p0.at(iSignal).size());iDSID++){
	//NtupleReader *myDS = new NtupleReader(Form("/data3/tkaji/SignalProduction/Common_v2.3.x/local_v2.3.4/Output/out.%d_%s._000001.common_ntuple.root", vDSID_EWK_1p0.at(iSignal).at(iDSID), MCName[iMC16].c_str()));
	NtupleReader *myDS = new NtupleReader(Form("/data3/tkaji/Common/TMP/out.%d_%s._000001.common_ntuple.root", vDSID_EWK_1p0.at(iSignal).at(iDSID), MCName[iMC16].c_str()));
	mySignal_EWK_1p0[iMC16][iSignal]->push_back(myDS);
	vSFSig_EWK_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
    }// for iSignal
  }// iMC16


  for(int iSignal=0;iSignal<1;iSignal++){
    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_1p0[iSignal].size());iDSID++){	
	int nEntry = mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->nEntry;
	for(int iEntry=0;iEntry<nEntry;iEntry++){
	  mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetEntry(iEntry);
	  
	  double weightCommon = 1.0;
	  weightCommon *= mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->weightXsec;
	  weightCommon *= mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->weightMCweight;
	  weightCommon *= mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->weightPileupReweighting;
	  float  AvePU  = mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->CorrectedAverageInteractionsPerCrossing;

	  hCutFlow->Fill(0.0);
	  hCutFlowWithWeight->Fill(0.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->IsPassedBadEventVeto==false)
	    continue;
	  hCutFlow->Fill(1.0);
	  hCutFlowWithWeight->Fill(1.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->LU_METtrigger_SUSYTools==false)
	    continue;
	  hCutFlow->Fill(2.0);
	  hCutFlowWithWeight->Fill(2.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->IsPassedLeptonVeto==false)
	    continue;
	  hCutFlow->Fill(3.0);
	  hCutFlowWithWeight->Fill(3.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->missingET->p4.Pt()/1000.0 < 200.0)
	    continue;
	  hCutFlow->Fill(4.0);
	  hCutFlowWithWeight->Fill(4.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->goodJets->size()==0)
	    continue;
	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->goodJets->at(0)->p4.Pt()/1000.0 < 100.0)
	    continue;
	  hCutFlow->Fill(5.0);
	  hCutFlowWithWeight->Fill(5.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->JetMetdPhiMin50 < 1.0)
	    continue;
	  hCutFlow->Fill(6.0);
	  hCutFlowWithWeight->Fill(6.0, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));	 

	  // truth particle
	  if(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->size() > 0){
	    for(int i=0;i<(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->size());i++){
	      int    PDGID = (int)TMath::Abs(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->PdgId);
	      float  DecayRadius = mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->DecayRadius;
	      bool fMatch = false;

	      if(PDGID==1000024){
		for(unsigned int iTrack=0;iTrack<(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->conventionalTracks->size());iTrack++){
		  double dR = TMath::Abs(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->p4.DeltaR(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->conventionalTracks->at(iTrack)->p4));
		  if(dR < 0.05)
		    fMatch = true;
		}// for track
		//hDecayRadius->Fill(DecayRadius, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));		
		hDecayRadius->Fill(DecayRadius);
		if(fMatch)
		  hDecayRadius_Pass->Fill(DecayRadius);
		//hDecayRadius_Pass->Fill(DecayRadius, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));		
		if(DecayRadius > 130.0 && DecayRadius < 290.0){		
		  hAvePU->Fill(AvePU);
		  //hAvePU->Fill(AvePU, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));
		  if(fMatch)
		    hAvePU_Pass->Fill(AvePU);
		  //hAvePU_Pass->Fill(AvePU, weightCommon*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));		    
		}// Decay Radius
	      }// AvePU
	    }// for truth
	  }// if truth size > 0
	}// for iEntry
      }// for iDSID      
    }// for iMC
  }// for iSignal
  
  std::cout << "All            : " << hCutFlow->GetBinContent(1) << ",   " << hCutFlowWithWeight->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning : " << hCutFlow->GetBinContent(2) << ",   " << hCutFlowWithWeight->GetBinContent(2) << std::endl;
  std::cout << "MET Trigger    : " << hCutFlow->GetBinContent(3) << ",   " << hCutFlowWithWeight->GetBinContent(3) << std::endl;
  std::cout << "Lepton VETO    : " << hCutFlow->GetBinContent(4) << ",   " << hCutFlowWithWeight->GetBinContent(4) << std::endl;
  std::cout << "MET > 200 GeV  : " << hCutFlow->GetBinContent(5) << ",   " << hCutFlowWithWeight->GetBinContent(5) << std::endl;
  std::cout << "Jet > 100 GeV  : " << hCutFlow->GetBinContent(6) << ",   " << hCutFlowWithWeight->GetBinContent(6) << std::endl;
  std::cout << "dPhi > 1.0     : " << hCutFlow->GetBinContent(7) << ",   " << hCutFlowWithWeight->GetBinContent(7) << std::endl;


  std::cout << "hoge19" << std::endl;
  gDecayRadius = new TGraphAsymmErrors(hDecayRadius_Pass, hDecayRadius);
  gAvePU = new TGraphAsymmErrors(hAvePU_Pass, hAvePU);
  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");

  hFrame = new TH2D("hFrame", ";Decay Radius [mm];Track Efficiency", 50, 0.0, 500.0, 105, 0.0, 1.05);
  hFrame->Draw();
  gDecayRadius->SetMarkerColor(kRed);
  gDecayRadius->Draw("sameP");
  ATLASLabel(0.6, 0.35, "Internal", kBlack);
  tl = new TLegend(0.6, 0.25, 0.90, 0.35);
  tl->AddEntry(gDecayRadius, "(600 GeV, 1.0 ns)", "EP");
  tl->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete hFrame; hFrame=NULL;
  
  hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Track Efficiency", 50, 10.0, 60.0, 105, 0.0, 1.05);
  hFrame->Draw();
  ATLASLabel(0.2, 0.35, "Internal", kBlack);
  delete tl;tl=NULL;
  tl = new TLegend(0.2, 0.25, 0.5, 0.35);
  tl->AddEntry(gAvePU, "(600 GeV, 1.0 ns)", "EP");
  tl->Draw("same");
  gAvePU->SetMarkerColor(kRed);
  gAvePU->Draw("sameP");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete hFrame; hFrame=NULL;

  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  return 0;
}
