#include "myPDFStudy.h"
namespace po = boost::program_options;
int LoadInput(void);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("CaloReg", po::value<int>()->default_value(0), "CaloReg")
    ("doTFUp", po::value<bool>()->default_value(false), "doTFUp")
    ("doTFDown", po::value<bool>()->default_value(false), "doTFDown")
    ("doSigmaUp", po::value<bool>()->default_value(false), "doSigmaUp")
    ("doSigmaDown", po::value<bool>()->default_value(false), "doSigmaDown")
    ("doSlopeUp", po::value<bool>()->default_value(false), "doSlopeUp")
    ("doSlopeDown", po::value<bool>()->default_value(false), "doSlopeDown")
    ("doSmear", po::value<bool>()->default_value(true), "doSmearing")
    ("SimpleSmear", po::value<bool>()->default_value(false), "SimpleSmearing")
    ("output-file,o", po::value<std::string>()->default_value("myPDFStudy"), "output file")
    ("conf", po::value<std::string>()->default_value("Nominal.txt"), "conf file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  fDoSmearing = vm["doSmear"].as<bool>();
  fSimpleSmear = vm["SimpleSmear"].as<bool>();

  CaloReg   = vm["CaloReg"].as<int>();

  fDoTFUp   = vm["doTFUp"].as<bool>();
  fDoTFDown = vm["doTFDown"].as<bool>();

  fDoSigmaUp   = vm["doSigmaUp"].as<bool>();
  fDoSigmaDown = vm["doSigmaDown"].as<bool>();
  fDoSlopeUp   = vm["doSlopeUp"].as<bool>();
  fDoSlopeDown = vm["doSlopeDown"].as<bool>();

  LoadInput();

  int nEntry;
  double vInt;
  double eInt;
  int iTFDisap[nCat] = {-1, 0, 2};
  int iTFMSCalo[nCat] = {0, 0, 10};
  int iCorrect[nCat] = {0, 10, -1};
  double nVal[nCat*nMETRegion]={0.0};
  double nErr[nCat*nMETRegion]={0.0};


  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");

  for(int iCat=0;iCat<nCat;iCat++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      int i = iCat*3 + iMET;
      hSimpleSmear->Reset();
      nEntry = treeCat[i]->GetEntries();

      for(int iEntry=0;iEntry<nEntry;iEntry++){
	treeCat[i]->GetEntry(iEntry);
	hData1D->Reset();

	fSmearMuon = (iCat==1 ? false : true);

	if(CaloReg==0){
	  if(iCat==2 && b_Calo>5.0)
	    continue;
	}else if(CaloReg==1){
	  if(iCat==2 && (b_Calo>10.0 || b_Calo<5.0))
	    continue;
	  iTFMSCalo[0] = 1;
	  iTFMSCalo[1] = 1;

	  iCorrect[0] = 1;
	  iCorrect[1] = 11;
	}else if(CaloReg==2){
	  iTFMSCalo[0] = -1;
	  iTFMSCalo[1] = -1;

	  iCorrect[0] = -1;
	  iCorrect[1] = -1;
       	}else{
	  std::cout << "Wrong CaloReg Value!!" << std::endl;
	}

	if(fDoSmearing==false){
	  break;
	}else if(fDoSmearing==true && fSimpleSmear==false){
	  GetTF(TFVal, b_pT, b_Eta, b_Phi, iTFDisap[iCat], iTFMSCalo[iCat], iCorrect[iCat]);
	  SmearPt(hData1D, b_pT, 1.0, true);      
	}else if(fDoSmearing==true && fSimpleSmear==true){

	}

	double vForLT20GeV = hData1D->Integral();
	if(fDoTFUp){
	  nVal[i] += (TFVal[0] + TFVal[1])*vForLT20GeV;
	}else if(fDoTFDown){
	  nVal[i] += ((TFVal[0] - TFVal[1] < 0.0) ? 0.0 : (TFVal[0] - TFVal[1]))*vForLT20GeV;
	}else{
	  nVal[i] +=     TFVal[0]*vForLT20GeV;
	}
	nErr[i] += pow(TFVal[1]*vForLT20GeV, 2);
	//nErr[iCat] += pow(TFVal[0]*vForLT20GeV*TFVal[1], 2);

	//std::cout << TFVal[0]*vForLT20GeV << " +- " << TFVal[1]*vForLT20GeV << std::endl;
	for(int iBin=0;iBin<nLogPt;iBin++){
	  double BinVal = hData1D->GetBinContent(iBin+1);
	  if(fDoTFUp){
	    hData1D->SetBinContent(iBin+1, BinVal*(TFVal[0] + TFVal[1])); 
	  }else if(fDoTFDown){
	    hData1D->SetBinContent(iBin+1, BinVal*((TFVal[0] - TFVal[1] < 0.0) ? 0.0 : (TFVal[0] - TFVal[1])));
	  }else{
	    hData1D->SetBinContent(iBin+1, BinVal*TFVal[0]); 
	  }

	  hData1D->SetBinError(  iBin+1, BinVal*TFVal[1]);
	}// for iBin
	hNominal[i]->Add(hData1D);
      }// for iEntry
      nErr[i] = TMath::Sqrt(nErr[i]);

      if(fDoSmearing==false){
	treeCat[i]->Draw("pT/1000.0>>hData1D", "vTF");
      }
      if(fDoSmearing==true && fSimpleSmear==true){
	for(int iBin=0;iBin<(hSimpleSmear->GetNbinsX());iBin++){
	  SmearPt(hData1D, hSimpleSmear->GetBinCenter(iBin+1), hSimpleSmear->GetBinContent(iBin+1), true);
	}// for iBin
      }

      vInt = hNominal[i]->IntegralAndError(1, nLogPt, eInt);
      if(iMET==0 ){
	double BlindFactor = hNominal[iCat]->Integral(1, nLogPt/3)/vInt;
	vInt *= BlindFactor;
	eInt *= BlindFactor;
      }
      //std::cout << CatName[iCat] << " : " << vInt << " +- " << eInt << std::endl;
      //std::cout << CatName[iCat] << " : " << nVal[iCat] << " +- " << nErr[iCat] << std::endl;
      std::cout << CatName[iCat] << " - " << RegName[iMET] << " : " << nVal[i] 
		<< ",   " << hNominal[i]->Integral(1, nLogPt) << ",   ";
      if(iMET!=2)
	std::cout << hNominal[i]->Integral(1, nLogPt/3) << std::endl;
      else
	std::cout << std::endl;
      //hData1D->Draw("E");
      for(int iBin=0;iBin<nLogPt;iBin++){
	double BinVal = hNominal[i]->GetBinContent(iBin+1);
	double BinErr = hNominal[i]->GetBinError(iBin+1);

	hNominal[i]->SetBinContent(iBin+1, BinVal/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin]));
	hNominal[i]->SetBinError(  iBin+1, BinErr/(XBinsLogPt[iBin+1] - XBinsLogPt[iBin]));
      }// for iBin

      hNominal[i]->Draw("E");
      c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    }// for iMET
  }// for iCat
  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  if(fDoTFUp){
    SysName = "TFUp";
    LineCol = EColor::kRed;
  }else if(fDoTFDown){
    SysName = "TFDown";
    LineCol = EColor::kGreen+1;
  }else if(fDoSigmaUp){
    SysName = "SigmaUp";
    LineCol = EColor::kRed;
  }else if(fDoSigmaDown){
    SysName = "SigmaDown";
    LineCol = EColor::kGreen+1;
  }else if(fDoSlopeUp){
    SysName = "SlopeUp";
    LineCol = EColor::kRed;
  }else if(fDoSlopeDown){
    SysName = "SlopeDown";
    LineCol = EColor::kGreen+1;
  }else{
    SysName = "nominal";
    LineCol = EColor::kBlack;
  }

  tfOutput = new TFile(Form("%s_%s.root", PDFName.c_str(), SysName.c_str()), "RECREATE");
  for(int iCat=0;iCat<nCat;iCat++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      int i = iCat*3 + iMET;
      hNominal[i]->SetName(Form("hCat%d_MET%d_%s", iCat, iMET, SysName.c_str()));
      hNominal[i]->SetLineColor(LineCol);
      hNominal[i]->Write("", TObject::kOverwrite);
    }// for iMET
  }// for iCat
  tfOutput->Close();

  return 0;  
}

int LoadInput(void){
  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/tmp/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  hLumiHistData15 = (TH1F *)tfInput->Get("lumi_histo_data15");
  hLumiHistData16 = (TH1F *)tfInput->Get("lumi_histo_data16");
  hLumiHistData17 = (TH1F *)tfInput->Get("lumi_histo_data17");
  hLumiHistData18 = (TH1F *)tfInput->Get("lumi_histo_data18");
  hEleBG_TF_PtEta    = (TH2D *)tfInput->Get("hEleBG_TF_PtEta");
  hEleBG_TF_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_InvPtEta");
  hEleBG_TF_CaloIso_4to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_PtEta");
  hEleBG_TF_CaloIso_4to123_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_PtEta");
  hEleBG_TF_CaloIso_432to0_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_PtEta");
  hEleBG_TF_CaloIso_432to1_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_PtEta");
  hEleBG_TF_CaloIso_432to01_PtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_PtEta");
  hEleBG_TF_CaloIso_4to0_InvPtEta   = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to0_InvPtEta");
  hEleBG_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_4to123_InvPtEta");
  hEleBG_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to0_InvPtEta");
  hEleBG_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to1_InvPtEta");
  hEleBG_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hEleBG_TF_CaloIso_432to01_InvPtEta");
  hMuBG_TF_1Bin     = (TH1D *)tfInput->Get("hMuBG_TF_1Bin");
  hMuBG_TF_13Bin    = (TH1D *)tfInput->Get("hMuBG_TF_13Bin");
  hMuBG_TF_PtEta    = (TH2D *)tfInput->Get("hMuBG_TF_PtEta");
  hMuBG_TF_InvPtEta = (TH2D *)tfInput->Get("hMuBG_TF_InvPtEta");
  hMSBG_TF_PhiEta   = (TH2D *)tfInput->Get("hMSBG_TF_PhiEta");
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta    = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_PtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta  = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_PtEta");
  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta");
  hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta = (TH2D *)tfInput->Get("hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta");

  P_caloveto05_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto05_hadron_StdTrk");
  P_caloveto10_hadron_StdTrk      = (TH1D *)tfInput->Get("P_caloveto10_hadron_StdTrk");
  P_caloveto05_10_hadron_StdTrk   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_StdTrk");  
  P_caloveto05_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto05_hadron_Trklet");     
  P_caloveto10_hadron_Trklet      = (TH1D *)tfInput->Get("P_caloveto10_hadron_Trklet");     
  P_caloveto05_10_hadron_Trklet   = (TH1D *)tfInput->Get("P_caloveto05_10_hadron_Trklet");  
							                                 
  P_caloveto05_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto05_electron_StdTrk");   
  P_caloveto10_electron_StdTrk    = (TH1D *)tfInput->Get("P_caloveto10_electron_StdTrk");   
  P_caloveto05_10_electron_StdTrk = (TH1D *)tfInput->Get("P_caloveto05_10_electron_StdTrk");
  P_caloveto05_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto05_electron_Trklet");   
  P_caloveto10_electron_Trklet    = (TH1D *)tfInput->Get("P_caloveto10_electron_Trklet");   
  P_caloveto05_10_electron_Trklet = (TH1D *)tfInput->Get("P_caloveto05_10_electron_Trklet");

  hCorrHad = (TH1D *)tfInput->Get("hCorrHad_caloveto05");
  hCorrHad_Side = (TH1D *)tfInput->Get("hCorrHad_caloveto05_10");
  hCorrEle = (TH1D *)tfInput->Get("hCorrEle_caloveto05");
  hCorrEle_Side = (TH1D *)tfInput->Get("hCorrEle_caloveto05_10");

  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");
  gErrorIgnoreLevel = 1001;

  fSmearInt = new TF1("fSmearInt", [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
  fSmearInt->SetParameter(0, g_par_Mean);
  fSmearInt->SetParameter(1, g_par_Sigma);
  fSmearInt->SetParameter(2, g_par_Alpha);

  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);

    if(fDoSigmaUp){
      fSmearInt_ele[i]->SetParameter(1, (1.0 + Sys_Sigma_Ele)*g_par_Sigma_ele[i]);
    }else if(fDoSigmaDown){
      fSmearInt_ele[i]->SetParameter(1, (1.0 - Sys_Sigma_Ele)*g_par_Sigma_ele[i]);
    }else{
      fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    }

    if(fDoSlopeUp){
      fSmearInt_ele[i]->SetParameter(2, (1.0 + Sys_Slope_Ele)*g_par_Alpha_ele[i]);    
    }else if(fDoSlopeDown){
      fSmearInt_ele[i]->SetParameter(2, (1.0 - Sys_Slope_Ele)*g_par_Alpha_ele[i]);    
    }else{
      fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    
    }

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);

    if(fDoSigmaUp){
      fSmearInt_mu[i]->SetParameter(1, (1.0 + Sys_Sigma_Mu)*g_par_Sigma_mu[i]);
    }else if(fDoSigmaDown){
      fSmearInt_mu[i]->SetParameter(1, (1.0 - Sys_Sigma_Mu)*g_par_Sigma_mu[i]);
    }else{
      fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    }

    if(fDoSlopeUp){
      fSmearInt_mu[i]->SetParameter(2, (1.0 + Sys_Slope_Mu)*g_par_Alpha_mu[i]);    
    }else if(fDoSlopeDown){
      fSmearInt_mu[i]->SetParameter(2, (1.0 - Sys_Slope_Mu)*g_par_Alpha_mu[i]);    
    }else{
      fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
    }

  }// for i
  c0 = new TCanvas("c0", "", 800, 600);
  c0->SetLogx();
  c0->SetLogy();

  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nLogPt, XBinsLogPt);
  hData1D = (TH1D *)hTemplate_TrackPt->Clone("hData1D");
  for(int iCat=0;iCat<nCat;iCat++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      hNominal[iCat*3+iMET] = (TH1D *)hTemplate_TrackPt->Clone(Form("hNominal_Cat%d_MET%d", iCat, iMET));
    }// for iMET
  }// for iCat

  double xBins[961];
  xBins[0]=5.0;
  for(int i=1;i<=960;i++){
    xBins[i] = xBins[i-1]*(pow(12500.0/5.0, 1.0/960.0));
  }
  hSimpleSmear = new TH1D("hSimpleSmear", "", 960, xBins);

  tfFile = new TFile(Form("myPDFStudyFile_Chain%d.root", iChain), "READ");
  gROOT->cd();
  for(int iCat=0;iCat<nCat;iCat++){
    for(int iMET=0;iMET<nMETRegion;iMET++){
      int i = iCat*3 + iMET;
      treeCat[i] = (TTree *)tfFile->Get(Form("tree%s%s", CatName[iCat].c_str(), RegName[iMET].c_str()));
      treeCat[i]->SetBranchAddress("pT" , &b_pT );
      treeCat[i]->SetBranchAddress("Eta", &b_Eta);
      treeCat[i]->SetBranchAddress("Phi", &b_Phi);
      treeCat[i]->SetBranchAddress("Calo", &b_Calo);
    }
  }// for 
  /*
  ifs.open(vm["conf"].as<std::string>());
  while(ifs>>VarName>>VarValue){
    if(name.find("MuSignal")!=string::npos){
      MuSignal = VarValue;
    }
    if(name.find("NSignalSR")!=string::npos){
      NSignalSR = VarValue;
    }
    if(name.find("NSignalVR")!=string::npos){
      NSignalVR = VarValue;
    }
    if(name.find("NMuonSR")!=string::npos){
      NMuonSR = VarValue;
    }
    if(name.find("NMuonVR")!=string::npos){
      NMuonVR = VarValue;
    }
    if(name.find("NMismeasuredVR")!=string::npos){
      NMismeasuredVR = VarValue;
    }
    if(name.find("NMismeasuredFakeVR")!=string::npos){
      NMismeasuredFakeVR = VarValue;
    }
    if(name.find("NMismeasuredFakeCR")!=string::npos){
      NMismeasuredFakeCR = VarValue;
    }
    if(name.find("CDRatio")!=string::npos){
      CDRatio = VarValue;
    }
    if(name.find("LogABCDRatio")!=string::npos){
      LogABCDRatio = VarValue;
    }
    if(name.find("SmearingFunctionSigmaMuon")!=string::npos){
      SmearingFunctionSigmaMuon = VarValue;
    }
    if(name.find("SmearingFunctionSlopeMuon")!=string::npos){
      SmearingFunctionSlopeMuon = VarValue;
    }
    if(name.find("SmearingFunctionSigmaPion")!=string::npos){
      SmearingFunctionSigmaPion = VarValue;
    }
    if(name.find("SmearingFunctionSlopePion")!=string::npos){
      SmearingFunctionSlopePion = VarValue;
    }
    if(name.find("SmearingFunctionSigmaElectron")!=string::npos){
      SmearingFunctionSigmaElectron = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
    if(name.find("")!=string::npos){
      = VarValue;
    }
  }
  */
  return 0;
}
