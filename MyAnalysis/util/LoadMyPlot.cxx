#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TEventList *eventList=NULL;
const int nMETRegion=3;
const std::string RegName[nMETRegion] = {"high-MET", "middle-MET", "low-MET"};
bool fUpdateNote=false;
bool fMaskPt=true;
double MaskPt=20.0;
TFile *tfOutput;
bool fSMBG=false;
TH2D *hHogeHoge;
//=== Smearing function parameters ===//
const double g_par_Mean  = -4.50114e-01;
const double g_par_Sigma =  1.35062e+01;
const double g_par_Alpha =  1.68961e+00;

// Load Common Input File
TFile *tfInput=NULL;
TH2D *hTrigMETJetEff[nMETTrig];
TH2D *hEleBG_TF_PtEta=NULL;
TH2D *hEleBG_TF_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_InvPtEta=NULL;
TH1D *hMuBG_TF_1Bin=NULL;
TH2D *hMuBG_TF_PtEta=NULL;
TH2D *hMuBG_TF_InvPtEta=NULL;
TH2D *hMSBG_TF_PhiEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta=NULL;

TH1D *P_caloveto05_hadron_StdTrk=NULL;
TH1D *P_caloveto10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_hadron_Trklet=NULL;
TH1D *P_caloveto10_hadron_Trklet=NULL;
TH1D *P_caloveto05_10_hadron_Trklet=NULL;

TH1D *P_caloveto05_electron_StdTrk=NULL;
TH1D *P_caloveto10_electron_StdTrk=NULL;
TH1D *P_caloveto05_10_electron_StdTrk=NULL;
TH1D *P_caloveto05_electron_Trklet=NULL;
TH1D *P_caloveto10_electron_Trklet=NULL;
TH1D *P_caloveto05_10_electron_Trklet=NULL;

// Load XS File
TFile *tfXS=NULL;
TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];
const std::string DirName_Strong = "/data3/tkaji/dev_MyProject/files/NewSignal/";
const std::string DirName_EWK    = "/data3/tkaji/dev_MyProject/files/NewSignal/";

const int nSignal_EWK=1;
std::vector<int> vDSID_EWK[nSignal_EWK]  = { {448304, 448305}};
//std::vector<int> vDSID_EWK[nSignal_EWK]  = { {448496, 448497}};
const int CMass_EWK[nSignal_EWK]={600};
const std::vector<double> vSignalXS_EWK[nSignal_EWK]={{0.0193666, 0.00911829}}; // pb
const std::vector<double> vFilterEff_EWK[nSignal_EWK]={{0.406994, 0.40207}};

const int nSignal_Strong=2;
const int GMass_Strong[nSignal_Strong]={1400, 1800};
const int CMass_Strong[nSignal_Strong]={1100,  900};
//const int GMass_Strong[nSignal_Strong]={1400, 2200};
//const int CMass_Strong[nSignal_Strong]={1100,  700};
const int SignalDSID[nSignal_Strong]={448380, 448381};
//const int SignalDSID[nSignal_Strong]={448360, 448567};
EColor SigColor[nSignal_Strong]={EColor::kMagenta, EColor::kCyan};
const std::string PlotStatus="Internal";
const int nData=4;
const std::string DataFile[4]={"/data3/tkaji/dev_MyProject/files/NewSignal/data15-18_13TeV.root", "/data3/tkaji/dev_MyProject/files/NewSignal/data15-16_13TeV.root", "/data3/tkaji/dev_MyProject/files/NewSignal/data17_13TeV.root", "/data3/tkaji/dev_MyProject/files/NewSignal/data18_13TeV.root"};
//const std::string SignalFile[nSignal]={"/data3/tkaji/dev_MyProject/files/out.1400_1200.root",
//				       "/data3/tkaji/dev_MyProject/files/out.1800_800.root"};
const std::string AllDataTag = "Data 2015-2018";
const std::string DataTag[4]={"Data 2015-2018", "Data 2015-2016", "Data 2017", "Data 2018"};
const int ColData[4] = {EColor::kBlack, EColor::kRed, EColor::kGreen+1, EColor::kBlue};
// 2015 : 0
// 2016 : 1141.73
// 2017 : 3030.55
// 2018 :  881.025

const int nSM = 10;
const int ColZnunu     = EColor::kYellow-7;
const int ColWmunu     = EColor::kBlue;
const int ColWenu      = EColor::kRed-4;
const int ColWtaunu    = EColor::kGreen+1 ;
const int ColZee       = EColor::kRed-10;
const int ColZmumu     = EColor::kAzure+6 ;
const int ColDiBoson   = EColor::kViolet;
const int ColZtautau   = EColor::kSpring-4 ;
//const int ColTop       = EColor::kRed+2;
const int ColTop       = EColor::kOrange+2;
const int Colmultijets = EColor::kOrange;
const int ColSM[nSM]   = {ColZnunu, ColWmunu, ColWenu, ColWtaunu, ColZee, ColZmumu, ColZtautau, ColDiBoson, ColTop, Colmultijets};
//const std::string SMName[nSM] = {"Znunu",  "Wmunu",  "Wenu",  "Wtaunu",  "Zee",  "Zmumu", "Ztautau",  "DiBoson",  "Top",  "multijets"};
const std::string SMName[nSM] = {"Z#nu#nu",  "W#mu#nu",  "We#nu",  "W#tau#nu",  "Zee",  "Z#mu#mu", "Z#tau#tau",  "VV",  "t,t#bar{t}",  "dijet"};

const int nMC16 = 3;
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
std::vector<int> vDSID_multijets[nMC16]  = { {361020, 361021, 361022, 361023, 361024, 361025, 361026, 361027, 361028, 361029, 361030, 361031, 361032},
					     {361020, 361021, 361022, 361023, 361024, 361025, 361026, 361027, 361028, 361029, 361030, 361031, 361032},
					     {361020, 361021, 361022, 361023, 361024, 361025, 361026, 361027, 361028, 361029, 361030, 361031, 361032}};
std::vector<int> vDSID_Zee[nMC16]        = { {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127},
					     {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127},
					     {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127}};
std::vector<int> vDSID_Zmumu[nMC16]      = { {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113},
					     {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113},
					     {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113}};
std::vector<int> vDSID_Ztautau[nMC16]    = { {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141},
					     {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141},
					     {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141}};
std::vector<int> vDSID_Znunu[nMC16]      = { {364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155},
					     {364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155},
					     {364222, 364223, 366010, 366011, 366012, 366013, 366014, 366015, 366016, 366017, 366019, 366020, 366021, 366022, 366023, 366024, 366025, 366026, 366028, 366029, 366030, 366031, 366032, 366033, 366034, 366035}};
std::vector<int> vDSID_Wmunu[nMC16]      = { {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169},
					     {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169},
					     {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169}};
std::vector<int> vDSID_Wenu[nMC16]       = { {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183},
					     {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183},
					     {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183}};
std::vector<int> vDSID_Wtaunu[nMC16]     = { {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197},
					     {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197},
					     {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197}};
std::vector<int> vDSID_Top[nMC16]        = { {410470, 410471, 410644, 410645, 410646, 410647},
					     {410470, 410471, 410644, 410645, 410646, 410647},
					     {410470, 410471, 410644, 410645, 410646, 410647}};
std::vector<int> vDSID_DiBoson[nMC16]    = { {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305},
					     {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305},
					     {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305}};

const std::string MCDir[nMC16]={"/data3/tkaji/dev_MyProject/files/mc16a_link", "/data3/tkaji/dev_MyProject/files/mc16d_link", "/data3/tkaji/dev_MyProject/files/mc16e_link"};
const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];
const double SignalXS_Strong[nSignal_Strong]={0.0284,
				       0.00293}; // fb = 1e3*pb
const double SignalProdEff[nSignal_Strong]={1.0, 1.0};
double SFSig_Strong[nMC16][nSignal_Strong];
std::vector<double> vSFSig_EWK[nMC16][nSignal_EWK];

//MyManager *mySignal[nSignal];
//HistManager *mySignalHist[nSignal];
MyManager *myData[nData];
HistManager *myHistData[nData];
MyManager *myTest[50][nData];
HistManager *myHistTest[50][nData];

MyManager *mySignal_Strong[nMC16][nSignal_Strong];
HistManager *myHistSignal_Strong[nMC16][nSignal_Strong];

std::vector<MyManager *> *mySignal_EWK[nMC16][nSignal_EWK];
std::vector<HistManager *> *myHistSignal_EWK[nMC16][nSignal_EWK];

std::vector<MyManager *> *myZnunu[nMC16];
std::vector<MyManager *> *myWmunu[nMC16];
std::vector<MyManager *> *myWenu[nMC16];
std::vector<MyManager *> *myWtaunu[nMC16];
std::vector<MyManager *> *myZee[nMC16];
std::vector<MyManager *> *myZmumu[nMC16];
std::vector<MyManager *> *myZtautau[nMC16];
std::vector<MyManager *> *myDiBoson[nMC16];
std::vector<MyManager *> *myTop[nMC16];
std::vector<MyManager *> *mymultijets[nMC16];
std::vector<MyManager *> *mySM[nSM][nMC16];

std::vector<HistManager *> *myHistZnunu[nMC16];
std::vector<HistManager *> *myHistWmunu[nMC16];
std::vector<HistManager *> *myHistWenu[nMC16];
std::vector<HistManager *> *myHistWtaunu[nMC16];
std::vector<HistManager *> *myHistZee[nMC16];
std::vector<HistManager *> *myHistZmumu[nMC16];
std::vector<HistManager *> *myHistZtautau[nMC16];
std::vector<HistManager *> *myHistDiBoson[nMC16];
std::vector<HistManager *> *myHistTop[nMC16];
std::vector<HistManager *> *myHistmultijets[nMC16];
std::vector<HistManager *> *myHistSM[nSM][nMC16];

bool fSimpleSmear=true;
int RebinVal=1;
TCanvas *c0;
TPad *pad0;
TPad *pad1[2];
TF1 *fSmearInt;
//TH1D *hData=NULL;
TLegend *tl;
TLegend *tlSM;
THStack *hStack_SM=NULL;
TH1D *h1000;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTemplate_MET=NULL;
TH1D *hTemplate_Calo=NULL;
TH1D *hData1D=NULL;
TH2D *hData2D=NULL;
TH1D *hSignal1D_Strong[nSignal_Strong];
TH2D *hSignal2D_Strong[nSignal_Strong];
TH1D *hSignal1D_EWK[nSignal_EWK];
TH2D *hSignal2D_EWK[nSignal_EWK];

TH1D *hCalo_Signal_Strong[nSignal_Strong];
TH1D *hCalo_Muon_Strong;
TH1D *hCalo_Fake_Strong;
TH1D *hCalo_LowMETHighPt_Strong;

TH2D *hFrame;
TH2D *hRatioFrame;
std::string InputName;
std::string PDFName;
int SetGraphPoint(double vNume, double vDenom, TGraphAsymmErrors *gGraph, int iPoint, bool fZeroError=false);
int CalcFrac(double vNume, double vDenom, double ret[2]);
double GetTF(double pT, double Eta, double Phi, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1);
int DrawTrackPt(std::string TreeName, TCut myCut, int iOpt=0, int iTFDisap=-1, int iTFMSCalo=-1, int iCorrect=-1, bool fSmear=false);
int DrawRatioPlot(void);
int DrawText(double xPos, double yPos, double iLumi, std::string myText1="", std::string myText2="");
int DrawTH1RatioPlot(std::string TreeName, std::string ValName, TH1D *hHist, double Scale, TCut myCut, int iOpt);
int DrawTH1RatioPlot(std::string HistName, int iHist, int nRebin=1, int iOpt=0, int SameOpt=0, bool fEWK=false);
double crystallBallIntegral(double* x, double *par);
void SmearPt(TH1D *hHoge, double pt, double);
THStack *GetHStack(std::string HistName, int iHist, int nRegin=1, int iOpt=0);
THStack *GetHStack(std::string TreeName, std::string ValName, double Scale=1.0, TCut myCut="1", int iOpt=0);
int FillOverflowBin(TH1D *h1);
int FillOverflowBinForData(TH1D *h1);
int SetupCanvas(int iOption, std::string xTitle, double MinX, double MaxX, double MinY, double MaxY);
TCut GetKinematicsCut(int iMETRegion=0,  // 0:SR,  1:VR(150 < MET < 200),  2:CR(100 < MET < 150)
		      int iMETType=0);   // 0:MET, 1:MET_ForEleCR, 2:MET_ForMuCR

TCut CaloVETO_0   = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
TCut CaloVETO_1   = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 10.0)";
TCut CaloVETO_123 = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 20.0)";

TCut CaloVETO_432 = "(DisappearingTracks.etclus20_topo/1000.0 > 10.0)";
TCut CaloVETO_4   = "(DisappearingTracks.etclus20_topo/1000.0 > 20.0)";
TCut FakeD0_CR    = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>10.0)";
TCut FakeD0_VR    = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>3.0 && TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)<10.0)";

//int CompareData(std::string HistName, int iHist, int nRebin=1);
//int CompareData(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist=NULL, double Scale=1.0);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("output-file,o", po::value<std::string>()->default_value("myPlot"), "output file")
    ("input-files,i", po::value<std::string>(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  InputName = vm["input-files"].as<std::string>();

  tfInput = new TFile(Form("%s", InputName.c_str()), "READ");
  gROOT->cd();

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");
  gErrorIgnoreLevel = 1001;
  fSmearInt = new TF1("fSmearInt", [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
  fSmearInt->SetParameter(0, g_par_Mean);
  fSmearInt->SetParameter(1, g_par_Sigma);
  fSmearInt->SetParameter(2, g_par_Alpha);
  
  tl = new TLegend(0.5, 0.4, 0.8, 0.7);
  tlSM = new TLegend(0.5, 0.4, 0.8, 0.7);
  c0 = new TCanvas("c0", "c0", 800, 600);

  hFrame = new TH2D("hFrame", "", 10, 0, 10, 10, 0, 10);
  hStack_SM = new THStack("hStack_SM", "");
  h1000 = new TH1D("h1000", "", 1000, 0, 1000);
  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nLogPt, XBinsLogPt);
  hTemplate_MET = new TH1D("hTemplate_MET", ";MET [GeV];Tracks", 1000, 0, 1000);
  hTemplate_Calo = new TH1D("hTemplate_Calo", ";etclus20_topo [GeV];Tracks", 50, 0, 50);
  hData1D = new TH1D("hData1D", "hoge", 10, 0, 10);
  hData2D = new TH2D("hData2D", "hoge", 10, 0, 10, 10, 0, 10);
  for(int i=0;i<nSignal_Strong;i++){
    hSignal1D_Strong[i] = new TH1D(Form("hSignal1D_Strong_%d", i), "", 10, 0, 10);
    hSignal2D_Strong[i] = new TH2D(Form("hSignal2D_Strong_%d", i), "", 10, 0, 10, 10, 0, 10);
  }
  for(int i=0;i<nSignal_EWK;i++){
    hSignal1D_EWK[i] = new TH1D(Form("hSignal1D_EWK_%d", i), "", 10, 0, 10);
    hSignal2D_EWK[i] = new TH2D(Form("hSignal2D_EWK_%d", i), "", 10, 0, 10, 10, 0, 10);
  }

  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    TH1D *hSR_TrackPt_CaloVETO0[nMETRegion];
    TH1D *hSR_TrackPt_CaloVETO1[nMETRegion];
    TH1D *hSR_TrackPt_CaloVETO432[nMETRegion];
    TH1D *hFakeCR_TrackPt_CaloVETO0;
    TH1D *hFakeCR_TrackPt_CaloVETO0_MET100;
    TH1D *hFakeCR_MET_CaloVETO0;
    TH1D *hFakeVR_TrackPt_CaloVETO0;
    TH1D *hFakeVR_TrackPt_CaloVETO0_MET100;
    TH1D *hFakeVR_MET_CaloVETO0;
    TH1D *hHadCR_TrackPt[nMETRegion];
    TH1D *hHadCR_TrackPt_withTFSmearing[nMETRegion];
    TH1D *hHadVR_TrackPt_withTFSmearing[nMETRegion];
    TH1D *hHadCR_TrackPt_withTFCorrSmearing[nMETRegion];
    TH1D *hHadVR_TrackPt_withTFCorrSmearing[nMETRegion];
    TH1D *hEleCR_TrackPt[nMETRegion];
    TH1D *hEleCR_TrackPt_withTFSmearing[nMETRegion];
    TH1D *hEleVR_TrackPt_withTFSmearing[nMETRegion];
    TH1D *hEleCR_TrackPt_withTFCorrSmearing[nMETRegion];
    TH1D *hEleVR_TrackPt_withTFCorrSmearing[nMETRegion];
    TH1D *hMuCR_TrackPt[nMETRegion];
    TH1D *hMuCR_TrackPt_withTFSmearing[nMETRegion];
    
    for(int iMET=0;iMET<nMETRegion;iMET++){
      hSR_TrackPt_CaloVETO0[iMET]                   = (TH1D *)tfInput->Get(Form("Chain%d/hSR_TrackPt_CaloVETO0_%d", iChain, iMET));
      hSR_TrackPt_CaloVETO1[iMET]                   = (TH1D *)tfInput->Get(Form("Chain%d/hSR_TrackPt_CaloVETO1_%d", iChain, iMET));
      hSR_TrackPt_CaloVETO432[iMET]                 = (TH1D *)tfInput->Get(Form("Chain%d/hSR_TrackPt_CaloVETO432_%d", iChain, iMET));
      hHadCR_TrackPt[iMET]                    = (TH1D *)tfInput->Get(Form("Chain%d/hHadCR_TrackPt_%d", iChain, iMET));
      hHadCR_TrackPt_withTFSmearing[iMET]     = (TH1D *)tfInput->Get(Form("Chain%d/hHadCR_TrackPt_withTFSmearing_%d", iChain, iMET));
      hHadVR_TrackPt_withTFSmearing[iMET]     = (TH1D *)tfInput->Get(Form("Chain%d/hHadVR_TrackPt_withTFSmearing_%d", iChain, iMET));
      hHadCR_TrackPt_withTFCorrSmearing[iMET] = (TH1D *)tfInput->Get(Form("Chain%d/hHadCR_TrackPt_withTFCorrSmearing_%d", iChain, iMET));
      hHadVR_TrackPt_withTFCorrSmearing[iMET] = (TH1D *)tfInput->Get(Form("Chain%d/hHadVR_TrackPt_withTFCorrSmearing_%d", iChain, iMET));
      hEleCR_TrackPt[iMET]                    = (TH1D *)tfInput->Get(Form("Chain%d/hEleCR_TrackPt_%d", iChain, iMET));
      hEleCR_TrackPt_withTFSmearing[iMET]     = (TH1D *)tfInput->Get(Form("Chain%d/hEleCR_TrackPt_withTFSmearing_%d", iChain, iMET));
      hEleVR_TrackPt_withTFSmearing[iMET]     = (TH1D *)tfInput->Get(Form("Chain%d/hEleVR_TrackPt_withTFSmearing_%d", iChain, iMET));
      hEleCR_TrackPt_withTFCorrSmearing[iMET] = (TH1D *)tfInput->Get(Form("Chain%d/hEleCR_TrackPt_withTFCorrSmearing_%d", iChain, iMET));
      hEleVR_TrackPt_withTFCorrSmearing[iMET] = (TH1D *)tfInput->Get(Form("Chain%d/hEleVR_TrackPt_withTFCorrSmearing_%d", iChain, iMET));
      hMuCR_TrackPt[iMET]                     = (TH1D *)tfInput->Get(Form("Chain%d/hMuCR_TrackPt_%d", iChain, iMET));
      hMuCR_TrackPt_withTFSmearing[iMET]      = (TH1D *)tfInput->Get(Form("Chain%d/hMuCR_TrackPt_withTFSmearing_%d", iChain, iMET));
    }// for iMET
    hFakeCR_TrackPt_CaloVETO0        = (TH1D *)tfInput->Get(Form("Chain%d/hFakeCR_TrackPt_CaloVETO0", iChain));
    hFakeCR_TrackPt_CaloVETO0_MET100 = (TH1D *)tfInput->Get(Form("Chain%d/hFakeCR_TrackPt_CaloVETO0_MET100", iChain));
    hFakeCR_MET_CaloVETO0            = (TH1D *)tfInput->Get(Form("Chain%d/hFakeCR_MET_CaloVETO0", iChain));
    hFakeVR_TrackPt_CaloVETO0        = (TH1D *)tfInput->Get(Form("Chain%d/hFakeVR_TrackPt_CaloVETO0", iChain));
    hFakeVR_TrackPt_CaloVETO0_MET100 = (TH1D *)tfInput->Get(Form("Chain%d/hFakeVR_TrackPt_CaloVETO0_MET100", iChain));
    hFakeVR_MET_CaloVETO0            = (TH1D *)tfInput->Get(Form("Chain%d/hFakeVR_MET_CaloVETO0", iChain));

    int iPtBin = 4;
    double R_Fake_SR = hFakeCR_MET_CaloVETO0->Integral((int)(KinematicsChain[iChain][0])+1, 1001)/hFakeCR_MET_CaloVETO0->Integral(101, 150);
    double R_Fake_VR = hFakeCR_MET_CaloVETO0->Integral(151, 200)/hFakeCR_MET_CaloVETO0->Integral(101, 150);
    //double P_Fake = hFakeCR_TrackPt_CaloVETO0->Integral(iPtBin+1, nLogPt+1)/hFakeCR_TrackPt_CaloVETO0->Integral(1, iPtBin);
    double P_Fake = hFakeCR_TrackPt_CaloVETO0_MET100->Integral(iPtBin+1, nLogPt+1)/hFakeCR_TrackPt_CaloVETO0_MET100->Integral(1, iPtBin);
    double P_Had[nMETRegion];
    double P_Ele[nMETRegion];
    double N_Obs_LowPt[nMETRegion];
    double N_Obs_HighPt[nMETRegion];
    double N_Fake_LowPt[nMETRegion];
    double N_Fake_HighPt[nMETRegion];
    double N_Had_LowPt[nMETRegion];
    double N_Had_HighPt[nMETRegion];
    double N_Ele_LowPt[nMETRegion];
    double N_Ele_HighPt[nMETRegion];
    double N_Mu_LowPt[nMETRegion];
    double N_Mu_HighPt[nMETRegion];
    for(int iMET=0;iMET<nMETRegion;iMET++){
      P_Had[iMET] = hHadCR_TrackPt_withTFCorrSmearing[iMET]->Integral(iPtBin+1, nLogPt+1)/hHadCR_TrackPt_withTFCorrSmearing[iMET]->Integral(1, iPtBin);
      P_Ele[iMET] = hEleCR_TrackPt_withTFCorrSmearing[iMET]->Integral(iPtBin+1, nLogPt+1)/hEleCR_TrackPt_withTFCorrSmearing[iMET]->Integral(1, iPtBin);

      N_Obs_LowPt[iMET]  = hSR_TrackPt_CaloVETO0[iMET]->Integral(1, iPtBin);
      N_Obs_HighPt[iMET] = hSR_TrackPt_CaloVETO0[iMET]->Integral(iPtBin+1, nLogPt+1);

      N_Mu_LowPt[iMET]  = hMuCR_TrackPt_withTFSmearing[iMET]->Integral(iPtBin+1, nLogPt+1);
      N_Mu_HighPt[iMET] = hMuCR_TrackPt_withTFSmearing[iMET]->Integral(1, iPtBin);
    }// for iMET

    N_Fake_LowPt[2] = (N_Obs_HighPt[2] - N_Mu_HighPt[2] - P_Had[2]*(N_Obs_LowPt[2] - N_Mu_LowPt[2]))/(P_Fake - P_Had[2]);
    N_Had_LowPt[2]  = (N_Obs_HighPt[2] - N_Mu_HighPt[2] - P_Fake*(N_Obs_LowPt[2] - N_Mu_LowPt[2]))/(P_Had[2] - P_Fake);
    
    N_Fake_HighPt[2] = P_Fake   * N_Fake_LowPt[2];
    N_Had_HighPt[2]  = P_Had[2] * N_Had_LowPt[2];

    N_Fake_LowPt[0]  = R_Fake_SR * N_Fake_LowPt[2];
    N_Fake_LowPt[1]  = R_Fake_VR * N_Fake_LowPt[2];

    N_Fake_HighPt[0] = R_Fake_SR * N_Fake_HighPt[2];
    N_Fake_HighPt[1] = R_Fake_VR * N_Fake_HighPt[2];

    N_Had_LowPt[0] = N_Obs_LowPt[0] - N_Mu_LowPt[0] - N_Fake_LowPt[0];
    N_Had_LowPt[1] = N_Obs_LowPt[1] - N_Mu_LowPt[1] - N_Fake_LowPt[1];

    N_Had_HighPt[0] = P_Had[0]*N_Had_LowPt[0];
    N_Had_HighPt[1] = P_Had[1]*N_Had_LowPt[1];

    double N_Exp_SR = N_Mu_HighPt[0] + N_Had_HighPt[0] + N_Fake_HighPt[0];
    double N_Exp_VR = N_Mu_HighPt[1] + N_Had_HighPt[1] + N_Fake_HighPt[1];

    std::cout << "**********   Result (Chain" << std::setw(2) << iChain << ")   **********" << std::endl;
    std::cout << std::setw(10) << "Category" << " : " << std::setw(23) << "100 GeV < MET < 150 GeV" << "  |  "                                               << std::setw(23) << "150 GeV < MET < 200 GeV" << "  |  "                                               << std::setw(23) << Form("%d GeV < MET          ", (int)(KinematicsChain[iChain][0]))                   << std::endl;
    std::cout << std::setw(10) << "        " << " : " << std::setw(23) << "  low-pT      high-pT  " << "  |  "                                               << std::setw(23) << "  low-pT      high-pT  " << "  |  "                                               << std::setw(23) << "  low-pT      high-pT  "   << std::endl;
    std::cout << std::setw(10) << "Observed" << " : " << std::setw(6) << Form("%.1lf", N_Obs_LowPt[2])  << std::setw(14) << Form("%.1lf", N_Obs_HighPt[2])   << std::setw(14) << Form("%.1lf", N_Obs_LowPt[1])  << std::setw(14) << Form("%.1lf", N_Obs_HighPt[1])  << std::setw(14) << Form("%.1lf", N_Obs_LowPt[0])  << std::setw(14) << " --- " << std::endl;
    std::cout << std::setw(10) << "Expected" << " : " << std::setw(6) << Form("---")                    << std::setw(14) << Form("---")                      << std::setw(14) << Form("---")                    << std::setw(14) << Form("%.1lf", N_Exp_VR)         << std::setw(14) << Form("---")                    << std::setw(14) << Form("%.1lf", N_Exp_SR)          << std::endl;
    std::cout << std::setw(10) << "Fake BG " << " : " << std::setw(6) << Form("%.1lf", N_Fake_LowPt[2]) << std::setw(14) << Form("%.1lf", N_Fake_HighPt[2])  << std::setw(14) << Form("%.1lf", N_Fake_LowPt[1]) << std::setw(14) << Form("%.1lf", N_Fake_HighPt[1]) << std::setw(14) << Form("%.1lf", N_Fake_LowPt[0]) << std::setw(14) << Form("%.1lf", N_Fake_HighPt[0])  << std::endl;
    std::cout << std::setw(10) << "Had  BG " << " : " << std::setw(6) << Form("%.1lf", N_Had_LowPt[2])  << std::setw(14) << Form("%.1lf", N_Had_HighPt[2])   << std::setw(14) << Form("%.1lf", N_Had_LowPt[1])  << std::setw(14) << Form("%.1lf", N_Had_HighPt[1])  << std::setw(14) << Form("%.1lf", N_Had_LowPt[0])  << std::setw(14) << Form("%.1lf", N_Had_HighPt[0])   << std::endl;
    std::cout << std::setw(10) << "Muon BG " << " : " << std::setw(6) << Form("%.1lf", N_Mu_LowPt[2])   << std::setw(14) << Form("%.1lf", N_Mu_HighPt[2])    << std::setw(14) << Form("%.1lf", N_Mu_LowPt[1])   << std::setw(14) << Form("%.1lf", N_Mu_HighPt[1])   << std::setw(14) << Form("%.1lf", N_Mu_LowPt[0])   << std::setw(14) << Form("%.1lf", N_Mu_HighPt[0])    << std::endl;
    std::cout << std::endl;
  }// for iChain
  std::cout << "myPlot : succeeded to run" << std::endl;

  return 0;  
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}


int FillOverflowBin(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  double LowVal      = h1->GetBinContent(0);
  double LowError    = h1->GetBinError(0);
  double LowOrgVal   = h1->GetBinContent(1);
  double LowOrgError = h1->GetBinError(1);
  double HighVal      = h1->GetBinContent(nBinsX+1);
  double HighError    = h1->GetBinError(nBinsX+1);
  double HighOrgVal   = h1->GetBinContent(nBinsX);
  double HighOrgError = h1->GetBinError(nBinsX);

  h1->SetBinError(1,   TMath::Sqrt(LowError*LowError + LowOrgError*LowOrgError));
  h1->SetBinContent(1, LowVal + LowOrgVal);

  h1->SetBinError(nBinsX, TMath::Sqrt(HighError*HighError + HighOrgError*HighOrgError));
  h1->SetBinContent(nBinsX, HighVal + HighOrgVal);

  return 0;
}

int FillOverflowBinForData(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  h1->AddBinContent(1, h1->GetBinContent(0));
  h1->SetBinContent(0, 0.0);
  h1->AddBinContent(nBinsX  , h1->GetBinContent(nBinsX+1));
  h1->SetBinContent(nBinsX+1, 0.0);

  return 0;
}

int SetupCanvas(int iOption, std::string xTitle, double MinX, double MaxX, double MinY, double MaxY){
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", Form(";%s;Tracks", xTitle.c_str()), 100, MinX, MaxX, 100, MinY, MaxY);
  delete hRatioFrame; hRatioFrame=NULL;
  hRatioFrame = new TH2D("hRatioFrame", Form(";%s;data/MC ", xTitle.c_str()), 100, MinX, MaxX, 100, 0.2, 1.8);

  c0->cd();
  if(fSMBG)
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.35, 1.0, 1.0);
  else
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
  pad1[1] = new TPad("pad1_1", "pad1_1", 0.0, 0.0, 1.0, 0.35);

  hFrame->GetXaxis()->SetNoExponent(true);
  
  pad1[0]->SetLogy(false);
  pad1[0]->SetLogx(false);
  if(fSMBG){
    hFrame->GetYaxis()->SetTitleOffset(0.6);
    hFrame->GetYaxis()->SetTitleSize(0.07);
    hFrame->GetYaxis()->SetLabelSize(0.07);
    pad1[0]->SetBottomMargin(0);
    pad1[0]->SetLeftMargin(0.12);
    pad1[1]->SetTopMargin(0);
    pad1[1]->SetLeftMargin(0.12);
    pad1[1]->SetBottomMargin(0.35);
  }else{
    hFrame->GetYaxis()->SetTitleOffset(0.8);
    hFrame->GetYaxis()->SetTitleSize(0.06);
    hFrame->GetYaxis()->SetLabelSize(0.06);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.05);
  }
  hRatioFrame->GetXaxis()->SetLabelSize(0.15);
  hRatioFrame->GetXaxis()->SetTitleSize(0.15);
  hRatioFrame->GetXaxis()->SetTitleOffset(1.0);
  hRatioFrame->GetYaxis()->SetLabelSize(0.13);
  hRatioFrame->GetYaxis()->SetTitleSize(0.13);
  hRatioFrame->GetYaxis()->SetTitleOffset(0.3);
  hRatioFrame->GetYaxis()->SetNdivisions(505);
  hRatioFrame->GetXaxis()->SetNoExponent(true);

  if(iOption==0){
    // right top : data+signal+bg
    delete tl;
    tl = new TLegend(0.20, 0.93-0.18, 0.82, 0.93-0.10);
    //    tl = new TLegend(0.30, 0.93-0.18, 0.92, 0.93-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1, 0.9, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==1){
    // right top : signal+bg or data+bg
    delete tl;
    tl = new TLegend(0.60, 0.94-0.18, 0.92, 0.94-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1, 0.9, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==2){
    // left top : data+bg
    delete tl;
    tl = new TLegend(0.50-0.35, 0.94-0.18, 0.92-0.35, 0.94-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50-0.35, 0.94-0.1, 0.9-0.35, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==3){
    // right bottom : data+bg
    delete tl;
    tl = new TLegend(0.50, 0.94-0.18-0.12, 0.92, 0.94-0.10-0.12);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1-0.12, 0.90, 0.94-0.12);
    tlSM->SetNColumns(5);
  }

  return 0;
}


int DrawText(double xPos, double yPos, double iLumi, std::string myText1, std::string myText2){
  ATLASLabel(xPos, yPos, PlotStatus.c_str(), kBlack);
  myText(xPos, yPos-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", iLumi));
  myText(xPos, yPos-0.12, kBlack, Form("%s", myText1.c_str()));
  myText(xPos, yPos-0.18, kBlack, Form("%s", myText2.c_str()));
  tl->Draw("same");
  tlSM->Draw("same");

  return 0;
}
