#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TLegend.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

bool fUpdateNote=true;
bool fMaskPt=true;
double MaskPt=20.0;
TFile *tfTemplate;
bool fSMBG=false;
bool fSignal=false;
TH2D *hHogeHoge;
TH2D *hPtEta_Bin18;
TH2D *hPtEta_Bin10;
std::ofstream ofsSMFrac;
//=== Smearing function parameters ===//
const double g_par_Mean  = -4.50114e-01;
const double g_par_Sigma =  1.35062e+01;
const double g_par_Alpha =  1.68961e+00;

const std::string PlotStatus="Internal";
const int nData=1;
const std::string DataFile[4]={"/gpfs/fs6001/toshiaki/MyProject/MakeMyAna/data.root", "/data3/tkaji/myAna/Data/data15-16_13TeV.root", 
			       "/data3/tkaji/myAna/Data/data17_13TeV.root"   , "/data3/tkaji/myAna/Data/data18_13TeV.root"};
const std::string AllDataTag = "Data 2015-2018";
const std::string DataTag[4]={"Data 2015-2018", "Data 2015-2016", "Data 2017", "Data 2018"};
const int ColData[4] = {EColor::kBlack, EColor::kRed, EColor::kGreen+1, EColor::kBlue};

const int nSM = 10;
const int ColZnunu     = EColor::kYellow-7;
const int ColWmunu     = EColor::kBlue;
const int ColWenu      = EColor::kRed-4;
const int ColWtaunu    = EColor::kGreen+1 ;
const int ColZee       = EColor::kRed-10;
const int ColZmumu     = EColor::kAzure+6 ;
const int ColDiBoson   = EColor::kViolet;
const int ColZtautau   = EColor::kSpring-4 ;
const int ColTop       = EColor::kOrange+2;
const int Colmultijets = EColor::kOrange;
const int ColSM[nSM]   = {ColZnunu, ColWmunu, ColWenu, ColWtaunu, ColZee, ColZmumu, ColZtautau, ColDiBoson, ColTop, Colmultijets};
const std::string SMName[nSM] = {"Z#nu#nu",  "W#mu#nu",  "We#nu",  "W#tau#nu",  "Zee",  "Z#mu#mu", "Z#tau#tau",  "VV",  "t,t#bar{t}",  "dijet"};
const int Col5[5]   = {ColWmunu, ColWenu, ColWtaunu, ColDiBoson, ColTop};

const int nMC16 = 3;
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
std::vector<int> vDSID_multijets[nMC16]  = { {364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712},
					     {364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712},
					     {364700, 364701, 364702, 364703, 364704, 364705, 364706, 364707, 364708, 364709, 364710, 364711, 364712}};
std::vector<int> vDSID_Zee[nMC16]        = { {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127},
					     {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127},
					     {364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127}};
std::vector<int> vDSID_Zmumu[nMC16]      = { {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113},
					     {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113},
					     {364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113}};
std::vector<int> vDSID_Ztautau[nMC16]    = { {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141},
					     {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141},
					     {364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141}};
std::vector<int> vDSID_Znunu[nMC16]      = { {364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155},
					     {364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155},
					     {364222, 364223, 366010, 366011, 366012, 366013, 366014, 366015, 366016, 366017, 366019, 366020, 366021, 366022, 366023, 366024, 366025, 366026, 366028, 366029, 366030, 366031, 366032, 366033, 366034, 366035}};
double CorrectFactor[26] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

/*
double CorrectFactor[26] = {1.0, 1.0, 20.609148/232.03, 9.7086809/85.26, 0.55247617/4.5536, 0.13345916/1.2026, 5.1418320/42.624, 0.55962648/4.4689, 0.16006654/1.3752, 0.47099694/3.5485,
			    55.787010/232.03, 23.524331/85.26, 1.2138293/4.5536, 0.32278895/1.2026, 11.788607/42.624, 1.2117525/4.4689, 0.37889255/1.3752, 1.0558753/3.5485, 189.56639/232.03,
			    73.381419/85.26, 2.6724912/4.5536, 0.71441256/1.2026, 33.482713/42.624, 2.5776784/4.4689, 0.80224970/1.3752, 2.6069673/3.5485};
*/
std::vector<int> vDSID_Wmunu[nMC16]      = { {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169},
					     {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169},
					     {364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169}};
std::vector<int> vDSID_Wenu[nMC16]       = { {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183},
					     {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183},
					     {364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183}};
std::vector<int> vDSID_Wtaunu[nMC16]     = { {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197},
					     {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197},
					     {364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197}};
std::vector<int> vDSID_Top[nMC16]        = { {410470, 410471, 410644, 410645, 410646, 410647},
					     {410470, 410471, 410644, 410645, 410646, 410647},
					     {410470, 410471, 410644, 410645, 410646, 410647}};
std::vector<int> vDSID_DiBoson[nMC16]    = { {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305},
					     {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305},
					     {345705, 345706, 345715, 345718, 345723, 363355, 363356, 363357, 363358, 363359, 363360, 363489, 363494, 364250, 364253, 364254, 364255, 364283, 364284, 364285, 364286, 364287, 364302, 364303, 364304, 364305}};

const std::string MCDir[nMC16]={"/data3/tkaji/myAna/SMBG/mc16a_link", "/data3/tkaji/myAna/SMBG/mc16d_link", "/data3/tkaji/myAna/SMBG/mc16e_link"};
const double IntLumi[nMC16]={2798.72 + 31260.7, 44083.8, 58125.9}; // pb-1
//const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];

MyManager *myData[nData];
HistManager *myHistData[nData];
MyManager *myTest[50][nData];
HistManager *myHistTest[50][nData];

std::vector<MyManager *> *myZnunu[nMC16];
std::vector<MyManager *> *myWmunu[nMC16];
std::vector<MyManager *> *myWenu[nMC16];
std::vector<MyManager *> *myWtaunu[nMC16];
std::vector<MyManager *> *myZee[nMC16];
std::vector<MyManager *> *myZmumu[nMC16];
std::vector<MyManager *> *myZtautau[nMC16];
std::vector<MyManager *> *myDiBoson[nMC16];
std::vector<MyManager *> *myTop[nMC16];
std::vector<MyManager *> *mymultijets[nMC16];
std::vector<MyManager *> *mySM[nSM][nMC16];

std::vector<HistManager *> *myHistZnunu[nMC16];
std::vector<HistManager *> *myHistWmunu[nMC16];
std::vector<HistManager *> *myHistWenu[nMC16];
std::vector<HistManager *> *myHistWtaunu[nMC16];
std::vector<HistManager *> *myHistZee[nMC16];
std::vector<HistManager *> *myHistZmumu[nMC16];
std::vector<HistManager *> *myHistZtautau[nMC16];
std::vector<HistManager *> *myHistDiBoson[nMC16];
std::vector<HistManager *> *myHistTop[nMC16];
std::vector<HistManager *> *myHistmultijets[nMC16];
std::vector<HistManager *> *myHistSM[nSM][nMC16];

const int nSignal_Strong=0;
const int nSignal_EWK=0;
MyManager *mySignal_Strong[nMC16][3];
HistManager *myHistSignal_Strong[nMC16][3];

bool doSSRemoval;
TH1D *hSignal1D_Strong[3];
TH2D *hSignal2D_Strong[3];
TH1D *hSignal1D_EWK[3];
TH2D *hSignal2D_EWK[3];
std::vector<MyManager *> *mySignal_EWK[nMC16][3];
std::vector<HistManager *> *myHistSignal_EWK[nMC16][3];

TFile *tfOut;
bool fSimpleSmear=true;
int RebinVal=1;
TCanvas *c0;
TPad *pad0;
TPad *pad1[2];
TF1 *fSmearInt;
//TH1D *hData=NULL;
TLegend *tl;
TLegend *tlSM;
THStack *hStack_SM=NULL;
TH1D *h1000;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTemplate_MET=NULL;
TH1D *hTemplate_Calo=NULL;
TH1D *hData1D=NULL;
TH2D *hData2D=NULL;

TH2D *hFrame;
TH2D *hRatioFrame;
std::string PDFName;
int DrawRatioPlot(void);
double CalcFracError(double vNume, double vDenom, double eNume, double eDenom);
int DrawTH1RatioPlot(std::string HistName, int iHist, int nRebin=1, int iOpt=0, int SameOpt=0, bool fEWK=false);
int DrawChainTH1RatioPlot(std::string HistName, int iChain, int iHist, int nRebin=1, int iOpt=0);
int DrawTH1RatioPlot(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist=NULL, double Scale=1.0, std::string myCut="1", int iOpt=0, int SameOpt=0, bool fEWK=false);
double crystallBallIntegral(double* x, double *par);
void SmearPt(TH1D *hHoge, double pt, double);
THStack *GetHStack(std::string HistName, int iChain, int iHist, int nRegin=1, int iOpt=0);
THStack *GetHStack(std::string TreeName, std::string BranchName, std::string ValName, double Scale=1.0, std::string myCut="1", int iOpt=0);
int FillOverflowBin(TH1D *h1);
int FillOverflowBinForData(TH1D *h1);
int SetupCanvas(int iOption=0);
int Rebin1D_For10Bin(TH1D *h10Bin, TH1D *hOS, TH1D *hSS);
int Rebin2D_For10Bin(TH2D *h10Bin, TH2D *hOS, TH2D *hSS);
int Rebin2D_For18Bin(TH2D *h18Bin, TH2D *hOS, TH2D *hSS);

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("doSSRemoval", po::value<bool>()->default_value(true), "ss removal option for TF calculation")
    ("output-file,o", po::value<std::string>()->default_value("MakeInput"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  doSSRemoval = vm["doSSRemoval"].as<bool>();

  for(int iData=0;iData<nData;iData++){
    myData[iData] = new MyManager(DataFile[iData], DataTag[iData]);
    myData[iData]->LoadMyFile();
    myHistData[iData] = myData[iData]->myHist_Old4L;
  }

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".0lf");
  gErrorIgnoreLevel = 1001;
  fSmearInt = new TF1("fSmearInt", [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
  fSmearInt->SetParameter(0, g_par_Mean);
  fSmearInt->SetParameter(1, g_par_Sigma);
  fSmearInt->SetParameter(2, g_par_Alpha);

  hPtEta_Bin10 = new TH2D("hPtEta_Bin10", ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  hPtEta_Bin18 = new TH2D("hPtEta_Bin18", ";p_{T} [GeV];#eta", 18, XBinsLogPt18ForLepton, 25, -2.5, 2.5);

  ofsSMFrac.open("ofsSMFrac.txt");
  /* **********   Load SM MC   ********** */
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    if(fSMBG==false)
      continue;

    // Znunu
    myZnunu[iMC16] = new std::vector<MyManager *>;
    myHistZnunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Znunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Znunu[iMC16].at(iDSID)), "Znunu");
      myDS->LoadMyFile();
      myZnunu[iMC16]->push_back(myDS);
      myHistZnunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID
    
    // Znunu
    myZnunu[iMC16] = new std::vector<MyManager *>;
    myHistZnunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Znunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Znunu[iMC16].at(iDSID)), "Znunu");
      myDS->LoadMyFile();
      myZnunu[iMC16]->push_back(myDS);
      myHistZnunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Wmunu
    myWmunu[iMC16] = new std::vector<MyManager *>;
    myHistWmunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Wmunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Wmunu[iMC16].at(iDSID)), "Wmunu");
      myDS->LoadMyFile();
      myWmunu[iMC16]->push_back(myDS);
      myHistWmunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Wenu
    myWenu[iMC16] = new std::vector<MyManager *>;
    myHistWenu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Wenu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Wenu[iMC16].at(iDSID)), "Wenu");
      myDS->LoadMyFile();
      myWenu[iMC16]->push_back(myDS);
      myHistWenu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Wtaunu
    myWtaunu[iMC16] = new std::vector<MyManager *>;
    myHistWtaunu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Wtaunu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Wtaunu[iMC16].at(iDSID)), "Wtaunu");
      myDS->LoadMyFile();
      myWtaunu[iMC16]->push_back(myDS);
      myHistWtaunu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Zee
    myZee[iMC16] = new std::vector<MyManager *>;
    myHistZee[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Zee[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Zee[iMC16].at(iDSID)), "Zee");
      myDS->LoadMyFile();
      myZee[iMC16]->push_back(myDS);
      myHistZee[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Zmumu
    myZmumu[iMC16] = new std::vector<MyManager *>;
    myHistZmumu[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Zmumu[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Zmumu[iMC16].at(iDSID)), "Zmumu");
      myDS->LoadMyFile();
      myZmumu[iMC16]->push_back(myDS);
      myHistZmumu[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Ztautau
    myZtautau[iMC16] = new std::vector<MyManager *>;
    myHistZtautau[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Ztautau[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Ztautau[iMC16].at(iDSID)), "Ztautau");
      myDS->LoadMyFile();
      myZtautau[iMC16]->push_back(myDS);
      myHistZtautau[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // DiBoson
    myDiBoson[iMC16] = new std::vector<MyManager *>;
    myHistDiBoson[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_DiBoson[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_DiBoson[iMC16].at(iDSID)), "DiBoson");
      myDS->LoadMyFile();
      myDiBoson[iMC16]->push_back(myDS);
      myHistDiBoson[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // Top
    myTop[iMC16] = new std::vector<MyManager *>;
    myHistTop[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_Top[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_Top[iMC16].at(iDSID)), "Top");
      myDS->LoadMyFile();
      myTop[iMC16]->push_back(myDS);
      myHistTop[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    // multijets
    mymultijets[iMC16] = new std::vector<MyManager *>;
    myHistmultijets[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_multijets[iMC16].size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/link_%d", MCDir[iMC16].c_str(), vDSID_multijets[iMC16].at(iDSID)), "multijets");
      myDS->LoadMyFile();
      mymultijets[iMC16]->push_back(myDS);
      myHistmultijets[iMC16]->push_back(myDS->myHist_Old4L);
    }// for iDSID

    mySM[0][iMC16] = myZnunu[iMC16];
    mySM[1][iMC16] = myWmunu[iMC16];
    mySM[2][iMC16] = myWenu[iMC16];
    mySM[3][iMC16] = myWtaunu[iMC16];
    mySM[4][iMC16] = myZee[iMC16];
    mySM[5][iMC16] = myZmumu[iMC16];
    mySM[6][iMC16] = myZtautau[iMC16];
    mySM[7][iMC16] = myDiBoson[iMC16];
    mySM[8][iMC16] = myTop[iMC16];
    mySM[9][iMC16] = mymultijets[iMC16];

    myHistSM[0][iMC16] = myHistZnunu[iMC16];
    myHistSM[1][iMC16] = myHistWmunu[iMC16];
    myHistSM[2][iMC16] = myHistWenu[iMC16];
    myHistSM[3][iMC16] = myHistWtaunu[iMC16];
    myHistSM[4][iMC16] = myHistZee[iMC16];
    myHistSM[5][iMC16] = myHistZmumu[iMC16];
    myHistSM[6][iMC16] = myHistZtautau[iMC16];
    myHistSM[7][iMC16] = myHistDiBoson[iMC16];
    myHistSM[8][iMC16] = myHistTop[iMC16];
    myHistSM[9][iMC16] = myHistmultijets[iMC16];
  }// for iMC16

  tl = new TLegend(0.5, 0.4, 0.8, 0.7);
  tlSM = new TLegend(0.5, 0.4, 0.8, 0.7);
  c0 = new TCanvas("c0", "c0", 800, 600);

  hFrame = new TH2D("hFrame", "", 10, 0, 10, 10, 0, 10);
  hStack_SM = new THStack("hStack_SM", "");
  h1000 = new TH1D("h1000", "", 1000, 0, 1000);
  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nLogPt, XBinsLogPt);
  hTemplate_MET = new TH1D("hTemplate_MET", ";MET [GeV];Tracks", 1000, 0, 1000);
  hTemplate_Calo = new TH1D("hTemplate_Calo", ";etclus20_topo [GeV];Tracks", 50, 0, 50);
  hData1D = new TH1D("hData1D", "hoge", 10, 0, 10);
  hData2D = new TH2D("hData2D", "hoge", 10, 0, 10, 10, 0, 10);

  tfOut = new TFile(Form("%s.root", PDFName.c_str()), "RECREATE");
  tfOut->mkdir("METTrigEff");
  gROOT->cd();

  // plot : MET Trigger Efficiency
  //for(int iChain=0;iChain<nKinematicsChain;iChain++){
  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    c0->Print(Form("METTrigEff_Chain%d.pdf[", iChain), "pdf");
    std::cout << "plot : MET Trigger Efficiency for Chain" << iChain << std::endl;
    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";E^{miss}_{T} (#mu-subtracted) [GeV];Efficiency", 100, 0, 500, 55, 0, 1.1);
    hFrame->GetYaxis()->SetTitleOffset(0.8);
    TH1D *hTrigMETEff[nMETTrig];
    TH1D *hTrigMETEff_Passed[nMETTrig];
    TGraphAsymmErrors *gTrigMETEff[nMETTrig];
    TH2D *hTrigMETJetEff[nMETTrig];
    c0->cd();
    pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
    pad0->SetLeftMargin(0.12);
    pad0->SetRightMargin(0.05);
    pad0->Draw();
    pad0->cd();
    delete tl;
    tl = new TLegend(0.6, 0.4, 0.9, 0.85);
    for(int iTrig=0;iTrig<nMETTrig;iTrig++){
      hTrigMETEff[iTrig] = myHistData[0]->hTrigMETEff_1stJetMET[iChain][iTrig]->ProjectionX(Form("hTrigMETEff_Chain%d_%d", iChain, iTrig), ((int)(KinematicsChain[iChain][1]/10))+1, 101);
      hTrigMETEff_Passed[iTrig] = myHistData[0]->hTrigMETEff_1stJetMET_Passed[iChain][iTrig]->ProjectionX(Form("hTrigMETEff_Passed_Chain%d_%d", iChain, iTrig), ((int)(KinematicsChain[iChain][1]/10))+1, 101);
      gTrigMETEff[iTrig] = new TGraphAsymmErrors(hTrigMETEff_Passed[iTrig], hTrigMETEff[iTrig]);
      gTrigMETEff[iTrig]->SetMarkerSize(1.0);
      gTrigMETEff[iTrig]->SetMarkerStyle(8);
      gTrigMETEff[iTrig]->SetMarkerColor(iTrig+1);
      gTrigMETEff[iTrig]->SetLineColor(iTrig+1);
      tl->AddEntry(gTrigMETEff[iTrig], METTrigName[iTrig].c_str(), "PE");

      hTrigMETJetEff[iTrig] = new TH2D(Form("hTrigMETJetEff_Chain%d_%d", iChain, iTrig), ";E^{miss}_{T} (#mu-subtracted) [GeV];1st jet p_{T} [GeV]",
				       50, 0.0, 500.0, 50, 0.0, 500);      
      for(int iX=0;iX<=50;iX++){
	for(int iY=0;iY<50;iY++){
	  if(iX==50){
	    hTrigMETJetEff[iTrig]->SetBinContent(iX+1, iY+1, myHistData[0]->hTrigMETEff_1stJetMET_Passed[iChain][iTrig]->Integral(iX+1, 101, iY+1, 101)/myHistData[0]->hTrigMETEff_1stJetMET[iChain][iTrig]->Integral(iX+1, 101, iY+1, 101));
	  }else{
	    hTrigMETJetEff[iTrig]->SetBinContent(iX+1, iY+1, myHistData[0]->hTrigMETEff_1stJetMET_Passed[iChain][iTrig]->Integral(iX+1, iX+1, iY+1, 101)/myHistData[0]->hTrigMETEff_1stJetMET[iChain][iTrig]->Integral(iX+1, iX+1, iY+1, 101));
	  }
	}// for iY
      }// for iX
    }// for iTrig

    hFrame->Draw();
    for(int iTrig=0;iTrig<nMETTrig;iTrig++){
      gTrigMETEff[iTrig]->Draw("sameEP");
    }
    tl->Draw("same");
    ATLASLabel(0.60, 0.30, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.30-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.30-0.12, kBlack, "");
    c0->Print(Form("METTrigEff_Chain%d.pdf", iChain), "pdf");
    if(iChain==1 && fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/Datasets/METTrigEff_ewk.pdf", "pdf");
    if(iChain==3 && fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/Datasets/METTrigEff_strong.pdf", "pdf");
    delete pad0;pad0=NULL;

    pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
    pad0->SetLeftMargin(0.12);
    pad0->SetRightMargin(0.15);
    pad0->Draw();
    pad0->cd();
    tfOut->cd();
    tfOut->cd("METTrigEff");
    for(int iTrig=0;iTrig<nMETTrig;iTrig++){
      hTrigMETJetEff[iTrig]->Draw("colz0");
      hTrigMETJetEff[iTrig]->Write();      
      c0->Print(Form("METTrigEff_Chain%d.pdf", iChain), "pdf");
    }
    tfOut->cd("../");
    gROOT->cd();
    delete pad0;pad0=NULL;

    // plot : mT for MET Trigger Efficiency
    std::cout << "plot : mT for MET Trigger Efficiency Chain" << iChain << std::endl;
    for(int iTrig=0;iTrig<nMETTrig;iTrig++){
      delete hFrame; hFrame=NULL;
      hFrame = new TH2D("hFrame", ";m_{T}(#mu, MET) [GeV];Events", 100, 0.0, 250, 100, 0.0, 2e5);
      delete hRatioFrame; hRatioFrame=NULL;
      hRatioFrame = new TH2D("hRatioFrame", ";m_{T}(#mu, MET) [GeV];Events", 100, 0, 250, 100, 0.2, 1.8);
      SetupCanvas();    
      DrawChainTH1RatioPlot("hTrigMETEff_mT", iChain, iTrig, 1, 0);
      hFrame->GetYaxis()->SetRangeUser(0.0, hData1D->GetMaximum()*1.2);
      ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
      myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
      myText(0.60, 0.90-0.12, kBlack, "");
      c0->Print(Form("METTrigEff_Chain%d.pdf", iChain), "pdf");

      delete pad1[0]; pad1[0]=NULL;
      delete pad1[1]; pad1[1]=NULL;      
    }// for i  
    c0->Print(Form("METTrigEff_Chain%d.pdf]", iChain), "pdf");
  }// for iChain

  /* **********   Common Part    ********** */
  std::cout << "***   Common Part   ***" << std::endl;
  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");

  TH1D *hnEvents = new TH1D("hnEvents", ";;number of events", 3, 0, 3);
  /*
  hnEvents->GetXaxis()->SetBinLabel(1, "data15-16");
  hnEvents->GetXaxis()->SetBinLabel(2, "data17");
  hnEvents->GetXaxis()->SetBinLabel(3, "data18");
  hnEvents->SetBinContent(1, myHistData[1]->hnEvents->GetBinContent(1));
  hnEvents->SetBinContent(2, myHistData[2]->hnEvents->GetBinContent(1));
  hnEvents->SetBinContent(3, myHistData[3]->hnEvents->GetBinContent(1));
  hnEvents->SetMarkerSize(1.5);
  hnEvents->GetYaxis()->SetRangeUser(1.3e9, 1.7e9);
  hnEvents->Draw("texthist");
  std::cout << " data15-16 : " << Form("%.0lf", hnEvents->GetBinContent(1)) << std::endl;
  std::cout << " data17    : " << Form("%.0lf", hnEvents->GetBinContent(2)) << std::endl;
  std::cout << " data18    : " << Form("%.0lf", hnEvents->GetBinContent(3)) << std::endl;
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  */
  /*   Nvtx   */
  // plot : Nvtx_Chain1
  std::cout << "plot : Nvtx_Chain1" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";N_{vtx};Events", 100, 0, 70, 100, 5e-1, 1e9);
  hRatioFrame = new TH2D("hRatioFrame", Form(";N_{vtx};data/MC "), 100, 0, 70, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=1;
  DrawTH1RatioPlot("hNvtx_Chain1", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : NvtxReweighted_Chain1
  std::cout << "plot : NvtxReweighted_Chain1" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";N_{vtx};Events", 100, 0, 70, 100, 5e-1, 1e9);
  hRatioFrame = new TH2D("hRatioFrame", Form(";N_{vtx};data/MC "), 100, 0, 70, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=1;
  DrawTH1RatioPlot("hNvtxReweighted_Chain1", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Nvtx_Chain3
  std::cout << "plot : Nvtx_Chain3" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";N_{vtx};Events", 100, 0, 70, 100, 5e-1, 1e9);
  hRatioFrame = new TH2D("hRatioFrame", Form(";N_{vtx};data/MC "), 100, 0, 70, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=1;
  DrawTH1RatioPlot("hNvtx_Chain3", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : NvtxReweighted_Chain3
  std::cout << "plot : NvtxReweighted_Chain3" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";N_{vtx};Events", 100, 0, 70, 100, 5e-1, 1e9);
  hRatioFrame = new TH2D("hRatioFrame", Form(";N_{vtx};data/MC "), 100, 0, 70, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=1;
  DrawTH1RatioPlot("hNvtxReweighted_Chain3", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  /* **********   Electrong BG   ********** */
  // plot : Electron Background Zmass Basic
  std::cout << "plot : Electron Background Zmass Basic" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  //hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e7);
  hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e2, 1e7);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=2;
  DrawTH1RatioPlot("hOld4LTracks_EleBG_Basic_ZMass", -1, RebinVal, 0, 1);
  //hFrame->GetYaxis()->SetRangeUser(5e2, 1e7);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_TrackDenom.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Electron Background Zmass Basic OS
  std::cout << "plot : Electron Background Zmass Basic OS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  //hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e7);
  hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e2, 1e7);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=2;
  DrawTH1RatioPlot("hOld4LTracks_EleBG_BasicOS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_TrackDenomOS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Electron Background Zmass Basic SS
  std::cout << "plot : Electron Background Zmass Basic SS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e0, 1e5);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  RebinVal=2;
  DrawTH1RatioPlot("hOld4LTracks_EleBG_BasicSS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_TrackDenomSS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Electron Background Zmass Disap
  std::cout << "plot : Electron Background Zmass Disap" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 2e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_EleBG_Disap_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_TrackNume.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Electron Background Zmass Disap OS
  std::cout << "plot : Electron Background Zmass Disap OS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 2e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_EleBG_DisapOS_ZMass", -1, RebinVal, 0, 1);
  hFrame->GetYaxis()->SetRangeUser(5e-1, 2e4);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_TrackNumeOS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Electron Background Zmass Disap SS
  std::cout << "plot : Electron Background Zmass Disap SS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 2e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_EleBG_DisapSS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_TrackNumeSS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogx(true);
  pad0->SetLogz(true);

  // plot : EleBG TF(disap) PtEta
  TH2D *hEleBG_BasicOS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_BasicOS_PtEta",-1)->Clone("hEleBG_BasicOS_PtEta");
  TH2D *hEleBG_BasicSS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_BasicSS_PtEta",-1)->Clone("hEleBG_BasicSS_PtEta");
  //TH2D *hEleBG_Basic_PtEta    = new TH2D("hEleBG_Basic_PtEta", ";probe p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_Basic_PtEta    = (TH2D *)hPtEta_Bin18->Clone("hEleBG_Basic_PtEta");
  Rebin2D_For18Bin(hEleBG_Basic_PtEta, hEleBG_BasicOS_PtEta, hEleBG_BasicSS_PtEta);
  /*
  for(int i=0;i<(hEleBG_Basic_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_Basic_PtEta->GetNbinsY());j++){
      double origE = hEleBG_BasicOS_PtEta->GetBinError(i+1, j+1);
      double origV = hEleBG_BasicOS_PtEta->GetBinContent(i+1, j+1);
      double addE = hEleBG_BasicSS_PtEta->GetBinError(i+1, j+1);
      double addV = hEleBG_BasicSS_PtEta->GetBinContent(i+1, j+1);
      hEleBG_Basic_PtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hEleBG_Basic_PtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  */
  hEleBG_Basic_PtEta->GetYaxis()->SetTitleOffset(0.9);  
  hEleBG_Basic_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hEleBG_DisapOS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_DisapOS_PtEta",-1)->Clone("hEleBG_DisapOS_PtEta");
  TH2D *hEleBG_DisapSS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_DisapSS_PtEta",-1)->Clone("hEleBG_DisapSS_PtEta");
  //TH2D *hEleBG_Disap_PtEta = new TH2D("hEleBG_Disap_PtEta", ";probe p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_Disap_PtEta = (TH2D *)hPtEta_Bin18->Clone("hEleBG_Disap_PtEta");
  Rebin2D_For18Bin(hEleBG_Disap_PtEta, hEleBG_DisapOS_PtEta, hEleBG_DisapSS_PtEta);
  /*
  for(int i=0;i<(hEleBG_Disap_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_Disap_PtEta->GetNbinsY());j++){
      double origE = hEleBG_DisapOS_PtEta->GetBinError(i+1, j+1);
      double origV = hEleBG_DisapOS_PtEta->GetBinContent(i+1, j+1);
      double addE = hEleBG_DisapSS_PtEta->GetBinError(i+1, j+1);
      double addV = hEleBG_DisapSS_PtEta->GetBinContent(i+1, j+1);
      hEleBG_Disap_PtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hEleBG_Disap_PtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  */
  hEleBG_Disap_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_Disap_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hEleBG_TF_PtEta = (TH2D *)hEleBG_Basic_PtEta->Clone("hEleBG_TF_PtEta");
  TH2D *hEleBG_TF_PtEta_Err = (TH2D *)hEleBG_Basic_PtEta->Clone("hEleBG_TF_PtEta_Err");
  hEleBG_TF_PtEta->Reset();
  hEleBG_TF_PtEta_Err->Reset();
  for(int i=0;i<(hEleBG_TF_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_TF_PtEta->GetNbinsY());j++){
      double V1 = hEleBG_Basic_PtEta->GetBinContent(i+1, j+1);
      double V2 = hEleBG_Disap_PtEta->GetBinContent(i+1, j+1);
      double E1 = hEleBG_Basic_PtEta->GetBinError(i+1, j+1);
      double E2 = hEleBG_Disap_PtEta->GetBinError(i+1, j+1);
      if(V1>0){
	hEleBG_TF_PtEta->SetBinContent(i+1, j+1, V2/V1);
	hEleBG_TF_PtEta->SetBinError(i+1, j+1, CalcFracError(V2, V1, E2, E1));
	hEleBG_TF_PtEta_Err->SetBinContent(i+1, j+1, CalcFracError(V2, V1, E2, E1));

      }
    }
  }
  hEleBG_TF_PtEta->GetZaxis()->SetRangeUser(1e-4,1.0);
  hEleBG_TF_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_Track.pdf", "pdf");
  hEleBG_TF_PtEta_Err->GetZaxis()->SetRangeUser(1e-4,1.0);
  hEleBG_TF_PtEta_Err->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_PtEta_Err->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_Track_Err.pdf", "pdf");

  // plot : EleBG TF(disap) InvPtEta
  pad0->SetLogx(false);
  TH2D *hEleBG_BasicOS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_BasicOS_InvPtEta",-1)->Clone("hEleBG_BasicOS_InvPtEta");
  TH2D *hEleBG_BasicSS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_BasicSS_InvPtEta",-1)->Clone("hEleBG_BasicSS_InvPtEta");
  TH2D *hEleBG_Basic_InvPtEta    = new TH2D("hEleBG_Basic_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);

  for(int i=0;i<(hEleBG_Basic_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_Basic_InvPtEta->GetNbinsY());j++){
      double origE = hEleBG_BasicOS_InvPtEta->GetBinError(i+1, j+1);
      double origV = hEleBG_BasicOS_InvPtEta->GetBinContent(i+1, j+1);
      double addE = hEleBG_BasicSS_InvPtEta->GetBinError(i+1, j+1);
      double addV = hEleBG_BasicSS_InvPtEta->GetBinContent(i+1, j+1);
      hEleBG_Basic_InvPtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hEleBG_Basic_InvPtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hEleBG_Basic_InvPtEta->GetYaxis()->SetTitleOffset(0.9);  
  hEleBG_Basic_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hEleBG_DisapOS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_DisapOS_InvPtEta",-1)->Clone("hEleBG_DisapOS_InvPtEta");
  TH2D *hEleBG_DisapSS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_EleBG_DisapSS_InvPtEta",-1)->Clone("hEleBG_DisapSS_InvPtEta");
  TH2D *hEleBG_Disap_InvPtEta = new TH2D("hEleBG_Disap_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  hEleBG_Disap_InvPtEta->Reset();
  for(int i=0;i<(hEleBG_Disap_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_Disap_InvPtEta->GetNbinsY());j++){
      double origE = hEleBG_DisapOS_InvPtEta->GetBinError(i+1, j+1);
      double origV = hEleBG_DisapOS_InvPtEta->GetBinContent(i+1, j+1);
      double addE = hEleBG_DisapSS_InvPtEta->GetBinError(i+1, j+1);
      double addV = hEleBG_DisapSS_InvPtEta->GetBinContent(i+1, j+1);
      hEleBG_Disap_InvPtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hEleBG_Disap_InvPtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hEleBG_Disap_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_Disap_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hEleBG_TF_InvPtEta = (TH2D *)hEleBG_Basic_InvPtEta->Clone("hEleBG_TF_InvPtEta");
  hEleBG_TF_InvPtEta->Reset();
  for(int i=0;i<(hEleBG_TF_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_TF_InvPtEta->GetNbinsY());j++){
      double V1 = hEleBG_Basic_InvPtEta->GetBinContent(i+1, j+1);
      double V2 = hEleBG_Disap_InvPtEta->GetBinContent(i+1, j+1);
      double E1 = hEleBG_Basic_InvPtEta->GetBinError(i+1, j+1);
      double E2 = hEleBG_Disap_InvPtEta->GetBinError(i+1, j+1);
      if(V1>0){
	hEleBG_TF_InvPtEta->SetBinContent(i+1, j+1, V2/V1);
	hEleBG_TF_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V2, V1, E2, E1));
      }
    }
  }
  hEleBG_TF_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  //  if(fUpdateNote)
  //c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_Track.pdf", "pdf");
  delete pad0;pad0=NULL;
  tfOut->cd();
  hEleBG_TF_PtEta->Write();
  hEleBG_TF_InvPtEta->Write();
  gROOT->cd();

  // TF(calo) measurement
  TH2D *hEleBG_CaloIso_OS_PtEta[nCaloCategory];
  TH2D *hEleBG_CaloIso_SS_PtEta[nCaloCategory];
  TH2D *hEleBG_CaloIso_PtEta[nCaloCategory];
  TH2D *hEleBG_CaloIso_OS_InvPtEta[nCaloCategory];
  TH2D *hEleBG_CaloIso_SS_InvPtEta[nCaloCategory];
  TH2D *hEleBG_CaloIso_InvPtEta[nCaloCategory];
  TH1D *hEleBG_CaloIso_OS_Pileup_Pt[nCaloCategory][nPileupCategory];
  TH1D *hEleBG_CaloIso_SS_Pileup_Pt[nCaloCategory][nPileupCategory];
  TH1D *hEleBG_CaloIso_Pileup_Pt[nCaloCategory][nPileupCategory];

  for(int iCalo=0;iCalo<nCaloCategory;iCalo++){
    hEleBG_CaloIso_OS_PtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_EleBG_OS_PtEta", iCalo)->Clone(Form("hEleBG_CaloIso_OS_PtEta_%d", iCalo));
    hEleBG_CaloIso_SS_PtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_EleBG_SS_PtEta", iCalo)->Clone(Form("hEleBG_CaloIso_SS_PtEta_%d", iCalo));
    //hEleBG_CaloIso_PtEta[iCalo]    = new TH2D(Form("hEleBG_CaloIso_PtEta_%d", iCalo), ";probe p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hEleBG_CaloIso_PtEta[iCalo]    = (TH2D *)hPtEta_Bin10->Clone(Form("hEleBG_CaloIso_PtEta_%d", iCalo));
    
    hEleBG_CaloIso_OS_InvPtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_EleBG_OS_InvPtEta", iCalo)->Clone(Form("hEleBG_CaloIso_OS_InvPtEta_%d", iCalo));
    hEleBG_CaloIso_SS_InvPtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_EleBG_SS_InvPtEta", iCalo)->Clone(Form("hEleBG_CaloIso_SS_InvPtEta_%d", iCalo));
    hEleBG_CaloIso_InvPtEta[iCalo]    = new TH2D(Form("hEleBG_CaloIso_InvPtEta_%d", iCalo), ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);

    for(int j=0;j<nPileupCategory;j++){
      hEleBG_CaloIso_OS_Pileup_Pt[iCalo][j] = (TH1D *)myHistData[0]->GetHistChain1D("hOld4LTracks_CaloIso_EleBG_OS_Pileup_Pt", iCalo, j)->Clone(Form("hEleBG_CaloIso_OS_Pileup_Pt_%d_%d", iCalo, j));
      hEleBG_CaloIso_SS_Pileup_Pt[iCalo][j] = (TH1D *)myHistData[0]->GetHistChain1D("hOld4LTracks_CaloIso_EleBG_SS_Pileup_Pt", iCalo, j)->Clone(Form("hEleBG_CaloIso_SS_Pileup_Pt_%d_%d", iCalo, j));
      hEleBG_CaloIso_Pileup_Pt[iCalo][j] = hEleBG_CaloIso_PtEta[iCalo]->ProjectionX();
      hEleBG_CaloIso_Pileup_Pt[iCalo][j]->SetName(Form("hEleBG_CaloIso_Pileup_Pt_%d_%d", iCalo, j));
      hEleBG_CaloIso_Pileup_Pt[iCalo][j]->Reset();
    }

    // plot : CaloIso Electron Background Zmass Basic
    std::cout << "plot : CaloIso Electron Background Zmass Basic" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    if(iCalo==4){
      hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e1, 1e7);
    } else{
      hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e7);
    }
    hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_EleBG_ZMass", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_CaloIso_%d.pdf", iCalo), "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(50, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Electron Background Zmass Basic OS
    std::cout << "plot : CaloIso Electron Background Zmass Basic OS" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    if(iCalo==4){
      hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e1, 1e7);
    }else{
      hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e7);
    }
    hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_EleBG_OS_ZMass", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_CaloIsoOS_%d.pdf", iCalo), "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(50, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Electron Background Zmass Basic SS
    std::cout << "plot : CaloIso Electron Background Zmass Basic SS" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    if(iCalo==4){
      hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e1, 1e7);
    }else{
      hFrame = new TH2D("hFrame", ";M_{ee} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e7);
    }
    hRatioFrame = new TH2D("hRatioFrame", Form(";M_{ee} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_EleBG_SS_ZMass", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print(Form("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_Zmass_CaloIsoSS_%d.pdf", iCalo), "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(50, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
    pad0->SetLeftMargin(0.12);
    pad0->SetRightMargin(0.15);
    pad0->Draw();
    pad0->cd();
    pad0->SetLogx(true);
    pad0->SetLogz(true);

    // plot : scale factor for electron track
    Rebin2D_For10Bin(hEleBG_CaloIso_PtEta[iCalo], hEleBG_CaloIso_OS_PtEta[iCalo], hEleBG_CaloIso_SS_PtEta[iCalo]);
    for(int j=0;j<nPileupCategory;j++){
      Rebin1D_For10Bin(hEleBG_CaloIso_Pileup_Pt[iCalo][j], hEleBG_CaloIso_OS_Pileup_Pt[iCalo][j], hEleBG_CaloIso_SS_Pileup_Pt[iCalo][j]);
    }
    hEleBG_CaloIso_PtEta[iCalo]->GetYaxis()->SetTitleOffset(0.9);  
    hEleBG_CaloIso_PtEta[iCalo]->Draw("colz0");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    // plot : scale factor for electron track
    for(int i=0;i<(hEleBG_CaloIso_InvPtEta[iCalo]->GetNbinsX());i++){
      for(int j=0;j<(hEleBG_CaloIso_InvPtEta[iCalo]->GetNbinsY());j++){
	double origE = hEleBG_CaloIso_OS_InvPtEta[iCalo]->GetBinError(i+1, j+1);
	double origV = hEleBG_CaloIso_OS_InvPtEta[iCalo]->GetBinContent(i+1, j+1);
	double addE  = hEleBG_CaloIso_SS_InvPtEta[iCalo]->GetBinError(i+1, j+1);
	double addV  = hEleBG_CaloIso_SS_InvPtEta[iCalo]->GetBinContent(i+1, j+1);
	hEleBG_CaloIso_InvPtEta[iCalo]->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
	hEleBG_CaloIso_InvPtEta[iCalo]->SetBinContent(i+1, j+1, origV - addV);
      }// for j
    }// for i
    pad0->SetLogx(false);
    hEleBG_CaloIso_InvPtEta[iCalo]->GetYaxis()->SetTitleOffset(0.9);  
    hEleBG_CaloIso_InvPtEta[iCalo]->Draw("colz0");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    delete pad0; pad0=NULL;
  }// for iCalo

  TH2D *hEleBG_TF_CaloIso_4to0_PtEta    = new TH2D("hEleBG_TF_CaloIso_4to0_PtEta"   , ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_4to123_PtEta  = new TH2D("hEleBG_TF_CaloIso_4to123_PtEta" , ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to0_PtEta  = new TH2D("hEleBG_TF_CaloIso_432to0_PtEta" , ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to1_PtEta  = new TH2D("hEleBG_TF_CaloIso_432to1_PtEta" , ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to01_PtEta = new TH2D("hEleBG_TF_CaloIso_432to01_PtEta", ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to0_PtEta_Err  = new TH2D("hEleBG_TF_CaloIso_432to0_PtEta_Err" , ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to1_PtEta_Err  = new TH2D("hEleBG_TF_CaloIso_432to1_PtEta_Err" , ";p_{T} [GeV];#eta", 10, XBinsLogPt10ForLepton, 25, -2.5, 2.5);
  TH1D *hEleBG_TF_CaloIso_432to0_Pileup_Pt[nPileupCategory];
  TH1D *hEleBG_TF_CaloIso_432to1_Pileup_Pt[nPileupCategory];
  for(int j=0;j<nPileupCategory;j++){
    hEleBG_TF_CaloIso_432to0_Pileup_Pt[j] = new TH1D(Form("hEleBG_TF_CaloIso_432to0_Pileup_Pt_%d", j), ";p_{T} [GeV];", 10, XBinsLogPt10ForLepton);
    hEleBG_TF_CaloIso_432to1_Pileup_Pt[j] = new TH1D(Form("hEleBG_TF_CaloIso_432to1_Pileup_Pt_%d", j), ";p_{T} [GeV];", 10, XBinsLogPt10ForLepton);
  }// for j pileup

  TH2D *hEleBG_TF_CaloIso_4to0_InvPtEta    = new TH2D("hEleBG_TF_CaloIso_4to0_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_4to123_InvPtEta  = new TH2D("hEleBG_TF_CaloIso_4to123_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to0_InvPtEta  = new TH2D("hEleBG_TF_CaloIso_432to0_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to1_InvPtEta  = new TH2D("hEleBG_TF_CaloIso_432to1_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hEleBG_TF_CaloIso_432to01_InvPtEta  = new TH2D("hEleBG_TF_CaloIso_432to01_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);

  for(int i=0;i<(hEleBG_TF_CaloIso_4to0_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_TF_CaloIso_4to0_PtEta->GetNbinsY());j++){
      double V0 = hEleBG_CaloIso_PtEta[0]->GetBinContent(i+1, j+1);
      double V1 = hEleBG_CaloIso_PtEta[1]->GetBinContent(i+1, j+1);
      double V2 = hEleBG_CaloIso_PtEta[2]->GetBinContent(i+1, j+1);
      double V3 = hEleBG_CaloIso_PtEta[3]->GetBinContent(i+1, j+1);
      double V4 = hEleBG_CaloIso_PtEta[4]->GetBinContent(i+1, j+1);

      double E0 = hEleBG_CaloIso_PtEta[0]->GetBinError(i+1, j+1);
      double E1 = hEleBG_CaloIso_PtEta[1]->GetBinError(i+1, j+1);
      double E2 = hEleBG_CaloIso_PtEta[2]->GetBinError(i+1, j+1);
      double E3 = hEleBG_CaloIso_PtEta[3]->GetBinError(i+1, j+1);
      double E4 = hEleBG_CaloIso_PtEta[4]->GetBinError(i+1, j+1);

      if(V4>0){
	hEleBG_TF_CaloIso_4to0_PtEta->SetBinContent(i+1, j+1, V0/V4);
	hEleBG_TF_CaloIso_4to0_PtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4, E0, E4));
	hEleBG_TF_CaloIso_4to123_PtEta->SetBinContent(i+1, j+1, (V1+V2+V3)/V4);
	hEleBG_TF_CaloIso_4to123_PtEta->SetBinError(i+1, j+1, CalcFracError(V1+V2+V3, V4, TMath::Sqrt(E1*E1 + E2*E2 + E3*E3), E4));
      }
      if((V4+V3+V2)>0){
	hEleBG_TF_CaloIso_432to0_PtEta->SetBinContent(i+1, j+1, V0/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to0_PtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4+V3+V2, E0, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to0_PtEta_Err->SetBinContent(i+1, j+1, CalcFracError(V0, V4+V3+V2, E0, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to1_PtEta->SetBinContent(i+1, j+1, V1/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to1_PtEta->SetBinError(i+1, j+1, CalcFracError(V1, V4+V3+V2, E1, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to1_PtEta_Err->SetBinContent(i+1, j+1, CalcFracError(V1, V4+V3+V2, E1, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to01_PtEta->SetBinContent(i+1, j+1, (V0+V1)/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to01_PtEta->SetBinError(i+1, j+1, CalcFracError(V0+V1, V4+V3+V2, TMath::Sqrt(E0*E0 + E1*E1), TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
      }
    }
  }

  // pile-up
  for(int j=0;j<nPileupCategory;j++){
    for(int i=0;i<10;i++){
      double V0 = hEleBG_CaloIso_Pileup_Pt[0][j]->GetBinContent(i+1);
      double V1 = hEleBG_CaloIso_Pileup_Pt[1][j]->GetBinContent(i+1);
      double V2 = hEleBG_CaloIso_Pileup_Pt[2][j]->GetBinContent(i+1);
      double V3 = hEleBG_CaloIso_Pileup_Pt[3][j]->GetBinContent(i+1);
      double V4 = hEleBG_CaloIso_Pileup_Pt[4][j]->GetBinContent(i+1);

      double E0 = hEleBG_CaloIso_Pileup_Pt[0][j]->GetBinError(i+1);
      double E1 = hEleBG_CaloIso_Pileup_Pt[1][j]->GetBinError(i+1);
      double E2 = hEleBG_CaloIso_Pileup_Pt[2][j]->GetBinError(i+1);
      double E3 = hEleBG_CaloIso_Pileup_Pt[3][j]->GetBinError(i+1);
      double E4 = hEleBG_CaloIso_Pileup_Pt[4][j]->GetBinError(i+1);
      if((V4+V3+V2)>0){
	hEleBG_TF_CaloIso_432to0_Pileup_Pt[j]->SetBinContent(i+1, V0/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to0_Pileup_Pt[j]->SetBinError(i+1, CalcFracError(V0, V4+V3+V2, E0, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to1_Pileup_Pt[j]->SetBinContent(i+1, (V0+V1)/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to1_Pileup_Pt[j]->SetBinError(i+1, CalcFracError(V0+V1, V4+V3+V2, TMath::Sqrt(E0*E0 + E1*E1), TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
      }
    }// for X
  }// for j pileup

  for(int i=0;i<(hEleBG_TF_CaloIso_4to0_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hEleBG_TF_CaloIso_4to0_InvPtEta->GetNbinsY());j++){
      double V0 = hEleBG_CaloIso_InvPtEta[0]->GetBinContent(i+1, j+1);
      double V1 = hEleBG_CaloIso_InvPtEta[1]->GetBinContent(i+1, j+1);
      double V2 = hEleBG_CaloIso_InvPtEta[2]->GetBinContent(i+1, j+1);
      double V3 = hEleBG_CaloIso_InvPtEta[3]->GetBinContent(i+1, j+1);
      double V4 = hEleBG_CaloIso_InvPtEta[4]->GetBinContent(i+1, j+1);

      double E0 = hEleBG_CaloIso_InvPtEta[0]->GetBinError(i+1, j+1);
      double E1 = hEleBG_CaloIso_InvPtEta[1]->GetBinError(i+1, j+1);
      double E2 = hEleBG_CaloIso_InvPtEta[2]->GetBinError(i+1, j+1);
      double E3 = hEleBG_CaloIso_InvPtEta[3]->GetBinError(i+1, j+1);
      double E4 = hEleBG_CaloIso_InvPtEta[4]->GetBinError(i+1, j+1);
      if(V4>0){
	hEleBG_TF_CaloIso_4to0_InvPtEta->SetBinContent(i+1, j+1, V0/V4);
	hEleBG_TF_CaloIso_4to0_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4, E0, E4));
	hEleBG_TF_CaloIso_4to123_InvPtEta->SetBinContent(i+1, j+1, (V1+V2+V3)/V4);
	hEleBG_TF_CaloIso_4to123_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V1+V2+V3, V4, TMath::Sqrt(E1*E1 + E2*E2 + E3*E3), E4));
      }
      if((V4+V3+V2)>0){
	hEleBG_TF_CaloIso_432to0_InvPtEta->SetBinContent(i+1, j+1, V0/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to0_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4+V3+V2, E0, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to1_InvPtEta->SetBinContent(i+1, j+1, V1/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to1_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V1, V4+V3+V2, E1, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hEleBG_TF_CaloIso_432to01_InvPtEta->SetBinContent(i+1, j+1, (V0+V1)/(V4+V3+V2));
	hEleBG_TF_CaloIso_432to01_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V0+V1, V4+V3+V2, TMath::Sqrt(E0*E0 + E1*E1), TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
      }
    }
  }
  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogx(true);
  pad0->SetLogz(true);
  hEleBG_TF_CaloIso_4to0_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_4to0_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hEleBG_TF_CaloIso_4to123_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_4to123_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hEleBG_TF_CaloIso_432to0_PtEta->GetZaxis()->SetRangeUser(1e-4,1.0);
  hEleBG_TF_CaloIso_432to0_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to0_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_CaloIso.pdf", "pdf");

  hEleBG_TF_CaloIso_432to1_PtEta->GetZaxis()->SetRangeUser(1e-4,1.0);
  hEleBG_TF_CaloIso_432to1_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to1_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_CaloIsoVR.pdf", "pdf");

  hEleBG_TF_CaloIso_432to0_PtEta_Err->GetZaxis()->SetRangeUser(1e-4,1.0);
  hEleBG_TF_CaloIso_432to0_PtEta_Err->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to0_PtEta_Err->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_CaloIso_Err.pdf", "pdf");

  hEleBG_TF_CaloIso_432to1_PtEta_Err->GetZaxis()->SetRangeUser(1e-4,1.0);
  hEleBG_TF_CaloIso_432to1_PtEta_Err->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to1_PtEta_Err->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/EleCR_TF_CaloIsoVR_Err.pdf", "pdf");

  hEleBG_TF_CaloIso_432to01_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to01_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  pad0->SetLogx(false);
  pad0->SetLogy(false);
  hEleBG_TF_CaloIso_4to0_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_4to0_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hEleBG_TF_CaloIso_4to123_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_4to123_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hEleBG_TF_CaloIso_432to0_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to0_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hEleBG_TF_CaloIso_432to1_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to1_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hEleBG_TF_CaloIso_432to01_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hEleBG_TF_CaloIso_432to01_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad0; pad0=NULL;

  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.15);
  pad0->SetRightMargin(0.12);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogx(true);
  pad0->SetLogy(true);
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";p_{T} [GeV];TF_{calo-veto}^{e}", 10, XBinsLogPt10ForLepton, 100, 1e-5, 1.0);
  hFrame->Draw();
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  delete tl;
  tl = new TLegend(0.50, 0.94-0.12-0.3, 0.8, 0.94-0.12);
  for(int j=1;j<nPileupCategory-1;j++){
    hEleBG_TF_CaloIso_432to0_Pileup_Pt[j]->SetLineColor(Col5[j-1]);
    hEleBG_TF_CaloIso_432to0_Pileup_Pt[j]->SetMarkerColor(Col5[j-1]);
    hEleBG_TF_CaloIso_432to0_Pileup_Pt[j]->Draw("sameE");
    tl->AddEntry(hEleBG_TF_CaloIso_432to0_Pileup_Pt[j], Form("%d < #mu < %d", j*10, j*10+10), "E");
  }
  tl->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";p_{T} [GeV];TF_{calo-sideband}^{e}", 10, XBinsLogPt10ForLepton, 100, 1e-4, 10.0);
  hFrame->Draw();
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  delete tl;
  tl = new TLegend(0.50, 0.94-0.12-0.3, 0.8, 0.94-0.12);
  for(int j=1;j<nPileupCategory-1;j++){
    hEleBG_TF_CaloIso_432to1_Pileup_Pt[j]->SetLineColor(Col5[j-1]);
    hEleBG_TF_CaloIso_432to1_Pileup_Pt[j]->SetMarkerColor(Col5[j-1]);
    hEleBG_TF_CaloIso_432to1_Pileup_Pt[j]->Draw("sameE");
    tl->AddEntry(hEleBG_TF_CaloIso_432to1_Pileup_Pt[j], Form("%d < #mu < %d", j*10, j*10+10), "E");
  }
  tl->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad0; pad0=NULL;

  tfOut->cd();
  hEleBG_TF_CaloIso_4to0_PtEta->Write();
  hEleBG_TF_CaloIso_4to123_PtEta->Write();
  hEleBG_TF_CaloIso_432to0_PtEta->Write();
  hEleBG_TF_CaloIso_432to1_PtEta->Write();
  hEleBG_TF_CaloIso_432to01_PtEta->Write();

  for(int j=0;j<nPileupCategory;j++){
    hEleBG_TF_CaloIso_432to0_Pileup_Pt[j]->Write();
    hEleBG_TF_CaloIso_432to1_Pileup_Pt[j]->Write();
  }// for j

  hEleBG_TF_CaloIso_4to0_InvPtEta->Write();
  hEleBG_TF_CaloIso_4to123_InvPtEta->Write();
  hEleBG_TF_CaloIso_432to0_InvPtEta->Write();
  hEleBG_TF_CaloIso_432to1_InvPtEta->Write();
  hEleBG_TF_CaloIso_432to01_InvPtEta->Write();
  gROOT->cd();


  /* **********   Muon BG   ********** */
  // plot : Muon Background Zmass Basic
  std::cout << "plot : Muon Background Zmass Basic" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e2, 1e7);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MuBG_Basic_ZMass", -1, RebinVal, 0, 1);
  hFrame->GetYaxis()->SetRangeUser(5e2, 1e7);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_TrackDenom.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Muon Background Zmass Basic OS
  std::cout << "plot : Muon Background Zmass Basic OS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e2, 1e7);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MuBG_BasicOS_ZMass", -1, RebinVal, 0, 1);
  hFrame->GetYaxis()->SetRangeUser(5e2, 1e7);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_TrackDenomOS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Muon Background Zmass Basic SS
  std::cout << "plot : Muon Background Zmass Basic SS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MuBG_BasicSS_ZMass", -1, RebinVal, 0, 1);
  hFrame->GetYaxis()->SetRangeUser(5e-1, 1e4);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_TrackDenomSS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Muon Background Zmass Disap
  std::cout << "plot : Muon Background Zmass Disap" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MuBG_Disap_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_TrackNume.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Muon Background Zmass Disap OS
  std::cout << "plot : Muon Background Zmass Disap OS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MuBG_DisapOS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_TrackNumeOS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : Muon Background Zmass Disap SS
  std::cout << "plot : Muon Background Zmass Disap SS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e4);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MuBG_DisapSS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_TrackNumeSS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;



  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogz(true);
  // plot : MuBG TF(disap) PtEta
  pad0->SetLogx(true);
  TH2D *hMuBG_BasicOS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_BasicOS_PtEta",-1)->Clone("hMuBG_BasicOS_PtEta");
  TH2D *hMuBG_BasicSS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_BasicSS_PtEta",-1)->Clone("hMuBG_BasicSS_PtEta");
  TH2D *hMuBG_Basic_PtEta = new TH2D("hMuBG_Basic_PtEta", ";probe p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  for(int i=0;i<(hMuBG_Basic_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hMuBG_Basic_PtEta->GetNbinsY());j++){
      double origE = hMuBG_BasicOS_PtEta->GetBinError(i+1, j+1);
      double origV = hMuBG_BasicOS_PtEta->GetBinContent(i+1, j+1);
      double addE = hMuBG_BasicSS_PtEta->GetBinError(i+1, j+1);
      double addV = hMuBG_BasicSS_PtEta->GetBinContent(i+1, j+1);
      hMuBG_Basic_PtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hMuBG_Basic_PtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hMuBG_Basic_PtEta->GetYaxis()->SetTitleOffset(0.9);  
  hMuBG_Basic_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMuBG_DisapOS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_DisapOS_PtEta",-1)->Clone("hMuBG_DisapOS_PtEta");
  TH2D *hMuBG_DisapSS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_DisapSS_PtEta",-1)->Clone("hMuBG_DisapSS_PtEta");
  TH2D *hMuBG_Disap_PtEta = new TH2D("hMuBG_Disap_PtEta", ";probe p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  for(int i=0;i<(hMuBG_Disap_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hMuBG_Disap_PtEta->GetNbinsY());j++){
      double origE = hMuBG_DisapOS_PtEta->GetBinError(i+1, j+1);
      double origV = hMuBG_DisapOS_PtEta->GetBinContent(i+1, j+1);
      double addE = hMuBG_DisapSS_PtEta->GetBinError(i+1, j+1);
      double addV = hMuBG_DisapSS_PtEta->GetBinContent(i+1, j+1);
      hMuBG_Disap_PtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hMuBG_Disap_PtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hMuBG_Disap_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hMuBG_Disap_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hMuBG_TF_13Bin  = new TH1D("hMuBG_TF_13Bin", ";probe p_{T} [GeV];TF^{#mu}_{pixel-only}", 13, XBinsLogPt13ForLepton);
  TH1D *hMuBG_TF_1Bin  = new TH1D("hMuBG_TF_1Bin", "", 1, 0, 1);
  TH2D *hMuBG_TF_PtEta = (TH2D *)hMuBG_Basic_PtEta->Clone("hMuBG_TF_PtEta");
  TH2D *hMuBG_TF_PtEta_Err = (TH2D *)hMuBG_Basic_PtEta->Clone("hMuBG_TF_PtEta_Err");
  hMuBG_TF_PtEta->Reset();
  hMuBG_TF_PtEta_Err->Reset();
  for(int i=0;i<(hMuBG_TF_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hMuBG_TF_PtEta->GetNbinsY());j++){
      double V1 = hMuBG_Basic_PtEta->GetBinContent(i+1, j+1);
      double V2 = hMuBG_Disap_PtEta->GetBinContent(i+1, j+1);
      double E1 = hMuBG_Basic_PtEta->GetBinError(i+1, j+1);
      double E2 = hMuBG_Disap_PtEta->GetBinError(i+1, j+1);
      if(V1>0){
	hMuBG_TF_PtEta->SetBinContent(i+1, j+1, V2/V1);
	hMuBG_TF_PtEta->SetBinError(i+1, j+1, CalcFracError(V2, V1, E2, E1));
	hMuBG_TF_PtEta_Err->SetBinContent(i+1, j+1, CalcFracError(V2, V1, E2, E1));
      }
    }
  }

  {
    double vNume = 0.0;
    double vDenom = 0.0;
    double eNume = 0.0;
    double eDenom = 0.0;
    double TMP_Error = 0.0;

    vNume  += hMuBG_Disap_PtEta->IntegralAndError(1, nLogPtForLepton, 4, 12, TMP_Error);
    eNume  += TMP_Error*TMP_Error;
    vNume  += hMuBG_Disap_PtEta->IntegralAndError(1, nLogPtForLepton, 14, 22, TMP_Error);
    eNume  += TMP_Error*TMP_Error;

    vDenom += hMuBG_Basic_PtEta->IntegralAndError(1, nLogPtForLepton, 4, 12, TMP_Error);
    eDenom += TMP_Error*TMP_Error;
    vDenom += hMuBG_Basic_PtEta->IntegralAndError(1, nLogPtForLepton, 14, 12, TMP_Error);
    eDenom += TMP_Error*TMP_Error;
    
    eNume  = TMath::Sqrt(eNume);
    eDenom = TMath::Sqrt(eDenom);

    hMuBG_TF_1Bin->SetBinContent(1, vNume/vDenom);
    hMuBG_TF_1Bin->SetBinError(1, CalcFracError(vNume, vDenom, eNume, eDenom));
    std::cout << "TF(muon, disap) = " << vNume/vDenom << std::endl;
  }

  for(int iBin=0;iBin<12;iBin++){
    double vNume = 0.0;
    double vDenom = 0.0;
    double eNume = 0.0;
    double eDenom = 0.0;
    double TMP_Error = 0.0;
    vNume  += hMuBG_Disap_PtEta->IntegralAndError(iBin*3+1, iBin*3+3, 4, 12, TMP_Error);
    eNume  += TMP_Error*TMP_Error;
    vNume  += hMuBG_Disap_PtEta->IntegralAndError(iBin*3+1, iBin*3+3, 14, 22, TMP_Error);
    eNume  += TMP_Error*TMP_Error;

    vDenom += hMuBG_Basic_PtEta->IntegralAndError(iBin*3+1, iBin*3+3, 4, 12, TMP_Error);
    eDenom += TMP_Error*TMP_Error;
    vDenom += hMuBG_Basic_PtEta->IntegralAndError(iBin*3+1, iBin*3+3, 14, 12, TMP_Error);
    eDenom += TMP_Error*TMP_Error;
    
    eNume  = TMath::Sqrt(eNume);
    eDenom = TMath::Sqrt(eDenom);

    if(vDenom==0)
      continue;
    hMuBG_TF_13Bin->SetBinContent(iBin+1, vNume/vDenom);
    hMuBG_TF_13Bin->SetBinError(iBin+1, CalcFracError(vNume, vDenom, eNume, eDenom));
  }

  {
    double vNume = 0.0;
    double vDenom = 0.0;
    double eNume = 0.0;
    double eDenom = 0.0;
    double TMP_Error = 0.0;
    int iBin=12;
    vNume  += hMuBG_Disap_PtEta->IntegralAndError(iBin*3+1, nLogPtForLepton, 4, 12, TMP_Error);
    eNume  += TMP_Error*TMP_Error;
    vNume  += hMuBG_Disap_PtEta->IntegralAndError(iBin*3+1, nLogPtForLepton, 14, 22, TMP_Error);
    eNume  += TMP_Error*TMP_Error;

    vDenom += hMuBG_Basic_PtEta->IntegralAndError(iBin*3+1, nLogPtForLepton, 4, 12, TMP_Error);
    eDenom += TMP_Error*TMP_Error;
    vDenom += hMuBG_Basic_PtEta->IntegralAndError(iBin*3+1, nLogPtForLepton, 14, 12, TMP_Error);
    eDenom += TMP_Error*TMP_Error;
    
    eNume  = TMath::Sqrt(eNume);
    eDenom = TMath::Sqrt(eDenom);

    hMuBG_TF_13Bin->SetBinContent(iBin+1, vNume/vDenom);
    hMuBG_TF_13Bin->SetBinError(iBin+1, CalcFracError(vNume, vDenom, eNume, eDenom));
  }

  hMuBG_TF_13Bin->GetYaxis()->SetRangeUser(1e-5,1e-2);
  hMuBG_TF_13Bin->GetYaxis()->SetTitleOffset(0.9);
  hMuBG_TF_13Bin->Draw("E");
  pad0->SetLogy(true);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_Track13Bin.pdf", "pdf");
  pad0->SetLogy(false);

  hMuBG_TF_PtEta->GetZaxis()->SetRangeUser(1e-4,1.0);
  hMuBG_TF_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hMuBG_TF_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_Track.pdf", "pdf");

  hMuBG_TF_PtEta_Err->GetZaxis()->SetRangeUser(1e-4,1.0);
  hMuBG_TF_PtEta_Err->GetYaxis()->SetTitleOffset(0.9);
  hMuBG_TF_PtEta_Err->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_Track_Err.pdf", "pdf");

  // plot : MuBG TF(disap) InvPtEta
  pad0->SetLogx(false);
  TH2D *hMuBG_BasicOS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_BasicOS_InvPtEta",-1)->Clone("hMuBG_BasicOS_InvPtEta");
  TH2D *hMuBG_BasicSS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_BasicSS_InvPtEta",-1)->Clone("hMuBG_BasicSS_InvPtEta");
  TH2D *hMuBG_Basic_InvPtEta = new TH2D("hMuBG_Basic_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);

  for(int i=0;i<(hMuBG_Basic_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hMuBG_Basic_InvPtEta->GetNbinsY());j++){
      double origE = hMuBG_BasicOS_InvPtEta->GetBinError(i+1, j+1);
      double origV = hMuBG_BasicOS_InvPtEta->GetBinContent(i+1, j+1);
      double addE = hMuBG_BasicSS_InvPtEta->GetBinError(i+1, j+1);
      double addV = hMuBG_BasicSS_InvPtEta->GetBinContent(i+1, j+1);
      hMuBG_Basic_InvPtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hMuBG_Basic_InvPtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hMuBG_Basic_InvPtEta->GetYaxis()->SetTitleOffset(0.9);  
  hMuBG_Basic_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMuBG_DisapOS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_DisapOS_InvPtEta",-1)->Clone("hMuBG_DisapOS_InvPtEta");
  TH2D *hMuBG_DisapSS_InvPtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MuBG_DisapSS_InvPtEta",-1)->Clone("hMuBG_DisapSS_InvPtEta");
  TH2D *hMuBG_Disap_InvPtEta = new TH2D("hMuBG_Disap_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  for(int i=0;i<(hMuBG_Disap_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hMuBG_Disap_InvPtEta->GetNbinsY());j++){
      double origE = hMuBG_DisapOS_InvPtEta->GetBinError(i+1, j+1);
      double origV = hMuBG_DisapOS_InvPtEta->GetBinContent(i+1, j+1);
      double addE = hMuBG_DisapSS_InvPtEta->GetBinError(i+1, j+1);
      double addV = hMuBG_DisapSS_InvPtEta->GetBinContent(i+1, j+1);
      hMuBG_Disap_InvPtEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hMuBG_Disap_InvPtEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hMuBG_Disap_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hMuBG_Disap_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMuBG_TF_InvPtEta = (TH2D *)hMuBG_Basic_InvPtEta->Clone("hMuBG_TF_InvPtEta");
  hMuBG_TF_InvPtEta->Reset();
  for(int i=0;i<(hMuBG_TF_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hMuBG_TF_InvPtEta->GetNbinsY());j++){
      double V1 = hMuBG_Basic_InvPtEta->GetBinContent(i+1, j+1);
      double V2 = hMuBG_Disap_InvPtEta->GetBinContent(i+1, j+1);
      double E1 = hMuBG_Basic_InvPtEta->GetBinError(i+1, j+1);
      double E2 = hMuBG_Disap_InvPtEta->GetBinError(i+1, j+1);
      if(V1>0){
	hMuBG_TF_InvPtEta->SetBinContent(i+1, j+1, V2/V1);
	hMuBG_TF_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V2, V1, E2, E1));
      }
    }
  }
  hMuBG_TF_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hMuBG_TF_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  //if(fUpdateNote)
  //c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_Track.pdf", "pdf");
  delete pad0;pad0=NULL;
  tfOut->cd();
  hMuBG_TF_1Bin->Write();
  hMuBG_TF_13Bin->Write();
  hMuBG_TF_PtEta->Write();
  hMuBG_TF_InvPtEta->Write();
  gROOT->cd();

  // plot : MS TF Zmass Basic
  std::cout << "plot : MS TF Zmass Basic" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5, 1e8);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MSBG_Basic_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_MSTrackDenom.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : MS TF Zmass Basic OS
  std::cout << "plot : MS TF Zmass Basic OS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5, 1e8);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MSBG_BasicOS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_MSTrackDenomOS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : MS TF Zmass Basic SS
  std::cout << "plot : MS TF Zmass Basic SS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e5);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MSBG_BasicSS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_MSTrackDenomSS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : MS TF Zmass Disap
  std::cout << "plot : MS TF Zmass Disap" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e6);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MSBG_Disap_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_MSTrackNume.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : MS TF Zmass Disap OS
  std::cout << "plot : MS TF Zmass Disap OS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e6);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MSBG_DisapOS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_MSTrackNumeOS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : MS TF Zmass DisapSS
  std::cout << "plot : MS TF Zmass Disap SS" << std::endl;
  delete hFrame; hFrame=NULL;
  delete hRatioFrame; hRatioFrame=NULL;
  hFrame = new TH2D("hFrame", ";M_{#mu#mu} [GeV];Events", 100, 50, 200, 100, 5e-1, 1e6);
  hRatioFrame = new TH2D("hRatioFrame", Form(";M_{#mu#mu} [GeV];data/MC "), 100, 50, 200, 100, 0.2, 1.8);
  SetupCanvas(3);
  pad1[0]->SetLogy(true);
  DrawTH1RatioPlot("hOld4LTracks_CaloIso_MSBG_DisapSS_ZMass", -1, RebinVal, 0, 1);
  ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
  myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  myText(0.60, 0.90-0.12, kBlack, "");
  tl->Draw("same");
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_Zmass_MSTrackNumeSS.pdf", "pdf");
  pad1[0]->SetLogy(false);
  hData1D->GetXaxis()->SetRangeUser(50, 200);
  hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad1[0]; pad1[0]=NULL;
  delete pad1[1]; pad1[1]=NULL;

  // plot : MuBG TF(noMS) PhiEta
  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogz(true);
  pad0->SetLogx(false);
  TH2D *hMSBG_BasicOS_PhiEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_BasicOS_PhiEta",-1)->Clone("hMSBG_BasicOS_PhiEta");
  TH2D *hMSBG_BasicSS_PhiEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_BasicSS_PhiEta",-1)->Clone("hMSBG_BasicSS_PhiEta");
  TH2D *hMSBG_Basic_PhiEta = new TH2D("hMSBG_Basic_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);

  for(int i=0;i<(hMSBG_Basic_PhiEta->GetNbinsX());i++){
    for(int j=0;j<(hMSBG_Basic_PhiEta->GetNbinsY());j++){
      double origE = hMSBG_BasicOS_PhiEta->GetBinError(i+1, j+1);
      double origV = hMSBG_BasicOS_PhiEta->GetBinContent(i+1, j+1);
      double addE = hMSBG_BasicSS_PhiEta->GetBinError(i+1, j+1);
      double addV = hMSBG_BasicSS_PhiEta->GetBinContent(i+1, j+1);
      hMSBG_Basic_PhiEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hMSBG_Basic_PhiEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hMSBG_Basic_PhiEta->GetYaxis()->SetTitleOffset(0.9);  
  hMSBG_Basic_PhiEta->RebinX(2);
  hMSBG_Basic_PhiEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMSBG_DisapOS_PhiEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_DisapOS_PhiEta",-1)->Clone("hMSBG_DisapOS_PhiEta");
  TH2D *hMSBG_DisapSS_PhiEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_DisapSS_PhiEta",-1)->Clone("hMSBG_DisapSS_PhiEta");
  TH2D *hMSBG_Disap_PhiEta = new TH2D("hMSBG_Disap_PhiEta", ";#phi;#eta", 50, -TMath::Pi(), TMath::Pi(), 25, -2.5, 2.5);
  hMSBG_Disap_PhiEta->Reset();
  for(int i=0;i<(hMSBG_Disap_PhiEta->GetNbinsX());i++){
    for(int j=0;j<(hMSBG_Disap_PhiEta->GetNbinsY());j++){
      double origE = hMSBG_DisapOS_PhiEta->GetBinError(i+1, j+1);
      double origV = hMSBG_DisapOS_PhiEta->GetBinContent(i+1, j+1);
      double addE = hMSBG_DisapSS_PhiEta->GetBinError(i+1, j+1);
      double addV = hMSBG_DisapSS_PhiEta->GetBinContent(i+1, j+1);
      hMSBG_Disap_PhiEta->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
      hMSBG_Disap_PhiEta->SetBinContent(i+1, j+1, origV - addV);
    }// for j
  }// for i
  hMSBG_Disap_PhiEta->GetYaxis()->SetTitleOffset(0.9);
  hMSBG_Disap_PhiEta->RebinX(2);
  hMSBG_Disap_PhiEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMSBG_TF_PhiEta = (TH2D *)hMSBG_Basic_PhiEta->Clone("hMSBG_TF_PhiEta");
  TH2D *hMSBG_TF_PhiEta_Err = (TH2D *)hMSBG_Basic_PhiEta->Clone("hMSBG_TF_PhiEta_Err");
  hMSBG_TF_PhiEta->Reset();
  hMSBG_TF_PhiEta_Err->Reset();
  for(int i=0;i<(hMSBG_TF_PhiEta->GetNbinsX());i++){
    for(int j=0;j<(hMSBG_TF_PhiEta->GetNbinsY());j++){
      double V1 = hMSBG_Basic_PhiEta->GetBinContent(i+1, j+1);
      double V2 = hMSBG_Disap_PhiEta->GetBinContent(i+1, j+1);
      double E1 = hMSBG_Basic_PhiEta->GetBinError(i+1, j+1);
      double E2 = hMSBG_Disap_PhiEta->GetBinError(i+1, j+1);
      if(V1>0){
	hMSBG_TF_PhiEta->SetBinContent(i+1, j+1, V2/V1);
	hMSBG_TF_PhiEta->SetBinError(i+1, j+1, CalcFracError(V2, V1, E2, E1));
	hMSBG_TF_PhiEta_Err->SetBinContent(i+1, j+1, CalcFracError(V2, V1, E2, E1));
      }
    }
  }
  hMSBG_TF_PhiEta->GetZaxis()->SetRangeUser(1e-4,1.0);
  hMSBG_TF_PhiEta->GetYaxis()->SetTitleOffset(0.9);
  hMSBG_TF_PhiEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_MSTrack.pdf", "pdf");

  hMSBG_TF_PhiEta_Err->GetZaxis()->SetRangeUser(1e-4,1.0);
  hMSBG_TF_PhiEta_Err->GetYaxis()->SetTitleOffset(0.9);
  hMSBG_TF_PhiEta_Err->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_MSTrack_Err.pdf", "pdf");


  // plot : MuBG TF(noMS) PtEta
  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogz(true);
  pad0->SetLogx(true);
  TH2D *hMSBG_BasicOS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_BasicOS_PtEta",-1)->Clone("hMSBG_BasicOS_PtEta");
  TH2D *hMSBG_BasicSS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_BasicSS_PtEta",-1)->Clone("hMSBG_BasicSS_PtEta");
  TH2D *hMSBG_Basic_PtEta = (TH2D *)hPtEta_Bin18->Clone("hMSBG_Basic_PtEta");
  Rebin2D_For18Bin(hMSBG_Basic_PtEta, hMSBG_BasicOS_PtEta, hMSBG_BasicSS_PtEta);
  hMSBG_Basic_PtEta->GetYaxis()->SetTitleOffset(0.9);  
  hMSBG_Basic_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMSBG_DisapOS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_DisapOS_PtEta",-1)->Clone("hMSBG_DisapOS_PtEta");
  TH2D *hMSBG_DisapSS_PtEta = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_MSBG_DisapSS_PtEta",-1)->Clone("hMSBG_DisapSS_PtEta");
  TH2D *hMSBG_Disap_PtEta = (TH2D *)hPtEta_Bin18->Clone("hMSBG_Disap_PtEta");
  Rebin2D_For18Bin(hMSBG_Disap_PtEta, hMSBG_DisapOS_PtEta, hMSBG_DisapSS_PtEta);
  hMSBG_Disap_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hMSBG_Disap_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hMSBG_TF_PtEta = (TH2D *)hMSBG_Basic_PtEta->Clone("hMSBG_TF_PtEta");
  TH2D *hMSBG_TF_PtEta_Err = (TH2D *)hMSBG_Basic_PtEta->Clone("hMSBG_TF_PtEta_Err");
  hMSBG_TF_PtEta->Reset();
  hMSBG_TF_PtEta_Err->Reset();
  for(int i=0;i<(hMSBG_TF_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hMSBG_TF_PtEta->GetNbinsY());j++){
      double V1 = hMSBG_Basic_PtEta->GetBinContent(i+1, j+1);
      double V2 = hMSBG_Disap_PtEta->GetBinContent(i+1, j+1);
      double E1 = hMSBG_Basic_PtEta->GetBinError(i+1, j+1);
      double E2 = hMSBG_Disap_PtEta->GetBinError(i+1, j+1);
      if(V1>0){
	hMSBG_TF_PtEta->SetBinContent(i+1, j+1, V2/V1);
	hMSBG_TF_PtEta->SetBinError(i+1, j+1, CalcFracError(V2, V1, E2, E1));
	hMSBG_TF_PtEta_Err->SetBinContent(i+1, j+1, CalcFracError(V2, V1, E2, E1));
      }
    }
  }
  hMSBG_TF_PtEta->GetZaxis()->SetRangeUser(1e-4,1.0);
  hMSBG_TF_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hMSBG_TF_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_MSTrack_PtEta.pdf", "pdf");

  hMSBG_TF_PtEta_Err->GetZaxis()->SetRangeUser(1e-4,1.0);
  hMSBG_TF_PtEta_Err->GetYaxis()->SetTitleOffset(0.9);
  hMSBG_TF_PtEta_Err->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  if(fUpdateNote)
    c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/MuCR_TF_MSTrack_PtEta_Err.pdf", "pdf");

  delete pad0; pad0=NULL;
  tfOut->cd();
  hMSBG_TF_PhiEta->Write();
  hMSBG_TF_PtEta->Write();
  gROOT->cd();

  /* **********   Tau BG   ********** */
  // TF(calo) measurement
  TH2D *hTauBG_MuTag_CaloIso_OS_PtEta[nCaloCategory];
  TH2D *hTauBG_MuTag_CaloIso_SS_PtEta[nCaloCategory];
  TH2D *hTauBG_MuTag_CaloIso_PtEta[nCaloCategory];
  TH2D *hTauBG_MuTag_CaloIso_OS_InvPtEta[nCaloCategory];
  TH2D *hTauBG_MuTag_CaloIso_SS_InvPtEta[nCaloCategory];
  TH2D *hTauBG_MuTag_CaloIso_InvPtEta[nCaloCategory];
  for(int iCalo=0;iCalo<nCaloCategory;iCalo++){
    hTauBG_MuTag_CaloIso_OS_PtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_TauBG_MuTag_OS_PtEta", iCalo)->Clone(Form("hTauBG_MuTag_CaloIso_OS_PtEta_%d", iCalo));
    hTauBG_MuTag_CaloIso_SS_PtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_TauBG_MuTag_SS_PtEta", iCalo)->Clone(Form("hTauBG_MuTag_CaloIso_SS_PtEta_%d", iCalo));
    hTauBG_MuTag_CaloIso_PtEta[iCalo]    = new TH2D(Form("hTauBG_MuTag_CaloIso_PtEta_%d", iCalo), ";probe p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hTauBG_MuTag_CaloIso_OS_InvPtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_TauBG_MuTag_OS_InvPtEta", iCalo)->Clone(Form("hTauBG_MuTag_CaloIso_OS_InvPtEta_%d", iCalo));
    hTauBG_MuTag_CaloIso_SS_InvPtEta[iCalo] = (TH2D *)myHistData[0]->GetHist2D("hOld4LTracks_CaloIso_TauBG_MuTag_SS_InvPtEta", iCalo)->Clone(Form("hTauBG_MuTag_CaloIso_SS_InvPtEta_%d", iCalo));
    hTauBG_MuTag_CaloIso_InvPtEta[iCalo]    = new TH2D(Form("hTauBG_MuTag_CaloIso_InvPtEta_%d", iCalo), ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);

    // plot : CaloIso Tau Background Zmass Basic
    std::cout << "plot : CaloIso Tau Background Zmass Basic" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    hFrame = new TH2D("hFrame", ";Mass [GeV];Events", 100, 0, 200, 100, 5e-1, 1e7);
    hRatioFrame = new TH2D("hRatioFrame", Form(";Mass [GeV];data/MC "), 100, 0, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_TauBG_MuTag_ZMass", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/TauCR_MuTag_Zmass_CaloIsoDenom.pdf", "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(0, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Tau Background Zmass Basic OS
    std::cout << "plot : CaloIso Tau Background Zmass Basic OS" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    hFrame = new TH2D("hFrame", ";Mass [GeV];Events", 100, 0, 200, 100, 5e-1, 1e7);
    hRatioFrame = new TH2D("hRatioFrame", Form(";Mass [GeV];data/MC "), 100, 0, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_TauBG_MuTag_OS_ZMass", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/TauCR_MuTag_Zmass_CaloIsoDenomOS.pdf", "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(0, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Tau Background Zmass Basic SS
    std::cout << "plot : CaloIso Tau Background Zmass Basic SS" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    hFrame = new TH2D("hFrame", ";Mass [GeV];Events", 100, 0, 200, 100, 5e-1, 1e7);
    hRatioFrame = new TH2D("hRatioFrame", Form(";Mass [GeV];data/MC "), 100, 0, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_TauBG_MuTag_SS_ZMass", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/TauCR_MuTag_Zmass_CaloIsoDenomSS.pdf", "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(0, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Tau Background mT Basic
    std::cout << "plot : CaloIso Tau Background mT Basic" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    hFrame = new TH2D("hFrame", ";mT [GeV];Events", 100, 0, 200, 100, 5e-1, 1e7);
    hRatioFrame = new TH2D("hRatioFrame", Form(";mT [GeV];data/MC "), 100, 0, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_TauBG_MuTag_mT", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/TauCR_MuTag_Zmass_CaloIsoDenom.pdf", "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(0, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Tau Background mT Basic OS
    std::cout << "plot : CaloIso Tau Background mT Basic OS" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    hFrame = new TH2D("hFrame", ";mT [GeV];Events", 100, 0, 200, 100, 5e-1, 1e7);
    hRatioFrame = new TH2D("hRatioFrame", Form(";mT [GeV];data/MC "), 100, 0, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_TauBG_MuTag_OS_mT", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/TauCR_MuTag_Zmass_CaloIsoDenomOS.pdf", "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(0, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    // plot : CaloIso Tau Background mT Basic SS
    std::cout << "plot : CaloIso Tau Background mT Basic SS" << std::endl;
    delete hFrame; hFrame=NULL;
    delete hRatioFrame; hRatioFrame=NULL;
    hFrame = new TH2D("hFrame", ";mT [GeV];Events", 100, 0, 200, 100, 5e-1, 1e7);
    hRatioFrame = new TH2D("hRatioFrame", Form(";mT [GeV];data/MC "), 100, 0, 200, 100, 0.2, 1.8);
    SetupCanvas(3);
    pad1[0]->SetLogy(true);
    DrawTH1RatioPlot("hOld4LTracks_CaloIso_TauBG_MuTag_SS_mT", iCalo, RebinVal, 0, 1);
    ATLASLabel(0.60, 0.90, PlotStatus.c_str(), kBlack);
    myText(0.60, 0.90-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
    myText(0.60, 0.90-0.12, kBlack, "");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    if(fUpdateNote)
      c0->Print("~/Documents/ANA-SUSY-2018-19-INT1/figures/LeptonBkgs/TauCR_MuTag_Zmass_CaloIsoDenomSS.pdf", "pdf");
    pad1[0]->SetLogy(false);
    hData1D->GetXaxis()->SetRangeUser(0, 200);
    hFrame->GetYaxis()->SetRangeUser(5e-1, hData1D->GetMaximum()*1.2);
    tl->Draw("same");
    tlSM->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    delete pad1[0]; pad1[0]=NULL;
    delete pad1[1]; pad1[1]=NULL;

    pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
    pad0->SetLeftMargin(0.12);
    pad0->SetRightMargin(0.15);
    pad0->Draw();
    pad0->cd();
    pad0->SetLogx(true);
    pad0->SetLogz(true);

    // plot : scale factor for tau track
    for(int i=0;i<(hTauBG_MuTag_CaloIso_PtEta[iCalo]->GetNbinsX());i++){
      for(int j=0;j<(hTauBG_MuTag_CaloIso_PtEta[iCalo]->GetNbinsY());j++){
	double origE = hTauBG_MuTag_CaloIso_OS_PtEta[iCalo]->GetBinError(i+1, j+1);
	double origV = hTauBG_MuTag_CaloIso_OS_PtEta[iCalo]->GetBinContent(i+1, j+1);
	double addE  = hTauBG_MuTag_CaloIso_SS_PtEta[iCalo]->GetBinError(i+1, j+1);
	double addV  = hTauBG_MuTag_CaloIso_SS_PtEta[iCalo]->GetBinContent(i+1, j+1);
	hTauBG_MuTag_CaloIso_PtEta[iCalo]->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
	hTauBG_MuTag_CaloIso_PtEta[iCalo]->SetBinContent(i+1, j+1, origV - addV);
      }// for j
    }// for i
    hTauBG_MuTag_CaloIso_PtEta[iCalo]->GetYaxis()->SetTitleOffset(0.9);  
    hTauBG_MuTag_CaloIso_PtEta[iCalo]->Draw("colz0");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    // plot : scale factor for tau track
    for(int i=0;i<(hTauBG_MuTag_CaloIso_InvPtEta[iCalo]->GetNbinsX());i++){
      for(int j=0;j<(hTauBG_MuTag_CaloIso_InvPtEta[iCalo]->GetNbinsY());j++){
	double origE = hTauBG_MuTag_CaloIso_OS_InvPtEta[iCalo]->GetBinError(i+1, j+1);
	double origV = hTauBG_MuTag_CaloIso_OS_InvPtEta[iCalo]->GetBinContent(i+1, j+1);
	double addE  = hTauBG_MuTag_CaloIso_SS_InvPtEta[iCalo]->GetBinError(i+1, j+1);
	double addV  = hTauBG_MuTag_CaloIso_SS_InvPtEta[iCalo]->GetBinContent(i+1, j+1);
	hTauBG_MuTag_CaloIso_InvPtEta[iCalo]->SetBinError(i+1, j+1, TMath::Sqrt(origE*origE + addE*addE));
	hTauBG_MuTag_CaloIso_InvPtEta[iCalo]->SetBinContent(i+1, j+1, origV - addV);
      }// for j
    }// for i
    pad0->SetLogx(false);
    hTauBG_MuTag_CaloIso_InvPtEta[iCalo]->GetYaxis()->SetTitleOffset(0.9);  
    hTauBG_MuTag_CaloIso_InvPtEta[iCalo]->Draw("colz0");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    delete pad0; pad0=NULL;
  }// for iCalo

  TH2D *hTauBG_MuTag_TF_CaloIso_4to0_PtEta    = new TH2D("hTauBG_MuTag_TF_CaloIso_4to0_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_4to123_PtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_4to123_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_432to0_PtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_432to0_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_432to1_PtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_432to1_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_432to01_PtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_432to01_PtEta", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  TH2D *hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta    = new TH2D("hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);
  TH2D *hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta  = new TH2D("hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta", ";1/p_{T} [GeV^{-1}];#eta", 50, 0.0, 0.1, 25, -2.5, 2.5);

  for(int i=0;i<(hTauBG_MuTag_TF_CaloIso_4to0_PtEta->GetNbinsX());i++){
    for(int j=0;j<(hTauBG_MuTag_TF_CaloIso_4to0_PtEta->GetNbinsY());j++){
      double V0 = hTauBG_MuTag_CaloIso_PtEta[0]->GetBinContent(i+1, j+1);
      double V1 = hTauBG_MuTag_CaloIso_PtEta[1]->GetBinContent(i+1, j+1);
      double V2 = hTauBG_MuTag_CaloIso_PtEta[2]->GetBinContent(i+1, j+1);
      double V3 = hTauBG_MuTag_CaloIso_PtEta[3]->GetBinContent(i+1, j+1);
      double V4 = hTauBG_MuTag_CaloIso_PtEta[4]->GetBinContent(i+1, j+1);

      double E0 = hTauBG_MuTag_CaloIso_PtEta[0]->GetBinError(i+1, j+1);
      double E1 = hTauBG_MuTag_CaloIso_PtEta[1]->GetBinError(i+1, j+1);
      double E2 = hTauBG_MuTag_CaloIso_PtEta[2]->GetBinError(i+1, j+1);
      double E3 = hTauBG_MuTag_CaloIso_PtEta[3]->GetBinError(i+1, j+1);
      double E4 = hTauBG_MuTag_CaloIso_PtEta[4]->GetBinError(i+1, j+1);
      if(V4>0){
	hTauBG_MuTag_TF_CaloIso_4to0_PtEta->SetBinContent(i+1, j+1, V0/V4);
	hTauBG_MuTag_TF_CaloIso_4to0_PtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4, E0, E4));
	hTauBG_MuTag_TF_CaloIso_4to123_PtEta->SetBinContent(i+1, j+1, (V1+V2+V3)/V4);
	hTauBG_MuTag_TF_CaloIso_4to123_PtEta->SetBinError(i+1, j+1, CalcFracError(V1+V2+V3, V4, TMath::Sqrt(E1*E1 + E2*E2 + E3*E3), E4));
      }
      if((V4+V3+V2)>0){
	hTauBG_MuTag_TF_CaloIso_432to0_PtEta->SetBinContent(i+1, j+1, V0/(V4+V3+V2));
	hTauBG_MuTag_TF_CaloIso_432to0_PtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4+V3+V2, E0, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hTauBG_MuTag_TF_CaloIso_432to1_PtEta->SetBinContent(i+1, j+1, V1/(V4+V3+V2));
	hTauBG_MuTag_TF_CaloIso_432to1_PtEta->SetBinError(i+1, j+1, CalcFracError(V1, V4+V3+V2, E1, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hTauBG_MuTag_TF_CaloIso_432to01_PtEta->SetBinContent(i+1, j+1, (V0+V1)/(V4+V3+V2));
	hTauBG_MuTag_TF_CaloIso_432to01_PtEta->SetBinError(i+1, j+1, CalcFracError(V0+V1, V4+V3+V2, TMath::Sqrt(E0*E0 + E1*E1), TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
      }
    }
  }
  for(int i=0;i<(hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->GetNbinsX());i++){
    for(int j=0;j<(hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->GetNbinsY());j++){
      double V0 = hTauBG_MuTag_CaloIso_InvPtEta[0]->GetBinContent(i+1, j+1);
      double V1 = hTauBG_MuTag_CaloIso_InvPtEta[1]->GetBinContent(i+1, j+1);
      double V2 = hTauBG_MuTag_CaloIso_InvPtEta[2]->GetBinContent(i+1, j+1);
      double V3 = hTauBG_MuTag_CaloIso_InvPtEta[3]->GetBinContent(i+1, j+1);
      double V4 = hTauBG_MuTag_CaloIso_InvPtEta[4]->GetBinContent(i+1, j+1);

      double E0 = hTauBG_MuTag_CaloIso_InvPtEta[0]->GetBinError(i+1, j+1);
      double E1 = hTauBG_MuTag_CaloIso_InvPtEta[1]->GetBinError(i+1, j+1);
      double E2 = hTauBG_MuTag_CaloIso_InvPtEta[2]->GetBinError(i+1, j+1);
      double E3 = hTauBG_MuTag_CaloIso_InvPtEta[3]->GetBinError(i+1, j+1);
      double E4 = hTauBG_MuTag_CaloIso_InvPtEta[4]->GetBinError(i+1, j+1);
      if(V4>0){
	hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->SetBinContent(i+1, j+1, V0/V4);
	hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4, E0, E4));
	hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta->SetBinContent(i+1, j+1, (V1+V2+V3)/V4);
	hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V1+V2+V3, V4, TMath::Sqrt(E1*E1 + E2*E2 + E3*E3), E4));
      }
      if((V4+V3+V2)>0){
	hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta->SetBinContent(i+1, j+1, V0/(V4+V3+V2));
	hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V0, V4+V3+V2, E0, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta->SetBinContent(i+1, j+1, V1/(V4+V3+V2));
	hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V1, V4+V3+V2, E1, TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
	hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta->SetBinContent(i+1, j+1, (V0+V1)/(V4+V3+V2));
	hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta->SetBinError(i+1, j+1, CalcFracError(V0+V1, V4+V3+V2, TMath::Sqrt(E0*E0 + E1*E1), TMath::Sqrt(E4*E4 + E3*E3 + E2*E2)));
      }
    }
  }
  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogx(true);
  pad0->SetLogz(true);
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  pad0->SetLogx(false);
  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta->GetYaxis()->SetTitleOffset(0.9);
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta->Draw("colz0");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  delete pad0; pad0=NULL;

  tfOut->cd();
  hTauBG_MuTag_TF_CaloIso_4to0_PtEta->Write();
  hTauBG_MuTag_TF_CaloIso_4to123_PtEta->Write();
  hTauBG_MuTag_TF_CaloIso_432to0_PtEta->Write();
  hTauBG_MuTag_TF_CaloIso_432to1_PtEta->Write();
  hTauBG_MuTag_TF_CaloIso_432to01_PtEta->Write();

  hTauBG_MuTag_TF_CaloIso_4to0_InvPtEta->Write();
  hTauBG_MuTag_TF_CaloIso_4to123_InvPtEta->Write();
  hTauBG_MuTag_TF_CaloIso_432to0_InvPtEta->Write();
  hTauBG_MuTag_TF_CaloIso_432to1_InvPtEta->Write();
  hTauBG_MuTag_TF_CaloIso_432to01_InvPtEta->Write();
  gROOT->cd();

  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");
  tfOut->Close();

  std::cout << "finish MakeInput " << std::endl;

  return 0;  
}

THStack *GetHStack(std::string HistName, int iChain, int iHist, int nRebin, int iOpt){
  THStack *ret = new THStack(Form("%s_SM", HistName.c_str()), "");
  if(fSMBG==false)
    return ret;

  TH1D *hSM[nSM];
  int OrderSM[nSM];
  double IntegralSM[nSM];

  for(int iSM=0;iSM<nSM;iSM++){
    hSM[iSM] = (TH1D *)(iChain==-1 ? myHistData[0]->GetHist1D(HistName, iHist) : myHistData[0]->GetHistChain1D(HistName, iChain, iHist))->Clone(Form("%s_%s", HistName.c_str(), SMName[iSM].c_str()));
    hSM[iSM]->Reset();
    if(iOpt==0){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(mySM[iSM][iMC16]->size());iDSID++){
	  //double SF = IntLumi[iMC16]/((iSM==(nSM-1) ? myHistSM[iSM][iMC16]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	  double SF = IntLumi[iMC16]/(myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  if(iSM==0 && iMC16==2)
	    SF *= CorrectFactor[iDSID];
	  hSM[iSM]->Add((iChain==-1 ? myHistSM[iSM][iMC16]->at(iDSID)->GetHist1D(HistName, iHist) : myHistSM[iSM][iMC16]->at(iDSID)->GetHistChain1D(HistName, iChain, iHist)), SF);
	}// for iDSID
      }// for iMC16
    }else{
      for(unsigned int iDSID=0;iDSID<(mySM[iSM][iOpt-1]->size());iDSID++){
	//double SF = IntLumi[iOpt-1]/((iSM==(nSM-1) ? myHistSM[iSM][iOpt-1]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	double SF = IntLumi[iOpt-1]/(myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	if(iSM==0 && iOpt==3)
	  SF *= CorrectFactor[iDSID];
	hSM[iSM]->Add((iChain==-1 ? myHistSM[iSM][iOpt-1]->at(iDSID)->GetHist1D(HistName, iHist) : myHistSM[iSM][iOpt-1]->at(iDSID)->GetHistChain1D(HistName, iChain, iHist)), SF);
      }// for iDSID
    }

    hSM[iSM]->SetFillColor(ColSM[iSM]);
    hSM[iSM]->Rebin(nRebin);
    FillOverflowBin(hSM[iSM]);

    OrderSM[iSM] = iSM;
    IntegralSM[iSM] = hSM[iSM]->Integral();
    //    ret->Add(hSM[iSM]);

    //if(iOpt==0 && HistName=="hOld4LKinematics_CutFlow"){
    //std::cout << "iSM = " << iSM << std::setw(15) << SMName[iSM] << ", ";
    //std::cout << std::setw(15) << hSM[iSM]->Integral(9, 9);
    //std::cout << " +- " << std::setw(15) << hSM[iSM]->Integral(9,9) << std::endl;
    //      std::cout << std::setw(15) << hSM[iSM]->GetBinContent(9);
    //      std::cout << " +- " << std::setw(15) << hSM[iSM]->GetBinError(9) << std::endl;
    //}// for iOpt
  }// for iSM

  for(int i=0;i<nSM-1;i++){
    for(int j=0;j<nSM-1-i;j++){
      if(IntegralSM[j] > IntegralSM[j+1]){
	double tmp = IntegralSM[j];
	IntegralSM[j]   = IntegralSM[j+1];
	IntegralSM[j+1] = tmp;

	int tmpInt = OrderSM[j];
	OrderSM[j]   = OrderSM[j+1];
	OrderSM[j+1] = tmpInt;
      }
    }// for j
  }// for i
  for(int iSM=0;iSM<nSM;iSM++){
    tlSM->AddEntry(hSM[OrderSM[iSM]], SMName[OrderSM[iSM]].c_str(), "f");
    ret->Add(hSM[OrderSM[iSM]]);
  }// for iSM
  
  ofsSMFrac << "********** " << HistName.c_str() << ", " << iHist << ", " << nRebin << ", " << iOpt << std::endl;
  for(int iSM=0;iSM<nSM;iSM++){
    ofsSMFrac << "iSM = " << iSM << std::setw(15) << SMName[OrderSM[iSM]] << ", ";
    if(HistName.find("ZMass") != std::string::npos){
      double Total = ((TH1D *)ret->GetStack()->Last())->Integral(41, 50);
      if(Total>0)
	ofsSMFrac << std::setw(15) << Form("%.1lf", hSM[OrderSM[iSM]]->Integral(41, 50)) << std::setw(10) << Form("%.1lf", 100.0*hSM[OrderSM[iSM]]->Integral(41, 50)/Total) << std::endl;
      else
	ofsSMFrac << std::setw(15) << "---" << std::endl;
    }else{
      double Total = ((TH1D *)ret->GetStack()->Last())->Integral();
      if(Total>0)
	ofsSMFrac << std::setw(15) << Form("%.1lf", hSM[OrderSM[iSM]]->Integral()) << std::setw(10) << Form("%.1lf", 100.0*hSM[OrderSM[iSM]]->Integral()/Total) << std::endl;
      else
	ofsSMFrac << std::setw(15) << "---" << std::endl;
    }
  }// for iSM

  return ret;
}

THStack *GetHStack(std::string TreeName, std::string BranchName, std::string ValName, double Scale, std::string myCut, int iOpt){
  THStack *ret = new THStack(Form("%s_%s_SM", TreeName.c_str(), BranchName.c_str()), "");
  if(fSMBG==false)
    return ret;

  TH1D *hTMPSM = (TH1D *)hData1D->Clone("hTMPSM");
  TH1D *hSM[nSM];
  int OrderSM[nSM];
  double IntegralSM[nSM];
  std::string CutAndWeight="";
  if(TreeName=="Old4L_Strong_singleEleCR" || TreeName=="Old4L_EWK_singleEleCR"){
    CutAndWeight = Form("(weightXsec*weightMCweight*weightPileupReweighting*weightElectronSF)*(%s)", myCut.c_str());
  }else if(TreeName=="Old4L_Strong_singleMuCR" || TreeName=="Old4L_EWK_singleMuCR"){
    CutAndWeight = Form("(weightXsec*weightMCweight*weightPileupReweighting*weightMuonSF)*(%s)", myCut.c_str());
  }else{
    CutAndWeight = Form("(weightXsec*weightMCweight*weightPileupReweighting)*(%s)", myCut.c_str());    
  }

  for(int iSM=0;iSM<nSM;iSM++){
    hSM[iSM] = (TH1D *)hData1D->Clone(Form("hSM_%s", SMName[iSM].c_str()));
    hSM[iSM]->Reset();
    if(iOpt==0){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(mySM[iSM][iMC16]->size());iDSID++){
	  hTMPSM->Reset();
	  //double SF = IntLumi[iMC16]/((iSM==(nSM-1) ? myHistSM[iSM][iMC16]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	  double SF = IntLumi[iMC16]/(myHistSM[iSM][iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  if(iSM==0 && iMC16==2)
	    SF *= CorrectFactor[iDSID];
	  mySM[iSM][iMC16]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMPSM", BranchName.c_str(), ValName.c_str(), Scale), Form("%s", CutAndWeight.c_str()));
	  hSM[iSM]->Add(hTMPSM, SF);
	}// for iDSID
      }// for iMC16
    }else{
      for(unsigned int iDSID=0;iDSID<(mySM[iSM][iOpt-1]->size());iDSID++){
	hTMPSM->Reset();
	//double SF = IntLumi[iOpt-1]/((iSM==(nSM-1) ? myHistSM[iSM][iOpt-1]->at(iDSID)->hnEventsProcessedBCK : myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK)->GetBinContent(1));
	double SF = IntLumi[iOpt-1]/(myHistSM[iSM][iOpt-1]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	if(iSM==0 && iOpt==3)
	  SF *= CorrectFactor[iDSID];
	mySM[iSM][iOpt-1]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMPSM", BranchName.c_str(), ValName.c_str(), Scale), Form("%s", CutAndWeight.c_str()));
	hSM[iSM]->Add(hTMPSM, SF);
      }// for iDSID
    }

    hSM[iSM]->SetFillColor(ColSM[iSM]);
    hSM[iSM]->SetLineWidth(1);
    FillOverflowBin(hSM[iSM]);
    OrderSM[iSM] = iSM;
    IntegralSM[iSM] = hSM[iSM]->Integral();
    //ret->Add(hSM[iSM]);
  }// for iSM

  for(int i=0;i<nSM-1;i++){
    for(int j=0;j<nSM-1-i;j++){
      if(IntegralSM[j] > IntegralSM[j+1]){
	double tmp = IntegralSM[j];
	IntegralSM[j]   = IntegralSM[j+1];
	IntegralSM[j+1] = tmp;

	int tmpInt = OrderSM[j];
	OrderSM[j]   = OrderSM[j+1];
	OrderSM[j+1] = tmpInt;
      }
    }// for j
  }// for i
  for(int iSM=0;iSM<nSM;iSM++){
    tlSM->AddEntry(hSM[OrderSM[iSM]], SMName[OrderSM[iSM]].c_str(), "f");
    ret->Add(hSM[OrderSM[iSM]]);
  }// for iSM

  ofsSMFrac << "********** " << TreeName.c_str() << ", " << BranchName.c_str() << ", " << ValName.c_str() << ", " << myCut.c_str() << std::endl;
  for(int iSM=0;iSM<nSM;iSM++){
    ofsSMFrac << "iSM = " << iSM << std::setw(15) << SMName[OrderSM[iSM]] << ", ";
    double Total = ((TH1D *)ret->GetStack()->Last())->Integral();
    if(Total>0)
      ofsSMFrac << std::setw(15) << Form("%.1lf", hSM[OrderSM[iSM]]->Integral()) << std::setw(10) << Form("%.1lf", 100.0*hSM[OrderSM[iSM]]->Integral()/Total) << std::endl;
    else
      ofsSMFrac << std::setw(15) << "---" << std::endl;
  }// for iSM

  delete hTMPSM; hTMPSM=NULL;

  return ret;
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

void SmearPt(TH1D *hHoge, double pt, double weight=1.0){
 for(int i=0;i<nLogPt;i++){
    double ptLow = XBinsLogPt[i];
    double ptUp  = XBinsLogPt[i+1];
    
    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = fSmearInt->Eval(qoverptLow);
    double w1Up  = fSmearInt->Eval(qoverptUp);
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = fSmearInt->Eval(qoverptLow);
    double w2Up  = fSmearInt->Eval(qoverptUp);
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 

int DrawTH1RatioPlot(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist, double Scale, std::string myCut, int iOpt, int SameOpt, bool fEWK){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  
  delete hData1D;
  hData1D = (TH1D *)hHist->Clone("hData1D");
  hData1D->Reset();
  TH1D *hTMP = (TH1D *)hData1D->Clone("hTMP");


  if((TreeName=="Old4L_Strong_SR" || TreeName=="Old4L_EWK_SR") && fMaskPt){
    myData[iOpt]->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hData1D", BranchName.c_str(), ValName.c_str(), Scale), 
							  Form("%s*(DisappearingTracks.p4.Pt()/1000.0 < %lf)", myCut.c_str(), MaskPt)); // for blind
  }else{
    myData[iOpt]->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hData1D", BranchName.c_str(), ValName.c_str(), Scale), myCut.c_str());  
  }
  tl->AddEntry(hData1D, DataTag[iOpt].c_str(), "PE");
  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  hData1D->SetBinErrorOption(TH1::kPoisson);
  FillOverflowBinForData(hData1D);

  delete hStack_SM;
  hStack_SM = GetHStack(TreeName, BranchName, ValName, Scale, myCut, iOpt);

  hFrame->Draw();
  hStack_SM->Draw("samehist");
  hData1D->Draw("sameEP");

  if(fSMBG)
    DrawRatioPlot();
  
  delete hTMP; hTMP=NULL;

  return 0;
}

int DrawTH1RatioPlot(std::string HistName, int iHist, int nRebin, int iOpt, int SameOpt, bool fEWK){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  
  hFrame->Draw();
  delete hData1D;
  hData1D = (TH1D *)myHistData[0]->GetHist1D(HistName, iHist)->Clone("hData1D");
  hData1D->Reset();
  hData1D->Add(myHistData[iOpt]->GetHist1D(HistName, iHist), 1.0);
  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  hData1D->SetBinErrorOption(TH1::kPoisson);
  tl->AddEntry(hData1D, DataTag[iOpt].c_str(), "PE");
  
  delete hStack_SM;
  hStack_SM = GetHStack(HistName, -1, iHist, nRebin, iOpt);
  hStack_SM->Draw("samehist");

  hData1D->Rebin(nRebin);
  FillOverflowBinForData(hData1D);
  hData1D->Draw("sameEP");


  if(fSMBG)
    DrawRatioPlot();
  return 0;
}

int DrawChainTH1RatioPlot(std::string HistName, int iChain, int iHist, int nRebin, int iOpt){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  
  hFrame->Draw();
  delete hData1D;
  hData1D = (TH1D *)myHistData[0]->GetHistChain1D(HistName, iChain, iHist)->Clone("hData1D");
  hData1D->Reset();

  hData1D->Add(myHistData[iOpt]->GetHistChain1D(HistName, iChain, iHist), 1.0);
  hData1D->SetMarkerSize(1.0);
  hData1D->SetMarkerStyle(8);
  hData1D->SetMarkerColor(kBlack);
  hData1D->SetLineColor(kBlack);
  hData1D->SetBinErrorOption(TH1::kPoisson);
  tl->AddEntry(hData1D, DataTag[iOpt].c_str(), "PE");
  
  delete hStack_SM;
  hStack_SM = GetHStack(HistName, iChain, iHist, nRebin, iOpt);
  hStack_SM->Draw("samehist");

  hData1D->Rebin(nRebin);
  FillOverflowBinForData(hData1D);
  hData1D->Draw("sameEP");

  if(fSMBG)
    DrawRatioPlot();

  return 0;
}

int FillOverflowBin(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  double LowVal      = h1->GetBinContent(0);
  double LowError    = h1->GetBinError(0);
  double LowOrgVal   = h1->GetBinContent(1);
  double LowOrgError = h1->GetBinError(1);
  double HighVal      = h1->GetBinContent(nBinsX+1);
  double HighError    = h1->GetBinError(nBinsX+1);
  double HighOrgVal   = h1->GetBinContent(nBinsX);
  double HighOrgError = h1->GetBinError(nBinsX);

  h1->SetBinError(1,   TMath::Sqrt(LowError*LowError + LowOrgError*LowOrgError));
  h1->SetBinContent(1, LowVal + LowOrgVal);

  h1->SetBinError(nBinsX, TMath::Sqrt(HighError*HighError + HighOrgError*HighOrgError));
  h1->SetBinContent(nBinsX, HighVal + HighOrgVal);

  return 0;
}

int FillOverflowBinForData(TH1D *h1){
  int nBinsX = h1->GetNbinsX();
  h1->AddBinContent(1, h1->GetBinContent(0));
  h1->SetBinContent(0, 0.0);
  h1->AddBinContent(nBinsX  , h1->GetBinContent(nBinsX+1));
  h1->SetBinContent(nBinsX+1, 0.0);

  return 0;
}

int SetupCanvas(int iOption){
  c0->cd();
  if(fSMBG)
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.35, 1.0, 1.0);
  else
    pad1[0] = new TPad("pad1_0", "pad1_0", 0.0, 0.0, 1.0, 1.0);
  pad1[1] = new TPad("pad1_1", "pad1_1", 0.0, 0.0, 1.0, 0.35);

  hFrame->GetXaxis()->SetNoExponent(true);
  
  pad1[0]->SetLogy(false);
  pad1[0]->SetLogx(false);
  if(fSMBG){
    hFrame->GetYaxis()->SetTitleOffset(0.6);
    hFrame->GetYaxis()->SetTitleSize(0.07);
    hFrame->GetYaxis()->SetLabelSize(0.07);
    pad1[0]->SetBottomMargin(0);
    pad1[0]->SetLeftMargin(0.12);
    pad1[1]->SetTopMargin(0);
    pad1[1]->SetLeftMargin(0.12);
    pad1[1]->SetBottomMargin(0.35);
  }else{
    hFrame->GetYaxis()->SetTitleOffset(0.8);
    hFrame->GetYaxis()->SetTitleSize(0.06);
    hFrame->GetYaxis()->SetLabelSize(0.06);
    pad1[0]->SetLeftMargin(0.12);
    pad1[0]->SetRightMargin(0.05);
  }
  hRatioFrame->GetXaxis()->SetLabelSize(0.15);
  hRatioFrame->GetXaxis()->SetTitleSize(0.15);
  hRatioFrame->GetXaxis()->SetTitleOffset(1.0);
  hRatioFrame->GetYaxis()->SetLabelSize(0.13);
  hRatioFrame->GetYaxis()->SetTitleSize(0.13);
  hRatioFrame->GetYaxis()->SetTitleOffset(0.3);
  hRatioFrame->GetYaxis()->SetNdivisions(505);
  hRatioFrame->GetXaxis()->SetNoExponent(true);

  if(iOption==0){
    // right top : data+signal+bg
    delete tl;
    tl = new TLegend(0.20, 0.93-0.18, 0.82, 0.93-0.10);
    //    tl = new TLegend(0.30, 0.93-0.18, 0.92, 0.93-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1, 0.9, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==1){
    // right top : signal+bg or data+bg
    delete tl;
    tl = new TLegend(0.50, 0.94-0.18, 0.92, 0.94-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1, 0.9, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==2){
    // left top : data+bg
    delete tl;
    tl = new TLegend(0.50-0.35, 0.94-0.18, 0.92-0.35, 0.94-0.10);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50-0.35, 0.94-0.1, 0.9-0.35, 0.94);
    tlSM->SetNColumns(5);
  }else if(iOption==3){
    // right bottom : data+bg
    delete tl;
    tl = new TLegend(0.50, 0.94-0.18-0.12, 0.92, 0.94-0.10-0.12);
    tl->SetNColumns(3);
    delete tlSM;
    tlSM = new TLegend(0.50, 0.94-0.1-0.12, 0.90, 0.94-0.12);
    tlSM->SetNColumns(5);
  }

  return 0;
}

int DrawRatioPlot(void){
  c0->cd();
  pad1[1]->SetGridy();
  pad1[1]->Draw();
  pad1[1]->cd();
  hRatioFrame->Draw();
  TGraphAsymmErrors *gRatioToSM = new TGraphAsymmErrors();
  //TH1D *hRatioToSM  = (TH1D *)hData1D->Clone("hRa
  TH1D *hMCStat     = (TH1D *)hData1D->Clone("hMCStat");
  //TH1D *hRatioToSM  = (TH1D *)hData1D->Clone(Form("%s_%s_RatioToSM", TreeName.c_str(), BranchName.c_str()));
  //TH1D *hMCStat     = (TH1D *)hData1D->Clone(Form("%s_%s_MCStat", TreeName.c_str(), BranchName.c_str()));
  hMCStat->Reset();
  int nBinsX = hData1D->GetNbinsX();
  for(int i=0;i<nBinsX;i++){
    double BinVal   = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinContent(i+1);
    double BinError = ((TH1D *)hStack_SM->GetStack()->Last())->GetBinError(i+1);
    if(BinVal==0)
      continue;
    gRatioToSM->SetPoint(i, hData1D->GetBinCenter(i+1), hData1D->GetBinContent(i+1)/BinVal);
    gRatioToSM->SetPointEYlow(i, hData1D->GetBinErrorLow(i+1)/BinVal);
    gRatioToSM->SetPointEYhigh(i, hData1D->GetBinErrorUp(i+1)/BinVal);
    gRatioToSM->SetPointEXlow(i, hData1D->GetBinCenter(i+1) - hData1D->GetBinLowEdge(i+1));
    gRatioToSM->SetPointEXhigh(i, hData1D->GetBinLowEdge(i+2) - hData1D->GetBinCenter(i+1));
    
    hMCStat->SetBinError(i+1, BinError/BinVal);
    hMCStat->SetBinContent(i+1, 1);
  }// for ibin
  gRatioToSM->SetMarkerSize(1.0);
  gRatioToSM->SetMarkerStyle(8);
  gRatioToSM->SetMarkerColor(kBlack);
  gRatioToSM->Draw("sameEP");
  hMCStat->SetFillColor(kRed);
  hMCStat->SetFillStyle(3002);
  hMCStat->SetMarkerSize(0);
  hMCStat->Draw("sameE2");
  
  c0->cd();
  pad1[0]->RedrawAxis();
  c0->cd();

  return 0;
}

/*
int CompareData(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist, double Scale){

  return 0;
}
*/
/*
int CompareData(std::string HistName, int iHist, int nRebin){
  c0->Clear();
  c0->cd();
  pad1[0]->Draw();
  pad1[0]->cd();
  TH1D *hCompData[nData]={NULL};
  hFrame->Draw();
  for(int i=0;i<nData;i++){
    hCompData[i] = (TH1D *)myHistData[i]->GetHist1D(HistName, iHist)->Clone(Form("%s_CompData_%d", HistName.c_str(), i));
    hCompData[i]->SetMarkerSize(1.0);
    hCompData[i]->SetMarkerStyle(8);
    hCompData[i]->SetMarkerColor(ColData[i]);
    hCompData[i]->SetLineColor(ColData[i]);
    hCompData[i]->SetBinErrorOption(TH1::kPoisson);
    tl->AddEntry(hCompData[i], myData[i]->OutputFileName.c_str(), "PE");
  }


  return 0;
}
*/
  /*
  pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();
  pad0->SetLogy(true);
  pad0->SetLogx(true);

  delete tlSM;
  tlSM = new TLegend(0.50, 0.94-0.2, 0.9, 0.94);
  tlSM->SetNColumns(3);
  TH1D *hSmearedPt[9];
  for(int i=0;i<9;i++){
    double iPt = 4.0*TMath::Power(5.0, i*0.5); // GeV
    hSmearedPt[i] = new TH1D(Form("hSmearedPt_%d", i), ";smeared track pT [GeV];", nLogPt, XBinsLogPt);
    SmearPt(hSmearedPt[i], iPt, 1.0);
    hSmearedPt[i]->SetLineWidth(3);
    hSmearedPt[i]->SetLineColor(ColSM[i+1]);
    FillOverflowBinForData(hSmearedPt[i]);
    hSmearedPt[i]->Draw("hist");
    tlSM->AddEntry(hSmearedPt[i], Form("%.0lf GeV", iPt), "l");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }// for i
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";smeared track p_{T} [GeV]", 100, 20, 12500, 100, 1e-4 ,1.0);
  hFrame->Draw();
  for(int i=0;i<9;i++){
    hSmearedPt[i]->Draw("samehist");
  }
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";smeared track p_{T} [GeV]", 100, 20, 12500, 100, 5e-6 , 50.0);
  hFrame->Draw();
  for(int i=0;i<9;i++){
    hSmearedPt[i]->Scale(1.0/hSmearedPt[i]->GetBinContent(1));
    hSmearedPt[i]->Draw("samehist");
  }
  tlSM->Draw("same");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");  
  
  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  return 0;  
  */


double CalcFracError(double vNume, double vDenom, double eNume, double eDenom){
  return TMath::Sqrt((eNume/vDenom)*(eNume/vDenom) + (vNume*eDenom/(vDenom*vDenom))*(vNume*eDenom/(vDenom*vDenom)));
}

int Rebin2D_For18Bin(TH2D *h18Bin, TH2D *hOS, TH2D *hSS){
  double OS_E;
  double OS_V;
  double SS_E;
  double SS_V;

  for(int iEta=0;iEta<(h18Bin->GetNbinsY());iEta++){
    for(int iPt=0;iPt<17;iPt++){
      OS_V = hOS->IntegralAndError(iPt*2+1, iPt*2+2, iEta+1, iEta+1, OS_E);
      SS_V = hSS->IntegralAndError(iPt*2+1, iPt*2+2, iEta+1, iEta+1, SS_E);
      h18Bin->SetBinError(iPt+1, iEta+1, TMath::Sqrt(OS_E*OS_E + SS_E*SS_E));
      if(doSSRemoval){
	h18Bin->SetBinContent(iPt+1, iEta+1, OS_V - SS_V);
      }else{
	h18Bin->SetBinContent(iPt+1, iEta+1, OS_V + SS_V);
      }
    }
    
    OS_V = hOS->IntegralAndError(35, 90, iEta+1, iEta+1, OS_E);
    SS_V = hSS->IntegralAndError(35, 90, iEta+1, iEta+1, SS_E);
    h18Bin->SetBinError(18, iEta+1, TMath::Sqrt(OS_E*OS_E + SS_E*SS_E));
    if(doSSRemoval){      
      h18Bin->SetBinContent(18, iEta+1, OS_V - SS_V);
    }else{
      h18Bin->SetBinContent(18, iEta+1, OS_V + SS_V);
    }
  }// for iEta

  return 0;
}

int Rebin2D_For10Bin(TH2D *h10Bin, TH2D *hOS, TH2D *hSS){
  double OS_E;
  double OS_V;
  double SS_E;
  double SS_V;

  for(int iEta=0;iEta<(h10Bin->GetNbinsY());iEta++){
    for(int iPt=0;iPt<9;iPt++){
      OS_V = hOS->IntegralAndError(iPt*2+1, iPt*2+2, iEta+1, iEta+1, OS_E);
      SS_V = hSS->IntegralAndError(iPt*2+1, iPt*2+2, iEta+1, iEta+1, SS_E);
      h10Bin->SetBinError(iPt+1, iEta+1, TMath::Sqrt(OS_E*OS_E + SS_E*SS_E));
      if(doSSRemoval){      
	h10Bin->SetBinContent(iPt+1, iEta+1, OS_V - SS_V);
      }else{
	h10Bin->SetBinContent(iPt+1, iEta+1, OS_V + SS_V);
      }
    }
    
    OS_V = hOS->IntegralAndError(19, 90, iEta+1, iEta+1, OS_E);
    SS_V = hSS->IntegralAndError(19, 90, iEta+1, iEta+1, SS_E);
    h10Bin->SetBinError(10, iEta+1, TMath::Sqrt(OS_E*OS_E + SS_E*SS_E));
    if(doSSRemoval){      
      h10Bin->SetBinContent(10, iEta+1, OS_V - SS_V);
    }else{
      h10Bin->SetBinContent(10, iEta+1, OS_V + SS_V);
    }
  }// for iEta

  return 0;
}

int Rebin1D_For10Bin(TH1D *h10Bin, TH1D *hOS, TH1D *hSS){
  double OS_E;
  double OS_V;
  double SS_E;
  double SS_V;

  for(int iPt=0;iPt<9;iPt++){
    OS_V = hOS->IntegralAndError(iPt*2+1, iPt*2+2, OS_E);
    SS_V = hSS->IntegralAndError(iPt*2+1, iPt*2+2, SS_E);
    h10Bin->SetBinError(iPt+1, TMath::Sqrt(OS_E*OS_E + SS_E*SS_E));
    if(doSSRemoval){      
      h10Bin->SetBinContent(iPt+1, OS_V - SS_V);
    }else{
      h10Bin->SetBinContent(iPt+1, OS_V + SS_V);
    }
  }
  
  OS_V = hOS->IntegralAndError(19, 90, OS_E);
  SS_V = hSS->IntegralAndError(19, 90, SS_E);
  h10Bin->SetBinError(10, TMath::Sqrt(OS_E*OS_E + SS_E*SS_E));
  if(doSSRemoval){      
    h10Bin->SetBinContent(10, OS_V - SS_V);
  }else{
    h10Bin->SetBinContent(10, OS_V + SS_V);
  }

  return 0;
}


