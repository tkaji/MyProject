#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TCut.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

const int nBinsMySignal = 18;
const double XBinsMySignal[nBinsMySignal+1] = { 20.0,  30.0,  40.0,  50.0,  60.0,  70.0,  80.0,  90.0, 100.0, 110.0, 
					       120.0, 130.0, 140.0, 150.0, 160.0, 170.0, 180.0, 190.0, 200.0};

bool ReadData=false;
int EWKOff;
int iChain;
TFile *tfInput=NULL;
TH2D *hTrigMETJetEff[nMETTrig];
bool fMaskPt=true;
bool fSmearTruthPt=true;
bool fUseTriggerBit=false;
double MaskPt=20.0;
TFile *tf;
bool fSMBG=false;
TH2D *hHogeHoge;
std::ofstream ofsSMFrac;
//=== Smearing function parameters ===//
const double g_par_Mean  = -4.50114e-01;
const double g_par_Sigma =  1.35062e+01;
const double g_par_Alpha =  1.68961e+00;
const int nSmearRegion = 8;
const double g_par_Mean_ele[nSmearRegion]  = {-0.203310, -0.203310, -0.203310, -0.203310, -0.146075, -0.126751, -0.205570, -0.213854};
const double g_par_Sigma_ele[nSmearRegion] = {20.944675, 19.536572, 18.330746, 17.005663, 15.424842, 14.490877, 13.900709, 14.030027};
const double g_par_Alpha_ele[nSmearRegion] = { 1.858616,  1.858616,  1.858616,  1.858616,  1.822388,  1.655727,  1.544172,  1.637405};
const double g_par_Mean_mu[nSmearRegion]  = {-0.248458, -0.248458, -0.248458, -0.248458, -0.188993, -0.133858, -0.280374, 0.001933};
const double g_par_Sigma_mu[nSmearRegion] = {16.957926, 15.542328, 14.912544, 14.836049, 14.212644, 13.638716, 13.440928, 13.208069};
const double g_par_Alpha_mu[nSmearRegion] = { 1.716832,  1.716832,  1.716832,  1.716832,  1.663774,  1.618689,  1.682396,  1.644168};
TF1 *fSmearInt_ele[nSmearRegion];
TF1 *fSmearInt_mu[nSmearRegion];

bool fSmearMuon=false;
double crystallBallIntegral(double* x, double *par);

const int nMC16 = 3;
const int nRunMC16 = 3;
TGraphAsymmErrors *gSF_DeadModule[nMC16];

TEventList *eventList=NULL;
const int nMETRegion=3;
const std::string RegName[3] = {"high-MET", "middle-MET", "low-MET"};
TFile *tfXS=NULL;
TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];
//const std::string DirSignal = "/data3/tkaji/dev_MyProject/files/NewSignal";
const std::string DirSignal = "/data3/tkaji/dev_MyProject/files/AddProdType";

TH1F *hDataStrong;
TH1F *hDataEWK;
TH1F *hBkgStrong;
TH1F *hBkgEWK;
TH1F *hDataOldEWK;
TH1F *hBkgOldEWK;
TH1F *hSignalOldEWK;

MyManager *myData;
HistManager *myHistData;

const int nSignal_Strong_0p2 = 2;
const int DSID_Strong_0p2[nSignal_Strong_0p2] = {448380, 448381};
const int GMass_Strong_0p2[nSignal_Strong_0p2] = {1400, 1800};
const int CMass_Strong_0p2[nSignal_Strong_0p2] = {1100,  900};
MyManager *mySignal_Strong_0p2[nMC16][nSignal_Strong_0p2];
HistManager *myHistSignal_Strong_0p2[nMC16][nSignal_Strong_0p2];
double SFSig_Strong_0p2[nMC16][nSignal_Strong_0p2];
TH1D *hSignal_Strong_0p2[nSignal_Strong_0p2];
TH1D *hSignalInvPt_Strong_0p2[nSignal_Strong_0p2];
TH2D *hLowMETLowPt_Strong_0p2;
TH2D *hLowMETHighPt_Strong_0p2;
TH2D *hHighMETLowPt_Strong_0p2;
TH2D *hHighMETHighPt_Strong_0p2;

TCut GetKinematicsCut(int iMETRegion=0,  // 0:SR,  1:VR(150 < MET < 200),  2:CR(100 < MET < 150)
		      int iMETType=0);   // 0:MET, 1:MET_ForEleCR, 2:MET_ForMuCR
TCut CaloVETO_0   = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
TCut CaloVETO_1   = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 10.0)";
TCut CaloVETO_123 = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 20.0)";

TCut CaloVETO_432 = "(DisappearingTracks.etclus20_topo/1000.0 > 10.0)";
TCut CaloVETO_4   = "(DisappearingTracks.etclus20_topo/1000.0 > 20.0)";
TCut FakeD0_CR    = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>10.0)";
TCut FakeD0_VR    = "(TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)>3.0 && TMath::Abs(DisappearingTracks.d0/DisappearingTracks.d0err)<10.0)";

TCut MCWeight = "(weightXsec*weightMCweight*weightPileupReweighting)";

const int nSignal_Strong_1p0 = 74;
const int DSID_Strong_1p0[nSignal_Strong_1p0]  = {448350, 448351, 448352, 448353, 448354, 448355, 448356, 448357, 448358, 448359, 
						  448360, 448361, 448362, 448363, 448364, 448365, 448366, 448367, 448368, 448369,
						  448370, 448371, 448372, 448373, 448374, 448375, 448376, 448377, 448378, 448379,
						  448550, 448551, 448552, 448553, 448554, 448555, 448556, 448557, 448558, 448559,
						  448560, 448561, 448562, 448563, 448564, 448565, 448566, 448567, 448568, 448569,
						  448570, 448571, 448572, 448573, 448574, 448575, 448576, 448577, 448578, 448579,
						  448580, 448581, 448582, 448583, 448584, 448585, 448586, 448587, 448588, 448589,
						  448590, 448591, 448592, 448593};
const int GMass_Strong_1p0[nSignal_Strong_1p0] = {   700,    800,   1000,   1000,   1200,   1200,   1200,   1200,   1400,   1400,
						    1400,   1400,   1400,   1600,   1600,   1600,   1600,   1600,   1600,   1600,
						    1800,   1800,   1800,   2000,   2000,   2000,   2200,   2200,   2200,   2200,
						     700,    800,    800,   1000,   1000,   1000,   1000,   1200,   1200,   1200,
						    1400,   1400,   1400,   1600,   1600,   1800,   1800,   1800,   1800,   1800,
						    1800,   1800,   2000,   2000,   2000,   2000,   2000,   2000,   2000,   2000,
						    2200,   2200,   2200,   2200,   2200,   2200,   2200,   2200,   2400,   2400,
						    2400,   2400,   2400,   2400};
const int CMass_Strong_1p0[nSignal_Strong_1p0] = {   650,    750,    100,    950,    100,    900,   1100,   1150,    100,    900,
						    1100,   1300,   1350,    100,    700,    900,   1100,   1300,   1500,   1550,
						     100,    700,   1750,    100,   1900,   1950,    100,   1900,   2100,   2150,
						     600,    600,    700,    300,    500,    700,    900,    300,    500,    700,
						     300,    500,    700,    300,    500,    300,    500,    900,   1100,   1300,
						    1500,   1700,    300,    500,    700,    900,   1100,   1300,   1500,   1700,
						     300,    500,    700,    900,   1100,   1300,   1500,   1700,    100,    300,
						     700,   1100,   1500,   1900};
MyManager *mySignal_Strong_1p0[nMC16][nSignal_Strong_1p0];
HistManager *myHistSignal_Strong_1p0[nMC16][nSignal_Strong_1p0];
TH1D *hData_dPhiTrackMET;
TH1D *hData_dPhiTrackMET_hadCR;
TH1D *hData_dPhiTrackMET_fakeCR;
TH1D *hData_dPhiTrackMET_singleEleCR;
TH1D *hData_dPhiTrackMET_singleMuCR;

const int nBenchmark_Strong = 10;
const int nBenchmark_EWK = 2;
TH1D *hSignal_dPhiTrackMET_Strong[nBenchmark_Strong];
TH1D *hSignal_dPhiTrackMET_EWK[nBenchmark_EWK];

double SFSig_Strong_1p0[nMC16][nSignal_Strong_1p0];
TH1D *hSignal_Strong_1p0[nSignal_Strong_1p0];
TH1D *hSignal_Strong_1p0to0p2[nSignal_Strong_1p0];
TH1D *hSignalInvPt_Strong_1p0[nSignal_Strong_1p0];
TH1D *hSignalInvPt_Strong_1p0to0p2[nSignal_Strong_1p0];
TH2D *hLowMETLowPt_Strong_1p0;
TH2D *hLowMETHighPt_Strong_1p0;
TH2D *hHighMETLowPt_Strong_1p0;
TH2D *hHighMETHighPt_Strong_1p0;
TH2D *hLowMETLowPt_Strong_1p0to0p2;
TH2D *hLowMETHighPt_Strong_1p0to0p2;
TH2D *hHighMETLowPt_Strong_1p0to0p2;
TH2D *hHighMETHighPt_Strong_1p0to0p2;

TH2D *hLowMETLowPt_EWK;
TH2D *hLowMETHighPt_EWK;
TH2D *hHighMETLowPt_EWK;
TH2D *hHighMETHighPt_EWK;

TH2D *hLowPt_Strong_0p2[nMETRegion];
TH2D *hHighPt_Strong_0p2[nMETRegion];
TH2D *hLowPt_Strong_1p0[nMETRegion];
TH2D *hHighPt_Strong_1p0[nMETRegion];
TH2D *hLowPt_Strong_1p0to0p2[nMETRegion];
TH2D *hHighPt_Strong_1p0to0p2[nMETRegion];
TH2D *hLowPt_EWK[nMETRegion];
TH2D *hHighPt_EWK[nMETRegion];

TH2D *hAll_Strong;
TH2D *hAll_EWK;
TH2D *hMETTrigger_Strong;
TH2D *hMETTrigger_EWK;
TH2D *hEffMETTrigger_Strong;
TH2D *hEffMETTrigger_EWK;
TH2D *hKinematics_Strong;
TH2D *hKinematics_EWK;
TH2D *hTracks_Strong_1p0;
TH2D *hTracks_Strong_1p0to0p2;
TH2D *hTracks_EWK;
TH2D *hEffKinematics_Strong;
TH2D *hEffKinematics_EWK;
TH2D *hEffTracks_Strong_1p0;
TH2D *hEffTracks_Strong_1p0to0p2;
TH2D *hEffTracks_EWK;

const int nSignal_Higgsino_0p2 = 5;
std::vector<int> vDSID_Higgsino_0p2[nSignal_Higgsino_0p2]      = {{448446  , 448447  , 448448  }, {448449  , 448450  , 448451  }, {448482  , 448482  , 448483  },
								  {448452  , 448453  , 448454  }, {448484  , 448484  , 448485  }};
const double CMass_Higgsino_0p2[nSignal_Higgsino_0p2]          = {120.0, 160.0, 200.0, 240.0, 300.0};
std::vector<MyManager *> *mySignal_Higgsino_0p2[nMC16][nSignal_Higgsino_0p2];
std::vector<HistManager *> *myHistSignal_Higgsino_0p2[nMC16][nSignal_Higgsino_0p2];
std::vector<double> vSFSig_Higgsino_0p2[nMC16][nSignal_Higgsino_0p2];
TH1D *hSignal_Higgsino_0p2[nSignal_Higgsino_0p2];
TH1D *hSignalInvPt_Higgsino_0p2[nSignal_Higgsino_0p2];
TH1D *hSignal_Higgsino_Lifetime_0p2[nSignal_Higgsino_0p2];
TH1D *hSignal_Higgsino_MET_0p2[nSignal_Higgsino_0p2];

const int nSignal_Higgsino_1p0 = 5;
std::vector<int> vDSID_Higgsino_1p0[nSignal_Higgsino_1p0]      = {{448455  , 448456  , 448457  }, {448458  , 448459  , 448460  }, {448488  , 448488  , 448489  },
								  {448461  , 448462  , 448463  }, {448490  , 448490  , 448491  }};
const double CMass_Higgsino_1p0[nSignal_Higgsino_1p0]          = {120.0, 160.0, 200.0, 240.0, 300.0};
std::vector<MyManager *> *mySignal_Higgsino_1p0[nMC16][nSignal_Higgsino_1p0];
std::vector<HistManager *> *myHistSignal_Higgsino_1p0[nMC16][nSignal_Higgsino_1p0];
std::vector<double> vSFSig_Higgsino_1p0[nMC16][nSignal_Higgsino_1p0];
TH1D *hSignal_Higgsino_1p0[nSignal_Higgsino_1p0];
TH1D *hSignalInvPt_Higgsino_1p0[nSignal_Higgsino_1p0];
TH1D *hSignal_Higgsino_Lifetime_1p0[nSignal_Higgsino_1p0];
TH1D *hSignal_Higgsino_MET_1p0[nSignal_Higgsino_1p0];

const int nSignal_Higgsino_4p0 = 3;
std::vector<int> vDSID_Higgsino_4p0[nSignal_Higgsino_4p0]      = {{448464  , 448465  , 448466  }, {448467  , 448468  , 448469  }, {448470  , 448471  , 448472  }};
const double CMass_Higgsino_4p0[nSignal_Higgsino_4p0]          = {120.0, 160.0, 240.0};
std::vector<MyManager *> *mySignal_Higgsino_4p0[nMC16][nSignal_Higgsino_4p0];
std::vector<HistManager *> *myHistSignal_Higgsino_4p0[nMC16][nSignal_Higgsino_4p0];
std::vector<double> vSFSig_Higgsino_4p0[nMC16][nSignal_Higgsino_4p0];
TH1D *hSignal_Higgsino_4p0[nSignal_Higgsino_4p0];
TH1D *hSignalInvPt_Higgsino_4p0[nSignal_Higgsino_4p0];
TH1D *hSignal_Higgsino_Lifetime_4p0[nSignal_Higgsino_4p0];
TH1D *hSignal_Higgsino_MET_4p0[nSignal_Higgsino_4p0];

const int nSignal_Higgsino_10p0 = 5;
std::vector<int> vDSID_Higgsino_10p0[nSignal_Higgsino_10p0]      = {{448473  , 448474  , 448475  }, {448476  , 448477  , 448478  }, {448416  , 448416  , 448417  },
								    {448479  , 448480  , 448481  }, {448418  , 448418  , 448419  }};
const double CMass_Higgsino_10p0[nSignal_Higgsino_10p0]          = {120.0, 160.0, 200.0, 240.0, 300.0};
std::vector<MyManager *> *mySignal_Higgsino_10p0[nMC16][nSignal_Higgsino_10p0];
std::vector<HistManager *> *myHistSignal_Higgsino_10p0[nMC16][nSignal_Higgsino_10p0];
std::vector<double> vSFSig_Higgsino_10p0[nMC16][nSignal_Higgsino_10p0];
TH1D *hSignal_Higgsino_10p0[nSignal_Higgsino_10p0];
TH1D *hSignalInvPt_Higgsino_10p0[nSignal_Higgsino_10p0];
TH1D *hSignal_Higgsino_Lifetime_10p0[nSignal_Higgsino_10p0];
TH1D *hSignal_Higgsino_MET_10p0[nSignal_Higgsino_10p0];

std::vector<int> vDSID_EWK_noMET = {448392, 448393};
//std::vector<int> vDSID_EWK_noMET = {};
std::vector<MyManager *> *mySignal_EWK_noMET[nMC16];
std::vector<HistManager *> *myHistSignal_EWK_noMET[nMC16];
std::vector<double> vSFSig_EWK_noMET[nMC16];
TH1D *hSignal_EWK_noMET;
TH1D *hSignalInvPt_EWK_noMET;

const int nSignal_EWK_0p2 = 10;
std::vector<int> vDSID_EWK_0p2[nSignal_EWK_0p2] = {{448390, 448391}, {448482, 448483}, {448484, 448485}, {448486, 448487}, {448302, 448303},
						   {448304, 448305}, {448394, 448395}, {448396, 448397}, {448398, 448399}, {448400, 448401}};
std::vector<double> vFilter_EWK_0p2[nSignal_EWK_0p2] = {{0.168270, 0.156013}, {0.271446, 0.260378}, {0.325306, 0.313875}, {0.355697, 0.35597}, {0.389475, 0.380619},
							{0.401724, 0.407582}, {0.419978, 0.419826}, {0.436685, 0.435000}, {0.447215, 0.442306}, {0.452403, 0.453603}};
const double CMass_EWK_0p2[nSignal_EWK_0p2]        = {            90.7,            199.7,            299.8,            400.3,            500.2,
								  599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<MyManager *> *mySignal_EWK_0p2[nMC16][nSignal_EWK_0p2];
std::vector<HistManager *> *myHistSignal_EWK_0p2[nMC16][nSignal_EWK_0p2];
std::vector<double> vSFSig_EWK_0p2[nMC16][nSignal_EWK_0p2];
TH1D *hSignal_EWK_0p2[nSignal_EWK_0p2];
TH1D *hSignalInvPt_EWK_0p2[nSignal_EWK_0p2];

const int nSignal_EWK_1p0 = 10;
std::vector<int> vDSID_EWK_1p0[nSignal_EWK_1p0] = {{448402, 448403}, {448488, 448489}, {448490, 448491}, {448492, 448493}, {448494, 448495}, 
						   {448496, 448497}, {448498, 448499}, {448404, 448405}, {448406, 448407}, {448408, 448409}};
std::vector<double> vFilter_EWK_1p0[nSignal_EWK_1p0] = {{0.168046, 0.155117}, {0.272754, 0.260745}, {0.329937, 0.313768}, {0.364487, 0.356164}, {0.385639, 0.387208},
							{0.406994, 0.402070}, {0.419776, 0.419888}, {0.427718, 0.434972}, {0.439794, 0.452383}, {0.445025, 0.457135}};
const double CMass_EWK_1p0[nSignal_EWK_1p0]        = {            90.7,            199.7,            299.8,            400.3,            500.2,
								  599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<MyManager *> *mySignal_EWK_1p0[nMC16][nSignal_EWK_1p0];
std::vector<HistManager *> *myHistSignal_EWK_1p0[nMC16][nSignal_EWK_1p0];
std::vector<double> vSFSig_EWK_1p0[nMC16][nSignal_EWK_1p0];
TH1D *hSignal_EWK_1p0[nSignal_EWK_1p0];
TH1D *hSignalInvPt_EWK_1p0[nSignal_EWK_1p0];

const int nSignal_EWK_4p0 = 7;
std::vector<int> vDSID_EWK_4p0[nSignal_EWK_4p0] = {{448500, 448501}, {448502, 448503},
						   {448306, 448307}, {448308, 448309}, {448504, 448505}, {448410, 448411}, {448412, 448413}};
std::vector<double> vFilter_EWK_4p0[nSignal_EWK_4p0] = {{0.363034, 0.356396}, {0.383109, 0.383474},
							{0.402688, 0.404920}, {0.416904, 0.421038}, {0.441118, 0.436923}, {0.444847, 0.44579}, {0.45595, 0.444661}};
const double CMass_EWK_4p0[nSignal_EWK_4p0]        = {           400.3,            500.2,
								 599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<MyManager *> *mySignal_EWK_4p0[nMC16][nSignal_EWK_4p0];
std::vector<HistManager *> *myHistSignal_EWK_4p0[nMC16][nSignal_EWK_4p0];
std::vector<double> vSFSig_EWK_4p0[nMC16][nSignal_EWK_4p0];
TH1D *hSignal_EWK_4p0[nSignal_EWK_4p0];
TH1D *hSignalInvPt_EWK_4p0[nSignal_EWK_4p0];


const int nSignal_EWK_10p0 = 10;
std::vector<int> vDSID_EWK_10p0[nSignal_EWK_10p0] = {{448414, 448415}, {448416, 448417}, {448418, 448419}, {448506, 448507}, {448508, 448509}, 
						     {448510, 448511}, {448512, 448513}, {448514, 448515}, {448516, 448517}, {448420, 448421}};
std::vector<double> vFilter_EWK_10p0[nSignal_EWK_10p0] = {{0.166412, 0.156802}, {0.274691, 0.259208}, {0.330299, 0.317009}, {0.355543, 0.356509}, {0.384207, 0.392457},
							  {0.402404, 0.394741}, {0.427389, 0.416474}, {0.432830, 0.425722}, {0.429111, 0.444475}, {0.447692, 0.458953}};
const double CMass_EWK_10p0[nSignal_EWK_10p0]        = {            90.7,            199.7,            299.8,            400.3,            500.2,
								    599.9,            700.1,            799.7,            899.6,           1000.0};


//const int nSignal_EWK_10p0 = 9;
//std::vector<int> vDSID_EWK_10p0[nSignal_EWK_10p0] = {{448414, 448415},/* {448416, 448417},*/ {448418, 448419}, {448506, 448507}, {448508, 448509}, 
//						     {448510, 448511}, {448512, 448513}, {448514, 448515}, {448516, 448517}, {448420, 448421}};
//std::vector<double> vFilter_EWK_10p0[nSignal_EWK_10p0] = {{0.166412, 0.156802},/* {0.274691, 0.259208},*/ {0.330299, 0.317009}, {0.355543, 0.356509}, {0.384207, 0.392457},
//							  {0.402404, 0.394741}, {0.427389, 0.416474}, {0.432830, 0.425722}, {0.429111, 0.444475}, {0.447692, 0.458953}};
//const double CMass_EWK_10p0[nSignal_EWK_10p0]        = {            90.7,            /*199.7,*/            299.8,            400.3,            500.2,
//								    599.9,            700.1,            799.7,            899.6,           1000.0};

std::vector<MyManager *> *mySignal_EWK_10p0[nMC16][nSignal_EWK_10p0];
std::vector<HistManager *> *myHistSignal_EWK_10p0[nMC16][nSignal_EWK_10p0];
std::vector<double> vSFSig_EWK_10p0[nMC16][nSignal_EWK_10p0];
TH1D *hSignal_EWK_10p0[nSignal_EWK_10p0];
TH1D *hSignalInvPt_EWK_10p0[nSignal_EWK_10p0];

const int nFrom10p0_Long = 9;
const double TauFrom10p0_Long[nFrom10p0_Long] = {1.3, 1.6, 2.0, 2.5, 3.2, 4.0, 5.0, 6.3, 7.9};
const int nFrom10p0 = 3;
const double TauFrom10p0[nFrom10p0] = {5.0, 6.3, 7.9};
TH1D *hSignal_EWK_From10p0[nSignal_EWK_10p0][nFrom10p0];
const int nFrom4p0 = 5;
const double TauFrom4p0[nFrom4p0] = {1.3, 1.6, 2.0, 2.5, 3.2};
TH1D *hSignal_EWK_From4p0[nSignal_EWK_4p0][nFrom4p0];
const int nFrom1p0 = 6;
const double TauFrom1p0[nFrom1p0] = {0.25, 0.32, 0.40, 0.50, 0.63, 0.79};
TH1D *hSignal_EWK_From1p0[nSignal_EWK_1p0][nFrom1p0];
const int nFrom0p2 = 13;
const double TauFrom0p2[nFrom0p2] = {0.010, 0.013, 0.016, 0.020, 0.025, 0.032, 0.040, 0.050, 0.063, 0.079, 0.100, 0.130, 0.160};
TH1D *hSignal_EWK_From0p2[nSignal_EWK_0p2][nFrom0p2];
TH1D *hSignal_Higgsino_MET_From0p2[nSignal_Higgsino_0p2][nFrom0p2];
TH1D *hSignal_Higgsino_Lifetime_From0p2[nSignal_Higgsino_0p2][nFrom0p2];
TH1D *hSignalInvPt_Higgsino_From0p2[nSignal_Higgsino_0p2][nFrom0p2];
TH1D *hSignal_Higgsino_From0p2[nSignal_Higgsino_0p2][nFrom0p2];
TH1D *hSignal_Higgsino_From1p0[nSignal_Higgsino_1p0][nFrom1p0];
TH1D *hSignal_Higgsino_From4p0[nSignal_Higgsino_4p0][nFrom4p0];
TH1D *hSignal_Higgsino_From10p0[nSignal_Higgsino_10p0][nFrom10p0];
/*
TH1D *hSignal_Higgsino_MET_From1p0[nSignal_Higgsino_0p2][nFrom1p0];
TH1D *hSignal_Higgsino_Lifetime_From1p0[nSignal_Higgsino_0p2][nFrom1p0];
TH1D *hSignal_Higgsino_From1p0[nSignal_Higgsino_0p2][nFrom1p0];
TH1D *hSignalInvPt_Higgsino_From1p0[nSignal_Higgsino_0p2][nFrom1p0];
*/
double TauBin[31] = {0.010, 0.013, 0.016, 0.020, 0.025, 0.032, 0.040, 0.050, 0.063, 0.079, 
		     0.100, 0.130, 0.160, 0.200, 0.250, 0.320, 0.400, 0.500, 0.630, 0.790,
		     1.000, 1.300, 1.600, 2.000, 2.500, 3.200, 4.000, 5.000, 6.300, 7.900, 10.000};

const std::string PlotStatus="Internal";

const int nSignal_EWK_Total = nSignal_EWK_10p0 + nSignal_EWK_4p0 + nSignal_EWK_1p0 + nSignal_EWK_0p2 + nSignal_EWK_10p0*nFrom10p0 + nSignal_EWK_4p0*nFrom4p0 + nSignal_EWK_1p0*nFrom1p0 + nSignal_EWK_0p2*nFrom0p2;

// 2015 : 0
// 2016 : 1141.73
// 2017 : 3030.55
// 2018 :  881.025
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];

TCanvas *c0;
TH1D *hTemplate_InvPt=NULL;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH1D *hTMP_InvPt=NULL;
TH1D *hTemplate_MET=NULL;
TH1D *hTMP_MET=NULL;
TH1D *hTemplate_Lifetime=NULL;
TH1D *hTMP_Lifetime=NULL;

TH1D *hMET_Filter=NULL;
TH1D *hMET_noFilter=NULL;
TH1D *hMET_Filter_JetCut=NULL;
TH1D *hMET_noFilter_JetCut=NULL;

TH2D *hFrame;
std::string PDFName;
double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau);
int GetReweightedHist_EWK(TCut myCut="1");
int GetReweightedHist_Strong(TCut myCut="1");
int GetSignalHist_Strong(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist=NULL, double Scale=1.0, TCut myCut="1");
int GetSignalHist_EWK(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist=NULL, double Scale=1.0, TCut myCut="1");

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("iChain", po::value<int>()->default_value(0), "i Chain (0 ~ 19)")
    ("EWKOff", po::value<int>()->default_value(0), "0: ON, 1: OFF")
    ("output-file,o", po::value<std::string>()->default_value("mySignal"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  iChain = vm["iChain"].as<int>();
  EWKOff = vm["EWKOff"].as<int>();

  tfInput = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    gSF_DeadModule[iMC16] = (TGraphAsymmErrors *)tfInput->Get(Form("SF_DeadModule_%s", MCName[iMC16].c_str()));
  }
  gXS_Strong_uncert = (TGraph *)tfInput->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfInput->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfInput->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfInput->Get("gXS_Higgsino_C1C1");
  
  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, g_par_Mean_ele[i]);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, g_par_Mean_mu[i]);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  eventList = new TEventList("eventList");
  hTemplate_InvPt   = new TH1D("hTemplate_InvPt",  ";1.0/p_{T} [GeV^{-1}];Tracks", 10, 0.0, 0.05);
  hTemplate_TrackPt = new TH1D("hTemplate_TrackPt", ";Track p_{T} [GeV];Tracks", nBinsMySignal, XBinsMySignal);
  hTMP = new TH1D("hTMP", ";Track p_{T} [GeV];Tracks", nBinsMySignal, XBinsMySignal);
  hTMP_InvPt = new TH1D("hTMP_InvPt",  ";1.0/p_{T} [GeV^{-1}];Tracks", 10, 0.0, 0.05);

  hTemplate_MET = new TH1D("hTemplate_MET", ";MET [GeV];", 50, 0.0, 1000.0);
  hTMP_MET = new TH1D("hTMP_MET", ";MET [GeV];", 50, 0.0, 500.0);
  hTemplate_Lifetime = new TH1D("hTemplate_Lifetime", ";lifetime [ns];", 50, 0.0, 0.5);
  hTMP_Lifetime = new TH1D("hTMP_Lifetime", ";lifetime [ns];", 50, 0.0, 0.5);

  hMET_Filter          = new TH1D("hMET_Filter"         , ";E_{T}^{miss} [GeV]", 50, 0, 500);
  hMET_noFilter        = new TH1D("hMET_noFilter"       , ";E_{T}^{miss} [GeV]", 50, 0, 500);
  hMET_Filter_JetCut   = new TH1D("hMET_Filter_JetCut"  , ";E_{T}^{miss} [GeV]", 50, 0, 500);
  hMET_noFilter_JetCut = new TH1D("hMET_noFilter_JetCut", ";E_{T}^{miss} [GeV]", 50, 0, 500);

  std::ofstream ofs_EWK(Form("List_EWK_Chain%d.txt", iChain));
  std::ofstream ofs_Higgsino(Form("List_Higgsino_Chain%d.txt", iChain));
  std::ofstream ofs_Strong0p2(Form("List_Strong0p2_Chain%d.txt", iChain));
  std::ofstream ofs_Strong1p0(Form("List_Strong1p0_Chain%d.txt", iChain));
  
  std::ofstream ofs_dPhiTrackMET(Form("ofs_dPhiTrackMET_Chain%d.txt", iChain));

  std::ofstream ofs_PtScan_EWK[15];
  std::ofstream ofs_PtScan_Higgsino[15];
  std::ofstream ofs_PtScan_Strong0p2[15];
  std::ofstream ofs_PtScan_Strong1p0[15];
  for(int i=0;i<15;i++){
    ofs_PtScan_EWK[i].open(Form("List_PtScan_EWK_Chain%d_%d", iChain, i));
    ofs_PtScan_Higgsino[i].open(Form("List_PtScan_Higgsino_Chain%d_%d", iChain, i));
    ofs_PtScan_Strong0p2[i].open(Form("List_PtScan_Strong0p2_Chain%d_%d", iChain, i));
    ofs_PtScan_Strong1p0[i].open(Form("List_PtScan_Strong1p0_Chain%d_%d", iChain, i));
  }

  if(ReadData){
    myData = new MyManager("/data3/tkaji/dev_MyProject/files/NewSignal/data15-18_13TeV.root", "Data 2015-2018");
    myData->LoadMyFile();
    myHistData = myData->myHist_Old4L;

    hData_dPhiTrackMET = new TH1D("hData_dRTrackMET", ";#DeltaR(track, MET)", 32, 0.0, 3.2);

    hData_dPhiTrackMET_hadCR  = new TH1D("hData_dRTrackMET_hadCR", ";#DeltaR(track, MET)", 32, 0.0, 3.2);
    hData_dPhiTrackMET_fakeCR = new TH1D("hData_dRTrackMET_fakeCR", ";#DeltaR(track, MET)", 32, 0.0, 3.2);
    hData_dPhiTrackMET_singleEleCR = new TH1D("hData_dRTrackMET_singleEleCR", ";#DeltaR(track, MET)", 32, 0.0, 3.2);
    hData_dPhiTrackMET_singleMuCR  = new TH1D("hData_dRTrackMET_singleMuCR", ";#DeltaR(track, MET)", 32, 0.0, 3.2);
  }
  for(int i=0;i<nBenchmark_Strong;i++){
    hSignal_dPhiTrackMET_Strong[i] = new TH1D(Form("hSignal_dRTrackMET_Strong_%d", i), ";#DeltaR(track, MET)", 32, 0.0, 3.2);
  }// for benchmark_strong

  for(int i=0;i<nBenchmark_EWK;i++){
    hSignal_dPhiTrackMET_EWK[i] = new TH1D(Form("hSignal_dRTrackMET_EWK_%d", i), ";#DeltaR(track, MET)", 32, 0.0, 3.2);
  }// for benchmark_EWK

  std::cout << "Start Loading Signal MC" << std::endl;
  /* **********   Start Loading Signal MC   ********** */
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    std::cout << MCName[iMC16] << " started" << std::endl;

    /* **********   Load Signal MC for Strong 0p2  ********** */
    for(int iSignal=0;iSignal<nSignal_Strong_0p2;iSignal++){
      std::cout << "Strong 0p2 : " << iSignal << std::endl;
      mySignal_Strong_0p2[iMC16][iSignal] = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), DSID_Strong_0p2[iSignal], MCName[iMC16].c_str()), Form("Signal (%d, %d)", GMass_Strong_0p2[iSignal], CMass_Strong_0p2[iSignal]));
      mySignal_Strong_0p2[iMC16][iSignal]->LoadMyFile();
      myHistSignal_Strong_0p2[iMC16][iSignal] = mySignal_Strong_0p2[iMC16][iSignal]->myHist_Old4L;
      //SFSig_Strong_0p2[iMC16][iSignal] = IntLumi[iMC16]*(gXS_Strong->Eval(GMass_Strong_0p2[iSignal]))/(myHistSignal_Strong_0p2[iMC16][iSignal]->hSumOfWeightsBCK->GetBinContent(1));
      SFSig_Strong_0p2[iMC16][iSignal] = IntLumi[iMC16]/(myHistSignal_Strong_0p2[iMC16][iSignal]->hSumOfWeightsBCK->GetBinContent(1));
      if(iMC16==0){
	hSignal_Strong_0p2[iSignal] = new TH1D(Form("hSignal_Strong_0p2_%d", iSignal), "", 10, 0, 10);
	hSignalInvPt_Strong_0p2[iSignal] = new TH1D(Form("hSignalInvPt_Strong_0p2_%d", iSignal), "", 10, 0, 10);
      }
    }// for iSignal

    /* **********   Load Signal MC for Strong 1p0  ********** */
    for(int iSignal=0;iSignal<nSignal_Strong_1p0;iSignal++){
      std::cout << "Strong 1p0 : " << iSignal << std::endl;
      mySignal_Strong_1p0[iMC16][iSignal] = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), DSID_Strong_1p0[iSignal], MCName[iMC16].c_str()), Form("Signal (%d, %d)", GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal]));
      mySignal_Strong_1p0[iMC16][iSignal]->LoadMyFile();
      myHistSignal_Strong_1p0[iMC16][iSignal] = mySignal_Strong_1p0[iMC16][iSignal]->myHist_Old4L;
      //SFSig_Strong_1p0[iMC16][iSignal] = IntLumi[iMC16]*(gXS_Strong->Eval(GMass_Strong_1p0[iSignal]))/(myHistSignal_Strong_1p0[iMC16][iSignal]->hSumOfWeightsBCK->GetBinContent(1));
      SFSig_Strong_1p0[iMC16][iSignal] = IntLumi[iMC16]/(myHistSignal_Strong_1p0[iMC16][iSignal]->hSumOfWeightsBCK->GetBinContent(1));
      if(iMC16==0){
	hSignal_Strong_1p0[iSignal] = new TH1D(Form("hSignal_Strong_1p0_%d", iSignal), "", 10, 0, 10);
	hSignal_Strong_1p0to0p2[iSignal] = new TH1D(Form("hSignal_Strong_1p0to0p2_%d", iSignal), "", 10, 0, 10);
	hSignalInvPt_Strong_1p0[iSignal] = new TH1D(Form("hSignalInvPt_Strong_1p0_%d", iSignal), "", 10, 0, 10);
	hSignalInvPt_Strong_1p0to0p2[iSignal] = new TH1D(Form("hSignalInvPt_Strong_1p0to0p2_%d", iSignal), "", 10, 0, 10);
      }
    }// for iSignal

    if(EWKOff==1)
      continue;

    /* **********   Load Signal MC for Higgsino 0p2  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_0p2;iSignal++){
      std::cout << "Higgsino 0p2 : " << iSignal << std::endl;
      mySignal_Higgsino_0p2[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_Higgsino_0p2[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_0p2[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_Higgsino_0p2[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_Higgsino_0p2[iSignal]));
	myDS->LoadMyFile();
	mySignal_Higgsino_0p2[iMC16][iSignal]->push_back(myDS);
	myHistSignal_Higgsino_0p2[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	if(iSignal==2){
	  if(iDSID==0){
	    double ProdTypeFrac = (myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(3))/(myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.290274907413/(gXS_EWK[0]->Eval(CMass_EWK_0p2[1])*ProdTypeFrac*myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==1){
	    double ProdTypeFrac = (myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(4))/(myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.157326848316/(gXS_EWK[0]->Eval(CMass_EWK_0p2[1])*ProdTypeFrac*myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==2){
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*0.250515174955/(gXS_EWK[1]->Eval(CMass_EWK_0p2[1])*myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}else if(iSignal==4){
	  if(iDSID==0){
	    double ProdTypeFrac = (myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(3))/(myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.064956427812/(gXS_EWK[0]->Eval(CMass_EWK_0p2[2])*ProdTypeFrac*myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==1){
	    double ProdTypeFrac = (myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(4))/(myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.0314521927711/(gXS_EWK[0]->Eval(CMass_EWK_0p2[2])*ProdTypeFrac*myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==2){
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*0.0540408247663/(gXS_EWK[1]->Eval(CMass_EWK_0p2[2])*myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}else{
	  if(iDSID==0 || iDSID==1){
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(2.0*IntLumi[iMC16]/(myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else{
	    vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_0p2[iSignal] = new TH1D(Form("hSignal_Higgsino_0p2_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom0p2;j++){
	  hSignal_Higgsino_From0p2[iSignal][j] = new TH1D(Form("hSignal_Higgsino_From0p2_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }// iMC16==0
    }// for iSignal

    /* **********   Load Signal MC for Higgsino 1p0  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_1p0;iSignal++){
      std::cout << "Higgsino 1p0 : " << iSignal << std::endl;
      mySignal_Higgsino_1p0[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_Higgsino_1p0[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_1p0[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_Higgsino_1p0[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_Higgsino_1p0[iSignal]));
	myDS->LoadMyFile();
	mySignal_Higgsino_1p0[iMC16][iSignal]->push_back(myDS);
	myHistSignal_Higgsino_1p0[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	if(iSignal==2){
	  if(iDSID==0){
	    double ProdTypeFrac = (myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(3))/(myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.290274907413/(gXS_EWK[0]->Eval(CMass_EWK_1p0[1])*ProdTypeFrac*myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==1){
	    double ProdTypeFrac = (myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(4))/(myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.157326848316/(gXS_EWK[0]->Eval(CMass_EWK_1p0[1])*ProdTypeFrac*myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==2){
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*0.250515174955/(gXS_EWK[1]->Eval(CMass_EWK_1p0[1])*myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}else if(iSignal==4){
	  if(iDSID==0){
	    double ProdTypeFrac = (myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(3))/(myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.064956427812/(gXS_EWK[0]->Eval(CMass_EWK_1p0[2])*ProdTypeFrac*myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==1){
	    double ProdTypeFrac = (myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(4))/(myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.0314521927711/(gXS_EWK[0]->Eval(CMass_EWK_1p0[2])*ProdTypeFrac*myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==2){
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*0.0540408247663/(gXS_EWK[1]->Eval(CMass_EWK_1p0[2])*myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}else{
	  if(iDSID==0 || iDSID==1){
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(2.0*IntLumi[iMC16]/(myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else{
	    vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_1p0[iSignal] = new TH1D(Form("hSignal_Higgsino_1p0_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom1p0;j++){
	  hSignal_Higgsino_From1p0[iSignal][j] = new TH1D(Form("hSignal_Higgsino_From1p0_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }// iMC16==0
    }// for iSignal

    /* **********   Load Signal MC for Higgsino 4p0  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_4p0;iSignal++){
      std::cout << "Higgsino 4p0 : " << iSignal << std::endl;
      mySignal_Higgsino_4p0[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_Higgsino_4p0[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_4p0[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_Higgsino_4p0[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_Higgsino_4p0[iSignal]));
	myDS->LoadMyFile();
	mySignal_Higgsino_4p0[iMC16][iSignal]->push_back(myDS);
	myHistSignal_Higgsino_4p0[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	if(iDSID==0 || iDSID==1){
	  vSFSig_Higgsino_4p0[iMC16][iSignal].push_back(2.0*IntLumi[iMC16]/(myHistSignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	}else{
	  vSFSig_Higgsino_4p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	}
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_4p0[iSignal] = new TH1D(Form("hSignal_Higgsino_4p0_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom4p0;j++){
	  hSignal_Higgsino_From4p0[iSignal][j] = new TH1D(Form("hSignal_Higgsino_From4p0_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }// iMC16==0
    }// for iSignal

    /* **********   Load Signal MC for Higgsino 10p0  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_10p0;iSignal++){
      std::cout << "Higgsino 10p0 : " << iSignal << std::endl;
      mySignal_Higgsino_10p0[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_Higgsino_10p0[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_10p0[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_Higgsino_10p0[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_Higgsino_10p0[iSignal]));
	myDS->LoadMyFile();
	mySignal_Higgsino_10p0[iMC16][iSignal]->push_back(myDS);
	myHistSignal_Higgsino_10p0[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	if(iSignal==2){
	  if(iDSID==0){
	    double ProdTypeFrac = (myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(3))/(myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.290274907413/(gXS_EWK[0]->Eval(CMass_EWK_10p0[1])*ProdTypeFrac*myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==1){
	    double ProdTypeFrac = (myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(4))/(myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.157326848316/(gXS_EWK[0]->Eval(CMass_EWK_10p0[1])*ProdTypeFrac*myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==2){
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*0.250515174955/(gXS_EWK[1]->Eval(CMass_EWK_10p0[1])*myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}else if(iSignal==4){
	  if(iDSID==0){
	    double ProdTypeFrac = (myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(3))/(myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.064956427812/(gXS_EWK[0]->Eval(CMass_EWK_10p0[2])*ProdTypeFrac*myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==1){
	    double ProdTypeFrac = (myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->GetBinContent(4))/(myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hProdType->Integral());
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*2.0*0.0314521927711/(gXS_EWK[0]->Eval(CMass_EWK_10p0[2])*ProdTypeFrac*myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else if(iDSID==2){
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*0.0540408247663/(gXS_EWK[1]->Eval(CMass_EWK_10p0[2])*myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}else{
	  if(iDSID==0 || iDSID==1){
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(2.0*IntLumi[iMC16]/(myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }else{
	    vSFSig_Higgsino_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	  }
	}
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_10p0[iSignal] = new TH1D(Form("hSignal_Higgsino_10p0_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom10p0;j++){
	  hSignal_Higgsino_From10p0[iSignal][j] = new TH1D(Form("hSignal_Higgsino_From10p0_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }// iMC16==0
    }// for iSignal


    /* **********   Load Signal MC for Electroweak 0p2  ********** */
    for(int iSignal=0;iSignal<nSignal_EWK_0p2;iSignal++){
      std::cout << "EWK 0p2 : " << iSignal << std::endl;
      mySignal_EWK_0p2[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_EWK_0p2[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_0p2[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_EWK_0p2[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_EWK_0p2[iSignal]));
	myDS->LoadMyFile();
	mySignal_EWK_0p2[iMC16][iSignal]->push_back(myDS);
	myHistSignal_EWK_0p2[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	//vSFSig_EWK_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_EWK[iDSID]->Eval(CMass_EWK_0p2[iSignal]))*vFilter_EWK_0p2[iSignal].at(iDSID)/(myHistSignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	vSFSig_EWK_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignalInvPt_EWK_0p2[iSignal] = new TH1D(Form("hSignalInvPt_EWK_0p2_%d", iSignal), "", 10, 0, 10);
	hSignal_EWK_0p2[iSignal] = new TH1D(Form("hSignal_EWK_0p2_%d", iSignal), "", 10, 0, 10);
	//hSignal_EWK_0p01[iSignal] = new TH1D(Form("hSignal_EWK_0p01_%d", iSignal), "", 10, 0, 10);
	//hSignal_EWK_0p04[iSignal] = new TH1D(Form("hSignal_EWK_0p04_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom0p2;j++){
	  hSignal_EWK_From0p2[iSignal][j] = new TH1D(Form("hSignal_EWK_From0p2_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }
    }// for iSignal

    /* **********   Load Signal MC for Electroweak 1p0  ********** */
    for(int iSignal=0;iSignal<nSignal_EWK_1p0;iSignal++){
      std::cout << "EWK 1p0 : " << iSignal << std::endl;
      mySignal_EWK_1p0[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_EWK_1p0[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_1p0[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_EWK_1p0[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_EWK_1p0[iSignal]));
	myDS->LoadMyFile();
	mySignal_EWK_1p0[iMC16][iSignal]->push_back(myDS);
	myHistSignal_EWK_1p0[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	//vSFSig_EWK_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_EWK[iDSID]->Eval(CMass_EWK_1p0[iSignal]))*vFilter_EWK_1p0[iSignal].at(iDSID)/(myHistSignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	vSFSig_EWK_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignalInvPt_EWK_1p0[iSignal] = new TH1D(Form("hSignalInvPt_EWK_1p0_%d", iSignal), "", 10, 0, 10);
	hSignal_EWK_1p0[iSignal] = new TH1D(Form("hSignal_EWK_1p0_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom1p0;j++){
	  hSignal_EWK_From1p0[iSignal][j] = new TH1D(Form("hSignal_EWK_From1p0_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }// iMC16==0
    }// for iSignal

    /* **********   Load Signal MC for Electroweak 4p0  ********** */
    for(int iSignal=0;iSignal<nSignal_EWK_4p0;iSignal++){
      std::cout << "EWK 4p0 : " << iSignal << std::endl;
      mySignal_EWK_4p0[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_EWK_4p0[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_4p0[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_EWK_4p0[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_EWK_4p0[iSignal]));
	myDS->LoadMyFile();
	mySignal_EWK_4p0[iMC16][iSignal]->push_back(myDS);
	myHistSignal_EWK_4p0[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	//vSFSig_EWK_4p0[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_EWK[iDSID]->Eval(CMass_EWK_4p0[iSignal]))*vFilter_EWK_4p0[iSignal].at(iDSID)/(myHistSignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	vSFSig_EWK_4p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignalInvPt_EWK_4p0[iSignal] = new TH1D(Form("hSignalInvPt_EWK_4p0_%d", iSignal), "", 10, 0, 10);
	hSignal_EWK_4p0[iSignal] = new TH1D(Form("hSignal_EWK_4p0_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom4p0;j++){
	  hSignal_EWK_From4p0[iSignal][j] = new TH1D(Form("hSignal_EWK_From4p0_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }// iMC16
    }// for iSignal

    /* **********   Load Signal MC for Electroweak 10p0  ********** */
    for(int iSignal=0;iSignal<nSignal_EWK_10p0;iSignal++){
      std::cout << "EWK 10p0 : " << iSignal << std::endl;
      mySignal_EWK_10p0[iMC16][iSignal] = new std::vector<MyManager *>;
      myHistSignal_EWK_10p0[iMC16][iSignal] = new std::vector<HistManager *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_10p0[iSignal].size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_EWK_10p0[iSignal].at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", (int)CMass_EWK_10p0[iSignal]));
	myDS->LoadMyFile();
	mySignal_EWK_10p0[iMC16][iSignal]->push_back(myDS);
	myHistSignal_EWK_10p0[iMC16][iSignal]->push_back(myDS->myHist_Old4L);
	//vSFSig_EWK_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_EWK[iDSID]->Eval(CMass_EWK_10p0[iSignal]))*vFilter_EWK_10p0[iSignal].at(iDSID)/(myHistSignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	vSFSig_EWK_10p0[iMC16][iSignal].push_back(IntLumi[iMC16]/(myHistSignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignalInvPt_EWK_10p0[iSignal] = new TH1D(Form("hSignalInvPt_EWK_10p0_%d", iSignal), "", 10, 0, 10);
	for(int j=0;j<nFrom10p0;j++){
	  hSignal_EWK_From10p0[iSignal][j] = new TH1D(Form("hSignal_EWK_From10p0_%d_%d", j, iSignal), "", 10, 0, 10);
	}
      }
    }// for iSignal

    /* **********   Load Signal MC for Electroweak no MET filter  ********** */
    std::cout << "EWK noMET" << std::endl;
    mySignal_EWK_noMET[iMC16] = new std::vector<MyManager *>;
    myHistSignal_EWK_noMET[iMC16] = new std::vector<HistManager *>;
    for(unsigned int iDSID=0;iDSID<(vDSID_EWK_noMET.size());iDSID++){
      MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID_EWK_noMET.at(iDSID), MCName[iMC16].c_str()), Form("Signal (%d GeV)", 500));
      myDS->LoadMyFile();
      mySignal_EWK_noMET[iMC16]->push_back(myDS);
      myHistSignal_EWK_noMET[iMC16]->push_back(myDS->myHist_Old4L);
      //vSFSig_EWK_noMET[iMC16].push_back(IntLumi[iMC16]*(gXS_EWK[iDSID]->Eval(500.2))/(myHistSignal_EWK_noMET[iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      vSFSig_EWK_noMET[iMC16].push_back(IntLumi[iMC16]/(myHistSignal_EWK_noMET[iMC16]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
    }// for iDSID
    if(iMC16==0){
      hSignalInvPt_EWK_noMET = new TH1D("hSignalInvPt_EWK_noMET", "", 10, 0, 10);
      hSignal_EWK_noMET = new TH1D("hSignal_EWK_noMET", "", 10, 0, 10);
    }
  }// for iMC16

  c0 = new TCanvas("c0", "c0", 800, 600);
  hFrame = new TH2D("hFrame", "", 10, 0, 10, 10, 0, 10);

  /* **********   Start Saving Plots   ********** */
  c0->Print(Form("%s_Chain%d.pdf[", PDFName.c_str(), iChain), "pdf");

  // 448302, 448303 (iSignal = 4)
  // save plot for note
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    for(int iDSID=0;iDSID<2;iDSID++){
      hTMP_MET->Reset();
      mySignal_EWK_noMET[iMC16]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("(MET.PhysObjBase.p4.Pt()/1000.0)>>hTMP_MET", MCWeight);
      hMET_Filter->Add(hTMP_MET, vSFSig_EWK_noMET[iMC16].at(iDSID));

      hTMP_MET->Reset();
      mySignal_EWK_noMET[iMC16]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("(MET.PhysObjBase.p4.Pt()/1000.0)>>hTMP_MET", MCWeight*GetKinematicsCut(5, 0));
      hMET_Filter_JetCut->Add(hTMP_MET, vSFSig_EWK_noMET[iMC16].at(iDSID));

      hTMP_MET->Reset();
      mySignal_EWK_0p2[iMC16][4]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("(MET.PhysObjBase.p4.Pt()/1000.0)>>hTMP_MET", MCWeight);
      hMET_noFilter->Add(hTMP_MET, vSFSig_EWK_0p2[iMC16][4].at(iDSID));

      hTMP_MET->Reset();
      mySignal_EWK_0p2[iMC16][4]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw("(MET.PhysObjBase.p4.Pt()/1000.0)>>hTMP_MET", MCWeight*GetKinematicsCut(5, 0));
      hMET_noFilter_JetCut->Add(hTMP_MET, vSFSig_EWK_0p2[iMC16][4].at(iDSID));

    }// for iDSID
  }// for iMC16

  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";E_{T}^{miss} [GeV];events", 50, 0, 500, 100, 2e-3, 2e1);
  hFrame->Draw();
  hMET_noFilter->SetLineColor(kRed);
  hMET_noFilter_JetCut->SetLineColor(kRed);
  hMET_noFilter_JetCut->SetLineStyle(2);
  hMET_Filter->SetLineColor(kBlue);
  hMET_Filter_JetCut->SetLineColor(kBlue);
  hMET_Filter_JetCut->SetLineStyle(2);
  hMET_noFilter->Draw("samehist");
  hMET_Filter->Draw("samehist");
  hMET_noFilter_JetCut->Draw("samehist");
  hMET_Filter_JetCut->Draw("samehist");
  ATLASLabel(0.6, 0.9, "Internal", kBlack);
  myText(0.6, 0.9-0.06, kBlack, Form("#sqrt{s} = 13 TeV, %.1lf fb^{-1}", SumLumi/1000.0));
  TLegend *tl = new TLegend(0.6, 0.4, 0.9, 0.6);
  tl->AddEntry(hMET_noFilter, "w/o MET filter", "l");
  tl->AddEntry(hMET_Filter  , "w/  MET filter", "l");
  tl->AddEntry(hMET_noFilter_JetCut, "w/o MET filter, after jet p_{T} and #Delta#phi cut", "l");
  tl->AddEntry(hMET_Filter_JetCut  , "w/  MET filter, after jet p_{T} and #Delta#phi cut", "l");
  tl->Draw("same");
  c0->SetLogy(true);
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  c0->SaveAs("METFilter.pdf", "pdf");

  double MSize = 1.0;
  for(int iMET=0;iMET<nMETRegion;iMET++){
    hLowPt_Strong_0p2[iMET]  = new TH2D(Form("hLowPt_Strong_0p2_%d", iMET) , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
    hHighPt_Strong_0p2[iMET] = new TH2D(Form("hHighPt_Strong_0p2_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
    hLowPt_Strong_1p0[iMET]  = new TH2D(Form("hLowPt_Strong_1p0_%d", iMET) , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
    hHighPt_Strong_1p0[iMET] = new TH2D(Form("hHighPt_Strong_1p0_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
    hLowPt_Strong_1p0to0p2[iMET]  = new TH2D(Form("hLowPt_Strong_1p0to0p2_%d", iMET) , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
    hHighPt_Strong_1p0to0p2[iMET] = new TH2D(Form("hHighPt_Strong_1p0to0p2_%d", iMET), ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
    hLowPt_EWK[iMET]  = new TH2D(Form("hLowPt_EWK_%d", iMET) , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
    hHighPt_EWK[iMET] = new TH2D(Form("hHighPt_EWK_%d", iMET), ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);

    hLowPt_Strong_0p2[iMET]  ->SetMarkerSize(MSize);
    hHighPt_Strong_0p2[iMET] ->SetMarkerSize(MSize);
    hLowPt_Strong_1p0[iMET]  ->SetMarkerSize(MSize);
    hHighPt_Strong_1p0[iMET] ->SetMarkerSize(MSize);
    hLowPt_Strong_1p0to0p2[iMET]  ->SetMarkerSize(MSize);
    hHighPt_Strong_1p0to0p2[iMET] ->SetMarkerSize(MSize);
    hLowPt_EWK[iMET]  ->SetMarkerSize(MSize);
    hHighPt_EWK[iMET] ->SetMarkerSize(MSize);
  }

  hLowMETLowPt_Strong_0p2   = new TH2D("hLowMETLowPt_Strong_0p2"  , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hLowMETHighPt_Strong_0p2  = new TH2D("hLowMETHighPt_Strong_0p2" , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hHighMETLowPt_Strong_0p2  = new TH2D("hHighMETLowPt_Strong_0p2" , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hHighMETHighPt_Strong_0p2 = new TH2D("hHighMETHighPt_Strong_0p2", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);

  hLowMETLowPt_Strong_1p0   = new TH2D("hLowMETLowPt_Strong_1p0"  , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hLowMETHighPt_Strong_1p0  = new TH2D("hLowMETHighPt_Strong_1p0" , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hHighMETLowPt_Strong_1p0  = new TH2D("hHighMETLowPt_Strong_1p0" , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hHighMETHighPt_Strong_1p0 = new TH2D("hHighMETHighPt_Strong_1p0", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);

  hLowMETLowPt_Strong_1p0to0p2   = new TH2D("hLowMETLowPt_Strong_1p0to0p2"  , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hLowMETHighPt_Strong_1p0to0p2  = new TH2D("hLowMETHighPt_Strong_1p0to0p2" , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hHighMETLowPt_Strong_1p0to0p2  = new TH2D("hHighMETLowPt_Strong_1p0to0p2" , ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hHighMETHighPt_Strong_1p0to0p2 = new TH2D("hHighMETHighPt_Strong_1p0to0p2", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);

  //hLowMETLowPt_EWK   = new TH2D("hLowMETLowPt_EWK"  , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 500, 0.01, 20);
  //hLowMETHighPt_EWK  = new TH2D("hLowMETHighPt_EWK" , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 500, 0.01, 20);
  //hHighMETLowPt_EWK  = new TH2D("hHighMETLowPt_EWK" , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 500, 0.01, 20);
  //hHighMETHighPt_EWK = new TH2D("hHighMETHighPt_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 500, 0.01, 20);

  hLowMETLowPt_EWK   = new TH2D("hLowMETLowPt_EWK"  , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hLowMETHighPt_EWK  = new TH2D("hLowMETHighPt_EWK" , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hHighMETLowPt_EWK  = new TH2D("hHighMETLowPt_EWK" , ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hHighMETHighPt_EWK = new TH2D("hHighMETHighPt_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);

  hAll_Strong        = new TH2D("hAll_Strong", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hMETTrigger_Strong = new TH2D("hMETTrigger_Strong", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hKinematics_Strong = new TH2D("hKinematics_Strong", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hTracks_Strong_1p0 = new TH2D("hTracks_Strong_1p0", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hTracks_Strong_1p0to0p2 = new TH2D("hTracks_Strong_1p0to0p2", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hEffMETTrigger_Strong = new TH2D("hEffMETTrigger_Strong", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hEffKinematics_Strong = new TH2D("hEffKinematics_Strong", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hEffTracks_Strong_1p0 = new TH2D("hEffTracks_Strong_1p0", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);
  hEffTracks_Strong_1p0to0p2 = new TH2D("hEffTracks_Strong_1p0to0p2", ";gluino mass [GeV];chargino mass [GeV]", 18, 450, 2250, 22, 50, 2250);

  hAll_EWK = new TH2D("hAll_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hMETTrigger_EWK = new TH2D("hMETTrigger_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hKinematics_EWK = new TH2D("hKinematics_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hTracks_EWK = new TH2D("hTracks_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hEffMETTrigger_EWK = new TH2D("hEffMETTrigger_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hEffKinematics_EWK = new TH2D("hEffKinematics_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);
  hEffTracks_EWK = new TH2D("hEffTracks_EWK", ";chargino mass [GeV];lifetime [GeV]", 10, 50, 1050, 30, TauBin);

  hLowMETLowPt_Strong_0p2   ->SetMarkerSize(MSize);
  hLowMETHighPt_Strong_0p2  ->SetMarkerSize(MSize);
  hHighMETLowPt_Strong_0p2  ->SetMarkerSize(MSize);
  hHighMETHighPt_Strong_0p2 ->SetMarkerSize(MSize);

  hLowMETLowPt_Strong_1p0   ->SetMarkerSize(MSize);
  hLowMETHighPt_Strong_1p0  ->SetMarkerSize(MSize);
  hHighMETLowPt_Strong_1p0  ->SetMarkerSize(MSize);
  hHighMETHighPt_Strong_1p0 ->SetMarkerSize(MSize);

  hLowMETLowPt_Strong_1p0to0p2   ->SetMarkerSize(MSize);
  hLowMETHighPt_Strong_1p0to0p2  ->SetMarkerSize(MSize);
  hHighMETLowPt_Strong_1p0to0p2  ->SetMarkerSize(MSize);
  hHighMETHighPt_Strong_1p0to0p2 ->SetMarkerSize(MSize);

  hLowMETLowPt_EWK   ->SetMarkerSize(MSize);
  hLowMETHighPt_EWK  ->SetMarkerSize(MSize);
  hHighMETLowPt_EWK  ->SetMarkerSize(MSize);
  hHighMETHighPt_EWK ->SetMarkerSize(MSize);

  int iPtBin=4;
  for(int iMET=0;iMET<nMETRegion;iMET++){
    /*
    GetSignalHist_Strong("Old4L_Common_SR", "DisappearingTracks", "p4.Pt()", hTemplate_TrackPt, 1.0/1000.0, GetKinematicsCut(iMET, 0));
    GetReweightedHist_Strong(GetKinematicsCut(iMET, 0));
    GetSignalHist_EWK("Old4L_Common_SR", "DisappearingTracks", "p4.Pt()", hTemplate_TrackPt, 1.0/1000.0, GetKinematicsCut(iMET, 0));
    GetReweightedHist_EWK(GetKinematicsCut(iMET, 0));
    */
    
    GetSignalHist_Strong("Old4L_Common_SR", "DisappearingTracks", "p4.Pt()", hTemplate_TrackPt, 1.0/1000.0, GetKinematicsCut(iMET, 0) && CaloVETO_0);
    GetReweightedHist_Strong(GetKinematicsCut(iMET, 0) && CaloVETO_0);
    GetSignalHist_EWK("Old4L_Common_SR", "DisappearingTracks", "p4.Pt()", hTemplate_TrackPt, 1.0/1000.0, GetKinematicsCut(iMET, 0) && CaloVETO_0);
    GetReweightedHist_EWK(GetKinematicsCut(iMET, 0) && CaloVETO_0);
    for(int iList=0;iList<nTauList_EWK;iList++){
      GetSignalHist_EWK(GetKinematicsCut(iMET, 0) && CaloVETO_0, "EWK", TauList_EWK[iList]);
    }
    for(int iSignal=0;iSignal<nSignal_Strong_0p2;iSignal++){
      hLowPt_Strong_0p2[iMET] ->Fill(GMass_Strong_0p2[iSignal], CMass_Strong_0p2[iSignal], hSignal_Strong_0p2[iSignal]->Integral(0,  iPtBin));
      hHighPt_Strong_0p2[iMET]->Fill(GMass_Strong_0p2[iSignal], CMass_Strong_0p2[iSignal], hSignal_Strong_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));    
    }// Signal_0p2

    std::cout << "Strong : " << RegName[iMET] << std::endl;
    for(int iSignal=0;iSignal<nSignal_Strong_1p0;iSignal++){    
      hLowPt_Strong_1p0[iMET]       ->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], hSignal_Strong_1p0[iSignal]->Integral(0,  iPtBin));
      hHighPt_Strong_1p0[iMET]      ->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], hSignal_Strong_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      hLowPt_Strong_1p0to0p2[iMET]  ->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], hSignal_Strong_1p0to0p2[iSignal]->Integral(0,  iPtBin));
      hHighPt_Strong_1p0to0p2[iMET] ->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], hSignal_Strong_1p0to0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      if(iSignal==10 || iSignal==47){
	for(int iBin=0;iBin<=nBinsMySignal+1;iBin++){
	  std::cout << "GMass = " << GMass_Strong_1p0[iSignal]<< ", CMass = " << CMass_Strong_1p0[iSignal] << ", iBin = " << iBin << " : " 
		    << hSignal_Strong_1p0to0p2[iSignal]->GetBinContent(iBin) << std::endl;
	}// for iBin
      }

      if(iMET!=0)
	continue;

      for(int iMC16=0;iMC16<nMC16;iMC16++){
	hAll_Strong->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], 
			  SFSig_Strong_1p0[iMC16][iSignal]*myHistSignal_Strong_1p0[iMC16][iSignal]->hSumOfWeightsBCK->GetBinContent(1));
	hMETTrigger_Strong->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], 
				 SFSig_Strong_1p0[iMC16][iSignal]*myHistSignal_Strong_1p0[iMC16][iSignal]->hKinematics_CutFlow[iChain]->GetBinContent(3));
	hKinematics_Strong->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], 
				 SFSig_Strong_1p0[iMC16][iSignal]*myHistSignal_Strong_1p0[iMC16][iSignal]->hKinematics_CutFlow[iChain]->GetBinContent(16));
      }// iMC16
      hTracks_Strong_1p0->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], hSignal_Strong_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      hTracks_Strong_1p0to0p2->Fill(GMass_Strong_1p0[iSignal], CMass_Strong_1p0[iSignal], hSignal_Strong_1p0to0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));

      //double Uncert = TMath::Sqrt(0.14*0.14 + (gXS_Strong_uncert->Eval(GMass_Strong_1p0[iSignal])/100.0)*(gXS_Strong_uncert->Eval(GMass_Strong_1p0[iSignal])/100.0));
      double Uncert = 0.14;
      ofs_Strong0p2 << GMass_Strong_1p0[iSignal] << " " << CMass_Strong_1p0[iSignal] << " " 
		    << (hSignal_Strong_1p0to0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		    << Uncert*(hSignal_Strong_1p0to0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;

      ofs_Strong1p0 << GMass_Strong_1p0[iSignal] << " " << CMass_Strong_1p0[iSignal] << " " 
		    << (hSignal_Strong_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		    << Uncert*(hSignal_Strong_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;

      for(int i=0;i<15;i++){
	ofs_PtScan_Strong0p2[i] << GMass_Strong_1p0[iSignal] << " " << CMass_Strong_1p0[iSignal] << " " 
				<< (hSignal_Strong_1p0to0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
				<< Uncert*(hSignal_Strong_1p0to0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;

	ofs_PtScan_Strong1p0[i] << GMass_Strong_1p0[iSignal] << " " << CMass_Strong_1p0[iSignal] << " " 
				<< (hSignal_Strong_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
				<< Uncert*(hSignal_Strong_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }
    }// Signal_1p0

    if(EWKOff==1)
      continue;

    for(int iSignal=0;iSignal<nSignal_EWK_10p0;iSignal++){
      hLowPt_EWK[iMET]  ->Fill(CMass_EWK_10p0[iSignal], 10.0, hSignal_EWK_10p0[iSignal]->Integral(0,  iPtBin));
      hHighPt_EWK[iMET] ->Fill(CMass_EWK_10p0[iSignal], 10.0, hSignal_EWK_10p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      if(iMET!=0)
	continue;

      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(vDSID_EWK_10p0[iSignal].size());iDSID++){
	  hAll_EWK->Fill(CMass_EWK_10p0[iSignal], 10.0,
			 vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  hMETTrigger_EWK->Fill(CMass_EWK_10p0[iSignal], 10.0,
				vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(3));
	  hKinematics_EWK->Fill(CMass_EWK_10p0[iSignal], 10.0,
				vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(16));
	}// iDSID
      }// iMC16
      hTracks_EWK->Fill(CMass_EWK_10p0[iSignal], 10.0, hSignal_EWK_10p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      
      ofs_EWK << CMass_EWK_10p0[iSignal] << " 10.0 "
	      << (hSignal_EWK_10p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_EWK_10p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
    }// Signal_10p0

    for(int iSignal=0;iSignal<nSignal_EWK_4p0;iSignal++){
      hLowPt_EWK[iMET]  ->Fill(CMass_EWK_4p0[iSignal], 4.0, hSignal_EWK_4p0[iSignal]->Integral(0,  iPtBin));
      hHighPt_EWK[iMET] ->Fill(CMass_EWK_4p0[iSignal], 4.0, hSignal_EWK_4p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      if(iMET!=0)
	continue;
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(vDSID_EWK_4p0[iSignal].size());iDSID++){
	  hAll_EWK->Fill(CMass_EWK_4p0[iSignal], 4.0,
			 vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  hMETTrigger_EWK->Fill(CMass_EWK_4p0[iSignal], 4.0,
				vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(3));
	  hKinematics_EWK->Fill(CMass_EWK_4p0[iSignal], 4.0,
				vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(16));
	}// iDSID
      }// iMC16
      hTracks_EWK->Fill(CMass_EWK_4p0[iSignal], 4.0, hSignal_EWK_4p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));

      ofs_EWK << CMass_EWK_4p0[iSignal] << " 4.0 "
	      << (hSignal_EWK_4p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_EWK_4p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
    }// Signal_4p0

    for(int iSignal=0;iSignal<nSignal_EWK_1p0;iSignal++){
      hLowPt_EWK[iMET]  ->Fill(CMass_EWK_1p0[iSignal], 1.0, hSignal_EWK_1p0[iSignal]->Integral(0,  iPtBin));
      hHighPt_EWK[iMET] ->Fill(CMass_EWK_1p0[iSignal], 1.0, hSignal_EWK_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      if(iMET!=0)
	continue;
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(vDSID_EWK_1p0[iSignal].size());iDSID++){
	  hAll_EWK->Fill(CMass_EWK_1p0[iSignal], 1.0,
			 vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  hMETTrigger_EWK->Fill(CMass_EWK_1p0[iSignal], 1.0,
				vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(3));
	  hKinematics_EWK->Fill(CMass_EWK_1p0[iSignal], 1.0,
				vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(16));
	}// iDSID
      }// iMC16
      hTracks_EWK->Fill(CMass_EWK_1p0[iSignal], 1.0, hSignal_EWK_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));

      ofs_EWK << CMass_EWK_1p0[iSignal] << " 1.0 "
	      << (hSignal_EWK_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_EWK_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
    }// Signal_1p0

    std::cout << "EWK : " << RegName[iMET] << std::endl;
    for(int iSignal=0;iSignal<nSignal_EWK_0p2;iSignal++){
      hLowPt_EWK[iMET]  ->Fill(CMass_EWK_0p2[iSignal], 0.2, hSignal_EWK_0p2[iSignal]->Integral(0,  iPtBin));
      hHighPt_EWK[iMET] ->Fill(CMass_EWK_0p2[iSignal], 0.2, hSignal_EWK_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));
      if(iSignal==5 || iSignal==6){
	for(int iBin=0;iBin<=nBinsMySignal+1;iBin++){
	  std::cout << "CMass = " << CMass_EWK_0p2[iSignal] << ", iBin = " << iBin << " : " << hSignal_EWK_0p2[iSignal]->GetBinContent(iBin) << std::endl;
	}// for iBin
      }
      if(iMET!=0)
	continue;
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	for(unsigned int iDSID=0;iDSID<(vDSID_EWK_0p2[iSignal].size());iDSID++){
	  hAll_EWK->Fill(CMass_EWK_0p2[iSignal], 0.2,
			 vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1));
	  hMETTrigger_EWK->Fill(CMass_EWK_0p2[iSignal], 0.2,
				vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(3));
	  hKinematics_EWK->Fill(CMass_EWK_0p2[iSignal], 0.2,
				vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID)*myHistSignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->hKinematics_CutFlow[iChain]->GetBinContent(16));
	}// iDSID
      }// iMC16
      hTracks_EWK->Fill(CMass_EWK_0p2[iSignal], 0.2, hSignal_EWK_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1));

      ofs_EWK << CMass_EWK_0p2[iSignal] << " 0.2 "
	      << (hSignal_EWK_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_EWK_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
    }// Signal_0p2

    for(int iSignal=0;iSignal<nSignal_EWK_0p2;iSignal++){
      if(iMET!=0)
	continue;
      for(int j=0;j<nFrom0p2;j++){
	hLowPt_EWK[iMET]  ->Fill(CMass_EWK_0p2[iSignal], TauFrom0p2[j], hSignal_EWK_From0p2[iSignal][j]->Integral(0, iPtBin));
	hHighPt_EWK[iMET] ->Fill(CMass_EWK_0p2[iSignal], TauFrom0p2[j], hSignal_EWK_From0p2[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1));
	ofs_EWK << CMass_EWK_0p2[iSignal] << " " << TauFrom0p2[j] << " "
		<< (hSignal_EWK_From0p2[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_EWK_From0p2[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_EWK_From0p2

    for(int iSignal=0;iSignal<nSignal_EWK_1p0;iSignal++){
      if(iMET!=0)
	continue;
      for(int j=0;j<nFrom1p0;j++){
	hLowPt_EWK[iMET]  ->Fill(CMass_EWK_1p0[iSignal], TauFrom1p0[j], hSignal_EWK_From1p0[iSignal][j]->Integral(0, iPtBin));
	hHighPt_EWK[iMET] ->Fill(CMass_EWK_1p0[iSignal], TauFrom1p0[j], hSignal_EWK_From1p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1));
	ofs_EWK << CMass_EWK_1p0[iSignal] << " " << TauFrom1p0[j] << " "
		<< (hSignal_EWK_From1p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_EWK_From1p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_EWK_From1p0
    for(int iSignal=0;iSignal<nSignal_EWK_4p0;iSignal++){
      if(iMET!=0)
	continue;
      for(int j=0;j<nFrom4p0;j++){
	hLowPt_EWK[iMET]  ->Fill(CMass_EWK_4p0[iSignal], TauFrom4p0[j], hSignal_EWK_From4p0[iSignal][j]->Integral(0, iPtBin));
	hHighPt_EWK[iMET] ->Fill(CMass_EWK_4p0[iSignal], TauFrom4p0[j], hSignal_EWK_From4p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1));
	ofs_EWK << CMass_EWK_4p0[iSignal] << " " << TauFrom4p0[j] << " "
		<< (hSignal_EWK_From4p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_EWK_From4p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_EWK_From4p0

    for(int iSignal=0;iSignal<nSignal_EWK_10p0;iSignal++){
      if(iMET!=0)
	continue;
      for(int j=0;j<nFrom10p0;j++){
	hLowPt_EWK[iMET]  ->Fill(CMass_EWK_10p0[iSignal], TauFrom10p0[j], hSignal_EWK_From10p0[iSignal][j]->Integral(0, iPtBin));
	hHighPt_EWK[iMET] ->Fill(CMass_EWK_10p0[iSignal], TauFrom10p0[j], hSignal_EWK_From10p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1));
	ofs_EWK << CMass_EWK_10p0[iSignal] << " " << TauFrom10p0[j] << " "
		<< (hSignal_EWK_From10p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_EWK_From10p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_EWK_From10p0

    for(int iSignal=0;iSignal<nSignal_Higgsino_0p2;iSignal++){
      if(iMET!=0)
	continue;
      ofs_Higgsino << CMass_Higgsino_0p2[iSignal] << " 0.2 "
	      << (hSignal_Higgsino_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_Higgsino_0p2[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      for(int j=0;j<nFrom0p2;j++){
	ofs_Higgsino << CMass_Higgsino_0p2[iSignal] << " " << TauFrom0p2[j] << " "
		<< (hSignal_Higgsino_From0p2[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_Higgsino_From0p2[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_Higgsino_

    for(int iSignal=0;iSignal<nSignal_Higgsino_1p0;iSignal++){
      if(iMET!=0)
	continue;
      ofs_Higgsino << CMass_Higgsino_1p0[iSignal] << " 1.0 "
	      << (hSignal_Higgsino_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_Higgsino_1p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      for(int j=0;j<nFrom1p0;j++){
	ofs_Higgsino << CMass_Higgsino_1p0[iSignal] << " " << TauFrom1p0[j] << " "
		<< (hSignal_Higgsino_From1p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_Higgsino_From1p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_Higgsino_

    for(int iSignal=0;iSignal<nSignal_Higgsino_4p0;iSignal++){
      if(iMET!=0)
	continue;
      ofs_Higgsino << CMass_Higgsino_4p0[iSignal] << " 4.0 "
	      << (hSignal_Higgsino_4p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_Higgsino_4p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      for(int j=0;j<nFrom4p0;j++){
	ofs_Higgsino << CMass_Higgsino_4p0[iSignal] << " " << TauFrom4p0[j] << " "
		<< (hSignal_Higgsino_From4p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_Higgsino_From4p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_Higgsino_

    for(int iSignal=0;iSignal<nSignal_Higgsino_10p0;iSignal++){
      if(iMET!=0)
	continue;
      ofs_Higgsino << CMass_Higgsino_10p0[iSignal] << " 10.0 "
	      << (hSignal_Higgsino_10p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
	      << 0.16*(hSignal_Higgsino_10p0[iSignal]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      for(int j=0;j<nFrom10p0;j++){
	ofs_Higgsino << CMass_Higgsino_10p0[iSignal] << " " << TauFrom10p0[j] << " "
		<< (hSignal_Higgsino_From10p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << " "
		<< 0.16*(hSignal_Higgsino_From10p0[iSignal][j]->Integral(iPtBin+1, nBinsMySignal+1)) << std::endl;
      }// for j
    }// Signal_Higgsino_
  }// for iMET

  std::cout << "end iMET loop " << std::endl;
  /*
  // dPhi for SR
  myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(0, 0) && CaloVETO_0);
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
    double trackPt = myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
    double dPhi = myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.DeltaPhi(myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4);
    if(trackPt < LowPtThreshold)
      hData_dPhiTrackMET->Fill(TMath::Abs(dPhi));
  }// for iEntry

  // dPhi forhadCR
  myData->GetNtupleReader("Old4L_Common_hadCR")->m_tree->Draw(">>eventList", GetKinematicsCut(0, 0));
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_hadCR")->GetEntry(eventList->GetEntry(iEntry));
    double dPhi = myData->GetNtupleReader("Old4L_Common_hadCR")->Tracks->at(0)->p4.DeltaPhi(myData->GetNtupleReader("Old4L_Common_hadCR")->missingET->p4);
    hData_dPhiTrackMET_hadCR->Fill(TMath::Abs(dPhi));
  }// for iEntry

  // dPhi for fakeCR
  myData->GetNtupleReader("Old4L_Common_fakeCR")->m_tree->Draw(">>eventList", GetKinematicsCut(0, 0) && CaloVETO_0 && FakeD0_CR);
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_fakeCR")->GetEntry(eventList->GetEntry(iEntry));
    double dPhi = myData->GetNtupleReader("Old4L_Common_fakeCR")->Tracks->at(0)->p4.DeltaPhi(myData->GetNtupleReader("Old4L_Common_fakeCR")->missingET->p4);
    hData_dPhiTrackMET_fakeCR->Fill(TMath::Abs(dPhi));
  }// for iEntry

  // dPhi for singleEleCR
  myData->GetNtupleReader("Old4L_Common_singleEleCR")->m_tree->Draw(">>eventList", GetKinematicsCut(0, 1));
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_singleEleCR")->GetEntry(eventList->GetEntry(iEntry));
    double dPhi = myData->GetNtupleReader("Old4L_Common_singleEleCR")->Tracks->at(0)->p4.DeltaPhi(myData->GetNtupleReader("Old4L_Common_singleEleCR")->missingET_Electron->p4);
    hData_dPhiTrackMET_singleEleCR->Fill(TMath::Abs(dPhi));
  }// for iEntry

  // dPhi for singleMuCR
  myData->GetNtupleReader("Old4L_Common_singleMuCR")->m_tree->Draw(">>eventList", GetKinematicsCut(0, 2) && CaloVETO_0);
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_singleMuCR")->GetEntry(eventList->GetEntry(iEntry));
    double dPhi = myData->GetNtupleReader("Old4L_Common_singleMuCR")->Tracks->at(0)->p4.DeltaPhi(myData->GetNtupleReader("Old4L_Common_singleMuCR")->missingET_Muon->p4);
    hData_dPhiTrackMET_singleMuCR->Fill(TMath::Abs(dPhi));
  }// for iEntry
  */
  // Strong
  for(int iX=0;iX<18;iX++){
    for(int iY=0;iY<22;iY++){
      double vAll  = hAll_Strong->GetBinContent(iX+1, iY+1);
      double vTrig = hMETTrigger_Strong->GetBinContent(iX+1, iY+1);
      double vKin  = hKinematics_Strong->GetBinContent(iX+1, iY+1);

      if(vAll!=0){
	hEffMETTrigger_Strong->SetBinContent(iX+1, iY+1, 100.0*vTrig/vAll);
      }
      if(vTrig!=0){
	hEffKinematics_Strong->SetBinContent(iX+1, iY+1, 100.0*vKin/vTrig);
      }
      if(vKin!=0){
	hEffTracks_Strong_1p0->SetBinContent(iX+1, iY+1, 100.0*hTracks_Strong_1p0->GetBinContent(iX+1, iY+1)/vKin);
	hEffTracks_Strong_1p0to0p2->SetBinContent(iX+1, iY+1, 100.0*hTracks_Strong_1p0to0p2->GetBinContent(iX+1, iY+1)/vKin);
      }
    }// for iY
  }// for iX

  // EWK
  for(int iX=0;iX<10;iX++){
    for(int iY=0;iY<30;iY++){
      if(EWKOff==1)
	continue;
      double vAll  = hAll_EWK->GetBinContent(iX+1, iY+1);
      double vTrig = hMETTrigger_EWK->GetBinContent(iX+1, iY+1);
      double vKin  = hKinematics_EWK->GetBinContent(iX+1, iY+1);

      if(vAll!=0){
	hEffMETTrigger_EWK->SetBinContent(iX+1, iY+1, 100.0*vTrig/vAll);
      }
      if(vTrig!=0){
	hEffKinematics_EWK->SetBinContent(iX+1, iY+1, 100.0*vKin/vTrig);
      }
      if(vKin!=0){
	hEffTracks_EWK->SetBinContent(iX+1, iY+1, 100.0*hTracks_EWK->GetBinContent(iX+1, iY+1)/vKin);
      }
    }// for iY
  }// for iX

  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";gluino mass [GeV];chargino mass [GeV]", 100, 400, 2300, 100, 0, 2300);
  for(int iMET=0;iMET<nMETRegion;iMET++){
    // Strong_0p2
    hFrame->Draw();
    hLowPt_Strong_0p2[iMET]->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hFrame->Draw();
    hHighPt_Strong_0p2[iMET]->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    // Strong_1p0
    hFrame->Draw();
    hLowPt_Strong_1p0[iMET]->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hFrame->Draw();
    hHighPt_Strong_1p0[iMET]->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    // Strong_1p0to0p2
    hFrame->Draw();
    hLowPt_Strong_1p0to0p2[iMET]->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    hFrame->Draw();
    hHighPt_Strong_1p0to0p2[iMET]->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  }

  hFrame->Draw();
  hAll_Strong->Draw("sametext");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hMETTrigger_Strong->Draw("sametext");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hKinematics_Strong->Draw("sametext");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hTracks_Strong_1p0->Draw("sametext");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hTracks_Strong_1p0to0p2->Draw("sametext");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
  hEffMETTrigger_Strong->Draw("sametextcol");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
  hEffKinematics_Strong->Draw("sametextcol");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
  hEffTracks_Strong_1p0->Draw("sametextcol");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
  hEffTracks_Strong_1p0to0p2->Draw("sametextcol");
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";#Delta#phi(track, MET)", 100, 0, 3.2, 100, 0.001, 100);

  c0->SetLogy(true);
  hFrame->Draw();
  if(ReadData){
    hData_dPhiTrackMET->SetLineColor(kBlack);
    hData_dPhiTrackMET->Draw("same");
  }
  for(int i=0;i<5;i++){
    hSignal_dPhiTrackMET_Strong[i]->SetLineColor(2+i);
    hSignal_dPhiTrackMET_Strong[i]->SetMarkerColor(2+i);
    hSignal_dPhiTrackMET_Strong[i]->Draw("same");
  }
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
  
  hFrame->Draw();
  if(ReadData){
    hData_dPhiTrackMET->SetLineColor(kBlack);
    hData_dPhiTrackMET->Draw("same");
  }
  for(int i=0;i<5;i++){
    hSignal_dPhiTrackMET_Strong[i+5]->SetLineColor(2+i);
    hSignal_dPhiTrackMET_Strong[i+5]->SetMarkerColor(2+i);
    hSignal_dPhiTrackMET_Strong[i+5]->Draw("same");
  }
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";#Delta#phi(track, MET)", 100, 0, 3.2, 100, 0.0001, 1.0);
  if(ReadData){
  ofs_dPhiTrackMET << "Data       hadCR : " 
		   << std::setw(20) << hData_dPhiTrackMET_hadCR->Integral(0, 33) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_hadCR->Integral(0, 10) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_hadCR->Integral(0, 16) << std::endl;

  ofs_dPhiTrackMET << "Data      fakeCR : "
		   << std::setw(20) << hData_dPhiTrackMET_fakeCR->Integral(0, 33) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_fakeCR->Integral(0, 10) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_fakeCR->Integral(0, 16) << std::endl;

  ofs_dPhiTrackMET << "Data singleEleCR : "
		   << std::setw(20) << hData_dPhiTrackMET_singleEleCR->Integral(0, 33) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_singleEleCR->Integral(0, 10) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_singleEleCR->Integral(0, 16) << std::endl;

  ofs_dPhiTrackMET << "Data  singleMuCR : "
		   << std::setw(20) << hData_dPhiTrackMET_singleMuCR->Integral(0, 33) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_singleMuCR->Integral(0, 10) << ", " 
		   << std::setw(20) << hData_dPhiTrackMET_singleMuCR->Integral(0, 16) << std::endl;

  hFrame->Draw();
  hData_dPhiTrackMET_hadCR->SetLineColor(kGreen+1);
  hData_dPhiTrackMET_hadCR->SetMarkerColor(kGreen+1);
  hData_dPhiTrackMET_hadCR->Scale(1.0/hData_dPhiTrackMET_hadCR->Integral());
  hData_dPhiTrackMET_hadCR->Draw("same");;
  hData_dPhiTrackMET_fakeCR->SetLineColor(kOrange);
  hData_dPhiTrackMET_fakeCR->SetMarkerColor(kOrange);
  hData_dPhiTrackMET_fakeCR->Scale(1.0/hData_dPhiTrackMET_fakeCR->Integral());
  hData_dPhiTrackMET_fakeCR->Draw("same");;
  hData_dPhiTrackMET_singleEleCR->SetLineColor(kRed);
  hData_dPhiTrackMET_singleEleCR->SetMarkerColor(kRed);
  hData_dPhiTrackMET_singleEleCR->Scale(1.0/hData_dPhiTrackMET_singleEleCR->Integral());
  hData_dPhiTrackMET_singleEleCR->Draw("same");;
  hData_dPhiTrackMET_singleMuCR->SetLineColor(kBlue);
  hData_dPhiTrackMET_singleMuCR->SetMarkerColor(kBlue);
  hData_dPhiTrackMET_singleMuCR->Scale(1.0/hData_dPhiTrackMET_singleMuCR->Integral());
  hData_dPhiTrackMET_singleMuCR->Draw("same");;
  }
  for(int i=0;i<5;i++){
    ofs_dPhiTrackMET << "Signal " << i << "          (Strong) : "
		     << std::setw(20) << hSignal_dPhiTrackMET_Strong[i]->Integral(0, 33) << ", " 
		     << std::setw(20) << hSignal_dPhiTrackMET_Strong[i]->Integral(0, 10) << ", " 
		     << std::setw(20) << hSignal_dPhiTrackMET_Strong[i]->Integral(0, 16) << std::endl;

    hSignal_dPhiTrackMET_Strong[i]->SetLineColor(2+i);
    hSignal_dPhiTrackMET_Strong[i]->SetLineStyle(2);
    hSignal_dPhiTrackMET_Strong[i]->Scale(1.0/hSignal_dPhiTrackMET_Strong[i]->Integral());
    hSignal_dPhiTrackMET_Strong[i]->Draw("samehist");
  }
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  hFrame->Draw();
  if(ReadData){
  hData_dPhiTrackMET_hadCR->SetLineColor(kGreen+1);
  hData_dPhiTrackMET_hadCR->SetMarkerColor(kGreen+1);
  hData_dPhiTrackMET_hadCR->Scale(1.0/hData_dPhiTrackMET_hadCR->Integral());
  hData_dPhiTrackMET_hadCR->Draw("same");;
  hData_dPhiTrackMET_fakeCR->SetLineColor(kOrange);
  hData_dPhiTrackMET_fakeCR->SetMarkerColor(kOrange);
  hData_dPhiTrackMET_fakeCR->Scale(1.0/hData_dPhiTrackMET_fakeCR->Integral());
  hData_dPhiTrackMET_fakeCR->Draw("same");;
  hData_dPhiTrackMET_singleEleCR->SetLineColor(kRed);
  hData_dPhiTrackMET_singleEleCR->SetMarkerColor(kRed);
  hData_dPhiTrackMET_singleEleCR->Scale(1.0/hData_dPhiTrackMET_singleEleCR->Integral());
  hData_dPhiTrackMET_singleEleCR->Draw("same");;
  hData_dPhiTrackMET_singleMuCR->SetLineColor(kBlue);
  hData_dPhiTrackMET_singleMuCR->SetMarkerColor(kBlue);
  hData_dPhiTrackMET_singleMuCR->Scale(1.0/hData_dPhiTrackMET_singleMuCR->Integral());
  hData_dPhiTrackMET_singleMuCR->Draw("same");;
  }
  for(int i=0;i<5;i++){
    ofs_dPhiTrackMET << "Signal " << i+5 << "          (Strong) : "
		     << std::setw(20) << hSignal_dPhiTrackMET_Strong[i+5]->Integral(0, 33) << ", " 
		     << std::setw(20) << hSignal_dPhiTrackMET_Strong[i+5]->Integral(0, 10) << ", " 
		     << std::setw(20) << hSignal_dPhiTrackMET_Strong[i+5]->Integral(0, 16) << std::endl;
    hSignal_dPhiTrackMET_Strong[i+5]->SetLineColor(2+i);
    hSignal_dPhiTrackMET_Strong[i+5]->SetLineStyle(2);
    hSignal_dPhiTrackMET_Strong[i+5]->Scale(1.0/hSignal_dPhiTrackMET_Strong[i+5]->Integral());
    hSignal_dPhiTrackMET_Strong[i+5]->Draw("samehist");
  }
  c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";chargino mass [GeV];lifetime [GeV]", 100, 0, 1100, 100, 0.01, 20);

  // EWK
  if(EWKOff==0){
    c0->SetLogy();
    for(int iMET=0;iMET<nMETRegion;iMET++){
      hFrame->Draw();
      hLowPt_EWK[iMET]->Draw("sametext");
      c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
      hFrame->Draw();
      hHighPt_EWK[iMET]->Draw("sametext");
      c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    }

    hFrame->Draw();
    hAll_EWK->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    hFrame->Draw();
    hMETTrigger_EWK->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    hFrame->Draw();
    hKinematics_EWK->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    hFrame->Draw();
    hTracks_EWK->Draw("sametext");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    hFrame->Draw();
    hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
    hEffMETTrigger_EWK->Draw("sametextcol");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    hFrame->Draw();
    hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
    hEffKinematics_EWK->Draw("sametextcol");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    hFrame->Draw();
    hFrame->GetZaxis()->SetRangeUser(0.0, 100.0);
    hEffTracks_EWK->Draw("sametextcol");
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    c0->SetLogy(false);

    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";#Delta#phi(track, MET)", 100, 0, 3.2, 100, 0.001, 100);

    c0->SetLogy(true);
    hFrame->Draw();
    if(ReadData){
      hData_dPhiTrackMET->SetLineColor(kBlack);
      hData_dPhiTrackMET->Draw("same");
    }
    for(int i=0;i<2;i++){
      hSignal_dPhiTrackMET_EWK[i]->SetLineColor(2+i);
      hSignal_dPhiTrackMET_EWK[i]->SetMarkerColor(2+i);
      hSignal_dPhiTrackMET_EWK[i]->Draw("same");
    }
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";#Delta#phi(track, MET)", 100, 0, 3.2, 100, 0.0001, 1.0);
    hFrame->Draw();
    if(ReadData){
      hData_dPhiTrackMET_hadCR->SetLineColor(kGreen+1);
      hData_dPhiTrackMET_hadCR->SetMarkerColor(kGreen+1);
      hData_dPhiTrackMET_hadCR->Scale(1.0/hData_dPhiTrackMET_hadCR->Integral());
      hData_dPhiTrackMET_hadCR->Draw("same");;
      hData_dPhiTrackMET_fakeCR->SetLineColor(kOrange);
      hData_dPhiTrackMET_fakeCR->SetMarkerColor(kOrange);
      hData_dPhiTrackMET_fakeCR->Scale(1.0/hData_dPhiTrackMET_fakeCR->Integral());
      hData_dPhiTrackMET_fakeCR->Draw("same");;
      hData_dPhiTrackMET_singleEleCR->SetLineColor(kRed);
      hData_dPhiTrackMET_singleEleCR->SetMarkerColor(kRed);
      hData_dPhiTrackMET_singleEleCR->Scale(1.0/hData_dPhiTrackMET_singleEleCR->Integral());
      hData_dPhiTrackMET_singleEleCR->Draw("same");;
      hData_dPhiTrackMET_singleMuCR->SetLineColor(kBlue);
      hData_dPhiTrackMET_singleMuCR->SetMarkerColor(kBlue);
      hData_dPhiTrackMET_singleMuCR->Scale(1.0/hData_dPhiTrackMET_singleMuCR->Integral());
      hData_dPhiTrackMET_singleMuCR->Draw("same");;
    }
    for(int i=0;i<2;i++){
      ofs_dPhiTrackMET << "Signal " << i << "          (EWK) : "
		       << std::setw(20) << hSignal_dPhiTrackMET_EWK[i]->Integral(0, 33) << ", " 
		       << std::setw(20) << hSignal_dPhiTrackMET_EWK[i]->Integral(0, 10) << ", " 
		       << std::setw(20) << hSignal_dPhiTrackMET_EWK[i]->Integral(0, 16) << std::endl;

      hSignal_dPhiTrackMET_EWK[i]->SetLineColor(2+i);
      hSignal_dPhiTrackMET_EWK[i]->SetLineStyle(2);
      hSignal_dPhiTrackMET_EWK[i]->Scale(1.0/hSignal_dPhiTrackMET_EWK[i]->Integral());
      hSignal_dPhiTrackMET_EWK[i]->Draw("samehist");
    }
    c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");

    c0->SetLogy(false);

    delete hFrame; hFrame=NULL;
    hFrame = new TH2D("hFrame", ";track pT [GeV]", 100, 20.0, 12500, 100, 0.0, 100);

    c0->SetLogx(true);
    for(int i=0;i<3;i++){
      if(i==0){
	hFrame->GetYaxis()->SetRangeUser(0.0, 30.0);
      }else if(i==1){
	hFrame->GetYaxis()->SetRangeUser(0.0, 10.0);
      }else if(i==2){
	hFrame->GetYaxis()->SetRangeUser(0.0, 1.0);
      }
      hFrame->Draw();

      c0->Print(Form("%s_Chain%d.pdf", PDFName.c_str(), iChain), "pdf");
    }// for i
  }// if EWK ON
  c0->Print(Form("%s_Chain%d.pdf]", PDFName.c_str(), iChain), "pdf");

  std::ofstream ofs(Form("mySignal_Chain%d.txt", iChain));

  tf = new TFile(Form("mySignal_Chain%d.root", iChain), "RECREATE");

  hDataOldEWK->Write();

  /*
  hBkgStrong->Write();
  hBkgEWK->Write();
  */
  hBkgOldEWK->Write();

  hSignalOldEWK->Write();

  for(int iMET=0;iMET<nMETRegion;iMET++){
    hLowPt_Strong_0p2[iMET]  ->Write();
    hHighPt_Strong_0p2[iMET] ->Write();
    hLowPt_Strong_1p0[iMET]  ->Write();
    hHighPt_Strong_1p0[iMET] ->Write();
    hLowPt_Strong_1p0to0p2[iMET]  ->Write();
    hHighPt_Strong_1p0to0p2[iMET] ->Write();
    hLowPt_EWK[iMET]  ->Write();
    hHighPt_EWK[iMET] ->Write();    
  }

  for(int iSignal=0;iSignal<nSignal_Strong_0p2;iSignal++){
    hSignal_Strong_0p2[iSignal]->Write();
    hSignalInvPt_Strong_0p2[iSignal]->Write();
  }
  for(int iSignal=0;iSignal<nSignal_Strong_1p0;iSignal++){
    hSignal_Strong_1p0[iSignal]->Write();
    hSignal_Strong_1p0to0p2[iSignal]->Write();
    hSignalInvPt_Strong_1p0[iSignal]->Write();
    hSignalInvPt_Strong_1p0to0p2[iSignal]->Write();
  }
  if(EWKOff==0){
    for(int iSignal=0;iSignal<nSignal_EWK_0p2;iSignal++){
      hSignal_EWK_0p2[iSignal]->Write();
      hSignalInvPt_EWK_0p2[iSignal]->Write();
    }// Signal_0p2
    for(int iSignal=0;iSignal<nSignal_EWK_1p0;iSignal++){
      hSignal_EWK_1p0[iSignal]->Write();
      hSignalInvPt_EWK_1p0[iSignal]->Write();
    }// Signal_1p0
  }// if EWK ON

  hAll_Strong->Write();
  hMETTrigger_Strong->Write();
  hKinematics_Strong->Write();
  hTracks_Strong_1p0->Write();
  hTracks_Strong_1p0to0p2->Write();
  hEffMETTrigger_Strong->Write();
  hEffKinematics_Strong->Write();
  hEffTracks_Strong_1p0->Write();
  hEffTracks_Strong_1p0to0p2->Write();

  hAll_EWK->Write();
  hMETTrigger_EWK->Write();
  hKinematics_EWK->Write();
  hTracks_EWK->Write();
  hEffMETTrigger_EWK->Write();
  hEffKinematics_EWK->Write();
  hEffTracks_EWK->Write();
  if(ReadData){
  hData_dPhiTrackMET->Write();
  }
  for(int i=0;i<nBenchmark_Strong;i++){
    hSignal_dPhiTrackMET_Strong[i]->Write();
  }// for benchmark_strong

  if(EWKOff==0){
    for(int i=0;i<nBenchmark_EWK;i++){
      hSignal_dPhiTrackMET_EWK[i]->Write();
    }// for benchmark_EWK

    //for(int i=0;i<3;i++){
      //hSignal_Higgsino_1p0[i]->Write();
      //hSignal_Higgsino_4p0[i]->Write();
      //hSignal_Higgsino_0p2[i]->Write();
      //hSignal_Higgsino_From0p2[i]->Write();    
      //hSignalInvPt_Higgsino_1p0[i]->Write();
      //hSignalInvPt_Higgsino_4p0[i]->Write();
      //hSignalInvPt_Higgsino_0p2[i]->Write();
      //}// for i
  }
  tf->Close();
  std::cout << "mySignal : succeeded to run " << std::endl;

  return 0;
}

int GetSignalHist_Strong(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist, double Scale, TCut myCut){
  std::cout << "GetSignalHist_Strong" << std::endl;

  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    // Strong_0p2
    for(int iSignal=0;iSignal<nSignal_Strong_0p2;iSignal++){
      if(iMC16==0){
	delete hSignal_Strong_0p2[iSignal]; hSignal_Strong_0p2[iSignal]=NULL;
	hSignal_Strong_0p2[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_Strong_0p2_%d", iSignal));
	hSignal_Strong_0p2[iSignal]->Reset();

	delete hSignalInvPt_Strong_0p2[iSignal]; hSignalInvPt_Strong_0p2[iSignal]=NULL;
	hSignalInvPt_Strong_0p2[iSignal] = (TH1D *)hTemplate_InvPt->Clone(Form("hSignalInvPt_Strong_0p2_%d", iSignal));
	hSignalInvPt_Strong_0p2[iSignal]->Reset();
      }
      hTMP->Reset();
      mySignal_Strong_0p2[iMC16][iSignal]->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight * myCut);
      hSignal_Strong_0p2[iSignal]->Add(hTMP, SFSig_Strong_0p2[iMC16][iSignal]);

      hTMP_InvPt->Reset();
      mySignal_Strong_0p2[iMC16][iSignal]->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight * myCut);
      hSignalInvPt_Strong_0p2[iSignal]->Add(hTMP_InvPt, SFSig_Strong_0p2[iMC16][iSignal]);
    }// Strong_0p2

    // Strong_1p0
    for(int iSignal=0;iSignal<nSignal_Strong_1p0;iSignal++){
      if(iMC16==0){
	delete hSignal_Strong_1p0[iSignal]; hSignal_Strong_1p0[iSignal]=NULL;
	hSignal_Strong_1p0[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_Strong_1p0_%d", iSignal));
	hSignal_Strong_1p0[iSignal]->Reset();

	delete hSignalInvPt_Strong_1p0[iSignal]; hSignalInvPt_Strong_1p0[iSignal]=NULL;
	hSignalInvPt_Strong_1p0[iSignal] = (TH1D *)hTemplate_InvPt->Clone(Form("hSignalInvPt_Strong_1p0_%d", iSignal));
	hSignalInvPt_Strong_1p0[iSignal]->Reset();
      }
      hTMP->Reset();
      mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight * myCut);
      hSignal_Strong_1p0[iSignal]->Add(hTMP, SFSig_Strong_1p0[iMC16][iSignal]);

      hTMP_InvPt->Reset();
      mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight * myCut);
      hSignalInvPt_Strong_1p0[iSignal]->Add(hTMP_InvPt, SFSig_Strong_1p0[iMC16][iSignal]);
    }// Strong_1p0
  }// for iMC16

  return 0;
}

int GetSignalHist_EWK(std::string TreeName, std::string BranchName, std::string ValName, TH1D *hHist, double Scale, TCut myCut){
  if(EWKOff==1)
    return 0;
  std::cout << "GetSignalHist_EWK" << std::endl;

  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    // Higgsino_0p2
    for(int iSignal=0;iSignal<nSignal_Higgsino_0p2;iSignal++){
      if(iMC16==0){
	delete hSignal_Higgsino_0p2[iSignal]; hSignal_Higgsino_0p2[iSignal]=NULL;
	hSignal_Higgsino_0p2[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_Higgsino_0p2_%d", iSignal));
	hSignal_Higgsino_0p2[iSignal]->Reset();
      }
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_0p2[iSignal].size());iDSID++){
	if(iSignal==2 ||iSignal==4){
	  if(iDSID==0){
	    hTMP->Reset();
	    mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*(myCut&&"prodType==0"));
	    hSignal_Higgsino_0p2[iSignal]->Add(hTMP, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	    
	    hTMP_InvPt->Reset();
	    mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*(myCut&&"prodType==0"));
	    hSignalInvPt_Higgsino_0p2[iSignal]->Add(hTMP_InvPt, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	  }else if(iDSID==1){
	    hTMP->Reset();
	    mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*(myCut&&"prodType==1"));
	    hSignal_Higgsino_0p2[iSignal]->Add(hTMP, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	    
	    hTMP_InvPt->Reset();
	    mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*(myCut&&"prodType==1"));
	    hSignalInvPt_Higgsino_0p2[iSignal]->Add(hTMP_InvPt, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	  }else if(iDSID==2){
	    hTMP->Reset();
	    mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	    hSignal_Higgsino_0p2[iSignal]->Add(hTMP, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	    
	    hTMP_InvPt->Reset();
	    mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	    hSignalInvPt_Higgsino_0p2[iSignal]->Add(hTMP_InvPt, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	  }
	}else{
	  hTMP->Reset();
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	  hSignal_Higgsino_0p2[iSignal]->Add(hTMP, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	  
	  hTMP_InvPt->Reset();
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	  hSignalInvPt_Higgsino_0p2[iSignal]->Add(hTMP_InvPt, vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	}
      }// for iDSID
    }// Higgsino_0p2


    // EWK_0p2
    for(int iSignal=0;iSignal<nSignal_EWK_0p2;iSignal++){
      if(iMC16==0){
	delete hSignal_EWK_0p2[iSignal]; hSignal_EWK_0p2[iSignal]=NULL;
	hSignal_EWK_0p2[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_EWK_0p2_%d", iSignal));
	hSignal_EWK_0p2[iSignal]->Reset();

	delete hSignalInvPt_EWK_0p2[iSignal]; hSignalInvPt_EWK_0p2[iSignal]=NULL;
	hSignalInvPt_EWK_0p2[iSignal] = (TH1D *)hTemplate_InvPt->Clone(Form("hSignalInvPt_EWK_0p2_%d", iSignal));
	hSignalInvPt_EWK_0p2[iSignal]->Reset();
      }
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_0p2[iSignal].size());iDSID++){
	hTMP->Reset();
	mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignal_EWK_0p2[iSignal]->Add(hTMP, vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID));

	hTMP_InvPt->Reset();
	mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignalInvPt_EWK_0p2[iSignal]->Add(hTMP_InvPt, vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID));
      }// for iDSID
    }// EWK_0p2

    // EWK_1p0
    for(int iSignal=0;iSignal<nSignal_EWK_1p0;iSignal++){
      if(iMC16==0){
	delete hSignal_EWK_1p0[iSignal]; hSignal_EWK_1p0[iSignal]=NULL;
	hSignal_EWK_1p0[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_EWK_1p0_%d", iSignal));
	hSignal_EWK_1p0[iSignal]->Reset();

	delete hSignalInvPt_EWK_1p0[iSignal]; hSignalInvPt_EWK_1p0[iSignal]=NULL;
	hSignalInvPt_EWK_1p0[iSignal] = (TH1D *)hTemplate_InvPt->Clone(Form("hSignalInvPt_EWK_1p0_%d", iSignal));
	hSignalInvPt_EWK_1p0[iSignal]->Reset();
      }
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_1p0[iSignal].size());iDSID++){
	hTMP->Reset();
	mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignal_EWK_1p0[iSignal]->Add(hTMP, vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));

	hTMP_InvPt->Reset();
	mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignalInvPt_EWK_1p0[iSignal]->Add(hTMP_InvPt, vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));
      }// for iDSID
    }// EWK_1p0

    // EWK_4p0
    for(int iSignal=0;iSignal<nSignal_EWK_4p0;iSignal++){
      if(iMC16==0){
	delete hSignal_EWK_4p0[iSignal]; hSignal_EWK_4p0[iSignal]=NULL;
	hSignal_EWK_4p0[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_EWK_4p0_%d", iSignal));
	hSignal_EWK_4p0[iSignal]->Reset();

	delete hSignalInvPt_EWK_4p0[iSignal]; hSignalInvPt_EWK_4p0[iSignal]=NULL;
	hSignalInvPt_EWK_4p0[iSignal] = (TH1D *)hTemplate_InvPt->Clone(Form("hSignalInvPt_EWK_4p0_%d", iSignal));
	hSignalInvPt_EWK_4p0[iSignal]->Reset();
      }
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_4p0[iSignal].size());iDSID++){
	hTMP->Reset();
	mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignal_EWK_4p0[iSignal]->Add(hTMP, vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID));

	hTMP_InvPt->Reset();
	mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignalInvPt_EWK_4p0[iSignal]->Add(hTMP_InvPt, vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID));
      }// for iDSID
    }// EWK_4p0

    // EWK_10p0
    for(int iSignal=0;iSignal<nSignal_EWK_10p0;iSignal++){
      if(iMC16==0){
	delete hSignal_EWK_10p0[iSignal]; hSignal_EWK_10p0[iSignal]=NULL;
	hSignal_EWK_10p0[iSignal] = (TH1D *)hHist->Clone(Form("hSignal_EWK_10p0_%d", iSignal));
	hSignal_EWK_10p0[iSignal]->Reset();

	delete hSignalInvPt_EWK_10p0[iSignal]; hSignalInvPt_EWK_10p0[iSignal]=NULL;
	hSignalInvPt_EWK_10p0[iSignal] = (TH1D *)hTemplate_InvPt->Clone(Form("hSignalInvPt_EWK_10p0_%d", iSignal));
	hSignalInvPt_EWK_10p0[iSignal]->Reset();
      }
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_10p0[iSignal].size());iDSID++){
	hTMP->Reset();
	mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignal_EWK_10p0[iSignal]->Add(hTMP, vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID));
	hTMP_InvPt->Reset();
	mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("1.0/(%s.%s*%lf)>>hTMP_InvPt", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
	hSignalInvPt_EWK_10p0[iSignal]->Add(hTMP_InvPt, vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID));
      }// for iDSID
    }// EWK_10p0

    // EWK_noMET
    if(iMC16==0){
      delete hSignal_EWK_noMET; hSignal_EWK_noMET=NULL;
      hSignal_EWK_noMET = (TH1D *)hHist->Clone("hSignal_EWK_noMET");
      hSignal_EWK_noMET->Reset();
    }
    for(unsigned int iDSID=0;iDSID<(vDSID_EWK_noMET.size());iDSID++){
      hTMP->Reset();
      mySignal_EWK_noMET[iMC16]->at(iDSID)->GetNtupleReader(TreeName)->m_tree->Draw(Form("%s.%s*%lf>>hTMP", BranchName.c_str(), ValName.c_str(), Scale), MCWeight*myCut);
      hSignal_EWK_noMET->Add(hTMP, vSFSig_EWK_noMET[iMC16].at(iDSID));
    }// for iDSID
  }// for iMC16

  return 0;
}

int GetReweightedHist_Strong(TCut myCut){
  for(int i=0;i<nBenchmark_Strong;i++){
    delete hSignal_dPhiTrackMET_Strong[i];
    hSignal_dPhiTrackMET_Strong[i] = new TH1D(Form("hSignal_dRTrackMET_Strong_%d", i), ";#DeltaR(track, MET)", 32, 0.0, 3.2);
  }// for benchmark_strong

  std::cout << "GetReweightedHist_Strong" << std::endl;
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    // Strong_1p0to0p2
    for(int iSignal=0;iSignal<nSignal_Strong_1p0;iSignal++){
      if(iMC16==0){
	delete hSignal_Strong_1p0to0p2[iSignal]; hSignal_Strong_1p0to0p2[iSignal]=NULL;
	hSignal_Strong_1p0to0p2[iSignal] = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_Strong_1p0to0p2_%d", iSignal));
	hSignal_Strong_1p0to0p2[iSignal]->Reset();
      }
      //bool fDebug = (GMass_Strong_1p0[iSignal]==2000);
      //bool fDebug = (iSignal==57);
      bool fDebug = false;
      if(fDebug) std::cout << "CMass : " << CMass_Strong_1p0[iSignal] << std::endl;
      mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut);
      if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;
      for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	double weight = 1.0;
	if(fDebug) std::cout << "weight = " << weight << std::endl;
	for(unsigned int iTruth=0;iTruth<(mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->truthParticles->size());iTruth++){
	  int PDGID = (int)mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	  if(TMath::Abs(PDGID)!=1000024)
	    continue;
	  float ProperTime = mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	  weight *= GetWeightLifetime(ProperTime, 1.0, 0.2);
	  if(fDebug) std::cout << "Proper Time  = " << ProperTime << std::endl;
	  if(fDebug) std::cout << "weight (GetWeightLifetime) = " << weight << std::endl;
	}// for iTruth
	weight *= mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->weightXsec;
	if(fDebug) std::cout << "weight (weightXsec) = " << weight << std::endl;
	weight *= mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->weightMCweight;
	if(fDebug) std::cout << "weight (MCweight) = " << weight << std::endl;
	weight *= mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->weightPileupReweighting;
	if(fDebug) std::cout << "weight (weightPileupReweighting) = " << weight << std::endl;
	double trackPt = mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	hSignal_Strong_1p0to0p2[iSignal]->Fill(trackPt, weight*SFSig_Strong_1p0[iMC16][iSignal]);
	
	if(GMass_Strong_1p0[iSignal]==1800){
	  double dPhi = mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.DeltaPhi(mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR")->missingET->p4);
	  int myIndex = -1;
	  if(CMass_Strong_1p0[iSignal]==1750){
	    myIndex = 9;
	  }else{
	    myIndex = ((CMass_Strong_1p0[iSignal]-100)/200);
	  }
	  hSignal_dPhiTrackMET_Strong[myIndex]->Fill(TMath::Abs(dPhi), weight*SFSig_Strong_1p0[iMC16][iSignal]);
	}// if gmass=1800

	if(fDebug) std::cout << "SFSig_Strong_1p0[" << iMC16 << "][" << iSignal << "] = " << SFSig_Strong_1p0[iMC16][iSignal] << std::endl;
	if(fDebug) std::cout << "trackPt = " << trackPt << std::endl;
	if(fDebug) std::cout << "Nbins = " << hSignal_Strong_1p0to0p2[iSignal]->GetNbinsX() << std::endl;
      }// for iEntry
    }// Strong_1p0to0p2
  }// iMC16

  return 0;
}

int GetSignalHist_EWK(TCut myCut, std::string Type, std::string Tau){
  if(EWKOff==1)
    return 0;
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    for(int iSignal=0;iSignal<GetNSignal(Type, Tau);iSignal++){
      if(iMC16==0){
	for(int iFrom=-1;iFrom<GetNFrom(Tau);iFrom++){
	  delete GetHistSignal(Type, Tau, iSignal, iFrom); GetHistSignal(Type, Tau, iSignal, iFrom)=NULL;
	  if(iFrom==-1)
	    GetHistSignal(Type, Tau, iSignal, iFrom) = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_%s_%s_%d", Type.c_str(), Tau.c_str(), iSignal));
	  else
	    GetHistSignal(Type, Tau, iSignal, iFrom) = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_%s_From%s_%d_%d", Type.c_str(), Tau.c_str(), iSignal, iFrom));
	  GetHistSignal(Type, Tau, iSignal, iFrom)->Reset();
	}
      }// iMC16=0
      for(unsigned int iDSID=0;iDSID<GetNSizeDSID(Type, Tau, iSignal);iDSID++){
	bool fDebug = false;
	DrawEventList(Type, Tau, myCut, iMC16, iSignal, iDSID);
	if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;

	for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	  GetReader(Type, Tau, iMC16, iSignal, iDSID)->GetEntry(eventList->GetEntry(iEntry));
	  if(fUseTriggerBit && GetReader(Type, Tau, iMC16, iSignal, iDSID)->LU_METtrigger_SUSYTools==false)
	    continue;
	  
	  double weightcommon = 1.0;
	  double weight[31];
	  for(int iFrom=0;iFrom<GetNFrom(Tau);iFrom++){
	    weight[iFrom] = 1.0;
	  }
	  for(unsigned int iTruth=0;iTruth<(GetReader(Type, Tau, iMC16, iSignal, iDSID)->truthParticles->size());iTruth++){
	    int PDGID = (int)GetReader(Type, Tau, iMC16, iSignal, iDSID)->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = GetReader(Type, Tau, iMC16, iSignal, iDSID)->truthParticles->at(iTruth)->ProperTime;
	    for(int iFrom=0;iFrom<GetNFrom(Tau);iFrom++){
	      weight[iFrom] *= GetWeightLifetime(ProperTime, Tau, iFrom);
	    }
	  }// for iTruth

	  if(fUseTriggerBit==false)
	    weightcommon *= GetTrigEff(GetReader(Type, Tau, iMC16, iSignal, iDSID));
	  weightcommon *= GetReader(Type, Tau, iMC16, iSignal, iDSID)->weightXsec;
	  weightcommon *= GetReader(Type, Tau, iMC16, iSignal, iDSID)->weightMCweight;
	  weightcommon *= GetReader(Type, Tau, iMC16, iSignal, iDSID)->weightPileupReweighting;
	  if(fSmearTruthPt){
	    double truthPt = GetReader(Type, Tau, iMC16, iSignal, iDSID)->Tracks->at(0)->TruthPt/1000.0;
	    hTMP->Reset();
	    if(truthPt > 0.0)
	      SmearPt(hTMP, truthPt, weightcommon*GetSFSig(Type, Tau, iMC16, iSignal, iDSID), true);
	    else
	      hTMP->Fill(-1, weightcommon*GetSFSig(Type, Tau, iMC16, iSignal, iDSID));
	    GetHistSignal(Type, Tau, iSignal, -1)->Add(hTMP, 1.0);
	    for(int iFrom=0;iFrom<GetNFrom(Tau);iFrom++){
	      GetHistSignal(Type, Tau, iSignal, iFrom)->Add(hTMP, weight[iFrom]);
	    }
	  }else{
	    double trackPt = GetReader(Type, Tau, iMC16, iSignal, iDSID)->Tracks->at(0)->p4.Pt()/1000.0;
	    GetHistSignal(Type, Tau, iSignal, -1)->Fill(trackPt, weightcommon*GetSFSig(Type, Tau, iMC16, iSignal, iDSID));
	    for(int iFrom=0;iFrom<GetNFrom(Tau);iFrom++){
	      GetHistSignal(Type, Tau, iSignal, iFrom)->Fill(trackPt, weightcommon*weight[iFrom]*GetSFSig(Type, Tau, iMC16, iSignal, iDSID));	    
	    }
	  }
	}// for iEntry
      }// for iDSID
    }// for iSignal
  }// for iMC16
}

int GetReweightedHist_EWK(TCut myCut){
  if(EWKOff==1)
    return 0;
  for(int i=0;i<nBenchmark_EWK;i++){
    delete hSignal_dPhiTrackMET_EWK[i];
    hSignal_dPhiTrackMET_EWK[i] = new TH1D(Form("hSignal_dRTrackMET_EWK_%d", i), ";#DeltaR(track, MET)", 32, 0.0, 3.2);
  }// for benchmark_EWK

  std::cout << "GetReweightedHist_EWK" << std::endl;
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    for(int iSignal=0;iSignal<nSignal_Higgsino_0p2;iSignal++){
      if(iMC16==0){
	for(int j=0;j<nFrom0p2;j++){
	  delete hSignal_Higgsino_From0p2[iSignal][j]; hSignal_Higgsino_From0p2[iSignal][j]=NULL;
	  hSignal_Higgsino_From0p2[iSignal][j] = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_Higgsino_From0p2_%d_%d", j, iSignal));
	  hSignal_Higgsino_From0p2[iSignal][j]->Reset();
	}
      }// iMC16=0
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_0p2[iSignal].size());iDSID++){
	bool fDebug = false;
	if((iSignal==2 || iSignal==4) && (iDSID==0)){
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut&&"prodType==0");
	}else if((iSignal==2 || iSignal==4) && (iDSID==1)){
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut&&"prodType==1");
	}else{
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut);
	}
	if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;
	for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	  double weightcommon = 1.0;
	  double weight[nFrom0p2];
	  for(int j=0;j<nFrom0p2;j++){
	    weight[j] = 1.0;
	  }

	  for(unsigned int iTruth=0;iTruth<(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->size());iTruth++){
	    int PDGID = (int)mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	    for(int j=0;j<nFrom0p2;j++){
	      weight[j] *= GetWeightLifetime(ProperTime, 0.2, TauFrom0p2[j]);
	    }
	  }// for iTruth
	  weightcommon *= mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightXsec;
	  weightcommon *= mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightMCweight;
	  weightcommon *= mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightPileupReweighting;
	  double trackPt = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	  for(int j=0;j<nFrom0p2;j++){
	    hSignal_Higgsino_From0p2[iSignal][j]->Fill(trackPt, weightcommon*weight[j]*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	  }
	}// for iEntry
      }// iDSID
    }// Higgsino_0p2

    for(int iSignal=0;iSignal<nSignal_EWK_0p2;iSignal++){
      if(iMC16==0){
	for(int j=0;j<nFrom0p2;j++){
	  delete hSignal_EWK_From0p2[iSignal][j]; hSignal_EWK_From0p2[iSignal][j]=NULL;
	  hSignal_EWK_From0p2[iSignal][j] = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_EWK_From0p2_%d_%d", j, iSignal));
	  hSignal_EWK_From0p2[iSignal][j]->Reset();
	}
      }// iMC16=0
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_0p2[iSignal].size());iDSID++){
	bool fDebug = false;
	mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut);
	if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;
	for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	  mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	  double weightcommon = 1.0;
	  double weight[nFrom0p2];
	  for(int j=0;j<nFrom0p2;j++){
	    weight[j] = 1.0;
	  }

	  for(unsigned int iTruth=0;iTruth<(mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->size());iTruth++){
	    int PDGID = (int)mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	    for(int j=0;j<nFrom0p2;j++){
	      weight[j] *= GetWeightLifetime(ProperTime, 0.2, TauFrom0p2[j]);
	    }
	  }// for iTruth
	  weightcommon *= mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightXsec;
	  weightcommon *= mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightMCweight;
	  weightcommon *= mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightPileupReweighting;
	  double trackPt = mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	  for(int j=0;j<nFrom0p2;j++){
	    hSignal_EWK_From0p2[iSignal][j]->Fill(trackPt, weightcommon*weight[j]*vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID));
	  }
	  double dPhi = mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.DeltaPhi(mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->missingET->p4);
	  if(CMass_EWK_0p2[iSignal]==199.7){
	    hSignal_dPhiTrackMET_EWK[0]->Fill(TMath::Abs(dPhi), weightcommon*vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID));
	  }else if(CMass_EWK_0p2[iSignal]==599.9){
	    hSignal_dPhiTrackMET_EWK[1]->Fill(TMath::Abs(dPhi), weightcommon*vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID));
	  }

	}// for iEntry
      }// iDSID
    }// EWK_0p2

    for(int iSignal=0;iSignal<nSignal_EWK_1p0;iSignal++){
      if(iMC16==0){
	for(int j=0;j<nFrom1p0;j++){
	  delete hSignal_EWK_From1p0[iSignal][j]; hSignal_EWK_From1p0[iSignal][j]=NULL;
	  hSignal_EWK_From1p0[iSignal][j] = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_EWK_From1p0_%d_%d", j, iSignal));
	  hSignal_EWK_From1p0[iSignal][j]->Reset();
	}
      }// iMC16=0
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_1p0[iSignal].size());iDSID++){
	bool fDebug = false;
	mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut);
	if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;
	for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	  mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	  double weightcommon = 1.0;
	  double weight[nFrom1p0];
	  for(int j=0;j<nFrom1p0;j++){
	    weight[j] = 1.0;
	  }

	  for(unsigned int iTruth=0;iTruth<(mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->size());iTruth++){
	    int PDGID = (int)mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	    for(int j=0;j<nFrom1p0;j++){
	      weight[j] *= GetWeightLifetime(ProperTime, 1.0, TauFrom1p0[j]);
	    }
	  }// for iTruth
	  weightcommon *= mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightXsec;
	  weightcommon *= mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightMCweight;
	  weightcommon *= mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightPileupReweighting;
	  double trackPt = mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	  for(int j=0;j<nFrom1p0;j++){
	    hSignal_EWK_From1p0[iSignal][j]->Fill(trackPt, weightcommon*weight[j]*vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID));
	  }
	}// for iEntry
      }// iDSID
    }// EWK_1p0

    for(int iSignal=0;iSignal<nSignal_EWK_4p0;iSignal++){
      if(iMC16==0){
	for(int j=0;j<nFrom4p0;j++){
	  delete hSignal_EWK_From4p0[iSignal][j]; hSignal_EWK_From4p0[iSignal][j]=NULL;
	  hSignal_EWK_From4p0[iSignal][j] = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_EWK_From4p0_%d_%d", j, iSignal));
	  hSignal_EWK_From4p0[iSignal][j]->Reset();
	}
      }// iMC16=0
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_4p0[iSignal].size());iDSID++){
	bool fDebug = false;
	mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut);
	if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;
	for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	  mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	  double weightcommon = 1.0;
	  double weight[nFrom4p0];
	  for(int j=0;j<nFrom4p0;j++){
	    weight[j] = 1.0;
	  }

	  for(unsigned int iTruth=0;iTruth<(mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->size());iTruth++){
	    int PDGID = (int)mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	    for(int j=0;j<nFrom4p0;j++){
	      weight[j] *= GetWeightLifetime(ProperTime, 4.0, TauFrom4p0[j]);
	    }
	  }// for iTruth
	  weightcommon *= mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightXsec;
	  weightcommon *= mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightMCweight;
	  weightcommon *= mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightPileupReweighting;
	  double trackPt = mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	  for(int j=0;j<nFrom4p0;j++){
	    hSignal_EWK_From4p0[iSignal][j]->Fill(trackPt, weightcommon*weight[j]*vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID));
	  }
	}// for iEntry
      }// iDSID
    }// EWK_4p0

    for(int iSignal=0;iSignal<nSignal_EWK_10p0;iSignal++){
      if(iMC16==0){
	for(int j=0;j<nFrom10p0;j++){
	  delete hSignal_EWK_From10p0[iSignal][j]; hSignal_EWK_From10p0[iSignal][j]=NULL;
	  hSignal_EWK_From10p0[iSignal][j] = (TH1D *)hTemplate_TrackPt->Clone(Form("hSignal_EWK_From10p0_%d_%d", j, iSignal));
	  hSignal_EWK_From10p0[iSignal][j]->Reset();
	}
      }// iMC16=0
      for(unsigned int iDSID=0;iDSID<(vDSID_EWK_10p0[iSignal].size());iDSID++){
	bool fDebug = false;
	mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", myCut);
	if(fDebug) std::cout << "GetN  : " << eventList->GetN() << std::endl;
	for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
	  mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
	  double weightcommon = 1.0;
	  double weight[nFrom10p0];
	  for(int j=0;j<nFrom10p0;j++){
	    weight[j] = 1.0;
	  }

	  for(unsigned int iTruth=0;iTruth<(mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->size());iTruth++){
	    int PDGID = (int)mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->truthParticles->at(iTruth)->ProperTime;
	    for(int j=0;j<nFrom10p0;j++){
	      weight[j] *= GetWeightLifetime(ProperTime, 10.0, TauFrom10p0[j]);
	    }
	  }// for iTruth
	  weightcommon *= mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightXsec;
	  weightcommon *= mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightMCweight;
	  weightcommon *= mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->weightPileupReweighting;
	  double trackPt = mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
	  for(int j=0;j<nFrom10p0;j++){
	    hSignal_EWK_From10p0[iSignal][j]->Fill(trackPt, weightcommon*weight[j]*vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID));
	  }
	}// for iEntry
      }// iDSID
    }// EWK_10p0

  }// iMC16

  return 0;
}

double GetWeightLifetime(double PropTime, std::string Tau, int iFrom){
  double OriginalTau;
  double TargetTau;

  if(Tau=="0p2"){
    OriginalTau = 0.2;
    TargetTau   = TauFrom0p2[iFrom];
  }
  if(Tau=="1p0"){
    OriginalTau = 1.0;
    TargetTau   = TauFrom1p0[iFrom];
  }
  if(Tau=="4p0"){
    OriginalTau = 4.0;
    TargetTau   = TauFrom4p0[iFrom];
  }
  if(Tau=="10p0"){
    OriginalTau = 10.0;
    TargetTau   = TauFrom10p0[iFrom];
  }

  if(TMath::Abs(PropTime) > 1e10)
    return 1.0;
  else
    return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
}

double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau){
  if(TMath::Abs(PropTime) > 1e10)
    return 1.0;
  else
    return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
}

TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==2){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 20.0)";
  return ret;
}
double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

int GetNSignal(std::string Type, std::string Tau){
  if(Type=="Higgsino"){
    if(Tau=="0p2" ) return nSignal_Higgsino_0p2;
    if(Tau=="1p0" ) return nSignal_Higgsino_1p0;
    if(Tau=="4p0" ) return nSignal_Higgsino_4p0;
    if(Tau=="10p0") return nSignal_Higgsino_10p0;
  }
  if(Type=="EWK"){
    if(Tau=="0p2" ) return nSignal_EWK_0p2;
    if(Tau=="1p0" ) return nSignal_EWK_1p0;
    if(Tau=="4p0" ) return nSignal_EWK_4p0;
    if(Tau=="10p0") return nSignal_EWK_10p0;
  }

  return 0;
}

int GetNFrom(std::string Tau){
  if(Tau=="0p2")  return nFrom0p2;
  if(Tau=="1p0")  return nFrom1p0;
  if(Tau=="4p0")  return nFrom4p0;
  if(Tau=="10p0") return nFrom10p0;
  if(Tau=="10p0_Long") return nFrom10p0_Long;
  if(Tau=="1p0_Strong")  return nFrom1p0_Strong;

  return 0;
}

unsigned int GetNSizeDSID(std::string Type, std::string Tau, int iSignal){
  if(Type=="Strong")
    return 1;
  if(Type=="Higgsino"){
    if(Tau=="0p2")  return vDSID_Higgsino_0p2[iSignal].size();
    if(Tau=="1p0")  return vDSID_Higgsino_1p0[iSignal].size();
    if(Tau=="4p0")  return vDSID_Higgsino_4p0[iSignal].size();
    if(Tau=="10p0") return vDSID_Higgsino_10p0[iSignal].size();
  }

  if(Type=="EWK"){
    if(Tau=="0p2")  return vDSID_EWK_0p2[iSignal].size();
    if(Tau=="1p0")  return vDSID_EWK_1p0[iSignal].size();
    if(Tau=="4p0")  return vDSID_EWK_4p0[iSignal].size();
    if(Tau=="10p0") return vDSID_EWK_10p0[iSignal].size();
  }
    
  return 0;
}

NtupleReader *GetReader(std::string Type, std::string Tau, int iMC16, int iSignal, int iDSID){
  if(Type=="Higgsino"){
    if(Tau=="0p2")  return mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="1p0")  return mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="4p0")  return mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="10p0") return mySignal_Higgsino_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
  }

  if(Type=="EWK"){
    if(Tau=="0p2")  return mySignal_EWK_0p2[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="1p0")  return mySignal_EWK_1p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="4p0")  return mySignal_EWK_4p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="10p0") return mySignal_EWK_10p0[iMC16][iSignal]->at(iDSID)->GetNtupleReader("Old4L_Common_SR");
  }

  if(Type=="Strong"){
    if(Tau=="0p2")  return mySignal_Strong_0p2[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR");
    if(Tau=="1p0")  return mySignal_Strong_1p0[iMC16][iSignal]->GetNtupleReader("Old4L_Common_SR");
  }

  return NULL;
}

int DrawEventList(std::string Type, std::string Tau, TCut myCut, int iMC16, int iSignal, int iDSID){
  if(Type=="Higgsino"){
    if(Tau=="0p2" || Tau=="1p0" || Tau=="10p0"){
      if((iSignal==2 || iSignal==4) && (iDSID==0)){
	GetReader(Type, Tau, iMC16, iSignal, iDSID)->m_tree->Draw(">>eventList", myCut&&"prodType==0");
      }else if((iSignal==2 || iSignal==4) && (iDSID==1)){
	GetReader(Type, Tau, iMC16, iSignal, iDSID)->m_tree->Draw(">>eventList", myCut&&"prodType==1");
      }else{
	GetReader(Type, Tau, iMC16, iSignal, iDSID)->m_tree->Draw(">>eventList", myCut);
      }
    }else if(Tau=="4p0"){
      GetReader(Type, Tau, iMC16, iSignal, iDSID)->m_tree->Draw(">>eventList", myCut);
    }
  }
  
  return GetReader(Type, Tau, iMC16, iSignal, iDSID)->m_tree->Draw(">>eventList", myCut);
}

double GetSFSig(std::string Type, std::string Tau, int iMC16, int iSignal, int iDSID){
  if(Type=="Higgsino"){
    if(Tau=="0p2")  return vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID);
    if(Tau=="1p0")  return vSFSig_Higgsino_1p0[iMC16][iSignal].at(iDSID);
    if(Tau=="4p0")  return vSFSig_Higgsino_4p0[iMC16][iSignal].at(iDSID);
    if(Tau=="10p0") return vSFSig_Higgsino_10p0[iMC16][iSignal].at(iDSID);
  }

  if(Type=="EWK"){
    if(Tau=="0p2")  return vSFSig_EWK_0p2[iMC16][iSignal].at(iDSID);
    if(Tau=="1p0")  return vSFSig_EWK_1p0[iMC16][iSignal].at(iDSID);
    if(Tau=="4p0")  return vSFSig_EWK_4p0[iMC16][iSignal].at(iDSID);
    if(Tau=="10p0") return vSFSig_EWK_10p0[iMC16][iSignal].at(iDSID);
  }

  if(Type=="Strong"){
    if(Tau=="0p2")  return SFSig_Strong_0p2[iMC16][iSignal];
    if(Tau=="1p0")  return SFSig_Strong_1p0[iMC16][iSignal];
  }

  return 1.0;
}

double GetTrigEff(NtupleReader *myReader){
  if(iChain==0) return myReader->TrigEff_Chain0;
  else if(iChain==1) return myReader->TrigEff_Chain1;
  else if(iChain==2) return myReader->TrigEff_Chain2;
  else if(iChain==3) return myReader->TrigEff_Chain3;
  else if(iChain==4) return myReader->TrigEff_Chain4;
  else if(iChain==5) return myReader->TrigEff_Chain5;
  else if(iChain==6) return myReader->TrigEff_Chain6;
  else return 0.0;
}

TH1D *GetHistSignal(std::string Type, std::string Tau, int iSignal, int iFrom){
  if(Type=="Higgsino"){
    if(Tau=="0p2")
      return (iFrom==-1 ? hSignal_Higgsino_0p2[iSignal] : hSignal_Higgsino_From0p2[iSignal][iFrom]);
    if(Tau=="1p0")
      return (iFrom==-1 ? hSignal_Higgsino_1p0[iSignal] : hSignal_Higgsino_From1p0[iSignal][iFrom]);
    if(Tau=="4p0")
      return (iFrom==-1 ? hSignal_Higgsino_4p0[iSignal] : hSignal_Higgsino_From4p0[iSignal][iFrom]);
    if(Tau=="10p0")
      return (iFrom==-1 ? hSignal_Higgsino_10p0[iSignal] : hSignal_Higgsino_From10p0[iSignal][iFrom]);
  }

  if(Type=="EWK"){
    if(Tau=="0p2")
      return (iFrom==-1 ? hSignal_EWK_0p2[iSignal] : hSignal_EWK_From0p2[iSignal][iFrom]);
    if(Tau=="1p0")
      return (iFrom==-1 ? hSignal_EWK_1p0[iSignal] : hSignal_EWK_From1p0[iSignal][iFrom]);
    if(Tau=="4p0")
      return (iFrom==-1 ? hSignal_EWK_4p0[iSignal] : hSignal_EWK_From4p0[iSignal][iFrom]);
    if(Tau=="10p0")
      return (iFrom==-1 ? hSignal_EWK_10p0[iSignal] : hSignal_EWK_From10p0[iSignal][iFrom]);
  }

  if(Type=="Strong"){
    if(Tau=="0p2")
      return hSignal_Strong_0p2[iSignal];
    if(Tau=="1p0")
      return (iFrom==-1 ? hSignal_Strong_1p0[iSignal] : hSignal_Strong_1p0to0p2[iSignal]);
  }
  
  return NULL;
}

void SmearPt(TH1D *hHoge, double pt, double weight, bool f2Flag){
 for(int i=0;i<nBinsMySignal;i++){
    double ptLow = XBinsMySignal[i];
    double ptUp  = XBinsMySignal[i+1];
    if(i==(nBinsMySignal-1)){
      ptUp = 12500.0;
    }

    int iFunc=-1;
    if(f2Flag){
      if(pt < 15.0){
	iFunc = 0;
      }else if(pt < 20.0){
	iFunc = 1;
      }else if(pt < 25.0){
	iFunc = 2;
      }else if(pt < 35.0){
	iFunc = 3;
      }else if(pt < 45.0){
	iFunc = 4;
      }else if(pt < 60.0){
	iFunc = 5;
      }else if(pt < 100.0){
	iFunc = 6;
      }else{
	iFunc = 7;
      }
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(f2Flag==false){
      w1Low = fSmearInt->Eval(qoverptLow);
      w1Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(f2Flag){
      w2Low = fSmearInt->Eval(qoverptLow);
      w2Up  = fSmearInt->Eval(qoverptUp);
    }else if(fSmearMuon){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 
