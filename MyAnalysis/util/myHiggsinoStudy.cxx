#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

const int nMC16 = 3;
const int nRunMC16 = 3;
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
const double SumLumi = 3219.56 + 32995.4 + 43587.3 + 58450.1 - 1141.73 - 3030.55 - 881.025; // pb-1
//const double IntLumi[nMC16]={3219.56 + 32995.4, 43587.3, 58450.1}; // pb-1
const double IntLumi[3]={3219.56 + 32995.4 - 1141.73,
			     43587.3 - 3030.55,
			     58450.1 - 881.025}; // pb-1
TFile *tf;
NtupleReader *myReader=NULL;
TEventList *eventList=NULL;
TFile *tfXS=NULL;
TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];

const double HiggsinoLifetime[3]={0.055, 0.046, 0.037};

const int nSignal_Higgsino_0p2 = 3;
std::vector<int> vDSID_Higgsino_0p2[nSignal_Higgsino_0p2]      = {{448446  , 448447  , 448448  }, {448449  , 448450  , 448451  }, {448452  , 448453  , 448454  }};
std::vector<double> vFilter_Higgsino_0p2[nSignal_Higgsino_0p2] = {{0.263026, 0.244803, 0.240685}, {0.298114, 0.296814, 0.280004}, {0.349207, 0.344303, 0.339475}};
const double CMass_Higgsino_0p2[nSignal_Higgsino_0p2]          = {120.0, 160.0, 240.0};
std::vector<NtupleReader *> *mySignal_Higgsino_0p2[nMC16][nSignal_Higgsino_0p2];
std::vector<double> vSFSig_Higgsino_0p2[nMC16][nSignal_Higgsino_0p2];

const int nSignal_Higgsino_1p0 = 3;
std::vector<int> vDSID_Higgsino_1p0[nSignal_Higgsino_1p0]      = {{448464  , 448465  , 448466  }, {448467  , 448468  , 448469  }, {448470  , 448471  , 448472  }};
std::vector<double> vFilter_Higgsino_1p0[nSignal_Higgsino_1p0] = {{0.256079, 0.24990 , 0.234509}, {0.296942, 0.297162, 0.281907}, {0.356301, 0.348424, 0.337573}};
const double CMass_Higgsino_1p0[nSignal_Higgsino_1p0]          = {120.0, 160.0, 240.0};
std::vector<NtupleReader *> *mySignal_Higgsino_1p0[nMC16][nSignal_Higgsino_1p0];
std::vector<double> vSFSig_Higgsino_1p0[nMC16][nSignal_Higgsino_1p0];

const int nSignal_Higgsino_4p0 = 3;
std::vector<int> vDSID_Higgsino_4p0[nSignal_Higgsino_4p0]      = {{448473  , 448474  , 448475  }, {448476  , 448477  , 448478  }, {448479  , 448480  , 448481  }};
std::vector<double> vFilter_Higgsino_4p0[nSignal_Higgsino_4p0] = {{0.256079, 0.24990 , 0.234509}, {0.296942, 0.297162, 0.281907}, {0.356301, 0.348424, 0.337573}};
const double CMass_Higgsino_4p0[nSignal_Higgsino_4p0]          = {120.0, 160.0, 240.0};
std::vector<NtupleReader *> *mySignal_Higgsino_4p0[nMC16][nSignal_Higgsino_4p0];
std::vector<double> vSFSig_Higgsino_4p0[nMC16][nSignal_Higgsino_4p0];

TCanvas *c0;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH2D *hFrame;
std::string PDFName;

TH1D *hSignal_Higgsino_0p2_TruthPt[3];
TH1D *hSignal_Higgsino_0p2_DecayRadius[3];
TH1D *hSignal_Higgsino_0p2_ProperTime[3];

TH1D *hSignal_Higgsino_From0p2_TruthPt[3];
TH1D *hSignal_Higgsino_From0p2_DecayRadius[3];
TH1D *hSignal_Higgsino_From0p2_ProperTime[3];

TH1D *hSignal_Higgsino_1p0_TruthPt[3];
TH1D *hSignal_Higgsino_1p0_DecayRadius[3];
TH1D *hSignal_Higgsino_1p0_ProperTime[3];

TH1D *hSignal_Higgsino_4p0_TruthPt[3];
TH1D *hSignal_Higgsino_4p0_DecayRadius[3];
TH1D *hSignal_Higgsino_4p0_ProperTime[3];

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("myCharginoStudy"), "output file");
  
  po::positional_options_description p;
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  PDFName = vm["output-file"].as<std::string>();
  std::string OutputFileName = vm["output-file"].as<std::string>();

  tfXS = new TFile("/afs/cern.ch/work/t/tkaji/public/DisappearingTrack/FirstFullRun2/gXS.root", "READ");
  gROOT->cd();
  gXS_Strong_uncert = (TGraph *)tfXS->Get("gXS_Strong_uncert");
  gXS_Strong   = (TGraphErrors *)tfXS->Get("gXS_Strong");
  gXS_EWK[0] = (TGraphErrors *)tfXS->Get("gXS_EWK_C1N1");
  gXS_EWK[1] = (TGraphErrors *)tfXS->Get("gXS_EWK_C1C1");
  gXS_Higgsino[0] = (TGraphErrors *)tfXS->Get("gXS_Higgsino_C1pN1");
  gXS_Higgsino[1] = (TGraphErrors *)tfXS->Get("gXS_Higgsino_C1mN1");
  gXS_Higgsino[2] = (TGraphErrors *)tfXS->Get("gXS_Higgsino_C1C1");

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  c0 = new TCanvas("c0", "c0", 800, 600);

  TPad *pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();

  std::cout << "Start Loading Signal MC" << std::endl;
  /* **********   Start Loading Signal MC   ********** */
  for(int iMC16=0;iMC16<nRunMC16;iMC16++){
    std::cout << MCName[iMC16] << " started" << std::endl;

    /* **********   Load Signal MC for Higgsino 0p2  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_0p2;iSignal++){
      std::cout << "Higgsino 0p2 : " << iSignal << std::endl;
      mySignal_Higgsino_0p2[iMC16][iSignal] = new std::vector<NtupleReader *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_0p2[iSignal].size());iDSID++){
	NtupleReader *myDS = new NtupleReader(Form("/data3/tkaji/SignalProduction/Common_v2.3.x/run/Output/out.%d_%s.common_ntuple.root", vDSID_Higgsino_0p2[iSignal].at(iDSID), MCName[iMC16].c_str()));
	mySignal_Higgsino_0p2[iMC16][iSignal]->push_back(myDS);
	vSFSig_Higgsino_0p2[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_Higgsino[iDSID]->Eval(CMass_Higgsino_0p2[iSignal]))*vFilter_Higgsino_0p2[iSignal].at(iDSID)/(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_0p2_TruthPt[iSignal]     = new TH1D(Form("hSignal_Higgsino_0p2_TruthPt_%d", iSignal), ";Truth p_{T} [GeV]", nLogPt, XBinsLogPt);
	hSignal_Higgsino_0p2_DecayRadius[iSignal] = new TH1D(Form("hSignal_Higgsino_0p2_DecayRadius_%d", iSignal), ";Decay Radius [mm]", 22, 100, 320);
	hSignal_Higgsino_0p2_ProperTime[iSignal]  = new TH1D(Form("hSignal_Higgsino_0p2_ProperTime_%d", iSignal), ";Proper Lifetime [ns]", 50, 0.0, 0.5);
	hSignal_Higgsino_From0p2_TruthPt[iSignal]     = new TH1D(Form("hSignal_Higgsino_From0p2_TruthPt_%d", iSignal), ";Truth p_{T} [GeV]", nLogPt, XBinsLogPt);
	hSignal_Higgsino_From0p2_DecayRadius[iSignal] = new TH1D(Form("hSignal_Higgsino_From0p2_DecayRadius_%d", iSignal), ";Decay Radius [mm]", 22, 100, 320);
	hSignal_Higgsino_From0p2_ProperTime[iSignal]  = new TH1D(Form("hSignal_Higgsino_From0p2_ProperTime_%d", iSignal), ";Proper Lifetime [ns]", 50, 0.0, 0.5);
      }// iMC16==0
    }// for iSignal

    /* **********   Load Signal MC for Higgsino 1p0  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_1p0;iSignal++){
      std::cout << "Higgsino 1p0 : " << iSignal << std::endl;
      mySignal_Higgsino_1p0[iMC16][iSignal] = new std::vector<NtupleReader *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_1p0[iSignal].size());iDSID++){
	NtupleReader *myDS = new NtupleReader(Form("/data3/tkaji/SignalProduction/Common_v2.3.x/run/Output/out.%d_%s.common_ntuple.root", vDSID_Higgsino_1p0[iSignal].at(iDSID), MCName[iMC16].c_str()));
	mySignal_Higgsino_1p0[iMC16][iSignal]->push_back(myDS);
	vSFSig_Higgsino_1p0[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_Higgsino[iDSID]->Eval(CMass_Higgsino_1p0[iSignal]))*vFilter_Higgsino_1p0[iSignal].at(iDSID)/(mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_1p0_TruthPt[iSignal]     = new TH1D(Form("hSignal_Higgsino_1p0_TruthPt_%d", iSignal), ";Truth p_{T} [GeV]", nLogPt, XBinsLogPt);
	hSignal_Higgsino_1p0_DecayRadius[iSignal] = new TH1D(Form("hSignal_Higgsino_1p0_DecayRadius_%d", iSignal), ";Decay Radius [mm]", 22, 100, 320);
	hSignal_Higgsino_1p0_ProperTime[iSignal]  = new TH1D(Form("hSignal_Higgsino_1p0_ProperTime_%d", iSignal), ";Proper Lifetime [ns]", 50, 0.0, 0.5);
      }// iMC16==0
    }// for iSignal

    /* **********   Load Signal MC for Higgsino 4p0  ********** */
    for(int iSignal=0;iSignal<nSignal_Higgsino_4p0;iSignal++){
      std::cout << "Higgsino 4p0 : " << iSignal << std::endl;
      mySignal_Higgsino_4p0[iMC16][iSignal] = new std::vector<NtupleReader *>;
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_4p0[iSignal].size());iDSID++){
	NtupleReader *myDS = new NtupleReader(Form("/data3/tkaji/SignalProduction/Common_v2.3.x/run/Output/out.%d_%s.common_ntuple.root", vDSID_Higgsino_4p0[iSignal].at(iDSID), MCName[iMC16].c_str()));
	mySignal_Higgsino_4p0[iMC16][iSignal]->push_back(myDS);
	vSFSig_Higgsino_4p0[iMC16][iSignal].push_back(IntLumi[iMC16]*(gXS_Higgsino[iDSID]->Eval(CMass_Higgsino_4p0[iSignal]))*vFilter_Higgsino_4p0[iSignal].at(iDSID)/(mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
      }// for iDSID
      if(iMC16==0){
	hSignal_Higgsino_4p0_TruthPt[iSignal]     = new TH1D(Form("hSignal_Higgsino_4p0_TruthPt_%d", iSignal), ";Truth p_{T} [GeV]", nLogPt, XBinsLogPt);
	hSignal_Higgsino_4p0_DecayRadius[iSignal] = new TH1D(Form("hSignal_Higgsino_4p0_DecayRadius_%d", iSignal), ";Decay Radius [mm]", 22, 100, 320);
	hSignal_Higgsino_4p0_ProperTime[iSignal]  = new TH1D(Form("hSignal_Higgsino_4p0_ProperTime_%d", iSignal), ";Proper Lifetime [ns]", 50, 0.0, 0.5);
      }// iMC16==0
    }// for iSignal
  }// iMC

  for(int iSignal=0;iSignal<nSignal_Higgsino_0p2;iSignal++){
    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_0p2[iSignal].size());iDSID++){	
	int nEntry = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->nEntry;
	for(int iEntry=0;iEntry<nEntry;iEntry++){
	  mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->GetEntry(iEntry);

	  if(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->IsPassedBadEventVeto==false)
	    continue;
	  if(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->LU_METtrigger_SUSYTools==false)
	    continue;
	  
	  // truth particle
	  if(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->size() > 0){
	    for(int i=0;i<(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->size());i++){
	      int    PDGID = (int)TMath::Abs(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->PdgId);
	      float  DecayRadius = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->DecayRadius;
	      float  ProperTime = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->ProperTime;
	      double TruthPt = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->p4.Pt()/1000.0;

	      if(PDGID==1000024 && DecayRadius > 120.0 && DecayRadius < 300.0){		
		double weight = 1.0;
		for(int iTruth=0;iTruth<(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->size());iTruth++){
		  int tmpID = (int)TMath::Abs(mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->at(iTruth)->PdgId);
		  if(tmpID!=1000024)
		    continue;
		  float tmpProperTime = mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->truthParticles->at(iTruth)->ProperTime;
		  weight *= MyUtil::GetWeightLifetime(tmpProperTime, 0.2, HiggsinoLifetime[iSignal]);
		}// for iTruth		
		
		double weightCommon = 1.0;
		//weightCommon *= mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->weightXsec;
		weightCommon *= mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->weightMCweight;
		weightCommon *= mySignal_Higgsino_0p2[iMC16][iSignal]->at(iDSID)->weightPileupReweighting;
		
		hSignal_Higgsino_0p2_TruthPt[iSignal]->Fill(TruthPt, weightCommon*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_0p2_DecayRadius[iSignal]->Fill(DecayRadius, weightCommon*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_0p2_ProperTime[iSignal]->Fill(ProperTime, weightCommon*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));

		hSignal_Higgsino_From0p2_TruthPt[iSignal]->Fill(TruthPt, weight*weightCommon*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_From0p2_DecayRadius[iSignal]->Fill(DecayRadius, weight*weightCommon*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_From0p2_ProperTime[iSignal]->Fill(ProperTime, weight*weightCommon*vSFSig_Higgsino_0p2[iMC16][iSignal].at(iDSID));
	      }// if chargino decay 120 < R < 300
	    }// for truth
	  }// if truth size > 0
	}// for iEntry
      }// for iDSID      
    }// for iMC
  }// for iSignal

  for(int iSignal=0;iSignal<nSignal_Higgsino_1p0;iSignal++){
    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_1p0[iSignal].size());iDSID++){	
	int nEntry = mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->nEntry;
	for(int iEntry=0;iEntry<nEntry;iEntry++){
	  mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->GetEntry(iEntry);

	  if(mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->IsPassedBadEventVeto==false)
	    continue;
	  if(mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->LU_METtrigger_SUSYTools==false)
	    continue;
	  
	  // truth particle
	  if(mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->size() > 0){
	    for(int i=0;i<(mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->size());i++){
	      double PDGID = TMath::Abs(mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->PdgId);
	      double DecayRadius = mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->DecayRadius;
	      double ProperTime = mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->ProperTime;
	      double TruthPt = mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->p4.Pt()/1000.0;

	      double weightCommon = 1.0;
	      //weightCommon *= mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->weightXsec;
	      weightCommon *= mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->weightMCweight;
	      weightCommon *= mySignal_Higgsino_1p0[iMC16][iSignal]->at(iDSID)->weightPileupReweighting;
	      
	      if(PDGID==1000024 && DecayRadius > 120.0 && DecayRadius < 300.0){
		hSignal_Higgsino_1p0_TruthPt[iSignal]->Fill(TruthPt, weightCommon*vSFSig_Higgsino_1p0[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_1p0_DecayRadius[iSignal]->Fill(DecayRadius, weightCommon*vSFSig_Higgsino_1p0[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_1p0_ProperTime[iSignal]->Fill(ProperTime, weightCommon*vSFSig_Higgsino_1p0[iMC16][iSignal].at(iDSID));
	      }
	    }// for 
	  }// truth jet
	}// for iEntry
      }// for iDSID      
    }// for iMC
  }// for iSignal

  for(int iSignal=0;iSignal<nSignal_Higgsino_4p0;iSignal++){
    for(int iMC16=0;iMC16<nRunMC16;iMC16++){
      for(unsigned int iDSID=0;iDSID<(vDSID_Higgsino_4p0[iSignal].size());iDSID++){	
	int nEntry = mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->nEntry;
	for(int iEntry=0;iEntry<nEntry;iEntry++){
	  mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->GetEntry(iEntry);

	  if(mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->IsPassedBadEventVeto==false)
	    continue;
	  if(mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->LU_METtrigger_SUSYTools==false)
	    continue;
	  
	  // truth particle
	  if(mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->truthParticles->size() > 0){
	    for(int i=0;i<(mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->truthParticles->size());i++){
	      double PDGID = TMath::Abs(mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->PdgId);
	      double DecayRadius = mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->DecayRadius;
	      double ProperTime = mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->ProperTime;
	      double TruthPt = mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->truthParticles->at(i)->p4.Pt()/1000.0;
	      
	      double weightCommon = 1.0;
	      //weightCommon *= mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->weightXsec;
	      weightCommon *= mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->weightMCweight;
	      weightCommon *= mySignal_Higgsino_4p0[iMC16][iSignal]->at(iDSID)->weightPileupReweighting;

	      if(PDGID==1000024 && DecayRadius > 120.0 && DecayRadius < 300.0){
		hSignal_Higgsino_4p0_TruthPt[iSignal]->Fill(TruthPt, weightCommon*vSFSig_Higgsino_4p0[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_4p0_DecayRadius[iSignal]->Fill(DecayRadius, weightCommon*vSFSig_Higgsino_4p0[iMC16][iSignal].at(iDSID));
		hSignal_Higgsino_4p0_ProperTime[iSignal]->Fill(ProperTime, weightCommon*vSFSig_Higgsino_4p0[iMC16][iSignal].at(iDSID));
	      }
	    }// for 
	  }// truth jet
	}// for iEntry
      }// for iDSID      
    }// for iMC
  }// for iSignal

  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");

  // 0p2
  for(int i=0;i<3;i++){
    pad0->SetLogx(true);
    hSignal_Higgsino_0p2_TruthPt[i]->Draw();
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    pad0->SetLogx(false);

    hSignal_Higgsino_0p2_DecayRadius[i]->Draw();
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hSignal_Higgsino_0p2_ProperTime[i]->Draw();
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }

  for(int i=0;i<3;i++){
    // 0p055, 0p046, 0p037
    pad0->SetLogx(true);
    hSignal_Higgsino_From0p2_TruthPt[i]->SetLineColor(kBlack);
    hSignal_Higgsino_From0p2_TruthPt[i]->SetMarkerColor(kBlack);
    hSignal_Higgsino_From0p2_TruthPt[i]->Draw();

    hSignal_Higgsino_1p0_TruthPt[i]->SetLineColor(kRed);
    hSignal_Higgsino_1p0_TruthPt[i]->SetMarkerColor(kRed);
    hSignal_Higgsino_1p0_TruthPt[i]->Draw("same");
    
    hSignal_Higgsino_4p0_TruthPt[i]->SetLineColor(kBlue);
    hSignal_Higgsino_4p0_TruthPt[i]->SetMarkerColor(kBlue);
    hSignal_Higgsino_4p0_TruthPt[i]->Draw("same");    
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    pad0->SetLogx(false);

    hSignal_Higgsino_From0p2_DecayRadius[i]->SetLineColor(kBlack);
    hSignal_Higgsino_From0p2_DecayRadius[i]->SetMarkerColor(kBlack);
    hSignal_Higgsino_From0p2_DecayRadius[i]->Draw();

    hSignal_Higgsino_1p0_DecayRadius[i]->SetLineColor(kRed);
    hSignal_Higgsino_1p0_DecayRadius[i]->SetMarkerColor(kRed);
    hSignal_Higgsino_1p0_DecayRadius[i]->Draw("same");
    
    hSignal_Higgsino_4p0_DecayRadius[i]->SetLineColor(kBlue);
    hSignal_Higgsino_4p0_DecayRadius[i]->SetMarkerColor(kBlue);
    hSignal_Higgsino_4p0_DecayRadius[i]->Draw("same");    
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hSignal_Higgsino_From0p2_ProperTime[i]->SetLineColor(kBlack);
    hSignal_Higgsino_From0p2_ProperTime[i]->SetMarkerColor(kBlack);
    hSignal_Higgsino_From0p2_ProperTime[i]->Draw();

    hSignal_Higgsino_1p0_ProperTime[i]->SetLineColor(kRed);
    hSignal_Higgsino_1p0_ProperTime[i]->SetMarkerColor(kRed);
    hSignal_Higgsino_1p0_ProperTime[i]->Draw("same");
    
    hSignal_Higgsino_4p0_ProperTime[i]->SetLineColor(kBlue);
    hSignal_Higgsino_4p0_ProperTime[i]->SetMarkerColor(kBlue);
    hSignal_Higgsino_4p0_ProperTime[i]->Draw("same");    
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }

  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  return 0;
}
