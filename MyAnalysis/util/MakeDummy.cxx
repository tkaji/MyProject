#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

MyManager *myData;
HistManager *myHistData;
NtupleWriter *myDummy;
TEventList *eventList;

double FakeRatio[3] = {495.0, 127.0, 69.0};
// 100 x 20, 60, 100
int nAdditionalFake[3] = {20, 60, 100}; // +1, 3, 5 events in SR
TH1D *hFakeMET;
TH1D *hFakeFit;
TFile *tfFakeFit;
TFile *tfInput;
TFile *tfOutput;
TCut GetKinematicsCut(int iMETRegion, int iMETType);
TCut CaloVETO  = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
int iChain;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("iChain", po::value<int>()->default_value(1), "i Chain (0 ~ 19)")
    ("output-file,o", po::value<std::string>()->default_value("myDummy"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  //p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  iChain = vm["iChain"].as<int>();

  myData = new MyManager("/data3/tkaji/myAna/Data/data15-18_13TeV.root", "Data 2015-2018");
  myData->LoadMyFile();
  myHistData = myData->myHist_Old4L;
  eventList = new TEventList("eventList", "eventList");

  tfFakeFit = new TFile("/afs/cern.ch/user/t/tkaji/hFakeFit.root", "READ");
  gROOT->cd();
  hFakeFit = (TH1D *)tfFakeFit->Get("hFakeFit");
  hFakeMET = new TH1D("hFakeMET", ";MET [GeV];", 3, 100, 250);
  for(int i=0;i<3;i++){
    hFakeMET->SetBinContent(i+1, FakeRatio[i]);
  }

  // Fill low-MET and middle-MET
  eventList->Reset();
  TCut KinematicsCut= "(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0) &&";
  myData->GetNtupleReader("Old4L_Common_SR")->m_tree->Draw(">>eventList", GetKinematicsCut(3, 0) && CaloVETO);
  myDummy = new NtupleWriter("myTree_Old4L_Common_SR");
  for(int iEntry=0;iEntry<(eventList->GetN());iEntry++){
    myData->GetNtupleReader("Old4L_Common_SR")->GetEntry(eventList->GetEntry(iEntry));
    double METVal = myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.Pt()/1000.0;
    double PtVal  = myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.Pt()/1000.0;
    if(METVal > 200.0 && PtVal > 58.4804)
      continue;
    myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
    myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
    myDummy->Fill();
  }// for iEntry
  tfOutput = new TFile("Extracted_SR.root", "RECREATE");
  myDummy->Write();
  tfOutput->Close();
  delete tfOutput; tfOutput=NULL;
  delete myDummy; myDummy=NULL;

  for(int iFile=0;iFile<100;iFile++){
    for(int iFake=0;iFake<3;iFake++){
      myDummy = new NtupleWriter("myTree_Old4L_Common_SR");
      for(int iEvent=0;iEvent<nAdditionalFake[iFake];iEvent++){
	double METVal = hFakeMET->GetRandom();
	double PtVal  = hFakeFit->GetRandom();
	std::cout << "METVal    = " << METVal << std::endl;
	myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.SetPerp(METVal*1000.0);
	std::cout << "missingET = " <<myData->GetNtupleReader("Old4L_Common_SR")->missingET->p4.Pt()/1000.0 << std::endl;
	myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0)->p4.SetPerp(PtVal*1000.0);
	myDummy->FillBasicInfo(myData->GetNtupleReader("Old4L_Common_SR"));
	myDummy->FillDisappearingTrack(myData->GetNtupleReader("Old4L_Common_SR")->Tracks->at(0));
	myDummy->Fill();
      }// for iEvent
      tfOutput = new TFile(Form("myDummy_Fake%d_File%d.root", nAdditionalFake[iFake], iFile), "RECREATE");
      myDummy->Write();
      tfOutput->Close();
      delete tfOutput; tfOutput=NULL;    
      delete myDummy; myDummy=NULL;
    }// for iFake
  }// iFile

  return 0;
}


TCut GetKinematicsCut(int iMETRegion, int iMETType){
  TCut myCut[nKinematicsCut];
  std::string METType="";
  if(iMETType==1){
    METType="_ForEleCR";
  }else if(iMETType==2){
    METType="_ForMuCR";
  }

  if(KinematicsChain[iChain][0] > 0.0){
    if(iMETRegion==0){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > %lf)", METType.c_str(), KinematicsChain[iChain][0]);
    }else if(iMETRegion==1){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 150.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 200.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==2){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0 && MET%s.PhysObjBase.p4.Pt()/1000.0 < 150.0)", METType.c_str(), METType.c_str());
    }else if(iMETRegion==3){
      myCut[0] = Form("(MET%s.PhysObjBase.p4.Pt()/1000.0 > 100.0)", METType.c_str());
    }
  }
 
  for(int iJet=0;iJet<4;iJet++){
    if(KinematicsChain[iChain][iJet+1] > 0.0){
      myCut[iJet+1] = Form("(GoodJets[%d].p4.Pt()/1000.0 > %lf)", iJet, KinematicsChain[iChain][iJet+1]);
    }
  }// for iJet

  if(KinematicsChain[iChain][5] > 0.0){
    myCut[5] = Form("(JetMetdPhiMin50%s > %lf)", METType.c_str(), KinematicsChain[iChain][5]);
  }// JetMetdPhi50

  if(KinematicsChain[iChain][6] > 0.0){
    myCut[6] = Form("(JetMetdPhiMin20%s > %lf)", METType.c_str(), KinematicsChain[iChain][6]);
  }// JetMetdPhi20

  if(KinematicsChain[iChain][7] > 0.0){
    myCut[7] = Form("(HT > %lf)", KinematicsChain[iChain][7]);
  }// HT

  if(KinematicsChain[iChain][8] > 0.0){
    myCut[8] = Form("(EffMass > %lf)", KinematicsChain[iChain][8]);
  }// Meff

  if(KinematicsChain[iChain][9] > 0.0){
    myCut[9] = Form("(Aplanarity > %lf)", KinematicsChain[iChain][9]);
  }// Aplanarity

  if(KinematicsChain[iChain][10] > 0.0){
    myCut[10] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/TMath::Sqrt(HT) > %lf)", METType.c_str(), KinematicsChain[iChain][10]);
  }// MET/sqrt(HT)

  if(KinematicsChain[iChain][11] > 0.0){
    myCut[11] = Form("((MET%s.PhysObjBase.p4.Pt()/1000.0)/EffMass > %lf)", METType.c_str(), KinematicsChain[iChain][11]);
  }// MET/Meff

  TCut ret;
  for(int i=0;i<nKinematicsCut;i++){
    ret = ret && myCut[i];
  }
  //ret = ret && "(DisappearingTracks.p4.Pt()/1000.0 > 20.0)";
  return ret;
}
