#include "SignalSyst.h"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TCanvas *c0;
//const int nMC16 = 3;
const std::string DirSignal = "/gpfs/fs6001/toshiaki/Common_tmp/";

const std::string PlotStatus="Internal";
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
const double IntLumi[nMC16]={2798.72 + 31260.7, 44083.8, 58125.9}; // special GRL pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];

std::string PDFName;

TFile *tfMyInput;
TH2D *hTrigEff[nKinematicsChain][nMETTrig];
TGraphAsymmErrors *gTrigEff[nKinematicsChain][nMETTrig];
bool fDump=false;
bool fAllStrong=true;
int ErrOpt;
double GetTrigEffVal(UInt_t v_runNum, double v_METPt, double v_JetPt, int iChain, int fErr=0);
double crystallBallIntegral(double* x, double *par);
TH1D *h1;
TLegend *tl;
TH2D *hFrame;
TGraph *gBP_EWK_TST;
TGraph *gBP_EWK_Up;
TGraph *gBP_EWK_Down;

TGraph *gBP_Strong_TST;
TGraph *gBP_Strong_Up;
TGraph *gBP_Strong_Down;

TH1D *hPU_Nominal;
TH1D *hPU_Up;
TH1D *hPU_Down;

int iBP=6;

std::vector<mySystVal> myEWK_0p2;
const int nEWK_0p2=10;
std::vector<std::vector<int>> vDSID_EWK_0p2 = {{448390, 448391}, {448482, 448483}, {448484, 448485}, {448486, 448487}, {448302, 448303},
					       {448496, 448497}, {448394, 448395}, {448396, 448397}, {448398, 448399}, {448400, 448401}};
//{448304, 448305}, {448394, 448395}, {448396, 448397}, {448398, 448399}, {448400, 448401}};
std::vector<double> CMass_EWK_0p2  = {            90.7,            199.7,            299.8,            400.3,            500.2,
						  599.9,            700.1,            799.7,            899.6,           1000.0};

std::vector<mySystVal> myHiggsino_0p2;
const int nHiggsino_0p2=12;
std::vector<std::vector<int>> vDSID_Higgsino_0p2 = {{448446, 448447, 448448}, {448449, 448450, 448451}, {500200, 500201, 500202},
						     {448452, 448453, 448454}, {500300, 500301, 500302}, {500400, 500401, 500402},
						     {500500, 500501, 500502}, {500600, 500601, 500602}, {500700, 500701, 500702},
						     {500800, 500801, 500802}, {500900, 500901, 500902}, {501000, 501001, 501002}};
std::vector<double> CMass_Higgsino_0p2  = { 120, 160, 200, 
					    240, 300, 400, 
					    500, 600, 700, 
					    800, 900, 1000};

std::vector<mySystVal> myStrong_1p0;
std::vector<std::vector<int>> vDSID_Strong_1p0    = {{448350}, {448351}, {448352}, {448353}, {448354}, {448355}, {448356}, {448357}, {448358}, {448359}, 
						     {448360}, {448361}, {448362}, {448363}, {448364}, {448365}, {448366}, {448367}, {448368}, {448369},
						     {448370}, {448371}, {448372}, {448373}, {448374}, {448375}, {448376}, {448377}, {448378}, {448379},
						     {448550}, {448551}, {448552}, {448553}, {448554}, {448555}, {448556}, {448557}, {448558}, {448559},
						     {448560}, {448561}, {448562}, {448563}, {448564}, {448565}, {448566}, {448567}, {448568}, {448569},
						     {448570}, {448571}, {448572}, {448573}, {448574}, {448575}, {448576}, {448577}, {448578}, {448579},
						     {448580}, {448581}, {448582}, {448583}, {448584}, {448585}, {448586}, {448587}, {448588}, {448589},
						     {448590}, {448591}, {448592}, {448593}};

std::vector<double> GMass_Strong_1p0 = {   700,    800,   1000,   1000,   1200,   1200,   1200,   1200,   1400,   1400,
					   1400,   1400,   1400,   1600,   1600,   1600,   1600,   1600,   1600,   1600,
					   1800,   1800,   1800,   2000,   2000,   2000,   2200,   2200,   2200,   2200,
					   700,    800,    800,   1000,   1000,   1000,   1000,   1200,   1200,   1200,
					   1400,   1400,   1400,   1600,   1600,   1800,   1800,   1800,   1800,   1800,
					   1800,   1800,   2000,   2000,   2000,   2000,   2000,   2000,   2000,   2000,
					   2200,   2200,   2200,   2200,   2200,   2200,   2200,   2200,   2400,   2400,
					   2400,   2400,   2400,   2400};
std::vector<double> CMass_Strong_1p0 = {   650,    750,    100,    950,    100,    900,   1100,   1150,    100,    900,
					   1100,   1300,   1350,    100,    700,    900,   1100,   1300,   1500,   1550,
					   100,    700,   1750,    100,   1900,   1950,    100,   1900,   2100,   2150,
					   600,    600,    700,    300,    500,    700,    900,    300,    500,    700,
					   300,    500,    700,    300,    500,    300,    500,    900,   1100,   1300,
					   1500,   1700,    300,    500,    700,    900,   1100,   1300,   1500,   1700,
					   300,    500,    700,    900,   1100,   1300,   1500,   1700,    100,    300,
					   700,   1100,   1500,   1900};

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("ErrOpt", po::value<int>()->default_value(0), "Error option")
    ("iBP", po::value<int>()->default_value(6), "iBP")
    ("output-file,o", po::value<std::string>()->default_value("SignalSyst"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  std::string OutputFileName = vm["output-file"].as<std::string>(); 
  ErrOpt = vm["ErrOpt"].as<int>();
  iBP = vm["iBP"].as<int>();
  iBP-=1;

  //tfMyInput = new TFile("/gpfs/fs6001/toshiaki/Fitting/disappearingtracksearch_shapefitter/data/Input.root", "READ");
  tfMyInput = new TFile("/gpfs/fs6001/toshiaki/MyProject/InputFile/TrigEffwithError.root", "READ");
  gROOT->cd();
  for(int iChain=0;iChain<nKinematicsChain;iChain++){
    for(int iTrig=0;iTrig<nMETTrig;iTrig++){
      hTrigEff[iChain][iTrig] = (TH2D *)tfMyInput->Get(Form("METTrigEff/hTrigMETJetEff_Chain%d_%d", iChain, iTrig));
      gTrigEff[iChain][iTrig] = (TGraphAsymmErrors *)tfMyInput->Get(Form("METTrigEff/gTrigMETJetEff_Chain%d_%d", iChain, iTrig));
    }// for iTrig
  }// for iChain

  hPU_Nominal = new TH1D("hPU_Nominal", "", 70, 0, 70);
  hPU_Up      = new TH1D("hPU_Up", "", 70, 0, 70);
  hPU_Down    = new TH1D("hPU_down", "", 70, 0, 70);

  h1 = new TH1D("h1", "", 3, 0, 3);
  tfInput = new TFile("/gpfs/fs6001/toshiaki/Fitting/disappearingtracksearch_shapefitter/data/Input.root", "READ");
  gROOT->cd();
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    gSF_DeadModule[iMC16] = (TGraphAsymmErrors *)tfInput->Get(Form("SF_DeadModule_%s", MCName[iMC16].c_str()));
  }
  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, 0.0);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, 0.0);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i
  hTrackPt = new TH1D("hTrackPt", ";track p_{T} [GeV]", nBinsMySignal, XBinsMySignal);

  std::vector<std::vector<int>> vDSID;
  vDSID  = vDSID_EWK_0p2;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){
    if(iSignal!=iBP)
      continue;
    mySystVal myVal;	
    for(unsigned int iDSID=0;iDSID<(vDSID.at(iSignal).size());iDSID++){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	mySignalSyst *mySignal = new mySignalSyst(Form("%s/out.%d_%s._000001.common_ntuple.root", DirSignal.c_str(), vDSID.at(iSignal).at(iDSID), MCName[iMC16].c_str()));
	double vSF = IntLumi[iMC16]/mySignal->hSumOfWeightsBCK->GetBinContent(1);
	
	for(int iEntry=0;iEntry<(mySignal->nEntry);iEntry++){
	  mySignal->m_tree->GetEntry(iEntry);
	  
	  hPU_Nominal->Fill(mySignal->averageInteractionsPerCrossing, vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[0][0])*mySignal->GetTrackWeight(iMC16));
	  if(mySignal->IsPassedKinematics_EWK){
	    fDump=false;
	    //myVal.Val_Nominal += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[0][0])*GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt, mySignal->JetPt, 1, ErrOpt);
	    
	    myVal.Val_Nominal += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[0][0])*GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt, mySignal->JetPt, 1, ErrOpt)*mySignal->GetTrackWeight(iMC16);
	    h1->Fill(0.0, vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[0][0])*GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt, mySignal->JetPt, 1, ErrOpt)*mySignal->GetTrackWeight(iMC16));
	    fDump=false;
	  }
	  // JES
	  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_JES[iUp][iSyst]){
		//		myVal.Val_Syst_JES[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[iUp][iSyst])
		//		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JES[iUp][iSyst], mySignal->JetPt_JES[iUp][iSyst], 1, ErrOpt);
		myVal.Val_Syst_JES[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JES[iUp][iSyst], mySignal->JetPt_JES[iUp][iSyst], 1, ErrOpt) * mySignal->GetTrackWeight(iMC16);
	      }
	    }// for iUp	    
	  }// for JES

	  // JER
	  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_JER[iUp][iSyst]){
		//		myVal.Val_Syst_JER[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JER[iUp][iSyst])
		//		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JER[iUp][iSyst], mySignal->JetPt_JER[iUp][iSyst], 1, ErrOpt);
		myVal.Val_Syst_JER[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JER[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JER[iUp][iSyst], mySignal->JetPt_JER[iUp][iSyst], 1, ErrOpt) * mySignal->GetTrackWeight(iMC16);
	      }
	    }// for iUp	    
	  }// for JER

	  // JVT
	  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_JVT[iUp][iSyst]){
		//myVal.Val_Syst_JVT[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JVT[iUp][iSyst])
		//* GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JVT[iUp][iSyst], mySignal->JetPt_JVT[iUp][iSyst], 1, ErrOpt);
		myVal.Val_Syst_JVT[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JVT[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JVT[iUp][iSyst], mySignal->JetPt_JVT[iUp][iSyst], 1, ErrOpt) * mySignal->GetTrackWeight(iMC16);
	      }
	    }// for iUp	    
	  }// for JVT

	  // PRW
	  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(iUp==1)
		hPU_Down->Fill(mySignal->averageInteractionsPerCrossing, vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst])*mySignal->GetTrackWeight(iMC16));
	      if(iUp==0)
		hPU_Up->Fill(mySignal->averageInteractionsPerCrossing, vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst])*mySignal->GetTrackWeight(iMC16));
	      if(mySignal->IsPassedKinematics_EWK_PRW[iUp][iSyst]){
		//		myVal.Val_Syst_PRW[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst])
		//		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_PRW[iUp][iSyst], mySignal->JetPt_PRW[iUp][iSyst], 1, ErrOpt);
		myVal.Val_Syst_PRW[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_PRW[iUp][iSyst], mySignal->JetPt_PRW[iUp][iSyst], 1, ErrOpt) * mySignal->GetTrackWeight(iMC16);
		h1->Fill(1.0+iUp, vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst]) * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_PRW[iUp][iSyst], mySignal->JetPt_PRW[iUp][iSyst], 1, ErrOpt) * mySignal->GetTrackWeight(iMC16));
	      }
	    }// for iUp	    
	  }// for PRW

	  // TST
	  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
	    if(mySignal->IsPassedKinematics_EWK_TST[iSyst]){
	      //	      myVal.Val_Syst_TST[iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_TST[iSyst])
	      //		* GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_TST[iSyst], mySignal->JetPt_TST[iSyst], 1, ErrOpt);
	      myVal.Val_Syst_TST[iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_TST[iSyst])
		* GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_TST[iSyst], mySignal->JetPt_TST[iSyst], 1, ErrOpt) * mySignal->GetTrackWeight(iMC16);
	    }
	  }// for TST
	  
	}// for iEntry	
	delete mySignal; mySignal=NULL;
      }// for iMC16
    }// for iDSID    
    myVal.CalcFinalVal();
    myEWK_0p2.push_back(myVal);
  }// for iSignal
  std::cout << "EWK_BP Nominal : " << myEWK_0p2.at(0).Val_Nominal << std::endl;
  std::cout << "*****   Benchmark Point for EWK     *****" << std::endl;
  std::cout << "JES : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JES[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JES[1]) << std::endl;
  std::cout << "JER : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JER[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JER[1]) << std::endl;
  std::cout << "JVT : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JVT[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JVT[1]) << std::endl;
  std::cout << "PRW : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_PRW[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_PRW[1]) << std::endl;
  std::cout << "TST : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_TST[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_TST[1]) << std::endl;

  std::cout << " Nominal : " << h1->GetBinContent(1) << " +- " << h1->GetBinError(1) << std::endl;
  std::cout << " Down    : " << h1->GetBinContent(2) << " +- " << h1->GetBinError(2) << std::endl;
  std::cout << " Up      : " << h1->GetBinContent(3) << " +- " << h1->GetBinError(3) << std::endl;
  
  TFile *tfTMP = new TFile("PUDist", "RECREATE");
  hPU_Nominal->Write();
  hPU_Up->Write();
  hPU_Down->Write();
  
  return 0;


  vDSID  = vDSID_Higgsino_0p2;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){
    mySystVal myVal;	
    for(unsigned int iDSID=0;iDSID<(vDSID.at(iSignal).size());iDSID++){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	mySignalSyst *mySignal = new mySignalSyst(Form("%s/out.%d_%s._000001.common_ntuple.root", DirSignal.c_str(), vDSID.at(iSignal).at(iDSID), MCName[iMC16].c_str()));
	double ProdTypeFracP = mySignal->hProdType->GetBinContent(3)/mySignal->hProdType->Integral();
	double ProdTypeFracM = mySignal->hProdType->GetBinContent(4)/mySignal->hProdType->Integral();
	double vSF = (iMC16<2 ? 2.0 : 1.0) * IntLumi[iMC16]/mySignal->hSumOfWeightsBCK->GetBinContent(1);
	if(iSignal==2){
	  if(iDSID==0){
	    vSF *= 0.290274907413/(1.7732458*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 0.157326848316/(1.7732458*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 0.250515174955/0.88125049;
	  }
	}else if(iSignal==4){ // 300 GeV
	  if(iDSID==0){
	    vSF *= 0.064956427812/(0.38078684*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 0.0314521927711/(0.38078684*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 0.0540408247663/0.18654525;
	  }
	}else if(iSignal==5){ // 400 GeV
	  if(iDSID==0){
	    vSF *= 20.4/1000.0/(0.118707*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 8.47/1000.0/(0.118707*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 16.3405/1000.0/0.0571705;
	  }
	}else if(iSignal==6){ // 500 GeV
	  if(iDSID==0){
	    vSF *= 8.03/1000.0/(0.0448229*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 3.06/1000.0/(0.0448229*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 6.21625/1000.0/0.0213396;
	  }
	}else if(iSignal==7){ // 600 GeV
	  if(iDSID==0){
	    vSF *= 3.57/1000.0/(0.0193563*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 1.27/1000.0/(0.0193563*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 2.68388/1000.0/0.00911337;
	  }
	}else if(iSignal==8){ // 700 GeV
	  if(iDSID==0){
	    vSF *= 1.71/1000.0/(0.00902732*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 0.57/1000.0/(0.00902732*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 1.25981/1000.0/0.00419284;
	  }
	}else if(iSignal==9){ // 800 GeV
	  if(iDSID==0){
	    vSF *= 0.87/1000.0/(0.00449222*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 0.28/1000.0/(0.00449222*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 0.630966/1000.0/0.00207425;
	  }
	}else if(iSignal==10){ // 900 GeV
	  if(iDSID==0){
	    vSF *= 0.46/1000.0/(0.00233163*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 0.14/1000.0/(0.00233163*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 0.328529/1000.0/0.0010816;
	  }
	}else if(iSignal==11){ // 1000 GeV
	  if(iDSID==0){
	    vSF *= 0.25/1000.0/(0.00124413*ProdTypeFracP);
	  }else if(iDSID==1){
	    vSF *= 0.07/1000.0/(0.00124413*ProdTypeFracM);
	  }else if(iDSID==2){
	    vSF *= 0.182305/1000.0/0.00057336;
	  }
	}
	
	for(int iEntry=0;iEntry<(mySignal->nEntry);iEntry++){
	  mySignal->m_tree->GetEntry(iEntry);
	  if(((iDSID==0 && mySignal->prodType==0) || (iDSID==1 && mySignal->prodType==1) || (iDSID==2))==false)
	    continue;

	  if(mySignal->IsPassedKinematics_EWK){
	    fDump=false;
	    myVal.Val_Nominal += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[0][0])*GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt, mySignal->JetPt, 1, ErrOpt);
	    fDump=false;
	  }
	  // JES
	  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_JES[iUp][iSyst]){
		myVal.Val_Syst_JES[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JES[iUp][iSyst], mySignal->JetPt_JES[iUp][iSyst], 1, ErrOpt);
	      }
	    }// for iUp	    
	  }// for JES

	  // JER
	  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_JER[iUp][iSyst]){
		myVal.Val_Syst_JER[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JER[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JER[iUp][iSyst], mySignal->JetPt_JER[iUp][iSyst], 1, ErrOpt);
	      }
	    }// for iUp	    
	  }// for JER

	  // JVT
	  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_JVT[iUp][iSyst]){
		myVal.Val_Syst_JVT[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JVT[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JVT[iUp][iSyst], mySignal->JetPt_JVT[iUp][iSyst], 1, ErrOpt);
	      }
	    }// for iUp	    
	  }// for JVT

	  // PRW
	  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_EWK_PRW[iUp][iSyst]){
		myVal.Val_Syst_PRW[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_PRW[iUp][iSyst], mySignal->JetPt_PRW[iUp][iSyst], 1, ErrOpt);
	      }
	    }// for iUp	    
	  }// for PRW

	  // TST
	  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
	    if(mySignal->IsPassedKinematics_EWK_TST[iSyst]){
	      myVal.Val_Syst_TST[iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_TST[iSyst])
		* GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_TST[iSyst], mySignal->JetPt_TST[iSyst], 1, ErrOpt);
	    }
	  }// for TST
	  
	}// for iEntry	
	delete mySignal; mySignal=NULL;
      }// for iMC16
    }// for iDSID    
    myVal.CalcFinalVal();
    myHiggsino_0p2.push_back(myVal);
  }// for iSignal

  for(int i=0;i<12;i++){
    std::cout << "Higgsino Nominal (" << CMass_Higgsino_0p2.at(i) << ") : " << myHiggsino_0p2.at(i).Val_Nominal << std::endl;
  }

  std::cout << "*****   JES   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==0){
      std::cout << Form("if     (info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JES[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JES[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JES[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JES[0])) << std::endl;
    }
  }// for iSignal

  std::cout << "*****   JER   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==0){
      std::cout << Form("if     (info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JER[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JER[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JER[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_JER[0])) << std::endl;
    }
  }// for iSignal

  std::cout << "*****   PRW   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==0){
      std::cout << Form("if     (info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_PRW[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_PRW[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_PRW[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_PRW[0])) << std::endl;
    }
  }// for iSignal

  std::cout << "*****   TST   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==0){
      std::cout << Form("if     (info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_TST[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_TST[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mWino ==  %d)  return array<double,2>{%.3f, %.3f};", (int)CMass_Higgsino_0p2.at(iSignal),
			TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_TST[1]), TMath::Abs((1.0/100.0)*myHiggsino_0p2.at(iSignal).FinalVal_Syst_TST[0])) << std::endl;
    }
  }// for iSignal
  
  vDSID  = vDSID_Strong_1p0;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){
    mySystVal myVal;	
    if(iSignal==5 || iSignal==27 || iSignal==35 || iSignal==50 || iSignal==59 || iSignal==67){

    }else{
      myStrong_1p0.push_back(myVal);
      continue;
    }
    /*
    if(fAllStrong==false && iSignal!=10)
      continue;
    if(iSignal==5){
      myStrong_1p0.push_back(myVal);
      continue;
    }
    if(iSignal==27){
      myStrong_1p0.push_back(myVal);
      continue;
    }
    if(iSignal==35){
      myStrong_1p0.push_back(myVal);
      continue;
    }
    if(iSignal==50){
      myStrong_1p0.push_back(myVal);
      continue;
    }
    if(iSignal==59){
      myStrong_1p0.push_back(myVal);
      continue;
    }
    if(iSignal==67){
      myStrong_1p0.push_back(myVal);
      continue;
    }
    */
    //    if(iSignal!=10)
    //if(iSignal!=47)
    //continue;
    for(unsigned int iDSID=0;iDSID<(vDSID.at(iSignal).size());iDSID++){
      for(int iMC16=0;iMC16<nMC16;iMC16++){
	mySignalSyst *mySignal = new mySignalSyst(Form("%s/out.%d_%s._000001.common_ntuple.root", DirSignal.c_str(), vDSID.at(iSignal).at(iDSID), MCName[iMC16].c_str()));
	double vSF = IntLumi[iMC16]/mySignal->hSumOfWeightsBCK->GetBinContent(1);
	
	for(int iEntry=0;iEntry<(mySignal->nEntry);iEntry++){
	  mySignal->m_tree->GetEntry(iEntry);
	  if(mySignal->IsPassedKinematics_Strong){
	    myVal.Val_Nominal += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[0][0])*GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt, mySignal->JetPt, 3, ErrOpt);
	  }
	  
	  // JES
	  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_Strong_JES[iUp][iSyst]){
		myVal.Val_Syst_JES[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JES[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JES[iUp][iSyst], mySignal->JetPt_JES[iUp][iSyst], 3, ErrOpt);
	      }
	    }// for iUp	    
	  }// for JES

	  // JER
	  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_Strong_JER[iUp][iSyst]){
		myVal.Val_Syst_JER[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JER[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JER[iUp][iSyst], mySignal->JetPt_JER[iUp][iSyst], 3, ErrOpt);
	      }
	    }// for iUp	    
	  }// for JER

	  // JVT
	  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_Strong_JVT[iUp][iSyst]){
		myVal.Val_Syst_JVT[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_JVT[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_JVT[iUp][iSyst], mySignal->JetPt_JVT[iUp][iSyst], 3, ErrOpt);
	      }
	    }// for iUp	    
	  }// for JVT

	  // PRW
	  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
	    for(int iUp=0;iUp<2;iUp++){
	      if(mySignal->IsPassedKinematics_Strong_PRW[iUp][iSyst]){
		myVal.Val_Syst_PRW[iUp][iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_PRW[iUp][iSyst])
		  * GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_PRW[iUp][iSyst], mySignal->JetPt_PRW[iUp][iSyst], 3, ErrOpt);
	      }
	    }// for iUp	    
	  }// for PRW

	  // TST
	  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
	    if(mySignal->IsPassedKinematics_Strong_TST[iSyst]){
	      myVal.Val_Syst_TST[iSyst] += vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting_TST[iSyst])
		* GetTrigEffVal(mySignal->randomRunNumber, mySignal->METPt_TST[iSyst], mySignal->JetPt_TST[iSyst], 3, ErrOpt);
	    }
	  }// for TST
	  
	}// for iEntry	

	delete mySignal; mySignal=NULL;
      }// for iMC16
    }// for iDSID    
    myVal.CalcFinalVal();
    myStrong_1p0.push_back(myVal);
  }// for iSignal
  
  gBP_EWK_Up   = new TGraph();
  gBP_EWK_Down = new TGraph();
  gBP_EWK_TST  = new TGraph();

  gBP_Strong_Up   = new TGraph();
  gBP_Strong_Down = new TGraph();
  gBP_Strong_TST  = new TGraph();

  c0 = new TCanvas("c0", "c0", 1200, 600);
  c0->SetTopMargin(0.05);
  c0->SetRightMargin(0.10);
  c0->SetLeftMargin(0.10);
  c0->SetBottomMargin(0.20);
  c0->SetGridy();

  hFrame = new TH2D("hFrame", ";", 22, 0, 22, 50, -5, 5);
  hFrame->SetYTitle("Relative Variation (%)");
  hFrame->GetYaxis()->SetTitleOffset(0.8);
  hFrame->GetYaxis()->SetTitleSize(0.055);
  hFrame->GetYaxis()->SetLabelSize(0.055);
  //hFrame->GetXaxis()->SetLabelSize(0.055);
  hFrame->SetStats(false);

  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1, SystName_JES[iSyst].c_str());    
    gBP_EWK_Up  ->SetPoint(iSyst, iSyst+0.5, 100.0*(myEWK_0p2.at(0).Val_Syst_JES[0][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
    gBP_EWK_Down->SetPoint(iSyst, iSyst+0.5, 100.0*(myEWK_0p2.at(0).Val_Syst_JES[1][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES, SystName_JER[iSyst].c_str());    
    gBP_EWK_Up  ->SetPoint(iSyst+nSyst_JES, iSyst+0.5+nSyst_JES, 100.0*(myEWK_0p2.at(0).Val_Syst_JER[0][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
    gBP_EWK_Down->SetPoint(iSyst+nSyst_JES, iSyst+0.5+nSyst_JES, 100.0*(myEWK_0p2.at(0).Val_Syst_JER[1][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES+nSyst_JER, SystName_JVT[iSyst].c_str());    
    gBP_EWK_Up  ->SetPoint(iSyst+nSyst_JES+nSyst_JER, iSyst+0.5+nSyst_JES+nSyst_JER, 100.0*(myEWK_0p2.at(0).Val_Syst_JVT[0][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
    gBP_EWK_Down->SetPoint(iSyst+nSyst_JES+nSyst_JER, iSyst+0.5+nSyst_JES+nSyst_JER, 100.0*(myEWK_0p2.at(0).Val_Syst_JVT[1][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES+nSyst_JER+nSyst_JVT, SystName_PRW[iSyst].c_str());    
    gBP_EWK_Up  ->SetPoint(iSyst+nSyst_JES+nSyst_JER+nSyst_JVT, iSyst+0.5+nSyst_JES+nSyst_JER+nSyst_JVT, 100.0*(myEWK_0p2.at(0).Val_Syst_PRW[0][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
    gBP_EWK_Down->SetPoint(iSyst+nSyst_JES+nSyst_JER+nSyst_JVT, iSyst+0.5+nSyst_JES+nSyst_JER+nSyst_JVT, 100.0*(myEWK_0p2.at(0).Val_Syst_PRW[1][iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES+nSyst_JER+nSyst_JVT+nSyst_PRW, SystName_TST[iSyst].c_str());    
    gBP_EWK_TST  ->SetPoint(iSyst, iSyst+0.5+nSyst_JES+nSyst_JER+nSyst_JVT+nSyst_PRW, 100.0*(myEWK_0p2.at(0).Val_Syst_TST[iSyst]/myEWK_0p2.at(0).Val_Nominal - 1.0));
  }// for iSyst
  
  hFrame->Draw();
  gBP_EWK_Up->SetMarkerStyle(8);
  gBP_EWK_Up->SetMarkerSize(1.5);
  gBP_EWK_Up->SetMarkerColor(kRed);
  gBP_EWK_Up->Draw("sameP");
  gBP_EWK_Down->SetMarkerStyle(8);
  gBP_EWK_Down->SetMarkerSize(1.0);
  gBP_EWK_Down->SetMarkerColor(kGreen);
  gBP_EWK_Down->Draw("sameP");
  gBP_EWK_TST->SetMarkerStyle(8);
  gBP_EWK_TST->SetMarkerSize(1.0);
  gBP_EWK_TST->SetMarkerColor(kBlue);
  gBP_EWK_TST->Draw("sameP");
  tl = new TLegend(0.55, 0.75, 0.70, 0.9);
  tl->AddEntry(gBP_EWK_Up, "Up", "P");
  tl->AddEntry(gBP_EWK_Down, "Down", "P");
  tl->Draw();
  c0->Print(Form("SignalSyst_BP_EWK_%d.pdf", iBP+1), "pdf");

  std::cout << "*****   Benchmark Point for EWK     *****" << std::endl;
  std::cout << "JES : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JES[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JES[1]) << std::endl;
  std::cout << "JER : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JER[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JER[1]) << std::endl;
  std::cout << "JVT : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JVT[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_JVT[1]) << std::endl;
  std::cout << "PRW : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_PRW[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_PRW[1]) << std::endl;
  std::cout << "TST : " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_TST[0]) << ", " << Form("%.1f", myEWK_0p2.at(0).FinalVal_Syst_TST[1]) << std::endl;


  std::cout << "Strong_BP Nominal : " << myStrong_1p0.at(0).Val_Nominal << std::endl;
  for(int iSyst=0;iSyst<nSyst_JES;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1, SystName_JES[iSyst].c_str());    
    gBP_Strong_Up  ->SetPoint(iSyst, iSyst+0.5, 100.0*(myStrong_1p0.at(0).Val_Syst_JES[0][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
    gBP_Strong_Down->SetPoint(iSyst, iSyst+0.5, 100.0*(myStrong_1p0.at(0).Val_Syst_JES[1][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_JER;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES, SystName_JER[iSyst].c_str());    
    gBP_Strong_Up  ->SetPoint(iSyst+nSyst_JES, iSyst+0.5+nSyst_JES, 100.0*(myStrong_1p0.at(0).Val_Syst_JER[0][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
    gBP_Strong_Down->SetPoint(iSyst+nSyst_JES, iSyst+0.5+nSyst_JES, 100.0*(myStrong_1p0.at(0).Val_Syst_JER[1][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_JVT;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES+nSyst_JER, SystName_JVT[iSyst].c_str());    
    gBP_Strong_Up  ->SetPoint(iSyst+nSyst_JES+nSyst_JER, iSyst+0.5+nSyst_JES+nSyst_JER, 100.0*(myStrong_1p0.at(0).Val_Syst_JVT[0][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
    gBP_Strong_Down->SetPoint(iSyst+nSyst_JES+nSyst_JER, iSyst+0.5+nSyst_JES+nSyst_JER, 100.0*(myStrong_1p0.at(0).Val_Syst_JVT[1][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_PRW;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES+nSyst_JER+nSyst_JVT, SystName_PRW[iSyst].c_str());    
    gBP_Strong_Up  ->SetPoint(iSyst+nSyst_JES+nSyst_JER+nSyst_JVT, iSyst+0.5+nSyst_JES+nSyst_JER+nSyst_JVT, 100.0*(myStrong_1p0.at(0).Val_Syst_PRW[0][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
    gBP_Strong_Down->SetPoint(iSyst+nSyst_JES+nSyst_JER+nSyst_JVT, iSyst+0.5+nSyst_JES+nSyst_JER+nSyst_JVT, 100.0*(myStrong_1p0.at(0).Val_Syst_PRW[1][iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
  }// for iSyst

  for(int iSyst=0;iSyst<nSyst_TST;iSyst++){
    hFrame->GetXaxis()->SetBinLabel(iSyst+1+nSyst_JES+nSyst_JER+nSyst_JVT+nSyst_PRW, SystName_TST[iSyst].c_str());    
    gBP_Strong_TST  ->SetPoint(iSyst, iSyst+0.5+nSyst_JES+nSyst_JER+nSyst_JVT+nSyst_PRW, 100.0*(myStrong_1p0.at(0).Val_Syst_TST[iSyst]/myStrong_1p0.at(0).Val_Nominal - 1.0));
  }// for iSyst
  
  hFrame->Draw();
  gBP_Strong_Up->SetMarkerStyle(8);
  gBP_Strong_Up->SetMarkerSize(1.5);
  gBP_Strong_Up->SetMarkerColor(kRed);
  gBP_Strong_Up->Draw("sameP");
  gBP_Strong_Down->SetMarkerStyle(8);
  gBP_Strong_Down->SetMarkerSize(1.0);
  gBP_Strong_Down->SetMarkerColor(kGreen);
  gBP_Strong_Down->Draw("sameP");
  gBP_Strong_TST->SetMarkerStyle(8);
  gBP_Strong_TST->SetMarkerSize(1.0);
  gBP_Strong_TST->SetMarkerColor(kBlue);
  gBP_Strong_TST->Draw("sameP");
  c0->Print("SignalSyst_BP_Strong.pdf", "pdf");

  std::cout << "*****   Benchmark Point for Strong     *****" << std::endl;
  std::cout << "JES : " << myStrong_1p0.at(0).FinalVal_Syst_JES[0] << ", " << myStrong_1p0.at(0).FinalVal_Syst_JES[1] << std::endl;
  std::cout << "JER : " << myStrong_1p0.at(0).FinalVal_Syst_JER[0] << ", " << myStrong_1p0.at(0).FinalVal_Syst_JER[1] << std::endl;
  std::cout << "JVT : " << myStrong_1p0.at(0).FinalVal_Syst_JVT[0] << ", " << myStrong_1p0.at(0).FinalVal_Syst_JVT[1] << std::endl;
  std::cout << "PRW : " << myStrong_1p0.at(0).FinalVal_Syst_PRW[0] << ", " << myStrong_1p0.at(0).FinalVal_Syst_PRW[1] << std::endl;
  std::cout << "TST : " << myStrong_1p0.at(0).FinalVal_Syst_TST[0] << ", " << myStrong_1p0.at(0).FinalVal_Syst_TST[1] << std::endl;

  std::cout << std::endl;
  std::cout << "*****   JES   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==5 || iSignal==27 || iSignal==35 || iSignal==50 || iSignal==59 || iSignal==67){

    }else{
      continue;
    }
    /*
    if(iSignal==5)
      continue;
    if(iSignal==27)
      continue;
    if(iSignal==35)
      continue;
    if(iSignal==50)
      continue;
    if(iSignal==59)
      continue;
    if(iSignal==67)
      continue;
    */
    if(iSignal==0){
      std::cout << Form("if     (info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal), 
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JES[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JES[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal),
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JES[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JES[0])) << std::endl;
    }
  }// for iSignal


  std::cout << std::endl;
  std::cout << "*****   JER   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==5 || iSignal==27 || iSignal==35 || iSignal==50 || iSignal==59 || iSignal==67){

    }else{
      continue;
    }
    /*
    if(iSignal==5)
      continue;
    if(iSignal==27)
      continue;
    if(iSignal==35)
      continue;
    if(iSignal==50)
      continue;
    if(iSignal==59)
      continue;
    if(iSignal==67)
      continue;
    */
    if(iSignal==0){
      std::cout << Form("if     (info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal), 
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JER[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JER[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal),
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JER[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_JER[0])) << std::endl;
    }
  }// for iSignal

  std::cout << std::endl;
  std::cout << "*****   PRW   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==5 || iSignal==27 || iSignal==35 || iSignal==50 || iSignal==59 || iSignal==67){

    }else{
      continue;
    }
    /*
    if(iSignal==5)
      continue;
    if(iSignal==27)
      continue;
    if(iSignal==35)
      continue;
    if(iSignal==50)
      continue;
    if(iSignal==59)
      continue;
    if(iSignal==67)
      continue;
    */
    if(iSignal==0){
      std::cout << Form("if     (info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal), 
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_PRW[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_PRW[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal),
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_PRW[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_PRW[0])) << std::endl;
    }
  }// for iSignal

  std::cout << std::endl;
  std::cout << "*****   TST   *****" << std::endl;
  for(unsigned int iSignal=0;iSignal<vDSID.size();iSignal++){    
    if(iSignal==5 || iSignal==27 || iSignal==35 || iSignal==50 || iSignal==59 || iSignal==67){

    }else{
      continue;
    }
    /*
    if(iSignal==5)
      continue;
    if(iSignal==27)
      continue;
    if(iSignal==35)
      continue;
    if(iSignal==50)
      continue;
    if(iSignal==59)
      continue;
    if(iSignal==67)
      continue;
    */
    if(iSignal==0){
      std::cout << Form("if     (info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal), 
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_TST[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_TST[0])) << std::endl;
    }else{
      std::cout << Form("else if(info.mGluino == %d && info.mWino == %d)  return array<double,2>{%.3f, %.3f};", (int)GMass_Strong_1p0.at(iSignal), (int)CMass_Strong_1p0.at(iSignal),
			TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_TST[1]), TMath::Abs((1.0/100.0)*myStrong_1p0.at(iSignal).FinalVal_Syst_TST[0])) << std::endl;
    }
  }// for iSignal

  return 0;
}

double GetTrigEffVal(UInt_t v_runNum, double v_METPt, double v_JetPt, int iChain, int fErr){
  int iTrig = MyUtil::GetUnprescaledMETTrigger(v_runNum);
  int iX = hTrigEff[iChain][iTrig]->GetXaxis()->FindBin(v_METPt);
  int iY = hTrigEff[iChain][iTrig]->GetXaxis()->FindBin(v_JetPt);
  if(iX==0) iX=1;
  if(iY==0) iY=1;
  if(iX==(hTrigEff[iChain][iTrig]->GetNbinsX()+1)) iX=hTrigEff[iChain][iTrig]->GetNbinsX();
  if(iY==(hTrigEff[iChain][iTrig]->GetNbinsY()+1)) iY=hTrigEff[iChain][iTrig]->GetNbinsY();
  
  if(fDump){
    std::cout << gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5) << " + " << gTrigEff[iChain][iTrig]->GetErrorYhigh(iX+50*(iY-1)-1) << " - " << gTrigEff[iChain][iTrig]->GetErrorYlow(iX+50*(iY-1)-1) 
	      << ", iX = " << iX << ", iY = " << iY 
	      << ",   " << v_runNum << ", " << v_METPt << ", " << v_JetPt << std::endl;
  }
  if(fErr==0){
    //std::cout << gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5) << std::endl;
    return gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5);
    //return hTrigEff[iChain][iTrig]->GetBinContent(iX, iY);
  }else if(fErr==1){
    //std::cout << gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5) + gTrigEff[iChain][iTrig]->GetErrorYhigh(iX+50*(iY-1)-1) << std::endl;
    return gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5) + gTrigEff[iChain][iTrig]->GetErrorYhigh(iX+50*(iY-1)-1);
    //    return hTrigEff[iChain][iTrig]->GetBinContent(iX, iY) + hTrigEff[iChain][iTrig]->GetBinErrorUp(iX, iY);
  }else if(fErr==2){
    //std::cout << gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5) - gTrigEff[iChain][iTrig]->GetErrorYlow(iX+50*(iY-1)-1) << std::endl;
    return gTrigEff[iChain][iTrig]->Eval(iX+50*(iY-1)-0.5) - gTrigEff[iChain][iTrig]->GetErrorYlow(iX+50*(iY-1)-1);
    //return hTrigEff[iChain][iTrig]->GetBinContent(iX, iY) - hTrigEff[iChain][iTrig]->GetBinErrorLow(iX, iY);
  }else{
    return 0.0;
  }
}
double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}
