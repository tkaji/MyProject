#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TLegend *tl;
TFile *tf;
NtupleReader *myZmumu=NULL;
NtupleReader *myZee=NULL;
TEventList *eventList=NULL;

TH1D *hPtCone40;
TH1D *hPtCone40_Mod;
TCanvas *c0;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH2D *hFrame;
std::string PDFName;
TH2D *h2;
TH1D *hEle_PU;
TH1D *hEle_PU_Pass;
TH1D *hMu_PU;
TH1D *hMu_PU_Pass;
TGraphAsymmErrors *gEle;
TGraphAsymmErrors *gMu;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("myRetrack"), "output file");
  
  po::positional_options_description p;
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  PDFName = vm["output-file"].as<std::string>();
  std::string OutputFileName = vm["output-file"].as<std::string>();

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  c0 = new TCanvas("c0", "c0", 800, 600);

  hPtCone40 = new TH1D("hPtCone40", "", 100, 0.0, 1000.0);
  hPtCone40_Mod = new TH1D("hPtCone40_Mod", "", 100, 0.0, 1.0);
  myZee   = new NtupleReader("Retrack_Zee.root");
  myZmumu = new NtupleReader("Retrack_Zmumu.root");
  h2 = new TH2D("h2", "", 100, 0.0, 200.0, 100, 0.0, 200.0);
  hEle_PU = new TH1D("hEle_PU", "", 50, 10, 60);
  hEle_PU_Pass = new TH1D("hEle_PU_Pass", "", 50, 10, 60);
  hMu_PU = new TH1D("hMu_PU", "", 50, 10, 60);
  hMu_PU_Pass = new TH1D("hMu_PU_Pass", "", 50, 10, 60);

  int nEntry;
  nEntry = myZmumu->nEntry;
  for(int iEntry=0;iEntry<nEntry;iEntry++){
    if((iEntry+1)%500!=0)
      continue;
    //if(iEntry==100000)
    //break;
    if((iEntry+1)%100000==0)
      std::cout << "iEntry = " << iEntry+1 << std::endl;
    myZmumu->GetEntry(iEntry);

    if(myZmumu->GRL==false || myZmumu->DetectorError==1 || myZmumu->IsPassedBadEventVeto==false)
      continue;
    if(myZmumu->LU_SingleMuontrigger==false)
      continue;

    if(myZmumu->goodMuons->size() < 1 || myZmumu->goodElectrons->size()!=0)
      continue;

    for(unsigned int iMu=0;iMu<(myZmumu->goodMuons->size());iMu++){
      bool IsTrigMatched=false;
      std::vector<std::string> triggerList = MyUtil::GetUnprescaledSingleMuonTrigger(myZmumu->RunNumber);
      for(unsigned int iTrig=0;iTrig<(triggerList.size());iTrig++){
	IsTrigMatched |= myZmumu->goodMuons->at(iMu)->triggerMatching[triggerList.at(iTrig)];
      }
      if(IsTrigMatched==false)
	continue;
      for(unsigned int iMS=0;iMS<(myZmumu->msTracks->size());iMS++){

      }// for iMS
      
    }// for iMu

    /*
    if(myZmumu->goodMuons->size() < 2 || myZmumu->goodElectrons->size()!=0)
      continue;
    
    if(myZmumu->goodMuons->at(0)->index_IDTrack.size() < 1 || myZmumu->goodMuons->at(1)->index_IDTrack.size() < 1)
      continue;
    
    if(myZmumu->conventionalTracks->at(myZmumu->goodMuons->at(0)->index_IDTrack.at(0))->nSCTHits < 6  || myZmumu->conventionalTracks->at(myZmumu->goodMuons->at(1)->index_IDTrack.at(0))->nSCTHits < 6)
      continue;


    double RecoMass = (myZmumu->goodMuons->at(0)->p4 + myZmumu->goodMuons->at(1)->p4).M();
    if(TMath::Abs(RecoMass - Z_mass)/1000.0 > 10.0)
      continue;
    float  AvePU = myZmumu->CorrectedAverageInteractionsPerCrossing;
    
    hMu_PU->Fill(AvePU);
    hMu_PU->Fill(AvePU);

    bool fMatch1 = false;
    bool fMatch2 = false;

    for(unsigned int iTrack=0;iTrack<(myZmumu->conventionalTracks->size());iTrack++){
      if(myZmumu->conventionalTracks->at(iTrack)->nSCTHits!=0)
	continue;
      int TrackQuality = MyUtil::CalcOld4LTrackQuality(myZmumu->conventionalTracks->at(iTrack));
      // 001 0000 0011 1111 1110
      if(MyUtil::IsPassed(TrackQuality, 0x103FE)==false)
	continue;
      double dR1 = myZmumu->goodMuons->at(0)->p4.DeltaR(myZmumu->conventionalTracks->at(iTrack)->p4);
      double dR2 = myZmumu->goodMuons->at(1)->p4.DeltaR(myZmumu->conventionalTracks->at(iTrack)->p4);
      if(dR1 < 0.05){
	hPtCone40->Fill(myZmumu->conventionalTracks->at(iTrack)->ptcone40_1gev);
	hPtCone40_Mod->Fill((myZmumu->conventionalTracks->at(iTrack)->ptcone40overPt_1gev));
	//hPtCone40_Mod->Fill((myZmumu->conventionalTracks->at(iTrack)->ptcone40_1gev - myZmumu->goodMuons->at(0)->p4.Pt()/1000.0)/(myZmumu->conventionalTracks->at(iTrack)->p4.Pt()/1000.0));
	h2->Fill(myZmumu->goodMuons->at(0)->p4.Pt()/1000.0, myZmumu->conventionalTracks->at(iTrack)->p4.Pt()/1000.0);
	if((myZmumu->conventionalTracks->at(iTrack)->ptcone40_1gev - myZmumu->goodMuons->at(0)->p4.Pt()/1000.0)/(myZmumu->conventionalTracks->at(iTrack)->p4.Pt()/1000.0) < 0.04)
	  fMatch1 = true;	
      }
      if(dR2 < 0.05){
	hPtCone40->Fill(myZmumu->conventionalTracks->at(iTrack)->ptcone40_1gev);
	hPtCone40_Mod->Fill((myZmumu->conventionalTracks->at(iTrack)->ptcone40overPt_1gev));
	//hPtCone40_Mod->Fill((myZmumu->conventionalTracks->at(iTrack)->ptcone40_1gev - myZmumu->goodMuons->at(0)->p4.Pt()/1000.0)/(myZmumu->conventionalTracks->at(iTrack)->p4.Pt()/1000.0));
	h2->Fill(myZmumu->goodMuons->at(1)->p4.Pt()/1000.0, myZmumu->conventionalTracks->at(iTrack)->p4.Pt()/1000.0);
	if((myZmumu->conventionalTracks->at(iTrack)->ptcone40_1gev - myZmumu->goodMuons->at(1)->p4.Pt()/1000.0)/(myZmumu->conventionalTracks->at(iTrack)->p4.Pt()/1000.0) < 0.04)
	  fMatch2 = true;
      }
    }// iTrack
    if(fMatch1)
      hMu_PU_Pass->Fill(AvePU);
    if(fMatch2)
      hMu_PU_Pass->Fill(AvePU);
    */
  }// for iEntry

  nEntry = myZee->nEntry;
  for(int iEntry=0;iEntry<nEntry;iEntry++){
    if((iEntry+1)%500!=0)
      continue;
    //if(iEntry==100000)
    //break;
    if((iEntry+1)%100000==0)
      std::cout << "iEntry = " << iEntry+1 << std::endl;
    myZee->GetEntry(iEntry);

    if(myZee->GRL==false || myZee->DetectorError==1 || myZee->IsPassedBadEventVeto==false)
      continue;
    if(myZee->LU_SingleElectrontrigger==false)
      continue;
    if(myZee->goodElectrons->size() < 2 || myZee->goodMuons->size()!=0)
      continue;
    
    if(myZee->goodElectrons->at(0)->index_IDTrack.size() < 1 || myZee->goodElectrons->at(1)->index_IDTrack.size() < 1)
      continue;
    
    if(myZee->conventionalTracks->at(myZee->goodElectrons->at(0)->index_IDTrack.at(0))->nSCTHits < 6  || myZee->conventionalTracks->at(myZee->goodElectrons->at(1)->index_IDTrack.at(0))->nSCTHits < 6)
      continue;

    double RecoMass = (myZee->goodElectrons->at(0)->p4 + myZee->goodElectrons->at(1)->p4).M();
    if(TMath::Abs(RecoMass - Z_mass)/1000.0 > 10.0)
      continue;
    float  AvePU = myZee->CorrectedAverageInteractionsPerCrossing;
    
    hEle_PU->Fill(AvePU);
    hEle_PU->Fill(AvePU);

    bool fMatch1 = false;
    bool fMatch2 = false;

    for(unsigned int iTrack=0;iTrack<(myZee->conventionalTracks->size());iTrack++){
      if(myZee->conventionalTracks->at(iTrack)->nSCTHits!=0)
	continue;
      int TrackQuality = MyUtil::CalcOld4LTrackQuality(myZee->conventionalTracks->at(iTrack));
      // 001 0000 0011 1111 1110
      if(MyUtil::IsPassed(TrackQuality, 0x103FE)==false)
	continue;
      double dR1 = myZee->goodElectrons->at(0)->p4.DeltaR(myZee->conventionalTracks->at(iTrack)->p4);
      double dR2 = myZee->goodElectrons->at(1)->p4.DeltaR(myZee->conventionalTracks->at(iTrack)->p4);
      if(dR1 < 0.05)
	if((myZee->conventionalTracks->at(iTrack)->ptcone40_1gev - myZee->goodElectrons->at(0)->p4.Pt()/1000.0)/(myZee->conventionalTracks->at(iTrack)->p4.Pt()/1000.0) < 0.04)
	  fMatch1 = true;
      if(dR2 < 0.05)
	if((myZee->conventionalTracks->at(iTrack)->ptcone40_1gev - myZee->goodElectrons->at(1)->p4.Pt()/1000.0)/(myZee->conventionalTracks->at(iTrack)->p4.Pt()/1000.0) < 0.04)
	  fMatch2 = true;
    }// iTrack
    if(fMatch1)
      hEle_PU_Pass->Fill(AvePU);
    if(fMatch2)
      hEle_PU_Pass->Fill(AvePU);
  }// for iEntry

  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");
  gMu = new TGraphAsymmErrors(hMu_PU_Pass, hMu_PU);
  hFrame = new TH2D("hFrame", ";Average Interactions Per Crossing;Track Efficiency", 50, 10.0, 60.0, 105, 0.0, 1.05);
  hFrame->Draw();
  gMu->Draw("sameP");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  gEle = new TGraphAsymmErrors(hEle_PU_Pass, hEle_PU);
  hFrame->Draw();
  gEle->Draw("sameP");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hFrame->Draw();
  gEle->SetLineColor(kRed);
  gEle->SetMarkerColor(kRed);
  gEle->Draw("sameP");
  gMu->SetLineColor(kBlue);
  gMu->SetMarkerColor(kBlue);
  gMu->Draw("sameP");

  ATLASLabel(0.2, 0.85, "Internal", kBlack);
  tl = new TLegend(0.2, 0.70, 0.5, 0.85);
  tl->AddEntry(gEle, "Electron", "EP");
  tl->AddEntry(gMu, "Muon", "EP");
  tl->Draw("same");
  
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hPtCone40->AddBinContent(1, hPtCone40->GetBinContent(0));
  hPtCone40->AddBinContent(100, hPtCone40->GetBinContent(101));
  hPtCone40->Draw();
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hPtCone40_Mod->AddBinContent(1, hPtCone40_Mod->GetBinContent(0));
  hPtCone40_Mod->AddBinContent(100, hPtCone40_Mod->GetBinContent(101));
  hPtCone40_Mod->Draw();
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  h2->Draw("colz");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  return 0;
}
