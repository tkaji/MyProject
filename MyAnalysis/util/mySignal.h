#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "TCanvas.h"
#include "TCut.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

const int nMC16 = 3;
const int nRunMC16 = 3;
TGraphAsymmErrors *gSF_DeadModule[nMC16];

const std::string PlotStatus="Internal";
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
//const double IntLumi[nMC16]={3219.56 + 32988.1, 44307.4, 58450.1}; // pb-1
const double IntLumi[nMC16]={2798.72 + 31260.7, 44083.8, 58125.9}; // special GRL pb-1
const double SumLumi = IntLumi[0] + IntLumi[1] + IntLumi[2];

const int iPtBin = 4;
const int nPtScan = 8;
const int nBinsMySignal = 12;
//const int nBinsMySignal = 18;
const double XBinsMySignal[nBinsMySignal+1] = {20, 26.1532, 34.1995, 44.7214, 58.4804, 76.4724, 100, 170.998, 292.402, 500, 1118.03, 2500, 12500};
//const double XBinsMySignal[nBinsMySignal+1] = { 20.0,  30.0,  40.0,  50.0,  60.0,  70.0,  80.0,  90.0, 100.0, 110.0, 
//					       120.0, 130.0, 140.0, 150.0, 160.0, 170.0, 180.0, 190.0, 200.0};

const int nFrom10p0 = 3;
const int nFrom4p0 = 5;
const int nFrom1p0 = 6;
const int nFrom0p2 = 13;
double TauBin[34] = {0.010, 0.013, 0.016, 0.020, 0.025, 0.032, 0.040, 0.050, 0.063, 0.079, 
		     0.100, 0.130, 0.160, 0.200, 0.250, 0.320, 0.400, 0.500, 0.630, 0.790,
		     1.000, 1.300, 1.600, 2.000, 2.500, 3.200, 4.000, 5.000, 6.300, 7.900, 
		     10.000, 13.0, 16.0, 20.0};
//double TauBin[31] = {0.010, 0.013, 0.016, 0.020, 0.025, 0.032, 0.040, 0.050, 0.063, 0.079, 
		     //		     0.100, 0.130, 0.160, 0.200, 0.250, 0.320, 0.400, 0.500, 0.630, 0.790,
		     //		     1.000, 1.300, 1.600, 2.000, 2.500, 3.200, 4.000, 5.000, 6.300, 7.900, 10.000};

TFile *tfInput=NULL;
TH1F *hLumiHistData15;
TH1F *hLumiHistData16;
TH1F *hLumiHistData17;
TH1F *hLumiHistData18;
TH1F *hAxisData15;
TH1F *hAxisData16;
TH1F *hAxisData17;
TH1F *hAxisData18;
TH2D *hTrigMETJetEff[nMETTrig];
bool fSmearTruthPt=false;
bool fUseTriggerBit=true;
bool fDeadModule=false;

//=== Smearing function parameters ===//
const double g_par_Mean  = -4.50114e-01;
const double g_par_Sigma =  1.35062e+01;
const double g_par_Alpha =  1.68961e+00;
const int nSmearRegion = 8;
const double g_par_Mean_ele[nSmearRegion]  = {-0.203310, -0.203310, -0.203310, -0.203310, -0.146075, -0.126751, -0.205570, -0.213854};
const double g_par_Sigma_ele[nSmearRegion] = {20.944675, 19.536572, 18.330746, 17.005663, 15.424842, 14.490877, 13.900709, 14.030027};
const double g_par_Alpha_ele[nSmearRegion] = { 1.858616,  1.858616,  1.858616,  1.858616,  1.822388,  1.655727,  1.544172,  1.637405};
const double g_par_Mean_mu[nSmearRegion]  = {-0.248458, -0.248458, -0.248458, -0.248458, -0.188993, -0.133858, -0.280374, 0.001933};
const double g_par_Sigma_mu[nSmearRegion] = {16.957926, 15.542328, 14.912544, 14.836049, 14.212644, 13.638716, 13.440928, 13.208069};
const double g_par_Alpha_mu[nSmearRegion] = { 1.716832,  1.716832,  1.716832,  1.716832,  1.663774,  1.618689,  1.682396,  1.644168};
TF1 *fSmearInt_ele[nSmearRegion];
TF1 *fSmearInt_mu[nSmearRegion];

TEventList *eventList=NULL;
const int nMETRegion=3;
const std::string RegName[3] = {"high-MET", "middle-MET", "low-MET"};
TGraph *gXS_Strong_uncert=NULL;
TGraphErrors *gXS_Strong=NULL;
TGraphErrors *gXS_EWK[2];
TGraphErrors *gXS_Higgsino[3];
const std::string DirSignal = "/data3/tkaji/myAna/Signal/";


TGraphAsymmErrors *gData15_Period_SR[nMETRegion];
TGraphAsymmErrors *gData16_Period_SR[nMETRegion];
TGraphAsymmErrors *gData17_Period_SR[nMETRegion];
TGraphAsymmErrors *gData18_Period_SR[nMETRegion];

TH1F *hData15_Period_SR[nMETRegion];
TH1F *hData16_Period_SR[nMETRegion];
TH1F *hData17_Period_SR[nMETRegion];
TH1F *hData18_Period_SR[nMETRegion];

TGraphAsymmErrors *gData15_Period_VR[nMETRegion];
TGraphAsymmErrors *gData16_Period_VR[nMETRegion];
TGraphAsymmErrors *gData17_Period_VR[nMETRegion];
TGraphAsymmErrors *gData18_Period_VR[nMETRegion];

TGraphAsymmErrors *gData15_Period_FakeCR;
TGraphAsymmErrors *gData16_Period_FakeCR;
TGraphAsymmErrors *gData17_Period_FakeCR;
TGraphAsymmErrors *gData18_Period_FakeCR;

TGraphAsymmErrors *gData15_Period_HadCR[nMETRegion];
TGraphAsymmErrors *gData16_Period_HadCR[nMETRegion];
TGraphAsymmErrors *gData17_Period_HadCR[nMETRegion];
TGraphAsymmErrors *gData18_Period_HadCR[nMETRegion];

TGraphAsymmErrors *gData15_Period_EleCR[nMETRegion];
TGraphAsymmErrors *gData16_Period_EleCR[nMETRegion];
TGraphAsymmErrors *gData17_Period_EleCR[nMETRegion];
TGraphAsymmErrors *gData18_Period_EleCR[nMETRegion];

TGraphAsymmErrors *gData15_Period_MuCR[nMETRegion];
TGraphAsymmErrors *gData16_Period_MuCR[nMETRegion];
TGraphAsymmErrors *gData17_Period_MuCR[nMETRegion];
TGraphAsymmErrors *gData18_Period_MuCR[nMETRegion];

TH2D *hEleBG_TF_PtEta=NULL;
TH2D *hEleBG_TF_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_PtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_4to123_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to0_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to1_InvPtEta=NULL;
TH2D *hEleBG_TF_CaloIso_432to01_InvPtEta=NULL;
TH1D *hMuBG_TF_1Bin=NULL;
TH2D *hMuBG_TF_PtEta=NULL;
TH2D *hMuBG_TF_InvPtEta=NULL;
TH2D *hMSBG_TF_PhiEta=NULL;
TH1D *P_caloveto05_hadron_StdTrk=NULL;
TH1D *P_caloveto10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_10_hadron_StdTrk=NULL;
TH1D *P_caloveto05_hadron_Trklet=NULL;
TH1D *P_caloveto10_hadron_Trklet=NULL;
TH1D *P_caloveto05_10_hadron_Trklet=NULL;
TH1D *P_caloveto05_electron_StdTrk=NULL;
TH1D *P_caloveto10_electron_StdTrk=NULL;
TH1D *P_caloveto05_10_electron_StdTrk=NULL;
TH1D *P_caloveto05_electron_Trklet=NULL;
TH1D *P_caloveto10_electron_Trklet=NULL;
TH1D *P_caloveto05_10_electron_Trklet=NULL;

TCut CaloVETO_0   = "(DisappearingTracks.etclus20_topo/1000.0 < 5.0)";
//TCut CaloVETO_0   = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 10.0)";
TCut CaloVETO_1   = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 10.0)";
TCut CaloVETO_123 = "(DisappearingTracks.etclus20_topo/1000.0 > 5.0 && DisappearingTracks.etclus20_topo/1000.0 < 20.0)";

TCut CaloVETO_432 = "(DisappearingTracks.etclus20_topo/1000.0 > 10.0)";
TCut CaloVETO_4   = "(DisappearingTracks.etclus20_topo/1000.0 > 20.0)";
TCut FakeD0_CR    = "(TMath::Abs(DisappearingTracks.d0sigTool)>10.0)";
TCut FakeD0_VR    = "(TMath::Abs(DisappearingTracks.d0sigTool)>3.0 && TMath::Abs(DisappearingTracks.d0sigTool)<10.0)";

TCut MCWeight = "(weightXsec*weightMCweight*weightPileupReweighting)";

const int nTau_Strong = 2;
const std::string Tau_Strong[nTau_Strong] = {"0p2", "1p0"};
const int nSignal_Strong[nTau_Strong]   = {2, 74};
const int nReweight_Strong[nTau_Strong] = {0, 1};

std::vector<std::vector<int>> vDSID_Strong_0p2 = {{448380}, {448381}};
std::vector<double> GMass_Strong_0p2           = {    1400,     1800};
std::vector<double> CMass_Strong_0p2           = {    1100,      900};
std::vector<double> TauReweight_Strong_0p2 = {};

std::vector<std::vector<int>> vDSID_Strong_1p0    = {{448350}, {448351}, {448352}, {448353}, {448354}, {448355}, {448356}, {448357}, {448358}, {448359}, 
						     {448360}, {448361}, {448362}, {448363}, {448364}, {448365}, {448366}, {448367}, {448368}, {448369},
						     {448370}, {448371}, {448372}, {448373}, {448374}, {448375}, {448376}, {448377}, {448378}, {448379},
						     {448550}, {448551}, {448552}, {448553}, {448554}, {448555}, {448556}, {448557}, {448558}, {448559},
						     {448560}, {448561}, {448562}, {448563}, {448564}, {448565}, {448566}, {448567}, {448568}, {448569},
						     {448570}, {448571}, {448572}, {448573}, {448574}, {448575}, {448576}, {448577}, {448578}, {448579},
						     {448580}, {448581}, {448582}, {448583}, {448584}, {448585}, {448586}, {448587}, {448588}, {448589},
						     {448590}, {448591}, {448592}, {448593}};
std::vector<double> GMass_Strong_1p0 = {   700,    800,   1000,   1000,   1200,   1200,   1200,   1200,   1400,   1400,
				          1400,   1400,   1400,   1600,   1600,   1600,   1600,   1600,   1600,   1600,
					  1800,   1800,   1800,   2000,   2000,   2000,   2200,   2200,   2200,   2200,
					   700,    800,    800,   1000,   1000,   1000,   1000,   1200,   1200,   1200,
					  1400,   1400,   1400,   1600,   1600,   1800,   1800,   1800,   1800,   1800,
					  1800,   1800,   2000,   2000,   2000,   2000,   2000,   2000,   2000,   2000,
					  2200,   2200,   2200,   2200,   2200,   2200,   2200,   2200,   2400,   2400,
					  2400,   2400,   2400,   2400};
std::vector<double> CMass_Strong_1p0 = {   650,    750,    100,    950,    100,    900,   1100,   1150,    100,    900,
					  1100,   1300,   1350,    100,    700,    900,   1100,   1300,   1500,   1550,
					   100,    700,   1750,    100,   1900,   1950,    100,   1900,   2100,   2150,
					   600,    600,    700,    300,    500,    700,    900,    300,    500,    700,
					   300,    500,    700,    300,    500,    300,    500,    900,   1100,   1300,
					  1500,   1700,    300,    500,    700,    900,   1100,   1300,   1500,   1700,
					   300,    500,    700,    900,   1100,   1300,   1500,   1700,    100,    300,
					   700,   1100,   1500,   1900};
std::vector<double> TauReweight_Strong_1p0 = {0.2};

const int nTau_Higgsino = 4;
const std::string Tau_Higgsino[nTau_Higgsino] = {"0p2", "1p0", "4p0", "10p0"};
const int nSignal_Higgsino[nTau_Higgsino] = {5, 5, 3, 5};
const int nReweight_Higgsino[nTau_Higgsino] = {nFrom0p2, nFrom1p0, nFrom4p0, nFrom10p0};

std::vector<std::vector<int>> vDSID_Higgsino_0p2 = {{448446  , 448447  , 448448  }, {448449  , 448450  , 448451  }, {448482  , 448482  , 448483  },
						    {448452  , 448453  , 448454  }, {448484  , 448484  , 448485  }};
std::vector<double> CMass_Higgsino_0p2       = {120.0, 160.0, 200.0, 240.0, 300.0};
std::vector<double> TauReweight_Higgsino_0p2 = {0.010, 0.013, 0.016, 0.020, 0.025, 0.032, 0.040, 0.050, 0.063, 0.079, 0.100, 0.130, 0.160};

std::vector<std::vector<int>> vDSID_Higgsino_1p0 = {{448455  , 448456  , 448457  }, {448458  , 448459  , 448460  }, {448488  , 448488  , 448489  },
						    {448461  , 448462  , 448463  }, {448490  , 448490  , 448491  }};
std::vector<double> CMass_Higgsino_1p0 = {120.0, 160.0, 200.0, 240.0, 300.0};
std::vector<double> TauReweight_Higgsino_1p0 = {0.25, 0.32, 0.40, 0.50, 0.63, 0.79};

std::vector<std::vector<int>> vDSID_Higgsino_4p0 = {{448464  , 448465  , 448466  }, {448467  , 448468  , 448469  }, {448470  , 448471  , 448472  }};
std::vector<double> CMass_Higgsino_4p0 = {120.0, 160.0, 240.0};
std::vector<double> TauReweight_Higgsino_4p0 = {1.3, 1.6, 2.0, 2.5, 3.2};

std::vector<std::vector<int>> vDSID_Higgsino_10p0 = {{448473  , 448474  , 448475  }, {448476  , 448477  , 448478  }, {448416  , 448416  , 448417  },
						     {448479  , 448480  , 448481  }, {448418  , 448418  , 448419  }};
std::vector<double> CMass_Higgsino_10p0 = {120.0, 160.0, 200.0, 240.0, 300.0};
std::vector<double> TauReweight_Higgsino_10p0 = {5.0, 6.3, 7.9};

const int nTau_EWK = 4;
const std::string Tau_EWK[nTau_EWK] = {"0p2", "1p0", "4p0", "10p0"};
const int nSignal_EWK[nTau_EWK] = {10, 10, 7, 10};
const int nReweight_EWK[nTau_EWK] = {nFrom0p2, nFrom1p0, nFrom4p0, nFrom10p0};

std::vector<std::vector<int>> vDSID_EWK_0p2 = {{448390, 448391}, {448482, 448483}, {448484, 448485}, {448486, 448487}, {448302, 448303},
					       {448304, 448305}, {448394, 448395}, {448396, 448397}, {448398, 448399}, {448400, 448401}};
std::vector<double> CMass_EWK_0p2  = {            90.7,            199.7,            299.8,            400.3,            500.2,
						 599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<double> TauReweight_EWK_0p2 = {0.010, 0.013, 0.016, 0.020, 0.025, 0.032, 0.040, 0.050, 0.063, 0.079, 0.100, 0.130, 0.160};

std::vector<std::vector<int>> vDSID_EWK_1p0 = {{448402, 448403}, {448488, 448489}, {448490, 448491}, {448492, 448493}, {448494, 448495}, 
					       {448496, 448497}, {448498, 448499}, {448404, 448405}, {448406, 448407}, {448408, 448409}};
std::vector<double> CMass_EWK_1p0  = {            90.7,            199.7,            299.8,            400.3,            500.2,
						  599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<double> TauReweight_EWK_1p0 = {0.25, 0.32, 0.40, 0.50, 0.63, 0.79};

std::vector<std::vector<int>> vDSID_EWK_4p0 = {{448500, 448501}, {448502, 448503},
					       {448306, 448307}, {448308, 448309}, {448504, 448505}, {448410, 448411}, {448412, 448413}};
std::vector<double> CMass_EWK_4p0  = {           400.3,            500.2,
						 599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<double> TauReweight_EWK_4p0 = {1.3, 1.6, 2.0, 2.5, 3.2};

std::vector<std::vector<int>> vDSID_EWK_10p0 = {{448414, 448415}, {448416, 448417}, {448418, 448419}, {448506, 448507}, {448508, 448509}, 
						{448510, 448511}, {448512, 448513}, {448514, 448515}, {448516, 448517}, {448420, 448421}};
std::vector<double> CMass_EWK_10p0 = {            90.7,            199.7,            299.8,            400.3,            500.2,
						  599.9,            700.1,            799.7,            899.6,           1000.0};
std::vector<double> TauReweight_EWK_10p0 = {5.0, 6.3, 7.9};

class MySignal{
 public:
  MySignal(const std::string, const std::string);
  ~MySignal();

  int FillPtHist(void);
  int FillYield(void);
  NtupleReader *GetReader(int iMC16, int iSignal, int iDSID, std::string BranchName){
    return mySignal[iMC16].at(iSignal).at(iDSID)->GetNtupleReader(BranchName);
  }

  int nSignal;
  int nReweight;
  std::vector<std::vector<int>> vDSID;
  std::vector<double> CMass;
  std::vector<double> GMass;
  std::vector<double> TauReweight;

  std::string Type;
  std::string Tau;
  double TauVal;
  std::ofstream ofs;

  std::vector<std::vector<MyManager *>> mySignal[nMC16];
  std::vector<std::vector<HistManager *>> myHist[nMC16];
  std::vector<std::vector<double>> SF[nMC16];

  std::vector<TH1D *> hPt[nMETRegion];
  std::vector<std::vector<TH1D *>> hPt_Reweighted[nMETRegion];

  std::vector<double> nSR_LowPt[nMETRegion];
  std::vector<double> nSR_HighPt[nMETRegion];
  std::vector<double> nSR_PtScan[nPtScan][nMETRegion];
  std::vector<double> nSR_CaloSideBand[nMETRegion];
  std::vector<double> nHadCR[nMETRegion];
  std::vector<double> nFakeCR[nMETRegion];
  std::vector<double> nSingleEleCR[nMETRegion];
  std::vector<double> nSingleMuCR[nMETRegion];

  /*
  std::vector<MyManager *> *mySignal[nMC16][nSignal];
  std::vector<HistManager *> *myHist[nMC16][nSignal];
  std::vector<double> SF[nMC16][nSignal];

  TH1D *hPt[nSignal][nMETRegion];
  std::vector<TH1D *> *hPt_Reweighted[nSignal][nMETRegion];

  double nSR_LowPt[nSignal][nMETRegion];
  double nSR_HighPt[nSignal][nMETRegion];
  double nSR_PtScan[nPtScan][nSignal][nMETRegion];
  double nSR_CaloSideBand[nSignal][nMETRegion];
  double nHadCR[nSignal][nMETRegion];
  double nFakeCR[nSignal][nMETRegion];
  double nSingleEleCR[nSignal][nMETRegion];
  double nSingleMuCR[nSignal][nMETRegion];
  */
};

MySignal::MySignal(const std::string Val1, const std::string Val2) : Type(Val1), Tau(Val2)
{
  std::cout << "MySignal::MySignal(" << Type << ", " << Tau << ")" << std::endl;
  if(Tau=="0p2")  TauVal =  0.2;
  if(Tau=="1p0")  TauVal =  1.0;
  if(Tau=="4p0")  TauVal =  4.0;
  if(Tau=="10p0") TauVal = 10.0;        

  if(Type=="EWK" && Tau=="0p2"){
    vDSID = vDSID_EWK_0p2;
    CMass = CMass_EWK_0p2;
    TauReweight = TauReweight_EWK_0p2;
  }else if(Type=="EWK" && Tau=="1p0"){
    vDSID = vDSID_EWK_1p0;
    CMass = CMass_EWK_1p0;
    TauReweight = TauReweight_EWK_1p0;
  }else if(Type=="EWK" && Tau=="4p0"){
    vDSID = vDSID_EWK_4p0;
    CMass = CMass_EWK_4p0;
    TauReweight = TauReweight_EWK_4p0;
  }else if(Type=="EWK" && Tau=="10p0"){
    vDSID = vDSID_EWK_10p0;
    CMass = CMass_EWK_10p0;
    TauReweight = TauReweight_EWK_10p0;
  }else if(Type=="Higgsino" && Tau=="0p2"){
    vDSID = vDSID_Higgsino_0p2;
    CMass = CMass_Higgsino_0p2;
    TauReweight = TauReweight_Higgsino_0p2;
  }else if(Type=="Higgsino" && Tau=="1p0"){
    vDSID = vDSID_Higgsino_1p0;
    CMass = CMass_Higgsino_1p0;
    TauReweight = TauReweight_Higgsino_1p0;
  }else if(Type=="Higgsino" && Tau=="4p0"){
    vDSID = vDSID_Higgsino_4p0;
    CMass = CMass_Higgsino_4p0;
    TauReweight = TauReweight_Higgsino_4p0;
  }else if(Type=="Higgsino" && Tau=="10p0"){
    vDSID = vDSID_Higgsino_10p0;
    CMass = CMass_Higgsino_10p0;
    TauReweight = TauReweight_Higgsino_10p0;
  }else if(Type=="Strong" && Tau=="0p2"){
    vDSID = vDSID_Strong_0p2;
    CMass = CMass_Strong_0p2;
    GMass = GMass_Strong_0p2;
    TauReweight = TauReweight_Strong_0p2;
  }else if(Type=="Strong" && Tau=="1p0"){
    vDSID = vDSID_Strong_1p0;
    CMass = CMass_Strong_1p0;
    GMass = GMass_Strong_1p0;
    TauReweight = TauReweight_Strong_1p0;
  }
  nSignal = vDSID.size();
  nReweight = TauReweight.size();

  std::cout << "nSignal = " << nSignal << std::endl;
  std::cout << "nReweight = " << nReweight << std::endl;
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    for(int iSignal=0;iSignal<nSignal;iSignal++){
      std::vector<MyManager *> vMyDS;
      std::vector<HistManager *> vMyHist;
      std::vector<double> vSF;

      for(unsigned int iDSID=0;iDSID<(vDSID.at(iSignal).size());iDSID++){
	MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirSignal.c_str(), vDSID.at(iSignal).at(iDSID), MCName[iMC16].c_str()), Type);
	myDS->LoadMyFile(3);
	vMyDS.push_back(myDS);
	vMyHist.push_back(myDS->myHist_Old4L);

	if(Type=="Higgsino"){
	  double ProdTypeFracP = (vMyHist.at(iDSID)->hProdType->GetBinContent(3))/(vMyHist.at(iDSID)->hProdType->Integral());
	  double ProdTypeFracM = (vMyHist.at(iDSID)->hProdType->GetBinContent(4))/(vMyHist.at(iDSID)->hProdType->Integral());
	  if(Tau=="4p0"){
	    if(iDSID==0 || iDSID==1){
	      vSF.push_back(2.0*IntLumi[iMC16]/(vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	    }else{
	      vSF.push_back(IntLumi[iMC16]/(vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	    }
	  }else{
	    if(iSignal==2){
	      if(iDSID==0){
		vSF.push_back(IntLumi[iMC16]*2.0*0.290274907413/(gXS_EWK[0]->Eval(CMass_EWK_1p0[1])*ProdTypeFracP*vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }else if(iDSID==1){
		vSF.push_back(IntLumi[iMC16]*2.0*0.157326848316/(gXS_EWK[0]->Eval(CMass_EWK_1p0[1])*ProdTypeFracM*vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }else if(iDSID==2){
		vSF.push_back(IntLumi[iMC16]*0.250515174955/(gXS_EWK[1]->Eval(CMass_EWK_1p0[1])*vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }
	    }else if(iSignal==4){
	      if(iDSID==0){
		vSF.push_back(IntLumi[iMC16]*2.0*0.064956427812/(gXS_EWK[0]->Eval(CMass_EWK_1p0[2])*ProdTypeFracP*vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }else if(iDSID==1){
		vSF.push_back(IntLumi[iMC16]*2.0*0.0314521927711/(gXS_EWK[0]->Eval(CMass_EWK_1p0[2])*ProdTypeFracM*vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }else if(iDSID==2){
		vSF.push_back(IntLumi[iMC16]*0.0540408247663/(gXS_EWK[1]->Eval(CMass_EWK_1p0[2])*vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }
	    }else{
	      if(iDSID==0 || iDSID==1){
		vSF.push_back(2.0*IntLumi[iMC16]/(vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }else{
		vSF.push_back(IntLumi[iMC16]/(vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	      }
	    }
	  }
	}else{
	  vSF.push_back(IntLumi[iMC16]/(vMyHist.at(iDSID)->hSumOfWeightsBCK->GetBinContent(1)));
	}
      }// for iDSID
      mySignal[iMC16].push_back(vMyDS);
      myHist[iMC16].push_back(vMyHist);
      SF[iMC16].push_back(vSF);

      if(iMC16==0){
	for(int iMET=0;iMET<nMETRegion;iMET++){
	  TH1D *hTMP = new TH1D(Form("hSignalPt_%s_%s_iSig%d_iMET%d", Type.c_str(), Tau.c_str(), iSignal, iMET), ";track p_{T} [GeV];", nBinsMySignal, XBinsMySignal);
	  std::vector<TH1D *> hTMP_Reweight;
	  for(int iFrom=0;iFrom<nReweight;iFrom++){
	    TH1D *hReweightedPt = new TH1D(Form("hReweightedPt_%s_%s_iFrom%d_iSig%d_iMET%d", Type.c_str(), Tau.c_str(), iFrom, iSignal, iMET), ";track p_{T} [GeV];", nBinsMySignal, XBinsMySignal);
	    hTMP_Reweight.push_back(hReweightedPt);
	  }

	  hPt[iMET].push_back(hTMP);
	  hPt_Reweighted[iMET].push_back(hTMP_Reweight);
	}// for iMET
      }// if iMC16==0
    }// for iSignal
  }// for iMC16
}

MySignal::~MySignal(){

}
