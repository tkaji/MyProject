#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

bool doSanity=true;
TFile *tfSanity;
TH1D *hTFCorrection_Had;
TH1D *hTFCorrection_Ele;

TFile *tf;
NtupleReader *myReader=NULL;
TEventList *eventList=NULL;

TCanvas *c0;
TH1D *hTemplate_TrackPt=NULL;
TH1D *hTMP=NULL;
TH2D *hFrame;
std::string PDFName;

const int nCategory = 2;
TH1D *hTrackPt[nCategory];
TH2D *hCaloPt[nCategory];
TH2D *hPtEta[nCategory];
TH2D *hPtEta_CaloSide[nCategory];
TH2D *hPtEta_withCaloVETO[nCategory];
TH2D *hCalo2040[nCategory];
TH2D *hCaloClus[nCategory];
TH1D *hdREgamma[nCategory];
TH2D *hTruthJetTrackPt[nCategory];
TH2D *hTruthParticleTrackPt[nCategory];
TH2D *hTruthJetCalo[nCategory];
TH2D *hTruthParticleCalo[nCategory];

TH2D *hRatio_StdToTracklet;
TH2D *hRatio_CaloVETO_Std;
TH2D *hRatio_CaloVETO_Tracklet;
TH2D *hRatio_StdToTrackletWithCaloVETO;

TH1D *hUnfoldTruthPt[18];
TH1D *hTrackRes[18];
TH1D *hTrackRes_Rebin[18];
TH1D *hInvTrackRes_Rebin[18];
Double_t xVal_Mean[18] = {4.3741297, 5.2306419, 6.2548705, 7.47966, 8.94427, 10.6957, 12.79, 15.2945, 18.2894, 21.8706, 26.1532, 31.2744, 37.3983, 44.7214, 53.4784, 63.9502, 76.4724, 91.4468};
TGraphAsymmErrors *gTrackRes;

TF1 *fGaus;

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("grid", po::value<int>()->default_value(0), "camma separeted file list")
    ("output-file,o", po::value<std::string>()->default_value("mySinglePionStudy"), "output file")
    ("input-files,i", po::value<std::vector<std::string> >(), "input Files");
  
  po::positional_options_description p;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);
  std::vector<std::string> InputFileList = vm["input-files"].as<std::vector<std::string> >();
  std::vector<std::string> ReadableInputFileList;
  if(vm["grid"].as<int>()==0){
    ReadableInputFileList = InputFileList;
  }else{
    std::string CommaList = InputFileList.at(0);
    while(true){
      std::string::size_type pos = CommaList.find(',');
      if(pos != std::string::npos){
	ReadableInputFileList.push_back(CommaList.substr(0, pos));
	CommaList.erase(0, pos+1);
      }else{
	ReadableInputFileList.push_back(CommaList);
	break;
      }
    }
  }
  PDFName = vm["output-file"].as<std::string>();
  std::string OutputFileName = vm["output-file"].as<std::string>();

  if(doSanity){
    tfSanity = new TFile("~/hCorrEleHad.root", "READ");
    hTFCorrection_Had = (TH1D *)tfSanity->Get("hTFCorrectionHad_caloveto05");
    hTFCorrection_Ele = (TH1D *)tfSanity->Get("hTFCorrectionEle_caloveto05");
  }

  SetAtlasStyle();
  gStyle->SetErrorX(0.5);
  gStyle->SetTextSize(0.045);
  gStyle->SetPalette(1);
  gStyle->SetPaintTextFormat(".1lf");
  c0 = new TCanvas("c0", "c0", 800, 600);

  //fGaus = new TF1("fGaus", "gaus(0)", 0.0, 50.0);
  fGaus = new TF1("fGaus", "[0]*TMath::Gaus(x, [1], [2]) + [3]*TMath::Gaus(x, [1], [4])", 0.0, 50.0);
  fGaus->SetParLimits(1, 0.0, 5.0);
  fGaus->SetParLimits(2, 0.0, 100.0);
  fGaus->SetParLimits(4, 0.0, 100.0);
  fGaus->SetParameter(0, 80.0);
  fGaus->SetParameter(2, 5.0);
  fGaus->SetParameter(3, 20.0);
  fGaus->SetParameter(4, 50.0);

  myReader = new NtupleReader(ReadableInputFileList.at(0));
  std::string BaseCut = "(Tracks.p4.Pt()/1000.0 > 10.0 && Tracks.numberOfContribPixelLayers>=4 && Tracks.numberOfInnermostPixelLayerHits>=1 && Tracks.numberOfGangedFlaggedFakes==0 && Tracks.nSiHoles==0 && Tracks.numberOfPixelSpoiltHits==0 && Tracks.nPixelOutliers==0 && Tracks.ptcone40overPt_1gev<0.04 && Tracks.IsPassedIsolatedLeading==1 && Tracks.Quality > 0.1 && TMath::Abs(Tracks.p4.Eta()) > 0.1 && TMath::Abs(Tracks.p4.Eta()) < 1.9)";
  std::string CutForStd = Form("%s && (Tracks.nSCTHits>=7)", BaseCut.c_str());
  std::string CutForTracklet = Form("%s && (Tracks.nSCTHits==0)", BaseCut.c_str());
  std::string CutForStd_withCaloVETO = Form("%s && (Tracks.nSCTHits>=7 && Tracks.etclus20_topo/1000.0<5.0)", BaseCut.c_str());
  std::string CutForTracklet_withCaloVETO = Form("%s && (Tracks.nSCTHits==0 && Tracks.etclus20_topo/1000.0<5.0)", BaseCut.c_str());
  std::string CutForStd_withCaloSide = Form("%s && (Tracks.nSCTHits>=7 && Tracks.etclus20_topo/1000.0>5.0 && Tracks.etclus20_topo/1000.0<10.0) ", BaseCut.c_str());
  std::string CutForTracklet_withCaloSide = Form("%s && (Tracks.nSCTHits==0 && Tracks.etclus20_topo/1000.0>5.0 && Tracks.etclus20_topo/1000.0<10.0)", BaseCut.c_str());

  TPad *pad0 = new TPad("pad0", "pad0", 0.0, 0.0, 1.0, 1.0);
  pad0->SetLeftMargin(0.12);
  pad0->SetRightMargin(0.15);
  pad0->Draw();
  pad0->cd();

  for(int i=0;i<18;i++){
    hUnfoldTruthPt[i] = new TH1D(Form("hUnfoldTruthPt_%d", i), ";truth p_{T} [GeV];", 18, XBinsLogPt18ForLepton);
    hTrackRes[i] = new TH1D(Form("hTrackRes_%d", i), ";#Delta p_{T} [GeV]", 20000, -100, 100);
    hTrackRes_Rebin[i] = new TH1D(Form("hTrackRes_Rebin_%d", i), ";#Delta p_{T} [GeV]", 200, 0, 50);
    hInvTrackRes_Rebin[i] = new TH1D(Form("hInvTrackRes_Rebin_%d", i), ";#Delta 1/p_{T} [GeV^{-1}]", 100, -0.05, 0.05);
  }

  TH1D *hStd_TrackPt = new TH1D("hStd_TrackPt", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("Tracks.p4.Pt()/1000.0>>hStd_TrackPt", Form("%s", CutForStd.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hTracklet_TrackPt = new TH1D("hTracklet_TrackPt", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("Tracks.p4.Pt()/1000.0>>hTracklet_TrackPt", Form("%s", CutForTracklet.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hStd_TrackPt_withCaloVETO = new TH1D("hStd_TrackPt_withCaloVETO", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("Tracks.p4.Pt()/1000.0>>hStd_TrackPt_withCaloVETO", Form("%s", CutForStd_withCaloVETO.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hTracklet_TrackPt_withCaloVETO = new TH1D("hTracklet_TrackPt_withCaloVETO", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("Tracks.p4.Pt()/1000.0>>hTracklet_TrackPt_withCaloVETO", Form("%s", CutForTracklet_withCaloVETO.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hStd_TrackPt_withCaloSide = new TH1D("hStd_TrackPt_withCaloSide", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("Tracks.p4.Pt()/1000.0>>hStd_TrackPt_withCaloSide", Form("%s", CutForStd_withCaloSide.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hTracklet_TrackPt_withCaloSide = new TH1D("hTracklet_TrackPt_withCaloSide", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("Tracks.p4.Pt()/1000.0>>hTracklet_TrackPt_withCaloSide", Form("%s", CutForTracklet_withCaloSide.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TGraphAsymmErrors *gStd_CaloVETO_Eff = new TGraphAsymmErrors(hStd_TrackPt_withCaloVETO, hStd_TrackPt);
  gStd_CaloVETO_Eff->SetName("gStd_CaloVETO_Eff");
  TGraphAsymmErrors *gTracklet_CaloVETO_Eff = new TGraphAsymmErrors(hTracklet_TrackPt_withCaloVETO, hTracklet_TrackPt);
  gTracklet_CaloVETO_Eff->SetName("gTracklet_CaloVETO_Eff");
  TGraphAsymmErrors *gStd_CaloSide_Eff = new TGraphAsymmErrors(hStd_TrackPt_withCaloSide, hStd_TrackPt);
  gStd_CaloSide_Eff->SetName("gStd_CaloSide_Eff");
  TGraphAsymmErrors *gTracklet_CaloSide_Eff = new TGraphAsymmErrors(hTracklet_TrackPt_withCaloSide, hTracklet_TrackPt);
  gTracklet_CaloSide_Eff->SetName("gTracklet_CaloSide_Eff");

  
  TH1D *hStd_TruthPt = new TH1D("hStd_TruthPt", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("TruthJets[0].p4.Pt()/1000.0>>hStd_TruthPt", Form("%s", CutForStd.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hTracklet_TruthPt = new TH1D("hTracklet_TruthPt", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("TruthJets[0].p4.Pt()/1000.0>>hTracklet_TruthPt", Form("%s", CutForTracklet.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hStd_TruthPt_Sanity_Ele = new TH1D("hStd_TruthPt_Sanity_Ele", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  TH1D *hStd_TruthPt_Sanity_Had = new TH1D("hStd_TruthPt_Sanity_Had", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);

  TH1D *hStd_TruthPt_Sanity_All = new TH1D("hStd_TruthPt_Sanity_All", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);

  TH1D *hStd_TruthPt_withCaloVETO = new TH1D("hStd_TruthPt_withCaloVETO", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("TruthJets[0].p4.Pt()/1000.0>>hStd_TruthPt_withCaloVETO", Form("%s", CutForStd_withCaloVETO.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hTracklet_TruthPt_withCaloVETO = new TH1D("hTracklet_TruthPt_withCaloVETO", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("TruthJets[0].p4.Pt()/1000.0>>hTracklet_TruthPt_withCaloVETO", Form("%s", CutForTracklet_withCaloVETO.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hStd_TruthPt_withCaloSide = new TH1D("hStd_TruthPt_withCaloSide", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("TruthJets[0].p4.Pt()/1000.0>>hStd_TruthPt_withCaloSide", Form("%s", CutForStd_withCaloSide.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH1D *hTracklet_TruthPt_withCaloSide = new TH1D("hTracklet_TruthPt_withCaloSide", ";p_{T} [GeV]", 18, XBinsLogPt18ForLepton);
  myReader->m_tree->Draw("TruthJets[0].p4.Pt()/1000.0>>hTracklet_TruthPt_withCaloSide", Form("%s", CutForTracklet_withCaloSide.c_str()));
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TGraphAsymmErrors *gStd_Truth_CaloVETO_Eff = new TGraphAsymmErrors(hStd_TruthPt_withCaloVETO, hStd_TruthPt);
  gStd_Truth_CaloVETO_Eff->SetName("gStd_Truth_CaloVETO_Eff");
  TGraphAsymmErrors *gTracklet_Truth_CaloVETO_Eff = new TGraphAsymmErrors(hTracklet_TruthPt_withCaloVETO, hTracklet_TruthPt);
  gTracklet_Truth_CaloVETO_Eff->SetName("gTracklet_Truth_CaloVETO_Eff");
  TGraphAsymmErrors *gStd_Truth_CaloSide_Eff = new TGraphAsymmErrors(hStd_TruthPt_withCaloSide, hStd_TruthPt);
  gStd_Truth_CaloSide_Eff->SetName("gStd_Truth_CaloSide_Eff");
  TGraphAsymmErrors *gTracklet_Truth_CaloSide_Eff = new TGraphAsymmErrors(hTracklet_TruthPt_withCaloSide, hTracklet_TruthPt);
  gTracklet_Truth_CaloSide_Eff->SetName("gTracklet_Truth_CaloSide_Eff");

  /*
    TH2D *hStd_CaloPt = new TH2D("hStd_CaloPt", ";p_{T} [GeV];associated calo cluster [GeV]", 40, 0.0, 100.0, 40, 0.0, 100.0);
  myReader->m_tree->Draw("Tracks.etclus20_topo/1000.0:Tracks.p4.Pt()/1000.0>>hStd_CaloPt", Form("%s", CutForStd.c_str()), "colz");
  hStd_CaloPt->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hTracklet_CaloPt = new TH2D("hTracklet_CaloPt", ";p_{T} [GeV];associated calo cluster [GeV]", 40, 0.0, 100.0, 40, 0.0, 100.0);
  myReader->m_tree->Draw("Tracks.etclus20_topo/1000.0:Tracks.p4.Pt()/1000.0>>hTracklet_CaloPt", Form("%s", CutForTracklet.c_str()), "colz");
  hTracklet_CaloPt->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hStd_PtEta              = new TH2D("hStd_PtEta"    , ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  pad0->SetLogx();
  myReader->m_tree->Draw("Tracks.p4.Eta():Tracks.p4.Pt()/1000.0>>hStd_PtEta", Form("%s", CutForStd.c_str()), "colz");
  hStd_PtEta->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hStd_PtEta_withCaloVETO = new TH2D("hStd_PtEta_withCaloVETO", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  myReader->m_tree->Draw("Tracks.p4.Eta():Tracks.p4.Pt()/1000.0>>hStd_PtEta_withCaloVETO", Form("%s", CutForStd_withCaloVETO.c_str()), "colz");
  hStd_PtEta_withCaloVETO->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hTracklet_PtEta              = new TH2D("hTracklet_PtEta"    , ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  myReader->m_tree->Draw("Tracks.p4.Eta():Tracks.p4.Pt()/1000.0>>hTracklet_PtEta", Form("%s", CutForTracklet.c_str()), "colz");
  hTracklet_PtEta->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  TH2D *hTracklet_PtEta_withCaloVETO = new TH2D("hTracklet_PtEta_withCaloVETO", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  myReader->m_tree->Draw("Tracks.p4.Eta():Tracks.p4.Pt()/1000.0>>hTracklet_PtEta_withCaloVETO", Form("%s", CutForTracklet_withCaloVETO.c_str()), "colz");
  hTracklet_PtEta_withCaloVETO->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  */
  for(int iCategory=0;iCategory<nCategory;iCategory++){
    hTrackPt[iCategory] = new TH1D(Form("hTrackPt_%d", iCategory), ";p_{T} [GeV]", 100, 0.0, 500.0);
    hCaloPt[iCategory]  = new TH2D(Form("hCaloPt_%d", iCategory) , ";p_{T} [GeV];associated calo cluster [GeV]", 40, 0.0, 100.0, 40, 0.0, 100.0);
    hCalo2040[iCategory]  = new TH2D(Form("hCalo2040_%d", iCategory) , ";associated calo cluster (#DeltaR < 0.2) [GeV];associated calo cluster (#DeltaR < 0.4) [GeV]", 40, 0.0, 100.0, 40, 0.0, 100.0);
    hPtEta[iCategory]   = new TH2D(Form("hPtEta_%d", iCategory)  , ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hPtEta_CaloSide[iCategory] = new TH2D(Form("hPtEta_CaloSide_%d", iCategory), ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hPtEta_withCaloVETO[iCategory] = new TH2D(Form("hPtEta_withCaloVETO_%d", iCategory), ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
    hCaloClus[iCategory]  = new TH2D(Form("hCaloClus_%d", iCategory) , ";associated calo cluster [GeV];leading calo cluster [GeV]", 40, 0.0, 100.0, 40, 0.0, 100.0);
    
    hdREgamma[iCategory] = new TH1D(Form("hdREgamma_%d", iCategory), ";#DeltaR(track, clus)", 100, 0.0, 1.0); 

    hTruthJetTrackPt[iCategory]  = new TH2D(Form("hTruthJetTrackPt_%d", iCategory) , ";truth jet p_{T} [GeV];track p_{T} [GeV]", 120, 0.0, 600.0, 120, 0.0, 600.0);
    hTruthParticleTrackPt[iCategory]  = new TH2D(Form("hTruthParticleTrackPt_%d", iCategory) , ";truth jet p_{T} [GeV];track p_{T} [GeV]", 120, 0.0, 600.0, 120, 0.0, 600.0);
    hTruthJetCalo[iCategory]  = new TH2D(Form("hTruthJetCalo_%d", iCategory) , ";truth jet p_{T} [GeV];associated calo cluster [GeV]", 120, 0.0, 600.0, 120, 0.0, 600.0);
    hTruthParticleCalo[iCategory]  = new TH2D(Form("hTruthParticleCalo_%d", iCategory) , ";truth jet p_{T} [GeV];associated calo cluster [GeV]", 120, 0.0, 600.0, 120, 0.0, 600.0);
  }// for iCategory

  hRatio_StdToTracklet  = new TH2D("hRatio_StdToTracklet", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hRatio_CaloVETO_Std   = new TH2D("hRatio_CaloVETO_Std" , ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hRatio_CaloVETO_Tracklet = new TH2D("hRatio_CaloVETO_Tracklet", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);
  hRatio_StdToTrackletWithCaloVETO = new TH2D("hRatio_StdToTrackletWithCaloVETO", ";p_{T} [GeV];#eta", nLogPtForLepton, XBinsLogPtForLepton, 25, -2.5, 2.5);

  int nEntry = myReader->nEntry;
  for(int iEntry=0;iEntry<nEntry;iEntry++){
    if((iEntry+1)%100000==0 ){
      std::cout << "iEntry = " << (iEntry+1) << std::endl;
    }
    myReader->GetEntry(iEntry);

    for(unsigned int iTrack=0;iTrack<(myReader->conventionalTracks->size());iTrack++){
      int TrackQuality = MyUtil::CalcOld4LTrackQuality(myReader->conventionalTracks->at(iTrack)); // 011 0000 1100 1111 1100
      double TrackPt  = myReader->conventionalTracks->at(iTrack)->p4.Pt()/1000.0;
      double TrackEta = myReader->conventionalTracks->at(iTrack)->p4.Eta();
      double calo20 = myReader->conventionalTracks->at(iTrack)->etclus20_topo/1000.0;
      double calo40 = myReader->conventionalTracks->at(iTrack)->etclus40_topo/1000.0;
      
      //if(MyUtil::IsPassed(TrackQuality, 0x3ECFC) && (TrackPt > 10.0)){
      if(MyUtil::IsPassed(TrackQuality, 0x30CFC) && (TrackPt > 10.0)){
	int Category = -1;
	if(myReader->conventionalTracks->at(iTrack)->nSCTHits>=7){
	  Category = 0;
	}else if(myReader->conventionalTracks->at(iTrack)->nSCTHits==0){
	  Category = 1;
	}else{
	  continue;
	}

	hTrackPt[Category]->Fill(TrackPt);
	hCaloPt[Category]->Fill(TrackPt, calo20);
	hCalo2040[Category]->Fill(calo20, calo40);

	// tracklet over stdtrack
	if(myReader->egammaClusters->size() > 0){
	  hdREgamma[Category]->Fill(myReader->conventionalTracks->at(iTrack)->p4.DeltaR(myReader->egammaClusters->at(0)->p4));
	  hCaloClus[Category]->Fill(calo20, myReader->egammaClusters->at(0)->p4.Pt()/1000.0);
	} // egamma

	// truth jets
	if(myReader->truthJets->size() > 0){
	  hTruthJetTrackPt[Category]->Fill(myReader->truthJets->at(0)->p4.Pt()/1000.0, TrackPt);
	  hTruthJetCalo[Category]->Fill(myReader->truthJets->at(0)->p4.Pt()/1000.0, calo20);

	  if(Category==0 && doSanity){
	    double TruthPt = myReader->truthJets->at(0)->p4.Pt()/1000.0;
	    hStd_TruthPt_Sanity_All->Fill(TruthPt);
	    if(calo20<5.0){
	      hStd_TruthPt_Sanity_Ele->Fill(TruthPt, hTFCorrection_Ele->GetBinContent(hTFCorrection_Ele->FindBin(TrackPt)));
	      hStd_TruthPt_Sanity_Had->Fill(TruthPt, hTFCorrection_Had->GetBinContent(hTFCorrection_Had->FindBin(TrackPt)));
	    }
	  }
	  
	  if(Category==0){
	    double TruthPt = myReader->truthJets->at(0)->p4.Pt()/1000.0;
	    for(int i=0;i<18;i++){	    
	      if(TruthPt > XBinsLogPt18ForLepton[i] && TruthPt < XBinsLogPt18ForLepton[i+1]){
		hTrackRes[i]->Fill(TruthPt - TrackPt);
		hTrackRes_Rebin[i]->Fill(TruthPt - TrackPt);
		hInvTrackRes_Rebin[i]->Fill(1.0/TruthPt - 1.0/TrackPt);
	      }
	      
	      if(TrackPt > XBinsLogPt18ForLepton[i] && TrackPt < XBinsLogPt18ForLepton[i+1]){
		hUnfoldTruthPt[i]->Fill(TruthPt);
	      }
	    }
	  }// category==0
	}// truth jet

	// truth jets
	if(myReader->truthParticles->size() > 0){
	  for(int i=0;i<(myReader->truthParticles->size());i++){
	    if(TMath::Abs(myReader->truthParticles->at(i)->PdgId)==211 && TMath::Abs(myReader->truthParticles->at(i)->p4.DeltaR(myReader->conventionalTracks->at(iTrack)->p4)) < 0.05){
	      hTruthParticleTrackPt[Category]->Fill(myReader->truthParticles->at(0)->p4.Pt()/1000.0, TrackPt);
	      hTruthParticleCalo[Category]->Fill(myReader->truthParticles->at(0)->p4.Pt()/1000.0, calo20);
	    }
	  }
	}// truth jet
	
	if(calo20 < 5.0){
	  hPtEta_withCaloVETO[Category]->Fill(TrackPt, TrackEta);
	}else if(calo20 < 10.0){
	  hPtEta_CaloSide[Category]->Fill(TrackPt, TrackEta);
	}else{
	  hPtEta[Category]->Fill(TrackPt, TrackEta);
	}
      }
    }// for iTrack
  }// for iEntry

  gTrackRes = new TGraphAsymmErrors();
  gTrackRes->SetMarkerStyle(8);
  gTrackRes->SetMarkerSize(1.0);

  c0->Print(Form("%s.pdf[", PDFName.c_str()), "pdf");

  for(int iCategory=0;iCategory<nCategory;iCategory++){
    hTrackPt[iCategory]->Draw();    
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hCaloPt[iCategory]->Draw("colz");
    hCaloPt[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hCalo2040[iCategory]->Draw("colz");
    hCalo2040[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hCaloClus[iCategory]->Draw("colz");
    hCaloClus[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    pad0->SetLogy(true);
    hdREgamma[iCategory]->Draw();
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    pad0->SetLogy(false);

    pad0->SetLogx(true);
    hPtEta[iCategory]->Draw("colz");
    hPtEta[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hPtEta_CaloSide[iCategory]->Draw("colz");
    hPtEta_CaloSide[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hPtEta_withCaloVETO[iCategory]->Draw("colz");
    hPtEta_withCaloVETO[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    pad0->SetLogx(false);

    hTruthJetTrackPt[iCategory]->Draw("colz");
    hTruthJetTrackPt[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hTruthJetCalo[iCategory]->Draw("colz");
    hTruthJetCalo[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    
    hTruthParticleTrackPt[iCategory]->Draw("colz");
    hTruthParticleTrackPt[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    hTruthParticleCalo[iCategory]->Draw("colz");
    hTruthParticleCalo[iCategory]->GetYaxis()->SetTitleOffset(1.0);
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }// for iCategory

  for(int i=0;i<nLogPtForLepton;i++){
    for(int j=0;j<25;j++){
      double vStd                   = hPtEta[0]->GetBinContent(i+1, j+1);
      double vStd_withCaloVETO      = hPtEta_withCaloVETO[0]->GetBinContent(i+1, j+1);
      double vTracklet              = hPtEta[1]->GetBinContent(i+1, j+1);
      double vTracklet_withCaloVETO = hPtEta_withCaloVETO[1]->GetBinContent(i+1, j+1);

      if(vStd>0){
	hRatio_StdToTracklet->SetBinContent(i+1, j+1, vTracklet/vStd);
	hRatio_CaloVETO_Std->SetBinContent(i+1, j+1, vStd_withCaloVETO/vStd);
	hRatio_StdToTrackletWithCaloVETO->SetBinContent(i+1, j+1, vTracklet_withCaloVETO/vStd);
      }
      if(vTracklet>0){
	hRatio_CaloVETO_Tracklet->SetBinContent(i+1, j+1, vTracklet_withCaloVETO/vTracklet);
      }
    }// for j
  }// for i

  pad0->SetLogx(true);
  pad0->SetLogz(true);
  hRatio_StdToTracklet->Draw("colz");
  hRatio_StdToTracklet->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hRatio_CaloVETO_Std->Draw("colz");
  hRatio_CaloVETO_Std->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hRatio_CaloVETO_Tracklet->Draw("colz");
  hRatio_CaloVETO_Tracklet->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  hRatio_StdToTrackletWithCaloVETO->Draw("colz");
  hRatio_StdToTrackletWithCaloVETO->GetYaxis()->SetTitleOffset(1.0);
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

  pad0->SetLogy(false);
  pad0->SetLogx(false);
  for(int i=5;i<18;i++){
    hTrackRes[i]->Draw();
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");

    //fGaus->SetParameter(0, 80.0);
    //fGaus->SetParameter(2, 5.0);
    //fGaus->SetParameter(3, 20.0);
    //fGaus->SetParameter(4, 50.0);
    hTrackRes_Rebin[i]->Fit("fGaus");
    hTrackRes_Rebin[i]->Fit("fGaus");
    hTrackRes_Rebin[i]->Fit("fGaus");
    //hTrackRes_Rebin[i]->Fit("fGaus", "", "", 0.0, 3.0*fGaus->GetParameter(2));
    //hTrackRes_Rebin[i]->Fit("fGaus", "", "", 0.0, 3.0*fGaus->GetParameter(2));
    hTrackRes_Rebin[i]->Draw();

    gTrackRes->SetPoint(i, xVal_Mean[i], fGaus->GetParameter(2));
    if(i==17){
      gTrackRes->SetPointEXlow(i, xVal_Mean[i] - 100.0);
      gTrackRes->SetPointEXhigh(i, 100.0 - xVal_Mean[i]);
    }else{
      gTrackRes->SetPointEXlow(i, xVal_Mean[i] - XBinsLogPt18ForLepton[i]);
      gTrackRes->SetPointEXhigh(i, XBinsLogPt18ForLepton[i+1] - xVal_Mean[i]);
    }
    gTrackRes->SetPointEYlow(i, fGaus->GetParError(2));
    gTrackRes->SetPointEYhigh(i, fGaus->GetParError(2));

    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
    hInvTrackRes_Rebin[i]->Draw();
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }
  pad0->SetLogy(false);
  pad0->SetLogx(true);
  hFrame = new TH2D("hFrame", ";truth p_{T} [GeV];#sigma #Delta p_{T} [GeV]", 50, 10.0, 100.0, 50, 0.0, 100.0);
  hFrame->Draw();
  gTrackRes->Draw("samePL");
  c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  
  delete hFrame; hFrame=NULL;
  hFrame = new TH2D("hFrame", ";truth p_{T} [GeV];a.u.", 50, 10.0, 100.0, 50, 0.0, 1.0);

  for(int i=5;i<18;i++){
    hFrame->Draw();
    hUnfoldTruthPt[i]->Scale(1.0/hUnfoldTruthPt[i]->Integral());
    hUnfoldTruthPt[i]->Draw("same");
    c0->Print(Form("%s.pdf", PDFName.c_str()), "pdf");
  }

  c0->Print(Form("%s.pdf]", PDFName.c_str()), "pdf");

  TGraphAsymmErrors *gStd_Truth_Sanity_Ele;
  TGraphAsymmErrors *gStd_Truth_Sanity_Had;
  for(int i=0;i<5;i++){
    hStd_TruthPt_Sanity_All->SetBinContent(i+1, 1);
    hStd_TruthPt_Sanity_Ele->SetBinContent(i+1, 1);
    hStd_TruthPt_Sanity_Had->SetBinContent(i+1, 1);
  }
  if(doSanity){
    gStd_Truth_Sanity_Ele = new TGraphAsymmErrors(hStd_TruthPt_Sanity_Ele, hStd_TruthPt_Sanity_All);
    gStd_Truth_Sanity_Ele->SetName("gStd_Truth_Sanity_Ele");
    gStd_Truth_Sanity_Had = new TGraphAsymmErrors(hStd_TruthPt_Sanity_Had, hStd_TruthPt_Sanity_All);
    gStd_Truth_Sanity_Had->SetName("gStd_Truth_Sanity_Had");
  }

  //tf = new TFile("mySinglePionStudy.root", "RECREATE");
  tf = new TFile(Form("%s.root", PDFName.c_str()), "RECREATE");
  for(int iCategory=0;iCategory<nCategory;iCategory++){
    hTrackPt[iCategory]->Write();
    hCaloPt[iCategory]->Write();
    hCalo2040[iCategory]->Write();
    hCaloClus[iCategory]->Write();
    hdREgamma[iCategory]->Write();
    hPtEta[iCategory]->Write();
    hPtEta_withCaloVETO[iCategory]->Write();
    hTruthJetTrackPt[iCategory]->Write();
    hTruthJetCalo[iCategory]->Write();
    hTruthParticleTrackPt[iCategory]->Write();
    hTruthParticleCalo[iCategory]->Write();
  }
  hRatio_StdToTracklet->Write();
  hRatio_CaloVETO_Std->Write();
  hRatio_CaloVETO_Tracklet->Write();
  hRatio_StdToTrackletWithCaloVETO->Write();

  hStd_TrackPt->Write();
  hTracklet_TrackPt->Write();
  hStd_TrackPt_withCaloVETO->Write();
  hTracklet_TrackPt_withCaloVETO->Write();
  gStd_CaloVETO_Eff->Write();
  gTracklet_CaloVETO_Eff->Write();
  gStd_CaloSide_Eff->Write();
  gTracklet_CaloSide_Eff->Write();
  
  hStd_TruthPt->Write();
  hTracklet_TruthPt->Write();
  hStd_TruthPt_withCaloVETO->Write();
  hTracklet_TruthPt_withCaloVETO->Write();
  gStd_Truth_CaloVETO_Eff->Write();
  gTracklet_Truth_CaloVETO_Eff->Write();
  gStd_Truth_CaloSide_Eff->Write();
  gTracklet_Truth_CaloSide_Eff->Write();

  for(int i=0;i<18;i++){
    hTrackRes_Rebin[i]->Write();
  }
  for(int i=0;i<18;i++){
    hUnfoldTruthPt[i]->Write();
  }  
  hStd_TruthPt_Sanity_All->Write();
  hStd_TruthPt_Sanity_Had->Write();
  hStd_TruthPt_Sanity_Ele->Write();
  gStd_Truth_Sanity_Ele->Write();
  gStd_Truth_Sanity_Had->Write();
  tf->Close();

  return 0;
}
