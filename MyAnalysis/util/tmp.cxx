#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "src/PhysicsObjectProxyBase.h"
#include "src/MyManager.h"
#include "src/HistManager.h"
#include "src/NtupleReader.h"
#include "src/Constant.h"
#include "src/Utility.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TF1.h"
#include "TFile.h"
#include "THStack.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "../atlasrootstyle/AtlasStyle.C"
#include "../atlasrootstyle/AtlasLabels.C"
#include "../atlasrootstyle/AtlasUtils.C"
#include <boost/program_options.hpp>
namespace po = boost::program_options;

TCanvas *c0;
std::string PDFName;
TH1D *hTrackPt;

const int nBinsMySignal = 12;
const double XBinsMySignal[nBinsMySignal+1] = {20, 26.1532, 34.1995, 44.7214, 60.0, 76.4724, 100, 170.998, 292.402, 500, 1118.03, 2500, 12500};
const std::string DirName="/gpfs/fs6001/toshiaki/Common_nominal";
const std::string MCName[3]={"mc16a", "mc16d", "mc16e"};
std::vector<int> vDSID_EWK    = {448304, 448305};
std::vector<int> vDSID_Strong = {448360};

TFile *tfMyInput;
TFile *tfOutput;
TH2D *hTrigMETJetEff[nMETTrig];
TGraphAsymmErrors *gSF_DeadModule[nMC16];
const int nMC16 = 3;
TF1 *fSmearInt_ele[nSmearRegion];
TF1 *fSmearInt_mu[nSmearRegion];
const int nSmearRegion = 8;
const double g_par_Mean_ele[nSmearRegion]  = {-0.203310, -0.203310, -0.203310, -0.203310, -0.146075, -0.126751, -0.205570, -0.213854};
const double g_par_Sigma_ele[nSmearRegion] = {20.944675, 19.536572, 18.330746, 17.005663, 15.424842, 14.490877, 13.900709, 14.030027};
const double g_par_Alpha_ele[nSmearRegion] = { 1.858616,  1.858616,  1.858616,  1.858616,  1.822388,  1.655727,  1.544172,  1.637405};
const double g_par_Mean_mu[nSmearRegion]  = {-0.248458, -0.248458, -0.248458, -0.248458, -0.188993, -0.133858, -0.280374, 0.001933};
const double g_par_Sigma_mu[nSmearRegion] = {16.957926, 15.542328, 14.912544, 14.836049, 14.212644, 13.638716, 13.440928, 13.208069};
const double g_par_Alpha_mu[nSmearRegion] = { 1.716832,  1.716832,  1.716832,  1.716832,  1.663774,  1.618689,  1.682396,  1.644168};
double crystallBallIntegral(double* x, double *par);
double GetTrigEffVal(UInt_t v_runNum, double v_METPt, double v_JetPt);
void SmearPt(TH1D *hHoge, double pt, double weight, bool fMuonFlag);
double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau);

NtupleReader *mySignal;

const int nCut=14;
bool fReweight=false;
double TargetLifetime;
int iChain;
std::vector<int>vDSID;
std::vector<MyManager *> vMyDS;
std::vector<HistManager *> vMyHist;

double nSignal_SR[3];
double nSignal_CaloSide[3];
double nSignal_FakeCR[3];
double nSignal_HadCR[3];
double nSignal_EleCR[3];
double nSignal_MuCR[3];

int main(int argc, char **argv){
  po::options_description desc("option description");
  desc.add_options()
    ("help,h", "print help")
    ("iChain", po::value<int>()->default_value(1), "1: EWK, 3: Strong")
    ("fReweight", po::value<bool>()->default_value(false), "false : no reweight")
    ("output-file,o", po::value<std::string>()->default_value("SignalContami"), "output file")
    ("input-files,i", po::value<std::vector<int>>(), "input Files");
  
  po::positional_options_description p;
  po::variables_map vm;
  p.add("input-files", -1); // this line enable you to don't need to specify --input-files option
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  fReweight = vm["fReweight"].as<bool>();
  iChain         = vm["iChain"].as<int>();
  vDSID = vm["input-files"].as<std::vector<int> >();

  std::string OutputFileName = vm["output-file"].as<std::string>();
  hTrackPt = new TH1D("hTrackPt", ";track p_{T} [GeV]", nBinsMySignal, XBinsMySignal);


  tfInput = new TFile("/gpfs/fs6001/toshiaki/Fitting/disappearingtracksearch_shapefitter/data/Input.root", "READ");
  gROOT->cd();
  for(int iTrig=0;iTrig<nMETTrig;iTrig++){
    hTrigMETJetEff[iTrig] = (TH2D *)tfInput->Get(Form("METTrigEff/METTrigEff_Chain%d_%d", iChain, iTrig));
  }// for iTrig
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    gSF_DeadModule[iMC16] = (TGraphAsymmErrors *)tfInput->Get(Form("SF_DeadModule_%s", MCName[iMC16].c_str()));
  }
  for(int i=0;i<nSmearRegion;i++){
    fSmearInt_ele[i] = new TF1(Form("fSmearInt_ele_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_ele[i]->SetParameter(0, 0.0);
    fSmearInt_ele[i]->SetParameter(1, g_par_Sigma_ele[i]);
    fSmearInt_ele[i]->SetParameter(2, g_par_Alpha_ele[i]);    

    fSmearInt_mu[i] = new TF1(Form("fSmearInt_mu_%d", i), [&](double*x, double *p){ return crystallBallIntegral(x,p); }, -1000, 1000, 3);
    fSmearInt_mu[i]->SetParameter(0, 0.0);
    fSmearInt_mu[i]->SetParameter(1, g_par_Sigma_mu[i]);
    fSmearInt_mu[i]->SetParameter(2, g_par_Alpha_mu[i]);    
  }// for i

  std::cout << "nFile : " << nFile << std::endl;
  
  NtupleReader *mySignal;
  for(int iMC16=0;iMC16<nMC16;iMC16++){
    for(unsigned int iDSID=0;iDSID<vDSID.size();iDSID++){
      MyManager *myDS = new MyManager(Form("%s/out.%d_%s.myAna.root", DirName.c_str(), vDSID->at(iDSID), MCName[iMC16].c_str()), "Signal");
      myDS->LoadMyFile(3);
      vMyDS.push_back(myDS);
      vMyHist.push_back(myDS->myHist_Old4L);

      double vSF = IntLumi[iMC16]/myDS->myHist_Old4L->hSumofWeightsBCK->GetBinContent(1);

      // SR 
      mySignal = myDS->GetNtupleReader("Old4L_Common_SR");
      for(int iEntry=0;iEntry<(mySignal->nEntry);iEntry++){
	mySignal->GetEntry(iEntry);

	double weight = vSF*(mySignal->weightXsec)*(mySignal->weightMCweight)*(mySignal->weightPileupReweighting);

	int iMETReg = -1;
	bool fKinematics=false;
	double MET    = mySignal->missingET->p4.Pt()/1000.0;
	//double MET_ForMuCR     = mySignal->missingET_Muon->p4.Pt()/1000.0;
	//double MET_ForEleCR    = mySignal->missingET_Electron->p4.Pt()/1000.0;
	double JetPt0 = (mySignal->goodJets->size()>0) ? mySignal->goodJets->at(0)->p4.Pt()/1000.0 : 0.0;
	double JetPt1 = (mySignal->goodJets->size()>1) ? mySignal->goodJets->at(1)->p4.Pt()/1000.0 : 0.0;
	double JetPt2 = (mySignal->goodJets->size()>2) ? mySignal->goodJets->at(2)->p4.Pt()/1000.0 : 0.0;
	double dPhi   = mySignal->JetMetdPhiMin50;
	double TrigVal = GetTrigEffVal(mySignal->randomRunNumber, MET, JetPt0);
	//double TrigVal_ForMuCR  = GetTrigEffVal(mySignal->randomRunNumber, MET_ForMuCR, JetPt0);
	//double TrigVal_ForEleCR = GetTrigEffVal(mySignal->randomRunNumber, MET_ForMuCR, JetPt0);
	weight *= TrigVal;
	if(mySignal->IsPassedBadEventVeto==false)
	  continue;

	if(mySignal->IsPassedLeptonVeto==false)
	  continue;

	if(MET > KinematicsChain[iChain][0]){
	  iMETReg = 0;
	}else if(MET < 200.0 && MET > 150.0){
	  iMETReg = 1;
	}else if(MET < 150.0 && MET > 100.0){
	  iMETReg = 2;
	}else{
	  continue;
	}
	if(JetPt0 < KinematicsChain[iChain][1])
	  continue;
	if(JetPt1 < KinematicsChain[iChain][2])
	  continue;
	if(JetPt2 < KinematicsChain[iChain][3])
	  continue;
	if(dPhi < KinematicsChain[iChain][5])
	  continue;

	if(TMath::Abs(mySignal->Tracks->at(iTrack)->d0sigTool) > 1.5)
	  continue;

	bool fTracklet  = false; // 8
	bool fQuality   = false; // 9
	bool fIsolation = false; // 10
	bool fGeometry  = false; // 11
	bool fCaloveto  = false; // 12
	bool fPt        = false; // 13

	double wTracklet  = 0.0; // 8
	double wQuality   = 0.0; // 9
	double wIsolation = 0.0; // 10
	double wGeometry  = 0.0; // 11
	double wCaloveto  = 0.0; // 12
	double wPt        = 0.0; // 13
      
	int IsoTrackID = -1.0;
	int IsoTrackPt =  0.0;
	int IsoTrackletID = -1.0;
	int IsoTrackletPt =  0.0;
	
	// Lifetime Weight 
	double LifetimeWeight = 1.0;
	if(fReweight){
	  for(unsigned int iTruth=0;iTruth<(mySignal->truthParticles->size());iTruth++){
	    int PDGID = (int)mySignal->truthParticles->at(iTruth)->PdgId;
	    if(TMath::Abs(PDGID)!=1000024)
	      continue;
	    float ProperTime = mySignal->truthParticles->at(iTruth)->ProperTime;
	    LifetimeWeight = GetWeightLifetime(ProperTime, 1.0, 0.2);
	  }// for iTruth
	}// reweight
	weight *= LifetimeWeight;

	    
	double trackEta = mySignal->conventionalTracks->at(iTrack)->p4.Eta();
	double DeadWeight = gSF_DeadModule[iMC16]->Eval(trackEta);
	double truthPt = mySignal->conventionalTracks->at(iTrack)->TruthPt/1000.0;
	double weightPt20=0.0;
	double weightPt60=0.0;
	hTrackPt->Reset();
	if(truthPt > 0.0){
	  SmearPt(hTrackPt, truthPt, 1.0, false);
	  weightPt20 = hTrackPt->Integral(1, 12);
	  weightPt60 = hTrackPt->Integral(5, 12);
	}else{
	  weightPt20 = 0.0;
	  weightPt60 = 0.0;
	}
	fTracklet=true;
	wTracklet += DeadWeight*weightPt20;

	if((TMath::Abs(mySignal->conventionalTracks->at(iTrack)->d0sigTool)       < 1.5) &&
	   (TMath::Abs(mySignal->conventionalTracks->at(iTrack)->z0sinthetawrtPV) < 0.5) &&
	   (mySignal->conventionalTracks->at(iTrack)->Quality > 0.1)){
	  fQuality=true;
	  wQuality += DeadWeight*weightPt20;
	    
	      if((mySignal->conventionalTracks->at(iTrack)->ptcone40overPt_1gev < 0.04) &&
		 (mySignal->conventionalTracks->at(iTrack)->IsPassedIsolatedLeading==true) &&
		 (mySignal->conventionalTracks->at(iTrack)->dRJet50 > 0.4) &&
		 (mySignal->conventionalTracks->at(iTrack)->dRElectron > 0.4) &&
		 (mySignal->conventionalTracks->at(iTrack)->dRMuon > 0.4) &&
		 (mySignal->conventionalTracks->at(iTrack)->dRMSTrack > 0.4)){
		fIsolation=true;
		wIsolation += DeadWeight*weightPt20;
	      
		if((TMath::Abs(mySignal->conventionalTracks->at(iTrack)->p4.Eta()) > 0.1) &&
		   (TMath::Abs(mySignal->conventionalTracks->at(iTrack)->p4.Eta()) < 1.9)){
		  fGeometry=true;
		  wGeometry += DeadWeight*weightPt20;
		
		  if(mySignal->conventionalTracks->at(iTrack)->etclus20_topo/1000.0 < 5.0){
		    fCaloveto=true;
		    wCaloveto += DeadWeight*weightPt20;
		  
		    fPt=true;
		    wPt += DeadWeight*weightPt60;
		  }// Caloveto
		}// Geometry
	      }// Isolation
	    }// Quality	  
	  }// fTracklet	
	}// for iTrack
      
	if(fPt)        hCutflow->Fill(13, weight*wPt);
      }// for iEntry
      delete mySignal; mySignal=NULL;
    }// for iDSID
  }//for iMC16

  tfOutput = new TFile(OutputFileName.c_str(), "RECREATE");
  hCutflow->Write("", TObject::kOverwrite);

  return 0;
}

double crystallBallIntegral(double* x, double *par){
    double constant = 1;//par[0];
    double mean = par[0];
    double sigma = par[1];
    double alpha = par[2];//*sigma;

    // evaluate the crystal ball function
    if (sigma < 0.)     return 0.;
    if (alpha < 0.)     return 0.;
    double z = (x[0] - mean)/sigma;
    alpha = std::abs(alpha);
    double norm1 = sigma*sqrt(2*M_PI)*erf(alpha/sqrt(2));
    double norm2 = sigma*exp(-alpha*alpha/2)/alpha;
    double norm3 = norm2;
    constant /= (norm1 + norm2 + norm3);
    if (z  < - alpha){
        return constant * (+1) * sigma / alpha * std::exp( alpha * (z + 0.5 * alpha));
    }else if (z  > + alpha){
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        double add1 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(- alpha / sqrt(2));
        double sub1 = constant * (-1) * sigma / alpha * std::exp(-alpha * (alpha - 0.5 * alpha));
        return constant * (-1) * sigma / alpha * std::exp(-alpha * (z - 0.5 * alpha)) + add0 + add1 - sub0 - sub1;
    }else{
        double add0 = constant * (+1) * sigma / alpha * std::exp( alpha * (- alpha + 0.5 * alpha));
        double sub0 = constant * (-1) * sqrt(M_PI_2) * sigma * erf(alpha / sqrt(2));
        return constant * (-1) * sqrt(M_PI_2) * sigma * erf(- z / sqrt(2)) + add0 - sub0;
    }
}

double GetTrigEffVal(UInt_t v_runNum, double v_METPt, double v_JetPt){
  int iTrig = MyUtil::GetUnprescaledMETTrigger(v_runNum);
  int iX = hTrigMETJetEff[iChain][iTrig]->GetXaxis()->FindBin(v_METPt);
  int iY = hTrigMETJetEff[iChain][iTrig]->GetXaxis()->FindBin(v_JetPt);
  if(iX==0) iX=1;
  if(iY==0) iY=1;
  if(iX==(hTrigMETJetEff[iChain][iTrig]->GetNbinsX()+1)) iX=hTrigMETJetEff[iChain][iTrig]->GetNbinsX();
  if(iY==(hTrigMETJetEff[iChain][iTrig]->GetNbinsY()+1)) iY=hTrigMETJetEff[iChain][iTrig]->GetNbinsY();
  
  return hTrigMETJetEff[iChain][iTrig]->GetBinContent(iX, iY);
}


void SmearPt(TH1D *hHoge, double pt, double weight, bool fMuonFlag){
 for(int i=0;i<nBinsMySignal;i++){
    double ptLow = XBinsMySignal[i];
    double ptUp  = XBinsMySignal[i+1];
    if(i==(nBinsMySignal-1)){
      ptUp = 12500.0;
    }

    int iFunc=-1;
    if(pt < 15.0){
      iFunc = 0;
    }else if(pt < 20.0){
      iFunc = 1;
    }else if(pt < 25.0){
      iFunc = 2;
    }else if(pt < 35.0){
      iFunc = 3;
    }else if(pt < 45.0){
      iFunc = 4;
    }else if(pt < 60.0){
      iFunc = 5;
    }else if(pt < 100.0){
      iFunc = 6;
    }else{
      iFunc = 7;
    }

    //normal
    double qoverptLow = 1/ptLow - 1/pt;
    double qoverptUp  = 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w1Low = 0;
    double w1Up  = 0;
    if(fMuonFlag){
      w1Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w1Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w1Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w1 = w1Low - w1Up;

    //charge mis-id
    qoverptLow = - 1/ptLow - 1/pt;
    qoverptUp  = - 1/ptUp  - 1/pt;
    qoverptLow *= 1e+3; // GeV^-1 -> TeV^-1
    qoverptUp  *= 1e+3; // GeV^-1 -> TeV^-1
    double w2Low = 0;
    double w2Up  = 0;
    if(fMuonFlag){
      w2Low = fSmearInt_mu[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_mu[iFunc]->Eval(qoverptUp);
    }else{
      w2Low = fSmearInt_ele[iFunc]->Eval(qoverptLow);
      w2Up  = fSmearInt_ele[iFunc]->Eval(qoverptUp);
    }
    double w2 = w2Up - w2Low;
    
    double origEY = hHoge->GetBinError(i+1);
    double origY  = hHoge->GetBinContent(i+1);
    double EY = weight*(w1+w2);
    double Y = weight*(w1+w2);
    hHoge->SetBinError(i+1, TMath::Sqrt(origEY*origEY + EY*EY));
    hHoge->SetBinContent(i+1, origY + Y);
 }
}// 

double GetWeightLifetime(double PropTime, double OriginalTau, double TargetTau){
  if(TMath::Abs(PropTime) > 1e10)
    return 1.0;
  else
    return ((1.0/TargetTau)*TMath::Exp(-PropTime/TargetTau))/((1.0/OriginalTau)*TMath::Exp(-PropTime/OriginalTau));
}
